<?php
function outputCSV($data) {
    $output = fopen("scanresult/output/memberscan_".date("YmdHis").".csv", "w");
    //$output = fopen("file.csv", "w");
    foreach ($data as $row) {
        fputcsv($output, $row, ';'); // here you can change delimiter/enclosure
    }
    fclose($output);
}

$dir    = 'scanresult/'.date('Ymd').'/';
$files1 = scandir($dir); // sort asc
$files2 = scandir($dir, 1); //sort desc
$totalscan = 0;
$totalf = 0;
$totalr = 0;
$totalk = 0;
$totalsk = 0;
$currmember = '';
$flagx = 1;
//print_r($files1);
//print_r($files2);
//echo sizeof($files1);
//$x = 0;
$memberscan =  array(
				array("member_id", "form", "ktp", "rekening", "surat_kuasa", "createddate"),
			   );

echo 'List Of File : <br>/n/r';
for($x=0;$x<sizeof($files1);$x++){
	if($files1[$x]!='.' && $files1[$x]!='..'){
		//echo $files1[$x].'<br>';
		$expfile =explode('.',$files1[$x]);
		$expfilename =  explode('_',$expfile[0]);
		$memberid = $expfilename[0];

		if($currmember==''){
			$currmember = $memberid;
			$memberscan[$flagx][0] = $memberid;
		}else if($currmember!=$memberid){
			$flagx++;
			$currmember = $memberid;
			$memberscan[$flagx][0] = $memberid;
		}
		   
		switch($expfilename[1]) {
			case '001' : $filetype = 'Form';
						$memberscan[$flagx][1] = 1;
						$totalf++;
						break;
			case '001x' : $filetype = 'Invalid Form';
						$memberscan[$flagx][1] = 0;
						break;
			case '002' : $filetype = 'KTP';
						$memberscan[$flagx][2] = 1;
						$totalk++;
						break;
			case '002x' : $filetype = 'Invalid KTP';
						$memberscan[$flagx][2] = 0;
						break;
			case '003' : $filetype = 'Rekening';
						$memberscan[$flagx][3] = 1;
						$totalr++;
						break;
			case '003x' : $filetype = 'Invalid Rekening';
						$memberscan[$flagx][3] = 0;
						break;
			case '004' : $filetype = 'Surat Kuasa';
						$memberscan[$flagx][4] = 1;
						$totalsk++;
						break;
			case '004x' : $filetype = 'Invalid Surat Kuasa';
						$memberscan[$flagx][4] = 0;
						break;
			default : $filetype = 'Unknown';
						break;
						
		}
		$memberscan[$flagx][5] = date('Y-m-d');
		echo 'File Name = '.$files1[$x].' | Member ID = '.$memberid.' | Doc Type = '.$filetype.' <br>/n/r';
		$totalscan++;
	}
}
//print_r($memberscan);
echo ' <br>/n/r'.' <br>/n/r'.' <br>/n/r';
echo 'Finish Scan Directory.'.' <br>/n/r';

echo 'create CSV <br>/n/r';
outputCSV($memberscan);
echo 'done <br>/n/r';

echo 'Summary'.' <br>/n/r';
echo '========================'.' <br>/n/r';
echo 'Total Form : '.$totalf.' <br>/n/r';
echo 'Total Rekening : '.$totalr.' <br>/n/r';
echo 'Total KTP : '.$totalk.' <br>/n/r';
echo 'Total Surat Kuasa : '.$totalsk.' <br>/n/r';

?>