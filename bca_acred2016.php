 <?php
 //----------------------------- database ---------------------------//
$nama_svr = 'localhost';//'sgh-production.cy9qiwmgjpp1.ap-southeast-1.rds.amazonaws.com';
$nama_db = 'soho_20200630_rbfinclose'; //'soho';
$nama_usr = 'admin';
$pwd_usr = 'P@ssw0rd';//'H!21h3j00q1';//
/*
$nama_svr = 'sgh-production.cy9qiwmgjpp1.ap-southeast-1.rds.amazonaws.com';
$nama_db = 'soho';
$nama_usr = 'admin';
$pwd_usr = 'H!21h3j00q1';
*/
	$con = mysqli_connect($nama_svr, $nama_usr, $pwd_usr);
	if(!$con){
	  trigger_error("Problem connecting to server");
	}	
	$db =  mysqli_select_db( $con,$nama_db);
	if(!$db){
	   trigger_error("Problem selecting database");
	}	
//--------------------------- EOF database ------------------------//

 

$petik = ""; //"\'";
            $query = "SELECT ifnull(btb.bi_code,'') as bicode, dt.*
                FROM(
                        SELECT namaNasabah, bank_id, MAX(cabang)AS cabang, concat('".$petik."',`no`) as noRekening, SUM(ewallet)AS payoutTransfer
                                -- , MAX(biayaTransfer)AS biayaTransfer
                                -- , SUM(ewallet) -  MAX(biayaTransfer) AS tkHomePayed
                                , CASE 
									WHEN MAX(urut) = 0 THEN 0
									WHEN MAX(urut) = 1 THEN MAX(biayaTransfer)
								  END AS biayaTransfer
                                , CASE
									WHEN MAX(urut) = 0 THEN SUM(ewallet)
									WHEN MAX(urut) = 1 THEN	SUM(ewallet) -  MAX(biayaTransfer) 
								  END AS tkHomePayed
                                -- , urut2, flag
                                , CASE 
                                        WHEN MAX(urut) = 0 THEN 'BCA'
                                        WHEN MAX(urut) = 1 THEN 'Non BCA'
                                        ELSE 'NoValid'
                                END AS urut
								, 'Bonus Member' as Berita
                        FROM
                        (
                                SELECT dtbr.memberId AS memberId, dtbr.account_id
                                        , CASE WHEN dtbr.nama <> dtbr.namaNasabah THEN 'Cek' ELSE '' END AS note
                                        , dtbr.ewallet - IFNULL(bns.bns,0) AS b4, IFNULL(bns.bns,0)AS bns
                                        , dtbr.ewallet - 5000 AS ewallet, dtbr.nama AS nama, dtbr.namaNasabah
                                        , dtbr.bank_id AS bank_id, dtbr.cabang AS cabang
                                        , dtbr.urut AS urut2, flag
                                        , CASE 
                                                WHEN dtbr.urut THEN 5000
                                                WHEN flag = 2 THEN 2500
                                                WHEN flag = 1 THEN 0
                                                ELSE 'n/a'
                                        END AS biayaTransfer
                                        , IFNULL(REPLACE(REPLACE(REPLACE(dtbr.no,'-',''),'.',''),' ',''),'-') AS NO
                                        -- , dtbr.no as no
                                        , dtbr.urut
                                FROM(
                                        SELECT dt.*, CASE 
                                                        WHEN 
                                                                -- `no` LIKE '%000000%' 
                                                                `no` REGEXP '^[0-0]+$' -- LIKE '%000000%' 
                                                                OR namaNasabah LIKE '%000000%' 
                                                                OR `no` IS NULL 
                                                                OR (bank_id = 'BCA' AND LENGTH(REPLACE(REPLACE(REPLACE(`no`,'.',''),' ',''),'-','')) <> 10 )
                                                                OR bank_id = '-' 
																OR ( LENGTH(REPLACE(REPLACE(REPLACE(`no`,'.',''),' ',''),'-','')) < 8 )
                                                        THEN '2'
                                                        -- WHEN bank_id = 'BCA' AND `no` NOT LIKE '%000000%' THEN '0'
														WHEN bank_id = 'BCA' AND `no` NOT REGEXP '^[0-0]+$' THEN '0' 
                                                        ELSE '1' END AS urut
                                        FROM(
                                                SELECT 
                                                        m.id AS memberId, m.ewallet, m.nama, m.account_id
                                                        , IFNULL(acc.bank_id,'-')AS bank_id, IFNULL(acc.area,'-') AS cabang
                                                        , IFNULL(acc.no,'_')AS NO, UCASE(IFNULL(acc.`name`,'-')) AS namaNasabah
                                                        , acc.flag
                                                FROM member m
                                                LEFT JOIN account acc ON m.account_id = acc.id
                                                WHERE m.ewallet >= 55000 AND m.id <> '00000001'
                                                -- and acc.bank_id = 'BCA'
                                                GROUP BY m.id
                                                ORDER BY acc.bank_id DESC, m.ewallet DESC
                                        )AS dt
                                        GROUP BY dt.memberId
                                        ORDER BY dt.bank_id DESC, dt.ewallet DESC
                                )AS dtbr
                                LEFT JOIN(
                                        SELECT member_id, SUM(nominal) AS bns
                                        FROM bonus
                                        WHERE periode = LAST_DAY(CURDATE() - INTERVAL 1 MONTH)
                                        GROUP BY member_id
                                )AS bns ON dtbr.memberId = bns.member_id
                                WHERE ewallet-5000 > 50000
                                ORDER BY urut, bank_id, ewallet DESC
                        )AS dt
                        -- WHERE bank_id = 'BCA'
                        GROUP BY namaNasabah, bank_id, `no`
                        ORDER BY urut, namaNasabah
                )AS dt
				left join bank_to_bicode btb ON dt.bank_id = btb.id
                WHERE urut <> 'NoValid'
				and urut = 'BCA'
				/*
				and noRekening NOT IN (
				'0000000009'
				-- juni 2016
				,'7795052963'
				,'2156700001'
				,'0000000008'
				,'2700008081'
				,'0813362728'
				,'0575023696'
				,'7290045034'
				,'0120280899'
				,'1038541868'
				,'7910493688'
				,'0038837567'
				,'0291059104'
				)
				*/
				-- juli 2016
				/*
				and noRekening NOT IN (
				'4050265931'
				,'2571540001'
				,'7290045034'
				,'0140164772'
				,'0887140130'
				,'0291059104'
				)
				*/
				/*
				-- agustus 2016
				and noRekening NOT IN (
				'4050265931'
				,'7820101787'
				,'4780061591'
		   		,'255726353'
				)
				*/
				-- september 2016
				/*
				and noRekening NOT IN (
				'120280899'
				)
				*/
				/*
				-- october 2016
				and noRekening NOT IN (
				'8400012844'
				,'8140015501'
				,'7925178389'
				,'1191868183'
				)
				*/
				-- november 2016
				/*
				and noRekening NOT IN (
				'3890296598'
				,'1010492820'
				,'6540127008'
				,'7925178389'
				,'7910170584'
				,'7570028027'
				,'0446543456'
				,'0262041385'
				,'7910174473'
				,'7920717254'
				)
				*/
				-- desember 2016
				/*
				and noRekening NOT IN (
				'7098978024'
				,'6041024973'
				,'0446543456'
				,'0255357161'
				,'0653453890'
				,'5865057722'
				,'0260733109'
				)
				*/
				-- januari 2017
				/*
				and noRekening NOT IN (
				'7098978024'
				,'5250674464'
				,'7975011844'
				,'0640463830'
				)
				*/
				-- Februari 2017
				/*
				and noRekening NOT IN (
				'7955088866'
				,'0506127092'
				,'7975011844'
				,'0640463830'
				,'0248532625'
				)
				*/
				/*
				-- Maret 2017
				and noRekening NOT IN (
				'1410476271'
				,'0656096050'
				,'3213213111'
				,'7500043231'
				,'1003689928'
				)
				*/
				/*
				-- April 2017
				and noRekening NOT IN (
				'3213213111'
				,'7500043231'
				,'5125032467'
				,'4591564633'
				,'0640463830'
				)
				-- limit 0,64
				*/
				/*
				-- MEI 2017
				and noRekening NOT IN (
				'0072611726'
				,'0466497397'
				)
				*/
				/*
				-- JUNI 2017
				and noRekening NOT IN (
				'8295031678'
				,'4780061591'
				,'3503001795'
				,'0072611726'
				,'0011223548'
				,'6440004141'
				,'0123654987'
				,'0324970528'
				,'0466497397'
				,'0197670446'
				)
				*/
				/*
				-- JULI 2017
				and noRekening NOT IN (
				'0320303328'
				,'4780061591'
				,'0197670446'
				,'6440004141'
				,'8530543871'
				)
				*/
				/*
				--  AGUSTUS 2017
				and noRekening NOT IN (
				'0320303328'
				,'8530543871'
				,'0197670446'
				,'7004384916'
				,'6440004141'
				)
				*/
				/*
				--  SEPTEMBER 2017
				and noRekening NOT IN (
				'1122334567'
				,'7920757575'
				)
				*/
				/*
				--  OKTOBER 2017
				and noRekening NOT IN (
				'8140062105'
				,'7920276583'
				,'0320229991'
				,'7920757575'
				,'6560186547'
				,'1022778825'
				,'1642022489'
				,'0221092656'
				,'7920883000'
				)
				*/
				/*
				-- NOVEMBER 2017
				and noRekening NOT IN (
				'0522012844'
				,'0322076868'
				,'5000584331'
				)
				*/
				/*
				-- DECEMBER 2017
				and noRekening NOT IN (
				'0322076868'
				,'0514118078'
				)
				*/
				/*
				-- Januari 2018
				and noRekening NOT IN (
				'0522012844'
				,'0511559323'
				,'0799016617'
				)
				*/
				/*
				-- Februari 2018
				and noRekening NOT IN (
				'0536566261'
				,'8320000394'
				,'0522012844'
				,'0651516659'
				,'0255830223'
				,'7245036425'
				)
				*/
				/*
				-- Maret 2018
				and noRekening NOT IN (
				'tidakdiisi'
				,'6262562407'
				,'0536566261'
				,'0601740931'
				,'0651516659'
				)
				*/
				/*
				-- April 2018
				and noRekening NOT IN (
				'8025008477'
				,'7800112230'
				,'7811002430'
				)
				*/
				/*
				-- May 2018
				and noRekening NOT IN (
				'0038318907'
				,'0674879459'
				,'8020384592'
				,'7990807682'
				)
				*/
				/*
				-- Juni 2018
				and noRekening NOT IN (
				'0506252761'
				,'7640438643'
				,'0192243131'
				,'7990807682'
				)
				*/
				/*
				-- July 2018
				and noRekening NOT IN (
				'3909519772'
				,'0192243131'
				,'7990807682'
				)
				*/
				/*
				-- Agustus 2018
				and noRekening NOT IN (
				'3909519772'
				,'0108377888'
				,'2478987569'
				,'6225011140'
				,'2356734685'
				,'0081835956'
				,'0192243131'
				,'7990807682'
				)
				*/
				/*
				-- September 2018
				and noRekening NOT IN (
				'3909519772'				   
				,'0108377888'
				,'6225011140'
				,'2356734685'
				,'0192243131'
				,'7990807682'
				,'0081835956'
				,'7935339907'
				,'7870482242' -- Dela Marsella info pak riky
				,'7870508527' -- Marlengga info pak riky
				)
				*/
				/*
				-- October 2018
				-- 7870508527 --> dicairkan marlengga pensiun
				and noRekening NOT IN (
				'1070546821'
				,'7935339907'
				,'0108377888'
				,'6225011140'
				,'0081835956'
				,'0192243131'
				,'7990807682'
				,'7870482242' -- Dela Marsella info pak riky
				)
				*/
				/*
				-- November 2018
				and noRekening NOT IN (
				'7870482242' -- Dela Marsella info pak riky
				,'1773086592'
				,'7935339907'
				,'0108377888'
				,'6225011140'
				,'0081835956'
				,'0192243131'
				,'0813403211'
				,'7990807682'
				)
				*/
				/*
				-- Desember 2018
				and noRekening NOT IN (
				'1773086592'				   
				,'3909519772'
				,'0292392931'
				,'7935339907'
				,'3890388239'
				,'0108377888'
				,'6252763836'
				,'7019193739'
				,'6225011140'
				,'0575238508'
				,'0081835956'
				,'0192243131'
				,'7990807682'
				)
				*/
				/*
				-- Januari 2019
				and noRekening NOT IN (
				'1773086592'
				,'3909519772'
				,'0292392931'
				,'7935339907'
				,'1285783614'
				,'0108377888'
				,'7019193739'
				,'6225011140'
				,'0697076027'
				,'0081835956'
				,'0192243131'
				,'7990807682'
				)
				*/
				/*
				-- Februari 2019
				and noRekening NOT IN (
				'7890745534'
				,'1773086592'
				,'3909519772'
				,'0292392931'
				,'7935339907'
				,'1285783614'
				,'0108377888'
				,'7910327700'
				,'7019193739'
				,'6225011140'
				,'0697076027'
				,'0081835956'
				,'0192243131'
				,'7990807682'
				)
				*/
				/*
				-- MARET 2019
				and noRekening NOT IN (
				'7890745534'
				,'7935339907'
				,'0108377888'
				,'6252763836'
				,'7019193739'
				,'6225011140'
				,'2850039910'
				,'0081835956'
				,'0192243131'
				,'7990807682'
				,'1773086592'
				,'3909519772'
				,'0292392931'
				,'1285783614                  '
				)
				*/
				/*
				-- APRIL 2019
				and noRekening NOT IN (
				'1070546821'
				,'3909519772'
				,'7935339907'
				,'0108377888'
				,'6225011140'
				,'0081835956'
				,'0192243131'
				,'7990807682'
				,'7890745534'
				,'1773086592'
				,'0292392931'
				,'1285783614'
				,'6130003135'
				,'8205012345'
				,'8803839233'
				,'7019193739'
				,'9710036585'
				)
				*/
				/*
				-- MEI 2019
				and noRekening NOT IN (
				'1410618008'
				,'8315061893'
				,'1773086592'
				,'3909519772'
				,'0292392931'
				,'7935339907'
				,'1285783614'
				,'6130003135'
				,'8205012345'
				,'8205032316'
				,'0108377888'
				,'1010134272'
				,'1941370727'
				,'8803839233'
				,'1710479824'
				,'6252763836'
				,'7019193739'
				,'6225011140'
				,'9710036585'
				,'0192243131'
				,'7890745534'
				,'0081835956'
				)
				*/
				/*
				-- JUNI 2019
				and noRekening NOT IN (
				'7092992197'
				,'1411123811'
				,'8315061893'
				,'8205032316'
				,'1410658998'
				,'2850039910'
				,'9710036585'
				,'1415044659'
				)
				*/
				/*
				-- JULI 2019
				and noRekening NOT IN (
				'8530033633'
				,'1025616215'
				,'0429945527'
				,'1150555588'
				,'2581763975'
				)
				*/
				/*
				-- AGUSTUS 2019
				and noRekening NOT IN (
				'1005544664'
				,'8205334567'
				,'7500016321'
				,'1900242711'
				)
				*/
				/*
				-- SEPTEMBER 2019
				and noRekening NOT IN (
				'0210146387'
				,'7920767813'
				,'7910982223'
				,'0674879459'
				,'7085017016'
				,'7990807682'
				)
				*/
				/*
				-- OKTOBER 2019
				and noRekening NOT IN (
				'0550141656'
				,'7491183256'
				,'8765033316'
				,'8503031367'
				,'8705032456'
				)
				*/
				/*
				-- NOVEMBER 2019
				and noRekening NOT IN (
				'0550141656'
				,'7491183256'
				,'1436968679'
				,'7880500585'
				)
				*/
				/*
				-- DECEMBER 2019
				and noRekening NOT IN (
				'1436968679'
				,'7910286167'
				,'1051377473'
				,'6025260000'
				,'7401102998'
				,'1271125101'
				,'8190212711'
				,'4650297423'
				,'8205033340'
				,'8205033343'
				,'7870095819'
				,'8205033318'
				)
				*/
				/*
				-- JANUARI 2020
				and noRekening NOT IN (
				'7910286167'
				,'3730006411'
				,'1051377473'
				,'6025260000'
				,'1271125101'
				,'8205033340'
				,'8205033367'
				,'8530248081'
				,'8205033343'
				,'8720282743'
				,'8205033318'
				)
				*/
				/*
				-- FEBRUARI 2020
				and noRekening NOT IN (
				'0272541255'
				)
				*/
				/*
				-- MARET 2020
				and noRekening NOT IN (
				'0251524998'
				,'4780061591'
				,'8200392966'
				,'0272541255'
				,'0714659973'
				)
				*/
				/*
				-- APRIL 2020
				and noRekening NOT IN (
				'3011217625'
				,'8200392966'
				,'7810700566'
				,'5171679743'
				,'0701571542'
				)
				*/
				/*
				-- MEI 2020
				and noRekening NOT IN (
				'1410559875'
				,'7910128167'
				,'6129409458'
				,'0701571542'
				)
				*/
				-- JUNI 2020
				and noRekening NOT IN (
				'7140313009'
				,'1234567812'
				,'0989877601'
				,'7140313005'
				,'8642060000'
				,'7122678417'
				,'7772106852'
				)
				";
$queryData	= mysqli_query($con,$query) or die(mysqli_error($con));
$xx = 0;
$totalAmount = 0;
echo 'start retrive detail data'."<br>"."\r\n";
while ($row = mysqli_fetch_array($queryData, MYSQLI_ASSOC))
{
	/*
	Field	Name	Type	Length	Value	Mandatory
	
	Detail 					
	1	RECORD-TYPE 				Number	1	1 =Detail 	TRUE
	2	Credited Account 			Number	34	Credit Account 	TRUE
	3	Filler 						String	18	-	TRUE
	4	Credited Amount 			Number	17.2	Account to be credited	TRUE
	5	Credited Amount  Name 		String	70	Receiving Account Name (mandatory for LLG / RTGS)	TRUE 
	6	Transfer Type 				String	6	BCA = B / LLG= N / RTGS = Y	TRUE 
	7	Filler 						String	1	-	
	8	BI CODE 					String 	7		
	9	Filler						String 	4		
	10	Bank Name 					String 	18	bank name 	
	11	RECEIVER-BANK-BRANCH 		String	18		FALSE 
	12	TRANSACTION-REMARK-1		String	18	Remarks	FALSE
	13	TRANSACTION-REMARK-2		String	18	Remarks	FALSE
	14	Filler 						String 	18		
	15	Receiver Cust Type 			String 	1	Jenis Nasabah Penerima (1 = perorangan , 2 = perusahaan, 3 = pemerintah )	TRUE 
	16	Receiver Cust Residence 	String 	1	Status Penduduk Nasabah Penerima	TRUE ( R = penduduk , N = bukan penduduk) - (Non BCA only)	
	17	Bank Code 					Number	3	014/888	014 =BCA , 888 = Non BCA 
	*/
	
	$detail['RECORD-TYPE'][$xx] 			= "1"; 
	$detail['credited_account'][$xx] 		= str_pad($row['noRekening'],34," ",STR_PAD_RIGHT);
	$detail['filler1'][$xx] 				= str_pad("",18," ",STR_PAD_RIGHT); 
	$detail['credited_amount'][$xx] 		= str_pad($row['tkHomePayed'],17,"0",STR_PAD_LEFT).".00";  $totalAmount+=$row['tkHomePayed']; 
	$detail['credited_amount_name'][$xx] 	= str_pad($row['namaNasabah'],70," ",STR_PAD_RIGHT);
	if($row['urut'] == 'BCA') 
		$detail['transfer_type'][$xx] 			= str_pad("B",6," ",STR_PAD_RIGHT);  //BCA = B / LLG= N / RTGS = Y
	else
		$detail['transfer_type'][$xx] 			= str_pad("N",6," ",STR_PAD_RIGHT);  //BCA = B / LLG= N / RTGS = Y
	$detail['filler2'][$xx] 				= str_pad("",1," ",STR_PAD_RIGHT); 
	$detail['BI_CODE'][$xx] 				= str_pad($row['bicode'],7," ",STR_PAD_RIGHT); //BCA = 1111111 
	$detail['filler3'][$xx] 				= str_pad("",4," ",STR_PAD_RIGHT); 
	$detail['bank_name'][$xx] 				= str_pad($row['bank_id'],18," ",STR_PAD_RIGHT); 
	$detail['RECEIVER-BANK-BRANCH'][$xx] 	= str_pad(substr($row['cabang'],0,18),18," ",STR_PAD_RIGHT); 
	$detail['TRANSACTION-REMARK-1'][$xx] 	= str_pad("BONUS MEMBER",18," ",STR_PAD_RIGHT); 
	$detail['TRANSACTION-REMARK-2'][$xx] 	= str_pad("JUNI 2020",18," ",STR_PAD_RIGHT); 
	$detail['filler4'][$xx] 				= str_pad("",18," ",STR_PAD_RIGHT); 
	$detail['receiver_cust_type'][$xx] 		= "1";  // (1 = perorangan , 2 = perusahaan, 3 = pemerintah )
	$detail['receiver_cust_residence'][$xx] = "R"; //( R = penduduk , N = bukan penduduk) - (Non BCA only)
	if($row['urut'] == 'BCA')
		$detail['bank_code'][$xx] 				= "014"; //014 =BCA , 888 = Non BCA 
	else
		$detail['bank_code'][$xx] 				= "888"; //014 =BCA , 888 = Non BCA 

	$xx++;
}

 /*
 Header 					
1	RECORD-TYPE 		Number	1	0 = Header 	TRUE
2	TRANSACTION-TYPE 	String	2	SP/MP	TRUE
3	EFFECTIVE-DATE 		Number	8	Format YYYYMMDD	TRUE
4	COMPANY-ACCOUNT 	Number	10	To be debited account	TRUE
5	Filler				String 	1	RightPad w/ Space 	-
6	COMPANY-CODE 		Number	8	Company code Auto Credit	TRUE
7	Filler				String 	15		
8	TOTAL-AMOUNT		Number	17.2	Include Decimal Amount	TRUE
9	TOTAL-RECORD 		Number	5	Total of detail records	TRUE
10	Transfer type 		String 	3	BCA/LLG/RTG	TRUE 
11	Filler 				String 	15	-	
12	Remark 1 			String	18	Remark 1 	FALSE 
13	Remark 2 			String	18	Remark 2 	FALSE 
14	Filler 				String 	132	- 	

 */
echo "start retrieve header Data"."<br>"."\r\n";
$header['RECORD-TYPE'] = "0"; 
$header['TRANSACTION-TYPE'] = "SP"; // SP = BCA, MP = NON BCA
$header['EFFECTIVE-DATE'] = "20200709"; 
$header['COMPANY-ACCOUNT'] = "0033059008";  //BCA unihealth
$header['filler1'] = str_pad("",1," ",STR_PAD_LEFT); 
$header['COMPANY-CODE'] = str_pad("00030331",8," ",STR_PAD_LEFT); 
$header['filler2'] = str_pad("",15," ",STR_PAD_LEFT); 
$header['TOTAL-AMOUNT'] = str_pad($totalAmount,17,"0",STR_PAD_LEFT).".00"; 
$header['TOTAL-RECORD'] = str_pad($xx,5,"0",STR_PAD_LEFT); 
$header['transfer_type'] = "BCA";  //BCA/LLG/RTG
$header['filler3'] = str_pad("",15," ",STR_PAD_LEFT);
$header['remark1'] = str_pad("BONUS MEMBER",18," ",STR_PAD_RIGHT); 
$header['remark2'] = str_pad("JUNI 2020",18," ",STR_PAD_RIGHT); 
$header['filler4'] = str_pad("",132," ",STR_PAD_LEFT); 

$headerText = "";

$headerText.= $header['RECORD-TYPE'];
$headerText.= $header['TRANSACTION-TYPE'];
$headerText.= $header['EFFECTIVE-DATE'];
$headerText.= $header['COMPANY-ACCOUNT'];
$headerText.= $header['filler1'];
$headerText.= $header['COMPANY-CODE'];
$headerText.= $header['filler2'];
$headerText.= $header['TOTAL-AMOUNT'];
$headerText.= $header['TOTAL-RECORD'];
$headerText.= $header['transfer_type'];
$headerText.= $header['filler3'];
$headerText.= $header['remark1'];
$headerText.= $header['remark2'];
$headerText.= $header['filler4'];
			  
echo "creating file..."."<br>"."\r\n";
$namaFile = "acred_".date("YmdHis").".txt";
$myfile = fopen("bcatransferfile/".$namaFile, "wb") or die("Unable to open file!");
//$myfile = fopen("/home/deploy/source/sohomlm/bcatransferfile/".$namaFile, "wb") or die("Unable to open file!");
fwrite($myfile, $headerText."\r\n");
for($yy = 0; $yy < $xx; $yy++){
	$detailText = "";
	$detailText.= $detail['RECORD-TYPE'][$yy];
	$detailText.= $detail['credited_account'][$yy];
	$detailText.= $detail['filler1'][$yy];
	$detailText.= $detail['credited_amount'][$yy];
	$detailText.= $detail['credited_amount_name'][$yy];
	$detailText.= $detail['transfer_type'][$yy];
	$detailText.= $detail['filler2'][$yy];
	$detailText.= $detail['BI_CODE'][$yy];
	$detailText.= $detail['filler3'][$yy];
	$detailText.= $detail['bank_name'][$yy];
	$detailText.= $detail['RECEIVER-BANK-BRANCH'][$yy];
	$detailText.= $detail['TRANSACTION-REMARK-1'][$yy];
	$detailText.= $detail['TRANSACTION-REMARK-2'][$yy];
	$detailText.= $detail['filler4'][$yy];
	$detailText.= $detail['receiver_cust_type'][$yy];
	$detailText.= $detail['receiver_cust_residence'][$yy];
	$detailText.= $detail['bank_code'][$yy];
	fwrite($myfile, $detailText."\r\n");
}
fclose($myfile);

echo "||||||||||.... 100% DONE"."<br>"."\r\n";
echo "<a href='bcatransferfile/".$namaFile."'>".$namaFile."</a>";
?> 

