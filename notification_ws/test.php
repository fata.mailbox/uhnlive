<?php
    require_once "application/libraries/SimpleXMLExtend.php";
	$test = '<test/>';
   
    $msg = "This is a test & stuff.";
   
    $xml = new SimpleXMLExtend($test);
    $xml->addAttribute('foo', $msg);
   
    echo htmlentities($xml->asXML());
   
    $xml->addChild('bar', $msg);
   
    echo htmlentities($xml->asXML());
?>
