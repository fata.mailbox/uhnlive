<?php
class Minusstockmodel extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function deleteMinusStock(){
	   $result = array();
	       $query = "Delete from minus_stock";
			log_message('INFO','Delete minus stock query = "'.$query.'"');
			$rs = $this->db->query($query);
			
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
	   
		return $result;
        
	}
}
?>