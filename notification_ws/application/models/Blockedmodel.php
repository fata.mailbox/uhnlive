<?php
class Blockedmodel extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function getStatus(){
	   
		$rs = $this->db->query("SELECT status from blocked where blockid = 1");
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}
}
?>