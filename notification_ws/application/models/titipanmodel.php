<?php
class Titipanmodel extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function getMinusList(){
	   
		$rs = $this->db->query("SELECT t.member_id, s.no_stc, m.nama, t.item_id, i.name AS prd, t.qty, t.harga
FROM titipan t
LEFT JOIN item i ON t.item_id = i.id
LEFT JOIN member m ON t.member_id = m.id
LEFT JOIN stockiest s ON m.id = s.id
WHERE t.qty < 0
AND t.member_id <> 4");
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}
}
?>