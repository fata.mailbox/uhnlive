<?php
class Inventoryreport extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function checkData($PERIOD_ID, $SUB_DIST_ID, $SUBMIT_DATE='', $PART_NO=''){
	    if($SUBMIT_DATE!=''){
			$wQuery = " AND SUBMIT_DATE = TO_DATE('$SUBMIT_DATE','yyyymmdd')";	
		}else{
			$wQuery = "";
		}
	    if($PART_NO!=''){
			$wQuery.= " AND PART_NO = '$PART_NO'";	
		}else{
			$wQuery.= "";
		}
		$rs = $this->db->query("SELECT * 
    				    FROM CTM_SDR_REPORT_INVENTORY_TAB
                        WHERE PERIOD_ID = '$PERIOD_ID' AND SUB_DIST_ID = '$SUB_DIST_ID'".$wQuery);// AND SUBMIT_DATE = TO_DATE('$SUBMIT_DATE','yyyymmdd') AND PART_NO = '$PART_NO'");
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}
	
	function insertInventory($PERIOD_ID,$SUB_DIST_ID,$SUBMIT_DATE,$PART_NO,$QTY_ONHAND,$QTY_SALEABLE,$QTY_NOT_SALEABLE,$ROWVERSION)
	{
	   $checkData = $this->checkData($PERIOD_ID, $SUB_DIST_ID, $SUBMIT_DATE, $PART_NO);
	   $result = array();
	   if ($checkData['countResult']==0){
		   $query = "INSERT 
							INTO CTM_SDR_REPORT_INVENTORY_TAB
							(
							 	PERIOD_ID,
								SUB_DIST_ID,
								SUBMIT_DATE,
								PART_NO,
								QTY_ONHAND,
								QTY_SALEABLE,
								QTY_NOT_SALEABLE,
								ROWVERSION
							)
							VALUES
							(
							 	'$PERIOD_ID',
								'$SUB_DIST_ID',
								TO_DATE('$SUBMIT_DATE','yyyymmdd'),
								'$PART_NO',
								$QTY_ONHAND,
								$QTY_SALEABLE,
								$QTY_NOT_SALEABLE,
								SYSDATE
							 )
							";
			$rs = $this->db->query($query);
			
			log_message('INFO','Insert query = "'.$query.'"');
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
	   }else{
		   $result['responseCode']='10';	
	   }
		return $result;
        
	}
}
?>