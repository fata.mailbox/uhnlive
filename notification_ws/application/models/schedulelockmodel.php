<?php
class Schedulelockmodel extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function updateSched(){
	   $result = array();
	       $query = "UPDATE scheduler_lock SET blocked = 1, created_date = CURRENT_TIMESTAMP WHERE scheduler_name = 'abnormal_so';
					";
			log_message('INFO','Update Period query = "'.$query.'"');
			$rs = $this->db->query($query);
			
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
	   
		return $result;
        
	}
}
?>