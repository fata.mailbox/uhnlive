<?php
class Subdistmodel extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function getSubdistMstList(){
		$rs = $this->db->query("SELECT * 
    				    FROM CTM_SDR_SUB_DIST_MST_TAB");
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
	}
}
?>