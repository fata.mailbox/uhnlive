<?php
class Usermodel extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function getData($user,$password){
	   
		$rs = $this->db->query("SELECT a.*,b.* 
    				    FROM ctm_sdr_sub_dist_user_tab a, ctm_sdr_sub_dist_mst_tab b
                        WHERE a.sub_dist_id=b.sub_dist_id and a.user_id = '$user' and a.password = '$password'");
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}
	function getUserData($user_id){
	   
		$rs = $this->db->query("SELECT a.*,b.* 
    				    FROM ctm_sdr_sub_dist_user_tab a, ctm_sdr_sub_dist_mst_tab b
                        WHERE a.sub_dist_id=b.sub_dist_id and a.user_id = '$user_id'");
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}
	
	function getUserListView($start,$limit){
		$query = '
		SELECT * FROM 
		(
		  SELECT a.USER_ID, a.ROWVERSION, b.NAME, 
		  ROW_NUMBER() OVER (ORDER BY a.ROWVERSION DESC) R 
		  FROM ctm_sdr_sub_dist_user_tab a, ctm_sdr_sub_dist_mst_tab b
		  WHERE a.SUB_DIST_ID=b.SUB_DIST_ID
		)		
		';
		if($start!=''&&$limit!=''){
			$end = $start + $limit - 1;
			$wQuery = 'WHERE R BETWEEN '.$start.' and '.$end;
		}else{
			$wQuery = '';
		}
		$rs = $this->db->query($query.$wQuery);
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

		$rsTotal = $this->db->query($query);
        $result['countResult']=$rs->num_rows();
		$result['totalRow']=$rsTotal->num_rows();
        $rs->free_result();
		return $result;
        
	}
	
	function updatePass($user,$oldpassword,$newpassword){
	   $checkData = $this->getData($user,$oldpassword);
	   $result = array();
	   if ($checkData['countResult']>0){
		   $query = "UPDATE
					ctm_sdr_sub_dist_user_tab
					SET password = '$newpassword'
					WHERE user_id = '$user'";
			$rs = $this->db->query($query);
			
			log_message('INFO','Update user password query = "'.$query.'"');
			if($rs){
				$result['responseCode']='00';
			}else{
				$result['responseCode']='02';	
			}
	   }else{
		   $result['responseCode']='01';	
	   }
		return $result;
        
	}
}
?>