<?php
class Somodel extends CI_Model{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function getList(){
	   	/*
		$rs = $this->db->query("SELECT so.id, so.tgl, s.no_stc AS nostc, m.nama, i.name AS prd, sod.qty, sod.harga, sod.pv, sod.qty*sod.harga AS totalharga, r.nama AS region
FROM so
LEFT JOIN member m ON so.stockiest_id = m.id
LEFT JOIN stockiest s ON m.id = s.id
LEFT JOIN kota k ON s.kota_id = k.id
LEFT JOIN region r ON k.region = r.id
LEFT JOIN so_d sod ON so.id = sod.so_id
LEFT JOIN item i ON sod.item_id = i.id
WHERE so.tgl BETWEEN DATE_FORMAT(NOW() ,'%Y-%m-01') AND LAST_DAY(DATE_FORMAT(NOW() ,'%Y-%m-01'))
AND so.totalharga_ = 0 AND so.totalharga > 0");
		*/
		$rs = $this->db->query("SELECT ms.so_id as id, so.tgl, s.no_stc AS nostc, m.nama
	, i.name AS prd, sod.qty, sod.harga, sod.pv, sod.qty * sod.harga AS totalharga
	, r.nama AS region
FROM minus_stock ms
LEFT JOIN so ON ms.so_id = so.id
LEFT JOIN stockiest s ON so.stockiest_id = s.id
LEFT JOIN member m ON s.id = m.id
LEFT JOIN kota k ON s.kota_id = k.id
LEFT JOIN region r ON k.region = r.id
LEFT JOIN titipan t ON ms.titipan_id = t.id
LEFT JOIN so_d sod ON so.id = sod.so_id AND t.member_id = so.stockiest_id AND t.harga = sod.harga
LEFT JOIN item i ON t.item_id = i.id");
        
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}
	function getListBulanTujuh(){
	   	 /*
		$rs = $this->db->query("SELECT so.id, so.tgl, s.no_stc AS nostc, m.nama, i.name AS prd, sod.qty, sod.harga, sod.pv, sod.qty*sod.harga AS totalharga, r.nama AS region
FROM so
LEFT JOIN member m ON so.stockiest_id = m.id
LEFT JOIN stockiest s ON m.id = s.id
LEFT JOIN kota k ON s.kota_id = k.id
LEFT JOIN region r ON k.region = r.id
LEFT JOIN so_d sod ON so.id = sod.so_id
LEFT JOIN item i ON sod.item_id = i.id
WHERE so.tgl BETWEEN '2014-07-01' AND '2014-07-31'
AND so.totalharga_ = 0 AND so.totalharga > 0");
        */
		$rs = $this->db->query("SELECT ms.so_id as id, so.tgl, s.no_stc AS nostc, m.nama
	, i.name AS prd, sod.qty, sod.harga, sod.pv, sod.qty * sod.harga AS totalharga
	, r.nama AS region
FROM minus_stock ms
LEFT JOIN so ON ms.so_id = so.id
LEFT JOIN stockiest s ON so.stockiest_id = s.id
LEFT JOIN member m ON s.id = m.id
LEFT JOIN kota k ON s.kota_id = k.id
LEFT JOIN region r ON k.region = r.id
LEFT JOIN titipan t ON ms.titipan_id = t.id
LEFT JOIN so_d sod ON so.id = sod.so_id AND t.member_id = so.stockiest_id AND t.harga = sod.harga
LEFT JOIN item i ON t.item_id = i.id");
        $result = array();
        if ($rs->num_rows() > 0) {
            foreach($rs->result_array() as $row ) {
                $result['data'][] = $row;
            }
        }

        $result['countResult']=$rs->num_rows();
        $rs->free_result();
		return $result;
        
	}
}
?>