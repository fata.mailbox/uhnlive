<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Myresizer {
 
    function resizecrop($params){
		//ambil nilai dari parameter
		$file=$params['file'];
		$width=$params['width'];
		$height=$params['height'];
		$newwidth=$params['newwidth'];
		$newheight=$params['newheight'];
 
		//load library image_lib CodeIgniter 			
		$CI =& get_instance();
		$CI->load->library('image_lib');
 
		$config['source_image'] = $file;
		if(!empty($params['newfile']) )
			$config['new_image'] = $params['newfile'];
		$config['width'] = $newwidth;
		$config['height'] = $newheight;
		$config['maintain_ratio'] = TRUE;
 
		//menghitung posisi tengah untuk crop dan master_dim untuk resize	
		if( ($width/$newwidth) <= ($height/$newheight) ){
			$diff = ($newwidth/$width*$height)-$newheight;
			$x_axis = 0;
			$y_axis = round($diff / 2);
			$config['master_dim'] = 'width';
		}else{
			$diff = ($newheight/$height*$width)-$newwidth;
			$x_axis = round($diff / 2);
			$y_axis = 0;
			$config['master_dim'] = 'height';
		}
		$CI->image_lib->initialize($config);
		if ( ! $CI->image_lib->resize()){
			echo $CI->image_lib->display_errors();
		}
		unset($config);            
		$CI->image_lib->clear();
 
		//cropping
		if(!empty($params['newfile']) )
			$config['source_image'] = $params['newfile'];
		else
			$config['source_image'] = $file;
		$config['x_axis'] = $x_axis;
		$config['y_axis'] = $y_axis;
		$config['maintain_ratio'] = FALSE;
		$config['width'] = $newwidth;
		$config['height'] = $newheight;
 
		$CI->image_lib->initialize($config);
		if ( ! $CI->image_lib->crop()){
			echo $CI->image_lib->display_errors();
		}
		unset($config);            
		$CI->image_lib->clear();
    }
}
?>
