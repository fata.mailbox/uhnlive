<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Aboutus extends CI_Controller {
    function __construct()
    {
		parent::__construct();
		$this->load->model('MFrontend');
	}
	
	public function index(){
		$data['testi'] = $this->MFrontend->list_testimonial(10,0);
		
		$data['subcontent'] = 'index/tentang_kami';
		$data['content'] = 'index/aboutus';
		$this->load->view('index/index',$data);
	}
	public function visi(){
		$data['testi'] = $this->MFrontend->list_testimonial(10,0);
		
		$data['content'] = 'index/visi';
		$this->load->view('index/index',$data);
	}
	public function misi(){
		$data['testi'] = $this->MFrontend->list_testimonial(10,0);
		
		$data['content'] = 'index/misi';
		$this->load->view('index/index',$data);
	}
	public function budaya(){
		$data['testi'] = $this->MFrontend->list_testimonial(10,0);
		
		$data['content'] = 'index/budaya';
		$this->load->view('index/index',$data);
	}
}?>
