<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Main extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model('MMenu');
    }
    
    public function index(){
        $data['page_title'] = "Welcome ".$this->session->userdata('name');
        $this->load->view('main_backend',$data);
    }
    
}
?>