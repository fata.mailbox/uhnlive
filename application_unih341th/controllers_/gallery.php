<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {
    function __construct()
    {
	parent::__construct();
	$this->load->model('MFrontend');
    }
    
    public function index(){
        $this->load->library('pagination');
        
        $config['base_url'] = site_url().'gallery/index/';
        $config['per_page'] = 9;
        $config['uri_segment'] = 3;
        $config['total_rows'] = $this->MFrontend->count_list_gallery();
        $this->pagination->initialize($config);
        $data['results'] = $this->MFrontend->list_gallery($config['per_page'],$this->uri->segment($config['uri_segment']));
        
        //$data['news'] = $this->MFrontend->list_news('Y',5,0);
		$data['testi'] = $this->MFrontend->list_testimonial('Y',10,0);
	
        $data['content'] = 'index/gallery_photo';
        $this->load->view('index/index',$data);
    }
    
    public function album($album){
        $this->load->library('pagination');
        
		$album = str_replace("_",' ',$album);
		$config['base_url'] = site_url().'gallery/album/'.$this->uri->segment(3);
        $config['per_page'] = 9;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->MFrontend->count_gallery_album($album);
        $this->pagination->initialize($config);
        $data['results'] = $this->MFrontend->list_gallery_album($album,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
		$data['testi'] = $this->MFrontend->list_testimonial('Y',10,0);
	
		$data['album']=$album;
        $data['content'] = 'index/photo_gallery_album';
        $this->load->view('index/index',$data);
    }
}
?>