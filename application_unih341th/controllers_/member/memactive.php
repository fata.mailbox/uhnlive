<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MEmactive extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('MReport','MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('flag','','');
        
        if($this->form_validation->run()){
            $data['results']=$this->MReport->activedMember($this->input->post('flag'));
            //$data['total']=$this->MReport->sumActivedMember($this->input->post('flag'));
        }
        
        $data['page_title'] = 'Member Actived';
        $this->load->view('member/member_actived',$data);
    }
        
}
?>