<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Confreg extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id')>100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MSignup'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'member/confreg/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MSignup->countRegister($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MSignup->searchRegister($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Register Online';
        $this->load->view('member/register_index',$data);
    }
            
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MSignup->getRegister($id);
        
        if(!count($row)){
            redirect('member/confreg','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Register Online';
        $this->load->view('member/register_view',$data);
    }
    public function del(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'delete')){
            redirect('error','refresh');
        }
            
        if(!$this->MMenu->blocked()){
            $this->MSignup->register_online_del();
        }            
        redirect('member/confreg/','refresh');
    }
    
    
}
?>