<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if($this->session->userdata('group_id') < 100){
            redirect('','refresh');
        }
        
        $this->load->model(array('MMenu','MProfile'));
    }
    
    public function index(){
        
        $row = $this->MProfile->getProfile($this->session->userdata('userid'));
        if(!count($row)){
            redirect('error','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'Profile Member';
        $this->load->view('member/profile_index',$data);
    }
    
    public function edit(){
        $this->load->model('MAuth');
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('noktp','No. KTP','required|min_length[7]');
        $this->form_validation->set_rules('alamat','Alamat','required');
        $this->form_validation->set_rules('kodepos','Kode Pos','required|min_length[5]|numeric');
        $this->form_validation->set_rules('city','Kota','required');
        $this->form_validation->set_rules('email','Email','valid_email');
        $this->form_validation->set_rules('tempatlahir','Tempat Lahir','required');
        $this->form_validation->set_rules('jk','','');
        $this->form_validation->set_rules('date','','');
        $this->form_validation->set_rules('hp','No. HP','required|numeric|min_length[10]');
        $this->form_validation->set_rules('telp','','');
        $this->form_validation->set_rules('fax','','');
        $this->form_validation->set_rules('kota_id','','');
        $this->form_validation->set_rules('propinsi','','');
        $this->form_validation->set_rules('ahliwaris','','');
        $this->form_validation->set_rules('bank_id','','');
        $this->form_validation->set_rules('norek','','');
        $this->form_validation->set_rules('cabang','','');
        $this->form_validation->set_rules('area','','');
        
          $this->form_validation->set_rules('pin','PIN','required|callback__check_pin');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MProfile->updateProfile();
                $this->session->set_flashdata('message','Update your profile successfully');
            }
            redirect('member/profile/','refresh');
        }
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-green.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
            
        $row = $this->MProfile->getProfile($this->session->userdata('userid'));
        if(!count($row)){
            redirect('error','refresh');
        }
        $data['row'] = $row;
        $data['bank'] = $this->MProfile->getDropDownBank();
        $data['page_title'] = 'Edit Profile Member';
        
        $this->load->view('member/profile_edit',$data,1);
    }
    
    public function _check_pin(){
        if(!$this->MAuth->check_pin($this->session->userdata('username'),$this->input->post('pin'))){
            $this->form_validation->set_message('_check_pin','Sorry, your pin invalid');
            return false;
        }
        return true;
    }
    
    
}
?>