<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pph21 extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){ 
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MProfiles'));
		$this->load->model(array('MMenu','MProfile'));
    }
	public function edit($member_id){
		if($this->session->userdata('group_id')!=1){
			redirect('error','refresh');
		}
		$this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('member_id','Member ID','required|min_length[8]');
		$this->form_validation->set_rules('nama','Name','required');
		$this->form_validation->set_rules('address','Address','required');
		$this->form_validation->set_rules('npwp','NPWP Number','required|min_length[15]|callback__cek_npwp');
		$this->form_validation->set_rules('npwpdate','NPWP date','required|min_length[10]');
		$this->form_validation->set_rules('kerja','','');
		$this->form_validation->set_rules('menikah','Marital','required');
		$this->form_validation->set_rules('anak','Number of children','required');
		
		if($this->form_validation->run()){
			$rs = $this->MProfiles->cekNPWP($this->input->post('npwp'), $this->input->post('member_id'));
			if($rs==true){
				$this->MProfiles->saveNPWP();
				$this->session->set_flashdata('message','Update Tax Data successfully');
				redirect('member/memprofile','refresh');
			}else{
				$this->session->set_flashdata('message','Update Tax Data failed');
				redirect('member/memprofile','refresh');
			}
		}
        $row =array();
		$data["datapph"] = $this->MProfiles->getNPWP($member_id);
		$data['bank'] = $this->MProfile->getDropDownBank();
		$data['page_title'] = 'Edit Data of Tax';
        $this->load->view('member/vpph21',$data);
	}
	
	public function _cek_npwp(){
		if($this->MProfiles->cekNPWP($this->input->post('npwp'), $this->input->post('member_id'))){
			return true;
		}else{
			$this->form_validation->set_message('_cek_npwp','Invalid NPWP Number');
			return false;
		}
	}
}
?>