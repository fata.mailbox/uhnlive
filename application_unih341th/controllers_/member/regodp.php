<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Regodp extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){ 
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MProfiles'));
		$this->load->model(array('MMenu','MProfile'));
    }
	public function edit($member_id){
		if($this->session->userdata('group_id')!=1){
			redirect('error','refresh');
		}
		$this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('member_id','Member ID','required|min_length[8]|callback__cek_odp');
		
		if($this->form_validation->run()){
			$this->MProfile->saveOdp($member_id);
			$this->session->set_flashdata('message','ODP Register successfully');
			redirect('member/memprofile','refresh');
		}
        $row =array();
		$data["reff"] = $this->MProfiles->getNama($member_id);
		$data["id"] = $member_id;
		$data['page_title'] = 'ODP Register';
        $this->load->view('member/register_odp',$data);
	}
	
	public function _cek_odp(){
		$str = $this->MProfile->cekOdp($this->input->post('member_id'));
		if($str=='None'){
			//$this->form_validation->set_message('_cek_odp','This user has been registered, reffered by '.$str);
			return true;
		}else{
			$this->form_validation->set_message('_cek_odp','This user has been registered, reffered by '.$str);
			return false;
		}
	}
}
?>