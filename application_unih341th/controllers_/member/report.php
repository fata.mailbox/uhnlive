<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu', 'MMember'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
		
        
		$this->form_validation->set_rules('bulan','','');
		$this->form_validation->set_rules('tahun','','');
		$this->form_validation->set_rules('flag','','');
        
        if($this->form_validation->run()){
			if($this->input->post('submit')=='preview'){
				$data['results']=$this->MMember->newMemberAnalysis($this->input->post('bulan'), $this->input->post('tahun'), $this->input->post('flag'));
			//updated by Boby 20110329
			}elseif($this->input->post('submit')=='All'){
				$this->load->helper('to_excel');
				$fname="datamember";
				$qry=$this->MMember->newMemberAnalysisXl($this->input->post('bulan'), $this->input->post('tahun'), $this->input->post('flag'), 0);
				redirect('member/report','refresh');
			// updated by Andrew 20120529
			}elseif($this->input->post('submit')=='Stockiest'){
				$this->load->helper('to_excel');
				$fname="datamember";
				$qry=$this->MMember->newMemberAnalysisX2($this->input->post('bulan'), $this->input->post('tahun'), 2);
				redirect('member/report','refresh');
			}elseif($this->input->post('submit')=='M-Stockiest'){
				$this->load->helper('to_excel');
				$fname="datamember";
				$qry=$this->MMember->newMemberAnalysisX2($this->input->post('bulan'), $this->input->post('tahun'), 3);
				redirect('member/report','refresh');
			// end update by Andrew 20120529
			}else{
				$this->load->helper('to_excel');
				$fname="datamember";
				$qry=$this->MMember->newMemberAnalysisXl($this->input->post('bulan'), $this->input->post('tahun'), $this->input->post('flag'), 1);
				redirect('member/report','refresh');
			//updated by Boby 20110329
			}
        }else{
            $data['results']=false;
        }
        
        $data['page_title'] = 'Member Analysis Report';
        $this->load->view('member/report_analisis',$data);
    }
	
}
?>