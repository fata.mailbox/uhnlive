<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Topten extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        $this->load->model(array('MMenu','MMember'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        
        if($this->form_validation->run()){
			$data['topten'] = $this->MMember->getTop10recuit($this->input->post('fromdate'),$this->input->post('todate'));
			$data['topbuy'] = $this->MMember->getTop10buyers($this->input->post('fromdate'),$this->input->post('todate'));
			//$data['rekap'] = $this->MMember->browseRC($this->input->post('fromdate'),$this->input->post('todate'));
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'Top Ten Member';
        $this->load->view('member/topten_index',$data);
    }    
}
?>