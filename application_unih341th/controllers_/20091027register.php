<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Register extends Controller{
	public function Register(){
		parent::Controller();
		
		$this->load->model('Nested_sets_model');
		$this->load->model('Networks_model','cats');
        }
	public function index(){
		$this->activation();
	}
	
	public function activation(){
                $this->load->model(array('MMenu','captcha_model','MSignup'));
                $this->load->library(array('form_validation','messages'));
		
		$this->form_validation->set_rules('introducerid','Introducer ID','trim|required|callback__introducerid_check');
		$this->form_validation->set_rules('userid','Member ID','trim|required|callback__check_userid');
		$this->form_validation->set_rules('activation','Activation Code','trim|required');
		$this->form_validation->set_rules('hp','No. HP','trim|required|numeric|min_length[10]');
		$this->form_validation->set_rules('email','Email','valid_email');
		$this->form_validation->set_rules('password','Password','trim|required|min_length[8]|matches[password2]');
		$this->form_validation->set_rules('password2','','');
		$this->form_validation->set_rules('pin','PIN','trim|required|numeric|min_length[6]|matches[pin2]');
		$this->form_validation->set_rules('pin2','','');
		$this->form_validation->set_rules('name','Name Member','required|min_length[3]');
		$this->form_validation->set_rules('question','Your Question','trim|required');
		$this->form_validation->set_rules('answer','Your Answer','trim|required');
		$this->form_validation->set_rules('confirmCaptcha','Confirm Captcha','required|callback_check_captcha');
                $this->form_validation->set_rules('placementid','Sponsor ID','trim|required|callback__check_placementid');
		
		if($this->form_validation->run()){
                        if(!$this->MMenu->blocked()){
                                $introducerid = strtoupper($this->input->post('introducerid'));                                
                                $placementid = strtoupper($this->input->post('placementid'));
                                $memberid = strtoupper($this->input->post('userid'));
				$passwd = $this->db->escape_str($this->input->post('password'));
				$pin = $this->db->escape_str($this->input->post('pin'));
				$name = $this->db->escape_str($this->input->post('name'));
				$email = $this->input->post('email');
				
                                $signup = $this->MSignup->signup(
                                        $introducerid,
                                        $memberid,
                                        $this->input->post('activation'),
                                        $placementid,
                                        $passwd,
                                        $pin,
                                        $name,
					$this->input->post('hp'),
					$email,
					$this->db->escape_str($this->input->post('question')),
                                        $this->db->escape_str($this->input->post('answer'))
                                );
                                switch($signup)
                                {
                                        case 'REGISTRATION_SUCCESS':
                                                //$category_name = trim($memberid);
						$row = $this->MSignup->getParentID($placementid);
						$parentid = $row['id'];
						$parentNode = $this->cats->getNodeFromId($parentid);
						
						$fields_array = array("member_id"=>$memberid);
						$this->cats->appendNewChild($parentNode,$fields_array);
						
						if($email){
							$this->send_mail(); 
						}
						$this->session->set_flashdata('message','Your signup is successfully, please update your profile');
                                                redirect('main/','refresh');
                                                break;
                                        default: // Failed
                                                redirect('');
                                                break;
                                }
                        }else{
                                redirect('register/activation/','refresh');
                        }
                }
                else{
                        if(empty($_POST))
                        {
                                $captcha = $this->captcha_model->generateCaptcha();
                                $this->session->set_userdata(array('captchaWord'=> $captcha['word']));
                                
                                $data['captcha'] = $captcha;
				
                                $data['content'] = 'index/activation_member';
                                $data['title'] = "Activation Member";
                                $this->load->view('index/index', $data, 1);
                        }
                        else{
                                $captcha = $this->captcha_model->generateCaptcha();
        
                                $this->session->set_userdata(array('captchaWord'=> $captcha['word']));
                                
                                $data['captcha'] = $captcha;
                                $this->form_validation->set_message('check_captcha','The confirm security code is not available');
                                
                                $data['content'] = 'index/activation_member';
                                $data['title'] = "Activation Member";
                                $this->load->view('index/index', $data, 1);
                        }
                }
	}
	public function check_captcha($confirmCaptcha)
	{
		if ($this->MSignup->check_captcha($confirmCaptcha) == 0)
		{
			$this->form_validation->set_message('check_captcha','The confirm security code is not available');
			return false;
		}
		return true;
	}
	
	public function _introducerid_check()
	{
		$introducerid = $this->input->post('introducerid');
		if ($this->MSignup->check_introducerid($introducerid)){
			$this->form_validation->set_message('_introducerid_check', 'The introducer id <b>'.$introducerid.'</b> is not available.');
			return false;
		}
		return true;
	}
        
	public function _check_placementid()
	{
                $placementid = strtoupper($this->input->post('placementid'));
                if ($this->MSignup->check_placementid($placementid)){
                        $this->form_validation->set_message('_check_placementid', 'The placement id <b>'.$placementid.'</b> is not available.');
                        return false;
                }else{
                        $introducerid = strtoupper($this->input->post('introducerid'));
			
                        if ($placementid != $introducerid && !validation_errors()){
                                $param = $this->MSignup->check_crossline($placementid,$introducerid);
                                if ($param == 'error'){
                                        $this->form_validation->set_message('_check_placementid', 'The placement id <b>'.$placementid.'</b> crossline !');
                                        return false;
                                }
                        }
                        return true;
                }
	}
        public function _check_userid()
	{
		if ($this->MSignup->_check_userid($this->input->post('userid'),$this->input->post('activation'))){
			$this->form_validation->set_message('_check_userid', 'The user id  or activation code is not available.');
			return false;
		}
		return true;
	}
	
	protected function send_mail(){ 
		$data['memberid'] = strtoupper($this->input->post('userid'));
		$data['passwd'] = $this->db->escape_str($this->input->post('password'));
		$data['pin'] = $this->db->escape_str($this->input->post('pin'));
		$data['name'] = $this->db->escape_str($this->input->post('name'));
		$data['email'] = $this->input->post('email');	
				
	    $this->load->library('Email');
	    
	    $config['mailtype'] = 'html';
	    $config['wordwrap'] = TRUE;     
	    $this->email->initialize($config);     
	    
	    $this->email->from('info@uni-health.com', 'MLM Online Register');
	    $this->email->to($data['email']);
	    //$this->email->bcc('info@smartindo-technology.com');
	    $this->email->subject('Registration Online MLM UNIHEALTH');
		$body = $this->load->view('mail/registration', $data, true);
		$this->email->message($body);
		$this->email->send();
	   
		//echo $this->email->print_debugger();
	}
	
	public function member(){
		$this->load->model(array('MMenu','captcha_model','MSignup'));
                $this->load->library(array('form_validation','messages'));
		
		$this->form_validation->set_rules('name','Name Member','required|min_length[3]');
		$this->form_validation->set_rules('address','Alamat','required');
		$this->form_validation->set_rules('email','Email','required|valid_email');
		$this->form_validation->set_rules('zip','Kode Pos','required|numeric|min_length[5]');
		$this->form_validation->set_rules('infofrom','','');
		$this->form_validation->set_rules('delivery','','');
		$this->form_validation->set_rules('hp','No. HP','trim|required|numeric|min_length[9]');
		$this->form_validation->set_rules('confirmCaptcha','Confirm Captcha','required|callback_check_captcha');
		
		if($this->form_validation->run()){
                        if(!$this->MMenu->blocked()){
                                $this->MSignup->add_register();
				//$this->send_mail_register();
				$this->session->set_flashdata('message','Thank you, register successfully...');
			}
			redirect('register/member/','refresh');
		}else{
                        if(empty($_POST))
                        {
                                $captcha = $this->captcha_model->generateCaptcha();
                                $this->session->set_userdata(array('captchaWord'=> $captcha['word']));
                                
                                $data['captcha'] = $captcha;
				
                                $data['content'] = 'index/register_member';
                                $data['title'] = "Register Member";
                                $this->load->view('index/index', $data, 1);
                        }
                        else{
                                $captcha = $this->captcha_model->generateCaptcha();
        
                                $this->session->set_userdata(array('captchaWord'=> $captcha['word']));
                                
                                $data['captcha'] = $captcha;
                                $this->form_validation->set_message('check_captcha','The confirm security code is not available');
                                
                                $data['content'] = 'index/register_member';
                                $data['title'] = "Register Member";
                                $this->load->view('index/index', $data, 1);
                        }
                }
	}
	
	protected function send_mail_register(){ 
		$data['name'] = $this->db->escape_str($this->input->post('name'));
		$data['email'] = $this->input->post('email');	
				
		$this->load->library('Email');
	    
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;     
		$this->email->initialize($config);     
	    
		$this->email->from('info@uni-health.com', 'MLM Online Register');
		$this->email->to($data['email']);
		
		//$this->email->bcc('info@smartindo-technology.com');
		$this->email->subject('Registration Online MLM UNIHEALTH');
		$body = $this->load->view('mail/register_member', $data, true);
		$this->email->message($body);
		$this->email->send();
	   
		//echo $this->email->print_debugger();
	}

}?>