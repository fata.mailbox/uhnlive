<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Updatepv extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MUpdatepv'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'order/updatepv/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MUpdatepv->count_update_pv($keywords);
        //echo $config['total_rows'];
        $this->pagination->initialize($config);
        $data['results'] = $this->MUpdatepv->search_update_pv($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Update PV';
        $this->load->view('order/updatepv_index',$data);
    }
    
    public function create($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('so_id','','');
        
        $data['page_title'] = 'Create Update PV';
        $this->load->view('order/updatepv_form',$data);
    }
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MUpdatepv->get_update_pv($id);
        
        if(!count($row)){
            redirect('order/updatepv','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MUpdatepv->get_update_pv_detail($id);
        $data['page_title'] = 'View Update PV';
        $this->load->view('order/updatepv_view',$data);
    }
    public function go(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MUpdatepv'));
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('so_id','','');
        $row = $this->MUpdatepv->get_so($this->input->post('so_id'));
        
        if(!count($row)){
            redirect('order/updatepv/create','refresh');
        }
        if($this->form_validation->run() && $_POST['submit'] == 'Submit'){
            if(!$this->MMenu->blocked()){
                $this->MUpdatepv->editUpdatePV();
                $this->session->set_flashdata('message','Update PV successfully');
            }
            redirect('order/updatepv','refresh');  
        }
        $data['row'] = $row;
        $data['items'] = $this->MUpdatepv->get_so_detail($this->input->post('so_id'));
        $data['page_title'] = 'View Update PV';
        $this->load->view('order/updatepv_form',$data);
    }
    
}
?>