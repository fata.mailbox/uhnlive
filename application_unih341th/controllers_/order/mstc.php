<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mstc extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MMobilestc'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        
        $config['base_url'] = site_url().'order/mstc/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MMobilestc->countMSTC($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MMobilestc->searchMSTC($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Mobile Stockiest';
        $this->load->view('order/mobilestc_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));

        $this->form_validation->set_rules('no_stc','Stockiest ID','required|min_length[2]|callback__check_nostc');
        $this->form_validation->set_rules('member_id','Member ID','required|callback__check_memberid');
        $this->form_validation->set_rules('stcid','Stockiest ID','required');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('total','Total repeat order','required|callback__check_min');
        $this->form_validation->set_rules('totalpv','','');
        $this->form_validation->set_rules('pin','PIN','required|callback__check_pin');
        $this->form_validation->set_rules('newpasswd','Password New Stc','required|min_length[8]|matches[newpasswd2]');
        $this->form_validation->set_rules('newpasswd2','Retype Password','');
        $this->form_validation->set_rules('newpin','PIN New Stc','required|numeric|min_length[6]|matches[newpin2]');
        $this->form_validation->set_rules('newpin2','Retype PIN','');
        
        $this->form_validation->set_rules('itemcode0','','');
        $this->form_validation->set_rules('itemname0','','');
        $this->form_validation->set_rules('qty0','','');
        $this->form_validation->set_rules('price0','','');
        $this->form_validation->set_rules('subtotal0','','');
        $this->form_validation->set_rules('pv0','','');
        $this->form_validation->set_rules('subtotalpv0','','');
        $this->form_validation->set_rules('titipan_id0','','');
        
        $this->form_validation->set_rules('itemcode1','','');
        $this->form_validation->set_rules('itemname1','','');
        $this->form_validation->set_rules('qty1','','');
        $this->form_validation->set_rules('price1','','');
        $this->form_validation->set_rules('subtotal1','','');
        $this->form_validation->set_rules('pv1','','');
        $this->form_validation->set_rules('subtotalpv1','','');
        $this->form_validation->set_rules('titipan_id1','','');
        
        $this->form_validation->set_rules('itemcode2','','');
        $this->form_validation->set_rules('itemname2','','');
        $this->form_validation->set_rules('qty2','','');
        $this->form_validation->set_rules('price2','','');
        $this->form_validation->set_rules('subtotal2','','');
        $this->form_validation->set_rules('pv2','','');
        $this->form_validation->set_rules('subtotalpv2','','');
        $this->form_validation->set_rules('titipan_id2','','');
        
        $this->form_validation->set_rules('itemcode3','','');
        $this->form_validation->set_rules('itemname3','','');
        $this->form_validation->set_rules('qty3','','');
        $this->form_validation->set_rules('price3','','');
        $this->form_validation->set_rules('subtotal3','','');
        $this->form_validation->set_rules('pv3','','');
        $this->form_validation->set_rules('subtotalpv3','','');
        $this->form_validation->set_rules('titipan_id3','','');
        
        $this->form_validation->set_rules('itemcode4','','');
        $this->form_validation->set_rules('itemname4','','');
        $this->form_validation->set_rules('qty4','','');
        $this->form_validation->set_rules('price4','','');
        $this->form_validation->set_rules('subtotal4','','');
        $this->form_validation->set_rules('pv4','','');
        $this->form_validation->set_rules('subtotalpv4','','');
        $this->form_validation->set_rules('titipan_id4','','');
        
        $this->form_validation->set_rules('itemcode5','','');
        $this->form_validation->set_rules('itemname5','','');
        $this->form_validation->set_rules('qty5','','');
        $this->form_validation->set_rules('price5','','');
        $this->form_validation->set_rules('subtotal5','','');
        $this->form_validation->set_rules('pv5','','');
        $this->form_validation->set_rules('subtotalpv5','','');
        $this->form_validation->set_rules('titipan_id5','','');
        
        $this->form_validation->set_rules('itemcode6','','');
        $this->form_validation->set_rules('itemname6','','');
        $this->form_validation->set_rules('qty6','','');
        $this->form_validation->set_rules('price6','','');
        $this->form_validation->set_rules('subtotal6','','');
        $this->form_validation->set_rules('pv6','','');
        $this->form_validation->set_rules('subtotalpv6','','');
        $this->form_validation->set_rules('titipan_id6','','');
        
        $this->form_validation->set_rules('itemcode7','','');
        $this->form_validation->set_rules('itemname7','','');
        $this->form_validation->set_rules('qty7','','');
        $this->form_validation->set_rules('price7','','');
        $this->form_validation->set_rules('subtotal7','','');
        $this->form_validation->set_rules('pv7','','');
        $this->form_validation->set_rules('subtotalpv7','','');
        $this->form_validation->set_rules('titipan_id7','','');
        
        $this->form_validation->set_rules('itemcode8','','');
        $this->form_validation->set_rules('itemname8','','');
        $this->form_validation->set_rules('qty8','','');
        $this->form_validation->set_rules('price8','','');
        $this->form_validation->set_rules('subtotal8','','');
        $this->form_validation->set_rules('pv8','','');
        $this->form_validation->set_rules('subtotalpv8','','');
        $this->form_validation->set_rules('titipan_id8','','');
        
        $this->form_validation->set_rules('itemcode9','','');
        $this->form_validation->set_rules('itemname9','','');
        $this->form_validation->set_rules('qty9','','');
        $this->form_validation->set_rules('price9','','');
        $this->form_validation->set_rules('subtotal9','','');
        $this->form_validation->set_rules('pv9','','');
        $this->form_validation->set_rules('subtotalpv9','','');
        $this->form_validation->set_rules('titipan_id9','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                
                $rqtid = $this->MMobilestc->addRequestOrder();
                $results = $this->MMobilestc->check_titipan($rqtid);
                if($results == 'ok'){
                    $this->MMobilestc->addMobileStc($rqtid);
                    $this->session->set_flashdata('message','Create M-STC successfully');
                    redirect('order/mstc','refresh');        
                }else{
                    $this->session->set_flashdata('message','Procces Cancel, Stock tidak mencukupi');
                    redirect('order/mstc/create/','refresh');
                }
            }
            redirect('order/mstc','refresh');
        }
        
        $str = $this->session->userdata('username');
        $data['no_stc'] = explode('-', $str, 2);
		// updated by Boby 20100614
		$data['kota'] = $this->MMobilestc->getKota();
        // end updated by Boby 20100614
		
        $data['page_title'] = 'Create Mobile Stockiest';
        $this->load->view('order/mobilestc_form',$data);
    }
    
    public function _check_min(){
        $amount = str_replace(".","",$this->input->post('total'));
        if($amount <= 0){
            $this->form_validation->set_message('_check_min','browse product...');
            return false;
        }else{
            if($amount < 5000000){
                $this->form_validation->set_message('_check_min','Minimum pembelajaan M-Stockiest Rp. 5.000.000,-');
                return false;
            }
        }
        return true;
    }
    public function _check_pin(){
        if(!$this->MAuth->check_pin($this->session->userdata('username'),$this->input->post('pin'))){
            $this->form_validation->set_message('_check_pin','Sorry, your pin invalid');
            return false;
        }
        return true;
    }
    public function _check_memberid(){
        if($this->MMobilestc->getStockiest('id',$this->input->post('member_id'))){
            $this->form_validation->set_message('_check_memberid','Member tersebut sudah terdaftar menjadi Stockiest');
            return false;
        }
        return true;
    }
    public function _check_nostc(){
        if($this->MMobilestc->getStockiest('no_stc',$this->input->post('no_stc1').$this->input->post('no_stc'))){
            $this->form_validation->set_message('_check_nostc','Stockiest ID tersebut sudah terdaftar');
            return false;
        }
        return true;
    }
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MMobilestc->getStockiestDetail($id);
        
        if(!count($row)){
            redirect('order/mstc','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Mobile Stc Stockiest';
        $this->load->view('order/mobilestc_view',$data);
    }
    
    
}
?>