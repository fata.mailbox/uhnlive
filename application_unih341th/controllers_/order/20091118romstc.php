<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Romstc extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MRo'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'order/romstc/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MRo->countROMSTC($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MRo->searchROMSTC($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['results2'] = $this->MRo->searchROTemp();
        
        $data['page_title'] = 'Request Order M-Stc Approval';
        $this->load->view('order/requestordermstc_index',$data);
    }
            
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MRo->getRequestOrderMSTC($id);
        
        if(!count($row)){
            redirect('order/romstc','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MRo->getRequestOrderDetail($id);
        $data['page_title'] = 'View Request Order Stockiest';
        $this->load->view('order/requestorder_view',$data);
    }
    public function view2($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MRo->getRequestOrderTempMSTC($id);
        
        if(!count($row)){
            //redirect('order/romstc','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MRo->getRequestOrderDetailTemp($id);
        $data['page_title'] = 'View Request Order Stockiest';
        $this->load->view('order/requestorder_mstc_approved_view',$data);
    }
    public function del(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'delete')){
            redirect('error','refresh');
        }
            
        if(!$this->MMenu->blocked()){
            $this->MRo->requestOrderTempDel();
        }            
        redirect('order/romstc/','refresh');
    }
    public function approved(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        if(!$this->MMenu->blocked()){
            $results = $this->MRo->check_titipan($this->input->post('id'),$this->input->post('stockiest_id'));
            if($results == 'ok'){
                $this->MRo->addROApproved($this->input->post('id'));
                $this->session->set_flashdata('message','Approved Request Order M-STC successfully');
            }else{
                $this->session->set_flashdata('message','Procces Cancel, Stock tidak mencukupi');
            }
        }
        redirect('order/romstc/','refresh');
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));

        $this->form_validation->set_rules('member_id','Member ID','required');
        $this->form_validation->set_rules('no_stc','Stockiest ID','required');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('total','Total repeat order','required|callback__check_min');
        $this->form_validation->set_rules('totalpv','','');
        if($this->session->userdata('group_id')>100)$this->form_validation->set_rules('pin','PIN','required|callback__check_pin');
        
        $this->form_validation->set_rules('itemcode0','','');
        $this->form_validation->set_rules('itemname0','','');
        $this->form_validation->set_rules('qty0','','');
        $this->form_validation->set_rules('price0','','');
        $this->form_validation->set_rules('subtotal0','','');
        $this->form_validation->set_rules('pv0','','');
        $this->form_validation->set_rules('subtotalpv0','','');
        $this->form_validation->set_rules('titipan_id0','','');
        
        $this->form_validation->set_rules('itemcode1','','');
        $this->form_validation->set_rules('itemname1','','');
        $this->form_validation->set_rules('qty1','','');
        $this->form_validation->set_rules('price1','','');
        $this->form_validation->set_rules('subtotal1','','');
        $this->form_validation->set_rules('pv1','','');
        $this->form_validation->set_rules('subtotalpv1','','');
        $this->form_validation->set_rules('titipan_id1','','');
        
        $this->form_validation->set_rules('itemcode2','','');
        $this->form_validation->set_rules('itemname2','','');
        $this->form_validation->set_rules('qty2','','');
        $this->form_validation->set_rules('price2','','');
        $this->form_validation->set_rules('subtotal2','','');
        $this->form_validation->set_rules('pv2','','');
        $this->form_validation->set_rules('subtotalpv2','','');
        $this->form_validation->set_rules('titipan_id2','','');
        
        $this->form_validation->set_rules('itemcode3','','');
        $this->form_validation->set_rules('itemname3','','');
        $this->form_validation->set_rules('qty3','','');
        $this->form_validation->set_rules('price3','','');
        $this->form_validation->set_rules('subtotal3','','');
        $this->form_validation->set_rules('pv3','','');
        $this->form_validation->set_rules('subtotalpv3','','');
        $this->form_validation->set_rules('titipan_id3','','');
        
        $this->form_validation->set_rules('itemcode4','','');
        $this->form_validation->set_rules('itemname4','','');
        $this->form_validation->set_rules('qty4','','');
        $this->form_validation->set_rules('price4','','');
        $this->form_validation->set_rules('subtotal4','','');
        $this->form_validation->set_rules('pv4','','');
        $this->form_validation->set_rules('subtotalpv4','','');
        $this->form_validation->set_rules('titipan_id4','','');
        
        $this->form_validation->set_rules('itemcode5','','');
        $this->form_validation->set_rules('itemname5','','');
        $this->form_validation->set_rules('qty5','','');
        $this->form_validation->set_rules('price5','','');
        $this->form_validation->set_rules('subtotal5','','');
        $this->form_validation->set_rules('pv5','','');
        $this->form_validation->set_rules('subtotalpv5','','');
        $this->form_validation->set_rules('titipan_id5','','');
        
        $this->form_validation->set_rules('itemcode6','','');
        $this->form_validation->set_rules('itemname6','','');
        $this->form_validation->set_rules('qty6','','');
        $this->form_validation->set_rules('price6','','');
        $this->form_validation->set_rules('subtotal6','','');
        $this->form_validation->set_rules('pv6','','');
        $this->form_validation->set_rules('subtotalpv6','','');
        $this->form_validation->set_rules('titipan_id6','','');
        
        $this->form_validation->set_rules('itemcode7','','');
        $this->form_validation->set_rules('itemname7','','');
        $this->form_validation->set_rules('qty7','','');
        $this->form_validation->set_rules('price7','','');
        $this->form_validation->set_rules('subtotal7','','');
        $this->form_validation->set_rules('pv7','','');
        $this->form_validation->set_rules('subtotalpv7','','');
        $this->form_validation->set_rules('titipan_id7','','');
        
        $this->form_validation->set_rules('itemcode8','','');
        $this->form_validation->set_rules('itemname8','','');
        $this->form_validation->set_rules('qty8','','');
        $this->form_validation->set_rules('price8','','');
        $this->form_validation->set_rules('subtotal8','','');
        $this->form_validation->set_rules('pv8','','');
        $this->form_validation->set_rules('subtotalpv8','','');
        $this->form_validation->set_rules('titipan_id8','','');
        
        $this->form_validation->set_rules('itemcode9','','');
        $this->form_validation->set_rules('itemname9','','');
        $this->form_validation->set_rules('qty9','','');
        $this->form_validation->set_rules('price9','','');
        $this->form_validation->set_rules('subtotal9','','');
        $this->form_validation->set_rules('pv9','','');
        $this->form_validation->set_rules('subtotalpv9','','');
        $this->form_validation->set_rules('titipan_id9','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $rqtid = $this->MRo->addRequestOrderMSTC();
                
                if($this->input->post('stcid') == '0')$results="ok";
                else $results = $this->MRo->check_titipan($rqtid);
                
                if($results == 'ok'){
                    $this->MRo->addROMSTC($rqtid);
                    $this->session->set_flashdata('message','Create RO M-STC successfully');
                    redirect('order/romstc','refresh');        
                }else{
                    $this->session->set_flashdata('message','Procces Cancel, Stock tidak mencukupi');
                    redirect('order/romstc/create/','refresh');
                }
            }
            redirect('order/romstc','refresh');
        }
        
        $data['page_title'] = 'Create RO M-Stockiest';
        $this->load->view('order/requestorder_mstc_form',$data);
    }
    
    public function _check_min(){
        $amount = str_replace(".","",$this->input->post('total'));
        if($amount <= 0){
            $this->form_validation->set_message('_check_min','browse product...');
            return false;
        }
        return true;
    }
    public function _check_pin(){
        if(!$this->MAuth->check_pin($this->session->userdata('username'),$this->input->post('pin'))){
            $this->form_validation->set_message('_check_pin','Sorry, your pin invalid');
            return false;
        }
        return true;
    }
    
    
}
?>