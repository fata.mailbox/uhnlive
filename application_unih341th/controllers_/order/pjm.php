<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pjm extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MPinjaman'));
	  $this->load->model(array('MMenu','MRo'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'order/pjm/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MPinjaman->countPinjaman($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MPinjaman->searchPinjaman($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Stockiest Consignment';
        $this->load->view('order/pinjamanstockiest_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('member_id','Stockiest','required');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('remark','','');
        $this->form_validation->set_rules('total','Total pinjaman','required|callback__check_min');
        $this->form_validation->set_rules('totalpv','','');
        $this->form_validation->set_rules('whsid','','');
        
        $this->form_validation->set_rules('itemcode0','','');
        $this->form_validation->set_rules('itemname0','','');
        $this->form_validation->set_rules('qty0','','');
        $this->form_validation->set_rules('price0','','');
        $this->form_validation->set_rules('subtotal0','','');
        $this->form_validation->set_rules('pv0','','');
        $this->form_validation->set_rules('subtotalpv0','','');
        
        $this->form_validation->set_rules('itemcode1','','');
        $this->form_validation->set_rules('itemname1','','');
        $this->form_validation->set_rules('qty1','','');
        $this->form_validation->set_rules('price1','','');
        $this->form_validation->set_rules('subtotal1','','');
        $this->form_validation->set_rules('pv1','','');
        $this->form_validation->set_rules('subtotalpv1','','');
        
        $this->form_validation->set_rules('itemcode2','','');
        $this->form_validation->set_rules('itemname2','','');
        $this->form_validation->set_rules('qty2','','');
        $this->form_validation->set_rules('price2','','');
        $this->form_validation->set_rules('subtotal2','','');
        $this->form_validation->set_rules('pv2','','');
        $this->form_validation->set_rules('subtotalpv2','','');
        
        $this->form_validation->set_rules('itemcode3','','');
        $this->form_validation->set_rules('itemname3','','');
        $this->form_validation->set_rules('qty3','','');
        $this->form_validation->set_rules('price3','','');
        $this->form_validation->set_rules('subtotal3','','');
        $this->form_validation->set_rules('pv3','','');
        $this->form_validation->set_rules('subtotalpv3','','');
        
        $this->form_validation->set_rules('itemcode4','','');
        $this->form_validation->set_rules('itemname4','','');
        $this->form_validation->set_rules('qty4','','');
        $this->form_validation->set_rules('price4','','');
        $this->form_validation->set_rules('subtotal4','','');
        $this->form_validation->set_rules('pv4','','');
        $this->form_validation->set_rules('subtotalpv4','','');
        
        $this->form_validation->set_rules('itemcode5','','');
        $this->form_validation->set_rules('itemname5','','');
        $this->form_validation->set_rules('qty5','','');
        $this->form_validation->set_rules('price5','','');
        $this->form_validation->set_rules('subtotal5','','');
        $this->form_validation->set_rules('pv5','','');
        $this->form_validation->set_rules('subtotalpv5','','');
        
        $this->form_validation->set_rules('itemcode6','','');
        $this->form_validation->set_rules('itemname6','','');
        $this->form_validation->set_rules('qty6','','');
        $this->form_validation->set_rules('price6','','');
        $this->form_validation->set_rules('subtotal6','','');
        $this->form_validation->set_rules('pv6','','');
        $this->form_validation->set_rules('subtotalpv6','','');
        
        $this->form_validation->set_rules('itemcode7','','');
        $this->form_validation->set_rules('itemname7','','');
        $this->form_validation->set_rules('qty7','','');
        $this->form_validation->set_rules('price7','','');
        $this->form_validation->set_rules('subtotal7','','');
        $this->form_validation->set_rules('pv7','','');
        $this->form_validation->set_rules('subtotalpv7','','');
        
        $this->form_validation->set_rules('itemcode8','','');
        $this->form_validation->set_rules('itemname8','','');
        $this->form_validation->set_rules('qty8','','');
        $this->form_validation->set_rules('price8','','');
        $this->form_validation->set_rules('subtotal8','','');
        $this->form_validation->set_rules('pv8','','');
        $this->form_validation->set_rules('subtotalpv8','','');
        
        $this->form_validation->set_rules('itemcode9','','');
        $this->form_validation->set_rules('itemname9','','');
        $this->form_validation->set_rules('qty9','','');
        $this->form_validation->set_rules('price9','','');
        $this->form_validation->set_rules('subtotal9','','');
        $this->form_validation->set_rules('pv9','','');
        $this->form_validation->set_rules('subtotalpv9','','');
        
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MPinjaman->addPinjaman();
                $this->session->set_flashdata('message','Stockiest Consignment Successfully');
            }
            redirect('order/pjm','refresh');
        }
        $data['warehouse']=$this->MPinjaman->getDropDownWhs('');
        $data['page_title'] = 'Create Request Pinjaman';
        $this->load->view('order/pinjamanstockiest_form',$data);
    }
    
    public function _check_min(){
        $amount = str_replace(".","",$this->input->post('total'));
        if($amount <= 0){
            $this->form_validation->set_message('_check_min','browse product...');
            return false;
        }
	  	//Updated by Boby (2009-11-18)
				else{
					$flag = 0;
					if($this->input->post('itemcode0')){$item[$flag]=$this->input->post('itemcode0'); $qty[$flag]=str_replace(".","",$this->input->post('qty0')); $itemname[$flag]=$this->input->post('itemname0');	$prc[$flag]=str_replace(".","",$this->input->post('price0'));	$flag+=1;}
					if($this->input->post('itemcode1')){$item[$flag]=$this->input->post('itemcode1'); $qty[$flag]=str_replace(".","",$this->input->post('qty1')); $itemname[$flag]=$this->input->post('itemname1');	$prc[$flag]=str_replace(".","",$this->input->post('price1'));	$flag+=1;}
					if($this->input->post('itemcode2')){$item[$flag]=$this->input->post('itemcode2'); $qty[$flag]=str_replace(".","",$this->input->post('qty2')); $itemname[$flag]=$this->input->post('itemname2');	$prc[$flag]=str_replace(".","",$this->input->post('price2'));	$flag+=1;}
					if($this->input->post('itemcode3')){$item[$flag]=$this->input->post('itemcode3'); $qty[$flag]=str_replace(".","",$this->input->post('qty3')); $itemname[$flag]=$this->input->post('itemname3');	$prc[$flag]=str_replace(".","",$this->input->post('price3'));	$flag+=1;}
					if($this->input->post('itemcode4')){$item[$flag]=$this->input->post('itemcode4'); $qty[$flag]=str_replace(".","",$this->input->post('qty4')); $itemname[$flag]=$this->input->post('itemname4');	$prc[$flag]=str_replace(".","",$this->input->post('price4'));	$flag+=1;}

					if($this->input->post('itemcode5')){$item[$flag]=$this->input->post('itemcode5'); $qty[$flag]=str_replace(".","",$this->input->post('qty5')); $itemname[$flag]=$this->input->post('itemname5');	$prc[$flag]=str_replace(".","",$this->input->post('price5'));	$flag+=1;}
					if($this->input->post('itemcode6')){$item[$flag]=$this->input->post('itemcode6'); $qty[$flag]=str_replace(".","",$this->input->post('qty6')); $itemname[$flag]=$this->input->post('itemname6');	$prc[$flag]=str_replace(".","",$this->input->post('price6'));	$flag+=1;}
					if($this->input->post('itemcode7')){$item[$flag]=$this->input->post('itemcode7'); $qty[$flag]=str_replace(".","",$this->input->post('qty7')); $itemname[$flag]=$this->input->post('itemname7');	$prc[$flag]=str_replace(".","",$this->input->post('price7'));	$flag+=1;}
					if($this->input->post('itemcode8')){$item[$flag]=$this->input->post('itemcode8'); $qty[$flag]=str_replace(".","",$this->input->post('qty8')); $itemname[$flag]=$this->input->post('itemname8');	$prc[$flag]=str_replace(".","",$this->input->post('price8'));	$flag+=1;}
					if($this->input->post('itemcode9')){$item[$flag]=$this->input->post('itemcode9'); $qty[$flag]=str_replace(".","",$this->input->post('qty9')); $itemname[$flag]=$this->input->post('itemname9');	$prc[$flag]=str_replace(".","",$this->input->post('price9'));	$flag+=1;}

					$temp = 0;$twin=0;
					$whsid = $this->session->userdata('whsid');
					
					for($i=0;$i<=$flag;$i++){
						if($temp>0){
							for($j=0;$j<$temp;$j++){
								if($brg[$j]==$item[$i]){//jika sama
									$twin = 1;
									$jml[$j]+=$qty[$i];
									//echo $qty[$i]."<br>";
								}
							}
						}			
						if($twin==0){//jika tidak ada yang sama
							$jml[$temp]=$qty[$i];
							$brg[$temp]=$item[$i];
							$price[$temp]=$prc[$i];
							$nama[$temp]=$itemname[$i];
							$temp+=1;
						}else{$twin=0;}
					}
					for($i=0;$i<count($jml)-1;$i++){
						//echo "<br><br>".$brg[$i]." = ".$jml[$i]." ".$nama[$i]." ";
						$readystok=$this->MRo->cekStok($whsid, $brg[$i], $price[$i], $jml[$i], 0);
						if($readystok!='Ready'){$twin=1;$stok="We only have ".$readystok." ".$nama[$i];}
						//echo "Stok = ".$readystok."<br>";
					}
					if($twin > 0){
						$this->form_validation->set_message('_check_min','Out of order ( '.$stok.' )');
						return false;
					}
				}
				//end Updated by Boby (2009-11-18)
        return true;
    }
    
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MPinjaman->getPinjaman($id);
        
        if(!count($row)){
            redirect('order/pjm','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MPinjaman->getPinjamanDetail($id);
        $data['page_title'] = 'View Stockiest Consignment';
        $this->load->view('order/pinjamanstockiest_view',$data);
    }
    
}
?>