<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Itemrosearch extends CI_Controller{
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MSearch'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'itemrosearch/index/'.$this->uri->segment(3);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        //echo $this->uri->segment(1);
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countItemROSearch($keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->itemROSearch($keywords,$config['per_page'],$this->uri->segment(4));
        $data['page_title'] = 'Item Search';
        
        $this->load->view('search/itemro_search',$data);
    }
    
}
?>