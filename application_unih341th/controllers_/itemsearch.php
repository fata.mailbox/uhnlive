<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Itemsearch extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MSearch'));
    }
    
    public function ass(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'itemsearch/ass/'.$this->uri->segment(3);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countItemSearch($keywords,'ass');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->itemSearch($keywords,'ass',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Item Search';
        
        $this->load->view('search/item_search',$data);
    }
    
    public function all(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'itemsearch/all/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countItemSearch($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->itemSearch($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Item Search';
        
        $this->load->view('search/item_search',$data);
    }
    
    public function ttp(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'itemsearch/ttp/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countItemSearchTitipan($keywords,'ttp');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->itemSearchTitipan($keywords,'ttp',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Item Search';
        
        $this->load->view('search/itemtitipan_search',$data);
    }
    
}
?>