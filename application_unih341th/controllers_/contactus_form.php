<!--content-->
            <div class="content" style="width:572px;">
            	<!--about-->
                <div class="about" style="width:560px; padding:5px; border-color:#dfe8ec; background:none;">
                	<div class="aboutCont">
                    	<h2 style="color:#15384f;">Inquiry Form</h2><hr />
                        <?php if ($this->session->flashdata('message')){
					echo "<div class='message'><b>".$this->session->flashdata('message')."</b>";}?>
                    
          <?php if(!$isi){ ?>                      
                        <p>Silakan mengisi formulir dibawah ini untuk mengetahui informasi lebih lengkap tentang kami.</p>
                        <?php echo form_open('contactus/cform', array('id' => 'form'));?>
                            <div class="regForm">
                            <fieldset>
                            <legend>Box Form</legend>
                                <ul class="formBox">
                                    <li><label>First Name</label><input type="text" name="firstname" id="firstname" value="<?php echo set_value('firstname');?>" maxlength="50" /> *<br>
                                        <span class="textError"><p><?php echo form_error('firstname');?></p><!----></span>
                                    </li>
                                    <li class="li"><label for="berdiri">Last Name</label><input type="text" name="lastname" id="lastname" value="<?php echo set_value('lastname');?>" maxlength="50" /> *<br>
                                        <span class="textError"><p><?php echo form_error('lastname');?></p><!----></span>
                                    </li>
                                    <li><label>Email</label><input type="text" name="email" id="email" value="<?php echo set_value('email');?>" maxlength="50" /> *<br>
                                        <span class="textError"><p><?php echo form_error('email');?></p><!----></span>
                                    </li>
                                    <li class="li"><label for="merk">Phone</label><input type="text" name="phone" id="phone" value="<?php echo set_value('phone');?>" maxlength="50" /> *<br>
                                        <span class="textError"><p><?php echo form_error('phone');?></p><!----></span>
                                    </li>
                                    <li><label>Subject</label><input type="text" name="subject" id="subject" value="<?php echo set_value('subject');?>" maxlength="50" /> *<br>
                                        <span class="textError"><p><?php echo form_error('subject');?></p><!----></span>
                                    </li>
                                    <li class="li"><label for="telp">Message</label><textarea name="message" id="message" rows="1" cols="5" class="textarea"><?php echo set_value('message');?></textarea> *<br>
                                        <span class="textError"><p><?php echo form_error('message');?></p><!----></span>
                                    </li>
                                    <li><label>Security code</label>
                                        <span id="captchaImage"><?php echo $captcha['image'];?></span>
                                    </li>
                                    <li class="li"><label>Confirm security code</label><?php $data = array('name'=>'confirmCaptcha','id'=>'confirmCaptcha','autocomplete'=>'off','size'=>'8','maxlength'=>'6'); echo form_input($data);?> *<br>
                                        <span class="textError"><p><?php echo form_error('confirmCaptcha');?></p><!----></span>
                                    </li>
                                    <li><label>&nbsp;</label>
                                        <input name="submit" value="Send Message" class="submit" type="submit">
                                    </li>
                                </ul>
                            </fieldset>
                            <div style="margin:5px 0; padding:0px;float:right; color:#f00;"><p>* Required</p></div>
                            </div><br/>
                        <?php echo form_close(); ?>
                    <?php }?>
                    </div>
                  <div class="clearBoth"></div>
                </div><!--end about-->                
                
              <div class="clearBoth"></div>
            </div><!--end content-->