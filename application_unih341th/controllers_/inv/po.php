<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Po extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MPo'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'inv/po/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MPo->countPO($keywords);
        //echo $config['total_rows'];
        $this->pagination->initialize($config);
        $data['results'] = $this->MPo->searchPO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Purchase Order';
        $this->load->view('inv/purchaseorder_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('supplier_id','Supplier','required');
        $this->form_validation->set_rules('remark','','');
        $this->form_validation->set_rules('total','Total order','required|callback__check_min');
        $this->form_validation->set_rules('password1','Password','required|callback__check_password');
                
        $this->form_validation->set_rules('itemcode0','','');
        $this->form_validation->set_rules('itemname0','','');
        $this->form_validation->set_rules('qty0','','');
        $this->form_validation->set_rules('price0','','');
        $this->form_validation->set_rules('subtotal0','','');
        
        $this->form_validation->set_rules('itemcode1','','');
        $this->form_validation->set_rules('itemname1','','');
        $this->form_validation->set_rules('qty1','','');
        $this->form_validation->set_rules('price1','','');
        $this->form_validation->set_rules('subtotal1','','');
        
        $this->form_validation->set_rules('itemcode2','','');
        $this->form_validation->set_rules('itemname2','','');
        $this->form_validation->set_rules('qty2','','');
        $this->form_validation->set_rules('price2','','');
        $this->form_validation->set_rules('subtotal2','','');
        
        $this->form_validation->set_rules('itemcode3','','');
        $this->form_validation->set_rules('itemname3','','');
        $this->form_validation->set_rules('qty3','','');
        $this->form_validation->set_rules('price3','','');
        $this->form_validation->set_rules('subtotal3','','');
                
        $this->form_validation->set_rules('itemcode4','','');
        $this->form_validation->set_rules('itemname4','','');
        $this->form_validation->set_rules('qty4','','');
        $this->form_validation->set_rules('price4','','');
        $this->form_validation->set_rules('subtotal4','','');
                
        $this->form_validation->set_rules('itemcode5','','');
        $this->form_validation->set_rules('itemname5','','');
        $this->form_validation->set_rules('qty5','','');
        $this->form_validation->set_rules('price5','','');
        $this->form_validation->set_rules('subtotal5','','');
                
        $this->form_validation->set_rules('itemcode6','','');
        $this->form_validation->set_rules('itemname6','','');
        $this->form_validation->set_rules('qty6','','');
        $this->form_validation->set_rules('price6','','');
        $this->form_validation->set_rules('subtotal6','','');
        
        $this->form_validation->set_rules('itemcode7','','');
        $this->form_validation->set_rules('itemname7','','');
        $this->form_validation->set_rules('qty7','','');
        $this->form_validation->set_rules('price7','','');
        $this->form_validation->set_rules('subtotal7','','');
                
        $this->form_validation->set_rules('itemcode8','','');
        $this->form_validation->set_rules('itemname8','','');
        $this->form_validation->set_rules('qty8','','');
        $this->form_validation->set_rules('price8','','');
        $this->form_validation->set_rules('subtotal8','','');
                
        $this->form_validation->set_rules('itemcode9','','');
        $this->form_validation->set_rules('itemname9','','');
        $this->form_validation->set_rules('qty9','','');
        $this->form_validation->set_rules('price9','','');
        $this->form_validation->set_rules('subtotal9','','');
                
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MPo->addPO();
                $this->session->set_flashdata('message','PO successfully');
            }
            redirect('inv/po','refresh');
        }
        $data['supplier'] = $this->MPo->getDropDownSupplier();
        $data['page_title'] = 'Create Purchase Order';
        $this->load->view('inv/purchaseorder_form',$data);
    }
    
    public function _check_min(){
        $amount = str_replace(".","",$this->input->post('total'));
        if($amount <= 0){
            $this->form_validation->set_message('_check_min','browse product...');
            return false;
        }else{
            if(strtolower($this->uri->segment(3)) == 'edit'){
                if($this->MPo->getStatus($this->uri->segment(4))){
                    $this->form_validation->set_message('_check_min','Status actual PO closed');
                    return false;
                }
            }
        }
        return true;
    }
    public function _check_password(){
        $this->load->model('MAuth');
        if(!$this->MAuth->check_password($this->session->userdata('user'),$this->input->post('password1'))){
            $this->form_validation->set_message('_check_password','Sorry, your password invalid');
            return false;
        }
        return true;
    }
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MPo->getPO($id);
        
        if(!count($row)){
            redirect('inv/po','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MPo->getPODetail($id);
        $data['results'] = $this->MPo->list_po($id);
        $data['total'] = $this->MPo->sum_po($id);
        $data['page_title'] = 'View Purchase Order';
        $this->load->view('inv/purchaseorder_view',$data);
    }
    
    public function edit($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
        $row = $this->MPo->getPO($id);
        
        if(!count($row)){
            redirect('inv/po','refresh');
        }
        $this->form_validation->set_rules('date','Tanggal Jatuh Tempo','required');
        $this->form_validation->set_rules('total','Total order','required|callback__check_min');
        $this->form_validation->set_rules('event_id','','');
        $this->form_validation->set_rules('remark','','');        
        $this->form_validation->set_rules('password1','Password','required|callback__check_password');
                
        $this->form_validation->set_rules('itemcode0','','');
        $this->form_validation->set_rules('itemname0','','');
        $this->form_validation->set_rules('qty0','','');
        $this->form_validation->set_rules('price0','','');
        $this->form_validation->set_rules('subtotal0','','');
        
        $this->form_validation->set_rules('itemcode1','','');
        $this->form_validation->set_rules('itemname1','','');
        $this->form_validation->set_rules('qty1','','');
        $this->form_validation->set_rules('price1','','');
        $this->form_validation->set_rules('subtotal1','','');
        
        $this->form_validation->set_rules('itemcode2','','');
        $this->form_validation->set_rules('itemname2','','');
        $this->form_validation->set_rules('qty2','','');
        $this->form_validation->set_rules('price2','','');
        $this->form_validation->set_rules('subtotal2','','');
        
        $this->form_validation->set_rules('itemcode3','','');
        $this->form_validation->set_rules('itemname3','','');
        $this->form_validation->set_rules('qty3','','');
        $this->form_validation->set_rules('price3','','');
        $this->form_validation->set_rules('subtotal3','','');
                
        $this->form_validation->set_rules('itemcode4','','');
        $this->form_validation->set_rules('itemname4','','');
        $this->form_validation->set_rules('qty4','','');
        $this->form_validation->set_rules('price4','','');
        $this->form_validation->set_rules('subtotal4','','');
                
        $this->form_validation->set_rules('itemcode5','','');
        $this->form_validation->set_rules('itemname5','','');
        $this->form_validation->set_rules('qty5','','');
        $this->form_validation->set_rules('price5','','');
        $this->form_validation->set_rules('subtotal5','','');
                
        $this->form_validation->set_rules('itemcode6','','');
        $this->form_validation->set_rules('itemname6','','');
        $this->form_validation->set_rules('qty6','','');
        $this->form_validation->set_rules('price6','','');
        $this->form_validation->set_rules('subtotal6','','');
        
        $this->form_validation->set_rules('itemcode7','','');
        $this->form_validation->set_rules('itemname7','','');
        $this->form_validation->set_rules('qty7','','');
        $this->form_validation->set_rules('price7','','');
        $this->form_validation->set_rules('subtotal7','','');
                
        $this->form_validation->set_rules('itemcode8','','');
        $this->form_validation->set_rules('itemname8','','');
        $this->form_validation->set_rules('qty8','','');
        $this->form_validation->set_rules('price8','','');
        $this->form_validation->set_rules('subtotal8','','');
                
        $this->form_validation->set_rules('itemcode9','','');
        $this->form_validation->set_rules('itemname9','','');
        $this->form_validation->set_rules('qty9','','');
        $this->form_validation->set_rules('price9','','');
        $this->form_validation->set_rules('subtotal9','','');
                
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MPo->addPOActual();
                $this->session->set_flashdata('message','PO successfully');
            }
            redirect('inv/po','refresh');
        }
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-green.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
            
        $data['row'] = $row;
        $data['page_title'] = 'Create Actual Purchase Order';
        $this->load->view('inv/purchaseorder_edit',$data);
    }
    public function status(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        
        if(!$this->MMenu->blocked()){
            $this->MPo->updateStatusPO();
        }
        redirect('inv/po/','refresh');
    }
    
}
?>