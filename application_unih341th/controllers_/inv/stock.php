<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stock extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MInvreport'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('status','Status','callback__check_stockcard');
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        $this->form_validation->set_rules('itemcode','','');
        $this->form_validation->set_rules('itemname','','');
        
        if($this->form_validation->run()){
            if($this->input->post('status') == 'qoh'){
                $data['results'] = $this->MInvreport->getQOH($this->input->post('whsid'));    
            }elseif($this->input->post('status') == 'ss'){
                $data['results'] = $this->MInvreport->getStockSummary($this->input->post('whsid'),$this->input->post('fromdate'),$this->input->post('todate'));    
            }else{
                $data['results'] = $this->MInvreport->getStockCard($this->input->post('whsid'),$this->input->post('itemcode'),$this->input->post('fromdate'),$this->input->post('todate'));    
            }            
        }else{
            $data['results'] = false;
        }
        
        $data['reportDate'] = date("Y-m-d");
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        if($this->session->userdata('whsid')==1)$data['warehouse'] = $this->MInvreport->getWarehouse();
        $data['page_title'] = 'Stock Report';
        $this->load->view('inv/stockreport_index',$data);
    }
    
    public function _check_stockcard(){
        if($this->input->post('status') == 'sc' && !$this->input->post('itemcode')){
            $this->form_validation->set_message('_check_stockcard','browse product...');
            return false;
        }
        return true;
    }
      
}
?>