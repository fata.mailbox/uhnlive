<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fpb extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MInv'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'inv/fpb/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MInv->countFPB($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MInv->searchFPB($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MInv->countFPB($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MInv->searchFPB($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Inventory In';
        $this->load->view('inv/inventoryin_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('noinvoice','No. Invoice','required');
        $this->form_validation->set_rules('password1','Password','required|callback__check_password');
        $this->form_validation->set_rules('remark','','');
        
        $this->form_validation->set_rules('itemcode0','','');
        $this->form_validation->set_rules('itemname0','','');
        $this->form_validation->set_rules('qty0','','');
        $this->form_validation->set_rules('price0','','');
        
        $this->form_validation->set_rules('itemcode1','','');
        $this->form_validation->set_rules('itemname1','','');
        $this->form_validation->set_rules('qty1','','');
        $this->form_validation->set_rules('price1','','');
        
        $this->form_validation->set_rules('itemcode2','','');
        $this->form_validation->set_rules('itemname2','','');
        $this->form_validation->set_rules('qty2','','');
        $this->form_validation->set_rules('price2','','');
        
        $this->form_validation->set_rules('itemcode3','','');
        $this->form_validation->set_rules('itemname3','','');
        $this->form_validation->set_rules('qty3','','');
        $this->form_validation->set_rules('price3','','');
        
        $this->form_validation->set_rules('itemcode4','','');
        $this->form_validation->set_rules('itemname4','','');
        $this->form_validation->set_rules('qty4','','');
        $this->form_validation->set_rules('price4','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MInv->addInventoryIn();
                $this->session->set_flashdata('message','Inventory In successfully');
            }
            redirect('inv/fpb','refresh');
        }
        $data['page_title'] = 'Create Inventory In';
        $this->load->view('inv/inventoryin_form',$data);
    }
    public function _check_password(){
        $this->load->model('MAuth');
        if(!$this->MAuth->check_password($this->session->userdata('user'),$this->input->post('password1'))){
            $this->form_validation->set_message('_check_password','Sorry, your password invalid');
            return false;
        }
        return true;
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MInv->getFPB($id);
        
        if(!count($row)){
            redirect('inv/fpb','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MInv->getFPBDetail($id);
        $data['page_title'] = 'View Inventory In';
        $this->load->view('inv/inventoryin_view',$data);
    }
    
}
?>