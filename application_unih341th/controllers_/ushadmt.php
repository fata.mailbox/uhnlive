<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ushadmt extends CI_Controller {
    function __construct()
    {
	parent::__construct();
    }
        
    public function index(){
        $this->load->model(array('MAuth','MMenu'));
        $this->load->library('form_validation');
	
        $this->form_validation->set_rules('username','username','trim|required');
        //echo substr(sha1(md5(sha1(('123456')))),5,17);
	$this->form_validation->set_rules('password','password','trim|required|callback__check_verifyuser');
        if($this->form_validation->run()){
            redirect('main','refresh');
        }
	
        $data['extraHeadContent'] = "<script type=\"text/javascript\" src=\"".base_url()."js/login.js\"></script>\n";
	
        $data['page_title'] = "Admin Login";
	$this->load->view('auth/login_index', $data);        
    }
    
    public function _check_verifyuser(){
        if(!$this->MAuth->verifyuser($this->input->post('username'), $this->input->post('password'))){
                $this->form_validation->set_message('_check_verifyuser','Sorry, your login id or password is incorrect!');
                return false;
        }
        return true;
    }
        
/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
    }
?>