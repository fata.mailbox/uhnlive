<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Welcome extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('MFrontend');
	}
	
	function index()
	{
		//$data['results'] = $this->MFrontend->list_product('',4,0);
		
		$data['news'] = $this->MFrontend->list_news('news','Y',25,0);
		$data['promo'] = $this->MFrontend->list_news('promo','Y',4,0);
		$data['jmlpromo'] = count($data['promo']);
		
		$data['gallery'] = $this->MFrontend->list_gallery(4,0);
		$data['testi'] = $this->MFrontend->list_testimonial(10,0);
		
		$data['content'] = 'index/home';
		$this->load->view('index/index',$data);
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */