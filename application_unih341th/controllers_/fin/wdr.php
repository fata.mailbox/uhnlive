<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Wdr extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MWithdrawal'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'fin/wdr/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MWithdrawal->countWithdrawal($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MWithdrawal->searchWithdrawal($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MWithdrawal->countWithdrawal($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MWithdrawal->searchWithdrawal($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Withdrawal Ewallet';
        $this->load->view('finance/withdrawal_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('account_id','','callback__check_accountbank');
        $this->form_validation->set_rules('amount','amount withdrawal','required|callback__check_amount');
        $this->form_validation->set_rules('remark','','');
        $this->form_validation->set_rules('pin','PIN','trim|required|callback__check_pin');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MWithdrawal->addWithdrawal();
                $this->session->set_flashdata('message','Create withdrawal ewallet successfully');
            }
            redirect('fin/wdr','refresh');
        }

        $data['row'] = $this->MWithdrawal->getEwallet($this->session->userdata('userid'));
        if($data['row']['account_id'] > 0){
            $data['bank']=$this->MWithdrawal->getAccountBank($data['row']['account_id']);
        }else $data['bank']=false;
        $data['page_title'] = 'Create Withdrawal Ewallet';
        $this->load->view('finance/withdrawal_form',$data);
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MWithdrawal->getWithdrawal($id);
        
        if(!count($row)){
            redirect('fin/wdr','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Withdrawal';
        $this->load->view('finance/withdrawal_view',$data);
    }
    public function _check_pin(){
        if(!$this->MAuth->check_pin($this->session->userdata('username'),$this->input->post('pin'))){
            $this->form_validation->set_message('_check_pin','Sorry, your pin invalid');
            return false;
        }
        return true;
    }
    public function _check_amount(){
        $amount = str_replace(".","",$this->input->post('amount'));
        
        $row = $this->MWithdrawal->getEwallet($this->session->userdata('userid'));
        if($amount > $row['ewallet']){
            $this->form_validation->set_message('_check_amount','Saldo ewallet tidak mencukupi');
            return false;
        }elseif($amount < 50000){
            $this->form_validation->set_message('_check_amount','Minimum withdrawal Rp. 50.000,-');
            return false;
        }
        return true;
    }  
    public function _check_accountbank(){
        if($this->input->post('account_id') < 1){
            $this->form_validation->set_message('_check_accountbank','Lengkapi data bank Anda terlebih dahulu!');
            return false;
        }
        return true;
    }
}
?>