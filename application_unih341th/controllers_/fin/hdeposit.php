<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Hdeposit extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        $this->load->model(array('MMenu','MRekap'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        $this->form_validation->set_rules('whsid','','');
                
        if($this->form_validation->run()){
            $data['results'] = $this->MRekap->list_deposit($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'));
            $data['total'] = $this->MRekap->sum_deposit($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'));
        }else{
            $data['results'] = false;
            $data['total'] = false;
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['warehouse'] = $this->MRekap->getDropDownWhs();
        $data['page_title'] = 'Summary Deposit';
        $this->load->view('finance/rekapdeposit_index',$data);
    }
    
}
?>