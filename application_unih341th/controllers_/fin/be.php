<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Be extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MEwallet'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('member_id','','');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        
        if($this->form_validation->run()){
            $data['results'] = $this->MEwallet->getBalanceEwallet($this->input->post('member_id'),$this->input->post('fromdate'),$this->input->post('todate'));
            $data['total'] = $this->MEwallet->sumBalanceEwallet($this->input->post('member_id'),$this->input->post('fromdate'),$this->input->post('todate'));
            $data['ewallet'] = $this->MEwallet->getEwallet($this->input->post('member_id'));
        }else{
            $data['results'] = false;
            $data['total'] = false;
            if($this->session->userdata('group_id')>100)$data['ewallet'] = $this->MEwallet->getEwallet($this->session->userdata('userid'));
            else $data['ewallet']=false;
        }
        
        $data['reportDate'] = date("Y-m-d");
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'Balance Ewallet Report';
        
        $this->load->view('finance/balance_ewallet_index',$data);
    }
}
?>