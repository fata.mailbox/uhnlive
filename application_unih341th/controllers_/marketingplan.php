<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Marketingplan extends CI_Controller {
    function __construct()
    {
	parent::__construct();
		$this->load->model('MFrontend');
	}
	
	public function index($id=0){
		$data['testi'] = $this->MFrontend->list_testimonial(10,0);
		
		$data['content'] = 'index/marketingplan';
		$this->load->view('index/index',$data);
	}
	public function daftar($id=0){
		$data['testi'] = $this->MFrontend->list_testimonial(10,0);
		
		$data['content'] = 'index/marketingplan_daftar';
		$this->load->view('index/index',$data);
	}
	
}
