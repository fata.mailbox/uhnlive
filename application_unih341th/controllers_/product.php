<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product extends CI_Controller {
    function __construct()
    {
	parent::__construct();
		$this->load->model('MFrontend');
	}
	
	public function index(){
		$data['testi'] = $this->MFrontend->list_testimonial(10,0);
		$data['results'] = $this->MFrontend->list_product('',10,0);
		$data['menu'] = $this->MFrontend->list_category();

		$data['content'] = 'index/product';
		$this->load->view('index/index',$data);
	}
	public function detail($id=0){
		$data['testi'] = $this->MFrontend->list_testimonial(10,0);
		$data['menu'] = $this->MFrontend->list_category();

		$row = $this->MFrontend->get_product($id);
		if(!count($row)){
			redirect('product','refresh');
		    }
		$data['row'] = $row;
		
		$data['content'] = 'index/detail_product';
		$this->load->view('index/index',$data);
	}
	public function cat($id=0,$page=0){
		// update by Boby 20100615
		$this->load->library('pagination');
		$data['menu'] = $this->MFrontend->list_category();
		// end update by Boby 20100615
		
		$data['testi'] = $this->MFrontend->list_testimonial(10,0);
		$data['results'] = $this->MFrontend->list_product($id,10,$page);
		
		// update by Boby 20100615
		
		$config['base_url'] = site_url().'product/cat/'.$id.'/';
		$config['per_page'] = 10;
		$config['uri_segment'] = 4;
		// if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
		$config['total_rows'] = $this->MFrontend->count_list_product($id);
		$data["test"] = $config['total_rows'];
		$this->pagination->initialize($config);
		
		// end update by Boby 20100615
		
		$data['content'] = 'index/product';
		$this->load->view('index/index',$data);
	}
	
}
