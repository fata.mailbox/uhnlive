<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Slsinc extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('MSales','MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('tahun','','');
		$this->form_validation->set_rules('quart','','');
        //$this->form_validation->set_rules('sort','','');
        
        if($this->form_validation->run()){
			$data['period_']="Quart";
			if($this->input->post('quart')=="00-00"){
				$data['results']=$this->MSales->sales_incentive($this->input->post('tahun'),0);
			}else{
				$period = $this->input->post('tahun');
				$period.= '-';
				$period.= $this->input->post('quart');
				
				$data['results']=$this->MSales->sales_incentive($this->input->post('tahun'),$period);
				$data['period_']="Month";
			}
            $data['thn']=$this->input->post('tahun');
			$data['total']=0;
        }else{
            $data['results']=false;
			$data['thn']=date("Y");
            $data['total']=false;
        }
        $data['dropdownyear'] = $this->MSales->get_year_report();
		$data['dropdownq'] = $this->MSales->get_q(1);
        $data['page_title'] = 'Sales Incentive Report';
        $this->load->view('report/sales_incentive',$data);
    }
        
}
?>