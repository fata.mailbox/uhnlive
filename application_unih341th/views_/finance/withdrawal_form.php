<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>

<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('fin/wdr/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		<tr>
			<td width='24%'>name</td>
			<td width='1%'>:</td>
			<td width='75%'><b><?php if($this->session->userdata('group_id') == 101)
			{ echo $this->session->userdata('userid'); } 
			else { echo $this->session->userdata('username'); }
			 echo " / ".$this->session->userdata('name');?></b></td> 
		</tr>
        <tr>
			<td valign='top'>transfer to</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo "nama bank: <b>".$bank['bank_id']."</b><br />"."no. rekening: <b>".$bank['no']."</b><br />"."atas nama: <b>".$bank['name']."</b><br /> Cabang: <b>".$bank['area']."</b>";?>
            
            <span class='error'><?php echo form_error('account_id');?></span></td> 
		</tr>
		<tr>
			<td>saldo ewallet Rp.</td>
			<td>:</td>
			<td><b><?php echo form_hidden('account_id',$row['account_id']); echo $row['fewallet'];?></b></td> 
		</tr>
		<tr>
			<td valign='top'>transfer amount Rp.</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" class="textbold" name="amount" id="amount" autocomplete="off" value="<?php echo set_value('amount');?>" onkeyup="this.value=formatCurrency(this.value);">
					 <span class='error'>*<?php echo form_error('amount');?></span></td> 
		</tr>
		
		<tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?></td> 
		</tr>
		<tr>
			<td valign='top'>PIN</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'pin','id'=>'pin','value'=>set_value('pin'));
    			echo form_password($data);?><span class='error'>* <?php echo form_error('pin');?></span></td> 
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>
		</table>

<?php echo form_close();?>
