<table width='99%'>
<?php echo form_open('fin/transfer/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='5%'>No.</th>
      <th width='10%'>Date</th>
      <th width='26%'>Transfer From</th>
      <th width='26%'>Transfer To</th>
      <th width='13%'>Amount Rp.</th>      
		<th width='20%'>Remark</th>
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor("/fin/transfer/view/".$row['id'],$counter);?></td>
     <td><?php echo anchor("/fin/transfer/view/".$row['id'],$row['tgl']);?></td>
     <td><?php echo anchor("/fin/transfer/view/".$row['id'],$row['member_id']." - ".$row['namamember']);?></td>
      <td><?php echo anchor("/fin/transfer/view/".$row['id'], $row['no_stc']." - ".$row['namastc']);?></td>      
      <td align="right"><?php echo anchor("/fin/transfer/view/".$row['id'],$row['famount']);?></td>
		<td><?php echo anchor("/fin/transfer/view/".$row['id'],$row['remark']." ");?></td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
