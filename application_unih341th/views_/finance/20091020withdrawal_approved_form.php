<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>

<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('fin/wdrapp/edit/'.$row->id, array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		<tr>
			<td width='24%'>name</td>
			<td width='1%'>:</td>
			<td width='75%'><b><?php if($row->flag == 'mem') echo $row->member_id; else echo $row->no_stc;
			 echo " / ".$this->session->userdata('name');?></b></td> 
		</tr>
        <tr>
			<td valign='top'>transfer to</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_hidden('amount',$row->amount); echo form_hidden('flag',$row->flag); echo form_hidden('id',$row->id); echo form_hidden('member_id',$row->member_id); echo "nama bank: <b>".$row->bank_id."</b><br />"."no. rekening: <b>".$row->no."</b><br />"."atas nama: <b>".$row->name."</b><br /> Cabang: <b>".$row->area."</b>";?></td> 
		</tr>
		<tr>
			<td valign='top'>transfer amount Rp.</td>
			<td valign='top'>:</td>
			<td valign='top'><b><?php echo $row->famount;?></b></td> 
		</tr>
		<tr>
			<td valign='top'>Approved ?</td>
			<td valign='top'>:</td>
			<td><?php $data = array('approved'=>'Approved','reject'=>'Reject');
    					echo form_dropdown('status',$data);?></td> 
		</tr>
		<tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?></td> 
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>
		</table>

<?php echo form_close();?>
	