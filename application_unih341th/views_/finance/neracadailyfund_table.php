
<table width='100%'>
<?php echo form_open('fin/ndf', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  echo form_input($data);?>   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
   echo "to: ".form_input($data);?>
   </td>
  </tr>
  
<tr>
			<td valign='top'>Cabang </td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('whsid',$warehouse);?></td>
		</tr>
  
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
	</tr>
<?php echo form_close();?>				
</table>

<table width="100%"> 
<tr>
  <td colspan="5" style="border-bottom:solid thin #000099"></td>
</tr>
<tr height="23">
	<td width="5%">1</td>
    <td width="38%"><strong>Deposit</strong></td>
	<td width="7%" align="right"><strong>Rp :</strong></td>
	<td width="25%" align="right"><strong><?php echo $dps['ftotal_approved'];?></strong></td>
	<td width="25%" align="right"> ________________ &nbsp;&nbsp;</td>
</tr>
<tr height="23">
	<td></td>
    <td>a. Bank Transfer</td>
	<td align="right">Rp :</td>
	<td align="right"><?php echo $dps['ftransfer'];?></td>
	<td align="right"> ________________ &nbsp;&nbsp;</td>
</tr>
<tr height="23">
	<td></td>
    <td>b. Cash</td>
	<td align="right">Rp :</td>
	<td align="right"><?php echo $dps['ftunai'];?></td>
	<td align="right"> ________________ &nbsp;&nbsp;</td>
</tr>
<tr height="23">
	<td></td>
    <td>c. Debit Card</td>
	<td align="right">Rp :</td>
	<td align="right"><?php echo $dps['fdebit_card'];?></td>
	<td align="right"> ________________ &nbsp;&nbsp;</td>
</tr>
<tr height="23">
	<td></td>
    <td>d. Credit Card</td>
	<td align="right">Rp :</td>
	<td align="right"><?php echo $dps['fcredit_card'];?></td>
	<td align="right"> ________________ &nbsp;&nbsp;</td>
</tr>
<tr>
  <td colspan="5" style="border-bottom:solid thin #000099"></td>
</tr>
<tr height="23">
	<td>2</td>
    <td><strong>Withdrawal</strong></td>
	<td align="right"><strong>Rp :</strong></td>
	<td align="right"><strong><?php echo $wdr['famount'];?></strong></td>
	<td align="right"> ________________ &nbsp;&nbsp;</td>
</tr>
<tr height="23">
	<td></td>
    <td>a. Bank Transfer</td>
	<td align="right">Rp :</td>
	<td align="right"><?php echo $wdr['famount'];?></td>
	<td align="right"> ________________ &nbsp;&nbsp;</td>
</tr>
<tr>
  <td colspan="5" style="border-bottom:solid thin #000099"></td>
</tr>
<tr height="23">
	<td align="center"></td>
	<td align="right"><strong>Total</strong> (1-2)</td>
	<td align="right"><strong>Rp :</strong></td>
	<td align="right"><strong><?php echo $total12;?></strong></td>
	<td align="right"> ________________ &nbsp;&nbsp;</td>
</tr>
<tr>
      <td colspan="5" style="border-bottom:double medium #000099"></td>
    </tr>
<tr><td>&nbsp;</td>
	<td colspan="4">Periksa lampiran Debet Card, Credit Card & total uang masuk [&nbsp;&nbsp;&nbsp;]Klop &nbsp;&nbsp;&nbsp;&nbsp;[&nbsp;&nbsp;&nbsp;]Tidak Klop</td></tr>
<tr><td colspan="5">&nbsp;</td></tr>
<tr>
  <td colspan="5" style="border-bottom:solid thin #000099"></td>
</tr>
<tr height="23">
	<td>3</td>
                  <td><strong>Sales Order</strong></td>
						<td align="right"></td>
						<td align="right">Tanggal Entry</td>
						<td align="right">Tangal Order</td>
					</tr>
					<tr height="23">
                  		<td align="center"></td>
		                <td>a. Direct Sales</td>
						<td align="right">Rp :</td>
						<td align="right"><?php echo $directsoentry['ftotalharga'];?></td>
						<td align="right"><?php echo $directso['ftotalharga'];?></td>
					</tr>
					<tr height="23">
                  		<td align="center"></td>
						<td align="right" colspan="2">Poin Value :</td>
						<td align="right"><?php echo $directsoentry['ftotalpv'];?></td>
						<td align="right"><?php echo $directso['ftotalpv'];?></td>
					</tr>
					
					<tr height="23">
                  <td align="center"></td>
                  <td>b. Sales Order Stockiest</td>
						<td align="right">Rp :</td>
<td align="right"><?php echo $stcsoentry['ftotalharga'];?></td>
						<td align="right"><?php echo $stcso['ftotalharga'];?></td>
                        </tr>
					<tr height="23">
                  <td align="center"></td>
					<td align="right" colspan="2">Poin Value :</td>
						<td align="right"><?php echo $stcsoentry['ftotalpv'];?></td>
						<td align="right"><?php echo $stcso['ftotalpv'];?></td>
					</tr>
					<tr height="23">
                  <td align="center"></td>
                  <td><strong>c. Total SO (a+b)</strong></td>
						<td align="right">Rp :</td>
                        <td align="right"><strong><?php echo $totalsoentry['ftotalharga'];?></strong></td>
						<td align="right"><strong><?php echo $totalso['ftotalharga'];?></strong></td>
					</tr>
                    
					<tr height="23">
                  <td align="center"></td>
                  <td align="right" colspan="2">Poin Value :</td>
						<td align="right"><strong><?php echo $totalsoentry['ftotalpv'];?></strong></td>
						<td align="right"><strong><?php echo $totalso['ftotalpv'];?></strong></td>
					</tr>
					<tr>
     <td colspan="5" style="border-bottom:solid thin #000099"></td>
</tr>

					<tr height="23">
                  <td>4</td>
                  <td><strong>SO KIT / New Member</strong></td>
						<td align="right"></td>
						<td align="right">Ke Perusahaan</td>
						<td align="right">Ke Stockiest</td>
					</tr>
					<tr height="23">
						<td align="right" colspan="3">Orang :</td>
						<td align="right"><?php echo $kit['fjmlmember']." orang";?></td>
						<td align="right"><?php echo $kitstc['fjmlmember']." orang";?></td>
					</tr>
					<tr height="23">
	                  <td colspan="3" align="right">Rp :</td>
						<td align="right"><?php echo $kit['ftotalharga'];?></td>
						<td align="right"><?php echo $kitstc['ftotalharga'];?></td>
					</tr>
                    <tr>
      <td colspan="5" style="border-bottom:double medium #000099"></td>
    </tr>
    

					<tr><td>&nbsp;</td>
					<td colspan="4">Periksa Sales Order, berdasarkan tanggal order & entry [&nbsp;&nbsp;&nbsp;]Klop &nbsp;&nbsp;&nbsp;&nbsp;[&nbsp;&nbsp;&nbsp;]Tidak Klop</td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>
                    <tr>
  <td colspan="5" style="border-bottom:solid thin #000099"></td>
</tr>

					<tr height="23">
                  <td>5</td>
                  <td><strong>Request Stock Stockiest</strong></td>
						<td align="right"></td>
						<td align="right">Ke Perusahaan</td>
						<td align="right">Ke Stockiest</td>
					</tr>
					<tr height="23">
						<td align="right" colspan="3">Rp :</td>
						<td align="right"><?php echo $ro['ftotalharga'];?></td>
						<td align="right"><?php echo $rostc['ftotalharga'];?></td>
					</tr>
					<tr height="23">
	                  <td colspan="3" align="right">Poin Value :</td>
						<td align="right"><?php echo $ro['ftotalpv'];?></td>
						<td align="right"><?php echo $rostc['ftotalpv'];?></td>
					</tr>
                    <tr>
      <td colspan="5" style="border-bottom:double medium #000099"></td>
    </tr>

					<tr><td>&nbsp;</td>
					<td colspan="4">Periksa Request Stock Stockiest/ M-Stc baik ke Perusahaan maupun ke Stockiest [&nbsp;&nbsp;&nbsp;]Klop &nbsp;&nbsp;&nbsp;&nbsp;[&nbsp;&nbsp;&nbsp;]Tidak Klop</td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>
                                        <tr>
  <td colspan="5" style="border-bottom:solid thin #000099"></td>
</tr>

					<tr height="23">
                  <td>6</td>
                  <td><strong>Pinjaman Stockiest</strong></td>
						<td align="right"></td>
						<td align="right">Pinjaman</td>
						<td align="right">Bayar Pinjaman</td>
					</tr>
					<tr height="23">
						<td align="right" colspan="3">Rp :</td>
						<td align="right"><?php echo $pinjaman['ftotalharga'];?></td>
						<td align="right"><?php echo $pelunasan['ftotalharga'];?></td>
					</tr>
					<tr height="23">
	                  <td colspan="3" align="right">Poin Value :</td>
						<td align="right"><?php echo $pinjaman['ftotalpv'];?></td>
						<td align="right"><?php echo $pelunasan['ftotalpv'];?></td>
					</tr>
                    <tr>
      <td colspan="5" style="border-bottom:double medium #000099"></td>
    </tr>
					<tr><td>&nbsp;</td>
					<td colspan="4">Periksa Pinjaman baik ke pinjaman maupun ke pinjaman ke titipan [&nbsp;&nbsp;&nbsp;]Klop &nbsp;&nbsp;&nbsp;&nbsp;[&nbsp;&nbsp;&nbsp;]Tidak Klop</td></tr>		
					<tr><td colspan="5">&nbsp;</td></tr>
                    
                         <tr>
  <td colspan="5" style="border-bottom:solid thin #000099"></td>
</tr>               
					<tr height="23">
                  <td>7</td>
                  <td><strong>Saldo ewallet</strong> uptodate</td>
						<td align="right">&nbsp;</td>
						<td align="right">Member</td>
						<td align="right">Stockiest & M-Stockiest</td>
					</tr>
					<tr height="23">
                  <td></td>
                  <td></td>
						<td align="right">Rp :</td>
						<td align="right"><?php echo $ewalletmember['ftotalewallet'];?></td>
						<td align="right"><?php echo $ewalletstc['ftotalewallet'];?></td>
					</tr>
					
                    <tr>
      <td colspan="5" style="border-bottom:double medium #000099"></td>
    </tr>
					<tr><td>&nbsp;</td>
					<td colspan="4">Periksa Total ewallet [&nbsp;&nbsp;&nbsp;]Klop &nbsp;&nbsp;&nbsp;&nbsp;[&nbsp;&nbsp;&nbsp;]Tidak Klop</td></tr>
					<tr><td colspan="5">&nbsp;</td></tr>
                    <tr>
  <td colspan="5" style="border-bottom:solid thin #000099"></td>
</tr>               
					<tr height="23">
                  <td>8</td>
                  <td colspan="2"><strong>Stock & Pinjaman Stokiest</strong> uptodate</td>
						<td align="right">Saldo Stock Stc</td>
						<td align="right">Saldo Pinjaman</td>
					</tr>
					<tr height="23">
						<td align="right" colspan="3">Rp :</td>
						<td align="right"><?php echo $saldotitipan['ftotalharga'];?></td>
						<td align="right"><?php echo $saldopinjaman['ftotalharga'];?></td>
					</tr>
					<tr height="23">
	                  <td colspan="3" align="right">Poin Value :</td>
						<td align="right"><?php echo $saldotitipan['ftotalpv'];?></td>
						<td align="right"><?php echo $saldopinjaman['ftotalpv'];?></td>
					</tr>
				<tr>
      <td colspan="5" style="border-bottom:double medium #000099"></td>
    </tr>
					<tr><td colspan="5">&nbsp;</td></tr>
				<tr><td></td>
				<td colspan="4">
				<table width="100%" cellspacing="0" cellpadding="0">
				<tr>
				<td align="center" class="cName">Diserahkan oleh</td>
				<td align="center" class="cName">Diterima oleh</td>
				<td align="center" class="cName">Diketahui oleh</td>
				</tr>
				<tr height="50">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td align="center" class="cName">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
				<td align="center" class="cName">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
				<td align="center" class="cName">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
				</tr>
				</table>				
				</td></tr>
				  </table>
                  
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>
