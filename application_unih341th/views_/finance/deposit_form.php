<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>

<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('fin/dps/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		<tr>
			<td width='24%'>transfer date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php $data = array('name'=>'date','id'=>'date1','readonly'=>'1','size'=>12,'value'=>set_value('date',date('Y-m-d')));
   			echo form_input($data);?></td> 
		</tr>
		<tr>
			<td valign='top'>transfer amount Rp.</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" class="textbold" name="amount" id="amount" autocomplete="off" value="<?php echo set_value('amount');?>" onkeyup="this.value=formatCurrency(this.value);">
					 <span class='error'>*<?php echo form_error('amount');?></span></td> 
		</tr>
		<tr>
			<td valign='top'>transfer to bank</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_dropdown('bank_id',$bank);?></td> 
		</tr>
		<tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?></td> 
		</tr>
		<tr>
			<td valign='top'>PIN</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'pin','id'=>'pin','value'=>set_value('pin'));
    			echo form_password($data);?><span class='error'>* <?php echo form_error('pin');?></span></td> 
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>
		</table>

<?php echo form_close();?>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>    
<?php $this->load->view('footer');?>
