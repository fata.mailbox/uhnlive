<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<div class="ViewHold">
	<div id="companyDetails"><?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?></div>
	<p>
		<strong><?php echo "withdrawal id : ";?> <?php echo $row->id;?><br />
		<?php echo "date : ";?> <?php echo $row->tgl;?>
		</strong>
	</p>
	
<h2>Form Withdrawal</h2>
<hr />
<h3><?php if($row->flag == 'stc') echo $row->no_stc." / ".$row->nama; else echo $row->member_id ." / ".$row->nama;?></h3>

<p>
		<?php
				echo "Withdrawal Rp: <b>".$row->famount."</b><br />";
				echo "Transfer ke: ".$row->bank_id." / ".$row->no." / ".$row->name." / ".$row->area;
		?>
	</p>
<table class="stripe">
	<tr>
      <th width='34%'>Description</th>
      <th width='1%'>:</th>
      <th width='65%'>Value</th>
	</tr>
	<tr>
		<td>Status ?</td>
		<td>:</td>
		<td><?php echo $row->status;?></td>
    </tr>
	<tr>
		<td>Tgl Approved</td>
		<td>:</td>
		<td><?php echo $row->approved;?></td>
    </tr>
    <tr>
		<td>Remark</td>
		<td>:</td>
		<td><?php echo $row->remark;?></td>
    </tr>
    <tr>
		<td>Remark Finance</td>
		<td>:</td>
		<td><?php echo $row->remarkapp;?></td>
    </tr>
    
    <tr>
		<td>Approved date / by</td>
		<td>:</td>
		<td><?php echo $row->approved." / ".$row->approvedby;?></td>
    </tr>
    <tr>
		<td>Created date / by</td>
		<td>:</td>
		<td><?php echo $row->created." / ".$row->createdby;?></td>
    </tr>
</table>
	
<?php $this->load->view('footer');?>
