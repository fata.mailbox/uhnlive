<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	<table width="100%">
	<?php echo form_open('report/allocation/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
        <tr>
			<td valign='top' width="19%">Tahun</td>
			<td valign='top' width="1%">:</td>
			<td width="80%">
				<?php 
					echo form_dropdown('tahun',$dropdownyear);
					echo form_dropdown('quart',$dropdownq);
				?>
			</td>
		</tr>
        <tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><?php echo form_submit('submit','preview');?></td>
		</tr>                
         <?php echo form_close();?>
    <tr><td colspan="3"><hr /></td></tr>
	</table>
	<?php echo anchor('report/allocation/create','Create Allocation Member'); ?>
<table class="stripe">
	<tr>
      <th width='20%'>Periode</th>
      <th width='20%'>MemberID - Nama</th>
      <th width='20%'>City</th>
      <th width='20%'>Region</th>
      <th width='20%'><div align="center">Action</div></th>
    </tr>
   
<?php
if ($results): 
	foreach($results as $key => $row): 
		$periode = $row['thn']." ".$row['namaBln'];
?>
    <tr>
		<td><?php echo $periode;?></td>
		<td><?php echo $row['member_id']." - ".$row['nama'];?></td>
		<td><?php echo $row['kota'];?></td>
		<td><?php echo $row['region'];?></td>
		<td align="center">Edit / Closed</td>
    </tr>
    <?php endforeach; 
else: ?>
    <tr>
		<td colspan="5">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>			                
<?php $this->load->view('footer');?>
