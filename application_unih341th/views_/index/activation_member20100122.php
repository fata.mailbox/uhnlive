<div class="leftContent">
          <div class="news">
            <div class="headTitleBgNews">
              <h6>Register</h6>
            </div>
            <div class="newsContent">
              <p>Untuk menjadi member atau anggota UNIHEALTH, silahkan anda melakukan register.<br />
                <br />
              </p>
              <div style="text-align:center"><a href="<?php echo site_url();?>register/member/"><img src="<?php echo base_url();?>images/frontend/btn-register.gif" width="130" height="35" /></a>&nbsp;&nbsp;</div>
              <br />
            </div>
          </div>
 </div>
        <div class="centerContent">
          <div class="headTitleContact"><h6>Activation Member</h6></div>
          <div class="prodCont">
        	<p>Apabila anda sudah mempunyai member id & activation code daftarkan segera menjadi member dengan mengisi form dibawah ini:</p>
        	<ul class="contactus">  
		  	  <li>&nbsp;</li>
              
                <?php echo form_open('register/activation/', array('id' => 'form'));?>
                <table width="100%">
					<tr>
							<td width='25%' valign='top'><span>Sponsor ID</span></td>
							<td width='75%'><li><?php $data = array('name'=>'introducerid','id'=>'introducerid','size'=>'8','maxlength'=>'8','value'=>set_value('introducerid')); echo form_input($data);?>*</li> 
                            <div class="errorMessage"><?php echo form_error('introducerid');?></div>
							</td> 
					</tr>
					<tr>
							<td valign='top'><span>Member ID</span></td>
							<td><li><?php $data = array('name'=>'userid','id'=>'userid','size'=>'8','maxlength'=>'8','value'=>set_value('userid')); echo form_input($data);?>*</li><div class="errorMessage"><?php echo form_error('userid');?></div>
							</td>
					</tr> 
					<tr>
							<td valign='top'><span>Activation Code</span></td>
							<td><li><?php $data = array('name'=>'activation','id'=>'activation','size'=>'15','maxlength'=>'20','value'=>set_value('activation')); echo form_input($data);?>*</li><div class="errorMessage"><?php echo form_error('activation');?></div>
							</td>
					</tr>
					<tr>
							<td valign='top'><span>Direct Upline ID</span> </td>
							<td><li><?php $data = array('name'=>'placementid','id'=>'placementid','size'=>'8','maxlength'=>'8','value'=>set_value('placementid')); echo form_input($data);?>*</li><div class="errorMessage">
							<?php echo form_error('placementid');?></div></td>
					</tr>
					 				
					 <tr>
							<td valign='top'><span>Full Name</span></td>
							<td><li><?php $data = array('name'=>'name','id'=>'name','size'=>'30','maxlength'=>'50','value'=>set_value('name')); echo form_input($data);?>*</li> <div class="errorMessage">
							<?php echo form_error('name');?></div></td>
					</tr>
                                        <tr>
							<td valign='top'><span>No. HP</span></td>
							<td><li><?php $data = array('name'=>'hp','id'=>'email','size'=>'15','maxlength'=>'15','value'=>set_value('hp')); echo form_input($data);?>*</li> <div class="errorMessage">
							<?php echo form_error('hp');?></div></td>
					</tr>

                    <tr>
							<td valign='top'><span>Email</span></td>
							<td><li><?php $data = array('name'=>'email','id'=>'email','size'=>'30','maxlength'=>'50','value'=>set_value('email')); echo form_input($data);?></li> <div class="errorMessage"><?php echo form_error('email');?></div></td>
					</tr>
					<tr>
							<td valign='top'><span>Password</span></td>
							<td><li><?php $data = array('name'=>'password','id'=>'password','size'=>'12','maxlength'=>'50','value'=>set_value('password')); echo form_password($data);?>*</li> <div class="errorMessage"><?php echo form_error('password');?></div>
							</td>
					</tr>
					<tr>
							<td valign='top'><span>Verify Password</span></td>
							<td><li><?php $data = array('name'=>'password2','id'=>'password2','size'=>'12','maxlength'=>'50','value'=>set_value('password2')); echo form_password($data);?></li></td>
					</tr> 
					<tr>
							<td valign='top'><span>PIN</span> </td>
							<td><li><?php $data = array('name'=>'pin','id'=>'pin','size'=>'12','maxlength'=>'50','value'=>set_value('pin')); echo form_password($data);?>*</li> <div class="errorMessage">
							<?php echo form_error('pin');?></div>
							</td>
					</tr>
					<tr>
							<td valign='top'><span>Verify PIN</span></td>
							<td><li><?php $data = array('name'=>'pin2','id'=>'pin2','size'=>'12','maxlength'=>'50','value'=>set_value('pin2')); echo form_password($data);?></li></td>
					</tr> 
					<tr>
							<td valign='top'><span>Your Question ?</span></td>
							<td><li><?php $data = array('name'=>'question','id'=>'question','size'=>'20','maxlength'=>'50','value'=>set_value('question')); echo form_input($data);?>*</li> <div class="errorMessage"> <?php echo form_error('question');?></div>
							</td>
					</tr>
					<tr>
							<td valign='top'><span>Your Answer</span> </td>
							<td><li><?php $data = array('name'=>'answer','id'=>'answer','size'=>'20','maxlength'=>'50','value'=>set_value('answer')); echo form_input($data);?>*</li> <div class="errorMessage"><?php echo form_error('answer');?></div></td>
					</tr>
					<tr>
							<td valign='top'><span>Security Code</span></td>
							<td><li><span id="captchaImage"><?php echo $captcha['image']; ?></span></li></td>
					</tr> 				
					<tr>
						<td valign='top'><span>Confirm Sercurity Code</span> </td>
						<td><li><?php $data = array('name'=>'confirmCaptcha','id'=>'confirmCaptcha','autocomplete'=>'off','size'=>'8','maxlength'=>'6'); echo form_input($data);?>*</li> <div class="errorMessage"><?php echo form_error('confirmCaptcha');?></div></td>
					</tr>
					<tr>
						<td></td>
						<td><?php echo form_submit('submit','Submit');?></td>
					</tr>
					<tr>
						<td><div class="errorMessage">*) required</span></td>
						<td></td>
					</tr>					
					</table>
        <?php echo form_close(); ?>
		  	  <li>&nbsp;</li>
        	</ul>
          </div>
        </div>
        <div class="rightContent">
			<?php $this->load->view('index/right_customer');?>
          	<?php $this->load->view('index/member_of_soho');?>
		</div>