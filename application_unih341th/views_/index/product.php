<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->

<!--content-->
            <div class="content" style="width:572px;">
            	<!--about-->
                <div class="about" style="width:560px; padding:5px; border-color:#dfe8ec; background:none;">
                	<div class="aboutCont">
                    	<h2 style="color:#15384f;">Products</h2><hr />
                        <!--list_product-->  
                        <div class="list_product">
                        	<ul>
                            
                                <?php foreach($results as $row): ?>    
                                <li>
                                	<table width="100%">
                                        <tr>
                                            <td width="141" valign="top">
                                            	<a href="<?php echo base_url();?>userfiles/product/<?php echo $row['image'];?>" class="highslide" onclick="return hs.expand(this)">
                                        		<img src="<?php echo base_url();?>userfiles/product/t_<?php echo $row['image'];?>" class="post" title="Click to enlarge" /></a>                                            </td>
                                            <td width="256" valign="top">
                                   	  		  	<div class="info">
                                                    <h3><a href="<?php echo site_url();?>product/detail/<?php echo $row['id'];?>"><?php echo $row['name'];?></a></h3>
                                                    <p><?php echo $row['headline'];?></p>
                                                </div>
                                          	</td>
                                            <td width="103" valign="top">
                                                <b>Harga :</b>
                                           	  	<p class="price">Rp. <?php echo number_format($row['pricecust'],'0','','.');?>,-</p>
                                              	<div class="readmore"><a href="<?php echo site_url();?>product/detail/<?php echo $row['id'];?>">Detail &raquo;</a></div>
                                       	  	</td>
                                   	  	</tr>
                                    </table>
                                </li>
                                <?php endforeach; ?>
                                
                            </ul>
                        </div><!--end list_product-->
                    </div>
                  <div class="clearBoth"></div>
                </div><!--end about-->
                
                
                <!--end pagiNation
                <div class="pagiNation">
                    <ul>
                        <li class="brdr_radius_left"><p>&laquo; Halaman:</p> </li>
                        <li class="pagiNation_active"><p>1</p></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li class="brdr_radius_right"><a href="#">Next &raquo;</a></li>
                        <span>1/5 Page</span>
                    </ul>
                </div> --> <!--end pagiNation-->
                
                
              <div class="clearBoth"></div>
            </div><!--end content-->
            