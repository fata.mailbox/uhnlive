<!--content-->
            <div class="content" style="width:572px;">
            	<!--about-->
                <div class="about" style="width:560px; padding:5px; border-color:#dfe8ec; background:none;">
                	<div class="aboutCont">
                    	<h2 style="color:#15384f;">Download</h2><hr />
                        <style type="text/css">
                            .downloadTable { padding:0px; }
                            .downloadTable tr.row1 { background:#effcdf; }
                            .downloadTable tr.row2 { background:#e2fcc2; }
                            .downloadTable tr.row1:hover, tr.row2:hover { background:#fff; }
                            .downloadTable td { border-bottom:solid 1px #d3e4c7; padding:10px 5px; }
						</style>
                        
                        <?php if ($this->session->flashdata('message')){
					echo "<div class='message'><b>".$this->session->flashdata('message')."</b></div><br>";}?>
					
		<?php echo form_open('download/view/'.$this->uri->segment(3), array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
        
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            	<tbody>
                                               	  <tr style="background:#effcdf;">
                                                   	  <td align="center" valign="top" width="10%"><img src="<?php echo base_url();?>images/frontend/<?php echo $row['type'];?>.gif" alt="<?php echo $row['type'];?>" title="<?php echo $row['type'];?>" height="40" width="40"></td>
                                                        <td align="left" width="90%"><?php echo $row['description'];?> <b>( <?php echo $row['file'];?> )</b></td>
                                                  </tr>
                                                </tbody>
                                            </table>
                                                            	<table class="downloadTable" border="0" cellpadding="0" cellspacing="0" width="100%">
                            	<tbody>
                                    <tr class="row1">
                                    	<td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr class="row2" height="45">
                                        <td valign="top" width="33%">Security code</td>
                                        <td valign="top" width="1%">:</td>
                                        <td width="66%"><span id="captchaImage"><?php echo $captcha['image']; ?></span></td>
                                  </tr>
                                    <tr class="row1">
                                        <td valign="top">Confirm security code</td>
                                        <td valign="top">:</td>
                                        <td><?php $data = array('name'=>'confirmCaptcha','id'=>'confirmCaptcha','size'=>'8','maxlength'=>'6'); echo form_input($data);?><span style="color:#FF0000;">* <?php echo form_error('confirmCaptcha');?></span></td>
                                    </tr>
                                    <tr class="row2">
                                        <td align="right" valign="top">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td><input name="submit" value="Download" type="submit"></td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        <?php echo form_close();?>
                    </div>
                  <div class="clearBoth"></div>
                </div><!--end about-->                
                
              <div class="clearBoth"></div>
            </div><!--end content-->
			
            