<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->



        <div class="submenuContent">
          <div class="headTitleBg"><h6>Archives</h6></div>
          <ul class="submenuUL">
          <?php if($plan_archive): ?>
          <li><a href="<?php echo site_url();?>news/">All Archives</a></li>
<?php foreach($plan_archive as $plan_archive): ?>
            <li><a href="<?php echo site_url();?>marketingplan/index/<?php echo $plan_archive['id'];?>"><?php echo $plan_archive['category'];?></a></li>
          <?php endforeach;?>          
          <?php else:  ?>
            Data is not available.
		<?php endif;?>	
          </ul>
          
        </div>  
        <div class="detailContent">
<div class="headTitleContent"><h6>Marketing Plan</h6></div><br />
          <ul class="newsList">
          <?php if($plan): 
					foreach($plan as $plan): ?>
            <li><p><?php echo $plan['longdesc'];?></li>
            
                          <?php endforeach; ?>
                <?php echo $this->pagination->create_links(); ?>          
               <?php else:?>
              
              	<li>Marketing plan not avalaible.</li>
              <?php endif;?>

          </ul>          
        </div>
        
        <div class="rightContent">
        <?php $this->load->view('index/right_customer');?>  
<?php $this->load->view('index/member_of_soho');?>
        </div>