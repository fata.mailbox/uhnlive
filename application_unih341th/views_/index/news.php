<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->

<!--content-->
            <div class="content" style="width:572px;">
            	<!--about-->
                <div class="about" style="width:560px; padding:5px; border-color:#dfe8ec; background:none;">
                	<div class="aboutCont">
                    	<h2 style="color:#15384f;"><?=$title;?></h2><hr />
                        <!--list_news-->  
                        <div class="list_news">
                        	<ul>
                            <?php if($news): 
					foreach($news as $news): ?>
                                <li>
                                	<table width="100%">
                                        <tr>
                                            <td width="160" valign="top">
                                            <?php if($news['file']){?>
                                            	<img src="<?=base_url();?>userfiles/news/<?=$news['file'];?>" class="post" title="" />
                                             <?php }else{?>
                                                <img src="<?=base_url();?>images/frontend/img_news.jpg" class="post" title="" />
                                             <?php }?>
                                            </td>
                                            <td width="340" valign="top">
                                   	  		  	<div class="info">
                                                    <h3><a href="<?php echo site_url();?>news/detail/<?php echo $news['id'];?>"><?php echo $news['title'];?></a></h3>
                                                    <code><?php echo $news['ftgl'];?> /</code> <span>by <?php echo $news['createdby'];?></span>
                                                    <p><?php echo $news['shortdesc'];?></p>
                                                	<div class="readmore"><a href="<?php echo site_url();?>news/detail/<?php echo $news['id'];?>">Selengkapnya &raquo;</a></div>
                                                  <div class="clearBoth"></div>
                                                </div>
                                          	</td>
                                   	  	</tr>
                                    </table>
                                </li>
                                
                          <?php endforeach; ?>
                          
                <?php echo $this->pagination->create_links(); ?>          
               <?php else:?>
                                <li>
                                	<table width="100%">
                                        <tr>
                                            <td width="400" colspan="2">Data belum tersedia.</td>
                                   	  	</tr>
                                    </table>
                                </li>
                                <?php endif;?>
                            </ul>
                        </div><!--end list_news-->
                    </div>
                  <div class="clearBoth"></div>
                </div><!--end about-->
                
                <!--end pagiNation
                <div class="pagiNation">
                    <ul>
                        <li class="brdr_radius_left"><p>&laquo; Halaman:</p> </li>
                        <li class="pagiNation_active"><p>1</p></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li class="brdr_radius_right"><a href="#">Next &raquo;</a></li>
                        <span>1/5 Page</span>
                    </ul>
                </div> --><!--end pagiNation-->
                
                
              <div class="clearBoth"></div>
            </div><!--end content-->