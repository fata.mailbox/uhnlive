<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->

<!--content-->
            <div class="content" style="width:572px;">
            	<!--about-->
                <div class="about" style="width:560px; padding:5px; border-color:#dfe8ec; background:none;">
                	<div class="aboutCont">
                    	<h2>Culture "Budaya"</h2><hr />
                        <ul style="list-style:decimal inside; margin:0 0 10px 0px; padding:0;font-size:20px; text-align:left; line-height:170%;">
                            <li>Kerja Sama yang memiliki Komitmen Tinggi</li>
                            <li>Pelayanan Prima kepada Pelanggan</li>
                            <li>Pemrakarsaan Cara Baru dalam Menjalankan Usaha</li>
                            <li>Dedikasi dan Produktivitas</li>
                            <li>Perlakuan yang Adil dan Penghargaan atas Prestasi</li>
                            <li>Perjuangan demi Hasil Optimal</li>
                            <li>Integritas, Kejujuran dan Disiplin</li>
                        </ul>
                    </div>
                  <div class="clearBoth"></div>
                </div><!--end about-->
                
              <div class="clearBoth"></div>
            </div><!--end content-->