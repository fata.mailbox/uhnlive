<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->


<style type="text/css">
  .hide { display:none; padding:0 0 0 0px; background:red; }
  .show { display:block; }  
  .hover { cursor:pointer; color:#0099FF; font-weight:900; text-decoration:underline; }
</style>
<script type="text/javascript">
  function toggleView (id) {    
  div1 = document.getElementById('details' + id);        
    div1.className = (div1.className == 'show')?
    div1.className = 'hide':div1.className = 'show';         
  return true;
}
</script>

<div class="submenuContent">
          <div class="headTitleBg"><h6>Register</h6></div>
          <ul class="submenuUL">
          Untuk menjadi member atau anggota UNIHEALTH , silahkan anda melakukan register.<br />
                <br />              
              <div style="text-align:center"><a href="<?php echo site_url();?>register/member/"><img src="<?php echo base_url();?>images/frontend/btn-register.gif" width="130" height="35" /></a>&nbsp;&nbsp;</div>
              </ul>
        </div>
        <div class="detailContent">
          <div class="headTitleContent"><h6>FAQ</h6>
          </div>
          <!-- Blok Tentang UNIHEALTH -->
          <p><strong>5 alasan memilih UNIHEALTH</strong><br />
       	    <ul class="faq">
              <li><span class="hover" onclick="toggleView(1)">1. Perusahaan MLM Indonesia</span>
                <div class="hide" id="details1" style="overflow:auto"><p>PT UNIVERSAL HEALTH NETWORK  dengan brand UNIHEALTH NETWORK adalah perusahaan distribusi dengan system Multi Level Marketing (MLM) yang didirikan oleh, dari, untuk dan di Indonesia.</p>
<p>UNIHEALTH NETWORK adalah satu-satu nya perusahaan MLM yang produk-produk nya di buat dalam Group perusahaan sendiri yaitu SOHO Group. SOHO Group adalah perusahan yang berkomitment dalam menyediakan produk dan jasa kesehatan berkualitas tinggi yang berbahan alami (herbal).</p>
<p>Pabrik SOHO Group terletak di Kawasan Industri Pulogadung, dan memiliki total lebih dari 3500 karyawan. Sehingga bisa dipastikan kemajuan UNIHEALTH NETWORK adalah kemajuan kita bersama. Bravo Indonesia!!!</p>
</div>
              </li>
              <li><span class="hover" onclick="toggleView(2)">2. Member of SOHO Group</span>
                <div class="hide" id="details2" style="overflow:auto"><p>UNIHEALTH NETWORK merupakan salah satu unit usaha di bawah SOHO Group. SOHO Group adalah termasuk salah satu Perusahaan farmasi 3 (tiga) terbesar di Indonesia (Majalah SWA edisi Januari 2009).</p>
<p>SOHO Group memiliki komitmen untuk menjadi Group Perusahaan Global Terkemuka dalam bidang Manufaktur, Distribusi dan menyediakan produk dan jasa kesehatan berkualitas tinggi Distribusi SOHO Group kini telah merambah dunia internasional.</p>
<p>SOHO Group telah mendapatkan <b>Hall of Fame Winner 2007</b> untuk <b>strategy execution</b> menggunakan kerangka Balance Scorecard (BSC). Ini adalah penghargaan untuk pengakuan publik pada perusahaan pengguna BSC dan <b>Strategy Focused Organization</b> di seluruh dunia. SOHO Group merupakan <b>perusahaan pertama di Indonesia dan salah satu Industri farmasi pertama di Asia</b> yang menerima penghargaan prestisius tersebut.</p>
</div>
              </li>
              
              <li><span class="hover" onclick="toggleView(3)">3. Manajemen yang Profesional</span>
                <div class="hide" id="details3" style="overflow:auto"><p>Management UNIHEALTH NETWORK berlatar belakang dan memiliki pengalaman dalam Industri Multi Level Markekting (MLM). Sedangkan induknya, SOHO Group berpengalaman dalam produk-produk kesehatan sejak 1948 dan memiliki system pengelolaan perusahaan yang professional serta tersertifikasi ISO 9000:2001.</p>
<p>Manajemen Memiliki tujuan yang mulia dan tulus yaitu agar semakin banyak masyarakat Indonesia yang mampu meraih dan menikmati suatu taraf kehidupan yang lebih baik, secara kesehatan, kecantikan maupun ekonomi keluarganya.</p>
</div>
              </li>
              
              <li><span class="hover" onclick="toggleView(4)">4. Produk Berkualitas</span>
                <div class="hide" id="details4" style="overflow:auto"><p>SOHO Group adalah produsen produk-produk kesehatan sejak tahun 1948. Sesuai dengan Visi UNIHEALTH NETWORK yaitu berkomitment untuk menyediakan secara terus menerus produk kesehatan yang berkualitas tinggi melalui system MLM baik secara lokal maupun internasional.</p>
<p>Produk-produk SOHO yang dipasarkan secara konvensional dan ethical (resep) menjadi pemimpin pasar (market leader) seperti Curcuma, Curcuma plus emulsion, Diapet, Lelap, Laxing, AstmaSOHO, Culvit, Imboost dan puluhan produk lainnya.</p>
</div>
              </li>
              <li><span class="hover" onclick="toggleView(5)">5. Marketing Plan yang FAIR</span>
                <div class="hide" id="details5" style="overflow:auto"><i>Fair untuk member & fair untuk perusahaan.</i>
<p>UNIHEALTH NETWORK menggunakan system MLM Murni/ Tradisional yang sederhana, mudah dipahami dan dijalankan. Sistem ini menguntungkan bagi semua pihak, Perusahaan & seluruh membernya. Bukan sistem “money game” , skema piramida ataupun system curang yang tersembunyi lainnya yang sangat merugikan nama baik industri MLM.</p>
<p>Marketing plan Unihealth telah teruji dan sesuai dengan kode etik APLI dan Peraturan Pemerintah (Dept Perdagangan RI ), Permendag No.32/2008). Setiap posisi yang berhasil di raih oleh member memberikan kepastian penghasilan tertentu.</p>
</div>
              </li>
              
            </ul>
          </p>
          <!-- Blok Produk UNIHEALTH -->
		  <p><strong>Produk LEGRES</strong><br />
       	    <ul class="faq">
              <li><span class="hover" onclick="toggleView(6)">Apa itu RADIKAL BEBAS?</span>
                <div class="hide" id="details6" style="overflow:auto"><p>Radikal bebas dapat menyebabkan kerusakan bagian sel tubuh sehingga menyebabkan munculnya berbagai jenis penyakit seperti tumor, kanker, arterosklerosis, jantung, katarak, keriput, penuaan dini dll.</p>
<p>Sumber radikal bebas berada disekeliling kita 24 jam sehari 7 hari seminggu seperti polusi industri, kendaraan, asap rokok, komputer, handphone, makanan, obat-Obat an  dll.</p>

</div>
              </li>
              <li><span class="hover" onclick="toggleView(7)">Apa itu Antioksidan?</span>
                <div class="hide" id="details7" style="overflow:auto">Antioksidan merupakan zat penangkal radiasi akibat dari radikal bebas. Tubuh memiliki antioksidan enzim namun dalam jumlah yang sedikit, sehingga kekuarangannya harus ditutupi oleh asupan makanan yang mengandung antioksidan.</div>
              </li>
              <li><span class="hover" onclick="toggleView(8)">Apa itu LEGRES?</span>
                <div class="hide" id="details8" style="overflow:auto">LEGRES adalah antioksidan yang teruji klinis memiliki kekuatan sampai 50 kali lebih besar dibandingkan Vitamin C,E dan B-Karoten. LEGRES  diserap 3 x lebih mudah dibanding antioxidant lain.</div>
              </li>
              <li><span class="hover" onclick="toggleView(9)">Apa bedanya LEGRES dengan antioksidan lain seperti Vitamin C, E & B-karoten?</span>
                <div class="hide" id="details9" style="overflow:auto">Beda sekali. LEGRES terdiri dari 2 kandungan aktif  alami yaitu <i>Leucoselect Phytosome</i> yang diambil dari ekstrak biji anggus <i>(grape seed)</i> dan <i>Lycopene</i> dari <i>karetonoid</i> tomat merah.</div>
              </li>
              <li><span class="hover" onclick="toggleView(10)">Apa bedanya LEGRES dengan biji anggur yang biasa?</span>
                <div class="hide" id="details10" style="overflow:auto">LEGRES dapat diserap 3 kali lipat lebih baik dibanding <i>Grape Seed extract</i> biasa karena merupakan gabungan dengan <i>Phytosome</i>.</div>
              </li>
              
              <li><span class="hover" onclick="toggleView(11)">Atas dasar apa LEGRES mengklaim sebagai “The Most Powerful Antioxidant Among Others”?</span>
                <div class="hide" id="details11" style="overflow:auto"><p>LEGRES dengan kandungan <i>Leucoselect Phytosome</i> dan <i>Lycopene</i> memiliki kekuatan antioksidan yang jauh lebih besar dari antioksidan yang ada seperti Vitamin C, E dan B-Karoten. Telah banyak penelitian dan sudah teruji klinis.</p>                            
<p><i>Wells</i>, Gale Encyclopedia of Alternative Medicine 2001, ->  bahwa Grape Seed extract (Leukoselect Pytosome) memiliki kekuatan 50 kali lebih baik disbanding Vit E & 20 kali lebih bai disbanding Vit C.<br /><br />
<i>Bratman</i>, Natural Health Bible 2000 -> sebagai antioksidan Lycopene berkisar dua kali lebih besar dibanding beta carotene (Vit A)<br /><br />
<i>Bachi</i>, Toxicology 2000, Aug 7;148 (2-3): 187-97 -> Hasil menunjukan bahwa grape seed extract memberikan perlindungan yang significant terhadap radikal bebas dan perioksidasi lipid serta terhadap DNA disbanding dengan Vit C,E & A. Juga melindungi  terhadap pemakaian berlebihan obat acetaminophen, radiasi ultraviolet & pankreatitis.<br /><br />
<i>Hirnoven</i>, Stroke 2000 Oct -> Flavonoid yang berasal dari barriers dan Vit E sama sekali tidak menurunkan resiko stroke, sedangkan Lycopene terbukti dapat menurunkan resiko stroke infark serebral dan perdarahan intracerebral.<br /><br />
<i>Bagchi</i>, Free Radic Biol Med 1999 -> Grape seed extract terbukti memberikan perlindungan dari asap 
tembakau yang lebih baik disbanding Vit C & E<br /><br />
<i>Bagchi</i>, Gen Pharmacol 1998 May -> Grape seed extract memberikan perlindungan terhadap kerusakan DNA dan peroksidasi lipid yang lebih baik disbanding Vit C,E & A.<br /><br />
<i>Bagchi</i>, Res Commun Mol Pathol Pharmacol 1997 -> Grape seed extract adalah penangkap radikal bebas yang lebih potential disbanding Vit C & E. </p>
</div>
              </li>
              
              <li><span class="hover" onclick="toggleView(12)">Apa yang dimaksud dengan “The French Paradox”?</span>
                <div class="hide" id="details12" style="overflow:auto"><p>Istilah “The French Paradox” menjadi popular sejak tahun 1991 pada stasiun TV CBS di Amerika pada acara “60 Minutes” melaporkan adanya ketidaksesuaian antara pola hidup disbanding rata-rata penyakit jantung pada bangsa Prancis.<br />
Walaupun sehari-hari memakan makanan yang banyak mengandung lemak seperti mentega, keju, telur, dan sosis yang 15% dari total kalori mengandung lemak jenuh dan rendah olah raga.</p>
<p>Tetapi kenyataannya rata-rata penyakit jantung bangsa Prancis hanya 40% dibandingkan dengan bangsa amerika. Hal tersebut dikarenakan bangsa Prancis memiliki kebiasaan meminum anggur merah (red wine) secara teratur setelah makan.</p>
<p>Setelah dilakukan penelitian ternyata dalam anggur merah mengandung komponen phenolic yang mempunyai efek antioksidan kuat dengan efektifitas lebih baik 20-50 kali lipat dibanding Vitamin C,E dan B Karoten.</p>
</div>
              </li>
              
              <li><span class="hover" onclick="toggleView(13)">Apakah anggur putih (white wine) juga mempunyai efek antioksidan seperti anggur merah?</span>
                <div class="hide" id="details13" style="overflow:auto">Tidak, karena dalam pembuatan anggur putih yang diproses hanya daging dari buah nya saja. Sedangkan anggur merah menggunakan kulit, biji, daging dan batang anggur. Dan menurut penelitian, biji anggurlah yang paling banyak mengandung khasiat antioksidan.</div>
              </li>
              
              <li><span class="hover" onclick="toggleView(14)">Apakah LEGRES dapat melindungi sel-sel tubuh dari kerusakan akibat pemakaian obat-obatan?</span>
                <div class="hide" id="details14" style="overflow:auto"><p>LEGRES terbukti dapat melindungi organ-organ tubuh (lebih baik dibandingkan Vitamin C, E dan B karoten) dari kerusakan akibat pemakaian obat-obatan seperti :</p>
•	Acetaminophen (Paracetamol) yang bersifat hepatotoksik<br />
•	Amiodarone yang bersifat pulmotoksik<br />
•	Doxorubicin yang bersifat kardiotoksik<br />
•	Idarubicin<br />
•	Hydroxycyclophosphamide<br />
•	Cadmium chloride yang bersifat nefrotoksik<br />
•	Dimetilnitrosamin yang bersifat splenotoksik<br />
•	Dipropyl Phoshorodithioate yang bersifat neurotoksik
</div>
              </li>
              
              <li><span class="hover" onclick="toggleView(15)">Apakah LEGRES bermanfaat untuk kasus-kasus neurology/ syaraf?</span>
                <div class="hide" id="details15" style="overflow:auto"><p>Beberapa penelitian terhadap kasus –kasus neurology LEGRES berperan dalam proses penyembuhan. Berikut adalah beberapa penelitian yaitu :</p>
                <b>Rao</b>, Nutr Neurosci 2002 : Peranan Lycopene terhadap penyakit  neurodegenerative Amyotropic Lateral Sclerosis (ALS), cerebellar disorders, Parkinson, Huntington, Cortical, Alzeimer dan Schizophrenia.<br />
<b>Muller</b>, FreeRadic Res 2002: Lycopenemeningkatkan apoptosis (perubahan sel adar dapat di fatogenesis), hal ini menjelaskan efek perlindungan Lycopene dari kanker pada manusia,<br />
<strong>Saganuma</strong>, J Nutr Sci Vitaminol (Tokyo) 2002 : Lycopene memberikan pencegahan pada Parkinson.<br />
<strong>Polidori</strong>, Free Radic Res 2002 : Penelitian terhadap 28 penderita post stroke iskemik akut (TIA). Disimpulkan bahwa setelah stroke iskemik terjadi peningkatan stress oksidatif .<br />
<strong>Roychowdhury</strong>, Nitric Oxide 2001 : Grape seed extract mampu memberikan perlindungan terhadap sel glial.<br />
<strong>Sun</strong>, J BiomedSci 2001 : Antioxidan poliphenolic khususnya grape seed extract  dapat melindungi otak dari kerusakan syaraf akibat etanol.<br />
<br />, Stroke 2000 : Flavonoid dan Vitamin E sama sekali tidak menurunkan resiko stroke, sedangkan Lycopene terbukti dapat menurunkan resiko stroke infark cerebral dan dan perdarahan intraserebral.
</div>
              </li>
              
              <li><span class="hover" onclick="toggleView(16)">Apakah peranan LEGRES untuk penyakit kronik?</span>
                <div class="hide" id="details16" style="overflow:auto"><p>Beberapa penelitian pada penyakit kronik yaitu :</p<strong>>Lasheras</strong>, Free Radic Res 2002 : penelitian terhadap 160 orang usia lanjut, bahwa dengan kadar Lycopene tinggi memiliki resiko sangat rendah terhadap kerusakan oksidatif (74-75%) yang bias menybabkan penyakit kanker dan jantung.<br>
<strong>Kalin</strong>, Free Radic Res 2002 : Grape seed extract mengurangi peradangan dan stress oksidatif yang timbul pada pasien sistemik sclerosis.<br>
<strong>Fairfield</strong>, JAMA 2002: Evaluasi beberapa vitamin untuk pencegahan penyakit kronis pada orang dewasa. Lycopene dapat menurunkan resiko kanker prostate.<br>
<strong>Preuss</strong>, Ann NY Acad Sci 2002 : Pemberian grape seed extract mengurangi pembentukan radikal bebas dan mengurangi gejala penyakit kronis yang berhubungan dengan usia termasuk syndrome X.<br>
<strong>Banerjee</strong>, Digestion 2001 : Grape seed extract pada penyakit pankreatitis mampu mengurangi frequensi dan intensitas yeri perut dan mengurangi muntah pasien.
                </div>
              </li>
              <li><span class="hover" onclick="toggleView(17)">Apakah LEGRES dapat membantu kasus-kasus infertilitas?</span>
                <div class="hide" id="details17" style="overflow:auto">Penelitian membuktikan dari 30 pria penderita infertilitas setelah diberikan LEGRES  2 X 2 mg selama 3 bulan terjadi peningkatan konsentrasi sperma 67%, motilitas sperma 73% dan morfologi sperma 63%.</div>
              </li>
              <li><span class="hover" onclick="toggleView(18)">Bagaimana peranan LEGRES terhadap kulit dan radiasi Ultraviolet?</span>
                <div class="hide" id="details18" style="overflow:auto"><p>Dari beberapa penelitian disebutkan :</p>
Stahl, Skin Pharmacol Appl Skin Physiol 2002 : Lycopene memberikan perlindungan optimal terhadap sinar Ultraviolet.<br>
Khanna, Free Radic Biol Med 2002 : Grape seed extract mempercepat penyembuhan luka dikulit.<br>
Cristoni, Vol.I,n,2 Acta Phytotherapeutica Grape seed extract melindungi kulit dari sinar Ultraviolet.
</div>
              </li>
              <li><span class="hover" onclick="toggleView(19)">Apakah LEGRES aman?</span>
                <div class="hide" id="details19" style="overflow:auto">Menurut penelitian, karena berbahan dasar alami dari nabati LEGRES aman dikonsumsi bahkan dalam jumlah pemakaian yang banyak. LEGRES telah memiliki izin edar dengan nomor registrasi POM SD 021 301 541. Maka tidak perlu khawatir dengan keamanan LEGRES.</div>
              </li>
              
            </ul>
          </p>
          <!-- Blok Keanggotaan UNIHEALTH -->
          <p><strong>Produk LEGRESKIN</strong><br />
       	    <ul class="faq">
              <li><span class="hover" onclick="toggleView(20)">Apa yang membuat terjadinya penuaan dini?</span>
                <div class="hide" id="details20" style="overflow:auto">•	Faktor yang berhubungan dengan Ultraviolet (UV), sinar x, polusi kendaraan & pabrik, freon, asap rokok, bahan kimia endogen & eksogen, makanan tinggi karbohidrat dan kalori.<br>
•	Faktor penyebab terjadinya kekeringan seperti cara perawatan kulit yang salah, kosmetik yang tidak sesuai, kelembaban udara yang rendah, ruang ber –AC, kipas angin, suhu dingin, dan panas akan mempercepat penguapan air dari kulit sehingga menyebabkan kulit kering.<br>
•	Faktor-faktor lain yaitu gizi buruk, penyakit menahun, minuman keras, kopi berlebihan dan stres
</div>
              </li>
              <li><span class="hover" onclick="toggleView(21)">Apa itu RADIKAL BEBAS?</span>
                <div class="hide" id="details21" style="overflow:auto"><p>Radikal bebas dapat menyebabkan kerusakan bagian sel tubuh sehingga menyebabkan munculnya berbagai jenis penyakit seperti tumor, kanker, arterosklerosis, jantung, katarak, keriput, penuaan dini dll.</p>
<p>Sumber radikal bebas berada disekeliling kita 24 jam sehari 7 hari seminggu seperti polusi industri, kendaraan, asap rokok, komputer, handphone, makanan, obat-Obat an  dll.</p>
</div>
              </li>
              <li><span class="hover" onclick="toggleView(22)">Bagaimana Antioksidan dapat menjadi anti aging? </span>
                <div class="hide" id="details22" style="overflow:auto">Antioksidan bekerja menangkap radikal bebas yang ada dalam kulit dan membuangnya bersama kotoran tubuh. Selain itu antioksidan juga melindungi protein atau asam amino yang membentuk kolagen dan elastin sehingga kesehatan kulit terjaga.</div>
              </li>
              <li><span class="hover" onclick="toggleView(23)">Apa itu LEGRESKIN?</span>
                <div class="hide" id="details23" style="overflow:auto">LEGRESKIN merupakan kombinasi efektif dari Lycoselect phytosome & Lycopene yang merupakan antioksidan yang teruji klinis memiliki kekuatan sampai 50 kali lebih besar dibandingkan Vitamin C,E dan B-Karoten, Collagen, Marine Protein Extract & Vitamin E.</div>
              </li>
              <li><span class="hover" onclick="toggleView(23)">Apa fungsi dari kandungan yang terdpat pada LEGRESKIN?</span>
                <div class="hide" id="details23" style="overflow:auto"><strong>Marine Protein & Collagen</strong><br>
•	Nutrisi dan asam amino untuk pembentukan protein kulit<br>
•	Melembutkan, melembabkan dan memudakan kembali kulit kering<br>
•	Mengencangkan & menjaga elastisitas kulit<br>
•	Mengurangi flek hitam
<br><br>
<strong>Lycopene&Leucoselect Phytosome</strong><br>
•	Memiliki kekuatan anti oksidan sampai 50 kali lebih besar dari Vit E,C & B caroten<br>
•	Mencegah kerusakan sel akibat radikal bebas<br>
•	Memperlambat proses penuaan<br>
•	Menghambat keriput<br>
•	Melindungi kulit dari sinar Ultra Violet (UV)<br>
•	Bersifat hidrofilik dan lipofilik<br>
<br>
<strong>Vitamin E</strong><br>
•	Meningkatkan stabilitas dan pemasukan selular lycopene<br>
•	Antioksidan<br>
•	Membantu pembentukan jaringan kolagen<br>
•	Menghaluskan kulit<br>
</div>
              </li>
                            <li><span class="hover" onclick="toggleView(24)">Apa manfaat LEGRESKIN ?</span>
                <div class="hide" id="details24" style="overflow:auto">Peranan LEGRESKIN adalah mengikat Radikal Bebas dan memperbaiki struktur matriks ekstravaskular sehingga mencegah terjadinya<br>
•	Penuaan dini.<br>
•	Tampak awet muda<br>
•	Menghilangkan keriput<br>
•	Kerusakan kulit akibat radiasi sinar Ultraviolet<br>
•	Penyakit Kardiovaskuler<br>
•	Berbagai macam kanker<br>
•	Memperbaiki elastisitas dan kekenyalan kulit<br>
</div>
              </li>

              <li><span class="hover" onclick="toggleView(25)">Bagaima aturan pakai LEGRESKIN?</span>
                <div class="hide" id="details25" style="overflow:auto">Untuk pemeliharaan LEGRESKIN dikonsumsi 1 – 2 kaplet per hari. Dan untuk pemulihan di konsumsi 2-3 kaple per hari.</div>
              </li>
              <li><span class="hover" onclick="toggleView(26)">Apakah LEGRESKIN aman?</span>
                <div class="hide" id="details26" style="overflow:auto">Sangat aman karena terbuat dari bahan nabati alami, sehingga kelebihan pemakaian pun tidak memiliki efek samping. LEGRESKIN telah memiliki izin edar dengan nomor registrasi POM SD 051 521 491. Maka tidak perlu khawatir dengan keamanan LEGRESKIN.</div>
              </li>

            </ul>
          </p>
        </div>
        <div class="rightContent">
<?php $this->load->view('index/right_customer');?>
<?php $this->load->view('index/member_of_soho');?>
        </div>
        
