<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
        
    	Yahoo Messenger	: qtakwa@yahoo.com
-->

<!--content-->
            <div class="content" style="width:572px;">
            	<!--about-->
                <div class="about" style="width:560px; padding:5px; border-color:#dfe8ec; background:none;">
                	<div class="aboutCont">
                    	<h2>About Us</h2><hr />
                        
                    	<p class="bold">Selamat dan terima kasih telah bergabung dalam bisnis MLM <strong>PT. Universal Health Network</strong>!</p>
                        <p>Kami adalah perusahaan yang mengedepankan kualitas atas produk-produk yang berhubungan dengan kesehatan dan kecantikan serta mengajak segenap masyarakat dari berbagai golongan dan latar belakang untuk meraih kesuksesan dan kesejahteraan bersama.</p>
                        <p><strong>PT. UNIVERSAL HEALTH NETWORK</strong> yang didirikan pada tanggal 6 April 2009 ini merupakan <strong>"Member of SOHO Group"</strong>. Seperti diketahui bahwa SOHO Group adalah salah satu perusahaan farmasi terbesar di Indonesia dan telah berdiri lebih 50 tahun yang lalu. Produk-produk yang dihasilkan sangat populer dan diterima dengan baik oleh masyarakat Indonesia, bahkan kini telah merambah ke luar negeri. </p>
                        <p>Dengan latar belakang manajemen yang berpengalaman dalam bidang produk-produk kesehatan dan sistem pengelolaan perusahaan yang profesional serta tersertifikasi ISO 9000:2001, kami memberanikan diri untuk ambil bagian meramaikan industri MLM dengan tujuan mulia dan tulus agar semakin banyak masyarakat Indonesia mampu meraih dan menikmati suatu taraf kehidupan yang lebih baik, baik secara kesehatan, kecantikan maupun secara taraf kehidupan ekonomi keluarganya. Kami berhasrat membawa setiap member dan konsumen untuk mencapai kehidupan yang lebih sehat secara jasmani dengan CARA mengkonsumsi produk-produk kesehatan yang kami sediakan. Kami menawarkan produk-produk yang pasti memberi manfaat nyata bagi setiap penggunanya serta dengan harga yang relatif terjangkau bagi masyarakat luas. </p>
                        <p>Sistem pemasaran, atau yang lebih dikenal dengan Marketing Plan MLM yang kami sediakan menganut sistem MLM tradisional atau MLM Murni yang mengutamakan keuntungan bagi semua pihak, baik perusahaan mau pun seluruh anggotannya. Sistem MLM kami jauh dari sistem "money game", atau skema piramida atau pun sistem curang yang tersembunyi lainnya yang sangat merugikan nama baik industri MLM. </p>
                        <p>Segenap pimpinan dan manajemen <strong>PT. UNIVERSAL HEALTH NETWORK</strong> mengucapkan puji syukur kepada Tuhan Yang Maha Esa atas berdirinya perusahaan ini. Dengan sikap tulus dan tujuan luhur demi ikut memajukan bangsa Indonesia, kami berterima kasih kepada Pemerintah Republik Indonesia, dalam hal ini Departemen Perdangan RI yang telah memberikan <strong>izin usaha penjualan berjenjang atau SIUP-L</strong>, serta kepada <strong>Asosiasi Penjualan Langsung Indonesia (APLI)</strong> yang turut memberi dukungan terhadap kemajuan industri MLM di Indonesia. </p>
                        <p>Kini, saatnya kami dengan tangan terbuka penuh hangat mengajak seluruh masyarakat luas untuk bergabung dan mencapai kesuksesan bersama dengan menjadi anggota MLM PT. UNIVERSAL HEALTH NETWORK untuk kehidupan yang lebih baik.Dengan menyatukan visi dan misi melalui sikap dalam nilai-nilai budaya yang tersebut di atas, menjadi modal dan kepercayaan diri bahwa kesuksesan dan kehidupan lebih baik pasti akan bisa diraih.</p>
                    </div>
                  <div class="clearBoth"></div>
                </div><!--end about-->
                
              <div class="clearBoth"></div>
            </div><!--end content-->