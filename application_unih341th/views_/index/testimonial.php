<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->

            <!--content-->
            <div class="content" style="width:572px;">
            	<!--about-->
                <div class="about" style="width:560px; padding:5px; border-color:#dfe8ec; background:none;">
                	<div class="aboutCont">
                    	<h2 style="color:#15384f;">Testimonial</h2><hr />
                        <!--list_testi-->  
                        <div class="list_testi">
                        	<ul>
                            <?php if($results): 
				foreach($results as $testi): ?>
                                <li>
                                	<table width="100%">
                                        <tr>
                                            <td width="125" valign="top">
                                            	<img src="<?php echo base_url();?>userfiles/thumbnail/<?php echo $testi['thumbnail'];?>" class="post" title="Click to enlarge" />
                                            </td>
                                            <td width="375" valign="top">
                                   	  		  	<div class="info">
                                                    <h3><a href="<?php echo site_url();?>testi/index/<?php echo $testi['id'];?>/<?php echo $this->uri->segment(4);?>"><?php echo $testi['title'];?></a></h3>
                                                    <p><?php echo $testi['shortdesc'];?></p>
                                                	<div class="readmore"><a href="<?php echo site_url();?>testi/index/<?php echo $testi['id'];?>">Selengkapnya &raquo;</a></div>
                                                  <div class="clearBoth"></div>
                                                </div>
                                          	</td>
                                   	  	</tr>
                                    </table>
                                </li>
                                <?php endforeach;?>
                                <li>
                                	<table width="100%">
                                        <tr>
                                            <td width="500" valign="top"><?php echo $this->pagination->create_links(); ?></td>
                                   	  	</tr>
                                    </table>
                                </li>
              
              <?php else: ?>
                                <li>
                                	<table width="100%">
                                        <tr>
                                            <td width="4oo" valign="top">Data belum tersedia.</td>
                                   	  	</tr>
                                    </table>
                                </li>
                                <?php endif;?>
                            </ul>
                        </div><!--end list_testi-->
                    </div>
                  <div class="clearBoth"></div>
                </div><!--end about-->
                
                <!--end pagiNation
                <div class="pagiNation">
                    <ul>
                        <li class="brdr_radius_left"><p>&laquo; Halaman:</p> </li>
                        <li class="pagiNation_active"><p>1</p></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li class="brdr_radius_right"><a href="#">Next &raquo;</a></li>
                        <span>1/5 Page</span>
                    </ul>
                </div>--><!--end pagiNation-->
                
                
              <div class="clearBoth"></div>
            </div><!--end content-->
