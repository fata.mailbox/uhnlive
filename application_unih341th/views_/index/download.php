<!--content-->
            <div class="content" style="width:572px;">
            	<!--about-->
                <div class="about" style="width:560px; padding:5px; border-color:#dfe8ec; background:none;">
                	<div class="aboutCont">
                    	<h2 style="color:#15384f;">Download</h2><hr />
                        <style type="text/css">
                            .downloadTable { padding:0px; }
                            .downloadTable tr.row1 { background:#effcdf; }
                            .downloadTable tr.row2 { background:#e2fcc2; }
                            .downloadTable tr.row1:hover, tr.row2:hover { background:#fff; }
                            .downloadTable td { border-bottom:solid 1px #d3e4c7; padding:10px 5px; }
						</style>
                        <table class="downloadTable" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <?php if($results): 
          foreach($results as $key => $row){
          if($key%2 == 0)$class = "row1";
          else $class = "row2";?>
          
          <tr class="<?php echo $class;?>">
                                <tr class="<?php echo $class;?>">
                                    <td align="center" valign="top" width="13%"><a href="<?php echo site_url()."download/view/".$row['id'];?>"><img src="<?php echo base_url();?>images/frontend/<?php echo $row['type'];?>.gif" alt="<?php echo $row['type'];?>" title="<?php echo $row['type'];?>" height="40" width="40"></a></td>
                                    <td align="left" width="76%"><?php echo $row['description'];?></td>
                                    <td align="center" valign="top" width="11%"><a href="<?php echo site_url()."download/view/".$row['id'];?>"><img src="<?=base_url();?>images/frontend/buttondownloads.gif" alt="download file" title="download file" height="31" width="32"></a></td>
                                </tr>
                                
                                <?php } else: ?> 	
            	<tr class="row1"><td colspan='3'>Data belum tersedia.</td></tr>
            <?php endif;?>
                                
                            </tbody>
                        </table>
                        <?php echo $this->pagination->create_links();?>
                    </div>
                  <div class="clearBoth"></div>
                </div><!--end about-->                
                
              <div class="clearBoth"></div>
            </div><!--end content-->
            