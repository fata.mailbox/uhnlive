<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->


<!--content-->
            <div class="content" style="width:572px;">
            	<!--about-->
                <div class="about" style="width:560px; padding:5px; border-color:#dfe8ec; background:none;">
                	<div class="aboutCont">
                    	<h2 style="color:#15384f;">Activation Member</h2><hr />
                        <p>Apabila anda sudah mempunyai member id &amp; activation code daftarkan segera menjadi member dengan mengisi form dibawah ini:</p>
                        <?php echo form_open('register/activation/', array('id' => 'form'));?>
                            <div class="regForm">
                                <fieldset>
                                <legend>Box Form Activation Member</legend>
                                    <ul class="formBox">
            
                                        <li>
                                            <label>Direct Upline ID</label>
                                            <?php $data = array('name'=>'placementid','id'=>'placementid','size'=>'8','maxlength'=>'8','value'=>set_value('placementid')); echo form_input($data);?>*<br>
                                            <span class="textError"><?php echo form_error('placementid');?></span>
                                        </li>
                                        
                                        <li class="li"><label>Sponsor ID</label>
                                            <?php $data = array('name'=>'introducerid','id'=>'introducerid','size'=>'8','maxlength'=>'8','value'=>set_value('introducerid')); echo form_input($data);?>*<br>
                                            <span class="textError"><?php echo form_error('introducerid');?></span>
                                        </li>
                                        
                                        <li>
                                            <label>Member ID</label>
                                            <?php $data = array('name'=>'userid','id'=>'userid','size'=>'8','maxlength'=>'8','value'=>set_value('userid')); echo form_input($data);?>*<br>
                                            <span style="margin-left:140px;">Default PIN of Member is Member ID</span><br />
                                            <span class="textError"><?php echo form_error('userid');?></span>
                                        </li>
                                        
                                        <li class="li"><label>Activation Code</label>
                                            <?php $data = array('name'=>'activation','id'=>'activation','size'=>'15','maxlength'=>'20','value'=>set_value('activation')); echo form_input($data);?>*<br>
                                            <span class="textError"><?php echo form_error('activation');?></span>
                                        </li>
                                        
                                        <li><label>Full Name</label>
                                            <?php $data = array('name'=>'name','id'=>'name','size'=>'30','maxlength'=>'50','value'=>set_value('name')); echo form_input($data);?>*<br>
                                            <span class="textError"><?php echo form_error('name');?></span>
                                        </li>
                                        
                                        <li class="li">
                                            <label>KTP Number</label>
                                            <?php $data = array('name'=>'ktp','id'=>'ktp','size'=>'17','maxlength'=>'16','value'=>set_value('ktp')); echo form_input($data);?>*<br>
                                            <span style="margin-left:140px;">Default password is 8 digits from left of KTP Number</span><br />
                                            <span class="textError"><?php echo form_error('ktp');?></span>
                                        </li>
                                        
                                        <li><label>Address</label>
                                            <?php $data = array('name'=>'alamat','id'=>'alamat','rows'=>2, 'cols'=>'30','tabindex'=>'1','value'=>set_value('alamat')); echo form_textarea($data);?>*<br>
                                            <span class="textError"><?php echo form_error('alamat');?></span>
                                        </li>
                                        
                                        <li class="li">
                                            <label>No Handphone</label>
                                            <?php $data = array('name'=>'hp','id'=>'hp','size'=>'15','maxlength'=>'15','value'=>set_value('hp')); echo form_input($data);?>*<br>
                                            <span class="textError"><?php echo form_error('hp');?></span>
                                        </li>
                                        
                                        <li><label>eMail</label>
                                            <?php $data = array('name'=>'email','id'=>'email','size'=>'30','maxlength'=>'50','value'=>set_value('email')); echo form_input($data);?><br>
                                            <span class="textError"><?php echo form_error('email');?></span>
                                        </li>
                                        <li class="li">
                                            <label>Heir / heiress</label>
                                            <?php $data = array('name'=>'ahliwaris','id'=>'ahliwaris','size'=>'30','maxlength'=>'50','value'=>set_value('ahliwaris')); echo form_input($data);?>*<br />
<span style="margin-left:140px;">(Ahli waris)</span><br />
                                            <span class="textError"><?php echo form_error('ahliwaris');?></span>
                                        </li>
                                        
                                        <li>
                                            <label>Security code</label>
                                            <span id="captchaImage"><?php echo $captcha['image']; ?></span>
                                        </li>
                                        <li class="li"><label>Confirm security code</label>
                                            <?php $data = array('name'=>'confirmCaptcha','id'=>'confirmCaptcha','autocomplete'=>'off','size'=>'10','maxlength'=>'10'); echo form_input($data);?>*<br>
                                            <span class="textError"><?php echo form_error('confirmCaptcha');?></span>
                                        </li>
                                        <li><label>&nbsp;</label>
                                            <input name="submit" value="Activation Member" class="submit" type="submit">
                                        </li>
                                    </ul>
                                </fieldset>              
                            </div>   
                        <?=form_close();?>
                    </div>
                  <div class="clearBoth"></div>
                </div><!--end about-->                
                
              <div class="clearBoth"></div>
            </div><!--end content-->
            
            