<div class="leftContent">
          <div class="news">
            <div class="headTitleBgNews"><h6>Activation Code & Register</h6></div>
            <div class="newsContent">
            <h6>Activation Code</h6>
              <p>Apabila anda sudah mempunyai member id & activation code daftarkan agar segera memjadi member.<br /><br />
              <div style="text-align:center"><a href="<?php echo site_url();?>register/activation/"><img src="<?php echo base_url();?>images/frontend/btn-activation.gif" width="154" height="41" /></a></div>
                  <br />
            </p>
            <br />
              <h6>Belum terdaftar ?</h6>
              <p>Jika anda belum terdaftar menjadi member UNIHEALTH Network silahkan anda mendaftar dengan mengklik tombol dibawah ini:<br /><br />
              <div style="text-align:center"><a href="<?php echo site_url();?>register/member/"><img src="<?php echo base_url();?>images/frontend/btn-register.gif" width="154" height="41" /></a></div>
                  <br />
            </p>
            </div>
          </div>
          </div>
        <div class="centerContent">
          <div class="headTitleContact">
            <h6>Login</h6>
          </div>
          <div class="prodCont">
        	<p><h3 style="padding-bottom:5px;">Login Member</h3>
       	    Jika anda sudah terdaftar menjadi member UNIHEALTH Network, silahkan anda login menggunakan form berikut ini:</p>
        	<br />
            <ul class="contactus">  
		  	<li>&nbsp;</li>
            <?php echo form_open('login/member/', array('id' => 'form'));?>
            
            <table width='100%' align="center">
		<tr>
			<td width='20%' valign='top'><span>Username</span></td>
			<td width='3%' valign='top'>&nbsp;</td>
			<td width='77%'><li><input type="text" name="username" id="username" autocomplete="off" value="" maxlength="50" size="20" /></li>
			<div class="errorMessage"><?php echo form_error('username');?></div></td>
		</tr>
		<tr>
			<td valign='top'><span>Password</span></td>
			<td valign='top'>&nbsp;</td>
			<td><li><input type="password" name="password" id="password" autocomplete="off" value="" maxlength="100" size="20" /></li><div class="errorMessage"><?php echo form_error('password');?></div></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td><?php echo form_submit('submit', 'login');?></td> 
		</tr>	
        <tr>
			<td colspan="3">&nbsp;</td>
		</tr>	
        
		</table>
<?php echo form_close();?>

</ul>
          </div>
  <div class="prodCont">
        	<p><h3 style="padding-bottom:5px;">Login Stockiest / M-Stockiest</h3>
       	    Jika anda sudah terdaftar menjadi stockiest UNIHEALTH Network, silahkan anda login menggunakan form berikut ini:</p>
        	<br />
            <ul class="contactus">  
		  	<li>&nbsp;</li>
            <?php echo form_open('login/stockiest/', array('id' => 'form'));?>
            <table width='100%' align="center">
		<tr>
			<td width='20%' valign='top'><span>Username</span></td>
			<td width='3%' valign='top'>&nbsp;</td>
			<td width='77%'><li><input type="text" name="usernamestc" id="usernamestc" autocomplete="off" value="" maxlength="50" size="20" /></li>
			<div class="errorMessage"><?php echo form_error('usernamestc');?></div></td>
		</tr>
		<tr>
			<td valign='top'><span>Password</span></td>
			<td valign='top'>&nbsp;</td>
			<td><li><input type="password" name="passwordstc" id="passwordstc" autocomplete="off" value="" maxlength="100" size="20" /></li><div class="errorMessage"><?php echo form_error('passwordstc');?></div></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td><?php echo form_submit('submit', 'login');?></td> 
		</tr>	
        <tr>
			<td colspan="3">&nbsp;</td>
		</tr>	
        
		</table>
<?php echo form_close();?>

</ul>
          </div>
        </div>
        <div class="rightContent">
        	<?php $this->load->view('index/right_customer');?>
				<?php $this->load->view('index/member_of_soho');?>
        </div>