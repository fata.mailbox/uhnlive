<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->

<!--content-->
            <div class="content" style="width:572px;">
            	<!--about-->
                <div class="about" style="width:560px; padding:5px; border-color:#dfe8ec; background:none;">
                	<div class="aboutCont">
                    	<h2 style="color:#15384f;">Office Address</h2><hr />

                        <p>Dibawah ini adalalah alamat kantor resmi <strong>PT. Universal Health Network</strong>:</p>
                        <a href="<?=base_url();?>images/frontend/gedung.jpg" class="highslide" onclick="return hs.expand(this)">
                        <img src="<?=base_url();?>images/frontend/gedung.jpg" width="200" height="300" class="imgFloatRight post" alt="" title="Click to enlarge" /></a>
                        <div style="padding-left:10px; margin:10px 0 20px 0; text-align:left; line-height:170%;">        
							<p><b>Head Office:</b><br>
							Jl. Pulo Gadung No.5<br />
                          	Kawasan Industri Pulo Gadung<br />
                            Jakarta Timur - Indonesia<br>
							Telp. 021-4605550<br>
							Fax. 021-4603808</p> 
							<p><b>Stock Point:</b><br>
							Ruko Mangga Dua Square Blok F-20<br />
                          	Jl. Gunung Sahari Raya No.1<br />
                            Jakarta Utara - Indonesia<br>
							Telp. 021-62310402<br>
							Fax. 021-62310403</p> 
                            <table border="0" cellpadding="0" cellspacing="0" width="293">
                          		
                                <tr>
                                  	<td height="30" width="109" valign="top"><b>Hotline Service</b></td>
                                  	<td valign="top" width="12"><b>:</b></td>
                                  	<td valign="top" width="162"><b style="font-size: 14px;">08571-7979-222</b></td>
                                </tr>
                                <tr>
                                    <td height="30"><b>E-Mail</b></td>
                                    <td><b>:</b></td>
                                    <td><b>info@uni-health.com<b></td>
                                </tr>
								 <tr>
                                    <td height="30"><b>Facebook</b></td>
                                    <td><b>:</b></td>
                                    <td><b><a href="http://www.facebook.com/unihealthofficial" target="_blank">facebook.com/unihealthofficial</a></b></td>
                                </tr>
								<!-- 
                                <tr>
                                    <td height="30"><b>Twitter</b></td>
                                    <td><b>:</b></td>
                                    <td><b><a href="http://www.twitter.com/uhn" target="_blank">twitter.com/uhn</a></b><td>
								</tr>
                               
                                <tr>
                                    <td height="30"><b>YM</b></td>
                                    <td><b>:</b></td>
                                    <td><a href="ymsgr:sendIM?cs1_unihealth"><img src="http://opi.yahoo.com/online?u=cs1_unihealth&amp;m=g&amp;t=2" border="0" class="yahooCont" /></a></td>
                                </tr>
                                <tr>
                                    <td height="30" colspan="2">&nbsp;</td>
                                    <td><a href="ymsgr:sendIM?cs2_unihealth"><img src="http://opi.yahoo.com/online?u=cs2_unihealth&amp;m=g&amp;t=2" border="0" class="yahooCont" /></a></td>
                                </tr>
								-->
                       	  	</table><br />
                            
                            <table border="0" cellpadding="0" cellspacing="0" width="400">
                                <tr>
                                    <td width="109" height="75" valign="top">
                                    	<a title="bca" class="post"><img src="<?=base_url();?>images/frontend/bca.png" width="80" sty/></a>
                                    </td>
                                    <td width="12" valign="top"><b>:</b></td>
                                    <td width="269" valign="top">
                                    	Atas Nama &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <b>PT. Universal Health Network</b><br />
                                        No.Rekening &nbsp;: <b style="font-size:15px;">003.305.9008</b><br />
                                        Cabang &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Gunung Sari - Jakarta.
                                    </td>
                                </tr>
                                <tr>
                                    <td height="75" valign="top">
                                    	<a title="mandiri" class="post"><img src="<?=base_url();?>images/frontend/mandiri.png" width="80"/></a>
                                    </td>
                                    <td valign="top"><b>:</b></td>
                                    <td valign="top">
                                    	Atas Nama &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <b>PT. Universal Health Network</b><br />
                                        No.Rekening &nbsp;: <b style="font-size:15px;">125.000.972.3016</b><br />
                                        Cabang &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Pulo Gadung - Jakarta.
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                    	<a title="bni" class="post"><img src="<?=base_url();?>images/frontend/bni.png" width="80"/></a>
                                    </td>
                                    <td valign="top"><b>:</b></td>
                                    <td valign="top">
                                    	Atas Nama &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <b>PT. Universal Health Network</b><br />
                                        No.Rekening &nbsp;: <b style="font-size:15px;">197-358-222</b><br />
                                        Cabang &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: JIEP - Jakarta.
                                    </td>
                                </tr>
                       	  	</table>
                            
                          <div class="clearBoth"></div>
                        </div>
                        <p>Jika Anda ingin mengetahui lebih lengkap tentang kami, menyampaikan keluhan dan atau usulan serta pertanyaan-pertanyaan lainnya sehubungan dengan bisnis MLM Unihealth, silakan mengisi kotak form yang telah kami sediakan. Kami akan menanggapinya dalam waktu 1x24 Jam dalam hari kerja. Terima kasih. <a href="<?=site_url();?>contactus/cform">klik disini...!!!</a></p>
                    </div>
                  <div class="clearBoth"></div>
                </div><!--end about-->                
                
              <div class="clearBoth"></div>
            </div><!--end content-->
    