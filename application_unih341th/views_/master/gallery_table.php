<table width="99%">
<?php echo form_open('master/gallery', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><?php echo anchor('master/gallery/create','create gallery');?></td>
		<td width='60%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>$this->validation->search);
    				echo form_input($data);?> <?=form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords: <b><?=$this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?=form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='8%'>No.</th>
      <th width='10%'>Judul</th>
      <th width='20%'>Nama File</th>
      <th width='30%'>Album</th>
	<th width='5%'>Cover</th>      
	 <th width='7%'>No. Urut</th>
      <th width='7%'>Status</th>
      <th width='10%'>ID User</th>
    </tr>
<?php
if ($results):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor('master/gallery/view/'.$row['id'], $counter);?></td>
                  <td><?php echo anchor('master/gallery/view/'.$row['id'], $row['title']." ");?></td>
                  <td><?php echo anchor('master/gallery/view/'.$row['id'], $row['filename']." ");?></td>
      
      <td><?php echo anchor('master/gallery/view/'.$row['id'], $row['album']." ");?></td>
      <td><?php echo anchor('master/gallery/view/'.$row['id'], $row['coveralbum']);?></td>
      <td><?php echo anchor('master/gallery/view/'.$row['id'], $row['nourut']);?></td>
      <td><?php echo anchor('master/gallery/view/'.$row['id'], $row['status']);?></td>

      <td><?php echo anchor('master/gallery/view/'.$row['id'], $row['createdby']);?></td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="8">Data tidak tersedia.</td>
    </tr>
<?php endif; ?>    
</table>
<?php $this->load->view('paging');?>