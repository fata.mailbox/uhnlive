<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>

<h2><?php echo $page_title;?></h2>
	<?php echo form_open('master/stc/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		<tr>
			<td width='24%'>date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td valign='top'>Stockiest ID</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="no_stc" value="<?php echo set_value('no_stc');?>" size"20" />
			<span class="error">* <?php echo form_error('no_stc');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Type</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_dropdown('type',$type);?></td> 
		</tr>
		<tr>
			<td colspan="3" style="border-bottom:solid thin #000099"></td>
		</tr>
		<tr>
			<td valign='top'>Member ID</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="member_id" readonly="1" value="<?php echo set_value('member_id');?>" size"12" />
			<?php 
				$atts = array(
				'width'      => '450',
				'height'     => '600',
				'scrollbars' => 'yes',
				'status'     => 'yes',
				'resizable'  => 'no',
				'screenx'    => '0',
				'screeny'    => '0'
				);
				echo anchor_popup('memsearch2/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
			?>
			<span class='error'>* <?php echo form_error('member_id');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Name Stockiest</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="name" readonly="1"  value="<?php echo set_value('name');?>" size"30" /></td>
		</tr>
		
		<?php // updated by Boby 20100616 ?>
		<tr>
			<td valign='top'>Phone Number</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="telp" value="<?php echo set_value('telp');?>" size"20" /></td>
		</tr>
		<tr>
			<td valign='top'>Hand Phone Number</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="hp" value="<?php echo set_value('hp');?>" size"20" /></td>
		</tr>
		<tr>
			<td valign='top'>Email</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="email" value="<?php echo set_value('email');?>" size"20" /></td>
		</tr>
		<tr>
			<td valign='top'>Stockiest Address</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $data = array('name'=>'alamat','id'=>'alamat','rows'=>2, 'cols'=>'30','tabindex'=>'1','value'=>set_value('alamat')); echo form_textarea($data);?></td>
		</tr>
		<?php // end updated by Boby 20100616 ?>
		
		<?php // updated by Boby 20100614 ?>
		<tr>
			<td colspan="3" style="border-bottom:solid thin #000099"></td>
		</tr>
		<tr>
			<td valign='top'>Leader</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="no_stc2" readonly="1" value="<?php echo set_value('no_stc2');?>" size="12" />
				<?php 
					echo form_hidden('member_id2',set_value('member_id2'));
					$atts = array(
					  'width'      => '450',
					  'height'     => '600',
					  'scrollbars' => 'yes',
					  'status'     => 'yes',
					  'resizable'  => 'yes',
					  'screenx'    => '0',
					  'screeny'    => '0'
					);
					echo anchor_popup('stcsearch/all2/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
				?>
				<span class='error'>* <?php echo form_error('member_id2');?></span>
			</td>
		</tr>
		<tr>
			<td valign='top'>Leader Name</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="name2" readonly="1"  value="<?php echo set_value('name2');?>" size"30" /></td>
		</tr>
		<tr>
			<td valign='top'>Area of Stockiest</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php
				echo form_hidden('kota_id',set_value('kota_id'));
				echo form_hidden('propinsi');
				$data = array('name'=>'city','id'=>'city','readonly'=>'1','value'=>set_value('city'));
				echo form_input($data);
				$atts = array(
					'width'      => '400',
					'height'     => '400',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'yes',
					'screenx'    => '0',
					'screeny'    => '0'
					);
				echo anchor_popup('citysearch/all/', "<img src='/images/backend/search.gif' border='0'>", $atts); 
				?>
				<span class='error'>*<?php echo form_error('kota_id'); ?></span>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="border-bottom:solid thin #000099"></td>
		</tr>
		<?php // end updated by Boby 20100614 ?>
		<tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $options = array('active' => 'active','inactive' => 'inactive');
    				echo form_dropdown('status',$options);?></td> 
		</tr>
		<tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('warehouse_id',$warehouse);?></td> 
		</tr>
		<tr>
			<td valign='top'>Password</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'passwd','id'=>'passwd','value'=>set_value('passwd'));
    			echo form_password($data);?><span class='error'>* <?php echo form_error('passwd');?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Retype Password</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'passwd2','id'=>'passwd2','value'=>set_value('passwd2'));
    			echo form_password($data);?></td> 
		</tr>
		<tr>
			<td valign='top'>PIN</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'pin','id'=>'pin','value'=>set_value('pin'));
    			echo form_password($data);?><span class='error'>* <?php echo form_error('pin');?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Retype PIN</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'pin2','id'=>'pin2','value'=>set_value('pin2'));
    			echo form_password($data);?></td> 
		</tr>
		
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>
		</table>

<?php echo form_close();?>
<?php $this->load->view('footer');?>
