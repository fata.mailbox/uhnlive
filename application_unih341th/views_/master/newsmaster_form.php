<?php
$this->load->view('header');
?>
<h2><?php echo $page_title;?></h2>
	<?php echo form_open_multipart('master/newsmaster/create');?>
		<table width='99%'>
		<tr>
			<td width='14%' valign='top'>Title News</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php $data = array('name'=>'title','id'=>'title','rows'=>2, 'cols'=>'40','class'=>'txtarea','value'=>set_value('title'));
    echo form_textarea($data);?> <span class='error'>*<?php echo form_error('title'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Short Description</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'shortdesc','id'=>'shortdesc','rows'=>2, 'cols'=>'40','class'=>'txtarea','value'=>set_value('shortdesc'));
    echo form_textarea($data);?> <span class='error'>*<?php echo form_error('shortdesc'); ?></span>	</td> 
		</tr>
        <tr>
			<td valign='top'>Image News</td>
			<td valign='top'>:</td>
			<td><input type="file" name="userfile" size="20" />
 <span class='error'>*<?php if(isset($error)) echo $error['error'];?></span>	</td> 
		</tr>
		<tr>
			<td valign='top'>Long Description</td>
			<td valign='top'>:</td>
			<td><?php $data = array(
              'name'        => 'longdesc',
              'id'          => 'longdesc',
              'toolbarset'  => 'Default',
              'basepath'    => '/fckeditor/',
              'width'       => '98%',
              'height'      => '400',
              'value'		 => set_value('longdesc')
              );
	echo form_fckeditor($data);?> <span class='error'>*<?php echo form_error('longdesc'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Promo or News?</td>
			<td valign='top'>:</td>
			<td><?php $options = array('news' => 'News & Event', 'promo' => 'Promo');
    echo form_dropdown('promo',$options,set_value('promo'));?> </td> 
		</tr>
        <tr>
			<td valign='top'>Promo or News Box?</td>
			<td valign='top'>:</td>
			<td><?php $options = array('Y' => 'Yes', 'N' => 'No');
    echo form_dropdown('newsbox',$options,set_value('newsbox'));?> </td> 
		</tr>
        <tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td><?php $options = array('active' => 'active', 'inactive' => 'inactive');
    echo form_dropdown('status',$options,set_value('status'));?> </td> 
		</tr>
		<tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><?php echo form_submit('submit','create news');?></td> 
		</tr>
		</table>
		<?php echo form_close();?>
		
<?php
$this->load->view('footer');
?>
