<table width='99%'>
<?php echo form_open('master/mp/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='7%'>No.</th>
      <th width='30%'>Category</th>
      <th width='18%'>Sort Ascending</th>
      <th width='10%'>Status</th>
      <th width='20%'>Created Date</th>
      <th width='15%'>User ID</th>
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor('master/mp/view/'.$row['id'], $counter);?></td>
      <td><?php echo anchor('master/mp/view/'.$row['id'], $row['category']);?></td>
      <td><?php echo anchor('master/mp/view/'.$row['id'], $row['sort']);?></td>
      <td><?php echo anchor('master/mp/view/'.$row['id'], $row['status']);?></td>
      <td><?php echo anchor('master/mp/view/'.$row['id'], $row['created']);?></td>
      <td><?php echo anchor('master/mp/view/'.$row['id'], $row['createdby']);?></td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
