<?php
$this->load->view('header');
?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open_multipart('master/downloadmaster/create');?>
		
		<table width='99%'>
		<tr>
			<td valign='top'>File Alias</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'alias','id'=>'alias','value'=>set_value('alias'));
    echo form_input($data);?> <span class='error'>*<?php echo form_error('alias'); ?></span>	</td> 
		</tr>
		<tr>
			<td valign='top'>Description</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'shortdesc','id'=>'shortdesc','rows'=>5, 'cols'=>'40','class'=>'txtarea','value'=>set_value('shortdesc'));
    echo form_textarea($data);?> <span class='error'>*<?php echo form_error('shortdesc'); ?></span>	</td> 
		</tr>
		<tr>
			<td valign='top'>File Download</td>
			<td valign='top'>:</td>
			<td><input type="file" name="userfile" size="20" />
 <span class='error'>*<?php if(isset($error)) print_r($error['error']);?></span>	</td> 
		</tr>
		
		<tr>
			<td valign='top'>Jenis File</td>
			<td valign='top'>:</td>
			<td><?php $options = array('Ms. Office Word' => 'Ms. Office Word', 'Ms. Office Excel' => 'Ms. Office Excel','Ms. Office Excel' => 'Ms. Office Excel','Ms. Office PowerPoint' => 'Ms. Office PowerPoint','PDF File' => 'PDF File','MP3 File' => 'MP3 File','Video File' => 'Video File');
    echo form_dropdown('jenisfile',$options,set_value('jenisfile'));?> </td> 
		</tr>
		<tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td><?php $options = array('active' => 'active', 'inactive' => 'inactive');
    echo form_dropdown('status',$options,set_value('status'));?> </td> 
		</tr>
		<tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><input type="submit" value="Submit" /></td> 
		</tr>
		</table>
		</form>
		
<?php
$this->load->view('footer');
?>
