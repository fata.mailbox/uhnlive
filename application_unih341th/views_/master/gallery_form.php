<?php
$this->load->view('header');
?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open_multipart('master/gallery/create/');?>
		
		<table width="99%">
        <tr>
			<td valign='top'>Judul</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'title','id'=>'title','value'=>set_value('title'));
    echo form_input($data);?></td> 
		</tr>
		<tr>
			<td valign='top'>Deskrisi</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'shortdesc','id'=>'shortdesc','rows'=>5, 'cols'=>'40','class'=>'txtarea','value'=>set_value('shortdesc'));
    echo form_textarea($data);?></td> 
		</tr>
		<tr>
			<td valign='top'>Image Gallery</td>
			<td valign='top'>:</td>
			<td><input type="file" name="userfile" size="20" />
 <span class='error'>*<?php if(isset($error)) print_r($error['error']);?></span>	</td> 
		</tr>
		<tr>
			<td valign='top'>Judul Album</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'album','id'=>'album','rows'=>1, 'cols'=>'40','class'=>'txtarea','value'=>set_value('album'));
    echo form_textarea($data);?><span class='error'>*<?=form_error('album');?></span></td> 
		</tr>
        <tr>
			<td valign='top'>Cover Album</td>
			<td valign='top'>:</td>
			<td><?php $options = array('1' => 'Yes', '0' => 'No');
    echo form_dropdown('cover',$options);?> </td> 
		</tr>
		<tr>
			<td valign='top'>No. Urut</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'nourut','id'=>'nourut','value'=>set_value('nourut','0'));
    echo form_input($data);?><span class='error'>*<?=form_error('nourut');?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td><?php $options = array('active' => 'active', 'inactive' => 'inactive');
    echo form_dropdown('status',$options);?> </td> 
		</tr>
		<tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><input type="submit" value="Submit" /></td> 
		</tr>
		</table>
		</form>
		
<?php
$this->load->view('footer');
?>
