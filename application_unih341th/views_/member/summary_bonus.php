<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="100%">
<?php echo form_open('member/hbonus/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
        <tr>
			<td valign='top' width="19%">Periode</td>
			<td valign='top' width="1%">:</td>
			<td width="80%"><?php echo form_dropdown('periode',$dropdown);?> to <?php echo form_dropdown('periode2',$dropdown);?></td>
		</tr>
        <tr>
			<td valign='top'>Sort by</td>
			<td valign='top'>:</td>
			<td><input type="radio" name="sort" value="nama" <?php echo set_radio('sort', 'nama', TRUE); ?> /> Nama Member (asc)
				<input type="radio" name="sort" value="amount" <?php echo set_radio('sort', 'amount'); ?> /> Total Bonus (desc)</td>
		</tr>
        <tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><?php echo form_submit('submit','preview');?></td>
		</tr>
                
         <?php echo form_close();?>
    <tr><td colspan="3"><hr /></td></tr>
	</table>
	
    <table class="stripe">
	<tr>
      <th width='10%'>No.</th>
      <th width='13%'>Member ID</th>
      <th width='35%'>Name</th>
      <th width='5%'>Level</th>
      <th width='20%'><div align="right">Sub Total</div></th>
      <th width='17%'>No. NPWP</th>
    </tr>
   
<?php
if ($results): 
	foreach($results as $key => $row): ?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['member_id'];?></td>
      <td><?php echo $row['nama'];?></td>
      <td><?php echo $row['jenjang'];?></td>
     <td align="right"><?php echo $row['fnominal'];?></td>
     <td><?php echo $row['npwp'];?></td>
    </tr>
    <?php endforeach; ?>
	<tr>
	      <td colspan="4"><b>Total Bonus Rp. </b></td>
	     	<td align="right"><b><?php echo $total['fnominal'];?></b></td>
            <td>&nbsp;</td>
    </tr>
<?php else: ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>			                
                
<?php $this->load->view('footer');?>
