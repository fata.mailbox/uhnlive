<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php echo anchor('member/memprofile/edit/'.$row['id'],'edit profile')."<br /><br />";?>
		
			<table width="100%">
            	<tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA PENDAFTARAN</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td width='20%'>Tanggal Daftar</td>
                    <td width='1%'>:</td>
					<td width='29%'><?php echo $row['ftglaplikasi'];?></td>
					<td width='20%'>Stockiest ID / Nama</td>
                    <td width='1%'>:</td>
                    <td width='29%'><?php if($row['stockiest_id'] == 'SH0000') echo "KANTOR UHN"; else echo $row['no_stc']." / ".$row['namastc'];?></td>
				</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA PRIBADI ANDA</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Member ID / Nama</td>
                    <td>:</td>
					<td><?php echo $row['id']." / ".$row['nama'];?></td>
					<td>Tempat Lahir</td>
                    <td>:</td>
                    <td><?php echo $row['tempatlahir'];?></td>
				</tr>
                <tr>
					<td>Jenis Kelamin</td>
                    <td>:</td>
					<td><?php echo $row['jk'];?></td>
					<td>Tanggal Lahir</td>
                    <td>:</td>
                    <td><?php echo $row['ftgllahir'];?></td>
				</tr>
				<tr>
					<td>Alamat</td>
                    <td>:</td>
					<td><?php echo $row['alamat'];?></td>
					<td>No. Telp</td>
                    <td>:</td>
                    <td><?php echo $row['telp'];?></td>
				</tr>
                <tr>
					<td>Kota</td>
                    <td>:</td>
					<td><?php echo $row['kota'];?></td>
					<td>No. Fax</td>
                    <td>:</td>
                    <td><?php echo $row['fax'];?></td>
				</tr>
                <tr>
					<td>Kode Pos</td>
                    <td>:</td>
					<td><?php echo $row['kodepos'];?></td>
					<td>No. HP</td>
                    <td>:</td>
                    <td><?php echo $row['hp'];?></td>
				</tr>
                <tr>
					<td>Propinsi</td>
                    <td>:</td>
					<td><?php echo $row['propinsi'];?></td>
					<td>Email</td>
                    <td>:</td>
                    <td><?php echo $row['email'];?></td>
				</tr>
                <tr>
					<td>No. KTP / SIM</td>
                    <td>:</td>
					<td><?php echo $row['noktp'];?></td>
					<td>Ahliwaris</td>
                    <td>:</td>
                    <td><?php echo $row['ahliwaris'];?></td>
				</tr>
                <tr>
					<td></td>
                    <td></td>
                    <td></td>
                    <td>No. NPWP</td>
                    <td>:</td>
					<td><?php echo $row['npwp'];?></td>
				</tr>

                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA BANK</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Nama Bank</td>
                    <td>:</td>
					<td><?php echo $row['namabank'];?></td>
					<td>Nama Nasabah</td>
                    <td>:</td>
                    <td><?php echo $row['namanasabah'];?></td>
				</tr>
                <tr>
					<td>Cabang</td>
                    <td>:</td>
					<td><?php echo $row['area'];?></td>
					<td>No. Rekening</td>
                    <td>:</td>
                    <td><?php echo $row['no'];?></td>
				</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA SPONSOR</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Member ID</td>
                    <td>:</td>
					<td><?php echo $row['enroller_id'];?></td>
					<td>Nama Sponsor</td>
                    <td>:</td>
                    <td><?php echo $row['namaenroller'];?></td>
				</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA PENEMPATAN</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Member ID</td>
                    <td>:</td>
					<td><?php echo $row['sponsor_id'];?></td>
					<td>Nama</td>
                    <td>:</td>
                    <td><?php echo $row['namasponsor'];?></td>
				</tr>
	</table>
<?php $this->load->view('footer');?>
