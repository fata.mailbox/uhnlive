<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<?php echo form_open('member/profile/edit/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
<table width="100%">
            	<tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA PENDAFTARAN</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td width='20%'>Tanggal Daftar</td>
                    <td width='1%'>:</td>
					<td width='29%'><?php echo $row['ftglaplikasi'];?></td>
					<td width='20%'>Stockiest ID / Nama</td>
                    <td width='1%'>:</td>
                    <td width='29%'><?php if($row['stockiest_id'] == 'SH0000') echo "KANTOR UHN"; else echo $row['no_stc']." / ".$row['namastc'];?></td>
				</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA PRIBADI ANDA</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Member ID / Nama</td>
                    <td>:</td>
					<td><?php echo $row['id']." / ".$row['nama'];?></td>
				  <td valign="top">Tempat Lahir</td>
                    <td valign="top">:</td>
                    <td><?php $data = array('name'=>'tempatlahir','value'=>set_value('tempatlahir',$row['tempatlahir']));
    						echo form_input($data);?> <span class='error'>*<?php echo form_error('tempatlahir'); ?></span></td>
				</tr>
                <tr>
					<td>Jenis Kelamin</td>
                    <td>:</td>
					<td><?php $options = array('Laki-laki' => 'Laki-laki', 'Perempuan' => 'Perempuan');
    echo form_dropdown('jk',$options);?></td>
					<td>Tanggal Lahir</td>
                    <td>:</td>
                    <td><?php $data = array('name'=>'date','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('date',$row['tgllahir']));
   echo form_input($data);?> (yyyy-mm-dd)</td>
				</tr>
				<tr>
				  <td valign="top">Alamat</td>
                    <td valign="top">:</td>
					<td><?php 
						if(strlen($row['noktp'])>1){
							$data = array('name'=>'alamat','readonly'=>'true','id'=>'alamat','rows'=>3, 'cols'=>'28','value'=>set_value('alamat',$row['alamat']));
    					}else{
							$data = array('name'=>'alamat','id'=>'alamat','rows'=>3, 'cols'=>'28','value'=>set_value('alamat',$row['alamat']));
						}
						echo form_textarea($data);?>
						<span class='error'>*<?php echo form_error('alamat'); ?></span>
					</td>
					<td>No. Telp</td>
                    <td>:</td>
                    <td><?php $data = array('name'=>'telp','value'=>set_value('telp',$row['telp']));	echo form_input($data);?></td>
				</tr>
                <tr>
				  <td valign="top">Kota</td>
                    <td valign="top">:</td>
					<td><?php 
					echo form_hidden('id',$row['id']); echo form_hidden('account_id',$row['account_id']);
					echo form_hidden('namanasabah',$row['nama']); 
					 echo form_hidden('kota_id', set_value('kota_id',$row['kota_id']));
					 $data = array('name'=>'city','id'=>'city','readonly'=>'1','value'=>set_value('city',$row['kota']));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('citysearch/all/', "<img src='/images/backend/search.gif' border='0'>", $atts); ?> <span class='error'>*<?php echo form_error('city'); ?></span></td>
					<td>No. Fax</td>
                    <td>:</td>
                    <td><?php $data = array('name'=>'fax','value'=>set_value('fax',$row['fax']));
    						echo form_input($data);?></td>
				</tr>
                <tr>
				  <td valign="top">Kode Pos</td>
                    <td valign="top">:</td>
					<td><?php $data = array('name'=>'kodepos','value'=>set_value('kodepos',$row['kodepos']));
    						echo form_input($data);?><span class='error'>*<?php echo form_error('kodepos'); ?></span></td>
					<td>No. HP</td>
                    <td>:</td>
                    <td><?php $data = array('name'=>'hp','value'=>set_value('hp',$row['hp']));	echo form_input($data);?><span class='error'>* <?php echo form_error('hp'); ?></span></td>
				</tr>
                <tr>
					<td>Propinsi</td>
                    <td>:</td>
					<td><?php $data = array('name'=>'propinsi','id'=>'propinsi','size'=>30,'readonly'=>'1','value'=>set_value('propinsi',$row['propinsi']));
    echo form_input($data);?></td>
				  <td valign="top">Email</td>
                    <td valign="top">:</td>
                    <td valign="top"><?php $data = array('name'=>'email','value'=>set_value('email',$row['email']));
    						echo form_input($data);?><span class='error'><?php echo form_error('email'); ?></span></td>
				</tr>
                <tr>
				  <td valign="top">No. KTP / SIM</td>
                    <td valign="top">:</td>
					<td><?php 
						if(strlen($row['noktp'])>1){
							$data = array('name'=>'noktp','disabled'=>'true','value'=>set_value('noktp',$row['noktp']));
						}else{
							$data = array('name'=>'noktp','value'=>set_value('noktp',$row['noktp']));
						}
						echo form_input($data);?>
						<span class='error'><?php echo form_error('noktp'); ?></span>
					</td>
					<td>Ahliwaris</td>
                    <td>:</td>
                    <td><?php 
						echo form_hidden('npwp',$row['npwp']); 
						if(strlen($row['ahliwaris'])>1){
							$data = array('name'=>'ahliwaris','disabled'=>'true','value'=>set_value('ahliwaris',$row['ahliwaris']));
						}else{
							$data = array('name'=>'ahliwaris','value'=>set_value('ahliwaris',$row['ahliwaris']));
						}
						echo form_input($data);?>
					</td>
				</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA BANK</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                
                <?php if($row['account_id'] > 0){?>
                <tr>
					<td colspan="6"><em>Data bank hanya bisa di rubah oleh bagian administrasi perusahaan</em></td>
                </tr>
				<tr>
					<td>Nama Bank</td>
                    <td>:</td>
					<td><?php echo $row['namabank'];?></td>
					<td>Nama Nasabah</td>
                    <td>:</td>
                    <td><?php echo $row['namanasabah'];?></td>
				</tr>
                <tr>
					<td>Cabang</td>
                    <td>:</td>
					<td><?php echo $row['area'];?></td>
					<td>No. Rekening</td>
                    <td>:</td>
                    <td><?php echo $row['no'];?></td>
				</tr>
                <?php } else {?>
                
                <tr>
					<td>Nama Bank</td>
                    <td>:</td>
					<td><?php echo form_dropdown('bank_id',$bank);?></td>
					<td>Nama Nasabah</td>
                    <td>:</td>
                    <td><?php echo $row['nama'];?></td>
				</tr>
                <tr>
					<td>Cabang</td>
                    <td>:</td>
					<td><?php $data = array('name'=>'area','value'=>set_value('area',$row['area'])); echo form_input($data);?> <span class='error'><?php echo form_error('area'); ?></td>
					<td>No. Rekening</td>
                    <td>:</td>
                    <td><?php $data = array('name'=>'norek','value'=>set_value('norek',$row['no']));echo form_input($data);?> <span class='error'><?php echo form_error('no'); ?></td>
				</tr>
                
                <?php }?>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA SPONSOR</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Member ID</td>
                    <td>:</td>
					<td><?php echo $row['enroller_id'];?></td>
					<td>Nama Sponsor</td>
                    <td>:</td>
                    <td><?php echo $row['namaenroller'];?></td>
				</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA PENEMPATAN</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Member ID</td>
                    <td>:</td>
					<td><?php echo $row['sponsor_id'];?></td>
					<td>Nama</td>
                    <td>:</td>
                    <td><?php echo $row['namasponsor'];?></td>
				</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
				  <td valign="top">PIN</td>
                    <td valign="top">:</td>
					<td><?php $data = array('name'=>'pin','id'=>'pin','size'=>'12','maxlength'=>'50');
						echo form_password($data);?> <span class='error'>*<?php echo form_error('pin'); ?></span></td>
					<td></td>
                    <td></td>
                    <td></td>
				</tr>
                <tr>
					<td><?php echo form_submit('submit','Process');?></td>
                  <td></td>
					<td></td>
					<td></td>
                    <td></td>
                    <td></td>
				</tr>
                
</table>
			<?php echo form_close();?>
            
<?php $this->load->view('footer');?>
            
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>