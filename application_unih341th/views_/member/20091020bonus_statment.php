<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="100%">
<?php echo form_open('member/bonus/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
<?php if($this->session->userdata('group_id') <= 100){ ?>
				 <tr>
			<td width='19%' valign='top'>Member ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?>				
					 <span class='error'>*<?php echo form_error('member_id');?></span></td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
        <?php }else{ ?>
        <tr>
			<td width='19%' valign='top'>Member ID / Nama </td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('userid')." / ".$this->session->userdata('name');?></b></td>
		</tr>
        <?php }?>
        
        <tr>
			<td valign='top'>Periode</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('periode',$dropdown);?></td>
		</tr>
        <tr>
			<td valign='top'>Jenjang</td>
			<td valign='top'>:</td>
			     	<td><b><?php echo $total['jenjang'];?></b></td>
		</tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
		<td><?php echo form_submit('submit','preview');?></td>
	</tr>
                    <?php echo form_close();?>
	</table>
	
    <table class="stripe">
	<tr>
      <th width='7%'>No.</th>
      <th width='45%'>Description</th>
      <th width='15%'>PV</th>
      <th width='10%'>Persen %</th>
      <th width='23%'>Nominal Rp</th>
    </tr>
   
<?php
if ($results): 
	foreach($results as $key => $row): ?>
    <tr>
      <td><?php echo anchor('member/bonus/detail/'.$row['id'], $row['i']);?></td>
      <td><?php echo anchor('member/bonus/detail/'.$row['id'], $row['titlebonus']);?></td>
      <td><?php echo anchor('member/bonus/detail/'.$row['id'], $row['fpv']);?></td>
      <td><?php echo anchor('member/bonus/detail/'.$row['id'], $row['persen']);?></td>
     <td><?php echo anchor('member/bonus/detail/'.$row['id'], $row['fnominal']);?></td>
    </tr>
    <?php endforeach; ?>
	<tr>
	      <td colspan="4"><b>Total Bonus Rp. </b></td>
     	<td><b><?php echo $total['fnominal'];?></b></td>
    </tr>
    <tr>
	      <td colspan="5"><br />Keterangan: <br />
          - PPH 21 vuntuk bonus bulan Agustus 2009 yang akan datang sebesar 5% bagi yang ber-NPWP dan 6% bagi member non-NPWP<br />
          - Bonus diatas Rp 50.000.000,- ke atas mengikuti ketentuan perpajakan yang berlaku (progresif).</td>
    </tr>
<?php else: ?>
    <tr>
      <td colspan="5">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>			
                
                
<?php $this->load->view('footer');?>
