<br>
<?php
	//if($databank){
?>
<?php echo form_open('member/bank/edit/'.$databank['member_id'], array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
<table width='100%'>
	<tr>
		<td width='24%' valign='top'>Member ID</td>
		<td width='1%' valign='top'>:</td>
		<td width='50%'><?php echo $databank['member_id'];?></td>
		<td width='25%'><?php $data = array('name'=>'member_id','readonly'=>'true','value'=>$databank['member_id']);echo form_input($data);?></td>
	</tr>
	<tr>
		<td valign='top'>Name</td>
		<td valign='top'>:</td>
		<td><?php echo $databank['nama'];?></td>
		<td valign='top'><?php $data = array('name'=>'flag','value'=>set_value('flag',$databank['flag']));echo form_input($data);?>
		<span class='error'>*<?php echo form_error('flag'); ?></span></td>
	</tr>
	<tr>
		<td width='24%' valign='top'>Bank ID</td>
		<td width='1%' valign='top'>:</td>
		<td width='50%'><?php 
			echo form_dropdown('bank_id',$bank,set_value('bank_id',$databank['bank_id']));
		?></td>
		<td width='25%'><?php 
			$data = array('name'=>'currbankid','readonly'=>'true','value'=>set_value('currbankid',$databank['bank_id']));echo form_input($data);
		?></td>
	</tr>
	<tr>
		<td valign='top'>Area / cabang</td>
		<td valign='top'>:</td>
		<td><?php 
			$data = array('name'=>'area','value'=>set_value('area',$databank['area']));echo form_input($data);
		?><span class='error'>*<?php echo form_error('area'); ?></span></td>
		<td valign='top'>&nbsp</td>
	</tr>
	<tr>
		<td valign='top'>No Rekening</td>
		<td valign='top'>:</td>
		<td><?php 	
				$data = array('name'=>'norek','value'=>set_value('norek',$databank['no']));echo form_input($data);
		?><span class='error'>*<?php echo form_error('norek'); ?></span></td>
		<td valign='top'><?php $data = array('name'=>'currnorek','readonly'=>'true','value'=>$databank['no']);echo form_input($data);?></td>
	</tr>
	<tr>
		<td valign='top'>Nama Nasabah</td>
		<td valign='top'>:</td>
		<td><?php 
			$data = array('name'=>'nasabah','value'=>set_value('nasabah',$databank['name']));echo form_input($data);
		?><span class='error'>*<?php echo form_error('nasabah'); ?></span></td>
		<td valign='top'><?php $data = array('name'=>'currnasabah','readonly'=>'true','value'=>$databank['name']);echo form_input($data);?></td>
	</tr>
	<tr>
		<td valign='top'></td>
		<td valign='top'></td>
		<td><?php echo form_submit('submit','Process');?></td>
		<td valign='top'>&nbsp</td>
	</tr>
	
</table>
<?php echo form_close();?>
<?php
	//}
?>