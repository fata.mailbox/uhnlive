<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>UNI-HEALTH.COM : <?php echo $page_title;?></title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="author" content="takwa"/>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="robots" content="noindex, nofollow" />
<meta name="rating" content="general" />
<meta name="copyright" content="Copyright (c) 2009-<?php echo date("Y");?> | www.smartindo-technology.com | contact person: Takwa Handphone: +62 817 906 1982 | Telphone: +6221 5435 5600" />
<noscript>Javascript is not enabled! Please turn on Javascript to use this site.</noscript>
<script type="text/javascript">
//<![CDATA[
base_url = '<?php echo  base_url();?>';
//]]>
</script>

<script type="text/javascript" src="<?php echo base_url();?>js/backend.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/simpletreemenu.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/prototype.js"></script>

<link rel="shortcut icon" href="<?php echo base_url();?>images/favicon.ico" type="image/ico" />

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/backend.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/simpletree.css" />	
<link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url();?>css/print.css" />

<?php
	if (isset($extraHeadContent)) {
		echo $extraHeadContent;
	}
?>

</head>
<body>
<div id="allHolder">
	<div id="container">
		<div id="masthead">
			<?php if ($this->session->userdata('logged_in')): ?>
            
			<ul id="submenu">
				<li><a href="<?php echo site_url('logout')?>" class="submenu_link logout">logout</a></li>
			</ul>
			
			<ul id="submenu">
				<li class="submenu_userid userid"><?php if($this->session->userdata('group_id')>100){ echo $this->session->userdata('username');} else { echo $this->session->userdata('user');} echo " / ".$this->session->userdata('name')?></li>
			</ul>
            <?php else: ?>
            <ul id="submenu">
				<li class="submenu_userid userid">Guest</li>
			</ul>
            <?php endif;?>
            
            <ul id="submenu">
				<li><a href="<?php echo site_url('')?>" class="submenu_link frontend">go to front end</a></li>
			</ul>
		</div>
			
		<div id="action_menu">
        
        <?php if ($this->session->userdata('logged_in')): ?>
				<?php $menu = $this->MMenu->getMenuGroup($this->session->userdata('group_id'));
				if(isset($menu)){ ?>
                <a href="javascript:ddtreemenu.flatten('treemenu', 'expand')"><b>Open</b></a><a href="#"> | </a><a href="javascript:ddtreemenu.flatten('treemenu', 'contact')" class="link_leftmenu"><b>Close</b></a><br><br>
                
					<ul id="treemenu" class="treeview">
                    
                	<?php foreach($menu as $key =>$row){ ;?>
							<li><?php echo $row['title'];?><ul>
                            <?php
									$menu2 = $this->MMenu->getSubMenu($this->session->userdata('group_id'),$row['id']);
		 							foreach($menu2 as $rowx){ 
										if(isset($rowx['title'])){ ?>
											<li class="menu"><?php echo anchor($rowx['folder']."/".$rowx['url'],$rowx['title']);?></li>	
									<?php }}?></ul></li>
                                    
                            
                <?php }?>
					</ul>
                    <script type="text/javascript">
						//ddtreemenu.createTree(treeid, enablepersist, opt_persist_in_days (default is 1))
						ddtreemenu.createTree("treemenu", true)
					</script>

					<?php echo anchor('','Print', array('class'=>'print','onclick' => 'print(); return false;'));?>
                    
                    <?php }?>
				<?php else: ?>
					<!-- <li class="menu_promo">Simple,<br>Beautiful,<br>Open Source,<br>Online System</li> -->
			<?php endif; ?>
		</div>

		<div id="main_content">