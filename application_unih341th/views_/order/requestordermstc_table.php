<table width='99%'>
	<tr>
		<td>&nbsp;</td>
  	</tr>  
	<tr>
		<td><strong>Pending Request Order M-STC ke Stockiest</strong></td>
  </tr>  
</table>

<?php echo form_open('order/romstc/del/', array('id' => 'my_form2', 'name' => 'my_form2', 'autocomplete' => 'off'));?>
<table class="stripe">
<tr>
					<td colspan='7'><?php echo form_submit('submit','Delete');?></td>					
				</tr>

	<tr>
      <th width='5%'>&nbsp;</th>
      <th width='7%'>ID</th>
      <th width='13%'>Date</th>
      <th width='38%'>M-STC / Name</th>
      <th width='17%'><div align="right">Total Price</div></th>
      <th width='12%'><div align="right">Total PV</div></th>
      <th width='8%'>Status</th>
    </tr>
   
<?php
if (isset($results2)):
	foreach($results2 as $key => $row): 
?>
    <tr>
<td><?php $data = array(
    'name'        => 'p_id[]',
    'id'          => 'p_id[]',
    'value'       => $row['id'],
    'checked'     => false,
    'style'       => 'border:none'
    );					
					echo form_checkbox($data); ?> </td>
      <td><?php echo anchor('order/romstc/view2/'.$row['id'], $row['id']);?></td>
      <td><?php echo anchor('order/romstc/view2/'.$row['id'], $row['date']);?></td>
      <td><?php echo anchor('order/romstc/view2/'.$row['id'], $row['no_stc']." - ".$row['nama']);?></td>
      <td align="right"><?php echo anchor('order/romstc/view2/'.$row['id'], $row['ftotalharga']);?></td>
		<td align="right"><?php echo anchor('order/romstc/view2/'.$row['id'], $row['ftotalpv']);?></td>
      <td><?php echo anchor('order/romstc/view2/'.$row['id'], $row['status']);?></td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<table width='99%'>
<?php echo form_open('order/ro', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='60%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='40%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b>
			<?php }?>
			
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
	<tr>
      <th width='7%'>No.</th>
      <th width='8%'>ID</th>
      <th width='10%'>Date</th>
      <th width='25%'>M-STC ID / Name</th>
      <th width='25%'>STC ID / Name</th>
      <th width='15%'><div align="right">Total Price</div></th>
      <th width='12%'><div align="right">Total PV</div></th>
      <th width='8%'>Status</th>
    </tr>
   
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor('order/romstc/view/'.$row['id'], $counter);?></td>
      <td><?php echo anchor('order/romstc/view/'.$row['id'], $row['id']);?></td>
      <td><?php echo anchor('order/romstc/view/'.$row['id'], $row['date']);?></td>
      <td><?php echo anchor('order/romstc/view/'.$row['id'], $row['no_stc']." - ".$row['nama']);?></td>
            <td><?php echo anchor('order/romstc/view/'.$row['id'], $row['no_stc2']." - ".$row['namastc']);?></td>
      <td align="right"><?php echo anchor('order/romstc/view/'.$row['id'], $row['ftotalharga']);?></td>
		<td align="right"><?php echo anchor('order/romstc/view/'.$row['id'], $row['ftotalpv']);?></td>
      <td><?php echo anchor('order/romstc/view/'.$row['id'], $row['status']);?></td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>