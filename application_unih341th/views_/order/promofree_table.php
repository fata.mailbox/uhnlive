
<table width='99%'>
<?php echo form_open('order/freeso', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='60%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='40%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b>
			<?php }?>
			
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
	<tr>
	  <th width='5%'>No.</th>
      <th width='10%'>Invoice No.</th>
      <th width='30%'>Stockiest ID / Name</th>
      <th width='20%'>Remark</th>
      <th width='10%'>Status</th>
      <th width='15%'>Approved Date</th>
      <th width='10%'>User ID</th>      
    </tr>
   
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor('order/freeso/view/'.$row['id'], $counter);?></td>
      <td><?php echo anchor('order/freeso/view/'.$row['id'], $row['id']);?></td>
      <td><?php echo anchor('order/freeso/view/'.$row['id'], $row['no_stc']." - ".$row['nama']);?></td>
      <td><?php echo anchor('order/freeso/view/'.$row['id'], $row['remark']." ");?></td>
	  <td><?php echo anchor('order/freeso/view/'.$row['id'], $row['status']);?></td>
	  <td><?php echo anchor('order/freeso/view/'.$row['id'], $row['approveddate']);?></td>
      <td><?php echo anchor('order/freeso/view/'.$row['id'], $row['approvedby']." ");?></td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="8">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
