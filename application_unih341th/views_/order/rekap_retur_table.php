
<table width='100%'>
<?php echo form_open('order/hretur', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  echo form_input($data);?>   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
   echo "to: ".form_input($data);?>
   </td>
  </tr>
 
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
	</tr>
<?php echo form_close();?>				
</table>

<table class="stripe">
	<tr>
      <td colspan="6" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <th width='5%'>No.</th>
      <th width='10%'>Invoice No.</th>
      <th width='15%'>Date</th>
      <th width='35%'>ID / Name</th>
      <th width='15%'><div align="right">Total Price</div></th>
      <th width='15%'><div align="right">Total PV</div></th>      
   </tr>
   <tr>
      <td colspan="6" style="border-bottom:solid thin #000099"></td>
  </tr>
<?php
if ($retur):
	foreach($retur as $row): 
?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['id'];?></td>
      <td><?php echo $row['tgl'];?></td>
      <td><?php echo $row['no_stc']." / ".$row['namamember'];?></td>
      <td align="right"><?php echo $row['ftotalharga'];?></td>
      <td align="right"><?php echo $row['ftotalpv'];?></td>
    </tr>
  <?php endforeach; ?>
  
  <tr>
      <td colspan="4" align="right"><b>Grand Total: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="right"><b><?php echo $totalretur['ftotalharga'];?></b></td>
      <td align="right"><b><?php echo $totalretur['ftotalpv'];?></b></td>
    </tr>
    <tr>
      <td colspan="6" style="border-bottom:double medium #000099"></td>
    </tr>
    
 <?php else: ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>


<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>
