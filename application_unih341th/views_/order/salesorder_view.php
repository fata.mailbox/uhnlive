<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<div class="ViewHold">
	<?php /* update by Boby 20120607 */ ?>
	<div id="companyDetails" align="right">
		<?php
			echo "<p><i>Lampiran B<br>FPSM 7-03-01/02-R01 </i></p>";
			echo "<img src='/images/backend/logo.jpg' border='0'><br>";
		?>
	</div>
	<?php /* end update by Boby 20120607 */?>

	<p>
		<strong>
			<?php echo "No. Invoice: ";?> <?php echo $row['id'];?><br />
			<?php
				echo $row['tgl'];
			?>
		</strong>
		
	</p>
	<br />
			<h2>Invoice Sales Order</h2>
	<hr />

	<h3><?php echo $row['member_id']," - ".$row['nama'];?></h3>

	<p>
		<?php
			echo $row['alamat']."<br />";
			echo $row['kota']." - ".$row['propinsi']." ".$row['kodepos'];
		?>
	</p>
	<p>
		<?php
			echo "No Stc / Nama: <b>".$row['no_stc']." / ".$row['namastc']."</b><br />";
			echo "Kit?: ".$row['kit']."<br />";
			echo "Remark: ".$row['remark'];
		?>
	</p>	
	
	<table class="stripe">
	<tr>
      <th width='10%'>Item Code</th>
      <th width='25%'>Item Name</th>
      <th width='10%'><div align="right">Qty</div></th>
      <th width='10%'><div align="right">Price</div></th>
      <th width='10%'><div align="right">PV</div></th>
      <th width='20%'><div align="right">Sub Total</div></th>      
      <th width='15%'><div align="right">Sub Total PV</div></th>
    </tr>
    
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><?php echo $item['item_id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td align="right"><?php echo $item['fqty'];?></td>
			<td align="right"><?php echo $item['fharga'];?></td>
			<td align="right"><?php echo $item['fpv'];?></td>
			<td align="right"><?php echo $item['fsubtotal'];?></td>
			<td align="right"><?php echo $item['fsubtotalpv'];?></td>
		</tr>
		<?php endforeach;?>
		<tr>
			<td colspan='5' align='right'><b>Total Rp.</b>&nbsp;</td>
			<td align="right"><b><?php echo $row['ftotalharga'];?></b></td>
			<td align="right"><b><?php echo $row['ftotalpv'];?></b></td>
		</tr>
	</table>
<?php
	/* Created by Andrew 20120530*/
	if($flag==0){ // jika admin
	?>
	<table width=100%>
	<tr>
	  <td colspan='1' align=center width='5%'> </td>
	  <td colspan='2' align=center width='25%'><b>Sales Counter</b></td>
	  <td colspan='1' align=center width='5%'> </td>
      <td colspan='2' align=center width='30%'><b>Warehouse</b></td>
	  <td colspan='1' align=center width='5%'> </td>
	  <td colspan='2' align=center width='25%'><b>Receiver</b></td> 	
	  <td colspan='1' align=center width='5%'> </td>
	  
	</tr>
	<tr>
	<td colspan='10'>
	<br><br><br><br><br><br>
	</td>
	</tr>
	
	<tr>
		<td></td>
		<td align='left'>(</td><td align='right'>)</td>
		<td></td>
		<td align='left'>(</td><td align='right'>)</td>
		<td></td>
		<td align='left'>(</td><td align='right'>)</td> 
		<td></td>
	</tr>
	</table>
	<?php 
		}
		/* End created by Andrew 20120530*/
	$this->load->view('footer');
?>
