<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>

<div class="ViewHold">
	<div id="companyDetails" align="right">
		<?php // echo "<img src='/images/backend/logo.jpg' border='0'><br>";?>
		<?php
			echo "<p><i>Lampiran C<br>FPSM 7-03-01/03-R01</i></p>";
			echo "<img src='/images/backend/logo.jpg' border='0'><br>";
		?>
	</div>

	<p>
		<strong>
			<?php echo "No. Invoice: ";?> <?php echo $row['id'];?><br />
			<?php
				echo $row['date']; // localized month
				echo "<br /><br />"; // day and year numbers
			?>
		</strong>
		
	</p>
	<br />
			<h2>Invoice Request Order</h2>
	<hr />

	<h3><?php echo $row['no_stc']," - ".$row['nama'];?></h3>

	<p>
		<?php
			echo $row['alamat']."<br />";
			echo $row['kota']." - ".$row['propinsi']." ".$row['kodepos'];
		?>
	</p>
	<p>
		<?php
			echo "No Stc / Nama: <b>".$row['no_stc2']." / ".$row['namastc']."</b><br />";
			echo "Status: ".$row['status']."<br />";
			echo "Delivery Date: ".$row['tglapproved']."<br />";
			echo "Remark Delivered: ".$row['remarkapp'];
		?>
	</p>	
	
	<table class="stripe">
	<tr>
      <th width='10%'>Item Code</th>
      <th width='25%'>Item Name</th>
      <th width='10%'><div align="right">Qty</div></th>
      <th width='10%'><div align="right">PV</div></th>
	  <th width='10%'><div align="right">Price</div></th>
      <th width='15%'><div align="right">Sub Total PV</div></th>
	  <th width='20%'><div align="right">Sub Total</div></th>
    </tr>
    
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><?php echo $item['item_id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td align="right"><?php echo $item['fqty'];?></td>
			<td align="right"><?php echo $item['fpv'];?></td>
			<td align="right"><?php echo $item['fharga'];?></td>
			<td align="right"><?php echo $item['fsubtotalpv'];?></td>
			<td align="right"><?php echo $item['fsubtotal'];?></td>
		</tr>
		<?php endforeach;
		if($row['diskon']>0){ ?>
		<tr>
			<td colspan='5' align='right'><b>Total</b>&nbsp;</td>
			<td align="right"><b><?php echo $row['ftotalpv'];?></b></td>
			<td align="right"><b>Rp.&nbsp;<?php echo $row['ftotalharga2'];?></b></td>
		</tr>
        <tr>
			<td colspan='5' align='right'>Diskon&nbsp;<?php echo $row['diskon'];?>%</td>
			<td colspan="2" align="right"><i>Rp. <?php echo $row['frpdiskon'];?></i></td>
		</tr>
        <?php } ?>
        <tr>
			<td colspan='5' align='right'><b>Total Bayar</b>&nbsp;</td>
			<td colspan="2" align="right"><b> Rp. <?php echo $row['ftotalharga'];?></b></td>
		</tr>
        
	</table>
	
<?php
$this->load->view('footer');
?>
