<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<div class="ViewHold">
	<div id="companyDetails">
		<?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?>
		
	</div>

	<p>
		<strong>
			<?php echo "No. Invoice: ";?> <?php echo $row['id'];?><br />
			<?php
				echo $row['tgl'];
			?>
		</strong>
		
	</p>
	<br />
			<h2>Invoice Sales Order</h2>
	<hr />

	<h3><?php echo $row['member_id']," - ".$row['nama'];?></h3>

	<p>
		<?php
			echo $row['alamat']."<br />";
			echo $row['kota']." - ".$row['propinsi']." ".$row['kodepos'];
		?>
	</p>
	
	<table class="stripe">
	<tr>
      <th width='10%'>Item Code</th>
      <th width='25%'>Item Name</th>
      <th width='10%'><div align="right">Qty</div></th>
      <th width='10%'><div align="right">PV Lama</div></th>
      <th width='20%'><div align="right">PV Baru</div></th>
    </tr>
    
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><?php echo $item['item_id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td align="right"><?php echo $item['fqty'];?></td>
			<td align="right"><?php echo $item['fpv_lama'];?></td>
			<td align="right"><?php echo $item['fpv_baru'];?></td>
		</tr>
		<?php endforeach;?>
		<tr>
			<td colspan='4' align='right'><b>Total PV Baru</b>&nbsp;</td>
			<td align="right"><b><?php echo $row['ftotalpv'];?></b></td>
		</tr>
	</table>
	
<?php
$this->load->view('footer');
?>
