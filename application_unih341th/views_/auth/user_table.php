<table width="99%">
<?php echo form_open('auth/user', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><?php $data = array(
			'id' => 'btn', 
			'content' => 'Form '.$page_title, 
			'onClick' => "location.href='".site_url()."auth/user/create'"
		); echo form_button($data); ?></td>
		<td width='60%' align='right'>Cari: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?=form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Kata kunci: <b><?=$this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?=form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='5%'>No.</th>
      <th width='15%'>Username</th>
      <th width='30%'>Nama</th>
      <th width='20%'>Group</th>
		<th width='10%'>Status</th>
      <th width='20%'>Login Terakhir</th>      
    </tr>
<?php
if ($results):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
if($row['status']=='inactive'){$a = '<i><u>';$z = "</u></i>";}else{$a = "<b>";$z = "</b>";}
?>
    <tr>
      <td><?php echo $a.anchor("/auth/user/view/".$row['id'],$counter).$z;?></td>
      <td><?php echo $a.anchor("/auth/user/view/".$row['id'],$row['username']).$z;?></td>
      <td><?php echo $a.anchor("/auth/user/view/".$row['id'],$row['name']).$z;?></td>
      <td><?php echo $a.anchor("/auth/user/view/".$row['id'],$row['title']).$z;?></td>
      <td><?php echo $a.anchor("/auth/user/view/".$row['id'],$row['status']).$z;?></td>
      <td><?php echo $a.anchor("/auth/user/view/".$row['id'],$row['last_login']).$z;?></td>
  </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="6">Data tidak tersedia.</td>
    </tr>
<?php endif; ?>    
</table>
<?php $this->load->view('paging');?>