<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<table width="85%">
                    <tr height="20">
                      <td width="22%">Group ID</td>
                      <td width="2%" align="center">:</td>
                      <td width="76%"><strong><?=$group->id;?></strong></td>
                    </tr>
                    <tr height="20">
                      <td>Nama Group</td>
                      <td align="center">:</td>
                      <td><strong><?=$group->title;?></strong></td>
                    </tr>
                    <tr height="2">
                      <td colspan="3">&nbsp;</td>
                    </tr>
                  </table>
                  
	 <?php echo form_open(current_url(), array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
				<table width="90%"> 
					<tr height="25">
					  <td width="8%" rowspan="2" align="center">No.</td>
					  <td width="46%" rowspan="2">Menu</td>
					  <td colspan="4" align="center">Authority</td>
				  </tr>
					<tr height="25">
	               <td width="13%" align="center">View?</td>
						<td width="13%" align="center">Save?</td>
                  <td width="13%" align="center">Edit?</td>
                  <td width="15%" align="center">Delete?</td>
                  
                  </tr>
          <?php if(isset($results)): 
          	$counter = 0; foreach($results as $key=>$row): $counter =$counter+1; ?>
			
				  <tr height="25">
                  <td align="center"><?=$counter;?><input name="menu_id[]" id="menu_id<?=$key;?>" value="<?=$row['id'];?>" type="hidden"/></td>
                  <td><?=$row['submenu'];?> &raquo; <?=$row['menu'];?></td>
                  <td align="center">V<?php $js='id=view'.$key; $opt = array('1' => 'Yes','0' => 'No'); echo form_dropdown('view[]',$opt,$row['view'],$js);?></td>
                  <td align="center">S<?php $js1='id=save'.$key; echo form_dropdown('save[]',$opt,$row['save'],$js1);?></td>
                  <td align="center">E<?php $js2='id=edit'.$key; echo form_dropdown('edit[]',$opt,$row['edit'],$js2);?></td>
                  <td align="center">D<?php $js3='id=delete'.$key; echo form_dropdown('delete[]',$opt,$row['delete'],$js3);?></td>
            
                  </tr>
					<?php endforeach; endif;?>                  
                  
				  <tr>
				    <td colspan="8" align="left"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr height="25">
                      <td align="left"><input name="action" type="submit" class="button" id="action" value="Update" />
                        <input name="Back" type="button" class="button" onclick="history.go(-1);" id="Back" value="	Back">
                      <strong>
                      <input name="groupid" value="<?=$group->id;?>" type="hidden" id="groupid" />
                      </strong></td>
                    </tr></table></td>
			      </tr>
		</table>
<?php echo form_close();?>

<?php $this->load->view('footer');?>