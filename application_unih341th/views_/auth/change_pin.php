<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<div>	
	 <?php if ($this->session->flashdata('message')){
					echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}?>
					
	<?php echo form_open('auth/cp/pin', array('id' => 'form'));?>
		
<table width='100%'>
		<tr>
			<td valign='top' width='14%'>Old PIN</td>
			<td valign='top' width='1%'>:</td>
			<td width='85%'><?php $data = array('name'=>'pin','id'=>'pin'); echo form_password($data);?> <span class='error'>*<?php echo form_error('pin');?></span></td>
		</tr>
		<tr>
			<td valign='top'>New PIN</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'newpin','id'=>'newpin'); echo form_password($data);?> <span class='error'>*<?php echo form_error('newpin');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Retype New PIN</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'newpin2','id'=>'newpin2'); echo form_password($data);?></td>
		</tr>
		<tr>
		<td colspan='2'>&nbsp;</td>
		<td><?php echo form_submit('submit','Change PIN');?></td></tr>
		
		</table>
		<?php echo form_close();?>
		
<?php $this->load->view('footer');?>