<div>	
	 <?php if ($this->session->flashdata('message')){
					echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}?>
					
	<?php echo form_open('auth/cp', array('id' => 'form'));?>
		
		<table width='80%'>
		<tr>
			<td width='20%' valign='top'>Old Password</td>
			<td width='1%' valign='top'>:</td>
			<td width='79%'><input type="password" name="oldpassword" id="oldpassword" autocomplete="off" value="<?php echo set_value('oldpassword');?>" maxlength="50" size="20" />
			<span class="error"><?php echo form_error('oldpassword');?></span></td>
		</tr>
		<tr>
			<td valign='top'>New Password</td>
			<td valign='top'>:</td>
			<td><input type="password" name="newpassword" id="newpassword" value="" maxlength="100" size="20" /> 
			<span class="error"><?php echo form_error('newpassword');?></span></td>
		</tr>
		<tr>
			<td>Retype New Password</td>
			<td>:</td>
			<td><input type="password" name="newpassword2" id="newpassword2" value="" maxlength="100" size="20" /></td> 
		</tr>
		<?php if($this->session->userdata('group_id') >100){?>
		<tr>
			<td valign='top'>PIN</td>
			<td valign='top'>:</td>
			<td><input type="password" name="pin" value="" maxlength="100" size="20" /> 
			<span class="error"><?php echo form_error('pin');?></span></td>
		</tr>			
		
		<?php }?>	
		<tr>
			<td></td>
			<td></td>
			<td><?php echo form_submit('submit', 'change password');?></td> 
		</tr>
        <?php if($this->session->userdata('group_id') > 100){ ?>
		<tr>
			<td>&nbsp;</td>
			<td colspan='2'>Click <?php echo anchor('auth/cp/pin/','here');?> if you want to change PIN</td>
		</tr>
        <?php }?>
		</table>
	<?php echo form_close();?>
</div>
