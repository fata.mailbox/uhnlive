<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

	 <?php echo form_open('inv/mnf/edit/'.$results[0]['manufaktur_id'], array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table width='65%'>
		<tr>
			<td valign='top' width="25%">ID BoM</td>
			<td valign='top' width="1%">:</td>
			<td valign='top' width="74%"><b><?php echo form_hidden('itemcodex',$results[0]['manufaktur_id']); echo $results[0]['manufaktur_id'];?></b></td>
		<tr>
			<td valign='top'>Nama BoM</td>
			<td valign='top'>:</td>
			<td valign='top'><b><?= $results[0]['namemanufaktur'];?></b></td>
		</tr>
		</table>
		<hr />
		<table width='65%'>	
		<tr>
			<td width='30%'>Kode Barang</td>
			<td width='45%'>Nama Barang</td>
			<td width='20%'>Qty</td>
			<td width='5%'>Del?</td>
		</tr>
		<?php if ($results): 
		 	 foreach($results as $key => $row):
		?>
		<tr>
			<td valign='top'><?php echo form_hidden('counter[]'); $data = array('name'=>'itemcode'.$key,'id'=>'itemcode'.$key,'size'=>'8','readonly'=>'1','value'=>set_value('itemcode'.$key,$row['item_id'])); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('search/invsearch/index/'.$key, '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<td valign='top'><input type="text" name="itemname<?=$key;?>" id="itemname<?=$key;?>" value="<?=set_value('itemname'.$key,$row['nameitem']);?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty<?=$key;?>" id="qty<?=$key;?>" value="<?=set_value('qty'.$key,$this->MMenu->numformat($row['qty']));?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode<?=$key;?>,document.form.itemname<?=$key;?>,document.form.qty<?=$key;?>);" src="<?= base_url();?>images/backend/delete.png" border="0"/></td>
</tr>
<?php endforeach;?>
<?php endif; ?>

<?php 
$x=0;
while($x < ($counti-count($results))){ $key++;?>

<tr>
			<td valign='top'><?php echo form_hidden('counter[]'); $data = array('name'=>'itemcode'.$key,'id'=>'itemcode'.$key,'size'=>'8','value'=>set_value('itemcode'.$key),'readonly'=>'1'); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            ); 

					echo anchor_popup('search/invsearch/index/'.$key, '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<td valign='top'><input type="text" name="itemname<?=$key;?>" id="itemname<?=$key;?>" value="<?=set_value('itemname'.$key);?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty<?=$key;?>" id="qty<?=$key;?>" value="<?=set_value('qty'.$key);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode<?=$key;?>,document.form.itemname<?=$key;?>,document.form.qty<?=$key;?>);" src="<?= base_url();?>images/backend/delete.png" border="0"/></td>
</tr>

<?php $x++; }?>
 
<tr>
        <td colspan="3"><input type="hidden" name="action" value="Save" id="action"/>
        	Add <input name="rowx" type="text" id="rowx" value="<?=set_value('rowx','1');?>" size="1" maxlength="2" />
          Row(s)<?php echo form_submit('action', 'Go');?>
</td>
        </tr>
		<tr><td colspan='3'><?php echo form_submit('submit', 'Submit');?></td></tr>
		
		</table>		
		<?php echo form_close();?>



<?php $this->load->view('footer');?>
