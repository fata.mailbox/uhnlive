<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>

<div class="ViewHold">
	<div id="companyDetails">
		<?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?>
		
	</div>

	<p>
		<strong>
			<?php echo "No. Inventory IN: ";?> <?php echo $row['id'];?><br />
			<?php
				echo $row['date']; // localized month
				echo "<br /><br />"; // day and year numbers
			?>
		</strong>
		
	</p>
	<br />
<h2>Inventory IN</h2>
	<hr />

	<h3><?php echo $row['invoiceno'];?></h3>

	<p>
		<?php
				echo $row['remark']."<br />";
				echo $row['createdby'];
		?>
	</p>

	<table class="stripe">
	<tr>
      <th width='10%'>No.</th>
      <th width='20%'>Item Code</th>
      <th width='40%'>Item Name</th>
      <th width='15%'>Qty</th>      
      <th width='15%'>Price</th>      
    </tr>
    
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><p><?php echo $counter;?></p></td>
			<td><?php echo $item['item_id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td><?php echo $item['qty'];?></td>
            <td><?php echo $item['fharga'];?></td>
		</tr>
		<?php endforeach;?>
	</table>
	
<?php
$this->load->view('footer');
?>
