<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<div id="view"><?php $data = array(
			'id' => 'btn', 
			'content' => 'Edit '.$page_title, 
			'onClick' => "location.href='".site_url()."inv/mnf/edit/".$row[0]['manufaktur_id']."'"
		); echo form_button($data); ?></div>

<div class="ViewHold">
	<div id="companyDetails"><?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?></div>
	<p>
		<strong><?php echo "Type : ";?> <?php echo $row[0]['type'];?></strong>
	</p>
	<br />
<h2><?=$page_title;?></h2>
<hr />
<h3><?php echo $row[0]['manufaktur_id']," - ".$row[0]['namemanufaktur'];?></h3>

	<table class="stripe">
	<tr>
      <th width='10%'>No.</th>
      <th width='20%'>Kode Barang</th>
      <th width='50%'>Nama Barang</th>
      <th width='20%'>Qty</th>      
    </tr>
    
    <?php $counter =0; foreach ($row as $row): $counter = $counter+1;?>
		<tr>
			<td><?php echo $counter;?></td>
			<td><?php echo $row['item_id'];?></td>
			<td><?php echo $row['nameitem'];?></td>
			<td><?php echo $row['fqty'];?></td>
		</tr>
		<?php endforeach;?>
	</table>
	
<?php
$this->load->view('footer');
?>
