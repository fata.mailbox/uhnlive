<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<?php if($row['status'] != 'close'){?>
	<div id="view"><?php echo anchor('inv/po/edit/'.$row['id'],'actual PO');?></div>
<?php }?>

<div class="ViewHold">
	<div id="companyDetails">
		<?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?>
		
	</div>

	<p>
		<strong>
			<?php echo "No. Invoice: ";?> <?php echo $row['id'];?><br />
			<?php
				echo $row['tgl'];
			?>
		</strong>
		
	</p>
	<br />
			<h2>Invoice Purchase Order</h2>
	<hr />

	<h3><?php echo $row['name'];?></h3>

	<p>
		<?php
			echo $row['cp']."<br />";
			echo $row['address'];
		?>
	</p>
	<p>
		<?php
			echo "Status: <b>".$row['status']."</b><br />";
			echo "Createdby: ".$row['createdby']."<br />";
			echo "Remark: ".$row['remark'];
		?>
	</p>	
	<p>&nbsp;&nbsp;<strong>Rincian PO</strong></p>
	<table class="stripe">
	<tr>
      <th width='10%'>Item Code</th>
      <th width='35%'>Item Name</th>
      <th width='15%'>Qty</th>
      <th width='15%'>Price</th>
      <th width='25%'>Sub Total</th>
    </tr>
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><?php echo $item['item_id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td align="right"><?php echo $item['fqty'];?></td>
			<td align="right"><?php echo $item['fharga'];?></td>
			<td align="right"><?php echo $item['fsubtotal'];?></td>
		</tr>
		<?php endforeach;?>
		<tr>
			<td colspan='4' align='right'><b>Total Rp.</b>&nbsp;</td>
			<td align="right"><b><?php echo $row['ftotalorder'];?></b></td>			
		</tr>
	</table>
    <br />
    <p>&nbsp;&nbsp;<strong>Rincian Actual PO</strong></p>
    
<table width="99%">
	<tr>
      <td colspan="7" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <td width='5%'><b>No.</b></td>
      <td width='10%'><b>Invoice No.</b></td>
      <td width='10%'><b>Date</b></td>
      <td width='20%'><b>Tgl Jatuh Tempo</b></td>
      <td width='10%'><b>Total Price</b></td>
      <td width='20%'><b>Remark</b></td>
      <td width='10%'><b>User ID</b></td>
   </tr>
   <tr>
      <td colspan="7" style="border-bottom:dashed thin #666666"></td>
  </tr>
   <tr>
      <td colspan="7"  style="border-bottom:solid thin #000099">
      	<table width="100%">
        	<tr>
	            <td width="5%" class="cName">&nbsp;</td>
            	<td width="5%" class="cName"><em>No.</em></td>
                <td width="10%" class="cName"><em>Item ID</em></td>
                <td width="30%" class="cName"><em>Nama</em></td>
                <td width="15%" class="cName"><em>Qty</em></td>
                <td width="15%" class="cName"><em>Price</em></td>
                <td width="20%" class="cName"><em>Jml Harga</em></td>
          </tr>
        </table>
     </td>    
</tr>
   
<?php
if ($results):
	foreach($results as $row): 
?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['id'];?></td>
      <td><?php echo $row['tgl'];?></td>
      <td><?php echo $row['tgljatuhtempo'];?></td>
      <td align="right"><?php echo $row['ftotalactual'];?></td>
      <td><?php echo $row['remark']." ";?></td>
      <td><?php echo $row['createdby'];?></td>
    </tr>
     <tr>
      <td colspan="7" style="border-bottom:dashed thin #666666"></td>
  </tr>

    <?php foreach($row['details'] as $row2):?>
    
     <tr>
      <td colspan="7">
      	<table width="100%">
        	<tr>
	            <td width="5%" class="cName">&nbsp;</td>
            	<td width="5%" class="cName"><em>
           	    <?php echo $row2['i'];?>
            	</em></td>
                <td width="10%" class="cName"><em>
                <?php echo $row2['item_id'];?>
                </em></td>
                <td width="30%" class="cName"><em>
                <?php echo $row2['name'];?>
                </em></td>
                <td width="15%" class="cName" align="right"><em>
                <?php echo $row2['fqty'];?>
                </em></td>
                <td width="15%" class="cName" align="right"><em>
                <?php echo $row2['fharga'];?>
                </em></td>
                <td width="20%" class="cName" align="right"><em>
                <?php echo $row2['fjmlharga'];?>
                </em></td>
          </tr>
        </table>
      </td>    
</tr>
  <?php endforeach;?>
  <tr>
      <td colspan="7" style="border-bottom:solid thin #000099"></td>
    </tr>
  <?php endforeach; ?>
  
  <tr>
      <td colspan="5" align="right"><b>Grand Total: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td colspan="2" align="right"><b><?php echo $total->ftotalactual;?></b></td>
    </tr>
    <tr>
      <td colspan="7" style="border-bottom:double medium #000099"></td>
    </tr>
    
 <?php else: ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
	
<?php
$this->load->view('footer');
?>
