
<table width='99%'>
<?php echo form_open('inv/po', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='60%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='40%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b>
			<?php }?>
			
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<?php echo form_open('inv/po/status/', array('id' => 'my_form2', 'name' => 'my_form2', 'autocomplete' => 'off'));?>
<table class="stripe">
	<tr>
					<td colspan='7'><?php $status=array('parsial'=>'parsial','close'=>'close','reject'=>'reject'); echo form_dropdown('status',$status);?> <?php echo form_submit('submit','Submit');?></td>					
				</tr>

	<tr>
      <th width='10%'>No.</th>
      <th width='10%'>Invoice No.</th>
      <th width='12%'>Date</th>
      <th width='35%'>Supplier</th>
      <th width='13%'>Total Order</th>
      <th width='8%'>Status</th>
    </tr>
   
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php if($row['status'] != 'close'){?> 
	  <?php $data = array(
    'name'        => 'p_id[]',
    'id'          => 'p_id[]',
    'value'       => $row['id'],
    'checked'     => false,
    'style'       => 'border:none'
    );					
					echo form_checkbox($data); ?>
					<?php }?>
					<?php echo anchor('inv/po/view/'.$row['id'], $counter);?></td>
      <td><?php echo anchor('inv/po/view/'.$row['id'], $row['id']);?></td>
      <td><?php echo anchor('inv/po/view/'.$row['id'], $row['tgl']);?></td>
      <td><?php echo anchor('inv/po/view/'.$row['id'], $row['name']);?></td>
      <td align="right"><?php echo anchor('inv/po/view/'.$row['id'], $row['ftotalorder']);?></td>
      <td><?php echo anchor('inv/po/view/'.$row['id'], $row['status']);?></td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php echo form_close();?>				