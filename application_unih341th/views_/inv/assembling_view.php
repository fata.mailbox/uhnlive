<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<div class="ViewHold">
	<div id="companyDetails"><?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?></div>
	<p>
		<strong>
			<?php echo "No. Assembling : ";?> <?php echo $row->id;?><br />
			<?php 
				echo $row->date; // localized month
				 // day and year numbers
			?>
		</strong>
	</p>
<h2>Assembling</h2>
<hr />
<h3><?php echo $row->item_id," - ".$row->name;?></h3>
<p>
		<?php
				echo "Quntity: <b>".$row->fqty."</b><br />";
				echo "Remark: ".$row->remark."<br />";
				echo "User ID: ".$row->createdby;
		?>
	</p>

	<table class="stripe">
	<tr>
      <th width='10%'>No.</th>
      <th width='20%'>Item Code</th>
      <th width='50%'>Item Name</th>
      <th width='20%'>Qty</th>      
    </tr>
    
    <?php $counter =0; foreach ($results as $row): $counter = $counter+1;?>
		<tr>
			<td><p><?php echo $counter;?></p></td>
			<td><?php echo $row['item_id'];?></td>
			<td><?php echo $row['name'];?></td>
			<td><?php echo $row['fqty'];?></td>
		</tr>
		<?php endforeach;?>
	</table>
	
<?php
$this->load->view('footer');
?>
