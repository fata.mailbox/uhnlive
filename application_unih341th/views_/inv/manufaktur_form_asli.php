<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('inv/mnf/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table width='65%'>
		<tr>
			<td valign='top'>Manufaktur ID</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $data = array('name'=>'itemcodex','id'=>'itemcodex','size'=>'8','readonly'=>'1','value'=>form_error('itemcodex); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('invsearch/index/x', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
					<span class="error">* <?php echo form_error('itemcodex'); ?></span>
			</td>
		</tr>			
		<tr>
			<td valign='top'>Manufaktur Name</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="itemnamex" id="itemnamex" value="<?php echo form_error('itemnamex;?>" readonly="1" size="30" />
				<?php echo  form_hidden('qtyx');?>
			</td>
		</tr>
		</table>
		
		<table width='65%'>	
		<tr>
			<td width='30%'>Item Code</td>
			<td width='45%'>Item Name</td>
			<td width='20%'>Qty</td>
			<td width='5%'>Del?</td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode0','id'=>'itemcode0','size'=>'8','readonly'=>'1','value'=>form_error('itemcode0); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('invsearch/index/0', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<td valign='top'><input type="text" name="itemname0" id="itemname0" value="<?php echo form_error('itemname0;?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty0" id="qty0" value="<?php echo form_error('qty0;?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode0,document.form.itemname0,document.form.qty0);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
</tr>

		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode1','id'=>'itemcode1','size'=>'8','readonly'=>'1','value'=>form_error('itemcode1); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('invsearch/index/1', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<td valign='top'><input type="text" name="itemname1" id="itemname1" value="<?php echo form_error('itemname1;?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty1" id="qty1" value="<?php echo form_error('qty1;?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode1,document.form.itemname1,document.form.qty1);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode2','id'=>'itemcode2','size'=>'8','readonly'=>'1','value'=>form_error('itemcode2); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('invsearch/index/2', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<td valign='top'><input type="text" name="itemname2" id="itemname2" value="<?php echo form_error('itemname2;?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty2" id="qty2" value="<?php echo form_error('qty2;?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode2,document.form.itemname2,document.form.qty2);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode3','id'=>'itemcode3','size'=>'8','readonly'=>'1','value'=>form_error('itemcode3); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('invsearch/index/3', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<td valign='top'><input type="text" name="itemname3" id="itemname3" value="<?php echo form_error('itemname3;?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty3" id="qty3" value="<?php echo form_error('qty3;?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode3,document.form.itemname3,document.form.qty3);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode4','id'=>'itemcode4','size'=>'8','readonly'=>'1','value'=>form_error('itemcode4); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('invsearch/index/4', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<td valign='top'><input type="text" name="itemname4" id="itemname4" value="<?php echo form_error('itemname4;?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty4" id="qty4" value="<?php echo form_error('qty4;?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode4,document.form.itemname4,document.form.qty4);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode5','id'=>'itemcode5','size'=>'8','readonly'=>'1','value'=>form_error('itemcode5); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('invsearch/index/5', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<td valign='top'><input type="text" name="itemname5" id="itemname5" value="<?php echo form_error('itemname5;?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty5" id="qty5" value="<?php echo form_error('qty5;?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode5,document.form.itemname5,document.form.qty5);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode6','id'=>'itemcode6','size'=>'8','readonly'=>'1','value'=>form_error('itemcode6); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('invsearch/index/6', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<td valign='top'><input type="text" name="itemname6" id="itemname6" value="<?php echo form_error('itemname6;?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty6" id="qty6" value="<?php echo form_error('qty6;?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode6,document.form.itemname6,document.form.qty6);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode7','id'=>'itemcode7','size'=>'8','readonly'=>'1','value'=>form_error('itemcode7); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('invsearch/index/7', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<td valign='top'><input type="text" name="itemname7" id="itemname7" value="<?php echo form_error('itemname7;?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty7" id="qty7" value="<?php echo form_error('qty7;?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode7,document.form.itemname7,document.form.qty7);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode8','id'=>'itemcode8','size'=>'8','readonly'=>'1','value'=>form_error('itemcode8); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('invsearch/index/8', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<td valign='top'><input type="text" name="itemname8" id="itemname8" value="<?php echo form_error('itemname8;?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty8" id="qty8" value="<?php echo form_error('qty8;?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode8,document.form.itemname8,document.form.qty8);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode9','id'=>'itemcode9','size'=>'8','readonly'=>'1','value'=>form_error('itemcode9); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('invsearch/index/9', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<td valign='top'><input type="text" name="itemname9" id="itemname9" value="<?php echo form_error('itemname9;?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty9" id="qty9" value="<?php echo form_error('qty9;?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode9,document.form.itemname9,document.form.qty9);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr><td colspan='3'><?php echo form_submit('submit', 'Submit');?></td></tr>
		
		</table>
		
		<?php echo form_close();?>



<?php
$this->load->view('footer');
?>
