<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('inv/mnf/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table width='65%'>
		<tr>
			<td valign='top'>Manufaktur ID</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $data = array('name'=>'itemcodex','id'=>'itemcodex','size'=>'8','readonly'=>'1','value'=>set_value('itemcodex', '')); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('invsearch/index/x', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
					<span class="error">* <?php echo form_error('itemcodex');?></span>
			</td>
		</tr>			
		<tr>
			<td valign='top'>Manufaktur Name</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="itemnamex" id="itemnamex" value="<?php echo set_value('itemnamex', '');?>" readonly="1" size="30" />
				<?php echo  form_hidden('qtyx');?>
			</td>
		</tr>
		</table>
		
		<table width='65%'>	
		<tr>
			<td width='30%'>Item Code</td>
			<td width='45%'>Item Name</td>
			<td width='20%'>Qty</td>
			<td width='5%'>Del?</td>
		</tr>
		<?php 
$x=0;
$x=0;
while($x < $i){ ?>

		<tr>
			<td valign='top'><input type="text" name="itemcode[<?php echo $x;?>]" value="<?php echo set_value('itemcode['.$x.']',''); ?>" size="8" />
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            ); 

					echo anchor_popup('invsearch/index/'.$x, '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<td valign='top'><input type="text" name="itemname[]" id="itemname<?php echo $x;?>" value="<?php echo set_value('itemname[]', '0'); ?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty[]" id="qty<?php echo $x;?>" value="" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode<?php echo $x;?>,document.form.itemname<?php echo $x;?>,document.form.qty<?php echo $x;?>);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
</tr>

<?php $x++; }?>
<tr>
        <td colspan="3">
        	Add <input name="rowx" type="text" id="rowx" value="<?php echo set_value('rowx', '1'); ?>" size="1" maxlength="2" />
          Row(s)<?php echo form_submit('action', 'Go');?>
        </div></td>
        </tr>
		
				<tr><td colspan='3'><?php echo form_submit('submit', 'Submit');?></td></tr>
		
		</table>
		
		<?php echo form_close();?>



<?php
$this->load->view('footer');
?>
