<table width="99%">
<?php echo form_open('inv/mnf', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><?php $data = array(
			'id' => 'btn', 
			'content' => 'Form '.$page_title, 
			'onClick' => "location.href='".site_url()."inv/mnf/create'"
		); echo form_button($data); ?></td>
		<td width='60%' align='right'>Cari: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?=form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Kata kunci: <b><?=$this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?=form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='10%'>No.</th>
      <th width='15%'>BoM ID</th>
      <th width='50%'>Nama</th>
      <th width='25%'>Type</th>      
    </tr>
<?php
if ($results):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor("/inv/mnf/view/".$row['manufaktur_id'],$counter);?></td>
      <td><?php echo anchor("/inv/mnf/view/".$row['manufaktur_id'],$row['manufaktur_id']);?></td>
      <td><?php echo anchor("/inv/mnf/view/".$row['manufaktur_id'],$row['name']);?></td>
      <td><?php echo anchor("/inv/mnf/view/".$row['manufaktur_id'],$row['type']);?></td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="4">Data tidak tersedia.</td>
    </tr>
<?php endif; ?>    
</table>
<?php $this->load->view('paging');?>