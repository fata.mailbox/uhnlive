<table width='99%'>
<?php echo form_open('inv/ass/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='10%'>No.</th>
      <th width='12%'>Date</th>
      <th width='10%'>Item ID</th>
      <th width='33%'>Name</th>
      <th width='15%'>Qty</th>
      <th width='10%'>Desembling ?</th>      
      <th width='10%'>User ID</th>      
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor("/inv/ass/view/".$row['id'],$counter);?></td>
      <td><?php echo anchor("/inv/ass/view/".$row['id'],$row['date']);?></td>
      <td><?php echo anchor("/inv/ass/view/".$row['id'],$row['item_id']);?></td>
      <td><?php echo anchor("/inv/ass/view/".$row['id'],$row['name']);?></td>
      <td><?php echo anchor("/inv/ass/view/".$row['id'],$row['fqty']);?></td> 
      <td><?php echo anchor("/inv/ass/view/".$row['id'],$row['desembling']);?></td> 
      <td><?php echo anchor("/inv/ass/view/".$row['id'],$row['createdby']);?></td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
