<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<?php if($row->status == 'N'){?><div id="view"><?php echo anchor('inv/mtsapp/app/'.$row->id,'approved');?></div><?php }?>

<div class="ViewHold">
	<div id="companyDetails"><?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?></div>
	<p>
		<strong>
			<?php echo "No. Mutasi : ";?> <?php echo $row->id;?><br />
			<?php 
				echo $row->date;
				
			?> 
		</strong>
	</p>
<h2>Mutasi Stock Approved</h2>
<hr />
<h3><?php echo $row->fromwhs." to : ".$row->towhs;?></h3>
	<p>
		<?php
				echo "App: ".$row->status." by: ".$row->approvedby." date: ".$row->approved."<br />";
				echo "Remark: ".$row->remark."<br />";
				echo "Remark App: ".$row->remarkapp."<br />";
				echo "User ID: ".$row->createdby;
		?>
	</p>

	<table class="stripe">
	<tr>
      <th width='10%'>No.</th>
      <th width='20%'>Item Code</th>
      <th width='50%'>Item Name</th>
      <th width='20%'>Qty</th>      
    </tr>
    
    <?php $counter =0; foreach ($items as $row): $counter = $counter+1;?>
		<tr>
			<td><?php echo $counter;?></td>
			<td><?php echo $row['item_id'];?></td>
			<td><?php echo $row['name'];?></td>
			<td><?php echo $row['fqty'];?></td>
		</tr>
		<?php endforeach;?>
	</table>
	
<?php
$this->load->view('footer');
?>
