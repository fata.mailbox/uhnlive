
<table width='100%'>
<?php echo form_open('inv/schistory', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'>
			<?php 
				$data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  
				echo form_input($data);
			?>   
			<?php 
				$data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
				echo "to: ".form_input($data);
			?>
   </td>
  </tr>
 
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
  </tr>
<?php echo form_close();?>				
</table>


<table border="1" bordercolor="#999999" class="stripe">
	<tr>
		<td colspan="11" style="border-bottom:solid thin #000099"></td>
	</tr>
	<tr>
	  <th rowspan="2" align="left"><div align="Left">Product Name ( Product ID) </div></th>
	  <th colspan="2" align="left">Stc Consignment </th>
	  <th colspan="2" align="left">SC Return </th>
	  <th colspan="2" align="left">Sum of SC </th>
  </tr>
	<tr>
		<th align="right"><div align="right">Qty</div></th>
		<th align="right"><div align="right">Total Price</div></th>
		<th align="right"><div align="right">Qty</div></th>
		<th align="right"><div align="right">Total Price</div></th>
		<th align="right"><div align="right">Qty</div></th>
		<th align="right"><div align="right">Total Price</div></th>
	</tr>
	<tr>
		<td colspan="11" style="border-bottom:solid thin #000099"></td>
	</tr>
	<?
		if ($sc):
			$qtya = 0;	$tota = 0;
			$qtyb = 0;	$totb = 0;
			$qty = 0;	$tot = 0;
			foreach($sc as $dt1): 
	?>
	<tr>
		<td align="left" width="40%"><?php echo $dt1['pname']." ( ".$dt1['id']." ) ";	?></td>
		<td align="right" width="5%"><?php echo number_format($dt1['jmla']);	$qtya+=$dt1['jmla'];	?></td>
		<td align="right" width="15%"><?php echo number_format($dt1['tota']); 	$tota+=$dt1['tota'];	?></td>
		<td align="right" width="5%"><?php echo number_format($dt1['jmlb']); 	$qtyb+=$dt1['jmlb'];	?></td>
		<td align="right" width="15%"><?php echo number_format($dt1['totb']); 	$totb+=$dt1['totb'];	?></td>
		<td align="right" width="5%"><?php echo number_format($dt1['qty']);	$qty+=$dt1['qty'];	?></td>
		<td align="right" width="15%"><?php echo number_format($dt1['hrg']); 	$tot+=$dt1['hrg'];	?></td>
	</tr>
	<?php
			endforeach;
			setlocale(LC_MONETARY, "en_US");
	?>
	<tr>
		<td colspan="11" style="border-bottom:solid thin #000099"></td>
	</tr>
	<tr>
		<td align="left"><strong>Grand Total</strong></td>
		<td align="right"><strong><?php echo number_format($qtya);	?></strong></td>
		<td align="right"><strong><?php echo number_format($tota); 	?></strong></td>
		<td align="right"><strong><?php echo number_format($qtyb); 	?></strong></td>
		<td align="right"><strong><?php echo number_format($totb); 	?></strong></td>
		<td align="right"><strong><?php echo number_format($qty);	?></strong></td>
		<td align="right"><strong><?php echo number_format($tot); 	?></strong></td>
	</tr>
	<?php
		else:
	?>
	<tr>
      <td colspan="11">Data is not available.</td>
    </tr>
	<?php
		endif;
	?>
	<tr>
		<td colspan="11" style="border-bottom:double medium #000099"></td>
	</tr>
</table>

<p>
  <script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
  </script>
</p>
