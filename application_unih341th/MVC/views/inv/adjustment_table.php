<table width='99%'>
<?php echo form_open('inv/adj/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='60%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='40%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?><?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='10%'>No.</th>
      <th width='10%'>Adj ID</th>
      <th width='10%'>Date</th>
      <th width='20%'>Warehouse</th>
      <th width='5%'>Adj ?</th>
      <th width='35%'>Remark</th>
      <th width='10%'>User ID</th>
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor('inv/adj/view/'.$row['id'], $counter);?></td>
      <td><?php echo anchor('inv/adj/view/'.$row['id'], $row['id']);?></td>
      <td><?php echo anchor('inv/adj/view/'.$row['id'], $row['date']);?></td>
      <td><?php echo anchor('inv/adj/view/'.$row['id'], $row['name']);?></td>
      <td><?php echo anchor('inv/adj/view/'.$row['id'], $row['flag']);?></td>
      <td><?php echo anchor('inv/adj/view/'.$row['id'], $row['remark']." ");?></td>
      <td><?php echo anchor('inv/adj/view/'.$row['id'], $row['createdby']);?></td>
    </tr>
    <?php endforeach; 
  else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
