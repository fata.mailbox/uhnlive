<table width='100%'>
<?php echo form_open('inv/ttp/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
    <?php if($this->session->userdata('group_id')<=100){ ?>
  <tr>
			<td width='24%' valign='top'>Stockies ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='75%'><?php echo form_hidden('member_id',set_value('member_id','')); $data = array('name'=>'no_stc','id'=>'no_stc','size'=>15,'readonly'=>'1','value'=>set_value('no_stc'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('stcsearch/all/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?>				
					 <span class='error'>*<?php echo form_error('member_id');?></span></td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
  
  <?php } else { echo form_hidden('member_id',$this->session->userdata('userid'));?>
		<tr>
			<td>Name</td>
			<td>:</td>
			<td><b><?php echo $this->session->userdata('username')." / ".$this->session->userdata('name');?></b></td>
		</tr>
	<?php }?>

	<tr>
		<td valign='top'>Stock</td>
			<td valign='top'>:</td>
		<td><?php $options = array('qoh' => 'Stock Updated', 'sc' => 'Stock Card');
    				echo form_dropdown('status',$options,set_value('status'))."&nbsp;";?>
   </td>
  </tr>
<tr>
		<td valign='top'>Periode</td>
		<td valign='top'>:</td>
		<td><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',$reportDate));
   echo form_input($data);?>
   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',$reportDate));
   echo "to: ".form_input($data);?>
   </td>
  </tr>
  <tr>
	  <td valign='top'>Item for Stock Card</td>
	<td valign='top'>:</td>
		<td valign='top'><?php $data = array('name'=>'itemcode','id'=>'itemcode','size'=>'8','readonly'=>'1','value'=>set_value('itemcode')); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('itemsearch/ttp/', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<input type="text" name="itemname" id="itemname" value="<?php echo set_value('itemname');?>" readonly="1" size="25" /> <input type="text" name="price" id="price" value="<?php echo set_value('price');?>" readonly="1" size="10" />
			<span class="error"><?php echo form_error('status'); ?></span>
			</td>
	</tr>
	<tr>
    	<td>&nbsp;</td>
        <td>&nbsp;</td>
		<td><?php echo form_submit('submit','preview');?></td>
	</tr>
				  
<?php echo form_close();?>				
</table>

<?php if($this->input->post('status') == 'qoh'){?>

<table class="stripe">
    <tr>
      <th width='5%'>No.</th>
      <th width='10%'>Item Code</th>
      <th width='33%'>Item Name</th>
      <th width='10%'><div align="right">Qty Stock</div></th>
      <th width='10%'><div align="right">Price</div></th>
      <th width='7%'><div align="right">PV</div></th>
      <th width='15%'><div align="right">Total Price</div></th>
      <th width='10%'><div align="right">Total PV</div></th>
     </tr>
<?php
if ($results):
	$counter = 0; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo $counter;?></td>
      <td><?php echo $row['item_id'];?></td>
      <td><?php echo $row['name'];?></td>
      <td align="right"><?php echo $row['fqty'];?></td>
      <td align="right"><?php echo $row['fharga'];?></td>
      <td align="right"><?php echo $row['fpv'];?></td>
      <td align="right"><?php echo $row['fjmlharga'];?></td>
      <td align="right"><?php echo $row['fjmlpv'];?></td>
     </tr>
    <?php endforeach;?> 
	<tr>
      <td colspan="6">Total : </td>
      <td align="right"><b><?php echo $total['ftotalharga'];?></b></td>
      <td align="right"><b><?php echo $total['ftotalpv'];?></b></td>
     </tr>
<?php else:
 ?>
    <tr>
      <td colspan="8">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<?php } else{ ?>

<table class="stripe">
    <tr>
      <th width='9%'>No.</th>
      <th width='15%'>Date</th>
      <th width='30%'>Description</th>
      <th width='12%'><div align="right">Stock In</div></th>
      <th width='12%'><div align="right">Stock Out</div></th>
      <th width='12%'><div align="right">Saldo</div></th>
      <th width='10%'>User ID</th>
     </tr>
<?php
if ($results):
	$counter = 1; ?> 
	<tr>
      <td>1</td>
      <td colspan='4'>Balance stok</td>
      <td><div align="right"><b><?php echo $results[0]['fsaldoawal'];?></b></div></td>
		<td>&nbsp;</td>      
     </tr>
	<?php 
	foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo $counter;?></td>
      <td><?php echo $row['tgl'];?></td>
      <td><?php echo $row['description'];?></td>
      <td align="right"><?php echo $row['fsaldoin'];?></td>
      <td align="right"><?php echo $row['fsaldoout'];?></td>
      <td align="right"><?php echo $row['fsaldoakhir'];?></td>
      <td><?php echo $row['createdby'];?></td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<?php }?>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>
