<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->


<div><img src="<?php echo base_url();?>images/frontend/head-title-product2.gif" width="504" height="26" alt="" /></div>
<?php foreach($results as $row): ?>
	<div class="prodCont">

    	<div class="productThumb"><a href="<?php echo site_url();?>product/detail/<?php echo $row['id'];?>"><img src="<?php echo base_url();?>userfiles/product/<?php echo $row['image'];?>" width="156" height="112" alt="" /></a></div>
            <div class="prodDesc"><h6><a href="<?php echo site_url();?>product/detail/<?php echo $row['id'];?>"><?php echo $row['name'];?></a></h6>
            <p><?php echo $row['headline'];?></p><h6>Harga: Rp. <?php echo number_format($row['pricecust'],'0','','.');?>,-</h6></div>
            <div class="prodSep"><img src="<?php echo base_url();?>userfiles/product/dot-separator2.gif" width="500" height="5" alt="" /></div>

</div>
<?php endforeach;?>