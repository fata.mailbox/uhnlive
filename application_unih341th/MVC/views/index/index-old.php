<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>UNI-HEALTH.COM</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="author" content="takwa"/>
<meta name="description" content="UNIHEALTH : Multi Level Marketing" />
<meta name="keywords" content="MLM, Multi Level Marketing, Makanan Kesehatan, Web Developer, smartindo, smartindo technology" />

<meta name="revisit-after" content="10 days" />
<meta name="robots" content="all,index,follow" />

<meta name="MSSmartTagsPreventParsing" content="TRUE" />
<meta http-equiv="Content-Language" content="en-us" />

<meta name="verify-v1" content="VCVjXvqlZ2A9LXgEy+EGCclrzw6TmBtD73F/mtMwSaQ=" />
<meta NAME="Distribution" CONTENT="Global" />
<meta NAME="Rating" CONTENT="General" />

<meta name="copyright" content="Copyright (c) 2009-<?php echo date("Y");?> | www.smartindo-technology.com | contact person: Takwa Handphone: +62 817 906 1982 | Telphone: +62 21 96 213 688" />
<!--
	Copyright (c) i2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +62 21 96 213 688 
    	Yahoo Messenger	: taqwatea
-->

<link rel="shortcut icon" href="<?php echo base_url();?>images/favicon.ico" type="image/ico" />

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/basicStyle.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/soho.css" />

<script src="<?php echo base_url();?>js/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/splash.js" type="text/javascript"></script>
</head>

<body>
  <div id="container">
    <div id="page">
      <div id="top">
        <div id="logo"><a href=""><img src="<?php echo base_url();?>images/frontend/logo.gif" alt="HOME" width="334" height="95" /></a></div>
        <div id="topNav"><a name="User" class="topMenuUser">
        <?php if($this->session->userdata('group_id')){ ?>
	        Hello <?php echo $this->session->userdata('name');?></a><a href="<?php echo site_url();?>logout/" class="topMenu">Logout</a><a href="<?php echo site_url();?>main/" class="topMenuReg">Go to Backend</a>
         <?php }else {?>
        	Hello Guest</a><a href="<?php echo site_url();?>login/" class="topMenu">Member Login</a><a href="<?php echo site_url();?>register/" class="topMenuReg">Register</a>
            <?php }?>
            <a href="<?php echo site_url();?>contactus/" class="topMenu">Contact&nbsp;Us</a><a href="<?php echo site_url();?>faq/" class="topMenu">FAQ</a></div>
      </div>
      <div id="headMenu">
      	<div class="emptyButton"><a name="empty"><img src="<?php echo base_url();?>images/frontend/empty-button.gif" width="154" height="42" alt="" /></a></div>
	    <div class="buttonHome"><a href="<?php echo site_url();?>"></a></div>
	    <div class="buttonAbout"><a href="<?php echo site_url();?>aboutus/"></a></div>
	    <div class="buttonProduct"><a href="<?php echo site_url();?>product/cat/"></a></div>
        <div class="buttonMarketing"><a href="<?php echo site_url();?>marketingplan/"></a></div>
	    <div class="buttonTesti"><a href="<?php echo site_url();?>testi"></a></div>
   	    <div class="buttonNews"><a href="<?php echo site_url();?>news/"></a></div>
	    <div class="buttonDownload"><a href="<?php echo site_url();?>download/"></a></div>
      </div>
      <div id="header">
        <div class="leftHeader"><img src="<?php echo base_url();?>images/frontend/welcome.gif" width="373" height="26" alt="" /><p class="welcomeNote">Selamat Datang di website MLM Unihealth! Anda akan kami hantar ke dalam zona peningkatan kualitas kesehatan dan kesuksesan melalui produk-produk terutama dari SOHO Group yang merupakan grup Perusahaan Farmasi Nasional yang terkemuka saat ini serta melalui sistem bisnis jaringan kami yang unik dan mengedepankan prinsip <em>fairness, profitable</em> dan <em>success for everyone</em>. Terima kasih telah mengunjungi website kami!</p></div>
        
        <div class="rightHeader"><script type="text/javascript">new fadeshow(fadeimages, 480, 246, 0, 5000, 1, "R")</script></div>
        
      </div>
      
      <div id="contentCont">
      	<?php $this->load->view($content);?>
      </div>
      
      <div id="footer">
       <p>COPYRIGHT &copy; 2009 UNIHEALTH NETWORK - ALL RIGHTS RESERVED</p>
       <p style="font-size:11px !important;">Webmaster: <a href="mailto:webmaster@uni-health.com">webmaster@uni-health.com</a><!-- developed by: http://www.smartindo-technology.com/" target="_blank">SMARTIDO Technology</a> --></p>
      </div>
	 <div><img src="<?php echo base_url();?>images/frontend/footer-bg.gif" width="946" height="59" alt="" /></div>
    </div>
</div> 

  	<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>
	<script type="text/javascript">
		_uacct = "UA-1709778-2";
		urchinTracker();
	</script> 
</body>
</html>