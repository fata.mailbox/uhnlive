<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/highslide-gallery.css" /><!--to photo-->
<script src="<?=base_url();?>js/highslide-with-gallery.js" 	type="text/javascript"></script> 	<!--to photo-->

<!--to  photo product-->
<script type="text/javascript">
	hs.graphicsDir ='<?=base_url();?>images/graphics/';
	hs.align = 'center';
	hs.transitions = ['expand', 'crossfade'];
	hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = 0.8;
	//hs.dimmingOpacity = 0.75;
	
	// Add the controlbar
	hs.addSlideshow({
		//slideshowGroup: 'group1',
		interval: 5000,
		repeat: false,
		useControls: true,
		fixedControls: 'fit',
		overlayOptions: {
			opacity: 0.75,
			position: 'bottom center',
			hideOnMouseOut: true
		}
	});
</script>


<div id="box_content">
        	<div id="menu_left">
            	<div id="menu_left_box">
                	<div id="menu_left_det">
                    	<div id="menu_left_judul">
                        	<div class="box_subtitle">OPPORTUNITY</div>
                        </div>
                        <div class="menu">
                            <div class="menu_uli"><a href="<?=site_url();?>opportunity">Why Join UNIHEALTH</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>opportunity/started">Getting Started</a></div>
							 <div class="menu_uli"><a href="<?=site_url();?>marketing_plan">Marketing Plan</a></div>
							<!-- <div class="menu_uli"><a href="<?=site_url();?>opportunity/conference">Conference</a></div> -->
							<!-- <div class="menu_uli"><a href="<?=site_url();?>opportunity/program_rekrut">Promo</a></div> -->
                           	<!-- <div class="menu_uli"><a href="<?=site_url();?>event_training">Special Programs</a></div> -->
                            <!-- <div class="menu_uli" style="height:90px; background-color:#FFF;">-->
							<div class="menu_uli"><a href="<?=site_url();?>event_training">Special Programs</a></div>
                            	<div class="menu_uli" style="background-color:#FFF;"><!--_sub--><a href="<?=site_url();?>gallery">Gallery <!--&amp; News--></a></div>
                                <!--<div class="menu_uli_sub2">
                                	<a href="<?=site_url();?>news">News</a>
                                </div>
                                <div class="menu_uli_sub2">
                                	<a href="<?=site_url();?>gallery">Gallery</a>
                                </div>
                            </div>-->
                            <div class="menu_uli"><a href="<?=site_url();?>faq">FAQ</a></div>
                            <!--<div class="menu_uli"><a href="<?=site_url();?>recognition">Recognition</a></div>-->
                        </div>
                    </div>
                </div>
            </div>
                    
            <div id="menu_right">
            	<div id="menu_right_banner">
                	<img src="<?=base_url();?>images/banner_opportunity.jpg"  />
                </div>
                <div id="menu_right_content">
                	<div id="menu_right_title">GALLERY ALBUM -&nbsp;<?=strtoupper($album);?></div>
                	<hr />
                </div>
                <div id="menu_right_mv">
                	<!--highslide-gallery-->
                    <div class="highslide-gallery" style="width:100%; margin:auto;">
                    	<?php if($results){
						foreach($results as $key => $row){ 
							if(($key-1) % 3 == '1')$x = '1'; else $x = '0';
						?>
                        <div class="highslide-bg">
                            <a href="<?=base_url();?>userfiles/gallery_photo/<?=$row['file'];?>" class="highslide post" onclick="return hs.expand(this)">
                                <img src="<?=base_url();?>userfiles/gallery_photo/t_<?=$row['file'];?>" width="148" height="120" alt="Highslide JS" title="<?=$row['title'];?>" />
                            </a>
                            <div class="highslide-caption">
                                <?=$row['description'];?>
                            </div>
                        </div>
                        
                        <?php if($x == '1'){?>
                        	<div class="clearBoth"></div>
                        <?php }
						}
						}else{?>
                        <div class="highslide-bg">
                            <a href="<?=base_url();?>images/foto_unavailable.jpg" class="highslide post" onclick="return hs.expand(this)">
                                <img src="<?=base_url();?>images/cover_album_unavailable.jpg" width="148" height="120" alt="Highslide JS" title="Click to enlarge" />
                            </a>
                            <div class="highslide-caption">
                                Data is nota available
                            </div>
                        </div>
                        <?php }?>
                        <div class="pagiNation" align="center"><?php echo $this->pagination->create_links(); ?></div>
                        <div class="clearBoth"></div>
                    </div><!--end highslide-gallery-->
                </div>
                
            </div>
            
        </div>
        