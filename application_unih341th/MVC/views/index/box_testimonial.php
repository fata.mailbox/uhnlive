<!--testi_box-->
                <div class="testi_box">
                	<h2><a href="<?=site_url();?>testi">Testimonial</a></h2>
                    <div style="margin:0 5px; background:url(<?=base_url();?>images/frontend/testi_bg_logo.jpg) no-repeat center">
                    <marquee onmouseover="this.stop()" onmouseout="this.start()" direction="up" scrollamount="2" scrolldelay="10" height="230" width="185">
                    	<?php if($testi): 
										foreach($testi as $row): ?>
                    	<div>
                            <img src="<?=base_url();?>userfiles/thumbnail/t_<?=$row['thumbnail'];?>" class="post" />
                            <b><?=$row['title'];?></b><br />
                            <?=$row['shortdesc'];?>
                            <a href="<?=site_url();?>testi/index/<?=$row['shortdesc'];?>"> selengkapnya</a> &raquo;
                        </div>
                        <?php endforeach; else: ?>
                        <div>
                            <img src="<?=base_url();?>images/frontend/photo-testi-unavailable.jpg" class="post" />
                            Data belum tersedia.
                        </div>
                        <?php endif;?>
                    </marquee>
                    </div>
                    <div class="testiMore">
                    	<a style="color:#00941f;" href="<?=site_url();?>testi"><b>Testimonial Lainnya</b></a> &raquo;
                    </div>
                </div><!--end testi_box-->