<br>
<?php 
if($row){
	foreach($row as $data):
		$gm = $data['gm'];
		$dl = $data['dl'];
		$molecool = $data['molecool'];
		$magozai = $data['magozai'];
		$pvg = $data['totalpv'];
		
		$molecoolg = $data['molecool_g6000'];
		$magozaig = $data['magozai_g3000'];
		$maideng = $data['maiden_g2500'];
	endforeach;
}
/*
if($row2){
	foreach($row2 as $data):
		$ps = $data['pv'];
	endforeach;
}
*/
?>
<table width='100%'>
<?php echo form_open('member/promo2', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	
<?php if($this->session->userdata('group_id') > 100){ ?>
	<tr>
		<td width='24%'>Member ID / Name</td>
		<td width='1%'>:</td>
        <td width='75%'><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('userid')." / ".$this->session->userdata('name');?></b></td>
	</tr>   
  
<?php }else{ ?>
	<tr>
		<td width='24%' valign='top'>Member ID</td>
		<td width='1%' valign='top'>:</td>
		<td width='75%'>
			<?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
				echo form_input($data);?> <?php $atts = array(
				  'width'      => '450',
				  'height'     => '600',
				  'scrollbars' => 'yes',
				  'status'     => 'yes',
				  'resizable'  => 'yes',
				  'screenx'    => '0',
				  'screeny'    => '0'
				);
				echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
				?>
		</td>
	</tr>
	<tr>
		<td valign='top'>Name</td>
		<td valign='top'>:</td>
		<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
	</tr>
<?php }?>

	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
	</tr>
<?php echo form_close();?>				
</table>
<br>
<table>
	<tr>
		<td><b>Posisi minimal GM (Gold Member) :</b></td>
		<td align="right"><?php if($gm==1){echo "Done";}else{echo "None";}?> </td>
		<td>&nbsp&nbsp&nbsp&nbsp&nbsp</td>
		<td><b>SCAP Molecool :</b></td>
		<td align="right"><?php echo number_format($molecool);?> PV</td>
	</tr>
	<tr>
		<td><b>Total downline :</b></td>
		<td align="right"><?php echo number_format($dl)." downline";?> </td>
		<td>&nbsp </td>
		<td><b>SCAP Magozai :</b></td>
		<td align="right"><?php echo number_format($magozai)." PV";?></td>
	</tr>
	<tr>
		<td>&nbsp </td>
		<td>&nbsp </td>
		<td>&nbsp </td>
		<td>&nbsp </td>
		<td>&nbsp </td>
	</tr>
	<tr>
		<td><b>PV Group (minimum 30.000 PV) :</b></td>
		<td align="right"><?php echo number_format($pvg);?></td>
		<td>&nbsp </td>
		<td><b>Kekurangan PV Group :</b></td>
		<td align="right"><?php if($pvg<30000){echo number_format(30000 - $pvg);}else{echo "Done";}?></td>
	</tr>
	<tr>
		<td><b>PV Group Molecool (minimum 6000 PV) :</b></td>
		<td align="right"><?php echo number_format($molecoolg);?></td>
		<td>&nbsp </td>
		<td><b>Kekurangan PV Group Molecool :</b></td>
		<td align="right"><?php if($molecoolg<6000){echo number_format(6000 - $molecoolg);}else{echo "Done";}?></td>
	</tr>
	<tr>
		<td><b>PV Group Magozai  (minimum 3000 PV):</b></td>
		<td align="right"><?php echo number_format($magozaig);?></td>
		<td>&nbsp </td>
		<td><b>Kekurangan PV Group Magozai :</b></td>
		<td align="right"><?php if($magozaig<3000){echo number_format(3000 - $magozaig);}else{echo "Done";}?></td>
	</tr>
	<tr>
		<td><b>PV Group Maiden  (minimum 2500 PV):</b></td>
		<td align="right"><?php echo number_format($maideng);?></td>
		<td>&nbsp </td>
		<td><b>Kekurangan PV Group Maiden :</b></td>
		<td align="right"><?php if($maideng<2500){echo number_format(2500 - $maideng);}else{echo "Done";}?></td>
	</tr>
</table>

<p><br>
Downline
<table class="stripe">
	<tr>
      <th><div align = "right">No.</div></th>
      <th><div align = "center">ID Downline</div></th>
      <th><div align = "left">Nama Downline</div></th>
      <th><div align="right">Total Group PV</div></th>
    </tr>

<?php
if ($results): 
	foreach($results as $key => $row): ?>
    <tr>
      <td align="right"><?php echo $row['i'];?></td>
      <td align="center"><?php echo $row['id'];?></td>
      <td align="left"><?php echo $row['nama'];?></td>
     <td align="right"><b><?php echo number_format($row['totalpv']);?></b></td>
    </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
      <td colspan="4">Data is not available.</td>
    </tr>
<?php endif; ?> 
</table><br>
<em>*) Hasil berikut ini adalah hasil perhitungan sementara</em>