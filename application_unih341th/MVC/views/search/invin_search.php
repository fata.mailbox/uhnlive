
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/popup.css" />

<table width="100%" border="0" cellpadding="5" cellspacing="5">
<tr><td>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr><td class='bg_border' align='center'>Browse Item Inventory</td></tr>
		<tr><td class='td_border2'>
			
			<?php echo form_open('invinsearch/index/'.$this->uri->segment(3), array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width='10%'>Search </td>
				<td width='1%'>:</td>
    			<td width='89%'><?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','search');?></td>
			</tr>
			<?php if($this->session->userdata('keywords')){ ?>
				<tr><td colspan='2'>&nbsp;</td>
				<td>Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b></td>
				</tr>
			<?php }?>
			
			</table>	
			<?php echo form_close();?>
				<?php if($results) {?>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">				
				<tr class='title_table'>
					<td class='td_report' width='15%'>No.</td>
					<td class='td_report' width='25%'>Item Code</td>
					<td class='td_report' width='65%'>Item Name</td>
				</tr>
				<?php $counter = $from_rows; foreach($results as $row) { $counter = $counter + 1; ?>
				<tr height='22' class='lvtColData' onmouseover='this.className="lvtColDataHover"' onmouseout='this.className="lvtColData"'
				 onclick="window.opener.document.form.itemcode<?php echo $this->uri->segment(3);?>.value ='<?php echo $row['id'];?>';
				  window.opener.document.form.itemname<?php echo $this->uri->segment(3);?>.value ='<?php echo $row['name'];?>';
				  window.close();">
					<td class='td_report'><?php echo $counter;?></td>
					<td class='td_report'><?php echo $row['id'];?></td>
					<td class='td_report'><?php echo $row['name'];?></td>
				</tr>
				<?php }?>
				<tr><td colspan='3'><?php echo $this->pagination->create_links(); ?></td></tr>
				</table>
				<?php }?>
		</td></tr>
</table>
		
</td></tr>
</table>