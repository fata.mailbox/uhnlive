<table width='99%'>
<?php echo form_open('fin/bemem/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
	<tr>
	<td>From date</td>
	<td>:</td>
		<td><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',$reportDate));
   echo form_input($data);?>
   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',$reportDate));
   echo "to: ".form_input($data);?>
   </td>
  </tr>
  <?php if($this->session->userdata('group_id')<=100){ ?>
  <tr>
			<td width='24%' valign='top'>Member ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='75%'><?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '450',
              'height'     => '600',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?></td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
  
  <?php } else {echo form_hidden('member_id',$this->session->userdata('userid'));?>
		<tr>
			<td>Name</td>
			<td>:</td>
			<td><b><?php echo $this->session->userdata('username')." / ".$this->session->userdata('name');?></b></td>
		</tr>
	<?php }?>
  <tr>
  		<td colspan='2'>&nbsp;</td>
		<td><?php echo form_submit('submit','preview');?></td>
	</tr>
				  
<?php echo form_close();?>				
</table>
<table class="stripe">
	<tr>
      <td colspan ='7'><b>Balance Ewallet</b></td>
    </tr>
    <tr>
      <th width='5%'>No.</th>
      <th width='18%'>Date</th>
      <th width='23%'>Description</th>
      <th width='13%'>Debet</th>
      <th width='13%'>Credit</th>
      <th width='13%'>Saldo</th>
      <th width='10%'>User ID</th>
     </tr>
<?php
if ($results):
	$counter = 1; ?> 
	<tr>
      <td>1</td>
      <td colspan='4'>Balance ewallet</td>
      <td><?php echo $results[0]['fsaldoawal'];?></td>
		<td>&nbsp;</td>      
     </tr>
	<?php 
	foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo $counter;?></td>
      <td><?php echo $row['created'];?></td>
      <td><?php echo $row['description'];?></td>
      <td align="right"><?php echo $row['fdebet'];?></td>
      <td align="right"><?php echo $row['fkredit'];?></td>
      <td align="right"><?php echo $row['fsaldoakhir'];?></td>
      <td><?php echo $row['createdby'];?></td>
     </tr>
    <?php endforeach;?>
    <tr>
     	<td colspan='3'><b>Total</b></td>
      <td align="right"><b><?php echo $total['fdebet'];?></b></td>
		<td align="right"><b><?php echo $total['fkredit'];?></b></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
     </tr>
   
    
<?php      
 else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
	<span class='message'>Saldo balance ewallet Rp. </span> &nbsp;
	<span class='message'><b><?php echo $ewallet['fewallet'];?></b></span>
	
	
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>
