<?php 
	/* ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- */
	/* Begin Part 1 Template */ // Created by Boby 20130625
	$this->load->view('header');
	echo "<h2>".$page_title."</h2>";
	echo anchor('fin/acc_val/create','Create Valid BCA Account Registration');
	if($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>
	<table width="99%">
	<?php echo form_open($base_url, array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search by: 
			<?php 
				echo form_dropdown('type',$type);
				$data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    			echo form_input($data);?> <?php echo form_submit('submit','go');
				if($this->session->userdata('keywords')){echo "<br/>Your search keywords : <b>".$this->session->userdata('keywords')."</b>";}
			?>
    	</td>
	</tr>
	<?php echo form_close();?>				
	</table>
<?php
	/* End Part 1 Template */
	
	/* ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- */
	/* Content (edit for header field) */
?>
<table class="stripe">
	<?php /* Header of table */ ?>
	<tr>
		<th>No.</th>
		<th>Name</th>
		<th>Account Number</th>
		<th><div align="right">Transfer Cost</div></th>
		<th>Last Updated/Created</th>
		<th>Created/Updated by</th>
		<th colspan="3"><div align="center">Choose For Update Transfer Cost</div></th>
	</tr>
	<?php /* End header of table */ ?>
	
<?php
	/* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== */
	/* Looping data table */
if (isset($results)):
	$counter = $from_rows;
	foreach($results as $key => $row):
		$counter = $counter+1;
?>
    <tr>
      <td><?php echo $counter;?></td>
      <td><?php echo $row['nama'];?></td>
      <td><?php echo $row['acc_no'];?></td>
      <td align = "right"><?php echo ($row['flag']);?></td>
      <td align = "center"><?php echo $row['created'];?></td>
      <td align = "center"><?php echo $row['createdby'];?></td>
      <td align = "right">
			<?php 
			$str = ""; $flag = 0;
			if($row['need']==1 & $row['flag_']==0){$str = 'Yes'; $flag = 1;}
			if($row['need']==1 & $row['flag_']<>0){$str = 'Rp. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0,-'; $flag = 1;}
			if($row['need']==0 & ($row['flag_']<>0 | $row['flag_']=='-')){$str = 'Rp. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0,-'; $flag = 1;}
			if($flag == 1){echo anchor('fin/acc_val/update/'.$row['acc_no'].'/1',$str);}else{echo " --------------- ";}
			echo ' | ';
			?>
	  </td>
	  <td align = "right">
			<?php
			$str = ""; $flag = 0;
			if($row['need']==1 & $row['flag_']==2500){$str = 'Yes'; $flag = 1;}
			if($row['need']==1 & $row['flag_']<>2500){$str = 'Rp.2.500,-'; $flag = 1;}
			if($row['need']==0 & ($row['flag_']<>2500 | $row['flag_']=='-')){$str = 'Rp.2.500,-'; $flag = 1;}
			if($flag == 1){echo anchor('fin/acc_val/update/'.$row['acc_no'].'/2',$str);}else{echo " --------------- ";}
			echo ' | ';
			?>
	  </td>
	  <td align = "right"><?php 
			$str = ""; $flag = 0;
			if($row['need']==1){$str = 'No Valid'; $flag = 1;}
			if($row['need']==0 & $row['flag_']<>'-'){$str = 'No Valid'; $flag = 1;}
			if($flag == 1){echo anchor('fin/acc_val/update/'.$row['acc_no'].'/3',$str);}else{echo " --------------- ";}
			?>
	  </td>
    </tr>
<?php 
	endforeach;
	/* End looping data table*/
	/* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== */
	else:
 ?>
    <tr>
      <td colspan="<?php echo $field;?>">Data is not available.</td>
    </tr>
<?php endif; ?>
</table>
<?php $this->load->view('paging');?>
<?php /* End Content */?>

<?php $this->load->view('footer');?>