<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="100%">
<?php echo form_open('fin/dailyreport/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
        <tr>
			<td valign='top' width="19%">Periode</td>
			<td valign='top' width="1%">:</td>
			<td width="80%"><?php echo form_dropdown('periode',$dropdown);?></td>
		</tr>
        <tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><?php echo form_submit('submit','preview');?></td>
		</tr>
                
         <?php echo form_close();?>
    <tr><td colspan="3"><hr /></td></tr>
	</table>
	
    <table class="stripe" width="100%">
	<tr>
      <th>Tanggal.</th>
      <th><div align="right">In Active</div></th>
      <th><div align="right">New Star</div></th>
      <th><div align="right">New Rekrut</div></th>
      <th><div align="right">Active</div></th>
      <th><div align="right">Reactive</div></th>
      <th><div align="right">SO Rp</div></th>
      <th><div align="right">SO BV</div></th>
      <th><div align="right">RO Rp</div></th>
      <th><div align="right">RO BV</div></th>
      <th><div align="right">SCP RP</div></th>
      <th><div align="right">SCP BV</div></th>
	  <th><div align="right">Retur Rp</div></th>
	  <th><div align="right">Retur BV</div></th>
	  <th><div align="right">Total Omset</div></th>
	</tr>
   
<?php
if ($results): 
	$gt=0;
	foreach($results as $key => $row): 
	$temp = 0;
?>
    <tr>
		<td><?php echo $row['ftgl'];?></td>
		<td><div align="right"><?php echo $row['inactive'];?></div></td>
		<td><div align="right"><?php echo $row['newstar'];?></div></td>
		<td><div align="right"><?php echo $row['newrecruit'];?></div></td>
		<td align="right"><?php echo $row['active'];?></td>
		<td align="right"><?php echo $row['reactive'];?></td>
		<td align="right"><?php echo $row['fsorp'];$temp+=str_replace(",","",$row['fsorp']);?></td>
		<td align="right"><?php echo $row['fsobv'];?></td>
		<td align="right"><?php echo $row['frorp'];$temp+=str_replace(",","",$row['frorp']);?></td>
		<td align="right"><?php echo $row['frobv'];?></td>
		<td align="right"><?php echo $row['fscpaymentrp'];$temp+=str_replace(",","",$row['fscpaymentrp']);?></td>
		<td align="right"><?php echo $row['fscpaymentbv'];?></td>
		<td align="right"><?php echo $row['freturrp'];$temp+=str_replace(",","",$row['freturrp']);?></td>
		<td align="right"><?php echo $row['freturbv'];?></td>
		<td align="right"><?php echo number_format($temp);$gt+=$temp;?></td>
    </tr>
    <?php endforeach; ?>
	<tr>
	<td><b>Total</b></td>
		<td><div align="right"><?php echo $total['inactive'];?></div></td>
		<td><div align="right"><?php echo $total['newstar'];?></div></td>
		<td><div align="right"><?php echo $total['newrecruit'];?></div></td>
		<td align="right"><?php echo $total['active'];?></td>
		<td align="right"><?php echo $total['reactive'];?></td>
		<td align="right"><?php echo $total['fsorp'];?></td>
		<td align="right"><?php echo $total['fsorp'];?></td>
		<td align="right"><?php echo $total['frorp'];?></td>
		<td align="right"><?php echo $total['frobv'];?></td>
		<td align="right"><?php echo $total['fscpaymentrp'];?></td>
		<td align="right"><?php echo $total['fscpaymentbv'];?></td>
		<td align="right"><?php echo $total['freturrp'];?></td>
		<td align="right"><?php echo $total['freturbv'];?></td>
		<td align="right"><?php echo number_format($gt);?></td>
	</tr>
<?php else: ?>
    <tr>
      <td colspan="14">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>			                
                
<?php $this->load->view('footer');?>
