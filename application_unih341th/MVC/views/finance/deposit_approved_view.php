<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<?php if($row->approved == 'pending' && $this->session->userdata('group_id') <= 100){?>
	<div id="view"><?php echo anchor('fin/dpsapp/edit/'.$row->id,'approved deposit');?></div>
<?php }?>

<div class="ViewHold">
	<div id="companyDetails"><?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?></div>
	<p>
		<strong><?php echo "invoice id : ";?> <?php echo $row->id;?><br />
		<?php echo "created : ";?> <?php echo $row->created;?>
		</strong>
	</p>
	
<h2>Form Deposit</h2>
<hr />
<h3><?php echo $row->no_stc." / ".$row->nama;?></h3>
<table class="stripe">
	<tr>
      <th width='34%'>Description</th>
      <th width='1%'>:</th>
      <th width='65%'>Value</th>
	</tr>
	<tr>
		<td>Approved ?</td>
		<td>:</td>
		<td><?php echo $row->approved;?></td>
    </tr>
	<tr>
		<td>Cash Rp</td>
		<td>:</td>
		<td><?php echo $row->ftunai;?></td>
    </tr>
    <tr>
		<td>Transfer Rp / Tgl Transfer</td>
		<td>:</td>
		<td><?php echo $row->ftransfer." / ".$row->tgl_transfer;?></td>
    </tr>
    <tr>
		<td>Debit Card Rp</td>
		<td>:</td>
		<td><?php echo $row->fdebit_card;?></td>
    </tr>
    <tr>
		<td>Credit Card Rp</td>
		<td>:</td>
		<td><?php echo $row->fcredit_card;?></td>
    </tr>
    <tr>
		<td>Kembali Rp</td>
		<td>:</td>
		<td><?php echo $row->fkembali;?></td>
    </tr>
    <tr>
		<td><b>Total Deposit Rp</b></td>
		<td>:</td>
		<td><b><?php echo $row->ftotal;?></b></td>
    </tr>
    <tr>
		<td><b>Total Deposit App Rp</b></td>
		<td>:</td>
		<td><b><?php echo $row->ftotal_approved;?></b></td>
    </tr>
    <tr>
		<td>Remark</td>
		<td>:</td>
		<td><?php echo $row->remark;?></td>
    </tr>
    <tr>
		<td>Approved date / by</td>
		<td>:</td>
		<td><?php echo $row->tgl_approved." / ".$row->approvedby;?></td>
    </tr>
    <tr>
		<td>Remark Finance</td>
		<td>:</td>
		<td><?php echo $row->remark_fin;?></td>
    </tr>
	<tr>
		<td>Created date / by</td>
		<td>:</td>
		<td><?php echo $row->created." / ".$row->createdby;?></td>
    </tr>
</table>
	
<?php $this->load->view('footer');?>
