
<table width='100%'>
<?php echo form_open('fin/hdeposit', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  echo form_input($data);?>   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
   echo "to: ".form_input($data);?>
   </td>
  </tr>
  
<tr>
			<td valign='top'>Cabang </td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('whsid',$warehouse);?></td>
		</tr>
  
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
	</tr>
<?php echo form_close();?>				
</table>

<table class="stripe">
	<tr>
      <td colspan="11" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <th width='5%'>No.</th>
		<th width='10%'>Date</th>
      <th width='10%'>ID</th>
       <th width='25%'>Name</th>
      <th width='5%'>Dps ID</th>
      <th width='10%'><div align="right">Transfer</div></th>
      <th width='10%'><div align="right">Cash</div></th>
      <th width='10%'><div align="right">Debit Card</div></th>
      <th width='10%'><div align="right">Credit Card</div></th>
      <th width='15%'><div align="right">Total Rp</div></th>
      <th width='10%'>User ID</th>
   </tr>
	<tr>
      <td colspan="11" style="border-bottom:solid thin #000099"></td>
  </tr>
   
<?php
if ($results):
	foreach($results as $row): 
?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['tgl_approved'];?></td>
      <td><?php if($row['flag'] == 'mem') echo $row['member_id']; else echo $row['no_stc'];?></td>
      <td><?php echo $row['nama'];?></td>
      <td><?php echo $row['id'];?></td>
      <td align="right"><?php echo $row['ftransfer'];?></td>
      <td align="right"><?php echo $row['ftunai'];?></td>
      <td align="right"><?php echo $row['fdebit_card'];?></td>
		<td align="right"><?php echo $row['fcredit_card'];?></td>
        <td align="right"><?php echo $row['ftotal_approved'];?></td>
        <td><?php echo $row['approvedby'];?></td>
    </tr>

  <?php endforeach;?>
  <tr>
      <td colspan="5" align="right"><strong>Grand Total: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
   	<td align="right"><strong><?php echo $total['ftransfer'];?></strong></td>
    <td align="right"><strong><?php echo $total['ftunai'];?></strong></td>
    <td align="right"><strong><?php echo $total['fdebit_card'];?></strong></td>
	<td align="right"><strong><?php echo $total['fcredit_card'];?></strong></td>
    <td align="right"><strong><?php echo $total['ftotal_approved'];?></strong></td>

    <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="11" style="border-bottom:double medium #000099"></td>
    </tr>
    
 <?php else: ?>
    <tr>
      <td colspan="9">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>
