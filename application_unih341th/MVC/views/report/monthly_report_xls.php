<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>


<table width="100%">
<?php echo form_open('report/monthly_report/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<!--
	<tr>
		<td valign='top' width="14%">Periode</td>
		<td valign='top' width="1%">:</td>
		<td width="85%">
			<?php 
				$thn = date('Y', now());
				$bln = date('m', now());
				
				$bulan = array(
					'01'  => 'January',		'02'	=> 'February',	'03'  => 'March',		'04'	=> 'April',
					'05'  => 'May',			'06'	=> 'Juny',		'07'  => 'July',		'08'	=> 'August',
					'09'  => 'September',	'10'	=> 'October',	'11'  => 'November',	'12'	=> 'December'
				);
				
				for($i=$thn;$i>=2009;$i--){
					$tahun[$i] = $i;
				}
				//echo form_dropdown('periode',$dropdown);
				echo form_dropdown('bulan', $bulan);
				echo form_dropdown('tahun', $tahun);
				
			?>
		</td>
	</tr>
	<tr>
		<td valign='top'>Category</td>
		<td valign='top'>:</td>
		<td width="85%">
			<?php
				$flag = array(
					'1' => 'New Member',
					'2' => 'Member Birth',
					'3' => 'All'
				);
				echo form_dropdown('flag',$flag);				
			?>
		</td>
	</tr>
    -->
	
	<tr>
		<td valign='top'>Export to Excel</td>
		<td valign='top'>:</td>
		<td>
                <label for="radio1"><?php echo form_radio("type_id", "royalty_magozai", (set_value("type_id") == "royalty_magozai"),"id='radio1'"); ?> Royalty Magozai</label>
                    <span class="error"><?=form_error('type_id');?></span>
		</td>
	</tr>    
    
    <tr>
		<td valign='top' colspan="3"><hr /></td>
	</tr>
    
    <tr>
		<td valign='top'>Accounting</td>
		<td valign='top'>:</td>
		<td><label for="radio2"><?php echo form_radio("type_id", "so_ke_uhn", (set_value("type_id") == "so_ke_uhn"),"id='radio2'"); ?> SO ke UHN</label>
		</td>
	</tr>
    <tr>
		<td valign='top'>&nbsp;</td>
		<td valign='top'></td>
		<td><label for="radio3"><?php echo form_radio("type_id", "ro_stc", (set_value("type_id") == "ro_stc"),"id='radio3'"); ?> RO Stockist</label>
		</td>
	</tr>
    <tr>
		<td valign='top'>&nbsp;</td>
		<td valign='top'></td>
		<td><label for="radio4"><?php echo form_radio("type_id", "ro_mstc", (set_value("type_id") == "ro_mstc"),"id='radio4'"); ?> RO M-Stockist</label>
		</td>
	</tr>
    <tr>
		<td valign='top'>&nbsp;</td>
		<td valign='top'></td>
		<td><label for="radio5"><?php echo form_radio("type_id", "scp", (set_value("type_id") == "scp"),"id='radio5'"); ?> SCP</label>
		</td>
	</tr>
    <tr>
		<td valign='top'>&nbsp;</td>
		<td valign='top'></td>
		<td><label for="radio6"><?php echo form_radio("type_id", "retur", (set_value("type_id") == "retur"),"id='radio6'"); ?> Retur</label>
		</td>
	</tr>
    
    <tr>
		<td valign='top' colspan="3"><hr /></td>
	</tr>
    
    <tr>
		<td valign='top'>Genereal Manager Report</td>
		<td valign='top'>:</td>
		<td><label for="radio7"><?php echo form_radio("type_id", "member_active_1_tahun", (set_value("type_id") == "member_active_1_tahun"),"id='radio7'"); ?> New member active 1 tahun dan belanja</label>
		</td>
	</tr>
    
	<tr>
		<td valign='top'>&nbsp;</td>
		<td valign='top'></td>
		<td><label for="radio8"><?php echo form_radio("type_id", "member_active_old_1_tahun", (set_value("type_id") == "member_active_old_1_tahun"),"id='radio8'"); ?> Member active 1 tahun dan belanja</label>
		</td>
	</tr>
    <tr>
		<td valign='top'>&nbsp;</td>
		<td valign='top'></td>
		<td><label for="radio9"><?php echo form_radio("type_id", "sponsor_member", (set_value("type_id") == "sponsor_member"),"id='radio9'"); ?> Sponsor Member 1 Tahun</label>
		</td>
	</tr>
    <tr>
		<td valign='top' colspan="3"><hr /></td>
	</tr>
    
    <tr>
		<td valign='top'>After Running Bonus Report</td>
		<td valign='top'>:</td>
		<td><label for="radio10"><?php echo form_radio("type_id", "cetak_stc", (set_value("type_id") == "cetak_stc"),"id='radio10'"); ?> Cetak Stockiest</label>
		</td>
	</tr>
    <tr>
		<td valign='top'>&nbsp;</td>
		<td valign='top'></td>
		<td><label for="radio102"><?php echo form_radio("type_id", "calon_stc", (set_value("type_id") == "calon_stc"),"id='radio102'"); ?> Upgrade Stc</label>
		</td>
	</tr>
	<tr>
		<td valign='top'>&nbsp;</td>
		<td valign='top'></td>
		<td><label for="radio101"><?php echo form_radio("type_id", "one_hit", (set_value("type_id") == "one_hit"),"id='radio101'"); ?> One Hit</label>
		</td>
	</tr>
    <tr>
		<td valign='top'>&nbsp;</td>
		<td valign='top'></td>
		<td><label for="radio11"><?php echo form_radio("type_id", "pph21_new", (set_value("type_id") == "pph21_new"),"id='radio11'"); ?> Pph21 New</label>
		</td>
	</tr>
    
    
	<tr>
		<td valign='top'>&nbsp;</td>
		<td valign='top'></td>
		<td><label for="radio12"><?php echo form_radio("type_id", "list_transfer", (set_value("type_id") == "list_transfer"),"id='radio12'"); ?> List Transfer</label>
		</td>
	</tr>
    <tr>
		<td valign='top'>&nbsp;</td>
		<td valign='top'></td>
		<td><label for="radio13"><?php echo form_radio("type_id", "bonus_account", (set_value("type_id") == "bonus_account"),"id='radio13'"); ?> Bonus Account</label>
		</td>
	</tr>
    
    <tr>
		<td valign='top'>&nbsp;</td>
		<td valign='top'></td>
		<td><?php echo form_submit('submit','Download Report');?></td>
	</tr>
    
    
    
	<?php echo form_close();?>
	<tr><td colspan="3"><hr /></td></tr>             
</table>
       
                
<?php $this->load->view('footer');?>
