<table width='99%'>
<?php echo form_open('member/memprofile/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search by: <?php $option = array('namamember'=>'Nama Member','member_id'=>'Member ID'); echo form_dropdown('option',$option); $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				 echo "&nbsp;".form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='5%'>No.</th>
      <th width='25%'>ID/Jenjang/Nama Member</th>
      <th width='5%'>KTP</th>
      <th width='12%'>Kota</th>
      <th width='10%'><div align="right">Ewallet</div></th>      
      <th width='10%'><div align="right">PS (PV)</div></th>
	<th width='15%'>Reason Banned</th>
    <th width='8%'>Action</th>
        
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo $counter;?></td>
     <td><?php echo $row['id']." / ".$row['jenjang']." / ".$row['nama'];?></td>
      <td><?php echo $row['noktp'];?></td>
      <td><?php echo $row['kota'];?></td>
      <td align="right"><?php echo $row['fewallet'];?></td>
		<td align="right"><?php echo $row['fps'];?></td>
       <td><?php echo $row['reason']." ";?></td>
       <td>
      	<a href="<?php echo site_url()."member/memprofile/view/".$row['id'];?>"><img src="/images/backend/detail.gif" alt="view / edit profile" title="view / edit profile"></a> &nbsp; 
      	<a href="<?php echo site_url()."member/memprofile/banned/".$row['id'];?>"><img src="/images/backend/user.png"  alt="Banned member" title="Banned member"></a>
      </td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
