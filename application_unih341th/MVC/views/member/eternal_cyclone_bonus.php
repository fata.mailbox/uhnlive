<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="100%">
<?php echo form_open('member/ecb/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
        <tr>
			<td valign='top' width="19%">Periode</td>
			<td valign='top' width="1%">:</td>
			<td width="80%"><?php echo form_dropdown('periode',$dropdown);?> <?php echo form_submit('submit','preview');?></td>
		</tr>
         <?php echo form_close();?>
    </table>
	
    <table class="stripe">
	<tr>
      <th width='5%'>No.</th>
      <th width='45%'>Member ID / Nama</th>
      <th width='10%'>ECB I</th>
      <th width='10%'>ECB II</th>
      <th width='10%'>ECB III</th>
      <th width='10%'>ECB IV</th>
      <th width='10%'>ECB V</th>
    </tr>
   
<?php
if ($results): 
	foreach($results as $key => $row): ?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['member_id']." / ".$row['nama'];?></td>
      <td><?php if($row['hadiah'] == 1)echo "V";?></td>
      <td><?php if($row['hadiah'] == 2)echo "V";?></td>
      <td><?php if($row['hadiah'] == 3)echo "V";?></td>
     <td><?php if($row['hadiah'] == 4)echo "V";?></td>
           <td><?php if($row['hadiah'] == 5)echo "V";?></td>
    </tr>
    <?php endforeach; ?>
	
<?php else: ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>			
                
                
<?php $this->load->view('footer');?>
