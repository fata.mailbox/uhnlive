<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="100%">
<?php echo form_open('member/analysis/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
        <tr>
			<td valign='top' width="19%">Periode</td>
			<td valign='top' width="1%">:</td>
			<td width="80%"><?php echo form_dropdown('periode',$dropdown);?> <?php echo form_submit('submit','preview');?></td>
		</tr>
         <?php echo form_close();?>
    <tr><td colspan="3"><hr /></td></tr>
    <tr>
        <td>Total Omzet (Rp)</td>
        <td>:</td>
		<td><b><?php echo $results[0]['ftotalharga'];?></b></td>
	</tr>
    <tr>
        <td>Total Omzet (PV)</td>
        <td>:</td>
		<td><b><?php echo $results[0]['ftotalpv'];?></b></td>
	</tr>
    <tr>
        <td>Total Payout Bonus Rp. </td>
        <td>:</td>
		<td><b><?php echo $total['fnominal'];?></b></td>
	</tr>
    <tr>
        <td>Ratio Payout Terhadap Rp.</td>
        <td>:</td>
		<td><b><?php if(isset($total['nominal'])<1) $ratiorp =0; else $ratiorp = round(($total['nominal']/$results[0]['totalharga'])*100,2); echo $ratiorp."%";?></b></td>
	</tr>
    <tr>
        <td>Ratio Payout Terhadap PV</td>
        <td>:</td>
		<td><b><?php if(isset($total['nominal'])<1) $ratiopv =0; else $ratiopv = round(($total['nominal']/($results[0]['totalpv']*1000))*100,2); echo $ratiopv."%";?></b></td>
	</tr>               
	</table>
	
    <table class="stripe">
	<tr>
      <th width='5%'>No.</th>
      <th width='35%'>Description</th>
      <th width='15%'>PV</th>
      <th width='9%'>Persen %</th>
      <th width='20%'>Nominal Rp</th>
      <th width='8%'>% Rp</th>
      <th width='8%'>% PV</th>
    </tr>
   
<?php
if ($results): 
	foreach($results as $key => $row): ?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['titlebonus'];?></td>
      <td align="right"><?php echo $row['fpv'];?></td>
      <td align="right"><?php echo $row['persen'];?></td>
     <td align="right"><?php echo $row['fnominal'];?></td>
     <td align="right"><?php $rp = round(($row['nominal'] / $row['totalharga'])*100,2); echo $rp."%";?></td>
		<td align="right"><?php $pv = round(($row['nominal'] / ($row['totalpv']*1000))*100,2); echo $pv."%";?></td>
    </tr>
    <?php endforeach; ?>
	<tr>
	      <td colspan="4"><b>Total Bonus Rp. </b></td>
	     	<td align="right"><b><?php echo $total['fnominal'];?></b></td>
            <td align="right"><b><?php $ratiorp = round(($total['nominal']/$results[0]['totalharga'])*100,2); echo $ratiorp."%";?></b></td>
		<td align="right"><b><?php $ratiopv = round(($total['nominal']/($results[0]['totalpv']*1000))*100,2); echo $ratiopv."%";?></b></td>
    </tr>
<?php else: ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>			
                
                
<?php $this->load->view('footer');?>
