<br>
<?php 
if($row){
	foreach($row as $data):
		$pvg = $data['pvg'];
		$pv = $data['pv'];
	endforeach;
}

if($row2){
	foreach($row2 as $data):
		$ps = $data['pv'];
	endforeach;
}

?>
<table width='100%'>
<?php echo form_open('member/promo', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	
<?php if($this->session->userdata('group_id') > 100){ ?>
	<tr>
		<td width='24%'>Member ID / Name</td>
		<td width='1%'>:</td>
        <td width='75%'><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('userid')." / ".$this->session->userdata('name');?></b></td>
	</tr>   
  
<?php }else{ ?>
	<tr>
		<td width='24%' valign='top'>Member ID</td>
		<td width='1%' valign='top'>:</td>
		<td width='75%'>
			<?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
				echo form_input($data);?> <?php $atts = array(
				  'width'      => '450',
				  'height'     => '600',
				  'scrollbars' => 'yes',
				  'status'     => 'yes',
				  'resizable'  => 'yes',
				  'screenx'    => '0',
				  'screeny'    => '0'
				);
				echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
				?>
		</td>
	</tr>
	<tr>
		<td valign='top'>Name</td>
		<td valign='top'>:</td>
		<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
	</tr>
<?php }?>

	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
	</tr>
<?php echo form_close();?>				
</table>
<br>
<table>
	<tr>
		<td><b>Akumulasi Pembelanjaan 1 Feb 2010 s/d 31 Agustus 2010 :</b></td>
		<td align="right"><?php echo number_format($pv);?> PV</td>
		<td>&nbsp&nbsp&nbsp&nbsp&nbsp</td>
		<td><b>Pembelanjaan pribadi bulan ini :</b></td>
		<td align="right"><?php echo number_format($ps);?> PV</td>
	</tr>
	<tr>
		<td><b>Akumulasi PV Group 1 Feb 2010 s/d 31 Agustus 2010 :</b></td>
		<td align="right"><?php echo number_format($pvg);?> PV</td>
		<td>&nbsp </td>
		<td><b>Kekurangan pembelanjaan bulan ini :</b></td>
		<td align="right"><?php if((100-$ps) > 0){echo number_format(100-$ps)." PV";}else{echo "None";}?></td>
	</tr>
	<tr>
		<td>&nbsp </td>
		<td>&nbsp </td>
		<td>&nbsp </td>
		<td>&nbsp </td>
		<td>&nbsp </td>
	</tr>
	<tr>
		<td colspan="2"><b>Kekurangan Pencapaian Paket Gold(Akumulasi PV Group) :</b></td>
		<td colspan="2" align="right"><?php if((85000-$pvg)>0){echo number_format(85000-$pvg) ." PV";}else{echo "None";}?></td>
		<td>&nbsp </td>
	</tr>
	<tr>
		<td colspan="2"><b>Kekurangan Pencapaian Paket Silver(Akumulasi PV Group) :</b></td>
		<td colspan="2" align="right"><?php if((50000-$pvg)>0){echo number_format(50000-$pvg) ." PV";}else{echo "None";}?></td>
		<td>&nbsp </td>
	</tr>
</table>

<p><br>
Downline
<table class="stripe">
	<tr>
      <th><div align = "right">No.</div></th>
      <th><div align = "center">ID Downline</div></th>
      <th><div align = "left">Nama Downline</div></th>
      <th><div align="right">Total Group PV</div></th>
    </tr>

<?php
if ($results): 
	foreach($results as $key => $row): ?>
    <tr>
      <td align="right"><?php echo $row['i'];?></td>
      <td align="center"><?php echo $row['id'];?></td>
      <td align="left"><?php echo $row['nama'];?></td>
     <td align="right"><b><?php echo number_format($row['pvg_true']);?></b></td>
    </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
      <td colspan="4">Data is not available.</td>
    </tr>
<?php endif; ?> 
</table>