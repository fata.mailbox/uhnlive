<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php 
if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";
}						
//$this->load->view('member/topten_table');
//<b>Balance of ECB</b>
?>

<table border="1" bordercolor="#999999" class="stripe">
	<tr>
		<td colspan="11" style="border-bottom:solid thin #000099"></td>
	</tr>
	<tr>
		<th align="right" width="3%"><div align="right">No</div></th>
		<th align="left" width="10%"><div align="left">Member Id </div></th>
		<th align="left" width="27%"><div align="left">Member Name</div></th>
		<th align="right" width="20%"><div align="right">ECB </div></th>
		<th align="right" width="20%"><div align="right">Consumption </div></th>
		<th align="right" width="20%"><div align="right">Balance of ECB </div></th>
	</tr>
	<?
		if ($ecb):
			$flag=0;
			foreach($ecb as $dt1): 
				$flag+=1;
	?>
	<tr>
		<td align="right"><?php echo number_format($flag); 				?></td>
		<td align="left"><?php echo $dt1['member_id'];	?></td>
		<td align="left"><?php echo $dt1['nama'];			?></td>
		<td align="right"><?php echo number_format($dt1['ecb']); 			?></td>
		<td align="right"><?php echo number_format($dt1['totalout']); 			?></td>
		<td align="right"><?php echo number_format($dt1['saldo_ecb']); 			?></td>
	</tr>
	<?php
			endforeach;setlocale(LC_MONETARY, "en_US");
		else:
	?>
	<tr>
      <td colspan="11">Data is not available.</td>
    </tr>
	<?php
		endif;
	?>
	<tr>
		<td colspan="11" style="border-bottom:double medium #000099"></td>
	</tr>
</table>
<?php
$this->load->view('footer');
?>