<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->



<?php $this->load->view('header');?>
<?php if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}?>

 <?php echo form_open('order/rsoapp/edit/'.$row['id'], array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		<tr>
			<td width='24%'>approved date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php echo date('d-M-Y',now());?></td> 
		</tr>
		<tr>
			<td valign='top'>Stockiest No.</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_hidden('member_id',set_value('member_id','')); echo form_hidden('soid',set_value('soid',$row['id'])); echo form_hidden('total',set_value('total',$row['totalharga'])); $data = array('name'=>'no_stc','id'=>'no_stc','size'=>15,'readonly'=>'1','value'=>set_value('no_stc'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('stcsearch/all/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?><span class='error'>*<?php echo form_error('member_id');?></span></td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
  <tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?></td> 
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Approved');?></td>
		</tr>
		</table>
 <?php echo form_close();?>
<div class="ViewHold">
	<div id="companyDetails">
		<?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?>
		
	</div>

	<p>
		<strong>
			<?php echo "No. Invoice: ";?> <?php echo $row['id'];?><br />
			<?php
				echo $row['tgl']; // localized month
				echo "<br /><br />"; // day and year numbers
			?>
		</strong>
		
	</p>
	<br />
			<h2>Invoice Request Order</h2>
	<hr />

	<h3><?php echo $row['no_stc']," - ".$row['nama'];?></h3>

	<p>
		<?php
			echo $row['alamat']."<br />";
			echo $row['kota']." - ".$row['propinsi']." ".$row['kodepos'];
		?>
	</p>
	<p>
		<?php
			echo "Remark: ".$row['remark']."<br />";
			echo "Approved Date: ".$row['tglapproved']."<br />";
			echo "Remark Approved: ".$row['remarkapp'];
		?>
	</p>	
	
	<table class="stripe">
	<tr>
      <th width='10%'>Item Code</th>
      <th width='25%'>Item Name</th>
      <th width='10%'>Qty</th>
      <th width='10%'>Price</th>
      <th width='10%'>PV</th>
      <th width='20%'>Sub Total</th>      
      <th width='15%'>Sub Total PV</th>
    </tr>
    
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><?php echo $item['item_id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td align="right"><?php echo $item['fqty'];?></td>
			<td align="right"><?php echo $item['fharga'];?></td>
			<td align="right"><?php echo $item['fpv'];?></td>
			<td align="right"><?php echo $item['fsubtotal'];?></td>
			<td align="right"><?php echo $item['fsubtotalpv'];?></td>
		</tr>
		<?php endforeach;?>
		<tr>
			<td colspan='5' align='right'><b>Total Rp.</b>&nbsp;</td>
			<td align="right"><b><?php echo $row['ftotalharga'];?></b></td>
			<td align="right"><b><?php echo $row['ftotalpv'];?></b></td>
		</tr>
	</table>
	
<?php
$this->load->view('footer');
?>
