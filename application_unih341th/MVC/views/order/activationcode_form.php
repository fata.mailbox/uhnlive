<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('order/ac/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		
		<table width='100%'>
		<tr>
			<td width='19%'>date</td>
			<td width='1%'>:</td>
			<td width='80%'><?php echo date('Y-m-d');?></td> 
		</tr>
        <tr>
			<td>stockiest id / name</td>
			<td>:</td>
			<td><?php echo $this->session->userdata('username')." / ".$this->session->userdata('name');?></td>
		</tr>
		
	<?php 
		// Admin
		if($this->session->userdata('group_id') <= 100){ 
	?>
		
		<tr>
			<td>Cabang</td>
			<td>:</td>
			<td><?php echo form_dropdown('warehouse_id',$warehouse);?></td>
		</tr>
		<tr>
			<td valign='top'>Quantity</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'qty','size'=>'5','class'=>'textbold','maxlength'=>'3','value'=>set_value('qty',1)); echo form_input($data);?>
			 <span class='error'>*<?php echo form_error('qty');?></span></td>
		</tr>
		<tr>
			<td>Varian of S.Kit</td>
			<td>:</td>
			<td><?php echo form_dropdown('skit',$skit);?></td>
		</tr>
		<!-- created by Boby 20130626 -->
		<tr>
			<td>Kit for internal</td>
			<td>:</td>
			<td><?php echo form_dropdown('yn',$yn);?></td>
		</tr>
		<!-- end created by Boby 20130626 -->
	<?php }
		// Stc
		else {
			if($qty){
	?>
	
		<tr>
			<td>stock titipan kit</td>
			<td>:</td>
			<td><?php echo $qty;?></td>
		</tr>
		
	<?php 
			}
	?>
	
		<tr>
			<td valign='top'>Quantity</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'qty','size'=>'5','class'=>'textbold','maxlength'=>'3','value'=>set_value('qty',1)); echo form_input($data);?>
			 <span class='error'>*<?php echo form_error('qty');?></span></td>
		</tr>
		<tr>
			<td>Varian of S.Kit</td>
			<td>:</td>
			<td><?php echo form_dropdown('skit',$skit);?></td>
		</tr>
		<tr>
			<td valign='top'>PIN</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'pin','size'=>'15','maxlength'=>'50');
				echo form_password($data);?> <span class='error'>*<?php echo form_error('pin');?></span></td>
		</tr>
		
	<?php
		}
	?>
	
		<tr>
			<td>&nbsp;</td>						
			<td>&nbsp;</td>
			<td><?php echo form_submit('submit','Submit');?></td>
		</tr>
	</table>
</form>						
<?php $this->load->view('footer');?>
