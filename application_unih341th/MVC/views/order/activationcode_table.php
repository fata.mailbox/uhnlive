<table width='99%'>
<?php echo form_open('order/ac', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='60%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='40%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b>
			<?php }?>
			
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<?php echo form_open('order/ac/reject/', array('id' => 'my_form2', 'name' => 'my_form2', 'autocomplete' => 'off'));?>
<table class="stripe">
	<tr>
		<td colspan='8'><?php echo form_submit('submit','reject');?></td>
	</tr>

	<tr>
	  <th width='5%'>&nbsp;</th>
      <th width='7%'>No.</th>
      <th width='8%'>Stc ID</th>
	  <th width='9%'>Variant of Kit</th>
      <th width='12%'>Member ID</th>
      <th width='25%'>Activation Code (Name Member)</th>
      <th width='10%'>Date</th>      
      <th width='12%'>Status</th>
      <th width='12%'>Used Date</th>
    </tr>
   
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
    <td><?php if($row['status'] == 'not used'){ 
					$data = array(
    'name'        => 'p_id[]',
    'id'          => 'p_id[]',
    'value'       => $row['id'],
    'checked'     => false,
    'style'       => 'border:none'
    );					
					echo form_checkbox($data); } else {?>&nbsp;<?php }?> </td>
      <td><?php echo $counter;?></td>
      <td><?php if($row['warehouse_id'] == 0) echo $row['no_stc']; else echo $row['warehouse_id'];?></td>
      <td><?php echo $row['name'];?></td>
	  <td><?php echo $row['member_id'];?></td>
      <td><?php if($row['status'] == 'not used') echo "<b>".$row['code']."</b>"; elseif($row['status'] == 'used') echo $row['nama']; else echo " "; ?></td>
      <td><?php echo $row['created'];?></td>
      <td><?php echo $row['status'];?></td>
		<td><?php echo $row['useddate'];?></td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="8">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php echo form_close();?>