
<table width='100%'>
<?php echo form_open('order/hso', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  echo form_input($data);?>   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
   echo "to: ".form_input($data);?>
   </td>
  </tr>
  
  <?php if($this->session->userdata('group_id') > 100){ ?>
  <tr>
  <td width='24%'>Member ID / Name</td>
		<td width='1%'>:</td>
        <td width='75%'><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('userid')." / ".$this->session->userdata('name');?></b></td>
     </tr>   
  
  <?php }else{ ?>
<tr>
			<td width='24%' valign='top'>Member ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='75%'><?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '450',
              'height'     => '600',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?></td>
					
		</tr>
<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
        <?php }?>
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
	</tr>
<?php echo form_close();?>				
</table>

<table width="100%">
	<tr>
      <td colspan="9" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <td width='5%'><b>No.</b></td>
      <td width='10%'><b>Invoice No.</b></td>
      <td width='10%'><b>Date</b></td>
      <td width='20%'><b>Member ID / Name</b></td>
      <td width='5%'><b>STC ID</b></td>
      <td width='10%'><b>Total Price</b></td>
      <td width='10%'><b>Total PV</b></td>
      <td width='20%'><b>Remark</b></td>
      <td width='10%'><b>Created Date</b></td>
   </tr>
   <tr>
      <td colspan="9" style="border-bottom:dashed thin #666666"></td>
  </tr>
   <tr>
      <td colspan="9"  style="border-bottom:solid thin #000099">
      	<table width="100%">
        	<tr>
	            <td width="5%" class="cName">&nbsp;</td>
            	<td width="5%" class="cName"><em>No.</em></td>
                <td width="10%" class="cName"><em>Item ID</em></td>
                <td width="20%" class="cName"><em>Nama</em></td>
                <td width="10%" class="cName" align="center"><em>Qty</em></td>
                <td width="10%" class="cName" align="center"><em>Price</em></td>
                <td width="10%" class="cName" align="center"><em>PV</em></td>
                <td width="15%" class="cName" align="center"><em>Jml Harga</em></td>
                <td width="15%" class="cName" align="center"><em>Jml Harga</em></td>
          </tr>
        </table>
     </td>    
</tr>
   
<?php
if ($results):
	foreach($results as $row): 
?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['id'];?></td>
      <td><?php echo $row['tgl'];?></td>
      <td><?php echo $row['member_id']." / ".$row['namamember'];?></td>
      <td><?php echo $row['no_stc'];?></td>
      <td align="right"><?php echo $row['ftotalharga'];?></td>
      <td align="right"><?php echo $row['ftotalpv'];?></td>
      <td align="right"><?php echo $row['remark'];?></td>
      <td><?php echo $row['created'];?></td>
    </tr>
     <tr>
      <td colspan="9" style="border-bottom:dashed thin #666666"></td>
  </tr>

    <?php foreach($row['details'] as $row2):?>
    
     <tr>
      <td colspan="9">
      	<table width="100%">
        	<tr>
	            <td width="5%" class="cName">&nbsp;</td>
            	<td width="5%" class="cName"><em>
           	    <?php echo $row2['i'];?>
            	</em></td>
                <td width="10%" class="cName"><em>
                <?php echo $row2['item_id'];?>
                </em></td>
                <td width="20%" class="cName"><em>
                <?php echo $row2['name'];?>
                </em></td>
                <td width="10%" class="cName" align="right"><em>
                <?php echo $row2['fqty'];?>
                </em></td>
                <td width="10%" class="cName" align="right"><em>
                <?php echo $row2['fharga'];?>
                </em></td>
                <td width="10%" class="cName" align="right"><em>
                <?php echo $row2['fpv'];?>
                </em></td>
                <td width="15%" class="cName" align="right"><em>
                <?php echo $row2['fjmlharga'];?>
                </em></td>
                <td width="15%" class="cName" align="right"><em>
                <?php echo $row2['fjmlpv'];?>
                </em></td>
          </tr>
        </table>
      </td>    
</tr>
  <?php endforeach;?>
  <tr>
      <td colspan="9" style="border-bottom:solid thin #000099"></td>
    </tr>
  <?php endforeach; ?>
  
  <tr>
      <td colspan="4" align="right"><b>Grand Total: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td colspan="2" align="right"><b><?php echo $total->ftotalharga;?></b></td>
      <td colspan="2" align="right"><b><?php echo $total->ftotalpv;?></b></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="9" style="border-bottom:double medium #000099"></td>
    </tr>
    
 <?php else: ?>
    <tr>
      <td colspan="9">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>
