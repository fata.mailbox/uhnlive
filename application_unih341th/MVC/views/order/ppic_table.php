
<table width='100%'>
<?php echo form_open('order/ppic', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'>
			<?php 
				$data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  
				echo form_input($data);
			?>   
			<?php 
				$data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
				echo "to: ".form_input($data);
			?>
   </td>
  </tr>
 
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
  </tr>
<?php echo form_close();?>				
</table>


<table border="1" bordercolor="#999999" class="stripe">
	<tr>
		<td colspan="11" style="border-bottom:solid thin #000099"></td>
	</tr>
	<tr>
		<th width="5%" rowspan="2" align="right"><div align="left">No</div></th>
		<th width="5%" rowspan="2" align="right">Part No </th>
		<th width="10%" rowspan="2" align="right">Product</th>
		<th width="10%" colspan="1" rowspan="2" align="right"><div align="center">Qty Onhand<br>(31-12-2009)</div>		  </th>
		<th width="60%" colspan="6" align="right"><div align="center">Sales</div>		  </th>
		<th width="10%" colspan="1" rowspan="2" align="right"><div align="center">01-12-2009<br>31-12-2009</div></th>
	</tr>
	<tr>
		<th align="right" width="10%"><div align="right">Apr-2009</div></th>
		<th align="right" width="10%"><div align="right">May-2009</div></th>
		<th align="right" width="10%"><div align="right">Jun-2009</div></th>
		<th align="right" width="10%"><div align="right">Jul-2009</div></th>
		<th align="right" width="10%"><div align="right">Aug-2009</div></th>
		<th align="right" width="10%"><div align="right">Sep-2009</div></th>
	</tr>
	<tr>
		<td colspan="13" style="border-bottom:solid thin #000099"></td>
	</tr>
	<?
		if ($curr):
			foreach($curr as $dt1): 
	?>
	<tr>
		<td align="left"><?php echo $dt1['i'];?></td>
		<td align="left"><?php echo $dt1['part_no'];?></td>
		<td align="left"><?php echo $dt1['Description'];?></td>
		<td align="right"><?php echo $dt1['id'];?></td>
		<td align="right">&nbsp;</td>
		<td align="right">&nbsp;</td>
		<td align="right">&nbsp;</td>
		<td align="right">&nbsp;</td>
		<td align="right">&nbsp;</td>
		<td align="right"><?php if($bln1){echo $bln1['qty'];}else{echo 'nope';}?></td>
		<td align="right"><?php echo number_format($dt1['qty']);?></td>
	</tr>
	<?php
			endforeach;
			setlocale(LC_MONETARY, "en_US");
	?>
	<tr>
		<td align="left">&nbsp;</td>
		<td align="left">&nbsp;</td>
		<td align="left">&nbsp;</td>
		<td align="right">&nbsp;</td>
		<td align="right">&nbsp;</td>
		<td align="right">&nbsp;</td>
		<td align="right">&nbsp;</td>
		<td align="right">&nbsp;</td>
		<td align="right">&nbsp;</td>
		<td align="right">&nbsp;</td>
		<td align="right">&nbsp;</td>
	</tr>
	<?php
		else:
	?>
	<tr>
      <td colspan="13">Data is not available.</td>
    </tr>
	<?php
		endif;
	?>
	<tr>
		<td colspan="13" style="border-bottom:double medium #000099"></td>
	</tr>
</table>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
</script>
