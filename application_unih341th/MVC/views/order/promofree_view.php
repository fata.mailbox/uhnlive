<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<?php if($row['status'] == 'pending' && $this->session->userdata('group_id') <= 100){?>
	<div id="view"><?php echo anchor('order/freeso/approved/'.$row['id'],'approved delivered');?></div>
<?php }?>

<div class="ViewHold">
	<div id="companyDetails">
		<?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?>
		
	</div>

	<p>
		<strong>
			<?php echo "No. Invoice: ";?> <?php echo $row['id'];?><br />
			<?php
				if($row['approveddate'] != '0000-00-00'){
				echo date('F', mysql_to_unix($row['approveddate'])); // localized month
				echo date(' j, Y', mysql_to_unix($row['approveddate']))."<br /><br />"; // day and year numbers
				}else echo '0000-00-00';
			?>
		</strong>
		
	</p>
	<br />
			<h2>Free Product Promo</h2>
	<hr />

	<h3><?php echo $row['no_stc']," - ".$row['nama'];?></h3>

	<p>
		<?php
			echo $row['alamat']."<br />";
			echo $row['kota']." - ".$row['propinsi']." ".$row['kodepos'];
		?>
	</p>
	<p>
		<?php
			echo "Status: ".$row['status']."<br />";
			echo "Remark Delivered: ".$row['remark']."<br />";
			echo "User ID: ".$row['approvedby'];
		?>
	</p>	
	
	<table class="stripe">
	<tr>
      <th width='15%'>Item Code</th>
      <th width='30%'>Item Name</th>
      <th width='10%'><div align="right">Qty</div></th>
      <th width='30%'>Member ID / Nama Member</th>
      <th width='15%'>Date</th>
    </tr>
    
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><?php echo $item['item_id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td align="right"><?php echo $item['fqty'];?></td>
			<td><?php echo $item['member_id']." / ".$item['nama'];?></td>
			<td><?php echo $item['tgl'];?></td>
		</tr>
		<?php endforeach;?>
		
	</table>
	
<?php $this->load->view('footer');?>
