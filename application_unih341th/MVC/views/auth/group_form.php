<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
    
	<?php echo form_open('auth/group/create', array('id' => 'roleForm', 'name' => 'roleForm', 'autocomplete' => 'off'));?> 
		<table width="99%">
		<tr>
			<td width='14%' valign='top'>Group ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php $data = array('name'=>'groupid','id'=>'groupid','size'=>5,'value'=>set_value('groupid'));
    echo form_input($data);?> <span class='error'>*<?php echo form_error('groupid'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Group Nama</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'groupname','id'=>'groupname','size'=>30,'value'=>set_value('groupname'));
    echo form_input($data);?> <span class='error'>*<?php echo form_error('groupname'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top' colspan='3'>
			 
				<table width="68%" align="center">
            	<tr>
               	<td width="40%">
               	<?php if(isset($results)): ?>
                		<select name="viewLstId" multiple="multiple" size="20" style="width:250px;">
                			<?php	foreach($results as $key => $row): ?>
										<option value="<?php echo $row['id'];?>"><?=$row['sub_menu'];?> &raquo; <?=$row['menu'];?></option>
									<?php endforeach;?>
							</select>
						<?php endif;?>
                	</td>
                	<td width="10%">
                		<table width="99%">
								<tr>
									<td>
										<A HREF="javascript:copyToList('viewLstId','viewSltdLstId')"><img src="/images/backend/right.jpg" border="0"></A>									      			
									</td>
						      </tr>
								<tr>
									<td><A HREF="javascript:copyToList('viewSltdLstId','viewLstId')"><img src="/images/backend/left.jpg" border="0"></A></td>
								</tr>
					    	</table>
					   </td>
                	<td width="40%">
                		<select name="viewSltdLstId[]" id="viewSltdLstId" multiple="multiple" size="20" style="width:250px;">
                			
                		</select>         
                	</td>
                	<td width="10%">
                		
                		<table width="99%">
								<tr>
									<td><a href="javascript:up('viewSltdLstId')"><img src="/images/backend/top.jpg" border="0"></a></td>
						      </tr>
								<tr>
									<td><a href="javascript:down('viewSltdLstId')"><img src="/images/backend/down.jpg" border="0"></a></td>
								</tr>
					    	</table>
					    	
					    </td>
                </tr>
               </table> 
         
        </td> 
		</tr>
		<tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td> <input name="xxx" class="button" type="button" value="create group user" onClick="javascript:selectAll('viewSltdLstId');document.roleForm.submit();"> <input type="button" name="cancel" value="Cancel" onclick="history.go(-1);" class='button' /></td> 
		</tr>
		</table>
<?php echo form_close();?>
		
		<script language="javascript">

function selectAll(col1){
	col1 = eval('document.forms[0].'+col1);
  for(i=0; i<col1.options.length; i++ ){
		col1.options[i].selected = true;
	}
}

function up(col1){
	col1 = eval('document.forms[0].'+col1);
  index = col1.selectedIndex;
  //alert(index);
  if (index == -1) {
  	alert ('You haven\'t selected any options!');
  } else {
	  if(index <= 0) {
	  	alert("Can't move to up!");
	  } else {
		  toMoveX = col1.options[index-1];
		  toMoveY = col1.options[index];
		  optX = new Option(toMoveX.text,toMoveX.value,false,false);
		  optY = new Option(toMoveY.text,toMoveY.value,false,false);
		  col1.options[index] = optX;
		  col1.options[index-1] = optY;
		  col1.selectedIndex = index-1;
		}
	}
}

function down(col1){
	col1 = eval('document.forms[0].'+col1);
  index = col1.selectedIndex;
  if (index == -1) {
  	alert ('You haven\'t selected any options!');
  } else {
	  if(index+1 >=  col1.options.length) {
	  	alert("Can't move to down!");
	 	} else {
		  toMoveX = col1.options[index];
		  toMoveY = col1.options[index+1];
		  optX = new Option(toMoveX.text,toMoveX.value,false,false);
		  optY = new Option(toMoveY.text,toMoveY.value,false,false);
		  col1.options[index] = optY;
		  col1.options[index+1] = optX;
		  col1.selectedIndex = index+1;
		}
	}
}
function copyToList(from,to) {
  fromList = eval('document.forms[0].' + from);
  toList = eval('document.forms[0].' + to);
  if (toList.options.length > 0 && toList.options[0].value == 'temp') {
    toList.options.length = 0;
  }
  var sel = false;
  for (i=0;i<fromList.options.length;i++) {
    var current = fromList.options[i];
    if (current.selected) {
      sel = true;
      if (current.value == 'temp') {
        alert ('You cannot move this text!');
        return;
      }
      txt = current.text;
      val = current.value;
      toList.options[toList.length] = new Option(txt,val);
      fromList.options[i] = null;
      i--;
    }
  }
  if (!sel) alert ('You haven\'t selected any options!');
}

function page_submit() {
	document.forms[0].submit();
};
</script>

<?php
$this->load->view('footer');
?>
