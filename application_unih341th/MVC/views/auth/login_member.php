<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<div>
	<?php echo form_open('login', array('id' => 'form'));?>
		
		<table width='50%'>
		<tr>
			<td width='20%' valign='top'>Username</td>
			<td width='1%' valign='top'>:</td>
			<td width='79%'><input type="text" name="username" id="username" autocomplete="off" value="" maxlength="50" size="20" />
			<span class="error"><?php echo form_error('username');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Password</td>
			<td valign='top'>:</td>
			<td><input type="password" name="password" id="password" autocomplete="off" value="" maxlength="100" size="20" /> 
			<span class="error"><?php echo form_error('password');?></span></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td><?php echo form_submit('submit', 'login');?></td> 
		</tr>	
        <tr>
			<td colspan="3">&nbsp;</td>
		</tr>	
        <tr>
        	<td colspan="3">Jika ingin login sebagai stockiest atau m-stockist klik <?php echo anchor('stclogin','disini');?></td> 
		</tr>	
		</table>
<?php echo form_close();?>
</div>

<?php $this->load->view('footer');?>