<?php 
	/* ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- */
	/* Begin Part 1 Template */ // Created by Boby 20130625
	$this->load->view('header');
	echo "<h2>".$page_title."</h2>";
	echo anchor('auth/rellocewallet/create','Create Relocation Ewallet');
	if($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>
	<table width="99%">
	<?php echo form_open($base_url, array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search by: 
			<?php 
				$data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    			echo form_input($data);?> <?php echo form_submit('submit','go');
				if($this->session->userdata('keywords')){echo "<br/>Your search keywords : <b>".$this->session->userdata('keywords')."</b>";}
			?>
    	</td>
	</tr>
	<?php echo form_close();?>				
	</table>
<?php
	/* End Part 1 Template */
	
	/* ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- */
	/* Content (edit for header field) */
?>
<table class="stripe">
	<?php /* Header of table */ ?>
	<tr>
		<th>No.</th>
		<th>Source Member</th>
		<th>Destination Member</th>
		<th>Grant Option</th>
		<th>Last Updated/Created</th>
		<th>Created/Updated by</th>
	</tr>
	<?php /* End header of table */ ?>
	
<?php
	/* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== */
	/* Looping data table */
if (isset($results)):
	$counter = $from_rows;
	foreach($results as $key => $row):
		$counter = $counter+1;
?>
    <tr>
      <td><?php echo $counter;?></td>
      <td><?php echo $row['src'];?></td>
      <td><?php echo $row['dsc'];?></td>
      <td><?php echo $row['note'];?></td>
      <td><?php echo $row['created'];?></td>
      <td><?php echo $row['createdby'];?></td>
    </tr>
<?php 
	endforeach;
	/* End looping data table*/
	/* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== */
	else:
 ?>
    <tr>
      <td colspan="<?php echo $field;?>">Data is not available.</td>
    </tr>
<?php endif; ?>
</table>
<?php $this->load->view('paging');?>
<?php /* End Content */?>

<?php $this->load->view('footer');?>