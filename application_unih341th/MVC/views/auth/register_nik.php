<?php 
	$this->load->view('header');
	if($result){
		$emp_id = $result['emp_id'];
		$empname = $result['nama'];
		$member_id = $result['mid'];
		$membername = $result['nama_member'];
		//$page_title = "Get";
	}
?>
<h2><?php echo $page_title;?></h2>
<?php
	if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}
?>
<?php echo form_open('auth/regnik/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		
		<table width='100%'>
		<?php if($this->session->userdata('group_id') <= 100){ ?>
		<tr>
			<td width='19%' valign='top'>Employee ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php 
				$data = array('name'=>'emp_id','id'=>'emp_id','size'=>15,'value'=>$emp_id);
				echo form_input($data);?> <?php $atts = array(
					  'width'      => '400',
					  'height'     => '400',
					  'scrollbars' => 'yes',
					  'status'     => 'yes',
					  'resizable'  => 'yes',
					  'screenx'    => '0',
					  'screeny'    => '0'
					);
				echo anchor_popup('search/searchempmem/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
				?><span class='error'>*<?php echo form_error('emp_id');?></span>
			</td>
		</tr>
		<tr>
			<td valign='top'>Employee Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="emp_name" id="emp_name" value="<?php echo $empname;?>" size="30" /></td>
		</tr>
		<tr>
			<td width='19%' valign='top'>Member ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php 
				$data = array('name'=>'member_id','id'=>'member_id','size'=>15, 'readonly'=>1 ,'value'=>$member_id);
				echo form_input($data);?> <?php $atts = array(
					  'width'      => '400',
					  'height'     => '400',
					  'scrollbars' => 'yes',
					  'status'     => 'yes',
					  'resizable'  => 'yes',
					  'screenx'    => '0',
					  'screeny'    => '0'
					);
				echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
				?><span class='error'>*<?php echo form_error('member_id');?></span>
			</td>
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo $membername;?>" size="30" /></td>
		</tr>
        <?php }else{ ?>
        <tr>
			<td width='19%' valign='top'>Member ID / Nama </td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('userid')." / ".$this->session->userdata('name');?></b></td>
		</tr>
        <?php }?>
		<tr>
			<td></td>
			<td></td>
			<td><?php echo form_submit('submit', 'Submit');?></td> 
		</tr>
		</table>
	<?php echo form_close();?>
<?php $this->load->view('footer');?>
