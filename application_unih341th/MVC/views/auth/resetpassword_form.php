<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<?php echo form_open('auth/rp/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		
		<table width='100%'>
		<tr>
			<td valign='top'>Member ID</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '450',
              'height'     => '600',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('memsearch/ewallet', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?>				
					 <span class='error'>*<?php echo form_error('member_id');?></span></td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
        <tr>
			<td valign='top'>Ewallet Rp</td>
			<td valign='top'>:</td>
			<td><input type="text" name="ewallet" id="ewallet" readonly="1" value="<?php echo set_value('ewallet');?>" size="15" /><span class='error'><?php echo form_error('ewallet');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Reset Password</td>
			<td valign='top'>:</td>
			<td><input type="checkbox" name="passwd[]" value="1" <?php echo  set_checkbox('passwd', '1') ?>/></td>
		</tr>
		<tr>
			<td valign="top">Reset PIN</td>
			<td valign="top">:</td>
			<td><input type="checkbox" name="pin[]" value="1" <?php echo  set_checkbox('pin', '1') ?>/><span class='error'><?php echo form_error('pin');?></span></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td><?php echo form_submit('submit', 'Submit');?></td> 
		</tr>
		</table>
	<?php echo form_close();?>
<?php $this->load->view('footer');?>
