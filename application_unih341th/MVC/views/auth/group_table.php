<table width="99%">
<?php echo form_open('auth/group', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><?php $data = array(
			'id' => 'btn', 
			'content' => 'Form '.$page_title, 
			'onClick' => "location.href='".site_url()."auth/group/create'"
		); echo form_button($data); ?></td>
		<td width='60%' align='right'>Cari: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?=form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Kata kunci: <b><?=$this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?=form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='10%'>No.</th>
      <th width='10%'>Group ID</th>
      <th width='70%' class="acenter">Title</th>
      <th width='10%'>Action</th>      
    </tr>
<?php
if ($results):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo $counter;?></td>
      <td><?php echo $row['id'];?></td>
      <td><?php echo $row['title'];?></td>
      <td>
      	<table>
        	<tr>
            <td><a href="<?php echo site_url()."auth/group/view/".$row['id'];?>"><img src="/images/backend/detail.gif" alt="view" title="view"></a></td>
            <td>
      	<a href="<?php echo site_url()."auth/group/edit/".$row['id'];?>"><img src="/images/backend/edit.gif" alt="edit" title="edit"></a></td>
            <td><a href="<?php echo site_url()."auth/group/editauth/".$row['id'];?>"><img src="/images/backend/b_useredit.png" alt="edit auth" title="edit auth"></a></td>
            </tr>
          </table>
      </td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="4">Data tidak tersedia.</td>
    </tr>
<?php endif; ?>    
</table>
<?php $this->load->view('paging');?>