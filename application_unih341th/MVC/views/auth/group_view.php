<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>

<div class="ViewHold">
	<div id="companyDetails">
		<?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?>
	</div>

	<p>
		<strong>
			<?php echo "Group ID: ";?> <?php echo $row[0]['id'];?>
			
		</strong>
		
	</p>
	<br />
<h2><?=$page_title;?></h2>
	<hr />

	<h3><?php echo $row[0]['grup'];?></h3>

<table class="stripe">
	<tr>
      <th width='52%'>Menu</th>
      <th width='12%'>View</th>
      <th width='12%'>Save</th>
      <th width='12%'>Edit</th>
      <th width='12%'>Delete</th>
   </tr>
   <?php if(isset($row)): 
   	foreach($row as $key => $row): ?>
	<tr>
		<td><?=$row['submenu'];?> &raquo; <?=$row['menu'];?></td>
		<td><?=$row['view'];?></td>
		<td><?=$row['save'];?></td>
		<td><?=$row['edit'];?></td>
		<td><?=$row['del'];?></td>
	</tr>
	<?php endforeach;?>
	<?php else: ?>
	<tr>
      <td colspan="5">Data tidak tersedia.</td>
    </tr>
	<?php endif; ?>
</table>
				
<?php $this->load->view('footer');?>
