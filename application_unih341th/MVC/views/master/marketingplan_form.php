<?php
$this->load->view('header');
?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('master/mp/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		
		<table width='99%'>
		<tr>
			<td width='14%' valign='top'>category</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php $data = array('name'=>'category','id'=>'category','value'=>set_value('category'));
    echo form_input($data);?> <span class='error'>*<?php echo form_error('category'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Long Description</td>
			<td valign='top'>:</td>
			<td><?php $data = array(
              'name'        => 'longdesc',
              'id'          => 'longdesc',
              'toolbarset'  => 'Default',
              'basepath'    => '/fckeditor/',
              'width'       => '98%',
              'height'      => '400',
              'value'		 => set_value('longdesc')
              );
	echo form_fckeditor($data);?> <span class='error'>*<?php echo form_error('longdesc'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>No. Sort</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'sort','id'=>'sort','size'=>5,'value'=>set_value('sort'));
    echo form_input($data);?> <span class='error'>*<?php echo form_error('sort'); ?></span></td> 
		</tr>
        <tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td><?php $options = array('active' => 'active', 'inactive' => 'inactive');
    echo form_dropdown('status',$options,set_value('status'));?> </td> 
		</tr>
		<tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><?php echo form_submit('submit','create marketing plan');?></td> 
		</tr>
		</table>
		<?php echo form_close();?>
		
<?php
$this->load->view('footer');
?>
