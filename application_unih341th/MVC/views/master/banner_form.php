<?php
$this->load->view('header');
?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open_multipart('master/banner/create/');?>
		
		<?php echo form_open_multipart(current_url(), array('id' => 'form', 'name' => 'form','class'=>'form', 'autocomplete' => 'off'),$hidden);?>
		
		<table  class="formtable">
		<tr>
			<td valign='top'>Exp. Date</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate')); echo form_input($data);?></td> 
		    <td><button id="f_btn1" class="calIcon" onclick = "return false">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button></td>
		</tr>        
        <tr>
			<td valign='top'>Title</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'title','id'=>'title','size'=>'50','value'=>set_value('title'));
    echo form_input($data);?></td> 
		    <td>&nbsp;</td>
        </tr>
	
    <tr>
			<td valign='top'>Link URL</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'url','id'=>'url','size'=>'50','value'=>set_value('url'));
    echo form_input($data);?></td> 
		    <td>&nbsp;</td>
        </tr>
		
        <tr>
			<td valign='top'>Description</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'shortdesc','id'=>'shortdesc','rows'=>2, 'cols'=>'40','class'=>'txtarea','value'=>set_value('shortdesc'));
    echo form_textarea($data);?></td> 
		    <td>&nbsp;</td>
		</tr>
		<tr>
			<td valign='top'>File Banner (1000px X 300px)</td>
			<td valign='top'>:</td>
			<td><input type="file" name="userfile" size="20" />
 <span class='error'><?php if(isset($error)) print_r($error['error']);?></span>	</td> 
		    <td>&nbsp;</td>
		</tr>
		
		<tr>
			<td valign='top'>order by (a-z)</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'nourut','id'=>'nourut','value'=>set_value('nourut','0'));
    echo form_input($data);?><span class='error'><?=form_error('title');?></span></td> 
		    <td>&nbsp;</td>
		</tr>
		<tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td><?php $options = array('active' => 'active', 'inactive' => 'inactive');
    echo form_dropdown('status',$options);?> </td> 
		    <td>&nbsp;</td>
		</tr>
		</table>
		<tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><input type="submit" value="Submit" /></td> 
		</tr>
	<?php echo form_close();?>
		
<?php
$this->load->view('footer');
?>

	
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>
