
<?php
$this->load->view('header');
?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open_multipart('master/whatnew/create');?>
		
		<table width='99%'>
		<tr>
			<td width='14%' valign='top'>Title</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php $data = array('name'=>'title','id'=>'title','rows'=>2, 'cols'=>'40','class'=>'txtarea','value'=>set_value('title'));
    echo form_textarea($data);?> <span class='error'>*<?php echo form_error('title'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Long Description</td>
			<td valign='top'>:</td>
			<td><?php $data = array(
              'name'        => 'longdesc',
              'id'          => 'longdesc',
              'toolbarset'  => 'Default',
              'basepath'    => '/fckeditor/',
              'width'       => '98%',
              'height'      => '400',
              'value'		 => set_value('longdesc')
              );
	echo form_fckeditor($data);?> <span class='error'>*<?php echo form_error('longdesc'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td><?php $options = array('active' => 'active', 'inactive' => 'inactive');
    echo form_dropdown('status',$options,set_value('status'));?> </td> 
		</tr>
		<tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><input type="submit" value="Submit" /></td> 
		</tr>
		</table>
		</form>
		
<?php
$this->load->view('footer');
?>
