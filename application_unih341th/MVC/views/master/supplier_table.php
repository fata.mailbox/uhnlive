<table width="99%">
<?php echo form_open('master/supplier/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='5%'>No.</th>
      <th width='25%'>Supplier Name</th>
      <th width='30%'>Address</th>
      <th width='10%'>Telp</th>      
      <th width='10%'>fax</th>
      <th width='20%'>contact person</th>
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor("/master/supplier/view/".$row['id'],$counter);?></td>
      <td><?php echo anchor("/master/supplier/view/".$row['id'],$row['name']);?></td>
      <td><?php echo anchor("/master/supplier/view/".$row['id'],$row['address']." ");?></td>
      <td><?php echo anchor("/master/supplier/view/".$row['id'],$row['telp']." ");?></td>
		<td><?php echo anchor("/master/supplier/view/".$row['id'],$row['fax']." ");?></td>
       <td><?php echo anchor("/master/supplier/view/".$row['id'],$row['cp']." ");?></td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
