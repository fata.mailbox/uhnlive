<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('master/bank/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		<tr>
			<td width='24%'>date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td valign='top'>Bank ID</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="id" value="<?php echo set_value('id');?>" size"20" />
			<span class="error">* <?php echo form_error('id');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Bank Name</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="name" value="<?php echo set_value('name');?>" size"30" /></td>
		</tr>
		<tr>
			<td valign='top'>Account No.</td>
			<td valign='top'>:</td>
			<td><input type="text" name="accountno" value="<?php echo set_value('accountno');?>" size"30" /></td> 
		</tr>
		<tr>
			<td valign='top'>Account Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="accountname" value="<?php echo set_value('accountname');?>" size"30" /></td>
		</tr>
		<tr>
			<td valign='top'>Account Area</td>
			<td valign='top'>:</td>
			<td><input type="text" name="accountarea" value="<?php echo set_value('accountarea');?>" size"30" /></td>
		</tr>
		<tr>
			<td valign='top'>Bank Company</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $options = array('Y' => 'Yes','N' => 'No');
    				echo form_dropdown('status',$options);?></td> 
		</tr>
		
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>
		</table>

<?php echo form_close();?>
<?php $this->load->view('footer');?>
