<?php
$this->load->view('header');
?>
<h2><?php echo $page_title;?></h2>
<?php if($_POST){
		$title = set_value('title');
		$shortdesc = set_value('shortdesc');
		$longdesc = set_value('longdesc');
		$status = set_value('status');
	}else{
		$title = $row['title'];
		$shortdesc = $row['shortdesc'];
		$longdesc = $row['longdesc'];
		$status = $row['status'];
	}?>
	
<?php echo form_open_multipart('master/testimaster/edit/'.$this->uri->segment(3));?>
		
		<table width='99%'>
		<tr>
			<td width='14%' valign='top'>Title Testimonial</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php $data = array('name'=>'title','id'=>'title','rows'=>2, 'cols'=>'40','class'=>'txtarea','value'=>$title);
    echo form_textarea($data);?> <span class='error'>*<?php echo form_error('title'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Short Description</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'shortdesc','id'=>'shortdesc','rows'=>2, 'cols'=>'40','class'=>'txtarea','value'=>$shortdesc);
    echo form_textarea($data);?> <span class='error'>*<?php echo form_error('shortdesc'); ?></span>	</td> 
		</tr>
        <tr>
			<td valign='top'>Nama File Thumbnail</td>
			<td valign='top'>:</td>
			<td><?php echo form_hidden('namafile',$row['thumbnail']);
				echo form_hidden('id',$row['id']);
				echo $row['thumbnail'];?></td> 
		</tr>
        <tr>
			<td valign='top'>Image  Thumbnail</td>
			<td valign='top'>:</td>
			<td><input type="file" name="userfile" size="20" />
 <span class='error'>*<?php if(isset($error)) echo $error['error'];?></span>	</td> 
		</tr>
		<tr>
			<td valign='top'>Long Description</td>
			<td valign='top'>:</td>
			<td><?php $data = array(
              'name'        => 'longdesc',
              'id'          => 'longdesc',
              'toolbarset'  => 'Default',
              'basepath'    => '/fckeditor/',
              'width'       => '98%',
              'height'      => '400',
              'value'		 => $longdesc
              );
	echo form_fckeditor($data);?> <span class='error'>*<?php echo form_error('longdesc'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td><?php $options = array('active' => 'active', 'inactive' => 'inactive');
    echo form_dropdown('status',$options,$status);?> </td> 
		</tr>
		<tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><?php echo form_submit('submit','edit testimonies');?></td> 
		</tr>
		</table>
		<?php echo form_close();?>
		
<?php
$this->load->view('footer');
?>
