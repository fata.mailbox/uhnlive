<?php
$this->load->view('header');
?>
<h2><?php echo $page_title;?></h2>
<?php if($_POST){
		$alias = set_value('alias');
		$jenisfile = set_value('jenisfile');
		$shortdesc = set_value('shortdesc');
		$status = set_value('status');
		
	}else{
		$alias = $row['file_alias'];
		$jenisfile = $row['type'];
		$shortdesc = $row['description'];
		$status = $row['status'];
	}?>
	<?php echo form_open_multipart('master/downloadmaster/edit/'.$this->uri->segment(4));?>
		
		<table width='99%'>
		<tr>
			<td valign='top'>Nama File</td>
			<td valign='top'>:</td>
			<td><?php echo form_hidden('namafile',$row['file']);
				echo form_hidden('id',$row['id']);
				echo $row['file'];?></td> 
		</tr>
		<tr>
			<td valign='top'>File Alias</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'alias','id'=>'alias','value'=>$alias);
    echo form_input($data);?> <span class='error'>*<?php echo form_error('alias'); ?></span>	</td> 
		</tr>
		<tr>
			<td valign='top' width='14%'>Description</td>
			<td valign='top' width='1%'>:</td>
			<td width='85%'><?php $data = array('name'=>'shortdesc','id'=>'shortdesc','rows'=>5, 'cols'=>'40','class'=>'txtarea','value'=>$shortdesc);
    echo form_textarea($data);?> <span class='error'>*<?php echo form_error('shortdesc'); ?></span>	</td> 
		</tr>
		<tr>
			<td valign='top'>File</td>
			<td valign='top'>:</td>
			<td><input type="file" name="userfile" size="20" />
 <span class='error'>*<?php if(isset($error)) echo $error['error'];?></span>	</td> 
		</tr>
		
		<tr>
			<td valign='top'>Jenis File</td>
			<td valign='top'>:</td>
			<td><?php $options = array('Ms. Office Word' => 'Ms. Office Word', 'Ms. Office Excel' => 'Ms. Office Excel','Ms. Office PowerPoint' => 'Ms. Office PowerPoint','PDF File' => 'PDF File','MP3 File' => 'MP3 File','Video File' => 'Video File');
    echo form_dropdown('jenisfile',$options,$jenisfile);?> </td> 
		</tr>
		<tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td><?php $options = array('active' => 'active', 'inactive' => 'inactive');
    echo form_dropdown('status',$options,$status);?> </td> 
		</tr>
		<tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><?php echo form_submit('submit','edit download');?></td> 
		</tr>
		</table>
		<?php echo form_close();?>
		
<?php
$this->load->view('footer');
?>
