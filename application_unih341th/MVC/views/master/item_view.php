<?php $this->load->view('header');?>
<div id="view"><?php echo anchor('master/item/edit/'.$row->id,'edit item product');?></div>

<div class="ViewHold">
	<div id="companyDetails"><?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?></div>
	<p>
		<strong><?php echo "created : ";?> <?php echo $row->created;?><br />
		<?php echo "createdby : ";?> <?php echo $row->createdby;?>
		</strong>
	</p>
	<br />
<h2>Item Product</h2>
<hr />
<h3><?php echo $row->id." - ".$row->name;?></h3>

<table class="stripe">
	<tr>
      <th width='24%'>Description</th>
      <th width='1%'>:</th>
      <th width='75%'>Value</th>
	</tr>
	<tr>
		<td>Type</td>
		<td>:</td>
		<td><?php echo $row->type;?></td>
	</tr>
	<tr>
		<td>Price of Member</td>
		<td>:</td>
		<td><?php echo $row->fprice;?></td>
	</tr>
	<tr>
		<td>Price of East area</td>
		<td>:</td>
		<td><?php echo $row->fprice2;?></td>
	</tr>
	<tr>
		<td>Poin Value (PV)</td>
		<td>:</td>
		<td><?php echo $row->fpv;?></td>
	</tr>
    <tr>
		<td>Bonus Value (BV)</td>
		<td>:</td>
		<td><?php echo $row->fbv;?></td>
	</tr>
	<tr>
		<td>Price of Customer</td>
		<td>:</td>
		<td><?php echo $row->fpricecust;?></td>
	</tr>
	<tr>
		<td>Sales</td>
		<td>:</td>
		<td><?php echo $row->sales;?></td>
	</tr>
	<tr>
		<td>Display</td>
		<td>:</td>
		<td><?php echo $row->display;?></td>
	</tr>
	<tr>
		<td>Manufaktur</td>
		<td>:</td>
		<td><?php echo $row->manufaktur;?></td>
	</tr>
	<tr>
		<td valign='top'>Image</td>
		<td valign='top'>:</td>
		<td><img src="<?php echo base_url();?>userfiles/product/<?php echo $row->image;?>"></td>
	</tr>
	<tr>
		<td valign='top'>Headline</td>
		<td valign='top'>:</td>
		<td><?php echo $row->headline." ";?></td>
    </tr>    
	<tr>
		<td valign='top'>Description</td>
		<td valign='top'>:</td>
		<td><?php echo $row->description." ";?></td>
	</tr>
	<tr>
		<td>Updated</td>
		<td>:</td>
		<td><?php echo $row->updated;?></td>
	</tr>
	<tr>
		<td>Updated By</td>
		<td>:</td>
		<td><?php echo $row->updatedby." "; ?></td>
	</tr>
</table>
	
<?php $this->load->view('footer');?>
