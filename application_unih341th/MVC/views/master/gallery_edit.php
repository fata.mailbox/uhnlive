<?php
$this->load->view('header');
?>
<h2><?php echo $page_title;?></h2>
	<?php echo form_open_multipart('master/gallery/edit/'.$row['id']);?>
		
		<table width="99%">
		<tr>
			<td valign='top'>Nama File</td>
			<td valign='top'>:</td>
			<td><?php echo form_hidden('namafile',$row['file']);
				echo form_hidden('id',$row['id']);
				echo $row['file'];?></td> 
		</tr>

        		<tr>
			<td valign='top'>Judul</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'title','id'=>'title','size'=>'50','value'=>set_value('title',$row['title']));
    echo form_input($data);?></td> 
		</tr>

		<tr>
			<td valign='top' width='14%'>Deskripsi</td>
			<td valign='top' width='1%'>:</td>
<td><?php $data = array('name'=>'shortdesc','id'=>'shortdesc','rows'=>5, 'cols'=>'40','class'=>'txtarea','value'=>set_value('shortdesc',$row['description']));
    echo form_textarea($data);?></td> 
    		</tr>
		<tr>
			<td valign='top'>Image Gallery</td>
			<td valign='top'>:</td>
			<td><input type="file" name="userfile" size="20" />
 <span class='error'>*<?php if(isset($error)) echo $error['error'];?></span>	</td> 
		</tr>
				<tr>
			<td valign='top' width='14%'>Judul Album</td>
			<td valign='top' width='1%'>:</td>
<td><?php $data = array('name'=>'album','id'=>'album','rows'=>1, 'cols'=>'40','class'=>'txtarea','value'=>set_value('album',$row['album']));
    echo form_textarea($data);?> <span class='error'><?php echo form_error('album'); ?></span></td> 
    		</tr>
		<tr>
			<td valign='top'>Cover Album</td>
			<td valign='top'>:</td>
			<td><?php $options = array('1' => 'Yes', '0' => 'No');
    echo form_dropdown('cover',$options,$row['coveralbum']);?> </td> 
		</tr>
		
		<tr>
			<td valign='top'>No. Urut</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'nourut','id'=>'nourut','value'=>set_value('nourut',$row['nourut']));
    echo form_input($data);?><span class='error'>*<?php echo form_error('nourut'); ?></span>	</td> 
		</tr>
		<tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td><?php $options = array('active' => 'active', 'inactive' => 'inactive');
    echo form_dropdown('status',$options,$row['status']);?> </td> 
		</tr>
		<tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><?php echo form_submit('submit','Submit');?></td> 
		</tr>
		</table>
		<?php echo form_close();?>
		
<?php
$this->load->view('footer');
?>