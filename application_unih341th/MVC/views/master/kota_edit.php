<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('master/kota/edit/'.$row->id, array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		<tr>
			<td width='24%'>date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td valign='top'></td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo $row->id;?></td>
		</tr>
		<tr>
			<td valign='top'>Propinsi Name</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="hidden" name="id" value="<?php echo set_value('id',$row->id);?>"/>
				<input type="text" name="name" value="<?php echo set_value('name',$row->name);?>" size"30" />
				<span class="error">* <?php echo form_error('name');?></span></td>
		</tr>
		<tr>
			<td>Propinsi</td>
			<td>:</td>
			<td><?php echo form_dropdown('propinsi_id',$propinsi,$row->propinsi_id);?></td>
		</tr>
		<tr>
			<td>Warehouse</td>
			<td>:</td>
			<td><?php echo form_dropdown('warehouse_id',$warehouse,$row->warehouse_id);?></td>
		</tr>
		<tr>
			<td>Minimum Order</td>
			<td>:</td>
			<td><input class='textbold' type="text" name="mqo" value="<?php echo set_value('mqo',$this->MMenu->numformat($row->mqo));?>" maxlength="15" size="12" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
		</tr>
		<tr>
			<td>Onkos Kirim</td>
			<td>:</td>
			<td><input class='textbold' type="text" name="ongkoskirim" value="<?php echo set_value('ongkoskirim',$this->MMenu->numformat($row->ongkoskirim));?>" maxlength="15" size="12" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>
		</table>

<?php echo form_close();?>
<?php $this->load->view('footer');?>
