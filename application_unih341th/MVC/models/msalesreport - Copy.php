<?php
/* created by Boby 2013-06-07 */
class MSalesreport extends CI_Model{
    function __construct()
    { parent::__construct(); }
	
	public function get_tree($show_level='',$auto=0,$parentid=0,$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		/* Created by Boby 20130823 */
		$where = "";
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		// echo $this->db->last_query();
		$got = 0;	// Updated by Boby 20131024
		$sel = "";	// Updated by Boby 20131024
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				/* Updated by Boby 20131024 */
				$l = $row['leftval']+1;	// $l = $row['leftval'];
				$l1 = $row['leftval'];
				$r = $row['rightval']-1; // $r = $row['rightval'];
				$r1 = $row['rightval'];
				// $where .= "AND n.leftval < $l AND n.rightval > $r ";
				$where .= "AND n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ";
				$sel .= " WHEN n.leftval = $l1 AND n.rightval = $r1 THEN 1 ";
				/* End updated by Boby 20131024 */
			}
		}
		/* Updated by Boby 20131024 */
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
		}
		/* End updated by Boby 20131024 */
		// echo $sel;
		// echo $where;
		$q->free_result();
		/* End created by Boby 20130823 */
		
		$a=', ifnull(pgs.ps,0) as ps 
			, m.aps+m.apgs as apgs_
			';
		$q = "
			SELECT 
				n.id - $auto as auto_id,n.parentid - $auto as parentid
				, m.posisi - $level as level, format(pgs.ps+pgs.pgs,0)as fpgs
				, format(m.aps+m.apgs,0)as apgs
				, j.name as jenjang, pgs.jenjang_id
				, format(ifnull(pgs.ps,blj.blj), 0)as fps
				, m.sponsor_id AS uplineId, m.enroller_id AS sponsorId, n.id, m.posisi, n.member_id, m.nama as name
				, m.hp, IFNULL(pgs.ps,blj.blj) AS pembelanjaan
				, m.created AS joindate
				, IFNULL(j.name,'-') AS title, pgs.pgs AS TGPV_
				, CASE WHEN pgs.pgs IS NULL THEN m.pgs ELSE pgs.pgs END AS TGPV
				, IFNULL(bns.bpp,0)AS bpp, IFNULL(bns.aktifasi,0)AS aktifasi
				, IFNULL(stat.act,'n')AS act
				, IFNULL(rek.rekrut,0)AS rekrut
				, IFNULL(blj.bln,0)AS bln"
			.$sel /* Updated by Boby 20131024 */
			.$a.
			"FROM networks n
			LEFT JOIN member m ON n.member_id = m.id
			LEFT JOIN(
				SELECT member_id, SUM(bpp)AS bpp, SUM(aktifasi) AS aktifasi
				FROM(
					SELECT member_id, title, nominal
						, CASE WHEN title = 25 THEN nominal ELSE 0 END AS bpp
						, CASE WHEN title BETWEEN 26 AND 30 THEN nominal ELSE 0 END AS aktifasi
					FROM bonus
					WHERE periode = (LAST_DAY('$tgl'))
					ORDER BY title
				)AS dt
				GROUP BY member_id
			)AS bns ON m.id = bns.member_id
			LEFT JOIN(
				SELECT member_id, CASE WHEN SUM(totalpv) > 0 THEN 'active' END AS act
				FROM so
				WHERE tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 4 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
				GROUP BY member_id
			)AS stat ON m.id = stat.member_id
			LEFT JOIN(
				-- SELECT m.enroller_id, COUNT(m.id)AS rekrut
				-- FROM so
				-- LEFT JOIN member m ON so.member_id = m.id
				-- WHERE tgl BETWEEN '$tgl' AND (LAST_DAY('$tgl'))
				-- GROUP BY m.enroller_id
				SELECT enroller_id, COUNT(member_id)AS rekrut
				FROM(
					SELECT m.enroller_id, so.member_id, so_.oms, MAX(so.kit)AS kit1, SUM(so.totalpv)AS pv
					FROM so
					LEFT JOIN member m ON so.member_id = m.id
					LEFT JOIN(
						SELECT member_id, SUM(totalharga_)AS oms
						FROM so
						WHERE tgl BETWEEN '$tgl' AND (LAST_DAY('$tgl'))
						AND kit = 'N'
						GROUP BY member_id
					)AS so_ ON m.id = so_.member_id
					WHERE tgl BETWEEN '$tgl' AND (LAST_DAY('$tgl'))
					GROUP BY member_id
					HAVING kit1 = 'y' AND so_.oms >= 599000
				)AS dt
				WHERE pv > 0
				GROUP BY enroller_id
			)AS rek ON m.id = rek.enroller_id
			LEFT JOIN(
				SELECT member_id, MAX(tgl)AS tgl, ROUND((DATEDIFF((LAST_DAY('$tgl')), MAX(tgl))/30),0)AS bln
					, SUM(totalpv)AS blj
				FROM so
				WHERE tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
				GROUP BY member_id
			)AS blj ON m.id = blj.member_id
			LEFT JOIN(
				SELECT pgs.tgl, pgs.member_id, pgs.jenjang_id, pgs.apgs, pgs.ps, pgs.pgs
				FROM pgs_bulanan pgs
				WHERE tgl = (LAST_DAY('$tgl'))
				ORDER BY member_id
			)AS pgs ON m.id = pgs.member_id
			LEFT JOIN jenjang j ON pgs.jenjang_id = j.id
			WHERE n.leftval >= $leftval AND n.rightval <= $rightval 
			".$where."
			AND substr(m.tglaplikasi,1,7) <= '$show_level' and n.active = 1
			ORDER BY n.leftval, n.rightval DESC
			";
		//echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
    }
	
    public function get_activity($member_id, $bln, $thn){
        $data = array();
		if(strlen($bln)<2){$bln = '0'.$bln;}
		$tgl = $thn."-".$bln."-01";
		echo $tgl;
        $q = "
			SELECT  
				m.sponsor_id AS uplineId, m.enroller_id AS sponsorId, n.id, m.posisi, n.member_id, m.nama
				, m.hp, IFNULL(pgs.ps,0) AS pembelanjaan
				, m.created AS joindate
				, IFNULL(j.name,'-') AS title, pgs.apgs AS TGPV_
				, CASE WHEN pgs.pgs IS NULL THEN m.pgs ELSE pgs.pgs END AS TGPV
				, IFNULL(bns.bpp,0)AS bpp, IFNULL(bns.aktifasi,0)AS aktifasi
				, IFNULL(stat.act,'n')AS act
				, IFNULL(rek.rekrut,0)AS rekrut
				, IFNULL(blj.bln,0)AS bln
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id
			LEFT JOIN(
				SELECT member_id, SUM(bpp)AS bpp, SUM(aktifasi) AS aktifasi
				FROM(
					SELECT member_id, title, nominal
						, CASE WHEN title = 25 THEN nominal ELSE 0 END AS bpp
						, CASE WHEN title BETWEEN 26 AND 30 THEN nominal ELSE 0 END AS aktifasi
					FROM bonus
					WHERE periode = (LAST_DAY('$tgl'))
					ORDER BY title
				)AS dt
				GROUP BY member_id
			)AS bns ON m.id = bns.member_id
			LEFT JOIN(
				SELECT member_id, CASE WHEN SUM(totalpv) > 0 THEN 'y' END AS act
				FROM so
				WHERE tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 4 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
				GROUP BY member_id
			)AS stat ON m.id = stat.member_id
			LEFT JOIN(
				-- SELECT m.enroller_id, COUNT(m.id)AS rekrut
				-- FROM so
				-- LEFT JOIN member m ON so.member_id = m.id
				-- WHERE tgl BETWEEN '$tgl' AND (LAST_DAY('$tgl'))
				-- GROUP BY m.enroller_id
				SELECT enroller_id, COUNT(member_id)AS rekrut
				FROM(
					SELECT m.enroller_id, so.member_id, MAX(so.kit)AS kit1, SUM(so.totalpv)AS pv
					FROM so
					LEFT JOIN member m ON so.member_id = m.id
					WHERE tgl BETWEEN '$tgl' AND (LAST_DAY('$tgl'))
					GROUP BY member_id
					HAVING kit1 = 'y'
				)AS dt
				WHERE pv > 0
				GROUP BY enroller_id
			)AS rek ON m.id = rek.enroller_id
			LEFT JOIN(
				SELECT member_id, MAX(tgl)AS tgl, ROUND((DATEDIFF((LAST_DAY('$tgl')), MAX(tgl))/30),0)AS bln
				FROM so
				WHERE tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 12 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
				GROUP BY member_id
			)AS blj ON m.id = blj.member_id
			LEFT JOIN(
				SELECT pgs.tgl, pgs.member_id, pgs.jenjang_id, pgs.apgs, pgs.ps
				FROM pgs_bulanan pgs
				WHERE tgl = (LAST_DAY('$tgl'))
				ORDER BY member_id
			)AS pgs ON m.id = pgs.member_id
			LEFT JOIN jenjang j ON pgs.jenjang_id = j.id
			WHERE n.leftval >= (SELECT leftval FROM networks WHERE member_id = '$member_id')
			AND n.rightval <= (SELECT rightval FROM networks WHERE member_id = '$member_id')
			ORDER BY n.leftval, n.rightval DESC
		";
        $q = $this->db->query($q);
        echo $this->db->last_query();
		//echo $tgl;
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function get_dropdown(){
        $data = array();
        $this->db->select("date_format(periode,'%Y-%m-%d') as tgl,date_format(periode,'%M - %Y')as ftgl",false);
        $this->db->from('bonus');
        $this->db->group_by('periode');
        $this->db->order_by('periode','desc');
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['tgl']]=$row['ftgl'];    
            }
        }
        $q->free_result();
        return $data;
    }
    
	/* Created by Boby 20130621 */
	public function getBonus($member_id,$periode){
		$periode = $periode.'-01';
		$periode = "LAST_DAY('".$periode."')";
        $data = array();
		/*
        $this->db->order_by('title','asc');
        $q = $this->db->get_where('v_bonus',array('member_id'=>$member_id,'periode'=>$periode));
		*/
		$q = "
			SELECT *
			FROM v_bonus
			WHERE member_id = '$member_id'
			AND periode = $periode
		";
		$q = $this->db->query($q);
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	/* End created by Boby 20130621 */
	
	/* Created by Boby 20130710 */
	public function dropdown_periode(){
        $data = array();
		//$bulan = date('');
        $q = $this->db
				->select("date_format(tgl,'%Y-%m')as tgl,date_format(tgl,'%M - %Y')as ftgl",false)
				->from('pgs_bulanan')
				->group_by('tgl')
				->order_by('tgl','desc')
				->get();
		$flag = 0;
		$data[''] = 'Bulan Sekarang'; // Modified by Boby 20121205
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
				if($flag==0){
					$flag+=1;
					$tgl = date('Y-m');
				}
                $data[$row['tgl']]=$row['ftgl'];    
            }
        }
		/* Created by Boby 20121205 */
		// unset($data);
		// $data[''] = 'Bulan Sekarang';
		/* End created by Boby 20121205 */
        $q->free_result();
        return $data;
    }
	/* End created by Boby 20130710 */
	
	/* Created by Boby 20130813 */
	public function getOneYearAct(){
        $data = array();
        $q = "
			SELECT up.id AS sponsor_id, up.nama AS spName, dt.member_id, m.nama, m.hp, m.alamat, k.name AS kota
				, IFNULL(SUM(oms1),0)AS oms1, IFNULL(SUM(oms2),0)AS oms2, IFNULL(SUM(oms3),0)AS oms3
				, IFNULL(SUM(oms4),0)AS oms4, IFNULL(SUM(oms5),0)AS oms5, IFNULL(SUM(oms6),0)AS oms6
				, IFNULL(SUM(oms7),0)AS oms7, IFNULL(SUM(oms8),0)AS oms8, IFNULL(SUM(oms9),0)AS oms9
				, IFNULL(SUM(oms10),0)AS oms10, IFNULL(SUM(oms11),0)AS oms11, IFNULL(SUM(oms12),0)AS oms12
				, IFNULL(SUM(oms1),0)+IFNULL(SUM(oms2),0)+IFNULL(SUM(oms3),0)
				+ IFNULL(SUM(oms4),0)+IFNULL(SUM(oms5),0)+IFNULL(SUM(oms6),0)
				+ IFNULL(SUM(oms7),0)+IFNULL(SUM(oms8),0)+IFNULL(SUM(oms9),0)
				+ IFNULL(SUM(oms10),0)+IFNULL(SUM(oms11),0)+IFNULL(SUM(oms12),0)AS omsAll
			FROM(
				SELECT so.member_id, MONTH(so.tgl)AS bln, SUM(so.totalharga)AS omset
					, CASE WHEN MONTH(so.tgl) = 1 THEN SUM(so.totalharga) ELSE 0 END AS oms1
					, CASE WHEN MONTH(so.tgl) = 2 THEN SUM(so.totalharga) ELSE 0 END AS oms2
					, CASE WHEN MONTH(so.tgl) = 3 THEN SUM(so.totalharga) ELSE 0 END AS oms3
					, CASE WHEN MONTH(so.tgl) = 4 THEN SUM(so.totalharga) ELSE 0 END AS oms4
					, CASE WHEN MONTH(so.tgl) = 5 THEN SUM(so.totalharga) ELSE 0 END AS oms5
					, CASE WHEN MONTH(so.tgl) = 6 THEN SUM(so.totalharga) ELSE 0 END AS oms6
					, CASE WHEN MONTH(so.tgl) = 7 THEN SUM(so.totalharga) ELSE 0 END AS oms7
					, CASE WHEN MONTH(so.tgl) = 8 THEN SUM(so.totalharga) ELSE 0 END AS oms8
					, CASE WHEN MONTH(so.tgl) = 9 THEN SUM(so.totalharga) ELSE 0 END AS oms9
					, CASE WHEN MONTH(so.tgl) = 10 THEN SUM(so.totalharga) ELSE 0 END AS oms10
					, CASE WHEN MONTH(so.tgl) = 11 THEN SUM(so.totalharga) ELSE 0 END AS oms11
					, CASE WHEN MONTH(so.tgl) = 12 THEN SUM(so.totalharga) ELSE 0 END AS oms12
				FROM so
				WHERE tgl BETWEEN (LAST_DAY((NOW() - INTERVAL 1 YEAR)- INTERVAL 1 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
				GROUP BY member_id, bln
				ORDER BY member_id
			)AS dt
			LEFT JOIN member m ON dt.member_id = m.id
			LEFT JOIN kota k ON m.kota_id = k.id
			LEFT JOIN member up ON m.sponsor_id = up.id
			GROUP BY dt.member_id
		";
        $q = $this->db->query($q);
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	public function periodeOmset(){
		$data = array();
		$qry = $this->db->query("
			SELECT 
			  CONCAT(LEFT(MONTHNAME(NOW()),3), ' ',YEAR(NOW())) AS bln12
			, CONCAT(LEFT(MONTHNAME(NOW() - INTERVAL 1 MONTH),3), ' ',YEAR(NOW() - INTERVAL 1 MONTH)) AS bln11
			, CONCAT(LEFT(MONTHNAME(NOW() - INTERVAL 2 MONTH),3), ' ',YEAR(NOW() - INTERVAL 2 MONTH)) AS bln10
			, CONCAT(LEFT(MONTHNAME(NOW() - INTERVAL 3 MONTH),3), ' ',YEAR(NOW() - INTERVAL 3 MONTH)) AS bln9
			, CONCAT(LEFT(MONTHNAME(NOW() - INTERVAL 4 MONTH),3), ' ',YEAR(NOW() - INTERVAL 4 MONTH)) AS bln8
			, CONCAT(LEFT(MONTHNAME(NOW() - INTERVAL 5 MONTH),3), ' ',YEAR(NOW() - INTERVAL 5 MONTH)) AS bln7
			, CONCAT(LEFT(MONTHNAME(NOW() - INTERVAL 6 MONTH),3), ' ',YEAR(NOW() - INTERVAL 6 MONTH)) AS bln6
			, CONCAT(LEFT(MONTHNAME(NOW() - INTERVAL 7 MONTH),3), ' ',YEAR(NOW() - INTERVAL 7 MONTH)) AS bln5
			, CONCAT(LEFT(MONTHNAME(NOW() - INTERVAL 8 MONTH),3), ' ',YEAR(NOW() - INTERVAL 8 MONTH)) AS bln4
			, CONCAT(LEFT(MONTHNAME(NOW() - INTERVAL 9 MONTH),3), ' ',YEAR(NOW() - INTERVAL 9 MONTH)) AS bln3
			, CONCAT(LEFT(MONTHNAME(NOW() - INTERVAL 10 MONTH),3), ' ',YEAR(NOW() - INTERVAL 10 MONTH)) AS bln2
			, CONCAT(LEFT(MONTHNAME(NOW() - INTERVAL 11 MONTH),3), ' ',YEAR(NOW() - INTERVAL 11 MONTH)) AS bln1
			
			, MONTH(NOW()) AS b12
			, MONTH(NOW() - INTERVAL 1 MONTH) AS b11
			, MONTH(NOW() - INTERVAL 2 MONTH) AS b10
			, MONTH(NOW() - INTERVAL 3 MONTH) AS b9
			, MONTH(NOW() - INTERVAL 4 MONTH) AS b8
			, MONTH(NOW() - INTERVAL 5 MONTH) AS b7
			, MONTH(NOW() - INTERVAL 6 MONTH) AS b6
			, MONTH(NOW() - INTERVAL 7 MONTH) AS b5
			, MONTH(NOW() - INTERVAL 8 MONTH) AS b4
			, MONTH(NOW() - INTERVAL 9 MONTH) AS b3
			, MONTH(NOW() - INTERVAL 10 MONTH) AS b2
			, MONTH(NOW() - INTERVAL 11 MONTH) AS b1
		");
        if($qry->num_rows()>0){
            $data = $qry->row_array();
        }
        $qry->free_result();
        return $data;
    }
	/* End created by Boby 20130813 */
}
?>