<?php
class MSales extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
	// Created by Boby 20130215
	public function get_year_report(){
        $data = array();
		$thn=date("Y");
		for($i=$thn;$i>='2009';$i--){
			$data[$i]=$i;
		}
        return $data;
    }
	public function sales_incentive($thn, $q){
        $data = array();
		$thn_ = $thn-1;
		if($q!=0){
			$tgl = $q;
			$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ";
			$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ";
			
			$qry1="_, bln AS periode ";
			$qry2="_, MONTH(periode) AS q ";
			$qry3="HAVING periode BETWEEN MONTH($awal) AND MONTH($akhir) ";
			//echo $qry3;
		}else{
			$qry1=" ";$qry2=" ";$qry3=" ";
		}
		
		$query = "
			SELECT dt.periode
				, IFNULL(r.nama,'Staff Order') AS region
				, IFNULL(SUM(omset1),0)AS omset1, IFNULL(SUM(omset2),0)AS omset2
				, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND((IFNULL(SUM(omset2),0)*100 / IFNULL(rt.target,0)),2),0)AS salesVStarget
				, ROUND((IFNULL(SUM(omset2),0)*100 / IFNULL(SUM(omset1),0)),2)AS newVSold
				, IFNULL(SUM(dt.nr),0)AS nr_
				, IFNULL(SUM(dt.sf),0)AS sf
				, IFNULL(rt.nr,0)AS nr
				, IFNULL(SUM(kit1),0)AS kit1, IFNULL(SUM(kit2),0)AS kit2
				, ROUND((IFNULL(SUM(kit2),0)*100 / IFNULL(rt.nr,0)),2)AS kit1vs2
			FROM(
				SELECT *
					, CASE WHEN quart1 = quart1M THEN akota
						WHEN stockiest_id <> 0 THEN skota
						ELSE mkota
					END AS kota1
					, CASE 
						WHEN bln BETWEEN 1 AND 3 THEN 1
						WHEN bln BETWEEN 4 AND 6 THEN 2
						WHEN bln BETWEEN 7 AND 9 THEN 3
						WHEN bln BETWEEN 10 AND 12 THEN 4
					END AS periode".$qry1."
				FROM(

					SELECT YEAR(so.tgl) AS thn, MONTH(so.tgl)AS bln
						, CASE 
							WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 1 AND 3 THEN 'qo1'	WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 4 AND 6 THEN 'qo2'
							WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 7 AND 9 THEN 'qo3'	WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 10 AND 12 THEN 'qo4'
						END AS quart1
						, CASE 
							WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 1 AND 3 THEN 'qo1'	WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 4 AND 6 THEN 'qo2'
							WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 7 AND 9 THEN 'qo3'	WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 10 AND 12 THEN 'qo4'
							ELSE 'qo5'
						END AS quart1M
						, CASE 
							WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 1 AND 3 THEN 'qn1'	WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 4 AND 6 THEN 'qn2'
							WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 7 AND 9 THEN 'qn3'	WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 10 AND 12 THEN 'qn4'
						END AS quart2
						, CASE 
							WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 1 AND 3 THEN 'qn1'	WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 4 AND 6 THEN 'qn2'
							WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 7 AND 9 THEN 'qn3'	WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 10 AND 12 THEN 'qn4'
							ELSE 'qn5'
						END AS quart2M
						, so.member_id, m.kota_id AS mkota
						, so.stockiest_id, s.kota_id AS skota
						, ma.kota_id AS akota
						, nr.nr -- , nr.sf
						, CASE WHEN YEAR(so.tgl) = $thn_ THEN totalharga END AS omset1
						, CASE WHEN YEAR(so.tgl) = $thn_ AND so.kit = 'y' THEN 1 ELSE 0 END AS kit1
						, CASE WHEN YEAR(so.tgl) = $thn THEN totalharga END AS omset2
						, CASE WHEN YEAR(so.tgl) = $thn AND so.kit = 'y' THEN 1 ELSE 0 END AS kit2
						, CASE WHEN YEAR(so.tgl) = $thn AND so.totalpv > 0 THEN 1 ELSE 0 END AS sf
					FROM so
					LEFT JOIN(
						SELECT member_id, MAX(nr)AS nr -- , 1 AS sf
						FROM (
							SELECT so.id, so.member_id, m.nama, m.created, so.tgl, so.kit, so.totalpv
								, CASE WHEN 
									MONTH(m.created) = MONTH(so.tgl) 
									AND YEAR(m.created) = YEAR(so.tgl) 
									AND so.totalpv > 0
									THEN 1 ELSE 0 
								END AS nr
							FROM so
							LEFT JOIN member m ON so.member_id=m.id
							WHERE YEAR(tgl) = $thn
						)AS dt
						GROUP BY member_id
					)AS nr ON so.member_id = nr.member_id
					LEFT JOIN member m ON so.member_id = m.id
					LEFT JOIN stockiest s ON so.stockiest_id = s.id
					LEFT JOIN member_allocation ma ON so.member_id = ma.member_id AND YEAR(so.tgl) = YEAR(ma.periode)
					WHERE YEAR(so.tgl) BETWEEN $thn_ AND $thn
					ORDER BY quart1 DESC, ma.kota_id DESC
				)AS dt
			)AS dt
			LEFT JOIN kota k ON dt.kota1 = k.id
			LEFT JOIN region r ON k.region = r.id
			LEFT JOIN(
				SELECT CASE WHEN MONTH(periode) BETWEEN 1 AND 3 THEN 1
					WHEN MONTH(periode) BETWEEN 4 AND 6 THEN 2
					WHEN MONTH(periode) BETWEEN 7 AND 9 THEN 3
					WHEN MONTH(periode) BETWEEN 10 AND 12 THEN 4
					END AS q".$qry2."
					, region_id, SUM(target)as target, SUM(nr)AS nr
				FROM region_target
				WHERE YEAR(periode) = $thn
				GROUP BY region_id, q
			)AS rt ON r.id = rt.region_id AND dt.periode = rt.q
			GROUP BY region, periode
			".$qry3."
			ORDER BY periode, r.id
		";
		$qry = $this->db->query($query);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	// End created by Boby 20130215
    public function viewTarget($thn)
    {
        $data=array();
		$q = $this->db->query("
			SELECT rt.id, rt.periode, rt.region_id, r.nama, rt.target, rt.nr
				, MONTHNAME(rt.periode)AS namaBln, YEAR(rt.periode)AS thn
			FROM region_target rt
			LEFT JOIN region r ON rt.region_id=r.id
			WHERE YEAR(periode) = $thn
		");
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	public function get_y(){
        $data = array();
		$thn=date("Y")+1;
		for($i=$thn;$i>='2009';$i--){
			$data[$i]=$i;
		}
        return $data;
    }
	public function get_q($q){
        $data = array();
		if($q==1){$data['00-00']='All';}
		$data['01-31']='Quarter1';
		$data['04-30']='Quarter2';
		$data['07-31']='Quarter3';
		$data['10-31']='Quarter4';
        return $data;
    }
	public function get_region()
    {
        $data=array();
		$qry = "
			SELECT id, nama
			FROM region
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[$row['id']] = $row['nama'];
            }
        }
        $q->free_result();
        return $data;
    }
	public function cek_data_target($periode, $region)
    {
        $data=array();
		$qry = "
			SELECT id
			FROM region_target
			WHERE periode = LAST_DAY('$periode')
			AND region_id = '$region'
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
        if($q->num_rows < 1){
            $temp = "ok";
        }
        $q->free_result();
        return $temp;
    }
	
	public function insert_data_target($periode, $region, $target, $nr)
    {
		$empid = $this->session->userdata('userid');
		$qry = "
			INSERT INTO region_target(periode, region_id, target, nr, createdby)
			VALUES(LAST_DAY('$periode'), '$region', '$target', '$nr', '$empid');
		";
		$q = $this->db->query($qry);
	}
	
	public function viewAlloc($thn)
    {
        $data=array();
		$q = $this->db->query("
			SELECT ma.periode
				, CASE WHEN MONTH(ma.periode) BETWEEN 1 AND 3 THEN 'quart1'
					WHEN MONTH(ma.periode) BETWEEN 4 AND 6 THEN 'quart2'
					WHEN MONTH(ma.periode) BETWEEN 7 AND 9 THEN 'quart3'
					WHEN MONTH(ma.periode) BETWEEN 10 AND 12 THEN 'quart4'
				END AS q
				, ma.member_id, m.nama, m.kota_id, k.name AS kota1, ma.kota_id, k1.name AS kota2
			FROM member_allocation ma
			LEFT JOIN member m ON ma.member_id = m.id
			LEFT JOIN kota k ON m.kota_id = k.id
			LEFT JOIN kota k1 ON ma.kota_id = k1.id
			WHERE YEAR(periode) = $thn
		");
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	public function cek_data_allocation($periode, $member_id)
    {
        $data=array();
		$qry = "
			SELECT id
			FROM member_allocation
			WHERE periode = '$periode'
			AND member_id = '$member_id'
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
        if($q->num_rows < 1){
            $temp = "ok";
        }
        $q->free_result();
        return $temp;
    }
	public function insert_member_allocation($periode, $member_id, $kota)
    {
		$empid = $this->session->userdata('userid');
		$data=array(
			'periode' => $periode,
			'member_id' => $member_id,
			'kota_id' => $kota,
			'createdby' => $empid
		);
		
		$this->db->insert('member_allocation',$data);
	}
	public function viewStcTarget($thn, $quart)
    {
        $data=array();
		//$thn = '2012';
		//$quart = '01';
		$tgl = $thn."-".$quart;
		$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ";
		$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ";
		//echo $tgl;
		$q = $this->db->query("
			SELECT s.id, m.nama, s.no_stc, s.type AS tipe, k.region
				, IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0) AS oms1
				, IFNULL(trg1,0)AS trg1
				, IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0) AS oms2
				, IFNULL(trg2,0)AS trg2
				, IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0) AS oms3
				, IFNULL(trg3,0)AS trg3
				, (IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0))+
				  (IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0))+
				  (IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0)) AS oms
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_id = k.id
			LEFT JOIN member m ON s.id = m.id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS ro1, SUM(oms2)AS ro2, SUM(oms3)AS ro3
				FROM(
					SELECT member_id
						, CASE WHEN MONTH(ro.`date`)=MONTH($awal) THEN totalharga END AS oms1
						, CASE WHEN MONTH(ro.`date`)=MONTH($awal)+1 THEN totalharga END AS oms2
						, CASE WHEN MONTH(ro.`date`)=MONTH($awal)+2 THEN totalharga END AS oms3
					FROM ro
					WHERE ro.stockiest_id=0
					AND ro.date BETWEEN $awal AND $akhir
				)AS ro_
				GROUP BY member_id
			)AS ro ON s.id = ro.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS pjm1, SUM(oms2)AS pjm2, SUM(oms3)AS pjm3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN MONTH(pjm.tgl)=MONTH($awal) THEN totalharga END AS oms1
						, CASE WHEN MONTH(pjm.tgl)=MONTH($awal)+1 THEN totalharga END AS oms2
						, CASE WHEN MONTH(pjm.tgl)=MONTH($awal) THEN totalharga END AS oms3
					FROM pinjaman_titipan pjm
					WHERE pjm.tgl BETWEEN $awal AND $akhir
				)AS pjm
				GROUP BY member_id
			)AS pjm ON s.id = pjm.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS rtr1, SUM(oms2)AS rtr2, SUM(oms3)AS rtr3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN MONTH(rtr.tgl)=MONTH($awal) THEN totalharga END AS oms1
						, CASE WHEN MONTH(rtr.tgl)=MONTH($awal)+1 THEN totalharga END AS oms2
						, CASE WHEN MONTH(rtr.tgl)=MONTH($awal)+2 THEN totalharga END AS oms3
					FROM retur_titipan rtr
					WHERE rtr.tgl BETWEEN $awal AND $akhir
				)AS rtr
				GROUP BY member_id
			)AS rtr ON s.id = rtr.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS trg1, SUM(oms2)AS trg2, SUM(oms3)AS trg3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN MONTH(trg.periode)=MONTH($awal) THEN target END AS oms1
						, CASE WHEN MONTH(trg.periode)=MONTH($awal)+1 THEN target END AS oms2
						, CASE WHEN MONTH(trg.periode)=MONTH($awal)+2 THEN target END AS oms3
					FROM stockiest_target trg
					WHERE trg.periode BETWEEN $awal AND $akhir
				)AS rtr
				GROUP BY member_id
			)AS trg ON m.id = trg.member_id
			HAVING oms <> 0
			ORDER BY k.region, s.type, s.no_stc
			
		");
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	public function insert_stc_target($periode, $stc, $target)
    {
		$empid = $this->session->userdata('userid');
		//$periode = "LAST_DAY('$periode') ";
		$data=array(
			'periode' => $periode,
			'stockiest_id' => $stc,
			'target' => $target,
			'createdby' => $empid
		);
		
		$this->db->insert('stockiest_target',$data);
	}
	public function cek_stc_target($periode, $stc)
    {
        $data=array();
		//$periode = "LAST_DAY('$periode') ";
		$qry = "
			SELECT id
			FROM stockiest_target
			WHERE periode = '$periode'
			AND stockiest_id = '$stc'
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
        if($q->num_rows < 1){
            $temp = "ok";
        }
        $q->free_result();
        return $temp;
    }
	public function get_bln(){
        $data = array();
		$data['01-31']='January';
		$data['02-28']='Febuary';
		$data['03-31']='March';
		$data['04-30']='April';
		$data['05-31']='May';
		$data['06-30']='June';
		$data['07-31']='Juli';
		$data['08-31']='August';
		$data['09-30']='September';
		$data['10-31']='October';
		$data['11-30']='November';
		$data['12-31']='December';
        return $data;
    }
	public function get_end_date($periode){
		$data=array();
		//$periode = "LAST_DAY('$periode') ";
		$qry = "
			SELECT LAST_DAY('$periode') AS tgl
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
		
		if($q->num_rows > 0){
			$row = $q->row_array();
			return $row['tgl'];
		}else{
			return $temp;
		}
		
        $q->free_result();
        
    }
	public function update_data_target($periode, $region, $target, $nr)
    {
		$empid = $this->session->userdata('userid');
		$qry = "
			UPDATE region_target SET target='$target', nr='$nr', updated=NOW(), updatedby='$empid'
			WHERE periode=LAST_DAY('$periode') AND region_id='$region';
		";
		$q = $this->db->query($qry);
	}
	
	/* Created by Boby 20130305 */
	public function viewAllocMember($thn, $q){
		$thn_ = $thn-1;
		if($q!=0){
			$qry = "WHERE YEAR(ma.periode) = '$thn' AND ma.periode = '$q' ";
		}else{
			$qry = "WHERE YEAR(ma.periode) = '$thn' ";
		}
        $data=array();
		$qry = "
			SELECT YEAR(ma.periode)AS thn, ma.member_id, m.nama, ma.kota_id, k.name AS kota, k.region
				, CASE 
					WHEN MONTH(ma.periode) BETWEEN 1 AND 3 THEN 'Q1'
					WHEN MONTH(ma.periode) BETWEEN 4 AND 6 THEN 'Q2'
					WHEN MONTH(ma.periode) BETWEEN 7 AND 9 THEN 'Q3'
					WHEN MONTH(ma.periode) BETWEEN 10 AND 12 THEN 'Q4'
				END AS namaBln
			FROM member_allocation ma
			LEFT JOIN member m ON ma.member_id = m.id
			LEFT JOIN kota k ON ma.kota_id = k.id
		".$qry;
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function cek_alloc_member($periode, $member_id){
        $data=array();
		$qry = "
			SELECT id
			FROM member_allocation
			WHERE periode = LAST_DAY('$periode')
			AND member_id = '$member_id'
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
        if($q->num_rows < 1){
            $temp = "ok";
        }
        $q->free_result();
        return $temp;
    }
	
	public function insert_data_alloc_member($periode, $member, $kota){
		$empid = $this->session->userdata('userid');
		$qry = "
			INSERT INTO member_allocation(periode, kota_id, member_id, createdby)
			VALUES(LAST_DAY('$periode'), '$kota', '$member', '$empid');
		";
		$q = $this->db->query($qry);
	}
	/* End created by Boby 20130305 */
	
	/* Created by Boby 20130512 */
	public function getNewMember($periode){
		$thn_ = $thn-1;
        $data=array();
		$qry = "
			SELECT nm
				, SUM(mtd)AS mtd
				, SUM(lmtd)AS lmtd
				, SUM(lytd)AS lytd
			FROM(
				SELECT
					1 AS nm, vm.member_id, joindate, YEAR(joindate)AS thn, MONTH(joindate)AS bln
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') THEN 1 ELSE 0 END AS mtd
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) THEN 1 ELSE 0 END AS lmtd
					, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') THEN 1 ELSE 0 END AS lytd
				FROM v_memberjoin vm
				LEFT JOIN(
					SELECT so.member_id, YEAR(so.tgl)AS thn, MONTH(so.tgl)AS bln, SUM(totalpv)AS pv
					FROM so
					WHERE totalpv = 0
					AND so.member_id <> 'STAFF'
					AND(tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
					OR tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					GROUP BY member_id, thn, bln
				)AS dt ON vm.member_id = dt.member_id AND YEAR(vm.joindate) = dt.thn AND MONTH(vm.joindate) = dt.bln
				WHERE dt.pv = 0
				AND(joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
				OR joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
			)AS dt
			GROUP BY nm
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
		if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getNewMember_($periode){
		$thn_ = $thn-1;
        $data=array();
		$qry = "
			SELECT nm
				, SUM(rmtd)AS rmtd
				, SUM(rlmtd)AS rlmtd
				, SUM(rlytd)AS rlytd
				
				, SUM(ormtd)AS ormtd
				, SUM(orlmtd)AS orlmtd
				, SUM(orlytd)AS orlytd
				
				, SUM(nmtd)AS nmtd
				, SUM(nlmtd)AS nlmtd
				, SUM(nlytd)AS nlytd
				
				, SUM(onmtd)AS onmtd
				, SUM(onlmtd)AS onlmtd
				, SUM(onlytd)AS onlytd
			FROM(
				SELECT 1 AS nm, pv_, dt.member_id, joindate, YEAR(joindate)AS thn, MONTH(joindate)AS bln
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') AND pv_ > 0 THEN 1 ELSE 0 END AS rmtd
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND pv_ > 0 THEN 1 ELSE 0 END AS rlmtd
					, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') AND pv_ > 0 THEN 1 ELSE 0 END AS rlytd
					
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') AND pv_ > 0 THEN oms ELSE 0 END AS ormtd
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND pv_ > 0 THEN oms ELSE 0 END AS orlmtd
					, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') AND pv_ > 0 THEN oms ELSE 0 END AS orlytd
					
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') AND pv_ = 0 THEN 1 ELSE 0 END AS nmtd
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND pv_ = 0 THEN 1 ELSE 0 END AS nlmtd
					, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') AND pv_ = 0 THEN 1 ELSE 0 END AS nlytd
					
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') AND pv_ = 0 THEN oms ELSE 0 END AS onmtd
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND pv_ = 0 THEN oms ELSE 0 END AS onlmtd
					, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') AND pv_ = 0 THEN oms ELSE 0 END AS onlytd
				FROM(
					SELECT so.member_id, MAX(so.kit)AS kit_, SUM(totalpv)AS pv_, SUM(totalharga)AS oms, vm.joindate
					FROM so
					LEFT JOIN v_memberjoin vm ON so.member_id = vm.member_id
					WHERE 
					(joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
					OR joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					AND (tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
					OR tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					GROUP BY vm.member_id
					HAVING kit_ = 'y'
					ORDER BY pv_ DESC
				)AS dt
			)AS dt
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
		if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getNewRecruit($periode){
		$thn_ = $thn-1;
        $data=array();
		$qry = "
			SELECT nr
				, SUM(mtd)AS mtd
				, SUM(lmtd)AS lmtd
				, SUM(lytd)AS lytd
			FROM(
				SELECT
					1 AS nr, vm.member_id, joindate, YEAR(joindate)AS thn, MONTH(joindate)AS bln
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') THEN 1 ELSE 0 END AS mtd
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) THEN 1 ELSE 0 END AS lmtd
					, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') THEN 1 ELSE 0 END AS lytd
				FROM v_memberjoin vm
				LEFT JOIN(
					SELECT member_id, YEAR(so.tgl)AS thn, MONTH(so.tgl)AS bln, SUM(totalpv)AS pv
					FROM so
					WHERE totalpv > 0
					AND member_id <> 'STAFF'
					AND(tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
					OR tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					GROUP BY member_id, thn, bln
				)AS dt ON vm.member_id = dt.member_id AND YEAR(vm.joindate) = dt.thn AND MONTH(vm.joindate) = dt.bln
				WHERE dt.pv > 0
				AND(joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
				OR joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
			)AS dt
			GROUP BY nr
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getOmsetNewMember($periode){
		$thn_ = $thn-1;
        $data=array();
		$qry = "
			SELECT nr
				, SUM(mtd)AS mtd
				, SUM(lmtd)AS lmtd
				, SUM(lytd)AS lytd
			FROM(
				SELECT
					1 AS nr, vm.member_id, joindate, YEAR(joindate)AS thn, MONTH(joindate)AS bln
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') THEN dt.oms ELSE 0 END AS mtd
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) THEN dt.oms ELSE 0 END AS lmtd
					, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') THEN dt.oms ELSE 0 END AS lytd
				FROM v_memberjoin vm
				LEFT JOIN(
					SELECT member_id, YEAR(so.tgl)AS thn, MONTH(so.tgl)AS bln, SUM(totalharga)AS oms
					FROM so
					WHERE (tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
					OR tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					GROUP BY member_id, thn, bln
					-- order by member_id
				)AS dt ON vm.member_id = dt.member_id AND YEAR(vm.joindate) = dt.thn AND MONTH(vm.joindate) = dt.bln
				WHERE vm.member_id <> 'STAFF'
				AND (joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
				OR joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
			)AS dt
			GROUP BY nr
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getSponsoring($periode){
		$thn_ = $thn-1;
        $data=array();
		$qry = "
			SELECT 1 AS sp
				, SUM(mtd)AS mtd
				, SUM(lmtd)AS lmtd
				, SUM(lytd)AS lytd
			FROM(
				SELECT enroller_id, thn, bln
					, CASE WHEN thn = YEAR('$periode') AND bln = MONTH('$periode') THEN 1 ELSE 0 END AS mtd
					, CASE WHEN thn = YEAR('$periode') AND bln = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) THEN 1 ELSE 0 END AS lmtd
					, CASE WHEN thn = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') THEN 1 ELSE 0 END AS lytd
				FROM(
					SELECT
						1 AS nm, m.enroller_id, vm.member_id, joindate, YEAR(joindate)AS thn, MONTH(joindate)AS bln
						, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') THEN 1 ELSE 0 END AS mtd
						, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) THEN 1 ELSE 0 END AS lmtd
						, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') THEN 1 ELSE 0 END AS lytd
					FROM v_memberjoin vm
					LEFT JOIN member m ON vm.member_id = m.id
					WHERE vm.member_id <> 'STAFF'
					AND joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
					OR joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR))
					ORDER BY thn, bln, m.enroller_id
				)AS dt
				GROUP BY enroller_id, thn, bln
				ORDER BY thn, bln, enroller_id
			)AS dt
			GROUP BY sp
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getOmsetOldMember($periode){
		$thn_ = $thn-1;
        $data=array();
		$qry = "
			SELECT 'om' AS om
				, SUM(oldNow)AS oldNow
				, SUM(oldBlnLalu)AS oldBlnLalu
				, SUM(oldThnLalu)AS oldThnLalu
				, SUM(rNow)AS rNow
				, SUM(rBlnLalu)AS rBlnLalu
				, SUM(rThnLalu)AS rThnLalu
				, SUM(ooldNow)AS ooldNow
				, SUM(ooldBlnLalu)AS ooldBlnLalu
				, SUM(ooldThnLalu)AS ooldThnLalu
				, SUM(orNow)AS orNow
				, SUM(orBlnLalu)AS orBlnLalu
				, SUM(orThnLalu)AS orThnLalu
			FROM(
				SELECT member_id
					, CASE WHEN skrg+lalu = 2 THEN 1 ELSE 0 END AS oldNow
					, CASE WHEN lalu+lalu2 = 2 THEN 1 ELSE 0 END AS oldBlnLalu
					, CASE WHEN lalu11+lalu12 = 2 THEN 1 ELSE 0 END AS oldThnLalu
					, CASE WHEN skrg = 1 AND lalu = 0 THEN 1 ELSE 0 END AS rNow
					, CASE WHEN lalu = 1 AND lalu2 = 0 THEN 1 ELSE 0 END AS rBlnLalu
					, CASE WHEN lalu11 = 1 AND lalu12 = 0 THEN 1 ELSE 0 END AS rThnLalu
					, CASE WHEN skrg+lalu = 2 THEN oskrg+olalu ELSE 0 END AS ooldNow
					, CASE WHEN lalu+lalu2 = 2 THEN olalu+olalu2 ELSE 0 END AS ooldBlnLalu
					, CASE WHEN lalu11+lalu12 = 2 THEN olalu11+olalu12 ELSE 0 END AS ooldThnLalu
					, CASE WHEN skrg = 1 AND lalu = 0 THEN oskrg ELSE 0 END AS orNow
					, CASE WHEN lalu = 1 AND lalu2 = 0 THEN olalu ELSE 0 END AS orBlnLalu
					, CASE WHEN lalu11 = 1 AND lalu12 = 0 THEN olalu11 ELSE 0 END AS orThnLalu
				FROM(
					SELECT member_id, MAX(skrg)AS skrg
						, MAX(lalu)AS lalu
						, MAX(lalu2)AS lalu2
						, MAX(lalu11)AS lalu11
						, MAX(lalu12)AS lalu12
						, SUM(oskrg)AS oskrg
						, SUM(olalu)AS olalu
						, SUM(olalu2)AS olalu2
						, SUM(olalu11)AS olalu11
						, SUM(olalu12)AS olalu12
					FROM(
						SELECT member_id, tgl
							, CASE WHEN MONTH(tgl) = MONTH('$periode') AND YEAR(tgl) = YEAR('$periode') THEN 1 ELSE 0 END AS skrg
							, CASE WHEN MONTH(tgl) = MONTH('$periode')-1 AND YEAR(tgl) = YEAR('$periode') THEN 1 ELSE 0 END AS lalu
							, CASE WHEN MONTH(tgl) = MONTH('$periode')-2 AND YEAR(tgl) = YEAR('$periode') THEN 1 ELSE 0 END AS lalu2
							, CASE WHEN MONTH(tgl) = MONTH('$periode') AND YEAR(tgl) = YEAR('$periode')-1 THEN 1 ELSE 0 END AS lalu11
							, CASE WHEN MONTH(tgl) = MONTH('$periode')-1 AND YEAR(tgl) = YEAR('$periode')-1 THEN 1 ELSE 0 END AS lalu12
							, CASE WHEN MONTH(tgl) = MONTH('$periode') AND YEAR(tgl) = YEAR('$periode') THEN totalpv ELSE 0 END AS oskrg
							, CASE WHEN MONTH(tgl) = MONTH('$periode')-1 AND YEAR(tgl) = YEAR('$periode') THEN totalpv ELSE 0 END AS olalu
							, CASE WHEN MONTH(tgl) = MONTH('$periode')-2 AND YEAR(tgl) = YEAR('$periode') THEN totalpv ELSE 0 END AS olalu2
							, CASE WHEN MONTH(tgl) = MONTH('$periode') AND YEAR(tgl) = YEAR('$periode')-1 THEN totalpv ELSE 0 END AS olalu11
							, CASE WHEN MONTH(tgl) = MONTH('$periode')-1 AND YEAR(tgl) = YEAR('$periode')-1 THEN totalpv ELSE 0 END AS olalu12
						FROM so
						WHERE so.member_id <> 'STAFF'
						AND (tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 3 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode')))
						OR (tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 14 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					)AS dt
					GROUP BY member_id
				)AS dt
			)AS dt
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getOmsetOldMember_($periode){
		$thn_ = $thn-1;
        $data=array();
		$qry = "
			SELECT om
				, SUM(mtd)AS mtd
				, SUM(lmtd)AS lmtd
				, SUM(lytd)AS lytd
				
				, SUM(omtd)AS omtd
				, SUM(olmtd)AS olmtd
				, SUM(olytd)AS olytd
			FROM(
				SELECT 1 AS om, pv_, member_id, joindate, YEAR(joindate)AS thn, MONTH(joindate)AS bln
					, CASE WHEN 
						YEAR(joindate) <= YEAR('$periode') AND MONTH(joindate) <= MONTH('$periode')-1 AND pv_ > 0 
						AND tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
						THEN 1 ELSE 0 END AS mtd
					, CASE WHEN 
						YEAR(joindate) <= YEAR('$periode') AND MONTH(joindate) <= MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND pv_ > 0 
						AND tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 MONTH))
						THEN 1 ELSE 0 END AS lmtd
					, CASE WHEN YEAR(joindate) <= YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) <= MONTH('$periode')-1 AND pv_ > 0 
						AND tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR))
						THEN 1 ELSE 0 END AS lytd
					
					, CASE WHEN
						YEAR(joindate) <= YEAR('$periode') AND MONTH(joindate) <= MONTH('$periode')-1 AND pv_ > 0 
						AND tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
						THEN oms ELSE 0 END AS omtd
					, CASE WHEN
						YEAR(joindate) <= YEAR('$periode') AND MONTH(joindate) <= MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND pv_ > 0 
						AND tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 MONTH))
						THEN oms ELSE 0 END AS olmtd
					, CASE WHEN YEAR(joindate) <= YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) <= MONTH('$periode')-1 AND pv_ > 0 
						AND tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR))
						THEN oms ELSE 0 END AS olytd
				FROM(
					SELECT so.member_id, so.tgl, MAX(so.kit)AS kit_, SUM(totalpv)AS pv_, SUM(totalharga)AS oms, vm.joindate
					FROM so
					LEFT JOIN v_memberjoin vm ON so.member_id = vm.member_id
					WHERE so.member_id <> 'STAFF'
						AND (joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 3 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 MONTH))
						OR joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 14 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 13 MONTH)))
						AND (tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
						OR tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					GROUP BY vm.member_id
					HAVING kit_ = 'n'
					ORDER BY pv_ DESC
				)AS dt
			)AS dt
			GROUP BY om
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getOmsetStaff($periode){
		$thn_ = $thn-1;
        $data = array();
		$qry = "
			SELECT 'staff' as staff
				, SUM(mtd)AS mtd
				, SUM(lmtd)AS lmtd
				, SUM(lytd)AS lytd
			FROM(
				SELECT
					1 AS nr -- , vm.member_id, joindate, YEAR(joindate)AS thn, MONTH(joindate)AS bln
					, CASE WHEN thn = YEAR('$periode') AND bln = MONTH('$periode') THEN dt.oms ELSE 0 END AS mtd
					, CASE WHEN thn = YEAR('$periode') AND bln = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) THEN dt.oms ELSE 0 END AS lmtd
					, CASE WHEN thn = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND bln = MONTH('$periode') THEN dt.oms ELSE 0 END AS lytd
				FROM (
					SELECT member_id, YEAR(so.tgl)AS thn, MONTH(so.tgl)AS bln, SUM(totalharga)AS oms
					FROM so
					WHERE member_id LIKE 'STAFF%'
					AND (tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
					OR tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					GROUP BY member_id, thn, bln
				)AS dt
			)AS dt
			GROUP BY nr
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	/* End created by Boby 20130512 */
	
	/* Created by Boby 20130716 */
	
	public function getBaseModel($periode,$flag){
        $data = array();
		if($flag==0){
			$periode = "'$periode'";
		}elseif($flag==1){
			$periode = "('$periode' - INTERVAL 1 MONTH)";
		}elseif($flag==2){
			$periode = "('$periode' - INTERVAL 1 YEAR)";
		}else{
			$periode = "NOW()";
		}
		$qry = "
			SELECT 
				SUM(nm)AS nm
				, SUM(nr)AS nr
				, SUM(om)AS om
				, SUM(rm)AS rm
				, SUM(nm_oms)AS nm_oms
				, SUM(nr_oms)AS nr_oms
				, SUM(om_oms)AS om_oms
				, SUM(rm_oms)AS rm_oms
				, SUM(staff)AS staff
			FROM(
				SELECT so.member_id, m.nama, m.joindate AS joindate, so.oms, so_.pv
					, CASE WHEN m.joindate BETWEEN (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY($periode)) AND so.pv = 0 AND m.member_id <> 'STAFF' THEN 1 ELSE 0 END AS nm
					, CASE WHEN m.joindate BETWEEN (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY($periode)) AND so.pv <> 0 AND m.member_id <> 'STAFF' THEN 1 ELSE 0 END AS nr
					, CASE WHEN m.joindate < (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND so_.pv <> 0 AND m.member_id <> 'STAFF' THEN 1 ELSE 0 END AS om
					, CASE WHEN m.joindate < (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND IFNULL(so_.pv,0) = 0 AND m.member_id <> 'STAFF' THEN 1 ELSE 0 END AS rm
					
					, CASE WHEN m.joindate BETWEEN (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY($periode)) AND so.pv = 0 AND m.member_id <> 'STAFF' THEN so.oms ELSE 0 END AS nm_oms
					, CASE WHEN m.joindate BETWEEN (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY($periode)) AND so.pv <> 0 AND m.member_id <> 'STAFF' THEN so.oms ELSE 0 END AS nr_oms
					, CASE WHEN m.joindate < (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND so_.pv <> 0 AND m.member_id <> 'STAFF' THEN so.oms ELSE 0 END AS om_oms
					, CASE WHEN m.joindate < (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND IFNULL(so_.pv,0) = 0 AND m.member_id <> 'STAFF' THEN so.oms ELSE 0 END AS rm_oms
					, CASE WHEN so.member_id = 'STAFF' THEN so.oms ELSE 0 END AS staff
				FROM(
					SELECT YEAR(so.tgl)AS thn, MONTH(so.tgl)AS bln, so.member_id, SUM(so.totalharga)AS oms, SUM(totalpv)AS pv
					FROM so
					WHERE so.tgl BETWEEN (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY($periode))
					GROUP BY member_id, thn, bln
				)AS so
				LEFT JOIN(
					SELECT so.member_id, SUM(so.totalharga)AS pv
					FROM so
					WHERE so.tgl BETWEEN (LAST_DAY($periode - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY($periode - INTERVAL 1 MONTH))
					GROUP BY member_id
				)AS so_ ON so.member_id = so_.member_id
				LEFT JOIN(
					SELECT member_id, nama, CASE WHEN joindate > (LAST_DAY($periode)) THEN (LAST_DAY($periode)) ELSE joindate END AS joindate
					FROM v_memberjoin m 
				)AS m ON so.member_id = m.member_id

			)AS dt
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getTarget($periode,$flag){
        $data = array();
		if($flag==0){
			$periode = "'$periode'";
		}elseif($flag==1){
			$periode = "(LAST_DAY('$periode' - INTERVAL 1 MONTH))";
		}elseif($flag==2){
			$periode = "(LAST_DAY('$periode' - INTERVAL 1 YEAR))";
		}else{
			$periode = "(LAST_DAY(NOW()))";
		}
		$qry = "
			SELECT periode, SUM(target)AS target
			FROM region_target
			WHERE periode = $periode
			GROUP BY periode
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	/* End created by Boby 20130716 */
}
?>