<?php // Created by Boby 20130925
class MPrintout extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
	
	public function getPrintoutAddress(){
		$data = array();
		$qry = "
			SELECT UPPER(m.nama)AS nama_
				, CASE WHEN (m.created < LAST_DAY(NOW() - INTERVAL 11 MONTH)) THEN CONCAT(UPPER(m.nama),'.') ELSE UPPER(m.nama) END AS nama
				, UPPER(m.alamat)AS alamat, m.hp
				, CASE WHEN m.hp NOT LIKE '000%' AND m.hp IS NOT NULL AND LENGTH(m.hp)>9 THEN 1 ELSE 0 END AS note_hp
				, m.telp
				, CASE WHEN m.telp NOT LIKE '000%' AND m.telp IS NOT NULL AND LENGTH(m.telp)>9 THEN 1 ELSE 0 END AS note_telp
				, CASE 
					WHEN (m.hp NOT LIKE '000%' AND m.hp IS NOT NULL AND LENGTH(m.hp)>9) OR (m.telp NOT LIKE '000%' AND m.telp IS NOT NULL AND LENGTH(m.telp)>9) THEN 1
					ELSE 0 
				END AS note_all
				, CONCAT(m.hp, ' / ', m.telp)AS tlp
				, CASE WHEN m.kota_id = 0 THEN 0 ELSE k.name END AS kota
			FROM(
				SELECT so.member_id
				FROM so
				WHERE tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 4 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY(NOW() - INTERVAL 1 MONTH))
				GROUP BY member_id
			)AS dt
			LEFT JOIN member m ON dt.member_id = m.id
			LEFT JOIN kota k ON m.kota_id = k.id
			WHERE LENGTH(m.alamat) >= 15
			AND wordcount(m.alamat) >= 5
			-- AND m.created < LAST_DAY(NOW() - INTERVAL 11 MONTH)
			ORDER BY k.region DESC, k.propinsi_id DESC, k.id DESC
			-- AND nama LIKE 'b%' LIMIT 0, 20
		";
        $q = $this->db->query($qry);
        // echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
	}
	
}
?>