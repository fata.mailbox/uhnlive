<?php
class MRekap extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Laporan Omzet 
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-09-29
    |
    */
    public function omzet_ro($stc,$fromdate,$todate)
    {
        $where = "a.stockiest_id = '0' and (a.date between '$fromdate' and '$todate') ";
        if($stc == 'stc')$where = "s.type = '1' and ".$where;
        else $where = "s.type = '2' and ".$where;
        $this->db->select("a.id,a.member_id,date_format(a.date,'%d-%b-%Y')as tgl,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama as namamember,s.no_stc",false) 
            ->from('ro a')
            ->join('member m','a.member_id=m.id','left')
            ->join('stockiest s','a.member_id=s.id','left')
            ->where($where);
            
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_omzet_ro($stc,$fromdate,$todate){
        $where = "a.stockiest_id = '0' and (a.date between '$fromdate' and '$todate') ";
        if($stc == 'stc')$where = "s.type = '1' and ".$where;
        else $where = "s.type = '2' and ".$where;
        
        $data = array();
        $this->db->select("sum(a.totalharga)as totalharga,format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('ro a')
            ->join('stockiest s','a.member_id=s.id','left')
            ->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function omzet_so($fromdate,$todate)
    {
        $where = "a.stockiest_id = '0' and (a.tgl between '$fromdate' and '$todate') ";
        
        $this->db->select("a.id,a.member_id,date_format(a.tgl,'%d-%b-%Y')as tgl,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama as namamember,s.no_stc",false) 
            ->from('so a')->join('member m','a.member_id=m.id','left')->join('stockiest s','a.stockiest_id=s.id','left')
            ->where($where);
            
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_omzet_so($fromdate,$todate){
        $where = "a.stockiest_id = '0' and (a.tgl between '$fromdate' and '$todate') ";
        $data = array();
        $this->db->select("sum(a.totalharga)as totalharga,format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('so a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function omzet_retur($fromdate,$todate)
    {
        $where = "(a.tgl between '$fromdate' and '$todate') ";
        
        $this->db->select("a.id,a.stockiest_id as member_id,date_format(a.tgl,'%d-%b-%Y')as tgl,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama as namamember,s.no_stc",false) 
            ->from('retur_titipan a')->join('member m','a.stockiest_id=m.id','left')->join('stockiest s','a.stockiest_id=s.id','left')
            ->where($where);
            
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_omzet_retur($fromdate,$todate){
        $where = "(a.tgl between '$fromdate' and '$todate') ";
        $data = array();
        $this->db->select("sum(a.totalharga)as totalharga,format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('retur_titipan a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Rekap Sales Order f   or Member & Back Office
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-05-20
    |
    */
    public function list_salesorder($fromdate,$todate,$member_id)
    {
        if($this->session->userdata('group_id') <= 100){
			$select = "a.id,a.member_id,date_format(a.tgl,'%d-%b-%Y')as tgl,format(a.totalharga_,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.kit,a.remark,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
						m.nama as namamember,s.no_stc";
            if($member_id)$where = "a.member_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
            else $where = "(a.tgl between '$fromdate' and '$todate') ";
        }else{
            $select = "a.id,a.member_id,date_format(a.tgl,'%d-%b-%Y')as tgl,format(a.totalharga_,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.kit,a.remark,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
						m.nama as namamember,s.no_stc";
			$where = "a.member_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
        }
        $this->db->select($select,false) 
            ->from('so a')->join('member m','a.member_id=m.id','left')->join('stockiest s','a.stockiest_id=s.id','left')
            ->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                //$rs['date_added'] = mdate("%d/%m/%Y", mysql_to_unix($rs['date_added']));
                $rs['details'] = $this->list_salesorder_detail($rs['id']);
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function list_salesorder_detail($id=0)
    {
        if($this->session->userdata('group_id') <= 100){
			$select = "d.item_id,i.name,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.pv,0)as fpv,format(d.jmlharga,0)as fjmlharga,format(d.jmlpv,0)as fjmlpv";
		}else{
			$select = "d.item_id,i.name,format(d.qty,0)as fqty,format(d.harga_,0)as fharga,format(d.pv,0)as fpv,format(d.jmlharga_,0)as fjmlharga,format(d.jmlpv,0)as fjmlpv";
		}
		$q = $this->db->select($select,false)
        ->from('so_d d')->join('item i','d.item_id=i.id','left')->where('d.so_id',$id)->get();
        $details = array();
        
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $details[] = $rs;
            }
        }
        $q->free_result();
        return $details;
    }
    public function sum_salesorder($fromdate,$todate,$member_id)
    {
        if($this->session->userdata('group_id') <= 100){
			$select = "format(sum(a.totalharga_),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv";
            if($member_id)$where = "a.member_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
            else $where = "(a.tgl between '$fromdate' and '$todate') ";
        }else{
			$select = "format(sum(a.totalharga_),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv";
            $where = "a.member_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
        }
        
        $data = array();
        $this->db->select($select,false)
            ->from('so a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
    public function getWarehouse(){
        $data = array();
        $q=$this->db->select("id,name",false)
            ->from('warehouse')
            ->order_by('id','asc')
            ->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[$row['id']] = $row['name'];
            }
        }
        $q->free_result();
        return $data;
    }    
    
    public function countFreeSO($keywords=0){
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND a.id LIKE '$keywords%'";
        }
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
            else $where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        $this->db->select("a.id",false);
        $this->db->from('promofree a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }

    public function getFreeSO($id=0){
        $data = array();
        $this->db->select("a.id,a.approveddate,a.approvedby,a.remark,a.status,a.approvedby,s.no_stc,m.nama,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('promofree a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->session->userdata('group_id')>100)$this->db->where('a.stockiest_id',$this->session->userdata('userid'));
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$this->db->where('a.warehouse_id',$whsid); 
        }
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getFreeSODetail($id=0){
        $data = array();
        $this->db->select("d.item_id,d.tgl,format(d.qty,0)as fqty,d.member_id,m.nama,a.name",false);
        $this->db->from('promofree_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->join('member m','d.member_id=m.id','left');
        $this->db->where('d.promofree_id',$id);
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function getFreeSOApproved($id=0){
        $data = array();
        $this->db->select("a.id,a.approveddate,a.approvedby,a.warehouse_id,a.remark,a.status,a.approvedby,s.no_stc,m.nama,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('promofree a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->session->userdata('group_id')>100)$this->db->where('a.stockiest_id',$this->session->userdata('userid'));
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$this->db->where('a.warehouse_id',$whsid); 
        }
        $this->db->where('a.status','pending');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    public function freeSOApproved(){
        $id = $this->input->post('id');
        $remarkapp=$this->db->escape_str($this->input->post('remark'));
        $empid = $this->session->userdata('user');
        $whsid = $this->input->post('whsid');
        $this->db->query("call sp_free_so('$id','$whsid','$remarkapp','$empid')");
    }
    
    /*
    |--------------------------------------------------------------------------
    | Rekap Sales Order for Stockiest & Back Office
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-04
    |
    */
    public function list_salesorder_stc($fromdate,$todate,$member_id)
    {
        if($this->session->userdata('group_id') <= 100){
            if($member_id)$where = "a.stockiest_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
            else $where = "(a.tgl between '$fromdate' and '$todate') ";
        }else{
            $where = "a.stockiest_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
        }
        $this->db->select("a.id,a.member_id,date_format(a.tgl,'%d-%b-%Y')as tgl,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.kit,a.remark,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                          m.nama as namamember,s.no_stc",false)
            ->from('so a')->join('member m','a.member_id=m.id','left')->join('stockiest s','a.stockiest_id=s.id','left')
            ->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                //$rs['date_added'] = mdate("%d/%m/%Y", mysql_to_unix($rs['date_added']));
                $rs['details'] = $this->list_salesorder_detail($rs['id']);
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_salesorder_stc($fromdate,$todate,$member_id)
    {
        if($this->session->userdata('group_id') <= 100){
            if($member_id)$where = "a.stockiest_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
            else $where = "(a.tgl between '$fromdate' and '$todate') ";
        }else{
            $where = "a.stockiest_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
        }
        $data = array();
        $this->db->select("format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('so a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
    
    public function getDropDownWhs(){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            $data['all']='All Cabang';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    /*
    |--------------------------------------------------------------------------
    | Rekap Deposit Back Office
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-04
    |
    */
    public function list_deposit($fromdate,$todate,$whsid)
    {
        if($whsid == 'all')$where = "a.approved = 'approved' and (a.tgl_approved between '$fromdate' and '$todate') ";
        else $where = "a.warehouse_id = '$whsid' and a.approved = 'approved' and (a.tgl_approved between '$fromdate' and '$todate') ";
        $this->db->select("a.id,a.member_id,a.tgl_approved,a.approvedby,m.nama,s.no_stc,a.flag,format(a.transfer,0)as ftransfer,format(a.tunai,0)as ftunai,format(a.debit_card,0)as fdebit_card,
                          format(a.credit_card,0)as fcredit_card,format(a.total_approved,0)as ftotal_approved",false)
            ->from('deposit a')->join('member m','a.member_id=m.id','left')->join('stockiest s','a.member_id=s.id','left')
            ->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_deposit($fromdate,$todate,$whsid)
    {
        if($whsid == 'all')$where = "a.approved = 'approved' and (a.tgl_approved between '$fromdate' and '$todate') ";
        else $where = "a.warehouse_id = '$whsid' and a.approved = 'approved' and (a.tgl_approved between '$fromdate' and '$todate') ";
        $this->db->select("format(sum(a.transfer),0)as ftransfer,format(sum(a.tunai),0)as ftunai,format(sum(a.debit_card),0)as fdebit_card,
                          format(sum(a.credit_card),0)as fcredit_card,format(sum(a.total_approved),0)as ftotal_approved,sum(a.total_approved)as total_approved",false)
            ->from('deposit a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    /*
    |--------------------------------------------------------------------------
    | Rekap Withdrawal Back Office
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-05
    |
    */
    public function list_withdrawal($via,$fromdate,$todate)
    {
        $where = "a.status = 'approved' and (a.approved between '$fromdate' and '$todate') ";
        if($via)$where="a.event_id = '$via' and ".$where;
        
        $this->db->select("a.id,a.member_id,a.approved,a.approvedby,m.nama,a.flag,format(a.amount,0)as famount,format(a.biayaadm,0)as fbiayaadm,format(a.amount-a.biayaadm,0)as fbayar,c.no_stc,s.bank_id,s.name,s.no,s.area",false)
            ->from('withdrawal a')->join('member m','a.member_id=m.id','left')->join('stockiest c','a.member_id=c.id','left')->join('account s','a.account_id=s.id','left')
            ->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_withdrawal($via,$fromdate,$todate)
    {
        $where = "a.status = 'approved' and (a.approved between '$fromdate' and '$todate') ";
        if($via)$where="a.event_id = '$via' and ".$where;
        
        $this->db->select("format(sum(a.amount),0)as famount,sum(a.amount)as amount,format(sum(a.biayaadm),0)as fbiayaadm,format(sum(a.amount)-sum(a.biayaadm),0)as fbayar",false)
            ->from('withdrawal a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Neraca Daily Fund
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-06
    |
    */
    public function sum_SO($fromdate,$todate,$whsid,$so,$flag)
    {
        if($so == 'so'){
            if($flag == 'order'){
                if($whsid == 'all')$where = "a.stockiest_id = '0' and (a.tgl between '$fromdate' and '$todate') ";
                else $where = "a.warehouse_id = '$whsid' and a.stockiest_id = '0' and (a.tgl between '$fromdate' and '$todate') ";
            }else{
                if($whsid == 'all')$where = "a.stockiest_id = '0' and (a.created between '$fromdate' and '$todate') ";
                else $where = "a.warehouse_id = '$whsid' and a.stockiest_id = '0' and (a.created between '$fromdate' and '$todate') ";
            }
        }elseif($so == 'stc'){
            if($flag == 'order'){
                if($whsid == 'all')$where = "a.stockiest_id != '0' and (a.tgl between '$fromdate' and '$todate') ";
                else $where = "a.warehouse_id = '$whsid' and a.stockiest_id != '0' and (a.tgl between '$fromdate' and '$todate') ";
            }else{
                if($whsid == 'all')$where = "a.stockiest_id != '0' and (a.created between '$fromdate' and '$todate') ";
                else $where = "a.warehouse_id = '$whsid' and a.stockiest_id != '0' and (a.created between '$fromdate' and '$todate') ";
            }
        }else{
            if($flag == 'order'){
                if($whsid == 'all')$where = "(a.tgl between '$fromdate' and '$todate') ";
                else $where = "a.warehouse_id = '$whsid' and (a.tgl between '$fromdate' and '$todate') ";
            }else{
                if($whsid == 'all')$where = "(a.created between '$fromdate' and '$todate') ";
                else $where = "a.warehouse_id = '$whsid' and (a.created between '$fromdate' and '$todate') ";
            }
        }
        $data = array();
        $this->db->select("format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('so a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function sum_SOKIT($fromdate,$todate,$whsid,$so)
    {
        if($so == 'so'){
            if($whsid == 'all')$where = "a.kit = 'Y' and a.stockiest_id = '0' and (a.tgl between '$fromdate' and '$todate') ";
            else $where = "a.kit = 'Y' and a.warehouse_id = '$whsid' and a.stockiest_id = '0' and (a.tgl between '$fromdate' and '$todate') ";
        }else{
            if($whsid == 'all')$where = "a.kit = 'Y' and a.stockiest_id != '0' and (a.tgl between '$fromdate' and '$todate') ";
            else $where = "a.kit = 'Y' and a.warehouse_id = '$whsid' and a.stockiest_id != '0' and (a.tgl between '$fromdate' and '$todate') ";
        }
        $data = array();
        $this->db->select("format(count(*),0)as fjmlmember,format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('so a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function sum_RO($fromdate,$todate,$whsid,$flag){
        if($flag == 'company'){
            if($whsid == 'all')$where = "a.stockiest_id = '0' and (a.date between '$fromdate' and '$todate') ";
            else $where = "a.warehouse_id = '$whsid' and a.stockiest_id = '0' and (a.date between '$fromdate' and '$todate') ";
        }else{
            if($whsid == 'all')$where = "a.stockiest_id != '0' and (a.date between '$fromdate' and '$todate') ";
            else $where = "a.warehouse_id = '$whsid' and a.stockiest_id != '0' and (a.date between '$fromdate' and '$todate') ";
        }
        $data = array();
        $this->db->select("format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('ro a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Rekap History Pinjaman Stockiest
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-09
    |
    */
    public function list_saldopinjaman($stcid)
    {
        $this->db->select("a.item_id,a.name,format(sum(a.qty),0)as fqty,a.fharga,a.fpv,format(sum(a.jmlharga),0)as ftotalharga,format(sum(a.jmlpv),0)as ftotalpv",false)
            ->from('v_pinjaman a');
        if($stcid)$this->db->where('a.stockiest_id',$stcid);
        $this->db->group_by('a.item_id');
        $this->db->group_by('a.harga');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_saldopinjaman($stcid)
    {
         $this->db->select("format(sum(a.jmlharga),0)as ftotalharga,format(sum(a.jmlpv),0)as ftotalpv",false)
            ->from('v_pinjaman a');
        if($stcid)$this->db->where('a.stockiest_id',$stcid);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function list_pinjamanhistory($fromdate,$todate,$stcid)
    {
        $where = "a.stockiest_id = '$stcid' and (a.tgl between '$fromdate' and '$todate') ";
        
        $this->db->select("a.noref,date_format(a.tgl,'%d-%b-%Y')as tgl,createdby,format(a.awal,0)as fawal,format(a.pinjam,0)as fpinjam,format(a.lunas,0)as flunas,format(a.akhir,0)as fakhir,a.remark",false)
            ->from('pinjaman_titipan_h a');
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=1;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_pinjamanhistory($fromdate,$todate,$stcid)
    {
        $where = "a.stockiest_id = '$stcid' and (a.tgl between '$fromdate' and '$todate') ";
        
         $this->db->select("format(sum(a.pinjam),0)as ftotalpinjam,format(sum(a.lunas),0)as ftotallunas",false)
            ->from('pinjaman_titipan_h a');
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function list_pinjaman_stock($fromdate,$todate,$stcid)
    {
        $where = "a.member_id = '$stcid' and (a.tgl between '$fromdate' and '$todate') ";
        
        $this->db->select("a.noref,date_format(a.tgl,'%d-%b-%Y')as ftgl,a.createdby,format(a.saldoawal,0)as fawal,
                          format(a.saldoin,0)as fin,format(a.saldoout,0)as fout,format(a.saldoakhir,0)as fakhir,
                          format(a.harga,0)as fharga,a.item_id,i.name",false)
            ->from('pinjaman_history a')
            ->join('item i','a.item_id=i.id','left')
            ->where($where)
            ->order_by('a.item_id','asc')
            ->order_by('a.harga','asc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    /*
    |--------------------------------------------------------------------------
    | Rekap Pinjaman & Pembayaran Stockiest
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-10
    |
    */
    public function sum_Pinjaman($fromdate,$todate,$whsid){
        if($whsid == 'all')$where = "(a.tgl between '$fromdate' and '$todate') ";
        else $where = "a.warehouse_id = '$whsid' and (a.tgl between '$fromdate' and '$todate') ";
        
        $data = array();
        $this->db->select("format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('pinjaman a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function sum_Pelunasan($fromdate,$todate){
        $where = "(a.tgl between '$fromdate' and '$todate') ";
        
        $data = array();
        $this->db->select("format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('pinjaman_titipan a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function sum_saldotitipan()
    {
         $this->db->select("format(sum(a.jmlharga),0)as ftotalharga,format(sum(a.jmlpv),0)as ftotalpv",false)
            ->from('v_titipan a');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function sum_saldoewallet($table){
        $data = array();
        $this->db->select("format(sum(ewallet),0)as ftotalewallet",false)
            ->from($table);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
	
	/* Created by Boby (2009-12-10) */
	public function omset_ro($fromdate,$todate)
    {
		$flag = 1;
		$q = "	
		SELECT 
			id, nama
			, FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			, ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama, dta.price AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
			FROM(
				SELECT id, `name`, price FROM item
			)AS dta

			LEFT JOIN(
				SELECT sod.item_id, SUM(qty)AS jml, sod.harga AS harga";
		if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM so_d sod
				LEFT JOIN so ON sod.so_id = so.id
				WHERE 
					so.tgl BETWEEN '$fromdate' AND '$todate'
					AND so.stockiest_id = '0'
				GROUP BY item_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id

			LEFT JOIN(
				SELECT rod.item_id, SUM(qty)AS jml, rod.harga AS harga";
		if($flag==0){$q.="	, SUM(rod.jmlharga)AS total";}else{$q.="	, SUM(1*rod.jmlharga)AS total";}
		$q.="	FROM ro_d rod
				LEFT JOIN ro ON rod.ro_id = ro.id
				WHERE 
					ro.`date` BETWEEN '$fromdate' AND '$todate'
					AND ro.stockiest_id = '0'
				GROUP BY item_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id

			LEFT JOIN(
				SELECT ptd.item_id, SUM(qty) AS jml, ptd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM pinjaman_titipan_d ptd
				LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
				WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY ptd.item_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id

			LEFT JOIN(
				SELECT rtd.item_id, SUM(qty) AS jml, rtd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM retur_titipan_d rtd
				LEFT JOIN retur_titipan rt ON rtd.retur_titipan_id = rt.id
				WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY rtd.item_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id
			
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				GROUP BY ncd.item_id
			)AS dtf ON dta.id = dtf.item_id
		)AS newdata
		";
        $data = array();
		$qry = $this->db->query($q);
		echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
	public function omzet_scp($fromdate,$todate)
    {
        $data = array();$i=0;
		$qry = $this->db->query("		
			SELECT pin.id, pin.tgl, s.no_stc, m.nama, pin.totalharga, pin.totalpv
			FROM pinjaman_titipan pin
			LEFT JOIN member m ON pin.stockiest_id = m.id
			LEFT JOIN stockiest s ON pin.stockiest_id = s.id
			WHERE tgl BETWEEN '$fromdate' AND '$todate'
		");
		
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$i+=1;
				$row['i'] = $i;
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
	public function sum_omzet_scp($fromdate,$todate){
        $data = array();
		$qry = $this->db->query("		
			SELECT SUM(pin.totalharga)AS totalhrg, SUM(pin.totalpv)AS totalpv
			FROM pinjaman_titipan pin
			LEFT JOIN member m ON pin.stockiest_id = m.id
			LEFT JOIN stockiest s ON pin.stockiest_id = s.id
			WHERE tgl BETWEEN '$fromdate' AND '$todate'
		");
        if($qry->num_rows()>0){
            $data = $qry->row_array();
        }
        $qry->free_result();
        return $data;
    }
    /* End created by Boby (2009-12-10) */
	
	/* Created by Boby 20100720 */
	public function periodeOmset($fromdate){
		$data = array();
		$qry = $this->db->query("
			SELECT CONCAT(MONTHNAME('$fromdate'), ' ',YEAR('$fromdate')) AS bln1
			, CONCAT(MONTHNAME('$fromdate' + INTERVAL 1 MONTH), ' ',YEAR('$fromdate' + INTERVAL 1 MONTH)) AS bln2
			, CONCAT(MONTHNAME('$fromdate' + INTERVAL 2 MONTH), ' ',YEAR('$fromdate' + INTERVAL 2 MONTH)) AS bln3
			, CONCAT(MONTHNAME('$fromdate' + INTERVAL 3 MONTH), ' ',YEAR('$fromdate' + INTERVAL 3 MONTH)) AS bln4
			, CONCAT(MONTHNAME('$fromdate' + INTERVAL 4 MONTH), ' ',YEAR('$fromdate' + INTERVAL 4 MONTH)) AS bln5
			, CONCAT(MONTHNAME('$fromdate' + INTERVAL 5 MONTH), ' ',YEAR('$fromdate' + INTERVAL 5 MONTH)) AS bln6
			, MONTH('$fromdate') AS b1
			, MONTH('$fromdate' + INTERVAL 1 MONTH) AS b2
			, MONTH('$fromdate' + INTERVAL 2 MONTH) AS b3
			, MONTH('$fromdate' + INTERVAL 3 MONTH) AS b4
			, MONTH('$fromdate' + INTERVAL 4 MONTH) AS b5
			, MONTH('$fromdate' + INTERVAL 5 MONTH) AS b6
		");
        if($qry->num_rows()>0){
            $data = $qry->row_array();
        }
        $qry->free_result();
        return $data;
    }

	public function omsetPerKota($fromdate, $todate, $leader){
        $data = array();
		//$leader = 1;
		$query = "
		SELECT *, dt4.1+dt4.2+dt4.3+dt4.4+dt4.5+dt4.6+dt4.7+dt4.8+dt4.9+dt4.10+dt4.11+dt4.12 AS total
		FROM (
			SELECT ";
				if($leader == 1){$query .= " leader, nmLeader as kota";}else{$query .= " kota ";} $query.="
				, IFNULL(SUM(bln1),0) AS '1' , IFNULL(SUM(bln2),0) AS '2'
				, IFNULL(SUM(bln3),0) AS '3' , IFNULL(SUM(bln4),0) AS '4'
				, IFNULL(SUM(bln5),0) AS '5' , IFNULL(SUM(bln6),0) AS '6'
				, IFNULL(SUM(bln7),0) AS '7' , IFNULL(SUM(bln8),0) AS '8'
				, IFNULL(SUM(bln9),0) AS '9' , IFNULL(SUM(bln10),0) AS '10'
				, IFNULL(SUM(bln11),0) AS '11' , IFNULL(SUM(bln12),0) AS '12'
			FROM (
				SELECT";
					if($leader == 1){$query .= " bln, leader, nmLeader ";}else{$query .= " bln, kota ";} $query.="
					, CASE WHEN bln = 1 THEN omset END AS 'bln1' , CASE WHEN bln = 2 THEN omset END AS 'bln2'
					, CASE WHEN bln = 3 THEN omset END AS 'bln3' , CASE WHEN bln = 4 THEN omset END AS 'bln4'
					, CASE WHEN bln = 5 THEN omset END AS 'bln5' , CASE WHEN bln = 6 THEN omset END AS 'bln6'
					, CASE WHEN bln = 7 THEN omset END AS 'bln7' , CASE WHEN bln = 8 THEN omset END AS 'bln8'
					, CASE WHEN bln = 9 THEN omset END AS 'bln9' , CASE WHEN bln = 10 THEN omset END AS 'bln10'
					, CASE WHEN bln = 11 THEN omset END AS 'bln11' , CASE WHEN bln = 12 THEN omset END AS 'bln12'
				FROM (
					SELECT bln, txt, num, SUM(omset)AS omset, ";
					if($leader == 1){$query .= " leader, nmLeader ";}else{$query .= " kota_id, kota ";} $query.="
					FROM (
						SELECT 
							MONTH(so.tgl)AS bln, CONCAT(MONTHNAME(so.tgl), ' ', YEAR(so.tgl)) AS txt, CONCAT(YEAR(so.tgl), MONTH(so.tgl)) AS num
							, so.stockiest_id, IFNULL(SUM(so.totalharga),0) AS omset, ";
							if($leader == 1){$query .= " stc.leader_id AS leader, l.nama AS nmLeader ";}
							else{$query .= " stc.kota_id, IFNULL(k.`name`, '__Unknown__') AS kota ";} $query.="
						FROM so
						LEFT JOIN stockiest stc ON so.stockiest_id = stc.id ";
						if($leader != 1){$query .= " LEFT JOIN kota k ON stc.kota_id = k.id ";}
						else{$query .= " LEFT JOIN member l ON stc.leader_id = l.id ";} 
						$query.="
						WHERE so.tgl BETWEEN '$fromdate' AND ";
						if($todate==$fromdate){
							// $query .= "LAST_DAY('$fromdate' + INTERVAL 5 MONTH)";
							$query .= "'$todate' ";
						}else{
							$query .= "'$todate' ";
						}
						$query .="
						GROUP BY MONTHNAME(so.tgl), so.stockiest_id";
					if($leader == 1){$query .= " 
					)AS dt1
					GROUP BY bln, leader
					ORDER BY num, leader
				)AS dt2
				ORDER BY leader, num
			)AS dt3
			GROUP BY leader
		)AS dt4
		ORDER BY total DESC ";}else{$query .= " 
					)AS dt1
					GROUP BY bln, kota
					ORDER BY num, kota_id
				)AS dt2
				ORDER BY kota, num
			)AS dt3
			GROUP BY kota
		)AS dt4
		ORDER BY total DESC";} $query.="
		";
		
		$qry = $this->db->query($query);
		
		// echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
	public function periodeOmsetPerProduct($fromdate, $leader, $leaderId, $kota_id){
		//$leader = 1;
		$data = array();
		$query = "
		SELECT *
			, dt4.jml1+dt4.jml2+dt4.jml3+dt4.jml4+dt4.jml5+dt4.jml6+dt4.jml7+dt4.jml8+dt4.jml9+dt4.jml10+dt4.jml11+dt4.jml12 AS total
			, dt4.bln1+dt4.bln2+dt4.bln3+dt4.bln4+dt4.bln5+dt4.bln6+dt4.bln7+dt4.bln8+dt4.bln9+dt4.bln10+dt4.bln11+dt4.bln12 AS totalJml
		FROM (
			SELECT 
				";$query.=($leader == 0)?"kota_id, ifnull(k.`name`, '__Unknown__') AS nama":"stc, m.nama";$query.="
				, item_id, i.`name` as kota
				, IFNULL(SUM(jml1),0) AS jml1, IFNULL(SUM(bln1),0) AS bln1
				, IFNULL(SUM(jml2),0) AS jml2, IFNULL(SUM(bln2),0) AS bln2
				, IFNULL(SUM(jml3),0) AS jml3, IFNULL(SUM(bln3),0) AS bln3
				, IFNULL(SUM(jml4),0) AS jml4, IFNULL(SUM(bln4),0) AS bln4
				, IFNULL(SUM(jml5),0) AS jml5, IFNULL(SUM(bln5),0) AS bln5
				, IFNULL(SUM(jml6),0) AS jml6, IFNULL(SUM(bln6),0) AS bln6
				, IFNULL(SUM(jml7),0) AS jml7, IFNULL(SUM(bln7),0) AS bln7
				, IFNULL(SUM(jml8),0) AS jml8, IFNULL(SUM(bln8),0) AS bln8
				, IFNULL(SUM(jml9),0) AS jml9, IFNULL(SUM(bln9),0) AS bln9
				, IFNULL(SUM(jml10),0) AS jml10, IFNULL(SUM(bln10),0) AS bln10
				, IFNULL(SUM(jml11),0) AS jml11, IFNULL(SUM(bln11),0) AS bln11
				, IFNULL(SUM(jml12),0) AS jml12, IFNULL(SUM(bln12),0) AS bln12
			FROM (
				SELECT
					bln, item_id, 
					";$query.=($leader == 0)?"kota AS kota_id":"stc";$query.="
					, CASE WHEN bln = 1 THEN qty END AS jml1 , CASE WHEN bln = 1 THEN jmlharga END AS bln1
					, CASE WHEN bln = 2 THEN qty END AS jml2 , CASE WHEN bln = 2 THEN jmlharga END AS bln2
					, CASE WHEN bln = 3 THEN qty END AS jml3 , CASE WHEN bln = 3 THEN jmlharga END AS bln3
					, CASE WHEN bln = 4 THEN qty END AS jml4 , CASE WHEN bln = 4 THEN jmlharga END AS bln4
					, CASE WHEN bln = 5 THEN qty END AS jml5 , CASE WHEN bln = 5 THEN jmlharga END AS bln5
					, CASE WHEN bln = 6 THEN qty END AS jml6 , CASE WHEN bln = 6 THEN jmlharga END AS bln6
					, CASE WHEN bln = 7 THEN qty END AS jml7 , CASE WHEN bln = 7 THEN jmlharga END AS bln7
					, CASE WHEN bln = 8 THEN qty END AS jml8 , CASE WHEN bln = 8 THEN jmlharga END AS bln8
					, CASE WHEN bln = 9 THEN qty END AS jml9 , CASE WHEN bln = 9 THEN jmlharga END AS bln9
					, CASE WHEN bln = 10 THEN qty END AS jml10 , CASE WHEN bln = 10 THEN jmlharga END AS bln10
					, CASE WHEN bln = 11 THEN qty END AS jml11 , CASE WHEN bln = 11 THEN jmlharga END AS bln11
					, CASE WHEN bln = 12 THEN qty END AS jml12 , CASE WHEN bln = 12 THEN jmlharga END AS bln12
				FROM (
					SELECT MONTH(so.tgl)AS bln, sod.item_id, 
						";$query.=($leader == 0)?"ifnull(s.kota_id,'__Unknown_') AS kota,":"s.leader_id AS stc,";$query.="
						SUM(sod.qty) AS qty, sod.harga, SUM(sod.qty)*sod.harga AS total, SUM(sod.jmlharga) AS jmlharga
					FROM so_d sod
					LEFT JOIN so ON sod.so_id = so.id
					LEFT JOIN stockiest s ON so.stockiest_id = s.id
					WHERE
						so.tgl BETWEEN '$fromdate' AND LAST_DAY('$fromdate' + INTERVAL 5 MONTH)
					GROUP BY sod.item_id, 
					";$query.=($leader == 0)?"kota":"stc";$query.="
					,MONTH(so.tgl)
					ORDER BY ";$query.=($leader == 0)?"kota":"stc";$query.=", bln, item_id
				)AS dt
				GROUP BY bln, item_id, ";$query.=($leader == 0)?"kota":"stc";$query.="
				ORDER BY ";$query.=($leader == 0)?"kota":"stc";$query.=", bln, item_id
			)AS dt1
			";$query.=($leader == 0)?"LEFT JOIN kota k ON dt1.kota_id = k.id":"LEFT JOIN member m ON dt1.stc = m.id";$query.="
			LEFT JOIN item i ON dt1.item_id = i.id ";
			if($leaderId!='' && $leader==1){$query.=" where stc = '$leaderId' ";}
			if($kota_id!='' && $leader==0){$query.=" where kota_id = '$kota_id' ";}
			$query.= " GROUP BY item_id, ";$query.=($leader == 0)?"kota_id":"stc";$query.="
			ORDER BY ";$query.=($leader == 0)?"kota_id":"stc";$query.=", item_id
		)AS dt4";
		$qry = $this->db->query($query);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
        $qry->free_result();
        return $data;
    }
	/* End created by Boby 20100720 */
	
	public function omset_ro_($fromdate,$todate)
    {
		$flag = 1;
		$q = "	
		SELECT 
			id, nama
			-- , FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			, ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
			, (total + totsc + 0) + totretur AS totalnominal1_
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama -- , dta.harga AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
			FROM(
				SELECT i.id, i.`name` -- , sod.harga
				FROM item i
				-- LEFT JOIN so_d sod ON i.id = sod.item_id
				-- GROUP BY sod.item_id -- , sod.harga
			)AS dta
			LEFT JOIN(
				SELECT sod.item_id, SUM(qty)AS jml, sod.harga AS harga";
		if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM so_d sod
				LEFT JOIN so ON sod.so_id = so.id
				WHERE 
					so.tgl BETWEEN '$fromdate' AND '$todate'
					AND so.stockiest_id = '0'
				GROUP BY item_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT rod.item_id, SUM(qty)AS jml, rod.harga AS harga";
		if($flag==0){$q.="	, SUM(rod.jmlharga)AS total";}else{$q.="	, SUM(1*rod.jmlharga)AS total";}
		$q.="	FROM ro_d rod
				LEFT JOIN ro ON rod.ro_id = ro.id
				WHERE 
					ro.`date` BETWEEN '$fromdate' AND '$todate'
					AND ro.stockiest_id = '0'
				GROUP BY item_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT ptd.item_id, SUM(qty) AS jml, ptd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM pinjaman_titipan_d ptd
				LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
				WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY ptd.item_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
			LEFT JOIN(
				SELECT rtd.item_id, SUM(qty) AS jml, rtd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM retur_titipan_d rtd
				LEFT JOIN retur_titipan rt ON rtd.retur_titipan_id = rt.id
				WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY rtd.item_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				GROUP BY ncd.item_id
			)AS dtf ON dta.id = dtf.item_id
		)AS newdata
		HAVING totalnominal1_ <> 0 OR jmlnc <> 0
		order by nama
		";
        $data = array();
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
    
	/* Created by Boby 2012-08-06 */
	public function periodeOmset_($fromdate){
		$data = array();
		$qry = $this->db->query("
			SELECT 
			  CONCAT(MONTHNAME('$fromdate' - INTERVAL 5 MONTH), ' ',YEAR('$fromdate' - INTERVAL 5 MONTH)) AS bln1
			, CONCAT(MONTHNAME('$fromdate' - INTERVAL 4 MONTH), ' ',YEAR('$fromdate' - INTERVAL 4 MONTH)) AS bln2
			, CONCAT(MONTHNAME('$fromdate' - INTERVAL 3 MONTH), ' ',YEAR('$fromdate' - INTERVAL 3 MONTH)) AS bln3
			, CONCAT(MONTHNAME('$fromdate' - INTERVAL 2 MONTH), ' ',YEAR('$fromdate' - INTERVAL 2 MONTH)) AS bln4
			, CONCAT(MONTHNAME('$fromdate' - INTERVAL 1 MONTH), ' ',YEAR('$fromdate' - INTERVAL 1 MONTH)) AS bln5
			, CONCAT(MONTHNAME('$fromdate'), ' ',YEAR('$fromdate')) AS bln6
			, MONTH('$fromdate' - INTERVAL 5 MONTH) AS b1
			, MONTH('$fromdate' - INTERVAL 4 MONTH) AS b2
			, MONTH('$fromdate' - INTERVAL 3 MONTH) AS b3
			, MONTH('$fromdate' - INTERVAL 2 MONTH) AS b4
			, MONTH('$fromdate' - INTERVAL 1 MONTH) AS b5
			, MONTH('$fromdate') AS b6
		");
        if($qry->num_rows()>0){
            $data = $qry->row_array();
        }
        $qry->free_result();
        return $data;
    }
	
	public function omsetByRgn($fromdate){
        $data = array();
		//$leader = 1;
		$query = "
SELECT *
FROM(
	SELECT k.id
	, IFNULL(oms,'UHN')AS oms
	,k.name AS kota
	,(IFNULL(st.bln1,'0')+IFNULL(u.bln1,'0'))AS '1'
	,(IFNULL(st.bln2,'0')+IFNULL(u.bln2,'0'))AS '2'
	,(IFNULL(st.bln3,'0')+IFNULL(u.bln3,'0'))AS '3'
	,(IFNULL(st.bln4,'0')+IFNULL(u.bln4,'0'))AS '4'
	,(IFNULL(st.bln5,'0')+IFNULL(u.bln5,'0'))AS '5'
	,(IFNULL(st.bln6,'0')+IFNULL(u.bln6,'0'))AS '6'
	,(IFNULL(st.bln7,'0')+IFNULL(u.bln7,'0'))AS '7'
	,(IFNULL(st.bln8,'0')+IFNULL(u.bln8,'0'))AS '8'
	,(IFNULL(st.bln9,'0')+IFNULL(u.bln9,'0'))AS '9'
	,(IFNULL(st.bln10,'0')+IFNULL(u.bln10,'0'))AS '10'
	,(IFNULL(st.bln11,'0')+IFNULL(u.bln11,'0'))AS '11'
	,(IFNULL(st.bln12,'0')+IFNULL(u.bln12,'0'))AS '12'
	,IFNULL(st.bln1,'0')+IFNULL(u.bln1,'0')
		+IFNULL(st.bln2,'0')+IFNULL(u.bln2,'0')
		+IFNULL(st.bln3,'0')+IFNULL(u.bln3,'0')
		+IFNULL(st.bln4,'0')+IFNULL(u.bln4,'0')
		+IFNULL(st.bln5,'0')+IFNULL(u.bln5,'0')
		+IFNULL(st.bln6,'0')+IFNULL(u.bln6,'0')
		+IFNULL(st.bln7,'0')+IFNULL(u.bln7,'0')
		+IFNULL(st.bln8,'0')+IFNULL(u.bln8,'0')
		+IFNULL(st.bln9,'0')+IFNULL(u.bln9,'0')
		+IFNULL(st.bln10,'0')+IFNULL(u.bln10,'0')
		+IFNULL(st.bln11,'0')+IFNULL(u.bln11,'0')
		+IFNULL(st.bln12,'0')+IFNULL(u.bln12,'0')
	AS total
	FROM kota k
	LEFT JOIN(
		SELECT 'Stockiest' AS oms
			, dt.kota_id
			,IFNULL(SUM(dt.bln1),'0')AS bln1
			,IFNULL(SUM(dt.bln2),'0')AS bln2
			,IFNULL(SUM(dt.bln3),'0')AS bln3
			,IFNULL(SUM(dt.bln4),'0')AS bln4
			,IFNULL(SUM(dt.bln5),'0')AS bln5
			,IFNULL(SUM(dt.bln6),'0')AS bln6
			,IFNULL(SUM(dt.bln7),'0')AS bln7
			,IFNULL(SUM(dt.bln8),'0')AS bln8
			,IFNULL(SUM(dt.bln9),'0')AS bln9
			,IFNULL(SUM(dt.bln10),'0')AS bln10
			,IFNULL(SUM(dt.bln11),'0')AS bln11
			,IFNULL(SUM(dt.bln12),'0')AS bln12
		FROM (
			SELECT  MONTH(s.tgl)AS bln, st.kota_id, SUM(s.totalharga) AS omset
				,CASE WHEN MONTH(s.tgl)=1 THEN SUM(s.totalharga) END AS bln1
				,CASE WHEN MONTH(s.tgl)=2 THEN SUM(s.totalharga) END AS bln2
				,CASE WHEN MONTH(s.tgl)=3 THEN SUM(s.totalharga) END AS bln3
				,CASE WHEN MONTH(s.tgl)=4 THEN SUM(s.totalharga) END AS bln4
				,CASE WHEN MONTH(s.tgl)=5 THEN SUM(s.totalharga) END AS bln5
				,CASE WHEN MONTH(s.tgl)=6 THEN SUM(s.totalharga) END AS bln6
				,CASE WHEN MONTH(s.tgl)=7 THEN SUM(s.totalharga) END AS bln7
				,CASE WHEN MONTH(s.tgl)=8 THEN SUM(s.totalharga) END AS bln8
				,CASE WHEN MONTH(s.tgl)=9 THEN SUM(s.totalharga) END AS bln9
				,CASE WHEN MONTH(s.tgl)=10 THEN SUM(s.totalharga) END AS bln10
				,CASE WHEN MONTH(s.tgl)=11 THEN SUM(s.totalharga) END AS bln11
				,CASE WHEN MONTH(s.tgl)=12 THEN SUM(s.totalharga) END AS bln12
			FROM so s
			LEFT JOIN stockiest st ON s.stockiest_id=st.id
			WHERE s.tgl BETWEEN (LAST_DAY('$fromdate'-INTERVAL 6 MONTH) + INTERVAL 1 DAY) AND LAST_DAY('$fromdate')
			AND st.kota_id<>0
			GROUP BY bln,st.kota_id
		)AS dt
		GROUP BY dt.kota_id
	)AS st ON k.id=st.kota_id
	LEFT JOIN(
		SELECT dt.kota_id
			,IFNULL(SUM(dt.bln1),'0')AS bln1
			,IFNULL(SUM(dt.bln2),'0')AS bln2
			,IFNULL(SUM(dt.bln3),'0')AS bln3
			,IFNULL(SUM(dt.bln4),'0')AS bln4
			,IFNULL(SUM(dt.bln5),'0')AS bln5
			,IFNULL(SUM(dt.bln6),'0')AS bln6
			,IFNULL(SUM(dt.bln7),'0')AS bln7
			,IFNULL(SUM(dt.bln8),'0')AS bln8
			,IFNULL(SUM(dt.bln9),'0')AS bln9
			,IFNULL(SUM(dt.bln10),'0')AS bln10
			,IFNULL(SUM(dt.bln11),'0')AS bln11
			,IFNULL(SUM(dt.bln12),'0')AS bln12
		FROM (
			SELECT  MONTH(s.tgl)AS bln, m.kota_id, SUM(s.totalharga) AS omset
				,CASE WHEN MONTH(s.tgl)=1 THEN SUM(s.totalharga) END AS bln1
				,CASE WHEN MONTH(s.tgl)=2 THEN SUM(s.totalharga) END AS bln2
				,CASE WHEN MONTH(s.tgl)=3 THEN SUM(s.totalharga) END AS bln3
				,CASE WHEN MONTH(s.tgl)=4 THEN SUM(s.totalharga) END AS bln4
				,CASE WHEN MONTH(s.tgl)=5 THEN SUM(s.totalharga) END AS bln5
				,CASE WHEN MONTH(s.tgl)=6 THEN SUM(s.totalharga) END AS bln6
				,CASE WHEN MONTH(s.tgl)=7 THEN SUM(s.totalharga) END AS bln7
				,CASE WHEN MONTH(s.tgl)=8 THEN SUM(s.totalharga) END AS bln8
				,CASE WHEN MONTH(s.tgl)=9 THEN SUM(s.totalharga) END AS bln9
				,CASE WHEN MONTH(s.tgl)=10 THEN SUM(s.totalharga) END AS bln10
				,CASE WHEN MONTH(s.tgl)=11 THEN SUM(s.totalharga) END AS bln11
				,CASE WHEN MONTH(s.tgl)=12 THEN SUM(s.totalharga) END AS bln12
			FROM so s
			LEFT JOIN stockiest st ON s.stockiest_id=st.id
			LEFT JOIN member m ON s.member_id=m.id
			WHERE st.kota_id=0 AND s.tgl BETWEEN (LAST_DAY('$fromdate'-INTERVAL 6 MONTH) + INTERVAL 1 DAY) AND LAST_DAY('$fromdate')
			GROUP BY bln,m.kota_id
		)AS dt
		GROUP BY dt.kota_id
	)AS u ON k.id=u.kota_id
	GROUP BY k.id, st.oms
)AS dt
WHERE dt.total>0
ORDER BY oms, id
		";
		
		$qry = $this->db->query($query);
		
		// echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	/* End created by Boby 2012-08-06 */
	
	/* Created by Boby 2012-09-28 */
	public function omzet_rtr($fromdate,$todate)
    {
        $data = array();$i=0;
		$qry = $this->db->query("		
			SELECT pin.id, pin.tgl, s.no_stc, m.nama, pin.totalharga, pin.totalpv
			FROM retur_titipan pin
			LEFT JOIN member m ON pin.stockiest_id = m.id
			LEFT JOIN stockiest s ON pin.stockiest_id = s.id
			WHERE tgl BETWEEN '$fromdate' AND '$todate'
		");
		
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$i+=1;
				$row['i'] = $i;
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
	public function sum_omzet_rtr($fromdate,$todate){
        $data = array();
		$qry = $this->db->query("		
			SELECT SUM(pin.totalharga)AS totalhrg, SUM(pin.totalpv)AS totalpv
			FROM retur_titipan pin
			LEFT JOIN member m ON pin.stockiest_id = m.id
			LEFT JOIN stockiest s ON pin.stockiest_id = s.id
			WHERE tgl BETWEEN '$fromdate' AND '$todate'
		");
        if($qry->num_rows()>0){
            $data = $qry->row_array();
        }
        $qry->free_result();
        return $data;
    }
	/* End created by Boby 2012-09-28 */
}?>