<?php
class MFreeso extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Free Promo Sales Order for Stockiest
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-05-19
    |
    */
    
    public function searchFreeSO($keywords=0,$num,$offset){
        $data = array();
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND a.id LIKE '$keywords%'";
        }
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
            else $where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        
        $this->db->select("a.id,a.approveddate,a.approvedby,a.stockiest_id,s.no_stc,m.nama,a.remark,a.status",false);
        $this->db->from('promofree a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->where($where);
        $this->db->order_by('a.status','asc');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countFreeSO($keywords=0){
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND a.id LIKE '$keywords%'";
        }
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
            else $where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        $this->db->select("a.id",false);
        $this->db->from('promofree a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }

    public function getFreeSO($id=0){
        $data = array();
        $this->db->select("a.id,a.approveddate,a.approvedby,a.remark,a.status,a.approvedby,s.no_stc,m.nama,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('promofree a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->session->userdata('group_id')>100)$this->db->where('a.stockiest_id',$this->session->userdata('userid'));
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$this->db->where('a.warehouse_id',$whsid); 
        }
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getFreeSODetail($id=0){
        $data = array();
        $this->db->select("d.item_id,d.tgl,format(d.qty,0)as fqty,d.member_id,m.nama,a.name",false);
        $this->db->from('promofree_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->join('member m','d.member_id=m.id','left');
        $this->db->where('d.promofree_id',$id);
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function getFreeSOApproved($id=0){
        $data = array();
        $this->db->select("a.id,a.approveddate,a.approvedby,a.warehouse_id,a.remark,a.status,a.approvedby,s.no_stc,m.nama,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('promofree a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->session->userdata('group_id')>100)$this->db->where('a.stockiest_id',$this->session->userdata('userid'));
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$this->db->where('a.warehouse_id',$whsid); 
        }
        $this->db->where('a.status','pending');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    public function freeSOApproved(){
        $id = $this->input->post('id');
        $remarkapp=$this->db->escape_str($this->input->post('remark'));
        $empid = $this->session->userdata('user');
        $whsid = $this->input->post('whsid');
        $this->db->query("call sp_free_so('$id','$whsid','$remarkapp','$empid')");
    }
    
}?>