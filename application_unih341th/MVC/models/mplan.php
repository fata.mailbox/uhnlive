<?php
class MPlan extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function search_plan($keywords=0,$num,$offset){
        $data = array();
        $this->db->select('id,category,longdesc,sort,status,created,createdby',false)
            ->from('marketing_plan')
            ->like('category', $keywords, 'match')
            ->order_by('sort','asc')
            ->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function count_plan($keywords=0){
        $this->db->like('category', $keywords, 'match');
        $this->db->from('marketing_plan');
        return $this->db->count_all_results();
    }
    public function add_plan(){
        $data = array(
            'category' => $this->input->post('category'),
            'longdesc' => $_POST['longdesc'],
            'sort' => $this->input->post('sort'),
            'status' => $this->input->post('status'),
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $this->session->userdata('user')
        );
        $this->db->insert('marketing_plan',$data);
    }
    
    public function update_plan(){
        $data = array(
            'category' => $this->input->post('category'),
            'longdesc' => $_POST['longdesc'],
            'sort' => $this->input->post('sort'),
            'status' => $this->input->post('status'),
            'updated' => date('Y-m-d H:m:s',now()),
            'updatedby' => $this->session->userdata('user')
        );
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('marketing_plan',$data);
    }
    
    public function get_plan($id=0){
        $data = array();
        $option = array('id' => $id);
        $this->db->select("id,status,category,longdesc,created,createdby,sort",false);
        $q = $this->db->get_where('marketing_plan',$option,1);
        if($q->num_rows() > 0){
                $data = $q->row_array();
        }
        $q->free_result();
        //echo $this->db->last_query();
        return $data;
    }
    

}
?>