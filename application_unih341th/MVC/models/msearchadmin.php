<?php
class MSearchadmin extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /* Created by Boby 20120316 */
    public function search($keywords=0, $group='',$num,$offset){
        $data = array();
        $this->db->select("id,name",false);
        $this->db->from('item');
        if($group){
            $where = "group = '$group' and ( id like '$keywords%' or name like '$keywords%' )";
            $this->db->where($where);
        }
        else{
            $this->db->like('id', $keywords, 'after');
            $this->db->or_like('name', $keywords, 'after');     
        }
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countSearch($keywords=0,$group){
        $this->db->from('item');
        if($group){
            $where = "group = '$group' and ( id like '$keywords%' or name like '$keywords%' )";
            $this->db->where($where);
        }
        else{
            $this->db->like('id', $keywords, 'after');
            $this->db->or_like('name', $keywords, 'after');     
        }

        return $this->db->count_all_results();
    }
	
	public function countSearchTtpStock($keywords=0,$stcid){
        $data = array();
        //$where = "stockiest_id = '$stcid' and type_id > 1 and ( item_id like '$keywords%' or name like '$keywords%' )";
        $where = "admin_id = '$stcid' and ( item_id like '%$keywords%' or name like '%$keywords%' )";
        
        $this->db->select("p.id",false);   
        $this->db->from('pinjaman_admin_saldo p');
		$this->db->join('item i','p.item_id=i.id','left');
        $this->db->where($where);
		$this->db->order_by('name','asc');
        //$this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->num_rows();
        }
        $q->free_result();
        return $data;
    }
    
	public function searchTtpStock($keywords=0,$num,$offset,$stcid){
        $data = array();
        if($stcid){
            //$where = "stockiest_id = '$stcid' and type_id > 1 and ( item_id like '$keywords%' or name like '$keywords%' )";
            $where = "admin_id = '$stcid' and ( item_id like '%$keywords%' or name like '%$keywords%' )";
            
            $this->db->select("p.id,p.item_id,i.name,p.harga,p.pv,format(p.harga,0)as fharga,format(p.pv,0)as fpv,format(p.qty,0)as fqty",false);
            $this->db->from('pinjaman_admin_saldo p');
			$this->db->join('item i','p.item_id=i.id','left');
            $this->db->where($where);
            $this->db->order_by('name','asc');
            $this->db->limit($num,$offset);
            $q = $this->db->get();
            //echo $this->db->last_query();
            if($q->num_rows() > 0){
                foreach($q->result_array() as $row){
                    $data[] = $row;
                }
            }
            $q->free_result();
        }
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | search item inventory
    |--------------------------------------------------------------------------
    |
    | to popup for all user
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2008-12-27
    |
    */
    public function itemSearch($keywords=0, $group='',$num,$offset){
        $data = array();
        $this->db->select("id,name",false);
        $this->db->from('item');
        if($group == 'ass'){
            // $where = "manufaktur = 'Yes' and ( id like '$keywords%' or name like '$keywords%' )";
		// updated by Boby 2010-05-27
            $where = "manufaktur = 'Yes' and sales = 'Yes' and ( id like '$keywords%' or name like '$keywords%' )";
        }elseif($group == 'all'){
            $where = "id like '$keywords%' or name like '$keywords%' ";
        }else{
            $where = "manufaktur = 'No' and ( id like '$keywords%' or name like '$keywords%' )";
        }
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countItemSearch($keywords=0,$group){
        $this->db->select("id",false);
        $this->db->from('item');
        if($group == 'ass'){
            // $where = "manufaktur = 'Yes' and ( id like '$keywords%' or name like '$keywords%' )";
		// updated by Boby 2010-05-27
            $where = "manufaktur = 'Yes' and sales = 'Yes' and ( id like '$keywords%' or name like '$keywords%' )";
        }elseif($group == 'all'){
            $where = "id like '$keywords%' or name like '$keywords%' ";
        }else{
            $where = "manufaktur = 'No' and ( id like '$keywords%' or name like '$keywords%' )";
        }
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Search city & propinsi to profile
    |--------------------------------------------------------------------------
    |
    | @author taQwa
    | @author 2008-12-28
    |
    */
    public function searchCity($keywords=0,$group='',$num, $offset){
        $data = array();
        
        if($group == 'all'){
            $where = "k.name like '$keywords%' or p.name like '$keywords%' ";
        }
        $this->db->select("k.id,k.name as kota,p.name as propinsi",false);
        $this->db->from('kota k');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->where($where);
        $this->db->order_by('k.name','asc');
        $this->db->order_by('p.name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countSearchCity($keywords=0,$group=''){
        if($group == 'all'){
            $where = "k.name like '$keywords%' or p.name like '$keywords%' ";
        }
        $this->db->select("k.id",false);
        $this->db->from('kota k');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
     /*
    |--------------------------------------------------------------------------
    | search Member
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-10
    |
    */
    public function searchMember($keywords=0,$num,$offset){
        $data = array();
        if($keywords){
			// updated by Boby 20100616
            // $this->db->select("id,nama,ewallet,format(ewallet,0)as fewallet",false);
			$this->db->select("
				m.id
				, m.nama
				, ifnull(k.id,0) as kota_id
				, ifnull(k.name,0) as namakota
				, m.alamat
				, m.hp
				, m.telp
				, m.email
				, m.ewallet
				, format(m.ewallet,0)as fewallet",false);
			// end updated by Boby 20100616
			
            $this->db->from('member m');
			
			// updated by Boby 20100616
			$this->db->join ('kota k','m.kota_id = k.id','left');
			// end updated by Boby 20100616
            
			$this->db->like('m.id',$keywords,'between');
            $this->db->or_like('m.nama',$keywords,'between');
            $this->db->order_by('m.nama','asc');
            $this->db->limit($num,$offset);
            $q = $this->db->get();
            //echo $this->db->last_query();
            if($q->num_rows > 0){
                foreach($q->result_array() as $row){
                    $data[] = $row;
                }
            }
            $q->free_result();
        }
        return $data;
    }
    public function countMember($keywords=0){
        if($keywords){
		$this->db->select("id,nama,ewallet,format(ewallet,0)as fewallet",false);
            $this->db->from('member');
            $this->db->like('id',$keywords,'between');
            $this->db->or_like('nama',$keywords,'between');
            $this->db->order_by('nama','asc');
            return $this->db->count_all_results();
        }else return 0;    
    }
    
     /*
    |--------------------------------------------------------------------------
    | search item inventory for request titipan
    |--------------------------------------------------------------------------
    |
    | to popup for all user
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2008-12-27
    |
    */
    public function itemROSearch($keywords=0,$num,$offset){
        $data = array();
        $where = "a.sales = 'Yes' and ( a.id like '$keywords%' or a.name like '$keywords%' )";
        
        $this->db->select("a.id,a.name,a.price,a.pv,format(a.price,0)as fprice,format(a.pv,0)as fpv,b.name as type",false);
        $this->db->from('item a');
        $this->db->join('type b','a.type_id=b.id','left');
        
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countItemROSearch($keywords=0){
        $where = "a.sales = 'Yes' and ( a.id like '$keywords%' or a.name like '$keywords%' )";
        $this->db->select("a.id",false);
        $this->db->from('item a');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
    /*
    |--------------------------------------------------------------------------
    | search Stockiest
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-16
    |
    */
    public function searchStockiest($keywords=0,$stc,$num,$offset){
	  //$stc = 100;
        $data = array();
		// updated by Boby 20100617
        // $this->db->select("a.id,a.no_stc,m.nama,format(a.ewallet,0)as fewalletstc",false);
		$this->db->select("a.id,k.id as kota_id,k.name as namakota,a.no_stc,m.nama,format(a.ewallet,0)as fewalletstc",false);
		// end updated by Boby 20100617
        
		$this->db->from('stockiest a');
        $this->db->join('member m','a.id=m.id','left');
        
		// updated by Boby 20100617
		$this->db->join('kota k','a.kota_id=k.id','left');
		// end updated by Boby 20100617
		
        $where = "a.status = 'active' and (a.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%') ";
	  //$where .= " and a.id <> '0' ";
        if($stc == '0')$where = "a.id != '$stc' and ".$where;
        elseif($stc == 'mstc')$where = "a.type = 1 and ".$where; //untuk browse hanya stockiest tdk termasuk m-stc
        elseif($stc == 'romstc')$where = "a.type = 2 and ".$where; //untuk browse hanya m-stc
        elseif($stc == 'type')$where = "a.type = '$stc' and ".$where;
        
        $this->db->where($where);
        $this->db->order_by('a.no_stc','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        // echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countStockiest($keywords=0,$stc){
        
        $where = "a.status = 'active' and (a.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%') ";
	  //$where .= " and a.id <> '0' ";
        if($stc == '0')$where = "a.id != '$stc' and ".$where;
        elseif($stc == 'mstc')$where = "a.type = 1 and ".$where; //untuk browse hanya stockiest tdk termasuk m-stc
        elseif($stc == 'romstc')$where = "a.type = 2 and ".$where; //untuk browse hanya m-stc
        elseif($stc == 'type')$where = "a.type = '$stc' and ".$where;
        else $where =$where;
        
        $this->db->select("a.id");
        $this->db->from('stockiest a');
        $this->db->join('member m','a.id=m.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
    /*
    |--------------------------------------------------------------------------
    | search item Titipan stockiest
    |--------------------------------------------------------------------------
    |
    | to popup for all stockiest and admin
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-21
    |
    */
    public function itemSearchTitipan($keywords=0, $group='',$num,$offset){
        $data = array();
        
        $this->db->select("id,item_id,name,harga,fharga,fpv,fqty",false);
        $this->db->from('v_titipan v');
        if($group == 'ttp'){
            $where = "item_id like '$keywords%' or name like '$keywords%' ";
        }
        $this->db->where($where);
        $this->db->group_by('item_id');
        $this->db->group_by('harga');
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countItemSearchTitipan($keywords=0,$group){
        $data = array();
        $this->db->select("item_id as total",false);   
        $this->db->from('v_titipan');
        if($group == 'ttp'){
            $where = "item_id like '$keywords%' or name like '$keywords%' ";
        }
        $this->db->where($where);
        $this->db->group_by('item_id');
        $this->db->group_by('harga');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->num_rows();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | search item Titipan stockiest
    |--------------------------------------------------------------------------
    |
    | to popup for all stockiest and admin
    | @poweredby www.smartindo-technology.com
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-05-10
    |
    */
    public function searchTtp($keywords=0,$num,$offset){
        $data = array();
        $where = "member_id = '".$this->session->userdata('userid')."' and ( item_id like '$keywords%' or name like '$keywords%' )";
        
        $this->db->select("id,item_id,name,harga,pv,fharga,fpv,fqty",false);
        $this->db->from('v_titipan');
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countSearchTtp($keywords=0){
        $data = array();
        $where = "member_id = '".$this->session->userdata('userid')."' and ( item_id like '$keywords%' or name like '$keywords%' )";
        
        $this->db->select("id",false);   
        $this->db->from('v_titipan');
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->num_rows();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | search item inventory for request titipan
    |--------------------------------------------------------------------------
    |
    | to popup for all user
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-05-15
    |
    */
    public function itemSOSearch($keywords=0,$num,$offset){
        $data = array();
        $where = "a.sales = 'Yes' and a.type_id > 1 and ( a.id like '$keywords%' or a.name like '$keywords%' )";
        
        $this->db->select("a.id,a.name,a.price,a.pv,a.bv,format(a.price,0)as fprice,format(a.pv,0)as fpv,format(a.bv,0)as fbv,b.name as type",false);
        $this->db->from('item a');
        $this->db->join('type b','a.type_id=b.id','left');
        
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countItemSOSearch($keywords=0){
        $where = "a.sales = 'Yes' and a.type_id > 1 and ( a.id like '$keywords%' or a.name like '$keywords%' )";
        $this->db->select("a.id",false);
        $this->db->from('item a');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
    /*
    |--------------------------------------------------------------------------
    | search item Titipan stockiest for SO
    |--------------------------------------------------------------------------
    |
    | to popup for all stockiest
    | @poweredby www.smartindo-technology.com
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-05-18
    |
    */
    public function searchTtpSO($keywords=0,$num,$offset){
        $data = array();
        $where = "t.member_id = '".$this->session->userdata('userid')."' and i.type_id > 1 and ( t.item_id like '$keywords%' or i.name like '$keywords%' )";
        
        $this->db->select("t.id,t.item_id,i.name,t.harga,t.pv,i.bv,format(t.harga,0)as fharga,format(t.pv,0)as fpv,format(i.bv,0)as fbv,format(t.qty,0)as fqty",false);
        $this->db->from('titipan t');
	$this->db->join('item i','t.item_id=i.id','left');
        $this->db->where($where);
        $this->db->order_by('i.name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countSearchTtpSO($keywords=0){
        $data = array();
        $where = "t.member_id = '".$this->session->userdata('userid')."' and i.type_id > 1 and ( t.item_id like '$keywords%' or i.name like '$keywords%' )";
        
        $this->db->select("t.id",false);
        $this->db->from('titipan t');
	$this->db->join('item i','t.item_id=i.id','left');
        $this->db->where($where);
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->num_rows();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | search item Titipan stockiest for SO
    |--------------------------------------------------------------------------
    |
    | to popup for all stockiest
    | @poweredby www.smartindo-technology.com
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-05-18
    |
    */
    
    
    /*
    |--------------------------------------------------------------------------
    | search item Titipan stockiest
    |--------------------------------------------------------------------------
    |
    | to popup for all stockiest and admin
    | @poweredby www.smartindo-technology.com
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-05-10
    |
    */
    public function searchReturTtp($keywords=0,$num,$offset,$stcid){
        $data = array();
        $where = "member_id = '$stcid' and ( item_id like '$keywords%' or name like '$keywords%' )";
        
        $this->db->select("id,item_id,name,harga,pv,fharga,fpv,fqty",false);
        $this->db->from('v_titipan');
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countReturTtp($keywords=0,$stcid){
        $data = array();
        $where = "member_id = '$stcid' and ( item_id like '$keywords%' or name like '$keywords%' )";
        
        $this->db->select("id",false);   
        $this->db->from('v_titipan');
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->num_rows();
        }
        $q->free_result();
        return $data;
    }
    
    public function search_inv_bom($manufaktur,$keywords=0,$num,$offset){
        $data = array();
        $this->db->select("id,name,format(price,0)as fprice,price",false);
        $this->db->from('item');
        $where = "manufaktur = '$manufaktur' and ( id like '$keywords%' or name like '$keywords%' )";
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function count_inv_bom($manufaktur,$keywords=0){
        $this->db->from('item');
        $where = "manufaktur = '$manufaktur' and ( id like '$keywords%' or name like '$keywords%' )";
        $this->db->where($where);
        return $this->db->count_all_results();
    }
	
    public function countAdmin($keywords=0){
        $this->db->select("id");
        $this->db->from('admins');
		// $where = " /* status = 'active' AND */ (`username` LIKE '%$keywords%' OR `name` LIKE '%$keywords%') ";
		$where = "username LIKE '%$keywords%' OR name LIKE '%$keywords%' ";
        $this->db->where($where);
		// echo $this->db->last_query();
        return $this->db->count_all_results();
    }
	public function searchAdmin($keywords=0,$num,$offset){
        $data = array();
		// $this->db->select("a.id,k.id as kota_id,k.name as namakota,a.no_stc,m.nama,format(a.ewallet,0)as fewalletstc",false);
		
		$this->db->select("id, username, name");
        $this->db->from('admins');
		// $where = " /* status = 'active' AND */ (`username` LIKE '%$keywords%' OR `name` LIKE '%$keywords%') ";
		$where = "username LIKE '%$keywords%' OR name LIKE '%$keywords%' ";
        $this->db->where($where);
		
        // $this->db->order_by('a.no_stc','asc');
        $this->db->limit($num,$offset);
        
		$q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function itemSearchAdm($keywords=0,$num,$offset){
        $data = array();
        $where = "( a.id like '$keywords%' or a.name like '$keywords%' )";
        
        $this->db->select("a.id,a.name,a.price,a.pv,format(a.price,0)as fprice,format(a.pv,0)as fpv,b.name as type",false);
        $this->db->from('item a');
        $this->db->join('type b','a.type_id=b.id','left');
        
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function countItemSearchAdm($keywords=0){
        $where = "( a.id like '$keywords%' or a.name like '$keywords%' )";
        $this->db->select("a.id",false);
        $this->db->from('item a');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
	
	/* Created by Boby 20130624 */
	public function searchEmp($keywords=0,$num,$offset){
        $data = array();
		if(!$offset){$offset="";}else{$offset=",".$offset;}
		$query = "
			SELECT 
				e.id, e.emp_id, e.nama, m.nama as nama_member, m.id AS mid, e.updatedby
				, CASE WHEN e.flag = 1 THEN 'verified' ELSE 'not verified' END AS note
			FROM empshgrp e
			LEFT JOIN member m ON e.member_id=m.id
			WHERE e.emp_id LIKE '%$keywords%' OR e.nama LIKE '%$keywords%' OR m.nama LIKE '%$keywords%' OR m.id LIKE '%$keywords%'
			LIMIT $num $offset
		";
		$q = $this->db->query($query);
		
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function countEmp($keywords=0){
        $this->db->select("id");
        $this->db->from('empshgrp e');
        $this->db->join('member m','e.member_id = m.id', 'left');
		$where = "e.emp_id LIKE '%$keywords%' OR e.nama LIKE '%$keywords%' OR m.nama LIKE '%$keywords%' OR m.id LIKE '%$keywords%'";
        $this->db->where($where);
		// echo $this->db->last_query();
        return $this->db->count_all_results();
    }
	
	public function getEmp($keywords=0){
        $query = "
			SELECT 
				e.id, e.emp_id, e.nama, m.nama as nama_member, m.id AS mid, e.updatedby
			FROM empshgrp e
			LEFT JOIN member m ON e.member_id=m.id
			WHERE e.id = '$keywords'
		";
		//echo $query;
		$q = $this->db->query($query);
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function saveEmployee($empid, $empname, $member_id, $uid){
		if($this->_getAccount($member_id)){
			$query = "CALL sp_employee_save('$empid', '$empname', '$member_id', '$uid')";
			$q = $this->db->query($query);
			$this->session->set_flashdata('message','Register complete..');
		}else{
			$this->session->set_flashdata('message','Member id already registered!');
		}
	}
	
	protected function _getAccount($member_id){  
		$query = "
			SELECT 
				e.id, e.emp_id, e.nama, m.nama as nama_member, m.id AS mid, e.updatedby
			FROM empshgrp e
			LEFT JOIN member m ON e.member_id=m.id
			WHERE e.id = '$keywords'
		";
		$q = $this->db->query($query);
        if($q->num_rows() > 0){
            return false;
        }
        return true;
    }
	/* End created by Boby 20130624 */
}?>