<?php
class MSignup extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | signup member
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-25
    |
    */
    public function check_introducerid($introducerid){
        $i = $this->db->select("id")	
            ->from('member')
            ->where('id', $introducerid)
            ->get();
            //echo $this->db->last_query();
			
	return $var = ($i->num_rows() > 0) ? false : true;
    }
    public function check_captcha($confirmCaptcha)
   {
           $captchaWord = $this->session->userdata('captchaWord');
           
           $this->session->unset_userdata('captchaWord');
           if(strcasecmp($captchaWord, $confirmCaptcha) == 0)
           {
                return true;
           } return false;
   }
    public function check_placementid($placementid){
        $i = $this->db->select("id")	
            ->from('member')
            ->where('id', $placementid)
            ->get();
			
	return $var = ($i->num_rows() > 0) ? false : true;
    }
    
    public function check_crossline($p,$i){
        $data =array();
        $q = $this->db->query("SELECT f_check_crossline('$p','$i') as l_result");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        //print_r($data);
        $q->free_result();
        return $data['l_result'];
    }
    
    public function _check_userid($p,$p2){
        $array = array('member_id'=>$p,'code'=>$p2,'status'=>'not used');
        $q = $this->db->select("account")	
            ->from('activation_code')
            ->where($array)
            ->get();
	    return $var = ($q->num_rows() > 0) ? false : true;
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | Signup member
    |--------------------------------------------------------------------------
    |
    | untuk signup/register member online
    |
    | @author taQwa qtakwa@yahoo.com@yahoo.com
    | @author 2009-04-27
    |
    */
	
	/*Modified by Boby : 2010-01-21*/
    public function signup($introducerid,$userid,$activation,$placementid,
                           $name,$hp,$email,$question,$answer,
						   $ktp, $alamat, $ahliwaris){
            
        # Insert question and answer into the questions table
        $this->db->insert('questions', array('question' => $question, 'answer' => $answer)); 
        
        # Retrieve the question_id
        $question_id = $this->db->insert_id();
        
        # Generate dynamic salt
        $hash = sha1(microtime()); 
        
        # Hash password
		$password = substr($ktp,0,8);
		//$password = substr('1234567890',0,8);
		$pin = $userid;
        $password_enc = substr(sha1(sha1(md5($hash.$password))),5,15);
        $pin_enc = substr(sha1(sha1(md5($hash.$pin))),3,7);
        
        # Automatic Signup
        $row = $this->_getWarehouseID($placementid);
        $whsid = $row['warehouse_id'];
        $this->db->query("call sp_signup('$introducerid','$userid','$activation','$placementid','$name','$hp','$email','$whsid','$password_enc','$pin_enc','$hash','$question_id','system')");
        /*Modified by Boby 2010-01-21*/
		$this->db->query("UPDATE member SET noktp = '$ktp', alamat = '$alamat', ahliwaris = '$ahliwaris' WHERE id = '$userid'");
		/*End Modified by Boby 2010-01-21*/
		
		/* Modified by Boby 20130625 */
		if(substr($userid, -8,1)==1){
			$this->db->query("
				INSERT INTO `empshgrp`(`id`,`member_id`,`emp_id`,`nama`,`flag`,`updatedby`,`updated`,`createdby`,`created`)
				VALUES(NULL,$userid,'','','0','system',NOW(),'system',CURRENT_TIMESTAMP);
			");
		}
		/* End modified by Boby 20130625 */
		
        # Automatic login
        $this->login($userid, $password_enc);
        
        return 'REGISTRATION_SUCCESS';
    }
	/*End Modified by Boby : 2010-01-21*/
    protected function _getWarehouseID($memberid){
        $data = array();
        $q=$this->db->select("warehouse_id",false)
            ->from('users')
            ->where('id',$memberid)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        return $data;
    }       
        
    
    /*
    |--------------------------------------------------------------------------
    | login
    |--------------------------------------------------------------------------
    |
    | @param string $username Vaild username
    | @param string $password Password
    | @return mixed
    | @author taQwa
    | @author 2008-11-04
    |
    */
    public function login ($username, $password){
        # Grab hash, password, id, activation_code and banned_id from database.
        $result = $this->_login($username);
        if ($result)
        {
            if ($password === $result->password)
            {
                $this->db->set('last_login', 'NOW()', false);
                $this->db->where('id',$result->id);
                $this->db->update('users',$data=array());
                $this->session->set_userdata(array('logged_in'=>'true','userid'=>$result->id,'last_login'=>$result->last_login,'name' => $result->nama,'username'=> $result->username,'group_id'=> $result->group_id));
                return 'SUCCESS';
            }
        }
        return false;
   }
   
   protected function _login ($username)
   {
        $i = $this->db->select("u.password,u.username,u.last_login,m.nama,u.id,u.group_id")	
            ->from('users u')
        ->join('member m', 'u.id = m.id', 'left')
        ->where('u.id', $username)
        ->limit(1)
        ->get();
        return $var = ($i->num_rows() > 0) ? $i->row() : false;
   }
   
    public function getParentID($placementid){
        $data = array();
        $i = $this->db->select("id")	
            ->from('networks')
            ->where('member_id', $placementid)
            ->get();
	if($i->num_rows() > 0){
            $data = $i->row_array();
        }
        $i->free_result();
	return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | add Register Member
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-24
    |
    */
    public function add_register(){
		//$portal = $this->input->post('zip');
		//$info = $this->input->post('infofrom');
		$info_ = 0;
		$portal = 0;
        $data = array(
            'tgl'=>date('Y-m-d',now()),
            'fullname'=>$this->input->post('name'),
            'hp'=>$this->input->post('hp'),
            'email'=>$this->input->post('email'),
            'address' => $this->db->escape_str($this->input->post('address')),
            'zip'=>$portal,
            'delivery'=>'0',
            'infofrom'=>$info_
        );
        $this->db->insert('register',$data);
    }
    public function searchRegister($keywords=0,$num,$offset){
        $data = array();
        $where = "( a.id LIKE '$keywords%' or a.fullname LIKE '$keywords%' )";
        $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.fullname,a.hp,a.email,delivery,a.infofrom",false);
        $this->db->from('register a');
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countRegister($keywords=0){
        $where = "( a.id LIKE '$keywords%' or a.fullname LIKE '$keywords%' )";
        $this->db->from('register a');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
    public function getRegister($id){
        $data = array();
        $q = $this->db->get_where('register',array('id'=>$id));
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;        
    }
    public function register_online_del(){
        if($this->input->post('p_id')){
            $row = array();
            
            $idlist = implode(",",array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";
            
            $row = $this->_countDelete($where);
            if($row){
                
                $this->db->delete('register',$where);
                
                $this->session->set_flashdata('message','Delete register online successfully');
            }else{
                $this->session->set_flashdata('message','Nothing to delete register online!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing to delete register online!');
        }
    }
    private function _countDelete($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('register');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function updateNetwork($memberid,$parentid){
		$this->db->update('networks',array('parentid'=>$parentid),array('member_id'=>$memberid));
    }
    //'updated' => date('Y-m-d H:i:s',now()),
    //'updatedby' => $this->session->userdata('user')
	
	/* created by Boby 2011-01-13 */
	public function cekSKit($member_id,$actCode){
		/*
		"SELECT item_id FROM activation_code WHERE `status` = 'not used' AND member_id = '00001977' AND `code` = '225f524c86bb';"
		"SELECT IFNULL(qty,0)AS qty FROM titipan WHERE member_id = '00000009' AND item_id = '101';"
		*/
		
        $this->db->select("item_id, stockiest_id",false);
        $this->db->where("`status` = 'not used' AND member_id = '$member_id' AND `code` = '$actCode'");
        $q = $this->db->get('activation_code');
        if($q->num_rows() > 0){$row = $q->row_array();}
		$item_id = $row["item_id"];
		$stc = $row["stockiest_id"];
        $q->free_result();
		
		if($stc=='0'){
			$where_ = "item_id = '$item_id'";
			$debe = "stock";
		}else{
			$where_ = "member_id = '$stc' AND item_id = '$item_id'";
			$debe = "titipan";
		}
		$where_ .= "ORDER BY qty DESC LIMIT 0,1";
		$this->db->select("qty",false);
        $this->db->where($where_);
        $q = $this->db->get($debe);
        if($q->num_rows() > 0){
			$row1 = $q->row_array();
		}else{
			return true;
		}
		$q->free_result();
		
		if($row1["qty"]>0){return false;}else{
			return true;
			echo $this->db->last_query();
		}
    }
	/* end created by Boby 2011-01-13 */
}
?>