<?php
class MBanner extends CI_Model{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    public function searchBanner($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("id,description,title,a.file as filename,nourut,status,created,createdby,date_format(expdate,'%d/%m/%y')as fexpdate",false)
            ->from('banner a')
            ->like('description', $keywords, 'match')
            ->order_by('id','desc')
            ->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            $i = 0;
            foreach($q->result_array() as $row){
                $i++;
                $row['i'] = $i;
                $row['l'] = $i % 2;
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countBanner($keywords=0){
        $this->db->like('description', $keywords, 'match');
        $this->db->from('banner');
        return $this->db->count_all_results();
    }
    public function addBanner($filename=''){        
        $data = array(
            'expdate' => $this->input->post('fromdate'),
            'file' => $filename,
            'title' => $this->input->post('title'),
            'description' => $this->db->escape_str($this->input->post('shortdesc')),
            'nourut' => $this->input->post('nourut'),
            'url' => $this->input->post('url'),
            'status' => $this->input->post('status'),
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $this->session->userdata('username')
        );
        $this->db->insert('banner',$data);
    }
    
    public function updateBanner($filename = ''){
        if($filename){
            $data = array(
                'expdate' => $this->input->post('fromdate'),
                'file' => $filename,
                'title' => $this->input->post('title'),
                'url' => $this->input->post('url'),
                'description' => $this->db->escape_str($this->input->post('shortdesc')),
                'nourut' => $this->input->post('nourut'),
                'status' => $this->input->post('status'),
                'updated' => date('Y-m-d H:i:s',now()),
                'updatedby' => $this->session->userdata('username')
            );
        }else{
            $data = array(
                'expdate' => $this->input->post('fromdate'),
                'title' => $this->input->post('title'),
                'url' => $this->input->post('url'),
                'description' => $this->db->escape_str($this->input->post('shortdesc')),
                'nourut' => $this->input->post('nourut'),
                'status' => $this->input->post('status'),
                'updated' => date('Y-m-d H:i:s',now()),
                'updatedby' => $this->session->userdata('username')
            );
        }
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('banner',$data);
    }
    
    public function getBanner($id=0){
        $data = array();
        $option = array('id' => $id);
        $q = $this->db->get_where('banner',$option,1);
        if($q->num_rows() > 0){
            $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
    
    public function searchVideo($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("id,description,title,utama,nourut,status,created,createdby,date_format(expdate,'%d/%m/%y')as fexpdate",false)
            ->from('video a')
            ->like('description', $keywords, 'match')
            ->order_by('id','desc')
            ->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            $i=0;
            foreach($q->result_array() as $row){
                $i++;
                $row['i']=$i;
                $row['l']=$i % 2;
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countVideo($keywords=0){
        $this->db->like('description', $keywords, 'match');
        $this->db->from('video');
        return $this->db->count_all_results();
    }
    public function addVideo($filename=''){
        if($this->input->post('utama') == '1'){
            $this->db->update('video',array('utama'=>'0'),array('utama'=>'1'));
        }
        
        $data = array(
            'expdate' => $this->input->post('fromdate'),
            'title' => $this->input->post('title'),
            'file' => $filename,
            'description' => $this->db->escape_str($this->input->post('shortdesc')),
            'nourut' => $this->input->post('nourut'),
            'status' => $this->input->post('status'),
            'utama' => $this->input->post('utama'),
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $this->session->userdata('username')
        );
        $this->db->insert('video',$data);
    }
    
    public function updateVideo($filename=''){
            if($this->input->post('utama') == '1'){
                $this->db->update('video',array('utama'=>'0'),array('utama'=>'1'));
            }
            if($filename){
                $data = array(
                    'expdate' => $this->input->post('fromdate'),
                    'title' => $this->input->post('title'),
                    'file' => $filename,
                    'description' => $this->db->escape_str($this->input->post('shortdesc')),
                    'nourut' => $this->input->post('nourut'),
                    'status' => $this->input->post('status'),
                    'updated' => date('Y-m-d H:i:s',now()),
                    'updatedby' => $this->session->userdata('username')
                );
            }else{
                $data = array(
                'expdate' => $this->input->post('fromdate'),
                'title' => $this->input->post('title'),
                'description' => $this->db->escape_str($this->input->post('shortdesc')),
                'nourut' => $this->input->post('nourut'),
                'status' => $this->input->post('status'),
                'updated' => date('Y-m-d H:i:s',now()),
                'updatedby' => $this->session->userdata('username')
            );
            }
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('video',$data);
    }
    
    public function getVideo($id=0){
        $data = array();
        $option = array('id' => $id);
        $this->db->select("id,expdate,title,file,nourut,description,date_format(created,'%d/%m/%Y %H:%i:%s')as fcreated,createdby,status,utama,updatedby,date_format(updated,'%d/%m/%Y %H:%i:%s')as fupdated",false);
        $q = $this->db->get_where('video',$option,1);
        if($q->num_rows() > 0){
                $data = $q->row();
        }
        $q->free_result();
        //echo $this->db->last_query();
        return $data;
    }
}
?>