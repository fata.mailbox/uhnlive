<?php
class MNetwork extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function getNetwork(){
        $result = array();
        $member_id = $this->input->post('member_id');
        $result = $this->_getMember($member_id);
        if($result){
            $data = array();
            if($this->session->userdata('group_id') > 100){
                if($member_id == $this->session->userdata('userid'))
                {
                    $this->db->query("call sp_genealogy('$member_id','".$this->session->userdata('userid')."')");
                    $q = $this->db->select("v.nama,v.tglapp,v.ps,v.aps,v.title,v.ayah,v.anak,v.cucu,v.cicit,v.title",false)
                        ->FROM('v_genealogy v')
                        ->WHERE('v.ayah',$member_id)
                        ->get();
                    if($q->num_rows()>0){
                        foreach($q->result_array() as $row){
                            $data[] = $row;
                        }
                    }
                    $q->free_result();
                }
                else
                {
                    $err = $this->_check_genealogy($member_id,$this->session->userdata('userid'));
                    //echo $err." -> Error ";
                    if($err == 'ok')$this->db->query("call sp_genealogy('$member_id','".$this->session->userdata('userid')."')");
                    else $member_id='taQwa';

                    if($result['jenjang_id'] >= 6)
                    {
                        $q = $this->db->select("v.nama,v.tglapp,v.ps,v.aps,v.title,v.ayah,v.anak,v.cucu,v.cicit,v.title",false)
                            ->FROM('v_genealogy v')
                            ->WHERE('v.cucu',$member_id)
                            ->limit(0,1)
                            ->get();
                        if($q->num_rows()>0){
                            foreach($q->result_array() as $row){
                                $data[] = $row;
                            }
                        }
                        $q->free_result();
                        //echo $sql." -> PM";
                    }
                    else
                    {
                        $q = $this->db->select("v.nama,v.tglapp,v.ps,v.aps,v.title,v.ayah,v.anak,v.cucu,v.cicit,v.title",false)
                            ->FROM('v_genealogy v')
                            ->WHERE('v.ayah',$member_id)
                            ->get();
                        if($q->num_rows()>0){
                            foreach($q->result_array() as $row){
                                $data[] = $row;
                            }
                        }
                        $q->free_result();
                        //echo $sql." -> TEST";
                    }
                }
            }else{
                $this->db->query("call sp_genealogy('$member_id','".$this->session->userdata('userid')."')");
                $q = $this->db->select("v.nama,v.tglapp,v.ps,v.aps,v.title,v.ayah,v.anak,v.cucu,v.cicit,v.title",false)
                    ->FROM('v_genealogy v')
                    ->WHERE('v.ayah',$member_id)
                    ->get();
                echo $this->db->last_query();
                if($q->num_rows()>0){
                    foreach($q->result_array() as $row){
                        $data[] = $row;
                    }
                }
                $q->free_result();
            }
            return $data;
        }return false;
    }
    protected function _getMember($meber_id){
        $q=$this->db->select("id,jenjang_id",false)
            ->from('member')
            ->where('id',$meber_id)
            ->get();
        return $var = ($q->num_rows()>0) ? $q->row() : false;
    }
    protected function _check_genealogy($member_id,$empid)
    {
        $data = array();
        $this->db->query("SELECT f_check_genealogy('$member_id','$empid') as l_result");
        $q=$this->db->get();
        if($q->num_rows()>0){
            $data = $q->row();
            $data = $data->l_result;
        }
        $q->free_result();
        return $data;
    }
      
}
?>