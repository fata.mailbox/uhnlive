<?php
/* Created by Boby 20130607 */
class MTopping extends CI_Model{
    function __construct()
    { parent::__construct(); }

    public function getDropDrownPeriode(){
        $data = array();
        $this->db->select("periode,tgl",false);
        $this->db->from('v_bonus');
        if($this->session->userdata('group_id')>100){
            $this->db->where('display','1');
            $this->db->where('member_id',$this->session->userdata('userid'));
        }
        $this->db->group_by('tgl');
        $this->db->order_by('periode','desc');
        $q=$this->db->get();
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $data[$row['periode']] = $row['tgl'];
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function get_year(){
        $data = array();
		for($i=date("Y");$i>=2009;$i--){
			$data[$i]=$i;
		}
        return $data;
    }
	
	public function get_month(){
        $data = array();
		for($i=1;$i<=12;$i++){
			switch ($i) {
				case 1: $data[$i]="January"; break;
				case 2: $data[$i]="February"; break;
				case 3: $data[$i]="March"; break;
				case 4: $data[$i]="April"; break;
				case 5: $data[$i]="May"; break;
				case 6: $data[$i]="June"; break;
				case 7: $data[$i]="July"; break;
				case 8: $data[$i]="August"; break;
				case 9: $data[$i]="September"; break;
				case 10: $data[$i]="October"; break;
				case 11: $data[$i]="November"; break;
				case 12: $data[$i]="December"; break;
			}
		}
        return $data;
    }
	
	public function get_month_name($periode){
        $data=array();
		$qry = "
			SELECT MONTHNAME('$periode')AS blnskrg, YEAR('$periode')AS thnskrg
			, MONTHNAME('$periode' - INTERVAL 1 MONTH)AS blnmin1, YEAR('$periode'- INTERVAL 1 MONTH)AS thnmin1
			, MONTHNAME('$periode' - INTERVAL 2 MONTH)AS blnmin2, YEAR('$periode'- INTERVAL 2 MONTH)AS thnmin2
			, MONTHNAME('$periode' - INTERVAL 3 MONTH)AS blnmin3, YEAR('$periode'- INTERVAL 3 MONTH)AS thnmin3
			, MONTHNAME('$periode' - INTERVAL 4 MONTH)AS blnmin4, YEAR('$periode'- INTERVAL 4 MONTH)AS thnmin4
			, MONTHNAME('$periode' - INTERVAL 5 MONTH)AS blnmin5, YEAR('$periode'- INTERVAL 5 MONTH)AS thnmin5
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	
}
?>