<?php
class MTemplate extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
	public function getDataPribadi($id){
		$data = array();
		$qry = $this->db->query("
			SELECT dt.member_id, nj.name as new_jenjang, j.id, j.name AS jenjang
				, IFNULL(SUM(pv1),0)AS pv1, IFNULL(SUM(pv2),0)AS pv2, IFNULL(SUM(pv3),0)AS pv3
				, IFNULL(SUM(pv4),0)AS pv4, IFNULL(SUM(pv5),0)AS pv5, IFNULL(SUM(pv6),0)AS pv6
				, zt.q1, zt.q2, zt.q3, zt.q4, zt.q5, zt.q6
			FROM(
				SELECT *
					, CASE WHEN bln = 11 THEN pv END AS pv1, CASE WHEN bln = 12 THEN pv END AS pv2, CASE WHEN bln = 1 THEN pv END AS pv3
					, CASE WHEN bln = 2 THEN pv END AS pv4, CASE WHEN bln = 3 THEN pv END AS pv5, CASE WHEN bln = 4 THEN pv END AS pv6
				FROM(
					SELECT member_id, MONTH(so.tgl)AS bln, SUM(totalpv)AS pv
					FROM so
					WHERE member_id = '$id'
					AND so.tgl BETWEEN '2012-11-01' AND '2013-04-30'
					GROUP BY member_id, bln
				)AS dt
			)AS dt
			LEFT JOIN z_201211_trip zt ON dt.member_id = zt.member_id
			LEFT JOIN member m ON dt.member_id = m.id
			LEFT JOIN jenjang j ON m.jenjang_id = j.id
			LEFT JOIN jenjang nj ON zt.new_jenjang = nj.id
			WHERE m.id = '$id'
			GROUP BY member_id
		");
		//echo $this->db->last_query();
		if($qry->num_rows()>0){$data=$qry->row_array();}
		$qry->free_result();
		return $data;
	}
	public function getDetail($id){
		$data = array();
		$qry = $this->db->query("
			SELECT m.id, m.nama, z.ps1, z.pvg1, z.ps2, z.pvg2, z.ps3, z.pvg3, z.ps4, z.pvg4, z.ps5, z.pvg5, z.ps6, z.pvg6
				, (z.pvg1+z.pvg2+z.pvg3+z.pvg4+z.pvg5+z.pvg6)as PVG
			FROM z_201211_trip z
			LEFT JOIN member m ON z.member_id = m.id
			WHERE z.sponsor_id = '$id'
			ORDER by PVG DESC
		");
		//echo $this->db->last_query();
		$i=0;
		if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            	$i++;
                $row['i'] = $i;
                $data[]=$row;
				
			}
        }
		$qry->free_result();
		return $data;
	}
}?>