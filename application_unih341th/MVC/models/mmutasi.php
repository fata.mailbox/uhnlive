<?php
class MMutasi extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Mutasi Stcock antar Warehouse
    |--------------------------------------------------------------------------
    |
    | to sign warehouse
    |
    | @created 2009-04-03
    | @author qtakwa@yahoo.com@yahoo.com
    |
    */
    public function searchMutasi($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.status,a.createdby,b.name as fromwhs,c.name as towhs",false);
        $this->db->from('mutasi a');
        $this->db->join('warehouse b','a.warehouse_id1=b.id','left');
        $this->db->join('warehouse c','a.warehouse_id2=c.id','left');
        if($this->session->userdata('group_id') == 9){
            $whsid = $this->session->userdata('whsid');
            $where = "`a`.`warehouse_id1` = '$whsid' and ( `a`.`id` like '$keywords%' )";
            $this->db->where($where);
        }else{
            $this->db->like('a.id',$keywords,'after');
        }
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countMutasi($keywords=0){
        $this->db->from('mutasi a');
        if($this->session->userdata('group_id') == 9){
            $whsid = $this->session->userdata('whsid');
            $where = "`a`.`warehouse_id1` = '$whsid' and ( `a`.`id` like '$keywords%' )";
            $this->db->where($where);
        }else{
            $this->db->like('a.id',$keywords,'after');
        }
        return $this->db->count_all_results();
    }
    public function addMutasi(){
        $warehouse_id1=$this->input->post('warehouse_id1');
        $warehouse_id2=$this->input->post('warehouse_id2');
        $empid = $this->session->userdata('user');
        $data = array(
            'date' => date('Y-m-d',now()),
            'warehouse_id1' => $warehouse_id1,
            'warehouse_id2' => $warehouse_id2,
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'event_id' => 'MT1',
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $empid
            );
       $this->db->insert('mutasi',$data);
       
       $id = $this->db->insert_id();
         
        $qty0 = str_replace(".","",$this->input->post('qty0'));
        if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0
            );
            
            $this->db->insert('mutasi_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1
            );
            
            $this->db->insert('mutasi_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2
            );
            
            $this->db->insert('mutasi_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3
            );
            
            $this->db->insert('mutasi_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4
            );
            
            $this->db->insert('mutasi_d',$data);
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5
            );
            
            $this->db->insert('mutasi_d',$data);
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6
            );
            
            $this->db->insert('mutasi_d',$data);
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7
            );
            
            $this->db->insert('mutasi_d',$data);
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8
            );
            
            $this->db->insert('mutasi_d',$data);
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
       if($this->input->post('itemcode9') and $qty9 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9
            );
            
            $this->db->insert('mutasi_d',$data);
       }
       $this->db->query("call sp_mutasi_stock('$id','$warehouse_id1','$warehouse_id2','N','$empid')");        
    }
    public function getDropDownWhs(){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function getMutasi($id){
        $data=array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.remarkapp,a.status,a.createdby,b.name as fromwhs,c.name as towhs,a.approved,a.approvedby",false);
        $this->db->from('mutasi a');
        $this->db->join('warehouse b','a.warehouse_id1=b.id','left');
        $this->db->join('warehouse c','a.warehouse_id2=c.id','left');
        if($this->session->userdata('group_id') == 9){
            $whsid = $this->session->userdata('whsid');
            $where = "`a`.`warehouse_id1` = '$whsid' and `a`.`id` = '$id'";
            $this->db->where($where);
        }else{
            $this->db->where('a.id',$id);
        }
        $q=$this->db->get();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function getMutasiDetail($id=0){
        $data = array();
        $q=$this->db->select("d.item_id,format(d.qty,0)as fqty,a.name",false)
            ->from('mutasi_d d')
            ->join('item a','d.item_id=a.id','left')
            ->where('d.mutasi_id',$id)
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Mutasi Stock antar Warehouse Approvede
    |--------------------------------------------------------------------------
    |
    | @created 2009-04-03
    | @author qtakwa@yahoo.com@yahoo.com
    |
    */
    public function searchMutasiApp($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.status,a.createdby,b.name as fromwhs,c.name as towhs",false);
        $this->db->from('mutasi a');
        $this->db->join('warehouse b','a.warehouse_id1=b.id','left');
        $this->db->join('warehouse c','a.warehouse_id2=c.id','left');
        if($this->session->userdata('group_id') == 9){
            $whsid = $this->session->userdata('whsid');
            $where = "`a`.`warehouse_id2` = '$whsid' and ( `a`.`id` like '$keywords%' )";
            $this->db->where($where);
        }else{
            $this->db->like('a.id',$keywords,'after');
        }
        $this->db->order_by('a.status','asc');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countMutasiApp($keywords=0){
        $this->db->from('mutasi a');
        if($this->session->userdata('group_id') == 9){
            $whsid = $this->session->userdata('whsid');
            $where = "`a`.`warehouse_id2` = '$whsid' and ( `a`.`id` like '$keywords%' )";
            $this->db->where($where);
        }else{
            $this->db->like('a.id',$keywords,'after');
        }
        return $this->db->count_all_results();
    }
    public function appMutasi(){
        $id=$this->input->post('id');
        $warehouse_id2=$this->input->post('warehouse_id2');
        $empid = $this->session->userdata('user');
        $data = array(
            'approved' => date('Y-m-d',now()),
            'remarkapp' => $this->db->escape_str($this->input->post('remark')),
            'status' => 'Y',
            'approved' => date('Y-m-d H:m:s',now()),
            'approvedby' => $empid
            );
       $this->db->update('mutasi',$data,array('id'=>$id));
              
       $this->db->query("call sp_mutasi_stock('$id','$warehouse_id2','Y','$empid')");        
    }
        
    public function getMutasiApp($id){
        $data=array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.remarkapp,a.status,a.createdby,b.name as fromwhs,c.name as towhs,a.approved,a.approvedby",false);
        $this->db->from('mutasi a');
        $this->db->join('warehouse b','a.warehouse_id1=b.id','left');
        $this->db->join('warehouse c','a.warehouse_id2=c.id','left');
        if($this->session->userdata('group_id') == 9){
            $whsid = $this->session->userdata('whsid');
            $where = "`a`.`warehouse_id2` = '$whsid' and `a`.`id` = '$id'";
            $this->db->where($where);
        }else{
            $this->db->where('a.id',$id);
        }
        $q=$this->db->get();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function getMutasiApp2($id){
        $data=array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.remarkapp,a.warehouse_id2,a.status,a.createdby,b.name as fromwhs,c.name as towhs,a.approved,a.approvedby",false);
        $this->db->from('mutasi a');
        $this->db->join('warehouse b','a.warehouse_id1=b.id','left');
        $this->db->join('warehouse c','a.warehouse_id2=c.id','left');
        if($this->session->userdata('group_id') == 9){
            $whsid = $this->session->userdata('whsid');
            $where = "`a`.`warehouse_id2` = '$whsid' and `a`.`id` = '$id' and `a`.`status` = 'N'";
            $this->db->where($where);
        }else{
            $this->db->where('a.id',$id);
            $this->db->where('a.status','N');
        }
        $q=$this->db->get();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function check_approved($id){
        $q=$this->db->select("id",false)
            ->from('mutasi')
            ->where('id',$id)
            ->where('status','Y')
            ->get();
        return ($q->num_rows() > 0)? true : false;        
    }
}?>