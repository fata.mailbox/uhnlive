<?php
class MBank extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Master Warehouse
    |--------------------------------------------------------------------------
    |
    | to sign warehouse
    |
    | @created 2009-03-29
    | @author qtakwa@yahoo.com@yahoo.com
    |
    */
    public function searchBank($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.name,a.accountno,a.accountname,a.accountarea,a.status",false);
        $this->db->from('bank a');
        $this->db->like('a.id',$keywords,'after');
        $this->db->or_like('a.name',$keywords,'after');
        $this->db->order_by('a.id','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countBank($keywords=0){
        $this->db->like('a.id',$keywords,'after');
        $this->db->or_like('a.name',$keywords,'after');
        $this->db->from('bank a');
        return $this->db->count_all_results();
    }
    public function addBank(){
        $data=array(
            'id' => $this->input->post('id'),
            'name' => $this->input->post('name'),
            'accountno' => $this->input->post('accountno'),
            'accountname' => $this->input->post('accountname'),
            'accountarea' => $this->input->post('accountarea'),
            'status' => $this->input->post('status'),
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $this->session->userdata('user')
        );
            
        $this->db->insert('bank',$data);
    }
    public function check_bankid($id){
        $q=$this->db->select("id",false)
            ->from('bank')
            ->where('id',$id)
            ->get();
        return ($q->num_rows() > 0)? true : false;    
    }   
    public function getBank($id){
        $data=array();
        $q = $this->db->get_where('bank',array('id'=>$id));
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function editBank(){
        $data=array(
            'name' => $this->input->post('name'),
            'accountno' => $this->input->post('accountno'),
            'accountname' => $this->input->post('accountname'),
            'accountarea' => $this->input->post('accountarea'),
            'status' => $this->input->post('status'),
            'updated' => date('Y-m-d H:i:s',now()),
            'updatedby' => $this->session->userdata('user')
        );
        $this->db->update('bank',$data,array('id'=>$this->input->post('id')));
    }
    
}?>