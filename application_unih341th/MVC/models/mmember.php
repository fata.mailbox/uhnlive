<?php
class MMember extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    public function createSTCID($huruf){
        $angka_master = '01234567890123456789012345678901234567890123456789';
        $x = 0;
        while ($x < 1) {
            $angka = '';
            for ($i=0; $i<4; $i++) {
                $angka .= $angka_master{rand(0,49)};
            }
            $userid = $huruf.$angka;
            
            if(!$this->_check_memberid($userid)){
                $x++;
            }
        }
        return $userid;
    }
    //update dc set  where id=in_memberid;
    protected function _check_memberid($memberid){
        $i = $this->db->select("id")	
            ->from('member')
            ->where('id', $memberid)
            ->get();
            //echo $this->db->last_query();
        if($i->num_rows() >0){
            $i->free_results();    
            return true;
        }
        return false;        
    }
    /*
    |--------------------------------------------------------------------------
    | Show list bank for profile
    |--------------------------------------------------------------------------
    |
    | @author taQwa
    | @author 2008-12-28
    |
    */
    public function getDropDownBank(){
        $data = array();
        $this->db->order_by('sort','asc');
        $q = $this->db->get('bank');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    
    
    
    /**
    * check stockist code exsis
    *
    * @access public
    * @param old password from input
    * @return boolean
   **/
    public function _check_stccode($code){
        $q = $this->db->get_where('users',array('id'=>$code));
        if($q->num_rows()>0){
            return true;
        }
        return false;
    }
    public function _check_sponsorid($code){
        $q = $this->db->get_where('users',array('id'=>$code));
        if($q->num_rows()>0){
            return false;
        }
        return true;
    }
    
    /*
    |--------------------------------------------------------------------------
    | add new member / stockist
    |--------------------------------------------------------------------------
    |
    | @author taQwa
    | @author 2008-12-28
    |
    */
    public function addMember($memberid,$levelid,$groupid,$sponsorid){
        # Generate dynamic salt
        $hash = sha1(microtime());
        # Hash password
        $password = $this->input->post('newpassword');
        $password_enc = substr(sha1(sha1(md5($hash.$password))),7,17);
        
        $data=array(
            'id' => $memberid,
            'username' => $memberid,
            'group_id' => $groupid,
            'password' => $password_enc,
            'hash' => $hash
        );
        $this->db->insert('users',$data);
        
        $data=array(
            'id' => $memberid,
            'name' => $this->db->escape_str($this->input->post('name')),
            'level_id' => $levelid,
            'sponsor_id' => $sponsorid,
            'gender' => $this->input->post('gender'),
            'birthday' => $this->input->post('date'),
            'address' => $this->db->escape_str($this->input->post('address')),
            'kota_id' => $this->input->post('kota_id'),
            'hp' => $this->input->post('hp'),
            'officetelp' => $this->input->post('officetelp'),
            'hometelp' => $this->input->post('hometelp'),
            'fax' => $this->input->post('fax'),
            'postcode' => $this->input->post('postcode'),
            'idnumber' => $this->input->post('idnumber'),
            'email' => $this->input->post('email'),
            'bank_id' => $this->input->post('bank_id'),
            'norek' => $this->input->post('norek'),
            'an' => $this->db->escape_str($this->input->post('name')),
            'cabang' => $this->input->post('cabang'),
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $this->session->userdata('user')
        );
        $this->db->insert('member',$data);
    }
    
    /*
    |--------------------------------------------------------------------------
    | edit member / stockist
    |--------------------------------------------------------------------------
    |
    | @author taQwa
    | @author 2008-12-29
    |
    */
    public function editMember(){
        $idmember = $this->input->post('id');
        $data=array(
            'gender' => $this->input->post('gender'),
            'birthday' => $this->input->post('date'),
            'address' => $this->db->escape_str($this->input->post('address')),
            'kota_id' => $this->input->post('kota_id'),
            'hp' => $this->input->post('hp'),
            'officetelp' => $this->input->post('officetelp'),
            'hometelp' => $this->input->post('hometelp'),
            'fax' => $this->input->post('fax'),
            'postcode' => $this->input->post('postcode'),
            'idnumber' => $this->input->post('idnumber'),
            'email' => $this->input->post('email'),
            'bank_id' => $this->input->post('bank_id'),
            'norek' => $this->input->post('norek'),
            'cabang' => $this->input->post('cabang'),
            'updated' => date('Y-m-d H:m:s',now()),
            'updatedby' => $this->session->userdata('user')
        );
        $this->db->update('member',$data,array('id'=>$idmember));
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | list stockist new
    |--------------------------------------------------------------------------
    |
    | to list stockist in sn
    |
    | @param void
    | @return void
    | @author takwa
    | @created 2008-12-28
    |
    */
    public function searchMember($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.name,k.name as kota,p.name as propinsi,date_format(a.created,'%d-%b-%Y')as created,a.sponsor_id,a.level_id,a.createdby",false);
        $this->db->from('member a');
        $this->db->join('kota k','a.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->like('a.id', $keywords, 'after');
        $this->db->or_like('a.name', $keywords, 'after');
        $this->db->or_like('k.name', $keywords, 'after');
        $this->db->order_by('a.created','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countMember($keywords=0){
        $this->db->like('id', $keywords, 'after');
        $this->db->or_like('name', $keywords, 'after');
        $this->db->from('member');
        //$this->db->get();
        
        return $this->db->count_all_results();
    }
    
    public function getMember($id=0){
        $data=array();
        $q=$this->db->select("m.*,s.name as sponsor_name,k.name as kota, p.name as propinsi,b.name as bank_name",false)
            ->from('member m')
            ->join('member s','m.sponsor_id=s.id','left')
            ->join('kota k','m.kota_id=k.id','left')
            ->join('propinsi p','k.propinsi_id=p.id','left')
            ->join('bank b','m.bank_id=b.id','left')
            ->where('m.id',$id)
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    /*
    |--------------------------------------------------------------------------
    | brouse member
    |--------------------------------------------------------------------------
    |
    | @author takwa
    | @created 2008-12-29
    |
    */
    public function browseMember($keywords=0,$num,$offset,$level){
        $data = array();
        $memberid=$this->session->userdata('userid');
        
        if($memberid == 'SC0000' or $memberid == 'DC0000'){
            $where = "`a.level_id` = '$level' and (a.name like '$keywords%' or a.id LIKE '$keywords%')";
        }else{
            if($level !='' && $this->session->userdata('group_id') > 100)$where = "`a.level_id` = '$level' and a.sponsor_id='$memberid' and (a.name like '$keywords%' or a.id LIKE '$keywords%')";
            else {
                if($level)$where = "`a.level_id` = '$level' and (a.id like '$keywords%' or a.name LIKE '$keywords%')";
                else $where = "a.id like '$keywords%' or a.name LIKE '$keywords%'";
            }
        }
        
        $this->db->select("a.id,a.name,a.sponsor_id,b.name as sponsor_name,k.name as kota,p.name as propinsi",false);
        $this->db->from('member a');
        $this->db->join('member b','a.sponsor_id=b.id','left');
        $this->db->join('kota k','a.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->where($where);
        $this->db->order_by('a.name','asc');
        $this->db->limit($num,$offset);
        
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countBrowseMember($keywords=0,$level){
        
        /* if($this->session->userdata('group_id')==101){
                $memberid=$this->session->userdata('userid');
                $where = "`level_id` = '$level' and a.sponsor_id='$memberid' and (a.name like '$keywords%' or a.id LIKE '$keywords%')";
        }else{
            if($level)$where = "`level_id` = '$level' and (a.id like '$keywords%' or a.name LIKE '$keywords%')";
            else $where = "a.id like '$keywords%' or a.name LIKE '$keywords%'";
        }*/
        $memberid=$this->session->userdata('userid');
        
        if($memberid == 'SC0000' or $memberid == 'DC0000'){
            $where = "`level_id` = '$level' and (a.name like '$keywords%' or a.id LIKE '$keywords%')";
        }else{
            if($level !='' && $this->session->userdata('group_id') > 100)$where = "`a.level_id` = '$level' and a.sponsor_id='$memberid' and (a.name like '$keywords%' or a.id LIKE '$keywords%')";
            else {
                if($level)$where = "`a.level_id` = '$level' and (a.id like '$keywords%' or a.name LIKE '$keywords%')";
                else $where = "a.id like '$keywords%' or a.name LIKE '$keywords%'";
            }
        }
        
        $this->db->from('member a');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
    public function addResetPassword(){
        # Generate dynamic salt
        $hash = sha1(microtime());
        
        $memberid = $this->input->post('member_id');
        $password = $this->input->post('newpassword');
        $password_enc = substr(sha1(sha1(md5($hash.$password))),7,17);
        $data = array(
            'password'=>$password_enc,
            'hash'  => $hash
        );
        $this->db->update('users',$data,array('id'=>$memberid));
        
        $data=array(
            'member_id'=>$memberid,
            'password'=>$password,
            'created'=>date('Y-m-d H:m:s',now()),
            'createdby'=>$this->session->userdata('user')
        );
        $this->db->insert('reset_password',$data);
    }
    
    /*
    |--------------------------------------------------------------------------
    | brouse member
    |--------------------------------------------------------------------------
    |
    | @author takwa
    | @created 2008-12-29
    |
    */
    public function searchResetPassword($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("r.member_id,r.password,date_format(r.created,'%d-%b-%Y')as created,r.createdby,m.name",false);
        $this->db->from('reset_password r');
        $this->db->join('member m','r.member_id=m.id','left');
        $this->db->like('r.member_id', $keywords, 'after');
        $this->db->or_like('m.name', $keywords, 'after');
        $this->db->order_by('r.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countResetPassword($keywords=0){
        $this->db->like('r.member_id', $keywords, 'after');
        $this->db->or_like('m.name', $keywords, 'after');
        
        $this->db->from('reset_password r');
        $this->db->join('member m','r.member_id=m.id','left');
        return $this->db->count_all_results();
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | login
    |--------------------------------------------------------------------------
    |
    | @param string $username Vaild username
    | @param string $password Password
    | @return mixed
    | @author taQwa
    | @author 2008-11-04
    |
    */
    public function login ($username, $password){
        # Grab hash, password, id, activation_code and banned_id from database.
        $result = $this->_login($username);
        //print_r($result);
        if ($result)
        {
            if(!empty($result->banned_id))
            {
                $this->logout();
                return 'BANNED';
            }
            else
            {
                $password = substr(sha1(sha1(md5($result->hash.$password))),7,17);

                if ($password === $result->password)
                {
                    $this->db->set('last_login', 'NOW()', false);
                    $this->db->where('id',$result->id);
                    $this->db->update('users',$data=array());
                    $this->session->set_userdata(array('logged_in'=>'true','userid'=> $result->id,'last_login'=>$result->last_login,'name' => $result->name,'username' => $result->username,'group_id'=> $result->group_id,'level_id'=> $result->level_id));
                    return 'SUCCESS';
                }
            }
        }
        return false;
   }
   
   /**
    * reason banned
    *
    * @access public
    * @param string $username Vaild username
    * @return mixed
   **/
   
   public function banned ($username)
   {
        return $result = $this->_login($username);
   }
    
    /**
    * _login
    *
    * @access protected
    * @param string $users Users table
    * @param string $banned Banned table
    * @param string $username Valid username
    * @return mixed
   **/
   protected function _login ($username)
   {
        $i = $this->db->select("u.password,u.username,u.last_login,m.name,m.level_id,u.hash,u.id,u.group_id,u.banned_id,b.reason")	
            ->from('users u')
        ->join('banned b', 'u.banned_id = b.id', 'left')
        ->join('member m', 'u.id = m.id', 'left')
        ->where('u.username', $username)
        ->limit(1)
        ->get();
        return $var = ($i->num_rows() > 0) ? $i->row() : false;
   }
   
   /**
    * check old password for member
    *
    * @access public
    * @param username get from session userid
    * @param old password from input
    * @return boolean
   **/
    public function check_passwordmember($username,$password){
        $result = $this->_getPasswordmember($username);
        if($result){
            $password = substr(sha1(sha1(md5($result->hash.$password))),7,17);
            if ($password === $result->password){
                return true;
            }    
        }
        return false;
    }
    /**
    * _get old password
    *
    * @access protected
    * @param username
    * @return old password
    * @author taQwa
    * @created 2008-11-29
   **/
    protected function _getPasswordmember($user){
        $i=$this->db->select("password,hash",false)
            ->from('users')
            ->where('username',$user)
            ->get();
        return $var = ($i->num_rows() >0) ? $i->row() : false;
    }
    
    /*
    |--------------------------------------------------------------------------
    | list stockist RC for frontend
    |--------------------------------------------------------------------------
    |
    | @param void
    | @return void
    | @author takwa
    | @created 2009-02-11
    |
    */
    public function browseRC(){
        $data = array();
        $this->db->select("a.id,a.name,a.address,a.hp,a.hometelp,k.name as kota,p.name as propinsi",false);
        $this->db->from('member a');
        $this->db->join('kota k','a.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->input->post('kota_id')){
            $this->db->where('a.kota_id', $this->input->post('kota_id'));
            $this->db->where('a.level_id', 'RC');
            $this->db->where('a.id !=', 'RC0000');
        }else{
            $stockistid = $this->input->post('stockistid');
            $where = "( `a`.`level_id` = 'RC' AND `a`.`id` != 'RC0000') or a.id LIKE '$stockistid'";
            $this->db->where($where);
        }
        
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
   
	/*Created by Boby (2009-11-26)*/
	public function getTop10recuit($awal, $akhir){
		$data = array();
		$qry = "
			SELECT 
				m.enroller_id
				, sp.nama
				, COUNT(m.id)AS jml
			FROM member m
			LEFT JOIN member sp ON m.enroller_id = sp.id
			WHERE m.created BETWEEN '$awal' AND '$akhir'
			AND m.enroller_id <> '00000001'
			GROUP BY m.enroller_id
			ORDER BY jml DESC, m.created
			LIMIT 0, 10
		";
        $q = $this->db->query($qry);
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
	}
	
	public function getTop10buyers($awal, $akhir){
		$data = array();
		$qry = "
			SELECT so.member_id, m.nama, FORMAT(SUM(so.totalharga),0)AS jml, FORMAT(SUM(so.totalpv),0)AS pv, SUM(so.totalharga)AS jml1
			FROM so
			LEFT JOIN member m ON so.member_id = m.id
			WHERE 
				so.tgl BETWEEN '$awal' AND '$akhir'
				AND so.kit <> 'Y'
			GROUP BY so.member_id
			ORDER BY jml1 DESC, so.tgl
			LIMIT 0, 10
		";
        $q = $this->db->query($qry);
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
	}
	/*Created by Boby (2009-11-26)*/
	
	/* Created by Boby 2010-08-24 */
	public function newMemberAnalysis($bln, $thn, $flag){
		$data = array();
		$qry = "
			SELECT m.id, m.nama, m.hp, m.telp, m.created
				, case when m.tgllahir < 1 then 'Unknown'
				else m.tgllahir
				end as tgllahir
				,case when (YEAR(CURDATE()) - YEAR(m.tgllahir)) > 200 then 'Unknown'
				else (YEAR(CURDATE()) - YEAR(m.tgllahir))
				end AS umur
			FROM member m
			WHERE ";
			
		if($flag==1){
			$qry .=	"MONTH(m.created) = '$bln' AND YEAR(m.created) = '$thn' ORDER BY DAY(m.created) ASC ";
		}else{
			$qry .= "MONTH(m.tgllahir) = '$bln' ORDER BY DAY(m.tgllahir) ASC ";
		}
		
        $q = $this->db->query($qry);
        //echo $this->db->last_query();
        if($q->num_rows > 0){
			//to_excel($q);
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
	}
	/* End created by Boby 2010-08-24 */
	
	/* Created by Boby 20110329 */
	public function newMemberAnalysisXl($bln, $thn, $flag, $temp){
		$data = array();
		$qry = "
			SELECT nama, REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(hp, '(', ''), ')', ''), ' ', ''),'-',''), '/', '')AS hp
			FROM member m
			LEFT JOIN(
				SELECT member_id, SUM(totalharga)AS blj
				FROM so
				WHERE tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 5 MONTH) + INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
				GROUP BY member_id
			)AS so ON m.id = so.member_id
			WHERE 
				so.blj > 200000
				AND LENGTH(hp)>=10
				AND hp NOT LIKE '000%'
				AND hp NOT LIKE '12345678%'
				AND hp NOT LIKE '11111111%'
				AND hp NOT LIKE '%o%'
				 ";
		/*
		if($flag==1){
			$qry .=	" AND MONTH(m.created) = '$bln' AND YEAR(m.created) = '$thn' ";
		}elseif($flag==2){
			$qry .=	" AND MONTH(m.tgllahir) = '$bln' ";
		}
		*/
		if($flag==2){
			$qry .=	" AND MONTH(m.tgllahir) = '$bln' ";
		}
		if($temp==1){
			$qry .=	"AND kota_id IN (230, 231, 232, 233, 234, 53, 483, 51, 52) ORDER BY hp ";
			$temp = "HP_Member_Jabodetabek(";
		}else{
			$qry .= "ORDER BY hp ";
			$temp = "HP_Member(";
		}
		$temp.=date('YmdHis',now()).")";
		
        $q = $this->db->query($qry);
        //echo $this->db->last_query()." ".$temp;
        if($q->num_rows > 0){
			to_excel($q, $temp);
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
	}
	/* End created by Boby 20110329 */
	
	//Created by Andrew 20120529
	public function newMemberAnalysisX2($bln, $thn, $temp){
		$data = array();
		$qry = "
			SELECT m.nama, REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(stc.hp, '(', ''), ')', ''), ' ', ''),'-',''), '/', '')AS hp
			FROM stockiest stc
			LEFT JOIN member m on stc.id=m.id
			WHERE 
				LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(stc.hp, '(', ''), ')', ''), ' ', ''),'-',''), '/', ''))>=10
				AND stc.hp NOT LIKE '000%'
				AND stc.hp NOT LIKE '%1234567%'
				AND stc.status = 'active'
				 ";
			
		if($temp==2){
			$qry .=	"AND stc.type= 1 AND stc.status= 'active' 
						AND stc.id NOT IN(00004689,10000001) ORDER BY hp ";
			$temp = "HP_Stockiest(";

		}elseif($temp==3){
			$qry .= "AND stc.type= 2 AND stc.status= 'active' 
						ORDER BY hp ";
			$temp = "HP_M-Stockiest(";
		}
		$temp.=date('YmdHis',now()).")";
		
        $q = $this->db->query($qry);
        //echo $this->db->last_query()." ".$temp;
        if($q->num_rows > 0){
			to_excel($q, $temp);
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
	}
	//End created by Andrew 20120529

	// Created by Boby 20120721
	public function getEcb(){
		$data = array();
		$qry = "
			SELECT de.member_id, m.nama, de.ecb, de.totalout, de.saldo_ecb
			FROM deposit_exclusive de
			LEFT JOIN member m ON de.member_id = m.id
			ORDER BY de.member_id
		";
        $q = $this->db->query($qry);
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
	}
	// End created by Boby 20120721
	
	/* Created By Boby 20131112 */
	public function emaildata($bln, $thn, $temp){
		$data = array();
		$qry = "
			SELECT m.nama, m.email AS hp
			FROM stockiest stc
			LEFT JOIN member m on stc.id=m.id
			WHERE 
				LENGTH(m.email)>=11
				 ";
			
		if($temp==1){
			$qry .=	"AND stc.type= 1 AND stc.status= 'active' 
						AND stc.id NOT IN(00004689,10000001) ORDER BY hp ";
			$temp = "Email_Stockiest(";

		}elseif($temp==2){
			$qry .= "AND stc.type= 2 AND stc.status= 'active' 
						ORDER BY hp ";
			$temp = "EMail_M-Stockiest(";
		}
		$temp.=date('YmdHis',now()).")";
		
	        $q = $this->db->query($qry);
        	//echo $this->db->last_query()." ".$temp;
	        if($q->num_rows > 0){
			to_excel($q, $temp);
			foreach($q->result_array() as $row){
	                	$data[] = $row;
	            	}
	        }
        	$q->free_result();
	        return $data;
	}
	/* End created By Boby 20131112 */
}
?>