<?php
class MDownload extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function searchDownload($keywords=0,$num,$offset){
        $data = array();
        $this->db->select('id,description,type,status,created,createdby',false)
            ->from('download')
            ->like('description', $keywords, 'match')
            ->order_by('id','desc')
            ->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countDownload($keywords=0){
        $this->db->like('description', $keywords, 'match');
        $this->db->from('download');
        return $this->db->count_all_results();
    }
    public function addDownload($name='',$file_ext){
        
        $data = array(
            'file_alias' =>strtolower(str_replace(' ','-',$this->input->post('alias'))),
            'file' => $name,
            'file_ext' => $file_ext,
            'type' => $this->input->post('jenisfile'),
            'description' => $this->db->escape_str($this->input->post('shortdesc')),
            'status' => $this->input->post('status'),
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $this->session->userdata('user')
        );
        $this->db->insert('download',$data);
    }
    
    public function updateDownload($filename = '',$file_ext){
        $alias = strtolower(str_replace(' ','-',$this->input->post('alias')));
        if($filename){
            $data = array(
                'type' => $this->input->post('jenisfile'),
                'description' => $this->db->escape_str($this->input->post('shortdesc')),
                'file' => $filename,
                'file_alias' => $alias,
                'file_ext' => $file_ext,
                'status' => $this->input->post('status'),
                'updated' => date('Y-m-d H:m:s',now()),
                'updatedby' => $this->session->userdata('user')
            );
        }else{
            $data = array(
                'type' => $this->input->post('jenisfile'),
                'description' => $this->db->escape_str($this->input->post('shortdesc')),
                'file_alias' => $alias,
                'status' => $this->input->post('status'),
                'updated' => date('Y-m-d H:m:s',now()),
                'updatedby' => $this->session->userdata('user')
            );
        }
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('download',$data);
    }
    
    public function getDownload($id=0){
        $data = array();
        $option = array('id' => $id);
        $q = $this->db->get_where('download',$option,1);
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    public function downloadList($num,$offset){
        $data = array();
        $this->db->select('id,description,type,file',false);
        $this->db->from('download');
        $this->db->where('status','active');
        
        $this->db->order_by('id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countDownloadList($type=''){
        $this->db->where('status','active');
        if($type)$this->db->where('type',$type);
        $this->db->from('download');
        return $this->db->count_all_results();
    }
    
    public function downloadListType($type){
        $data = array();
        $this->db->select('id,description,type,file',false);
        $this->db->from('download');
        $this->db->where('type',$type);
        $this->db->where('status','active');
        $this->db->order_by('id','desc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }

}
?>