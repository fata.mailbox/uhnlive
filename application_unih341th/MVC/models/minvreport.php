<?php
class MInvreport extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | report QOH - Stock Card - Stock Summary
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-08
    |
    */
    public function getQOH($whsid = 1){
        $data=array();
        $q=$this->db->select("i.id,i.name,format(s.qty,0)as stock",false)
            ->from('stock s')
            ->join('item i','s.item_id=i.id','left')
            ->where('s.warehouse_id',$whsid)
			->where('s.qty <> 0')
            ->order_by('i.name','asc')
            ->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getStockSummary($whsid = 1,$fromdate,$todate){
        $data=array();
        $where = "s.warehouse_id = '$whsid' and (s.tgl between '$fromdate' and '$todate') ";
        $q=$this->db->select("s.item_id,s.tgl,i.name,format(s.saldoawal,0)as fsaldoawal,format(sum(s.saldoin),0)as fsaldoin,format(sum(s.saldoout),0)as fsaldoout,format((s.saldoawal+sum(s.saldoin))-sum(s.saldoout),0)as fsaldoakhir", false)
            ->from('stock_summary s')
            ->join('item i','s.item_id=i.id','left')
            ->where($where)
            ->group_by('s.item_id')
            ->order_by('s.tgl','asc')
            ->order_by('i.name','asc')
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getStockCard($whsid = 1,$itemid,$fromdate,$todate){
        $data=array();
        $where = array('s.warehouse_id'=>$whsid,'s.item_id'=>$itemid,'s.date >=' => $fromdate,'s.date <=' => $todate);
        $q=$this->db->select("
				concat(s.desc_id,' - ',s.description)as description,s.date,createdby
				,format(s.saldo_awal,0)as saldo_awal, s.saldo_awal as saldoawal
				,format(s.saldo_in,0)as saldo_in, s.saldo_in as saldoin
				,format(s.saldo_out,0)as saldo_out, s.saldo_out as saldoout
				,format(s.saldo_akhir,0)as saldo_akhir, s.saldo_akhir as saldoakhir
				", false)
            ->from('stock_card s')
            ->where($where)
            ->order_by('s.id','asc')
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getWarehouse(){
        $data = array();
        $q=$this->db->select("id,name",false)
            ->from('warehouse')
            ->order_by('id','asc')
            ->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[$row['id']] = $row['name'];
            }
        }
        $q->free_result();
        return $data;
    }
    
   
}
?>