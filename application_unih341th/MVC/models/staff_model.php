<?php
class STaff_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function search($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.nik,a.name",false);
        $this->db->from('staff a');
        $this->db->like('a.name',$keywords,'between');
        $this->db->or_like('a.nik',$keywords,'between');
        $this->db->order_by('a.name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function count($keywords=0){
        $this->db->like('a.name',$keywords,'between');        
        $this->db->or_like('a.nik',$keywords,'between');
        $this->db->from('staff a');
        return $this->db->count_all_results();
    }
    public function add(){
        $data=array(
            'nik' => $this->input->post('nik'),
            'name' => $this->input->post('name')
        );
            
        $this->db->insert('staff',$data);
    }
    public function check_nik($id){
        $q=$this->db->select("nik",false)
            ->from('staff')
            ->where('nik',$id)
            ->get();
        return ($q->num_rows() > 0)? true : false;    
    }
    public function get($id){
        $data=array();
        $q = $this->db->get_where('staff',array('nik'=>$id));
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function edit(){
        $data=array(
            'name' => $this->input->post('name')
        );
        $this->db->update('staff',$data,array('nik'=>$this->input->post('nik')));
    }    
    
}?>