<?php
class MBonus extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    /*
    |--------------------------------------------------------------------------
    | Report Bonus Statment
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @development by www.smartindo-technology.com
    | @created 2009-05-29
    |
    */
    
    public function getBonus($member_id,$periode){
        $data = array();
        $this->db->order_by('title','asc');
        $q = $this->db->get_where('v_bonus',array('member_id'=>$member_id,'periode'=>$periode));
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getTotalBonus($member_id,$periode){
        $data = array();
        $q = $this->db->select("format(sum(nominal),0)as fnominal,jenjang",false)->from('v_bonus')->where(array('member_id'=>$member_id,'periode'=>$periode))->get();
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getSubBonus($id){
        $data = array();
        $this->db->from('v_bonus')->where('id',$id);
        if($this->session->userdata('group_id')>100)$this->db->where('member_id',$this->session->userdata('userid'));
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
                $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getBonusDetail($id){
        $data = array();
        $q = $this->db->select("d.downline_id,m.nama,format(pv,0)as fpv,d.persen,format(d.nominal,0)as fnominal",false)
				->from('bonus_d d')->join('member m','d.downline_id=m.id','left')->where('d.bonus_id',$id)->order_by('m.nama','asc')->get();
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getDropDrownPeriode(){
        $data = array();
        $this->db->select("periode,tgl",false);
        $this->db->from('v_bonus');
        if($this->session->userdata('group_id')>100){
            $this->db->where('display','1');
            $this->db->where('member_id',$this->session->userdata('userid'));
        }
        $this->db->group_by('tgl');
        $this->db->order_by('periode','desc');
        $q=$this->db->get();
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $data[$row['periode']] = $row['tgl'];
            }
        }
        $q->free_result();
        return $data;
    }
    public function getComisionAnalysis($periode1,$periode2){
        $data = array();
        
        $total = $this->_getOmzet($periode1,$periode2);
        
        $q = $this->db->select("v.title, v.titlebonus,v.persen,sum(v.nominal)as nominal,format(sum(v.nominal),0)as fnominal,format(sum(v.bv),0)as fbv",false)
            ->from('v_bonus v')
            ->where('periode >=',$periode1)
            ->where('periode <=',$periode2)
            ->group_by('title')->order_by('title','asc')->get();
        
		// echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $row['ftotalbv'] = $total['ftotalbv'];
                $row['ftotalharga'] = $total['ftotalharga'];
                $row['totalharga'] = $total['totalharga'];
                $row['totalbv'] = $total['totalbv'];
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    protected function _getOmzet($periode1,$periode2){
        $data=array();
        $q=$this->db->select("sum(totalbv)as totalbv,format(sum(totalbv),0)as ftotalbv,format(sum(totalharga),0)as ftotalharga,sum(totalharga)as totalharga",false)
            ->from('so')
            ->where('date_format(tgl,"%Y-%m") >=',substr($periode1,0,7))
            ->where('date_format(tgl,"%Y-%m") <=',substr($periode2,0,7))
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getTotalCA($periode1,$periode2){
        $data = array();
        $q = $this->db->select("sum(nominal)as nominal,format(sum(nominal),0)as fnominal",false)
            ->from('v_bonus')
            ->where(array('periode >='=>$periode1))
            ->where(array('periode <='=>$periode2))->get();
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getECB($periode){
        $data = array();
        
        $q = $this->db->select("a.member_id,a.hadiah,m.nama",false)
            ->from('ecb a')->join('member m','a.member_id=m.id','left')->where('tgl',$periode)->order_by('m.nama','asc')->get();
        
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Report Bonus Statment
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @development by www.smartindo-technology.com
    | @created 2009-06-02
    |
    */
    
    public function getJenjang($memberid,$periode){
        $data = array();
        $this->db->select("j.member_id,m.nama,date_format(j.tgl,'%M %Y')as ftgl,t.name as jenjanglama,t.decription as desc_jenjanglama,
                          t2.name as jenjangbaru,t2.decription as desc_jenjangbaru,date_format(j.tglakhir,'%M %Y')as ftglakhir",false)
            ->from('jenjang_history j')->join('member m','j.member_id=m.id','left')->join('jenjang t','j.jenjanglama=t.id','left')
            ->join('jenjang t2','j.jenjangbaru=t2.id','left');
        if($memberid){
            $this->db->where('member_id',$memberid);            
        }else{
            $this->db->where('j.tgl',$periode);
        }
        $this->db->order_by('j.id','desc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getPS($memberid){
        $data=array();
        $q = $this->db->select("format(m.ps,0)as fps,format(m.apgs,0)as fapgs
					,format(m.apgsbak,0)as fapgsbak
					, format(m.pgs,0)as fpgs,format(m.pgsbak,0)as fpgsbak,
                    t.name as jenjang,t2.name as jenjang2",false)
            ->from('member m')
            ->join('jenjang t','m.jenjang_id=t.id','left')
            ->join('jenjang t2','m.jenjang_id2=t2.id','left')
            ->where('m.id',$memberid)->get();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getTGPV($memberid,$periode){
        $data = array();
        $this->db->select("m.id,m.nama,format(m.ps,0)as fps,format(m.psbak,0)as fpsbak,format(m.pgs,0)as fpgs,format(m.pgsbak,0)as fpgsbak,t.name as jenjang",false)
            ->from('member m')->join('jenjang t','m.jenjang_id=t.id','left')->where('m.sponsor_id',$memberid);
        if($periode == '1'){
			$this->db->order_by('m.pgs','desc');
			$this->db->order_by('m.ps','desc');
			$this->db->order_by('m.id','asc');
        }else{
			$this->db->order_by('m.pgsbak','desc');
			$this->db->order_by('m.psbak','desc');
			$this->db->order_by('m.id','asc');
		}
        $q = $this->db->get();
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getTotalTGPV($memberid){
        $data=array();
        $q = $this->db->select("format(sum(m.pgs),0)as fpgs,format(sum(m.pgsbak),0)as fpgsbak",false)
            ->from('member m')->where('m.sponsor_id',$memberid)->get();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
/*Updated by Boby (2009-11-17)*/
    public function getTotalAGPV($memberid){
        $data=array();
        $q = $this->db->select("format(sum(m.apgs),0)as apgs",false)
            ->from('member m')->where('m.id',$memberid)->get();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
/*Updated by Boby (2009-11-17)*/


  /*
    |--------------------------------------------------------------------------
    | Report Summary Bonus
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @development by www.smartindo-technology.com
    | @created 2009-06-25
    |
    */
    
    public function sumBonus($periode,$sort){
        $data = array();
        $this->db->select("v.member_id,v.jenjang,v.nama,sum(v.nominal)as nominal,format(sum(v.nominal),0)as fnominal,v.npwp",false)
            ->from('v_bonus v');
        $this->db->where('v.periode',$periode);
        $this->db->group_by('v.member_id');
        if($sort == 'nama')$this->db->order_by('v.nama','asc');
        else$this->db->order_by('nominal','desc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }  
    public function get_downline($page,$member_id,$fromdate,$todate){
        $data = array();
        $this->db->select("n.id,n.member_id,n.leftval as lft,n.rightval as rgt,m.posisi as level,
                          m.nama as name,m.sponsor_id,date_format(m.tglaplikasi,'%d-%m-%Y')as ftglaplikasi",false);
        $this->db->from('networks n');
	$this->db->join('member m','n.member_id=m.id','left');
        $this->db->where('m.sponsor_id',$member_id);
        $this->db->order_by('n.id','asc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
		$row['level0'] = $this->get_jumlah_level_khusus($row['member_id'],$fromdate,$todate);
		$row['level1'] = $this->get_jumlah_level($page+1,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level2'] = $this->get_jumlah_level($page+2,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level3'] = $this->get_jumlah_level($page+3,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level4'] = $this->get_jumlah_level($page+4,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level5'] = $this->get_jumlah_level($page+5,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level6'] = $this->get_jumlah_level($page+6,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level7'] = $this->get_jumlah_level($page+7,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level8'] = $this->get_jumlah_level($page+8,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level9'] = $this->get_jumlah_level($page+9,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level10'] = $this->get_jumlah_level($page+10,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['fjml'] = number_format($row['level1']['total']+$row['level2']['total']+$row['level3']['total']+$row['level4']['total']+$row['level5']['total']+$row['level6']['total']+$row['level7']['total']+$row['level8']['total']+$row['level9']['total']+$row['level10']['total'],'0','',',');
				
		$row['total'] = $this->get_jumlah_level(0,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function get_jumlah_level($posisi,$lft=0,$rgt=0,$level=0,$fromdate,$todate){
        $data = array();
	    
	    if($posisi > 0)$where = "n.leftval >= $lft and n.rightval <= $rgt and (m.posisi - $level) = $posisi and (m.tglaplikasi between '$fromdate' and '$todate')";
	    else $where = "n.leftval >= $lft and n.rightval <= $rgt and (m.tglaplikasi between '$fromdate' and '$todate')";
	    
	    $this->db->select("count(*) as total,count(*)as ftotal",false);
            
	    $this->db->from('networks n');
            $this->db->join('member m','n.member_id=m.id','left');
	
	$this->db->where($where);
	//$this->db->group_by('level');
	//$this->db->limit($show_level,1);
	$q=$this->db->get();
	//echo $this->db->last_query()."<br>";
	if($q->num_rows()>0){
	    $data = $q->row_array();
	}else{
	    $data['fjml'] = 0;
	    $data['total']['ftotal'] = 0;
	}
	$q->free_result();
	return $data;
    }   
      
    public function get_jumlah_level_khusus($memberid,$fromdate,$todate){
        $data = array();
	    
	    $where = "n.member_id = '$memberid' and (m.tglaplikasi between '$fromdate' and '$todate')";
	    $this->db->select("count(*) as total,count(*)as ftotal",false);
	    
	    $this->db->from('networks n');
            $this->db->join('member m','n.member_id=m.id','left');
	
	$this->db->where($where);
	$q=$this->db->get();
	//echo $this->db->last_query()."<br>";
	if($q->num_rows()>0){
	    $data = $q->row_array();
	}
	$q->free_result();
	return $data;
    }
    
    public function kualifikasiLeader($periode,$memberid){
        $data = array();
        $this->db->select("q.member_id,m.nama,date_format(q.tgl,'%M %Y')as ftgl,q.qty",false)
            ->from('leader_qualified q')->join('member m','q.member_id=m.id','left');
        if($memberid){
            $this->db->where('member_id',$memberid);            
        }else{
            $this->db->where('q.tgl',$periode);
        }
        $this->db->order_by('q.tgl','desc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countLeader($periode){
        $data = array();
        $q = "SELECT q.member_id, m.nama, sum(q.qty) as lqty 
                FROM (leader_qualified q) 
                LEFT JOIN member m ON q.member_id=m.id 
                WHERE substr(q.tgl,1,7) >= substr(DATE_ADD('$periode', INTERVAL -11 month),1,7) 
	and substr(q.tgl,1,7) <= substr('$periode',1,7) GROUP BY q.member_id ORDER BY lqty desc";
        $q = $this->db->query($q);
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function get_dropdown(){
        $data = array();
        $this->db->select("date_format(periode,'%Y-%m-%d') as tgl,date_format(periode,'%M - %Y')as ftgl",false);
        $this->db->from('bonus');
        $this->db->group_by('periode');
        $this->db->order_by('periode','desc');
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['tgl']]=$row['ftgl'];    
            }
        }
        $q->free_result();
        return $data;
    }
    public function get_hightjenjang($memberid){
        $data = array();
        $q = "j.jenjangbaru,a.name,a.decription as jenjang from jenjang_history j left join jenjang a on j.jenjangbaru=a.id where j.member_id='$memberid' order by jenjangbaru desc limit 0,1";
        $this->db->select($q);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
                $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
	
	// Updated by Boby 20120522
	public function list_active_member($periode){
        $data = array();
        $where = "date_format(tgl,'%Y-%m') = '$periode'";
        $this->db->select("date_format(tgl,'%d-%m-%Y') as ftgl,d.inactive,d.newstar,d.newrecruit,d.active,d.reactive
                          ,format(d.sorp,0)as fsorp,format(d.sobv,0)as fsobv
                          ,format(d.rorp,0)as frorp,format(d.robv,0)as frobv
                          ,format(d.scpaymentrp,0)as fscpaymentrp,format(d.scpaymentbv,0)as fscpaymentbv
                          ,format(d.returrp,0)as freturrp,format(d.sobv,0)as freturbv
                          ",false)
            ->from('daily_report d');
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $i=0;
        
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function total_active_member($periode){
        $data = array();
        $where = "date_format(tgl,'%Y-%m') = '$periode'";
        $this->db->select("sum(d.inactive),sum(d.newstar),sum(d.newrecruit),sum(d.active),sum(d.reactive)
                          ,format(sum(d.sorp),0)as fsorp,format(sum(d.sobv),0)as fsobv
                          ,format(sum(d.rorp),0)as frorp,format(sum(d.robv),0)as frobv
                          ,format(sum(d.scpaymentrp),0)as fscpaymentrp,format(sum(d.scpaymentbv),0)as fscpaymentbv
                          ,format(sum(d.returrp),0)as freturrp,format(sum(d.sobv),0)as freturbv
                          ",false)
            ->from('daily_report d');
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $i=0;
        
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
	
	public function get_dropdown_daily(){
        $data = array();
        $this->db->select("date_format(tgl,'%Y-%m') as tgl,date_format(tgl,'%M - %Y')as ftgl",false);
        $this->db->from('daily_report');
        $this->db->group_by("date_format(tgl,'%Y-%m')");
        $this->db->order_by('tgl','desc');
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['tgl']]=$row['ftgl'];    
            }
        }
        $q->free_result();
        return $data;
    }
    // End updated by Boby 20120522
}
?>