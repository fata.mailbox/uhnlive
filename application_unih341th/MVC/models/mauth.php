<?php
class MAuth extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    /*
    |--------------------------------------------------------------------------
    | check for login admin
    |--------------------------------------------------------------------------
    |
    | syarat untuk msuk ke dalam backend
    |
    | @param void
    | @return void
    | @author takwa
    | @created 2008-12-26
    |
    */
    public function verifyuser($u,$pw){
        $this->db->select("id,username,name,group_id,warehouse_id,last_login")
            ->where('username',$u)
            ->where('password',substr(sha1(md5(sha1(($this->input->post('password'))))),5,17))
            ->where('status','active')
            ->limit(1);
        $q = $this->db->get('admins');
       
        if($q->num_rows() > 0){
            $row = $q->row_array();
            $this->db->set('last_login', 'NOW()', false);
            $this->db->where('id',$row['id']);
            $this->db->update('admins',$data=array());
            $this->session->set_userdata(array('logged_in'=>'true','userid' => $row['id'],'username' => $row['username'],'user' => $row['username'],'name'=>$row['name'],'last_login' => $row['last_login'],'group_id' => $row['group_id'],'whsid' => $row['warehouse_id']));
            return true;
        }
        return false; //$this->session->set_flashdata('error','Sory, your login id or password is incorrect!');
    }
    
    /**
    * check old password
    *
    * @access public
    * @param username get from session userid
    * @param old password from input
    * @return boolean
   **/
    public function check_password($username,$password){
        $result = $this->_getPassword($username);
        if($result){
            $password = substr(sha1(md5(sha1(($password)))),5,17);
            if ($password === $result->password){
                return true;
            }    
        }
        return false;
    }
    /**
    * _get old password
    *
    * @access protected
    * @param username
    * @return old password
    * @author taQwa
    * @created 2008-11-29
   **/
    protected function _getPassword($user){
        $i=$this->db->select("password",false)
            ->from('admins')
            ->where('username',$user)
            ->get();
            //echo $this->db->last_query();
        return $var = ($i->num_rows() >0) ? $i->row() : false;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Change Password User Office
    |--------------------------------------------------------------------------
    |
    | @param string $user Vaild username
    | @param string $password
    | @author taQwa
    | @author 2008-11-29
    |
    */
    public function change_password(){
        
        $data = array(
            'password' => substr(sha1(md5(sha1(($this->input->post('newpassword'))))),5,17)
        );
        $this->db->update("admins",$data,"id = ".$this->session->userdata('userid'));
        
    }
    
    /*
    |--------------------------------------------------------------------------
    | list Group Users
    |--------------------------------------------------------------------------
    |
    | @author takwa
    | @created 2009-03-09
    |
    */
    public function searchGroup($keywords=0,$num,$offset){
        $data = array();
        
        $this->db->select("a.id,a.title",false);
        $this->db->from('groups a');
        $this->db->like('a.title',$keywords,'after');
        $this->db->order_by('a.title','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countGroup($keywords=0){
        $this->db->from('groups a');
        $this->db->like('a.title',$keywords,'after');
        
        return $this->db->count_all_results();
    }
    public function all_module($code=''){
        $data = array();
        if($code){
            $temp=array();
            $i = $this->db->select("m.menu_id",false)
                ->from('modules m')
                ->where('m.group_id',$code)
                ->get();
            if($i->num_rows > 0 ){
                foreach($i->result_array() as $row){
                    $temp[$row['menu_id']]=$row['menu_id'];    
                }
                
                $q=$this->db->select("a.id,a.title as menu,b.title as sub_menu",false)
                    ->from('menu a')
                    ->join('sub_menu b','a.submenu_id=b.id','left')
                    ->where_not_in('a.id',$temp)
					/**/
			  ->order_by('b.sort','asc')
                    ->order_by('a.sort','asc')
					/**/
					/*
                    ->order_by('a.title','asc')
                    ->order_by('b.title','asc')
					*/
                    ->get();
                if($q->num_rows > 0 ){
                    foreach($q->result_array() as $row){
                        $data[] = $row;
                    }
                }
                $q->free_result();
                return $data;
            }else{
                $q = $this->db->select("a.id,a.title as menu,b.title as sub_menu",false)
                ->from('menu a')
                ->join('sub_menu b','a.submenu_id=b.id','left')
                ->order_by('b.title','asc')
                ->order_by('a.title','asc')
                ->get();
                //echo $this->db->last_query();
                if($q->num_rows > 0 ){
                    foreach($q->result_array() as $row){
                        $data[] = $row;
                    }
                }
                $q->free_result();
                return $data;
            }
            
        }else{
            $q = $this->db->select("a.id,a.title as menu,b.title as sub_menu",false)
                ->from('menu a')
                ->join('sub_menu b','a.submenu_id=b.id','left')
                ->order_by('b.title','asc')
                ->order_by('a.title','asc')
                ->get();
                //echo $this->db->last_query();
                if($q->num_rows > 0 ){
                    foreach($q->result_array() as $row){
                        $data[] = $row;
                    }
                }
                $q->free_result();
                return $data;
        }
    }
    
    public function _cekid($id){
        $q=$this->db->select("id,title",false)
            ->from('groups')
            ->where('id',$id)
            ->limit(1)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row() : false;
    }
    public function addGroup(){
        $groupid=$this->input->post('groupid');
        $data=array(
            'id' => $groupid,
            'title' => $this->input->post('groupname')            
        );
        $this->db->insert('groups',$data);
        
        $loop = COUNT($_POST['viewSltdLstId']);
        for($i=0;$i<$loop;$i++){
            $menu_id=$_POST['viewSltdLstId'][$i];
            $data=array(
                'menu_id' => $menu_id,
                'group_id' => $groupid,
                'view' => '1',
                'save' => '1',
                'edit' => '1',
                'modules.delete' => '1',
                'print' => '1'
            );
            $this->db->insert('modules',$data);
        }
    }
    public function EditGroup(){
        $groupid=$this->input->post('groupid');
        
        $data=array(
            'title' => $this->input->post('groupname')            
        );
        $this->db->update('groups',$data,array('id'=>$groupid));
        
        $loop = COUNT($_POST['viewSltdLstId']);
        for($i=0;$i<$loop;$i++){
            $menu_id=$_POST['viewSltdLstId'][$i];
            $flag=$this->_check_modules($groupid,$menu_id);
            if($flag){
                $this->db->update('modules',array('flag'=>'1'),array('group_id'=>$groupid,'menu_id'=>$menu_id));
            }else{
                $data=array(
                    'menu_id' => $menu_id,
                    'group_id' => $groupid,
                    'view' => '1',
                    'save' => '1',
                    'edit' => '1',
                    'modules.delete' => '1',
                    'print' => '1',
                    'flag' => '1'
                );
                $this->db->insert('modules',$data);
            }
        }
        $this->db->delete('modules',array('flag'=>'0','group_id'=>$groupid));
        $this->db->update('modules',array('flag'=>'0'),array('group_id'=>$groupid));
    }
    public function editAuthoruty(){
        $groupid=$this->input->post('groupid');
        
        $loop = COUNT($_POST['menu_id']);
        for($i=0;$i<$loop;$i++){
            if($_POST['view'][$i] == '1'){
                $data = array(
                   'save'=>$_POST['save'][$i],
                   'edit'=>$_POST['edit'][$i],
                   'modules.delete'=>$_POST['delete'][$i]
                );
                $this->db->update('modules',$data,array('group_id'=>$groupid,'menu_id'=>$_POST['menu_id'][$i]));
            }else{
                $this->db->delete('modules',array('group_id'=>$groupid,'menu_id'=>$_POST['menu_id'][$i]));    
            }
        }
    }
    
    protected function _check_modules($groupid,$menu_id){
        $q=$this->db->select("menu_id",false)
            ->from('modules')
            ->where('menu_id',$menu_id)
            ->where('group_id',$groupid)
            ->limit(1)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row() : false;
    }
    public function getModule($id=0){
        $data = array();
        
        $q = $this->db->select("if(m.view='1','v','-') as view,if(m.save='1','v','-') as save,if(m.edit='1','v','-') as edit,
                               if(m.delete='1','v','-') as del,mn.id,g.title as grup,mn.title as menu,sb.title as submenu",false)
            ->from('modules m')
            ->join('groups g','m.group_id=g.id','left')
            ->join('menu mn','m.menu_id=mn.id','left')
            ->join('sub_menu sb','mn.submenu_id=sb.id','left')
            ->where('m.group_id',$id)
			/**/
			->order_by('sb.sort','asc')
            ->order_by('mn.sort','asc')
			/**/
			/*
            ->order_by('mn.title','asc')
            ->order_by('sb.title','asc')
			*/
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows>0){
            foreach($q->result_array()as $row){
                $data[] =$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getModuleAuth($id=0){
        $data = array();
        
        $q = $this->db->select("m.view,m.save,m.edit,m.delete,mn.id,g.title as grup,mn.title as menu,sb.title as submenu",false)
            ->from('modules m')
            ->join('groups g','m.group_id=g.id','left')
            ->join('menu mn','m.menu_id=mn.id','left')
            ->join('sub_menu sb','mn.submenu_id=sb.id','left')
            ->where('m.group_id',$id)
			/**/
			->order_by('sb.sort','asc')
            ->order_by('mn.sort','asc')
			/**/
			/*
            ->order_by('mn.title','asc')
            ->order_by('sb.title','asc')
			*/
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows>0){
            foreach($q->result_array()as $row){
                $data[] =$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | list Users
    |--------------------------------------------------------------------------
    |
    | @author takwa
    | @created 2009-03-19
    |
    */
    public function searchUser($keywords=0,$num,$offset){
        $data = array();
        
        $this->db->select("a.id,a.username,a.name,a.status,g.title,a.last_login",false);
        $this->db->from('admins a');
        $this->db->join('groups g','a.group_id=g.id','left');
        $this->db->like('g.title',$keywords,'between');
        $this->db->or_like('a.name',$keywords,'between');
		$this->db->or_like('a.username',$keywords,'between');
		$this->db->order_by('a.username','asc');
		$this->db->order_by('g.id','asc');
		$this->db->order_by('a.status','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countUser($keywords=0){
        $this->db->from('admins a');
        $this->db->join('groups g','a.group_id=g.id','left');
        $this->db->like('g.title',$keywords,'between');
        $this->db->or_like('a.name',$keywords,'between');
		$this->db->or_like('a.username',$keywords,'between');
        
        return $this->db->count_all_results();
    }
    
    public function getUser($id=0){
        $data = array();
        
        $this->db->select("a.id,a.username,a.name,a.email,a.status,a.group_id,a.warehouse_id,g.title,a.last_login",false);
        $this->db->from('admins a');
        $this->db->join('groups g','a.group_id=g.id','left');
        $this->db->where('a.id',$id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    public function check_username($username){
        $q=$this->db->select("id",false)
            ->from('admins')
            ->where('username',$username)
            ->get();
        return ($q->num_rows() > 0)? true : false;
    }
    
    public function groupDropDown(){
        $data = array();
        
        $q=$this->db->select("id,title",false)
            ->from('groups')
            ->order_by('title','asc')
            ->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[$row['id']] = $row['title'];
            }
        }
        $q->free_result();
        return $data;
    }
    public function groupUserDropDown(){
        $data = array();
        
        $q=$this->db->select("id,title",false)
            ->from('groups')
            ->where('id <=',100)
            ->order_by('title','asc')
            ->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[$row['id']] = $row['title'];
            }
        }
        $q->free_result();
        return $data;
    }
    public function addUser(){
        
        $timezone = 'UP7';
        $gmttime = local_to_gmt(time(), $timezone);
        $localtime = gmt_to_local($gmttime,$timezone);
        $date = unix_to_human($localtime, TRUE, 'eu');
        
        $data = array(
            'username' => $this->input->post('username'),
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'status' => $this->input->post('status'),
            'password' => substr(sha1(md5(sha1(($this->input->post('passwd'))))),5,17),
            'group_id' => $this->input->post('group_id'),
            'warehouse_id' => $this->input->post('warehouse_id'),
            'created' => $date,
            'createdby' => $this->session->userdata('user')
        );
        
        $this->db->insert('admins',$data);
    }
    
    public function editUser(){
        if($this->input->post('passwd')){
            $data = array(
                'username' => $this->input->post('username'),
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'status' => $this->input->post('status'),
                'password' => substr(sha1(md5(sha1(($this->input->post('passwd'))))),5,17),
                'warehouse_id' => $this->input->post('warehouse_id'),
                'group_id' => $this->input->post('group_id')
            );    
        }else{
            $data = array(
                'username' => $this->input->post('username'),
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'status' => $this->input->post('status'),
                'warehouse_id' => $this->input->post('warehouse_id'),
                'group_id' => $this->input->post('group_id')
            );  
        }
        $this->db->update('admins',$data,array('id'=>$this->input->post('id')));
    }
    
    public function check_editusername($username,$id){
        $q=$this->db->select("id",false)
            ->from('admins')
            ->where('username',$username)
            ->where('id !=',$id)
            ->get();
        return ($q->num_rows() > 0)? true : false;
    }
    public function getDropDownWhs(){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | login for Stockiest
    |--------------------------------------------------------------------------
    |
    | @param string $username Vaild username
    | @param string $password Password
    | @return mixed
    | @author taQwa
    | @author 2008-11-04
    |
    */
    public function dclogin ($username, $password){
        # Grab hash, password, id, activation_code and banned_id from database.
        $result = $this->_dclogin($username);
        if ($result)
        {
            $password = substr(sha1(sha1(md5($result->hash.$password))),5,15);

            if ($password === $result->password)
            {
                $this->db->set('last_login', 'NOW()', false);
                $this->db->where('id',$result->id);
                $this->db->update('stockiest',$data=array());
                $this->session->set_userdata(array('logged_in'=>'true','userid'=>$result->id,'last_login'=>$result->last_login,'name' => $result->nama,'username'=> $result->no_stc,'group_id'=> $result->group_id,'whsid'=> $result->warehouse_id));
                return 'SUCCESS';
            }
        }
        return false;
    }
    protected function _dclogin($username){
        $i = $this->db->select("d.id,d.password,d.hash,d.no_stc,d.last_login,m.nama,d.group_id,d.warehouse_id")	
            ->from('stockiest d')
            ->join('member m', 'd.id= m.id', 'left')
            ->where('d.no_stc', $username)
            ->where('d.status', 'active')
            ->limit(1)
            ->get();
            //echo $this->db->last_query();
        return $var = ($i->num_rows() > 0) ? $i->row() : false;
    }
   
    /*
    |--------------------------------------------------------------------------
    | Change Password
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2000-04-10
    |
    */
    public function check_passwordmember($username,$password){
        $result = $this->_getPasswordSTC($username);
        if($result){
            $password = substr(sha1(sha1(md5($result->hash.$password))),5,15);
            if ($password === $result->password){
                return true;
            }
        }
        return false;
    }
    protected function _getPasswordSTC($username){
        if($this->session->userdata('group_id') == 101){
            $i = $this->db->select("d.id,d.password,d.pin,d.hash,d.username,d.last_login,m.nama,d.group_id,d.banned_id")	
            ->from('users d')
            ->join('member m', 'd.id= m.id', 'left')
            ->where('d.username', $username)
            ->limit(1)
            ->get();
        }else{
           $i = $this->db->select("d.id,d.password,d.pin,d.hash,d.no_stc,d.last_login,m.nama,d.group_id,d.warehouse_id")	
            ->from('stockiest d')
            ->join('member m', 'd.id= m.id', 'left')
            ->where('d.no_stc', $username)
            ->where('d.status', 'active')
            ->limit(1)
            ->get();
        }
        //echo $this->db->last_query();
        return $var = ($i->num_rows() > 0) ? $i->row() : false;
    }
    public function check_pin($username,$pin){
        $result = $this->_getPasswordSTC($username);
        if($result){
            $pin = substr(sha1(sha1(md5($result->hash.$this->input->post('pin')))),3,7);
            //echo $pin." === ". $result->pin;
            if ($pin === $result->pin){
                return true;
            }
        }
        return false;
    }
       
    public function change_password_member(){
        $result = $this->_getPasswordSTC($this->session->userdata('username'));
        $member_id = $this->session->userdata('userid');
        if($result){
            if($this->session->userdata('group_id') == 101){
                $data = array(
                    'password' => substr(sha1(sha1(md5($result->hash.$this->input->post('newpassword')))),5,15)
                );
                $this->db->update("users",$data,array('id' =>$member_id));
            }else{
                $data = array(
                    'password' => substr(sha1(sha1(md5($result->hash.$this->input->post('newpassword')))),5,15)
                );
                $this->db->update("stockiest",$data,array('id' =>$member_id));
            }
        }
    }
    public function change_pin(){
        $result = $this->_getPasswordSTC($this->session->userdata('username'));
        if($result){
            $pin = substr(sha1(sha1(md5($result->hash.$this->input->post('newpin')))),3,7);
            $data = array(
                'pin' => $pin
            );
            if($this->session->userdata('group_id') == 101){
                $this->db->update('users',$data,"id = '$result->id'");
            }else{
                $this->db->update('stockiest',$data,"id = '$result->id'");
            }
        }
    }
    
    /*
    |--------------------------------------------------------------------------
    | Login for Member
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2000-04-10
    |
    */
    public function memberLogin ($username, $password){
        # Grab hash, password, id, activation_code and banned_id from database.
        $result = $this->_memberLogin($username);
        if ($result)
        {
            if(!empty($result->banned_id))
            {
                $this->logout();
                return 'BANNED';
            }
            else
            {
                $password = substr(sha1(sha1(md5($result->hash.$this->input->post('password')))),5,15);

                if ($password === $result->password)
                {
                    $this->db->set('last_login', 'NOW()', false);
                    $this->db->where('id',$result->id);
                    $this->db->update('users',$data=array());
                    $this->session->set_userdata(array('logged_in'=>'true','userid'=>$result->id,'whsid'=>$result->warehouse_id,'last_login'=>$result->last_login,'name' => $result->nama,'username'=> $result->username,'group_id'=> $result->group_id));
                    return 'SUCCESS';
                }
            }
        }
        return false;
    }
    protected function _memberLogin($username){
        $i = $this->db->select("d.id,d.password,d.warehouse_id,d.hash,d.username,d.last_login,m.nama,d.group_id,d.banned_id,b.reason")	
            ->from('users d')
        ->join('banned b', 'd.banned_id = b.id', 'left')
        ->join('member m', 'd.id= m.id', 'left')
        ->where('d.username', $username)
        ->limit(1)
        ->get();
        return $var = ($i->num_rows() > 0) ? $i->row() : false;
    }
    
    public function banned ($username)
    {
        return $result = $this->_memberLogin($username);
    }
        
    
    
    /*
    |--------------------------------------------------------------------------
    | Logout
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-04-10
    |
    */
    public function logout(){
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('userid');
        $this->session->unset_userdata('group_id');
        $this->session->unset_userdata('whsid');
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('user');
        $this->session->unset_userdata('last_login');
        $this->session->unset_userdata('keywords');
        $this->session->unset_userdata('conditions');
        $this->session->sess_destroy();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Logout
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-05-07
    |
    */
    public function searchResetPassword($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("r.id,r.member_id,m.nama,r.passwd,r.pin,date_format(r.created,'%d-%b-%Y')as created,r.createdby",false);
        $this->db->from('reset_password r');
        $this->db->join('member m','r.member_id=m.id','left');
        $this->db->like('m.id', $keywords, 'after');
        $this->db->or_like('m.nama', $keywords, 'after');
        $this->db->order_by('id','desc');
        $q = $this->db->get();
        
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countResetPassword($option=0,$keywords=0){
        $this->db->select("r.id",false);
        $this->db->from('reset_password r');
        $this->db->join('member m','r.member_id=m.id','left');
        $this->db->like('m.id', $keywords, 'after');
        $this->db->or_like('m.nama', $keywords, 'after');
        
        return $this->db->count_all_results();
    }
    
    private function _getResetPin(){
        $angka_master = '0123456789012345678901234567890';
        
        $angka = '';
        for ($i=0; $i<6; $i++) {
            $angka .= $angka_master{rand(0,30)};
        }
        
        return $var = $angka;
    }
    private function _getResetPassword(){
        $angka_master = '01234567890abcdefghjkmnpqrstuvwxyz';
        
        $angka = '';
        for ($i=0; $i<8; $i++) {
            $angka .= $angka_master{rand(0,30)};
        }
        
        return $var = $angka;
    }
    
    public function addResetPassword(){
        $amount = 25000;
        $member_id = $this->input->post('member_id');
        $empid = $this->session->userdata('username');
        
        $hash = $this->_getHash($member_id);
                
        
        if($this->input->post('passwd') && $this->input->post('pin')){
            $password = $this->_getResetPassword();
            $pin = $this->_getResetPin();
            
            $data = array(
                'member_id' => $member_id,
                'passwd'  => $password,
                'pin'  =>  $pin,
                'amount'  => $amount,
                'event_id'  => 'SO3',
                'created' => date('Y-m-d', now()),
                'createdby' => $empid
            );
            
            $password_enc = substr(sha1(sha1(md5($hash->hash.$password))),5,15);
            $pin_enc = substr(sha1(sha1(md5($hash->hash.$pin))),3,7);
            
            $this->db->update('users',array('password'=>$password_enc,'pin'=>$pin_enc),array('id'=>$member_id));
        }else{
            if($this->input->post('passwd')){
                $password = $this->_getResetPassword();
                $data = array(
                    'member_id' => $member_id,
                    'passwd'  => $password,
                    'amount'  => $amount,
                    'event_id'  => 'SO3',
                    'created' => date('Y-m-d', now()),
                    'createdby' => $empid
                );
                
                $password_enc = substr(sha1(sha1(md5($hash->hash.$password))),5,15);
                $this->db->update('users',array('password'=>$password_enc),array('id'=>$member_id));
            }else{
                $pin = $this->_getResetPin();
                
                $data = array(
                    'member_id' => $member_id,
                    'pin'  => $pin,
                    'amount'  => $amount,
                    'event_id'  => 'SO3',
                    'created' => date('Y-m-d', now()),
                    'createdby' => $empid
                );
                $pin_enc = substr(sha1(sha1(md5($hash->hash.$pin))),3,7);
                $this->db->update('users',array('pin'=>$pin_enc),array('id'=>$member_id));
            }
        }
        $this->db->insert('reset_password',$data);
        $id = $this->db->insert_id();
        
        $this->db->query("call sp_ewallet_mem('$member_id','company','Reset Password / PIN','$id','25000','$empid')");
    }
    private function _getHash($id){
        $q=$this->db->select("hash",false)
            ->from('users')
            ->where('id',$id)
            ->get();
        return ($q->num_rows() > 0)? $q->row() : false;    
    }
    
    public function getResetPassword($id){
        $data = array();
        $this->db->select("r.id,r.member_id,m.nama,r.passwd,r.pin,date_format(r.created,'%d-%b-%Y')as created,r.createdby",false);
        $this->db->from('reset_password r');
        $this->db->join('member m','r.member_id=m.id','left');
        $this->db->where('r.id', $id);
        $q = $this->db->get();
        
        if($q->num_rows >0){
            $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
    public function getEwallet($member_id){
        $q = $this->db->select("ewallet,format(ewallet,0)as fewallet",false)
            ->from('member')
            ->where('id',$member_id)
            ->get();
        //echo $this->db->last_query();
        return $var = ($q->num_rows()>0)? $q->row() : false;
    }
	
	/* created by Boby 20100629 */
	public function change_name(){
		$member_id = $this->input->post('member_id');
		$nama = $this->input->post('name1');
		$oldname = $this->input->post('name');
		$empId = $this->session->userdata('username');
		$now = date("Y-m-d H:i:s");
        $this->db->query("UPDATE member SET nama = '$nama' WHERE id = '$member_id' ");
		$this->db->query("INSERT INTO rename_member (member_id, oldname, newname, createdby, createddate) VALUES ('$member_id', '$oldname', '$nama', '$empId', '$now') ");
    }
	/* end created by Boby 20100629 */
    
	/* created by Boby 20110317 */
	public function add_pdpm(){
		$member_id = $this->input->post('member_id');
		$empId = $this->session->userdata('username');
		$now = date("Y-m-d H:i:s");
        $this->db->query("CALL sp_z_201101_pdpm_reg('$now', '$member_id', '$empId') ");
    }
	/* end created by Boby 20110317 */
	
	/* created by Boby 20130812 */
	public function rellocation(){
		$member_id = $this->input->post('member_id0');
		$upline_id = $this->input->post('member_id1');
		$sponsor_id = $this->input->post('member_id2');
		$empId = $this->session->userdata('username');
		$now = date("Y-m-d H:i:s");
        $this->db->query("CALL tools_set_tree('$member_id','$upline_id','$sponsor_id', '$empId');");
    }
	/* end created by Boby 20130812 */
	
	/* Created by Boby 20130905 */
	public function getRellocation($keyword, $num, $offset){
        $data = array();
        $this->db->select("
			CONCAT(nh.member_id, '/', m.nama)AS member
			, CONCAT(nh.upline_old, '/', upo.nama)AS up_old
			, CONCAT(nh.upline_new, '/', upn.nama)AS up_new
			, CONCAT(nh.enroller_old, '/', eo.nama)AS en_old
			, CONCAT(nh.enroller_new, '/', en.nama)AS en_new
			, nh.createdby
		",false);
        $this->db->from('network_history nh');
        $this->db->join('member m','nh.member_id=m.id','left');
        $this->db->join('member upo','nh.upline_old=upo.id','left');
        $this->db->join('member upn','nh.upline_new=upn.id','left');
        $this->db->join('member eo','nh.enroller_old=eo.id','left');
        $this->db->join('member en','nh.enroller_new=en.id','left');
		$this->db->like('m.id',$keywords,'between');
		$this->db->or_like('m.nama',$keywords,'between');
		$this->db->order_by('nh.id','desc');
		$this->db->limit($num,$offset);
        $q = $this->db->get();
		// echo $this->db->last_query();
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	public function countRellocation($keyword){
        $data = array();
        
        $this->db->from('network_history nh');
        $this->db->join('member m','nh.member_id=m.id','left');
        $this->db->join('member upo','nh.upline_old=upo.id','left');
        $this->db->join('member upn','nh.upline_new=upn.id','left');
        $this->db->join('member eo','nh.enroller_old=eo.id','left');
        $this->db->join('member en','nh.enroller_new=en.id','left');
		$this->db->like('m.id',$keywords,'between');
		$this->db->or_like('m.nama',$keywords,'between');
		$this->db->order_by('nh.id','desc');
		$this->db->limit($num,$offset);
        // $q = $this->db->get();
		// echo $this->db->last_query();
        return $this->db->count_all_results();
    }
	/* End created by Boby 20130905 */
	
	/* Created by Boby 201404025 */
	public function countCancelSo($keywords){
        $data = array();
        $this->db->select("
			so.tgl, cso.so_id_canceled AS so_id, cso.so_id_execute AS so_cancel
			, so.member_id, m.nama, stc.no_stc, s.nama AS namaStc
			, cso.created, cso.createdby
		",false);
        $this->db->from('canceled_so cso');
        $this->db->join('so','cso.so_id_canceled = so.id','left');
        $this->db->join('member m','so.member_id = m.id','left');
        $this->db->join('stockiest stc','so.stockiest_id = stc.id','left');
        $this->db->join('member s','stc.id = s.id','left');
        $this->db->like('cso.so_id_canceled',$keywords,'between');
		$this->db->or_like('cso.so_id_execute',$keywords,'between');
		$this->db->or_like('m.id',$keywords,'between');
		$this->db->or_like('m.nama',$keywords,'between');
		$this->db->or_like('s.id',$keywords,'between');
		$this->db->or_like('s.nama',$keywords,'between');
		$this->db->or_like('stc.no_stc',$keywords,'between');
		$this->db->order_by('cso.id','desc');
		// $this->db->limit($num,$offset);
        // $q = $this->db->get();
		// echo $this->db->last_query();
        return $this->db->count_all_results();
    }
	public function getCancelSo($keywords, $num, $offset){
        $data = array();
        $this->db->select("
			so.tgl, cso.so_id_canceled AS so_id, cso.so_id_execute AS so_cancel
			, so.member_id, m.nama, stc.no_stc, s.nama AS namaStc
			, cso.created, cso.createdby
		",false);
        $this->db->from('canceled_so cso');
        $this->db->join('so','cso.so_id_canceled = so.id','left');
        $this->db->join('member m','so.member_id = m.id','left');
        $this->db->join('stockiest stc','so.stockiest_id = stc.id','left');
        $this->db->join('member s','stc.id = s.id','left');
        $this->db->like('cso.so_id_canceled',$keywords,'between');
		$this->db->or_like('cso.so_id_execute',$keywords,'between');
		$this->db->or_like('m.id',$keywords,'between');
		$this->db->or_like('m.nama',$keywords,'between');
		$this->db->or_like('s.id',$keywords,'between');
		$this->db->or_like('s.nama',$keywords,'between');
		$this->db->or_like('stc.no_stc',$keywords,'between');
		$this->db->order_by('cso.id','desc');
		$this->db->limit($num,$offset);
        $q = $this->db->get();
		// echo $this->db->last_query();
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	public function getInfoSo($keyword){
        $data = array();
		//$leader = 1;
		$query = "
			SELECT so.id, CASE 
					WHEN cso.so_id_canceled > 0 THEN 1
					WHEN cso1.so_id_execute > 0 THEN 2
					WHEN so.tgl < (SELECT MAX(periode) FROM bonus LIMIT 0,1) THEN 3
					WHEN so.kit = 'y' THEN 4
					ELSE 0
				END AS note
			FROM so
			LEFT JOIN canceled_so cso ON so.id = cso.so_id_canceled
			LEFT JOIN canceled_so cso1 ON so.id = cso1.so_id_execute
			WHERE so.id = '$keyword'
		";
		
		$qry = $this->db->query($query);
		
		// echo $this->db->last_query();
        if($qry->num_rows() > 0){
            $data = $qry->row_array();
        }
        $qry->free_result();
        return $data;
    }
	
	public function cancelSo(){
        $so_id = $this->input->post('so_id');
        $empid = $this->session->userdata('username');
		$this->db->query("CALL sp_cancel_so_sys('$so_id','$empid')");
    }
	/* End created by Boby 201404025 */
}
?>