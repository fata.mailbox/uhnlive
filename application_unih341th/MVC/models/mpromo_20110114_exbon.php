<?php
class MPromo_20110114_exbon extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
	public function getDropDrownPeriode(){
        $data = array();
        $this->db->select("CONCAT((DATE_FORMAT((periode)-INTERVAL 7 DAY,'%d %M %Y')),' - ', (DATE_FORMAT((periode),'%d %M %Y')))AS tgl, periode AS periode",false);
        $this->db->from('z_201101_exbon_d');
        $this->db->group_by('periode');
        $this->db->order_by('periode','desc');
        $q=$this->db->get();
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $data[$row['periode']] = $row['tgl'];
            }
        }
        $q->free_result();
        return $data;
    }
}
?>