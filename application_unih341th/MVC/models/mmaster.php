<?php
class MMaster extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Master Item
    |--------------------------------------------------------------------------
    |
    | to sign warehouse
    |
    | @created 2009-03-29
    | @author qtakwa@yahoo.com@yahoo.com
    |
    */
    public function searchItem($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.name,a.type,a.sales,a.display,a.manufaktur",false);
        $this->db->from('item a');
        $this->db->like('a.id',$keywords,'after');
        $this->db->or_like('a.name',$keywords,'after');
        $this->db->order_by('a.name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countItem($keywords=0){
        $this->db->like('a.id',$keywords,'after');
        $this->db->or_like('a.name',$keywords,'after');
        $this->db->from('item a');
        return $this->db->count_all_results();
    }
    public function addItem($image){
        $id=$this->input->post('id');
        if($image){
            $data=array(
                'id' => $id,
                'name' => $this->input->post('name'),
                'headline' => $this->db->escape_str($this->input->post('headline')),
                'description' => $_POST['longdesc'],
                'image' => $image,
                'sales' => $this->input->post('sales'),
                'display' => $this->input->post('display'),
                'type' => $this->input->post('type'),
                'manufaktur' => $this->input->post('manufaktur'),
                'created' => date('Y-m-d H:i:s',now()),
                'createdby' => $this->session->userdata('user')
            );
        }else{
            $data=array(
                'id' => $id,
                'name' => $this->input->post('name'),
                'headline' => $this->db->escape_str($this->input->post('headline')),
                'description' => $_POST['longdesc'],
                'sales' => $this->input->post('sales'),
                'display' => $this->input->post('display'),
                'type' => $this->input->post('type'),
                'manufaktur' => $this->input->post('manufaktur'),
                'created' => date('Y-m-d H:i:s',now()),
                'createdby' => $this->session->userdata('user')
            );
        }
        $this->db->insert('item',$data);
        
        $this->db->query("call sp_item('$id')");
    }
       
    public function getItem($id){
        $data=array();
        $q = $this->db->get_where('item',array('id'=>$id));
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function editItem($image){
        $id=$this->input->post('id');
        if($image){
            $data=array(
                'name' => $this->input->post('name'),
                'headline' => $this->db->escape_str($this->input->post('headline')),
                'description' => $_POST['longdesc'],
                'image' => $image,
                'sales' => $this->input->post('sales'),
                'display' => $this->input->post('display'),
                'type' => $this->input->post('type'),
                'manufaktur' => $this->input->post('manufaktur'),
                'created' => date('Y-m-d H:i:s',now()),
                'createdby' => $this->session->userdata('user')
            );
        }else{
            $data=array(
                'name' => $this->input->post('name'),
                'headline' => $this->db->escape_str($this->input->post('headline')),
                'description' => $_POST['longdesc'],
                'sales' => $this->input->post('sales'),
                'display' => $this->input->post('display'),
                'type' => $this->input->post('type'),
                'manufaktur' => $this->input->post('manufaktur'),
                'created' => date('Y-m-d H:i:s',now()),
                'createdby' => $this->session->userdata('user')
            );
        }
            
        $this->db->update('item',$data,array('id'=>$this->input->post('id')));
    }
    public function check_productid($id){
        $q=$this->db->select("id",false)
            ->from('item')
            ->where('id',$id)
            ->get();
        return ($q->num_rows() > 0)? true : false;    
    }
    
	/* Created by Boby 20140121 */
	public function updateDeliveryAddress(){
		$data = array(
			'member_id' => $this->input->post('member_id'),
			'kota' => $this->input->post('kota_id'),
			'alamat' => $this->input->post('addr'),
			'created' => $this->session->userdata('username'),
		);
		
		if(	$this->input->post('addr') != $this->input->post('addr1')	|| 	$this->input->post('kota_id') != $this->input->post('kota_id1')){
			$this->db->insert('member_delivery',$data);
			$id = $this->db->insert_id();
			$this->db->update('member',array('delivery_addr'=>$id),array('id' => $this->input->post('member_id')));
		}
    }
	/* End created by Boby 20140121 */
}?>