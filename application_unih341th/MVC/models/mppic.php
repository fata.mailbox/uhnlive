<?php
class MPpic extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
	
	/* Modified by Boby (2009-12-10) */
	public function ppicneed($fromdate,$todate)
    {
        $data = array();
		$qry = $this->db->query("		
		SELECT data1.id, MAX(data1.sh_prd)AS part_no, i.`name` AS Description, SUM(data1.totalqty)AS qty
		FROM(
			SELECT 
				CASE
					WHEN newdata.id IN (SELECT manufaktur_id FROM manufaktur GROUP BY manufaktur_id) THEN m.item_id
					ELSE newdata.id
				END AS id
				, newdata.sh_prd
				, CASE
					WHEN newdata.id IN (SELECT manufaktur_id FROM manufaktur GROUP BY manufaktur_id) THEN
						((newdata.jml + newdata.jmlsc + newdata.jmlnc) - newdata.jmlretur) * m.qty
					ELSE ((newdata.jml + newdata.jmlsc + newdata.jmlnc) - newdata.jmlretur)
				END AS totalqty
			FROM(
				SELECT 
					dta.id
					, dta.sh_prd
					, dta.`name` AS nama
					, dta.price AS harga
					, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
					, IFNULL(dtf.jml,0) AS jmlnc
					, IFNULL(dtd.jml,0) AS jmlsc
					, IFNULL(dte.jml,0) AS jmlretur
				FROM(
					SELECT id, `name`, price, sh_prd FROM item
				)AS dta

				LEFT JOIN(
					SELECT sod.item_id, SUM(qty)AS jml
					FROM so_d sod
					LEFT JOIN so ON sod.so_id = so.id
					WHERE 
						so.created BETWEEN '$fromdate' AND '$todate'
						AND so.stockiest_id = '0'
					GROUP BY item_id, sod.harga
				)AS dtb ON dta.id = dtb.item_id

				LEFT JOIN(
					SELECT rod.item_id, SUM(qty)AS jml
					FROM ro_d rod
					LEFT JOIN ro ON rod.ro_id = ro.id
					WHERE 
						ro.`date` BETWEEN '$fromdate' AND '$todate'
						AND ro.stockiest_id = '0'
					GROUP BY item_id, rod.harga
				)AS dtc ON dta.id = dtc.item_id

				LEFT JOIN(
					SELECT ptd.item_id, SUM(qty) AS jml
					FROM pinjaman_titipan_d ptd
					LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
					WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
					GROUP BY ptd.item_id, ptd.harga
				)AS dtd ON dta.id = dtd.item_id

				LEFT JOIN(
					SELECT rtd.item_id, SUM(qty) AS jml
					FROM retur_titipan_d rtd
					LEFT JOIN retur_titipan rt ON rtd.retur_titipan_id = rt.id
					WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
					GROUP BY rtd.item_id, rtd.harga
				)AS dte ON dta.id = dte.item_id
				
				LEFT JOIN(
					SELECT ncd.item_id, SUM(ncd.qty) AS jml
					FROM ncm_d ncd
					LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
					WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
					GROUP BY ncd.item_id
				)AS dtf ON dta.id = dtf.item_id
			)AS newdata
			LEFT JOIN manufaktur m ON newdata.id = m.manufaktur_id AND m.item_id IN(SELECT id FROM item WHERE sh_prd <> '-')
			LEFT JOIN item i ON m.item_id = i.id 
			WHERE newdata.id IN
			(SELECT id FROM item WHERE sh_prd <> '-' OR id IN (SELECT manufaktur_id FROM manufaktur GROUP BY manufaktur_id))
		)AS data1
		LEFT JOIN item i ON data1.id = i.id
		GROUP BY id
		");
        $i=0;
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
					$i+=1;
					$row['i'] = $i;
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
	public function ppicmonth($fromdate,$i)
    {
        $data = array();
		$qry = $this->db->query("		
		SELECT data1.id, MAX(data1.sh_prd)AS part_no, i.`name` AS Description, SUM(data1.totalqty)AS qty
		FROM(
			SELECT 
				CASE
					WHEN newdata.id IN (SELECT manufaktur_id FROM manufaktur GROUP BY manufaktur_id) THEN m.item_id
					ELSE newdata.id
				END AS id
				, newdata.sh_prd
				, CASE
					WHEN newdata.id IN (SELECT manufaktur_id FROM manufaktur GROUP BY manufaktur_id) THEN
						((newdata.jml + newdata.jmlsc + newdata.jmlnc) - newdata.jmlretur) * m.qty
					ELSE ((newdata.jml + newdata.jmlsc + newdata.jmlnc) - newdata.jmlretur)
				END AS totalqty
			FROM(
				SELECT 
						dta.id
						, dta.sh_prd
						, dta.`name` AS nama
						, dta.price AS harga
						, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
						, IFNULL(dtf.jml,0) AS jmlnc
						, IFNULL(dtd.jml,0) AS jmlsc
						, IFNULL(dte.jml,0) AS jmlretur
					FROM(
						SELECT id, `name`, price, sh_prd FROM item
					)AS dta

					LEFT JOIN(
						SELECT sod.item_id, SUM(qty)AS jml
						FROM so_d sod
						LEFT JOIN so ON sod.so_id = so.id
						WHERE 
							MONTH(so.created) = MONTH(DATE_SUB('$fromdate',INTERVAL $i MONTH))
							AND so.stockiest_id = '0'
						GROUP BY item_id, sod.harga
					)AS dtb ON dta.id = dtb.item_id

					LEFT JOIN(
						SELECT rod.item_id, SUM(qty)AS jml
						FROM ro_d rod
						LEFT JOIN ro ON rod.ro_id = ro.id
						WHERE 
							MONTH(ro.`date`) = MONTH(DATE_SUB('$fromdate',INTERVAL $i MONTH))
							AND ro.stockiest_id = '0'
						GROUP BY item_id, rod.harga
					)AS dtc ON dta.id = dtc.item_id

					LEFT JOIN(
						SELECT ptd.item_id, SUM(qty) AS jml
						FROM pinjaman_titipan_d ptd
						LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
						WHERE MONTH(pj.tgl) = MONTH(DATE_SUB('$fromdate',INTERVAL $i MONTH))
						GROUP BY ptd.item_id, ptd.harga
					)AS dtd ON dta.id = dtd.item_id

					LEFT JOIN(
						SELECT rtd.item_id, SUM(qty) AS jml
						FROM retur_titipan_d rtd
						LEFT JOIN retur_titipan rt ON rtd.retur_titipan_id = rt.id
						WHERE MONTH(rt.tgl) = MONTH(DATE_SUB('$fromdate',INTERVAL $i MONTH))
						GROUP BY rtd.item_id, rtd.harga
					)AS dte ON dta.id = dte.item_id
					
					LEFT JOIN(
						SELECT ncd.item_id, SUM(ncd.qty) AS jml
						FROM ncm_d ncd
						LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
						WHERE MONTH(nc.`date`) = MONTH(DATE_SUB('$fromdate',INTERVAL $i MONTH))
						GROUP BY ncd.item_id
					)AS dtf ON dta.id = dtf.item_id
				)AS newdata
				LEFT JOIN manufaktur m ON newdata.id = m.manufaktur_id AND m.item_id IN(SELECT id FROM item WHERE sh_prd <> '-')
				LEFT JOIN item i ON m.item_id = i.id 
				WHERE newdata.id IN
				(SELECT id FROM item WHERE sh_prd <> '-' OR id IN (SELECT manufaktur_id FROM manufaktur GROUP BY manufaktur_id))
		)AS data1
		LEFT JOIN item i ON data1.id = i.id
		GROUP BY id
		");
        $i=0;
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
					$i+=1;
					$row['i'] = $i;
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
    /* End modified by Boby (2009-12-10) */
}?>