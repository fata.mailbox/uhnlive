<?php
class MPo extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Purchase Order
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-28
    |
    */
    
    public function searchPO($keywords=0,$num,$offset){
        $data = array();
        $where = "( a.id LIKE '$keywords%' OR m.name LIKE '$keywords%' )";
        
        $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,format(a.totalorder,0)as ftotalorder,
                          a.status,m.name",false);
        $this->db->from('po_order a');
        $this->db->join('supplier m','a.supplier_id=m.id','left');
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countPO($keywords=0){
        $where = "( a.id LIKE '$keywords%' OR m.name LIKE '$keywords%' )";
        
        $this->db->from('po_order a');
        $this->db->join('supplier m','a.supplier_id=m.id','left');
        $this->db->where($where);
        //$q=$this->db->get();
        //echo $this->db->last_query();
        return $this->db->count_all_results();
    }
    public function getDropDownSupplier($all=''){
        $data = array();
        $q = $this->db->get('supplier');
        if($q->num_rows >0){
            if($all == 'all')$data['all']='All Supplier';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    public function addPO(){
        $totalharga = str_replace(".","",$this->input->post('total'));
        $empid = $this->session->userdata('user');
        $whsid = 1;
        $tgl = $this->input->post('date');
        $tgldetail = date('Y-m-d',now());
        
        $data = array(
            'tgl'=> $tgldetail,
            'supplier_id'=>$this->input->post('supplier_id'),
            'totalorder' => $totalharga,
            'warehouse_id' => $whsid,
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('po_order',$data);
        
        $id = $this->db->insert_id();
        
        $qty0 = str_replace(".","",$this->input->post('qty0'));
        if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'po_order_id' => $id,
                
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                'harga' => str_replace(".","",$this->input->post('price0')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal0'))
            );
            
            $this->db->insert('po_order_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'po_order_id' => $id,
                
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => str_replace(".","",$this->input->post('price1')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal1'))               
            );
            
            $this->db->insert('po_order_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'po_order_id' => $id,
                
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => str_replace(".","",$this->input->post('price2')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal2'))
            );
            
            $this->db->insert('po_order_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'po_order_id' => $id,
                
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => str_replace(".","",$this->input->post('price3')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal3'))
            );
            
            $this->db->insert('po_order_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'po_order_id' => $id,
                
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => str_replace(".","",$this->input->post('price4')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal4'))
            );
            
            $this->db->insert('po_order_d',$data);
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
            $data=array(
                'po_order_id' => $id,
                
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => str_replace(".","",$this->input->post('price5')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal5'))
            );
            
            $this->db->insert('po_order_d',$data);
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
            $data=array(
                'po_order_id' => $id,
                
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => str_replace(".","",$this->input->post('price6')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal6'))
            );
            
            $this->db->insert('po_order_d',$data);
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
            $data=array(
                'po_order_id' => $id,
                
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => str_replace(".","",$this->input->post('price7')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal7'))
            );
            
            $this->db->insert('po_order_d',$data);
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
            $data=array(
                'po_order_id' => $id,
                
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => str_replace(".","",$this->input->post('price8')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal8'))
            );
            
            $this->db->insert('po_order_d',$data);
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
       if($this->input->post('itemcode9') and $qty9 > 0){
            $data=array(
                'po_order_id' => $id,
                
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                'harga' => str_replace(".","",$this->input->post('price9')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal9'))
            );
            
            $this->db->insert('po_order_d',$data);
        }
            $totalorder = $this->_getTotalOrder($id,'po_order_d','po_order_id');
            
            $data = array('totalorder' => $totalorder['totalorder']);
            $this->db->update('po_order',$data,array('id'=>$id));
    }
    private function _getTotalOrder($id,$table,$key){
        $data = array();
        $q = $this->db->select("sum(jmlharga) as totalorder",false)->from($table)->where($key,$id)->get();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    public function getPO($id=0){
        $data = array();
        $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,format(a.totalorder,0)as ftotalorder,a.status,a.remark,
                          date_format(a.created,'%d-%b-%Y')as created,a.createdby,date_format(a.updated,'%d-%b-%Y')as updated,a.updatedby,
                    m.name,m.address,m.telp,m.cp",false);
        $this->db->from('po_order a');
        $this->db->join('supplier m','a.supplier_id=m.id','left');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getPOActual($id=0){
        $data = array();
        $this->db->select("a.id,b.event_id,date_format(a.tgl,'%d-%b-%Y')as tgl,date_format(a.tgljatuhtempo,'%d-%b-%Y')as tgljatuhtempo,
                          format(a.totalorder,0)as ftotalorder,format(a.totalactual,0)as ftotalactual,a.status,a.remark,
                          date_format(a.created,'%d-%b-%Y')as created,a.createdby,date_format(a.updated,'%d-%b-%Y')as updated,a.updatedby,
                    m.name,m.address,m.telp,m.cp",false);
        $this->db->from('po_order a');
        $this->db->join('supplier m','a.supplier_id=m.id','left');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getPODetail($id=0){
        $data = array();
        $this->db->select("d.item_id,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.jmlharga,0)as fsubtotal,a.name",false);
        $this->db->from('po_order_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.po_order_id',$id);
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getStatus($id){
        $q = $this->db->select("status",false)
            ->from('po_order')
            ->where('id',$id)
            ->where('status','close')
            ->get();
        return $var = ($q->num_rows()>0)? $q->row() : false;   
    }
    public function addPOActual(){
        $totalharga = str_replace(".","",$this->input->post('total'));
        $empid = $this->session->userdata('user');
        $tgl = $this->input->post('date');
        
        $idorder = $this->input->post('id');
        
        $data = array(
            'po_order_id' => $idorder,
            'tgljatuhtempo' => $tgl,
            'tgl'=>date('Y-m-d',now()),
            'totalactual' => $totalharga,
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('po',$data);
        
        $id = $this->db->insert_id();
        
        $qty0 = str_replace(".","",$this->input->post('qty0'));
        if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'po_id' => $id,
                
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                'harga' => str_replace(".","",$this->input->post('price0')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal0'))
            );
            
            $this->db->insert('po_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'po_id' => $id,
                
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => str_replace(".","",$this->input->post('price1')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal1'))               
            );
            
            $this->db->insert('po_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'po_id' => $id,
                
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => str_replace(".","",$this->input->post('price2')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal2'))
            );
            
            $this->db->insert('po_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'po_id' => $id,
                
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => str_replace(".","",$this->input->post('price3')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal3'))
            );
            
            $this->db->insert('po_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'po_id' => $id,
                
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => str_replace(".","",$this->input->post('price4')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal4'))
            );
            
            $this->db->insert('po_d',$data);
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
            $data=array(
                'po_id' => $id,
                
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => str_replace(".","",$this->input->post('price5')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal5'))
            );
            
            $this->db->insert('po_d',$data);
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
            $data=array(
                'po_id' => $id,
                
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => str_replace(".","",$this->input->post('price6')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal6'))
            );
            
            $this->db->insert('po_d',$data);
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
            $data=array(
                'po_id' => $id,
                
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => str_replace(".","",$this->input->post('price7')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal7'))
            );
            
            $this->db->insert('po_d',$data);
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
            $data=array(
                'po_id' => $id,
                
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => str_replace(".","",$this->input->post('price8')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal8'))
            );
            
            $this->db->insert('po_d',$data);
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
       if($this->input->post('itemcode9') and $qty9 > 0){
            $data=array(
                'po_id' => $id,
                
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                'harga' => str_replace(".","",$this->input->post('price9')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal9'))
            );
            
            $this->db->insert('po_d',$data);
        }
            
        $totalorder = $this->_getTotalOrder($id,'po_d','po_id');
            
        $data = array('totalactual' => $totalorder['totalorder']);
        $this->db->update('po',$data,array('id'=>$id));
        $this->db->update('po_order',array('status'=>$this->input->post('status')),array('id'=>$idorder));
        
       $this->db->query("call sp_po('$id','$empid')");
    }
    
    public function updateStatusPO(){
        if($this->input->post('p_id')){
            $row = array();
            
            $idlist = implode(",",array_values($this->input->post('p_id')));
            $status = $this->input->post('status');
            
            if($status == 'reject')$where = "`status` = 'order' and id in ($idlist) ";
            else $where = "id in ($idlist)";
            
            $row = $this->_countUpdate($where);
            if($row){
                $this->db->update('po_order',array('status'=>$status),$where);
                $this->session->set_flashdata('message','Update status PO successfully');
            }else{
                $this->session->set_flashdata('message','Nothing to update status PO!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing to update status PO!');
        }
    }
    private function _countUpdate($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('po_order');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function list_po($id)
    {
        $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,date_format(a.tgljatuhtempo,'%d-%b-%Y')as tgljatuhtempo,
                          format(a.totalactual,0)as ftotalactual,a.remark,a.createdby",false) 
            ->from('po a')
            ->where('po_order_id',$id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                //$rs['date_added'] = mdate("%d/%m/%Y", mysql_to_unix($rs['date_added']));
                $rs['details'] = $this->list_po_detail($rs['id']);
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function list_po_detail($id=0)
    {
        $q = $this->db->select("d.item_id,i.name,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.jmlharga,0)as fjmlharga",false)
        ->from('po_d d')->join('item i','d.item_id=i.id','left')->where('d.po_id',$id)->get();
        $details = array();
        
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $details[] = $rs;
            }
        }
        $q->free_result();
        return $details;
    }
    public function sum_po($id)
    {
        $data = array();
        $this->db->select("format(sum(a.totalactual),0)as ftotalactual",false)
            ->from('po a')->where('po_order_id',$id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
    
     
}?>