<?php
class MSchistory extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
	public function getSCHistory($tglawal, $tglakhir){
		$data = array();
		$qry = "
		SELECT 
			item.id
			, item.name as pname
			, IFNULL(dta.jml,0) AS jmla
			, IFNULL(dta.total,0) AS tota
			, IFNULL(dtb.jml,0) AS jmlb
			, IFNULL(dtb.total,0) AS totb
			, (IFNULL(dta.jml,0) + IFNULL(dtb.jml,0)) AS qty
			, (IFNULL(dta.total,0) + IFNULL(dtb.total,0)) AS hrg
		FROM item
		LEFT JOIN(
			SELECT pd.item_id, SUM(pd.qty)AS jml, SUM(pd.jmlharga)AS total
			FROM pinjaman_d pd
			LEFT JOIN pinjaman p ON pd.pinjaman_id = p.id
			WHERE p.tgl BETWEEN '$tglawal' AND '$tglakhir'
			GROUP BY pd.item_id
		) AS dta ON item.id = dta.item_id
		LEFT JOIN(
			SELECT rpd.item_id, SUM(rpd.qty)AS jml, SUM(rpd.jmlharga)AS total
			FROM retur_pinjaman_d rpd
			LEFT JOIN retur_pinjaman rp ON rpd.retur_pinjaman_id = rp.id
			WHERE rp.tgl BETWEEN '$tglawal' AND '$tglakhir'
			GROUP BY rpd.item_id
		) AS dtb ON item.id = dtb.item_id
		";
		$rs = $this->db->query($qry);
		//echo $this->db->last_query();
		if($rs->num_rows()>0){
			foreach($rs->result_array() as $row){
				$data[]=$row;
			}
		}
		$rs->free_result();
		return $data;
		
	}
}
?>