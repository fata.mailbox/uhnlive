<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ttp extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MTitipan'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('member_id','','');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('status','Status','callback__check_stockcard');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        $this->form_validation->set_rules('itemcode','','');
        $this->form_validation->set_rules('itemname','','');
        $this->form_validation->set_rules('price','','');
                
        if($this->form_validation->run()){
            if($this->input->post('status') == 'qoh'){
                $data['results'] = $this->MTitipan->getStockTitipan($this->input->post('member_id'));
                $data['total'] = $this->MTitipan->sumStockTitipan($this->input->post('member_id'));
            }else{
                // $data['results'] = $this->MTitipan->getStockCardTitipan($this->input->post('member_id'),$this->input->post('itemcode'),$this->input->post('fromdate'),$this->input->post('todate'));  // updated by Boby 20140730
                $data['results'] = $this->MTitipan->getStockCardTitipan($this->input->post('member_id'),$this->input->post('itemcode'),$this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('price'));
            }            
        }else{
            $data['results'] = false;
        }
        
        $data['reportDate'] = date("Y-m-d");
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'Balance Stock Stockiest Report';
        
        $this->load->view('inv/balance_titipan_index',$data);
    }
    public function _check_stockcard(){
        if($this->input->post('status') == 'sc' && !$this->input->post('itemcode')){
            $this->form_validation->set_message('_check_stockcard','browse product...');
            return false;
        }else{
            if($this->input->post('status') == 'sc' && !$this->input->post('member_id')){
                $this->form_validation->set_message('_check_stockcard','browse stockiest...');
                return false;
            }   
        }
        
        return true;
    }
}
?>