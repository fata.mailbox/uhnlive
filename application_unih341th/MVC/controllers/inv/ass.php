<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ass extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MAssembling'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
	
        $config['base_url'] = site_url().'inv/ass/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MAssembling->countAssembling($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MAssembling->searchAssembling($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MAssembling->countAssembling($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MAssembling->searchAssembling($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Assembling';
        $this->load->view('inv/assembling_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('condition','condition','');
        $this->form_validation->set_rules('itemcode','Item Code','required');
        $this->form_validation->set_rules('qty','Quantity','required|callback__check_qty');
        $this->form_validation->set_rules('itemname','','');
        $this->form_validation->set_rules('remark','','');
	$this->form_validation->set_rules('whsid','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MAssembling->addAssembling();
                $this->session->set_flashdata('message','Create assembling successfully');
            }
            redirect('inv/ass','refresh');
        }
	
        $data['warehouse'] = $this->MMenu->getDropDownWhs();        
        $data['page_title'] = 'Create Assembling';
        $this->load->view('inv/assembling_form',$data);
    }
    public function _check_qty(){
        $qty = str_replace(".","",$this->input->post('qty'));
        if($qty <=0){
            $this->form_validation->set_message('_check_qty','Quantity > 0 !');
            return false;
        }else{
			if($this->input->post('condition')=='No'){
				if($this->MAssembling->cekStokAssembling()>0){
					$this->form_validation->set_message('_check_qty','Need some pieces of this BOM !');
					return false;
				}
			}else{
				$jml = $this->MAssembling->cekStok();
				if($jml<$qty){
					$this->form_validation->set_message('_check_qty',"We only have $jml item for this manufacture !");
					return false;
				}
			}
		}
        return true;
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MAssembling->getAssembling($id);
        
        if(!count($row)){
            redirect('inv/ass','refresh');
        }
        $data['row'] = $row;
        $data['results'] = $this->MAssembling->getAssemblingDetail($id);
        $data['page_title'] = 'View Assembling';
        $this->load->view('inv/assembling_view',$data);
    }
    
}
?>