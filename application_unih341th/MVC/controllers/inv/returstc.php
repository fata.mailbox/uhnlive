<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Returstc  extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MReturtitipan'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'inv/returstc/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MReturtitipan->countReturTitipan($keywords);
        //echo $config['total_rows'];
        $this->pagination->initialize($config);
        $data['results'] = $this->MReturtitipan->searchReturTitipan($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Retur Stock Stockiest';
        $this->load->view('inv/returstockiest_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('member_id','Member','required');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('ewallet','','');
        $this->form_validation->set_rules('remark','','');
        $this->form_validation->set_rules('warehouse_id','','');
        $this->form_validation->set_rules('total','Total sales order','required|callback__check_min');
        $this->form_validation->set_rules('totalpv','','');
        
		/* Modified by Boby 20130409 */
		for($i=0;$i<=9;$i++){
			$this->form_validation->set_rules('itemcode'.$i,'','');
			$this->form_validation->set_rules('itemname'.$i,'','');
			$this->form_validation->set_rules('qty'.$i,'','');
			$this->form_validation->set_rules('price'.$i,'','');
			$this->form_validation->set_rules('subtotal'.$i,'','');
			$this->form_validation->set_rules('pv'.$i,'','');
			$this->form_validation->set_rules('subtotalpv'.$i,'','');
		}
		/* End modified by Boby 20130409 */
		
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                
                    $soid = $this->MReturtitipan->addReturTitipanTemp();
                    $results = $this->MReturtitipan->check_stock_titipan($soid,$this->input->post('member_id'));
                    if($results == 'ok'){
                        $this->MReturtitipan->addReturTitipan($soid);
                        $this->session->set_flashdata('message','Created Retur Stock Stockiest Successfully');
                        redirect('inv/returstc','refresh');        
                    }else{
                        $this->session->set_flashdata('message','Procces Cancel, Stock tidak mencukupi');
                        redirect('inv/returstc/create/','refresh');
                    }
            }
            redirect('inv/returstc','refresh');
        }
        $data['warehouse'] = $this->MReturtitipan->getDropDownWhs();
        $data['page_title'] = 'Create Retur Stock Stockiest';
        $this->load->view('inv/returstockiest_form',$data);
    }
    
    public function _check_min(){
        $amount = str_replace(".","",$this->input->post('total'));
        /*
		if($amount <= 0){
            $this->form_validation->set_message('_check_min','browse product...');
            return false;
        }
		*/
        return true;
    }
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MReturtitipan->getReturTitipan($id);
        
        if(!count($row)){
            redirect('inv/returstc','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MReturtitipan->getReturTitipanDetail($id);
        $data['page_title'] = 'View Retur Stock Stockiest';
        $this->load->view('inv/returstockiest_view',$data);
    }
    
}
?>