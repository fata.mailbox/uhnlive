<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Whs extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MWarehouse'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
	$config['base_url'] = site_url().'master/whs/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MWarehouse->countWarehouse($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MWarehouse->searchWarehouse($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MWarehouse->countWarehouse($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MWarehouse->searchWarehouse($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Warehouse';
        $this->load->view('master/warehouse_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('name','Warehouse Name','required');
        $this->form_validation->set_rules('address','','');
        $this->form_validation->set_rules('telp','','');
        $this->form_validation->set_rules('fax','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MWarehouse->addWarehouse();
                $this->session->set_flashdata('message','Create warehouse successfully');
            }
            redirect('master/whs','refresh');
        }
        
        $data['page_title'] = 'Create Warehouse';
        $this->load->view('master/warehouse_form',$data);
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MWarehouse->getWarehouse($id);
        
        if(!count($row)){
            redirect('master/whs','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Warehouse';
        $this->load->view('master/warehouse_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('name','Warehouse Name','required');
        $this->form_validation->set_rules('address','','');
        $this->form_validation->set_rules('telp','','');
        $this->form_validation->set_rules('fax','','');
        
        $row =array();
        $row = $this->MWarehouse->getWarehouse($id);
        
        if(!count($row)){
            redirect('master/whs','refresh');
        }
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MWarehouse->editWarehouse();
                $this->session->set_flashdata('message','Update warehouse successfully');
            }
            redirect('master/whs','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'Edit Warehouse';
        $this->load->view('master/warehouse_edit',$data);
    }
    
}
?>