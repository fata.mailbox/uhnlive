<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Supplier extends CI_Controller{
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MSupplier'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'master/supplier/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MSupplier->countSupplier($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MSupplier->searchSupplier($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Supplier';
        $this->load->view('master/supplier_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('name','Supplier Name','required');
        $this->form_validation->set_rules('address','','');
        $this->form_validation->set_rules('telp','','');
        $this->form_validation->set_rules('fax','','');
        $this->form_validation->set_rules('cp','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MSupplier->addSupplier();
                $this->session->set_flashdata('message','Create supplier successfully');
            }
            redirect('master/supplier','refresh');
        }
        
        $data['page_title'] = 'Create Supplier';
        $this->load->view('master/supplier_form',$data);
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MSupplier->getSupplier($id);
        
        if(!count($row)){
            redirect('master/supplier','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Supplier';
        $this->load->view('master/supplier_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('name','Supplier Name','required');
        $this->form_validation->set_rules('address','','');
        $this->form_validation->set_rules('telp','','');
        $this->form_validation->set_rules('fax','','');
        $this->form_validation->set_rules('cp','','');
        
        $row =array();
        $row = $this->MSupplier->getSupplier($id);
        
        if(!count($row)){
            redirect('master/supplier','refresh');
        }
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MSupplier->editSupplier();
                $this->session->set_flashdata('message','Update supplier successfully');
            }
            redirect('master/supplier','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'Edit Supplier';
        $this->load->view('master/supplier_edit',$data);
    }
    
}
?>