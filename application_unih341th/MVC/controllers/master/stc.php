<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stc extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MStockiest'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'master/stc/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MStockiest->countStockiest($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MStockiest->searchStockiest($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MStockiest->countStockiest($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MStockiest->searchStockiest($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Stockiest';
        $this->load->view('master/stockiest_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('no_stc','Stockiest Name','required|callback__check_nostc');
        $this->form_validation->set_rules('member_id','member id','required|callback__checkid');
        $this->form_validation->set_rules('name','','');
		
		// updated by Boby 20100614
		$this->form_validation->set_rules('member_id2','Leader','required');
		$this->form_validation->set_rules('no_stc2','','');
		$this->form_validation->set_rules('name2','','');
		$this->form_validation->set_rules('city','','');
		$this->form_validation->set_rules('kota_id','Kota','required');
        // end updated by Boby 20100614
		
		// updated by Boby 20140310
		$this->form_validation->set_rules('city_d','','');
		$this->form_validation->set_rules('kota_delivery','Kota Delivery','required');
        // end updated by Boby 20140310
		
		
		// updated by Boby 20100616
		$this->form_validation->set_rules('alamat','','');
		$this->form_validation->set_rules('telp','','');
		$this->form_validation->set_rules('hp','','');
		$this->form_validation->set_rules('email','Email','valid_email');
        // end updated by Boby 20100616
		
		$this->form_validation->set_rules('passwd','Password','required|min_length[8]|matches[passwd2]');
        $this->form_validation->set_rules('passwd2','Retype Password','');
        $this->form_validation->set_rules('pin','PIN','required|numeric|min_length[6]|matches[pin2]');
        $this->form_validation->set_rules('pin2','Retype PIN','');
        $this->form_validation->set_rules('type','','');
        $this->form_validation->set_rules('status','','');
        $this->form_validation->set_rules('warehouse_id','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MStockiest->addStockiest();
                $this->session->set_flashdata('message','Create stockiest successfully');
            }
            redirect('master/stc','refresh');
        }
        $data['type'] = $this->MStockiest->getDropDownType();
        $data['warehouse'] = $this->MStockiest->getDropDownWhs();
        $data['page_title'] = 'Create Stockiest';
        $this->load->view('master/stockiest_form',$data);
    }
    public function _checkid(){
        if($this->MStockiest->check_memberid($this->input->post('member_id'))){
            $this->form_validation->set_message('_checkid','Stockiest id available !');
            return false;
        }
        return true;
    } 
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MStockiest->getStockiest($id);
        
        if(!count($row)){
            redirect('master/stc','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Stockiest';
        $this->load->view('master/stockiest_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('id','','');
        $this->form_validation->set_rules('no_stc','Stockiest ID','required|callback__check_nostc2');
        $this->form_validation->set_rules('passwd','Password','callback__checkeditpassword');
        $this->form_validation->set_rules('pin','PIN','callback__checkeditpin');
        $this->form_validation->set_rules('type','','');
        $this->form_validation->set_rules('status','','');
		
		// updated by Boby 20100630
		$this->form_validation->set_rules('no_stc1','','');
        $this->form_validation->set_rules('type1','','');
		// end updated by Boby 20100630
		
		// updated by Boby 20140310
		$this->form_validation->set_rules('city_d','','');
		$this->form_validation->set_rules('kota_delivery','Kota Delivery','required');
        // end updated by Boby 20140310
		
		// updated by Boby 20100614
		$this->form_validation->set_rules('member_id2','Leader','required');
		$this->form_validation->set_rules('no_stc2','','');
		$this->form_validation->set_rules('name2','','');
		$this->form_validation->set_rules('city','','');
		$this->form_validation->set_rules('kota_id','Kota','required');
        // end updated by Boby 20100614
        
		// updated by Boby 20100616
		$this->form_validation->set_rules('alamat','','');
		$this->form_validation->set_rules('telp','','');
		$this->form_validation->set_rules('hp','','');
		$this->form_validation->set_rules('email','Email','valid_email');
        // end updated by Boby 20100616
		
        $row =array();
        $row = $this->MStockiest->getStockiest($id);
        
        if(!count($row)){
            redirect('master/stc','refresh');
        }
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MStockiest->editStockiest();
				// update by Boby 20100630
				$this->MStockiest->saveLogStc();
                // end update by Boby 20100630
				$this->session->set_flashdata('message','Update stockiest successfully');
            }
            redirect('master/stc','refresh');
        }
        $data['row'] = $row;
        $data['type'] = $this->MStockiest->getDropDownType();
        $data['warehouse'] = $this->MStockiest->getDropDownWhs();
        $data['page_title'] = 'Edit Stockiest';
        $this->load->view('master/stockiest_edit',$data);
    }
    public function _checkeditpassword(){
        if($this->input->post('passwd')){
            if(strlen($this->input->post('passwd') < 6)){
                $this->form_validation->set_message('_checkeditpassword','The password field must be at least 6 characters in length.');
                return false;
            }elseif($this->input->post('passwd') != $this->input->post('passwd2')){
                $this->form_validation->set_message('_checkeditpassword','Sorry, The password field does not match the retype password field.!');
                return false;
            }
        }
        return true;      
    }
    public function _check_nostc(){
        if($this->MStockiest->getCheckStockiest($this->input->post('no_stc'))){
            $this->form_validation->set_message('_check_nostc','No. STC sudah ada!');
            return false;
        }else{
			if($this->input->post('type')>1){
				if(substr_count(strtoupper($this->input->post('no_stc')), "-M")<1){
					$this->form_validation->set_message('_check_nostc','No. STC untuk Stockiest! Silakan beri "-M" setelah kode area..');
					return false;
				}
			}else{
				if(substr_count(strtoupper($this->input->post('no_stc')), "-M")>0){
					$this->form_validation->set_message('_check_nostc','No. STC untuk M-Stockiest!');
					return false;
				}
			}
		}
        return true;
    }
    
    public function _checkeditpin(){
        if($this->input->post('pin')){
            if(strlen($this->input->post('pin') < 6)){
                $this->form_validation->set_message('_checkeditpin','The PIN field must be at least 6 characters in length.');
                return false;
            }elseif($this->input->post('pin') != $this->input->post('pin2')){
                $this->form_validation->set_message('_checkeditpin','Sorry, The PIN field does not match the retype password field.!');
                return false;
            }
        }
        return true;      
    }
	
	// created by Boby 20100630
	public function _check_nostc2(){
		if($this->input->post('no_stc1') != $this->input->post('no_stc') || $this->input->post('type') != $this->input->post('type1')){
			if($this->MStockiest->getCheckStockiest($this->input->post('no_stc'))){
				$this->form_validation->set_message('_check_nostc2','No. STC sudah ada!');
				return false;
			}else{
				if($this->input->post('type')>1){
					if(substr_count(strtoupper($this->input->post('no_stc')), "-M")<1){
						$this->form_validation->set_message('_check_nostc2','No. STC untuk Stockiest! Silakan beri "-M" setelah kode area..');
						return false;
					}
				}else{
					if(substr_count(strtoupper($this->input->post('no_stc')), "-M")>0){
						$this->form_validation->set_message('_check_nostc2','No. STC untuk M-Stockiest!');
						return false;
					}
				}
			}
			return true;
		}
        
    }
	// end created by Boby 20100630
}
?>