<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Downloadmaster extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if($this->session->userdata('group_id') < 1 or $this->session->userdata('group_id') >= 100){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MDownload'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        $config['base_url'] = site_url().'downloadmaster/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MDownload->countDownload($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MDownload->searchDownload($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
            
        $data['page_title'] = 'Download Master';
        $this->load->view('master/downloadmaster_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('alias','file alias','required');
        $this->form_validation->set_rules('shortdesc','short description','required');
        $this->form_validation->set_rules('jenisfile','jenis file','');
        $this->form_validation->set_rules('status','','');
        
        if($this->form_validation->run()){
            $config['upload_path'] = './userfiles/download/';
            //$config['allowed_types'] = 'bin'; //khusus jika server linux
            $config['allowed_types'] = 'doc|xls|pdf|ppt|mp3|mpg|zip';
            $config['max_size']    = '10240'; //10 MB
            //$config['max_width']  = '1600';
            //$config['max_height']  = '1200';

            $tmp_ext = strtolower(substr($_FILES['userfile']['name'], strrpos($_FILES['userfile']['name'], '.')+1));
            if($tmp_ext)$config['allowed_types'] = substr($config['allowed_types'], strpos($config['allowed_types'], $tmp_ext), strlen($tmp_ext));

            $this->load->library('upload',$config);
            //print_r($this->upload->data());
            if ( ! $this->upload->do_upload())
            {
                $data['error'] = array('error' => $this->upload->display_errors());
                
                $data['page_title'] = 'Create Download';
                $this->load->view('master/downloadmaster_form',$data);
            }    
            else
            {
                $data = array('upload_data' => $this->upload->data());
                
                $temp = $this->upload->data();
                
                $filename = $temp['file_name'];
                $file_ext = $temp['file_ext'];
                
                $this->MDownload->addDownload($filename,$file_ext);
                
                $this->session->set_flashdata('message','Download Created');
                redirect('master/downloadmaster/','refresh');
            }
        }
        else{
            $data['page_title'] = 'Create Download';
            $this->load->view('master/downloadmaster_form',$data);
        }
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $data['row'] = $this->MDownload->getDownload($id);
            
        if(!count($data['row'])){
            redirect('master/downloadmaster/','refresh');
        }
        
        $data['page_title'] = 'Download Master';
        $this->load->view('master/downloadmaster_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('alias','file alias','required');
        $this->form_validation->set_rules('shortdesc','short description','required');
        $this->form_validation->set_rules('jenisfile','jenis file','');
        $this->form_validation->set_rules('status','','');
        
        if($this->form_validation->run()){
            
            $config['upload_path'] = './userfiles/download/';
            $config['allowed_types'] = 'doc|xls|pdf|ppt|mp3|mpg|zip';
            $config['max_size']    = '10240'; //10 MB
            //$config['max_width']  = '1600';
            //$config['max_height']  = '1200';

            $tmp_ext = strtolower(substr($_FILES['userfile']['name'], strrpos($_FILES['userfile']['name'], '.')+1));
            if($tmp_ext)$config['allowed_types'] = substr($config['allowed_types'], strpos($config['allowed_types'], $tmp_ext), strlen($tmp_ext));
            
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload()){
                if($_POST)$id=$this->input->post('id');
                $data['row'] = $this->MDownload->getDownload($id);
                
                if(!count($data['row'])){
                    redirect('master/downloadmaster/','refresh');
                }
            
                $error = array('error' => $this->upload->display_errors());
                $x = $error['error'];
                $y = "<p>You did not select a file to upload.</p>";
                if($x == $y){
                    $this->MDownload->updateDownload('','');
                
                    $this->session->set_flashdata('message','Download updated');
                    redirect('master/downloadmaster/','refresh');
                }
                $data['error'] = $error;
                $data['page_title'] = 'Edit Download';
                $this->load->view('master/downloadmaster_edit',$data);
            }else{
                $place = './userfiles/download/'.$this->input->post('namafile');
                if(file_exists($place)){
                    unlink($place);
                }
                $data = array('upload_data' => $this->upload->data());
                
                $temp = $this->upload->data();
                
                $filename = $temp['file_name'];
                $file_ext = $temp['file_ext'];
                $this->MDownload->updateDownload($filename,$file_ext);
                
                $this->session->set_flashdata('message','Download updated');
                redirect('master/downloadmaster/','refresh');
            }
        }else{
            if($_POST)$id=$this->input->post('id');
            $data['row'] = $this->MDownload->getDownload($id);
            
            if(!count($data['row'])){
                redirect('master/downloadmaster/','refresh');
            }
            
            $data['page_title'] = 'Edit Download';
            $this->load->view('master/downloadmaster_edit',$data);
        }
    }
}
?>