<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bank extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MBank'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'master/bank/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MBank->countBank($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MBank->searchBank($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MBank->countBank($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MBank->searchBank($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Bank';
        $this->load->view('master/bank_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('id','Bank ID','required|callback__check_id');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('accountno','','');
        $this->form_validation->set_rules('accountname','','');
        $this->form_validation->set_rules('accountarea','','');
        $this->form_validation->set_rules('status','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MBank->addBank();
                $this->session->set_flashdata('message','Create bank successfully');
            }
            redirect('master/bank','refresh');
        }
        
        $data['page_title'] = 'Create Bank';
        $this->load->view('master/bank_form',$data);
    }
    public function _check_id(){
        if($this->MBank->check_bankid($this->input->post('id'))){
            $this->form_validation->set_message('_check_id','Bank id available !');
            return false;
        }
        return true;
    } 
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MBank->getBank($id);
        
        if(!count($row)){
            redirect('master/bank','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Bank';
        $this->load->view('master/bank_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('name','Bank Name','required');
        $this->form_validation->set_rules('address','','');
        $this->form_validation->set_rules('telp','','');
        $this->form_validation->set_rules('fax','','');
        
        $row =array();
        $row = $this->MBank->getBank($id);
        
        if(!count($row)){
            redirect('master/bank','refresh');
        }
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MBank->editBank();
                $this->session->set_flashdata('message','Update bank successfully');
            }
            redirect('master/bank','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'Edit Bank';
        $this->load->view('master/bank_edit',$data);
    }
    
}
?>