<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if($this->session->userdata('group_id') < 1 or $this->session->userdata('group_id') >= 100){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MGallery'));
    }    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'master/gallery/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MGallery->countGallery($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MGallery->searchGallery($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
            
        $data['page_title'] = 'Photo Gallery';
        $this->load->view('master/gallery_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        ini_set('memory_limit', '256M');
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('title','title','');
        $this->form_validation->set_rules('shortdesc','short description','');
        $this->form_validation->set_rules('album','nama album','required');
        $this->form_validation->set_rules('cover','cover album','requred');
        $this->form_validation->set_rules('status','','');
        $this->form_validation->set_rules('nourut','no. urut','required|numeric');
        
        if($this->form_validation->run()){
            $config['upload_path'] = './userfiles/gallery_photo/';
            $config['allowed_types'] = 'gif|jpeg|jpg|png';
            $config['max_size']    = '10000'; //10 MB
            //$config['max_width']  = '1600';
            //$config['max_height']  = '1200';
            $config['remove_spaces']    = 'true';
            $config['overwrite']        = 'true';            
            
            $this->load->library('upload', $config);
            
            if($this->upload->do_upload()){
                $tp=$this->upload->data(); //ambil atribut data yang baru saja diupload
                
                $this->load->library('myresizer'); //load library
                $image= "userfiles/gallery_photo/".$tp['file_name']; //jangan lupa diset permissionnya
                
                //if($this->input->post('cover') == '1'){
                    $newimage= "userfiles/gallery_photo/t_".$tp['file_name']; //jangan lupa diset permissionnya
                    $newwidth=148; //width yang diinginkan
                    $newheight=120; //height yang diinginkan
                    $params = array(
                            'file' => $image,
                            'newfile'=> $newimage,
                            'width' => $tp['image_width'],
                            'height' => $tp['image_height'],
                            'newwidth' => $newwidth,
                            'newheight' => $newheight
                    );
                    $this->myresizer->resizecrop($params);
                    
                    /*
                    $newimage= "userfiles/gallery_photo/ft_".$tp['file_name']; //jangan lupa diset permissionnya
                    $newwidth=56; //width yang diinginkan
                    $newheight=44; //height yang diinginkan
                    
                    $params = array(
                            'file' => $image,
                            'newfile'=> $newimage,
                            'width' => $tp['image_width'],
                            'height' => $tp['image_height'],
                            'newwidth' => $newwidth,
                            'newheight' => $newheight
                    );
                    $this->myresizer->resizecrop($params);
                    */
                //}
                
                $newimage= "userfiles/gallery_photo/".$tp['file_name']; //jangan lupa diset permissionnya
                $newwidth=617; //width yang diinginkan
                $newheight=500; //height yang diinginkan
                
                $params = array(
                        'file' => $image,
                        'newfile'=> $newimage,
                        'width' => $tp['image_width'],
                        'height' => $tp['image_height'],
                        'newwidth' => $newwidth,
                        'newheight' => $newheight
                );
                $this->myresizer->resizecrop($params);
                    
                $filename = $tp['file_name'];
                $file_ext = $tp['file_ext'];
                
                $this->MGallery->addGallery($filename,$file_ext);
                
                $this->session->set_flashdata('message','Photo gallery sukses');
                redirect('master/gallery/','refresh');
            }else{
                $data['error'] = array('error' => $this->upload->display_errors());
                
                $data['page_title'] = 'Form Photo Gallery';
                $this->load->view('master/gallery_form',$data);
            }
        }else{
            $data['page_title'] = 'Form Photo Gallery';
            $this->load->view('master/gallery_form',$data);
        }
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $data['row'] = $this->MGallery->getGallery($id);
            
        if(!count($data['row'])){
            redirect('master/gallery/','refresh');
        }
        
        $data['page_title'] = 'View Photo Gallery';
        $this->load->view('master/gallery_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        ini_set('memory_limit', '256M');
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('title','title','');
        $this->form_validation->set_rules('shortdesc','short description','');
        $this->form_validation->set_rules('album','nama album','required');
        $this->form_validation->set_rules('cover','cover album','requred');
        $this->form_validation->set_rules('status','','');
        $this->form_validation->set_rules('nourut','No. Urut','required|numeric');
            
        if($this->form_validation->run()){
            $config['upload_path'] = './userfiles/gallery_photo/';
            $config['allowed_types'] = 'gif|jpeg|jpg|png';
            $config['max_size']    = '10000'; //10 MB
            //$config['max_width']  = '1600';
            //$config['max_height']  = '1200';
            $config['remove_spaces']    = 'true';
            $config['overwrite']        = 'true';            
            
            $this->load->library('upload', $config);
            
            if($this->upload->do_upload()){                
                $tp=$this->upload->data(); //ambil atribut data yang baru saja diupload
                
                if($this->input->post('namafile') != $tp['file_name'] and $this->input->post('namafile')){
                    $place = $_SERVER['DOCUMENT_ROOT']."userfiles/gallery_photo/".$this->input->post('namafile');
                    if(file_exists($place)){
                        unlink($place);
                    }
                    
                    $place = $_SERVER['DOCUMENT_ROOT']."userfiles/gallery_photo/t_".$this->input->post('namafile');
                    if(file_exists($place)){
                        unlink($place);
                    }                
                }
                $this->load->library('myresizer'); //load library
                $image= "userfiles/gallery_photo/".$tp['file_name']; //jangan lupa diset permissionnya
                
                //if($this->input->post('cover') == '1'){
                    $newimage2 = "userfiles/gallery_photo/t_".$tp['file_name']; //jangan lupa diset permissionnya
                    $newwidth=148; //width yang diinginkan
                    $newheight=120; //height yang diinginkan
                    $params = array(
                            'file' => $image,
                            'newfile'=> $newimage2,
                            'width' => $tp['image_width'],
                            'height' => $tp['image_height'],
                            'newwidth' => $newwidth,
                            'newheight' => $newheight
                    );
                    $this->myresizer->resizecrop($params);
                //}
                
                $newimage4 = "userfiles/gallery_photo/".$tp['file_name']; //jangan lupa diset permissionnya
                $newwidth=617; //width yang diinginkan
                $newheight=500; //height yang diinginkan
                
                $params = array(
                        'file' => $image,
                        'newfile'=> $newimage4,
                        'width' => $tp['image_width'],
                        'height' => $tp['image_height'],
                        'newwidth' => $newwidth,
                        'newheight' => $newheight
                );
                $this->myresizer->resizecrop($params);  
                
                $filename = $tp['file_name'];
                $file_ext = $tp['file_ext'];
                
                $this->MGallery->updateGallery($filename,$file_ext);
                
                $this->session->set_flashdata('message','Update photo gallery sukses');
                redirect('master/gallery/','refresh');
            }else{
                if($_POST)$id=$this->input->post('id');
                $data['row'] = $this->MGallery->getGallery($id);
                
                if(!count($data['row'])){
                    redirect('master/gallery/','refresh');
                }
            
                $error = array('error' => $this->upload->display_errors());
                $x = $error['error'];
                $y = "<p>You did not select a file to upload.</p>";
                if($x == $y){
                    $this->MGallery->updateGallery('','','');
                
                    $this->session->set_flashdata('message','Update Photo Gallery');
                    redirect('master/gallery/','refresh');
                }
                $data['error'] = $error;
                $data['page_title'] = 'Edit Photo Gallery';
                $this->load->view('master/gallery_edit',$data);
            }
        }else{
            if($_POST)$id=$this->input->post('id');
            $data['row'] = $this->MGallery->getGallery($id);
            
            if(!count($data['row'])){
                redirect('master/gallery/','refresh');
            }
            
            $data['page_title'] = 'Edit Photo Gallery';
            $this->load->view('master/gallery_edit',$data);
        }
    }
    
}
?>