<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class omsetrgn extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
            $this->load->model(array('MMenu','MRekap'));
    }

    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        
        if($this->form_validation->run()){
			/* Updated by Boby 2011-03-01 */
			//$data['rekap'] = $this->MRekap->omsetPerKota($this->input->post('fromdate'), $this->input->post('leader'));
			$data['rekap'] = $this->MRekap->omsetPerKota($this->input->post('fromdate'), $this->input->post('todate'), $this->input->post('leader'));
			/* End updated by Boby 2011-03-01 */
			$data['field_'] = $this->MRekap->periodeOmset($this->input->post('fromdate'));
        }else{
            $data['rekap']=false;
			$data['field_']=false;
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'Omset Per Regional';
        $this->load->view('order/omset_rgn_index',$data);
    }
    
}
?>