<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pjm_adm_his extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        $this->load->model(array('MMenu','MPinjamanadmin'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        $this->form_validation->set_rules('member_id','Stockiest No.','');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('name','','');
                
        if($this->form_validation->run()){
            $data['saldopinjaman'] = $this->MPinjamanadmin->list_saldopinjaman($this->input->post('member_id'));
            $data['totalsaldo'] = $this->MPinjamanadmin->sum_saldopinjaman($this->input->post('member_id'));
            $data['pinjamanhistory'] = $this->MPinjamanadmin->list_pinjamanhistory($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('member_id'));
            $data['totalhistory'] = $this->MPinjamanadmin->sum_pinjamanhistory($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('member_id'));
            $data['stock'] = $this->MPinjamanadmin->list_pinjaman_stock($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('member_id'));
        }

        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'Consignment History';
        $this->load->view('order/pinjaman_admin_history_index',$data);
    }
    
}
?>