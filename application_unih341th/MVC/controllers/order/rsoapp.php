<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rsoapp extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MRso'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'order/rsoapp/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MRso->countRequestSOMember($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MRso->searchRequestSOMember($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        if($this->session->userdata('group_id') == 103)$data['results2'] = $this->MRso->searchROTemp();
            
        $data['page_title'] = 'Request Sales Order Approved';
        $this->load->view('order/requestsalesorder_approved_index',$data);
    }
    
    public function _check_min(){
        $amount = str_replace(".","",$this->input->post('total'));
        if($amount <= 0){
            $this->form_validation->set_message('_check_min','browse product...');
            return false;
        }else{
            $row = $this->MRso->getEwallet($this->input->post('member_id'));
            if($amount > $row->ewallet){
                $this->form_validation->set_message('_check_min','Ewallet anda tidak mencukupi');
                return false;
            }
        }
        return true;
    }
    public function _check_pin(){
        if(!$this->MAuth->check_pin($this->session->userdata('username'),$this->input->post('pin'))){
            $this->form_validation->set_message('_check_pin','Sorry, your pin invalid');
            return false;
        }
        return true;
    }
    
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MRso->getRequestSOMember($id);
        
        if(!count($row)){
            redirect('order/rsoapp','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MRso->getRequestSOMemberDetail($id);
        $data['page_title'] = 'View Request Sales Order';
        $this->load->view('order/requestsalesorder_approved_view',$data);
    }
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('id','','');
        $this->form_validation->set_rules('member_id','Stockiest No.','required');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('nama','','');
        $this->form_validation->set_rules('soid','','');
        $this->form_validation->set_rules('remark','','');
        
        $row =array();
        $row = $this->MRso->getRSOApproved($id);
        
        if(!count($row)){
            redirect('order/rsoapp','refresh');
        }
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $soid = $this->input->post('soid');
                $member_id = $this->input->post('member_id');
                if($this->input->post('member_id') == '0'){
                    $this->MRso->requestOrderApproved();
                    $this->session->set_flashdata('message','Request Sales Order successfully');
                    redirect('order/rsoapp','refresh');        
                }else{
                    $results = $this->MRso->check_titipan_soapp($soid,$member_id);
                    if($results == 'ok'){
                        $this->MRso->requestOrderApproved();
                        $this->session->set_flashdata('message','Request Sales Order successfully');
                        redirect('order/rsoapp','refresh');        
                    }else{
                        $this->session->set_flashdata('message','Procces Cancel, Stock tidak mencukupi');
                        redirect('order/rsoapp/edit/'.$this->input->post('soid'),'refresh');
                    }
                }
            }
            redirect('order/rsoapp','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MRso->getRequestSOMemberDetail($id);
        $data['page_title'] = 'Approved Request Sales Order';
        $this->load->view('order/requestsalesorder_approved_form',$data);
    }
    
}
?>