<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class So extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MSo','MMaster'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'order/so/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MSo->countSO($keywords);
        //echo $config['total_rows'];
        $this->pagination->initialize($config);
        $data['results'] = $this->MSo->searchSO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Sales Order';
        $this->load->view('order/salesorder_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
		/*
		// Update by Boby 20140121
        $this->form_validation->set_rules('stc_id','','');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('namestc','','');
        */
		
        $this->form_validation->set_rules('date','Tanggal SO','required|callback__check_tglso');
        $this->form_validation->set_rules('member_id','Member','required');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('remark','','');
        // $this->form_validation->set_rules('total','Total repeat order','required|callback__check_min');
        $this->form_validation->set_rules('totalpv','','');
        $this->form_validation->set_rules('totalbv','','');
        $this->form_validation->set_rules('ewallet','','');
            
        $groupid = $this->session->userdata('group_id');
        if($groupid>100){
            $this->form_validation->set_rules('pin','PIN','required|callback__check_pin');
        }elseif($this->input->post('stc_id') == '0'){
			$this->form_validation->set_rules('total','Total repeat order','required|callback__check_min');
			$this->form_validation->set_rules('member_id','Member','required|callback__check_min');
			
            $this->form_validation->set_rules('city','','callback__check_delivery');
            $this->form_validation->set_rules('kota_id','','');
            $this->form_validation->set_rules('propinsi','','');
            $this->form_validation->set_rules('addr','','');
            $this->form_validation->set_rules('addr1','','');
            $this->form_validation->set_rules('timur','','');
            $this->form_validation->set_rules('pu','','');
			
            $this->form_validation->set_rules('cash','','');
            $this->form_validation->set_rules('debit','','');
            $this->form_validation->set_rules('credit','','');
            $this->form_validation->set_rules('totalbayar','Total Bayar','callback__check_bayar');
        }
        
		for($z=0; $z<=9; $z++){
			$this->form_validation->set_rules('itemcode'.$z,'','');
			$this->form_validation->set_rules('itemname'.$z,'','');
			$this->form_validation->set_rules('qty'.$z,'','');
			$this->form_validation->set_rules('price'.$z,'','');
			$this->form_validation->set_rules('price'.$z.'_','','');
			$this->form_validation->set_rules('subtotal'.$z,'','');
			$this->form_validation->set_rules('subtotal_'.$z,'','');
			$this->form_validation->set_rules('pv'.$z,'','');
			$this->form_validation->set_rules('subtotalpv'.$z,'','');
			$this->form_validation->set_rules('titipan_id'.$z,'','');
			$this->form_validation->set_rules('bv'.$z,'','');
			$this->form_validation->set_rules('subtotalbv'.$z,'','');
			$this->form_validation->set_rules('gprice'.$z,'','');
        }
		
        $this->form_validation->set_rules('total','','');
        $this->form_validation->set_rules('totalpv','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                if($groupid <= 100 && $this->input->post('stc_id') == '0'){
                    $this->MSo->addDirectSalesOrder();
					$this->MMaster->updateDeliveryAddress($soid);
                }else{
                    $soid = $this->MSo->addSalesOrderTemp();
					$sct_id = $this->input->post('stc_id');
					// $sct_id = 0;
					$results = $this->MSo->check_titipan($soid, $sct_id); 
					
                    if($results == 'ok'){
                        $this->MSo->addSalesOrderSTC($soid);
						$this->session->set_flashdata('message','Sales Order successfully');
                        redirect('order/so','refresh');
                    }else{
                        $this->session->set_flashdata('message','Procces Cancel, Stock tidak mencukupi');
                        redirect('order/so/create/','refresh');
                    }
                }
            }
            redirect('order/so','refresh');
        }
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-green.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['groupid'] = $groupid;
        $data['page_title'] = 'Create Sales Order';
        if($groupid>100)$this->load->view('order/salesorder_stc_form',$data);
        else $this->load->view('order/salesorder_form',$data);
    }
    
    public function _check_min(){
        
            $amount = str_replace(".","",$this->input->post('total'));
            if($amount <= 0){
                $this->form_validation->set_message('_check_min','browse product...');
                return false;
            }else{
                if($this->session->userdata('group_id') <= 100){
                    if($this->session->userdata('group_id') == 102 or $this->input->post('member_id') == '0'){
                        $row = $this->MSo->getEwallet($this->session->userdata('userid'));
                        if($amount > $row->ewallet){
                            $this->form_validation->set_message('_check_min','Ewallet anda tidak mencukupi');
                            return false;
                        }				
                    }

					else{
						$flag = 0;
						if($this->input->post('itemcode0')){$item[$flag]=$this->input->post('itemcode0'); $qty[$flag]=str_replace(".","",$this->input->post('qty0')); $itemname[$flag]=$this->input->post('itemname0');$flag+=1;}
						if($this->input->post('itemcode1')){$item[$flag]=$this->input->post('itemcode1'); $qty[$flag]=str_replace(".","",$this->input->post('qty1')); $itemname[$flag]=$this->input->post('itemname1');$flag+=1;}
						if($this->input->post('itemcode2')){$item[$flag]=$this->input->post('itemcode2'); $qty[$flag]=str_replace(".","",$this->input->post('qty2')); $itemname[$flag]=$this->input->post('itemname2');$flag+=1;}
						if($this->input->post('itemcode3')){$item[$flag]=$this->input->post('itemcode3'); $qty[$flag]=str_replace(".","",$this->input->post('qty3')); $itemname[$flag]=$this->input->post('itemname3');$flag+=1;}
						if($this->input->post('itemcode4')){$item[$flag]=$this->input->post('itemcode4'); $qty[$flag]=str_replace(".","",$this->input->post('qty4')); $itemname[$flag]=$this->input->post('itemname4');$flag+=1;}
			
						if($this->input->post('itemcode5')){$item[$flag]=$this->input->post('itemcode5'); $qty[$flag]=str_replace(".","",$this->input->post('qty5')); $itemname[$flag]=$this->input->post('itemname5');$flag+=1;}
						if($this->input->post('itemcode6')){$item[$flag]=$this->input->post('itemcode6'); $qty[$flag]=str_replace(".","",$this->input->post('qty6')); $itemname[$flag]=$this->input->post('itemname6');$flag+=1;}
						if($this->input->post('itemcode7')){$item[$flag]=$this->input->post('itemcode7'); $qty[$flag]=str_replace(".","",$this->input->post('qty7')); $itemname[$flag]=$this->input->post('itemname7');$flag+=1;}
						if($this->input->post('itemcode8')){$item[$flag]=$this->input->post('itemcode8'); $qty[$flag]=str_replace(".","",$this->input->post('qty8')); $itemname[$flag]=$this->input->post('itemname8');$flag+=1;}
						if($this->input->post('itemcode9')){$item[$flag]=$this->input->post('itemcode9'); $qty[$flag]=str_replace(".","",$this->input->post('qty9')); $itemname[$flag]=$this->input->post('itemname9');$flag+=1;}
			
						$temp = 0;$twin=0;
                                                $whsid = $this->session->userdata('whsid');
                                                
						for($i=0;$i<=$flag;$i++){
							if($temp>0){
								for($j=0;$j<$temp;$j++){
									if($brg[$j]==$item[$i]){//jika sama
										$twin = 1;
										$jml[$j]+=$qty[$i];
									}
								}
							}			
							if($twin==0){//jika tidak ada yang sama
								$jml[$temp]=$qty[$i];
								$brg[$temp]=$item[$i];
								$nama[$temp]=$itemname[$i];
								$temp+=1;
							}else{$twin=0;}
						}
						for($i=0;$i<count($jml)-1;$i++){
							$readystok=$this->MSo->cekStok($whsid, $brg[$i], $jml[$i]);
							if($readystok!='Ready'){$twin=1;$stok="We only have ".$readystok." ".$nama[$i]." on hand";}
						}
						if($twin > 0){
							$this->form_validation->set_message('_check_min','Out of order ( '.$stok.' )');
							return false;
						}
					}
                }
            }
        
        return true;
    }
    public function _check_bayar(){
        $amount = str_replace(".","",$this->input->post('total'));
        $bayar = str_replace(".","",$this->input->post('totalbayar'));
        
        $row = $this->MSo->getEwallet($this->input->post('member_id'));
        if($row){
            $saldo = $row->ewallet + $bayar;
            if($amount > $saldo){
                $this->form_validation->set_message('_check_bayar','Pembayaran tidak mencukupi');
                return false;
            }
        }
        return true;
    }
    public function _check_tglso(){
        if(date('Y-m-d',now()) > $this->input->post('date')){
		$allow = 0;
		
			if($this->session->userdata('group_id')>100){
	            $this->form_validation->set_message('_check_tglso','Tanggal sales order tidak boleh mundur');
				return false;
			}else{
				if($this->session->userdata('group_id')<100){
					$row=$this->MSo->get_blocked();
					if($this->input->post('date') <= $row->tglrunning){
						$this->form_validation->set_message('_check_tglso','Tanggal '.$row->tglrunning.' bonus sudah dijalankan');
						return false;
					}
				}
			}
        }
        return true;
    }
    
    public function _check_pin(){
        if(!$this->MAuth->check_pin($this->session->userdata('username'),$this->input->post('pin'))){
            $this->form_validation->set_message('_check_pin','Sorry, your pin invalid');
            return false;
        }
        return true;
    }
	
	public function _check_delivery(){
        if($this->input->post('pu')==1){
			if(strlen($this->input->post('city'))<1 OR strlen($this->input->post('addr'))<1){
				$this->form_validation->set_message('_check_delivery','Please fill the address.');
				return false;
			}
        }
        return true;
    }
	
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MSo->getSalesOrder($id);
        
        if(!count($row)){
            redirect('order/so','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MSo->getSalesOrderDetail($id);
		//update by Andrew 20120530
		if($this->session->userdata('group_id')>100){
			$data['flag']= 1;
		}else{
			$data['flag']=0;
		}
		//end update by Andrew 20120530
        $data['page_title'] = 'View Sales Order';
        $this->load->view('order/salesorder_view',$data);
    }
    
}
?>
