<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Freeso extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MFreeso'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        
        $config['base_url'] = site_url().'order/freeso/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MFreeso->countFreeSO($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MFreeso->searchFreeSO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Free Promotion Product';
        $this->load->view('order/promofree_index',$data);
    }
            
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MFreeso->getFreeSO($id);
        
        if(!count($row)){
            redirect('order/freeso','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MFreeso->getFreeSODetail($id);
        $data['page_title'] = 'View Free Product Promo';
        $this->load->view('order/promofree_view',$data);
    }
    
    public function approved($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('id','id','required');
        $this->form_validation->set_rules('whsid','','');
        $this->form_validation->set_rules('remark','','');
        
        $row =array();
        $row = $this->MFreeso->getFreeSOApproved($id);
        
        if(!count($row)){
            redirect('order/freeso','refresh');
        }
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MFreeso->freeSOApproved();
                $this->session->set_flashdata('message','Approved promotion product successfully');
            }
            redirect('order/freeso','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MFreeso->getFreeSODetail($id);
        $data['page_title'] = 'Approval Free Promotion Product';
        $this->load->view('order/promofree_approved_form',$data);
    }
    
    
}
?>