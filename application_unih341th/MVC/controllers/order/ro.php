<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ro extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MRo'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'order/ro/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MRo->countRO($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MRo->searchRO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        if($this->session->userdata('group_id') == 103)$data['results2'] = $this->MRo->searchROTemp();
            
        $data['page_title'] = 'Request Order Stockiest';
        $this->load->view('order/requestorder_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
		// Updated by Boby 20140325
		$disk = 0;
        if($this->session->userdata('group_id') == 102){
			$disk = 6;
		}elseif($this->session->userdata('group_id') == 103){
			$disk = 0;
		}
		
		if( $this->session->userdata('userid')=='00000005' || $this->session->userdata('userid')=='00000168'){
			$disk = 3;
		}
		$data["disk"] = $disk;
		// End updated by Boby 20140325
		
        $this->form_validation->set_rules('member_id','Stockiest','required');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('remark','','');
        $this->form_validation->set_rules('total','Total repeat order','required|callback__check_min');
        $this->form_validation->set_rules('totalpv','','');
        $this->form_validation->set_rules('pin','PIN','required|callback__check_pin');
		$this->form_validation->set_rules('diskon','','');	// Updated by Boby 20140325
		$this->form_validation->set_rules('payment','','');	// Updated by Boby 20140325
		$this->form_validation->set_rules('disc','','');	// Updated by Boby 20140325
        
        for($i=0;$i<10;$i++){
			$this->form_validation->set_rules('itemcode'.$i,'','');
			$this->form_validation->set_rules('itemname'.$i,'','');
			$this->form_validation->set_rules('qty'.$i,'','');
			$this->form_validation->set_rules('price'.$i,'','');
			$this->form_validation->set_rules('subtotal'.$i,'','');
			$this->form_validation->set_rules('pv'.$i,'','');
			$this->form_validation->set_rules('subtotalpv'.$i,'','');
			$this->form_validation->set_rules('diskon'.$i,'',''); // Updated by Boby 20140325
		}
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
				// Updated by Boby 20140325
				$this->MRo->addRequestOrder($disk);
                /* 
				if($this->input->post('member_id')=='0'){
                    $this->MRo->addRequestOrder($disk);
                }else{
                    $this->MRo->addRequestOrderTemp();
                }
				*/
				// End updated by Boby 20140325
                $this->session->set_flashdata('message','Request Order successfully');
            }
            redirect('order/ro','refresh');
        }
        $data['row'] = $this->MRo->getEwallet($this->session->userdata('userid'));
        $data['page_title'] = 'Create Request Order';
        $this->load->view('order/requestorder_form',$data);
    }
    
    public function _check_min(){
        $amount = str_replace(".","",$this->input->post('total'));
		$payment = str_replace(".","",$this->input->post('payment')); // created by Boby 20140407
		if($amount <= 0 or $payment <= 0){ // modified by Boby 20140407
            $this->form_validation->set_message('_check_min','browse product...');
            return false;
        }else{
			// Updated by Boby 20120319
			// $pay = round($amount * 0.03,0);
			$pay = round($amount * 0.97,0);
			$pay = str_replace(".","",$this->input->post('payment')); // created by Boby 20140407
			
            if($this->session->userdata('group_id') == 102 or $this->session->userdata('group_id') == 103 or $this->input->post('member_id') == '0'){
                $row = $this->MRo->getEwallet($this->session->userdata('userid'));
                if($pay > $row->ewallet){
                    $this->form_validation->set_message('_check_min','Ewallet anda tidak mencukupi');
                    return false;
                }
				//Updated by Boby (2009-11-18)
				else{
					$flag = 0;
					if($this->input->post('itemcode0')){$item[$flag]=$this->input->post('itemcode0'); $qty[$flag]=str_replace(".","",$this->input->post('qty0')); $itemname[$flag]=$this->input->post('itemname0');	$prc[$flag]=str_replace(".","",$this->input->post('price0'));	$flag+=1;}
					if($this->input->post('itemcode1')){$item[$flag]=$this->input->post('itemcode1'); $qty[$flag]=str_replace(".","",$this->input->post('qty1')); $itemname[$flag]=$this->input->post('itemname1');	$prc[$flag]=str_replace(".","",$this->input->post('price1'));	$flag+=1;}
					if($this->input->post('itemcode2')){$item[$flag]=$this->input->post('itemcode2'); $qty[$flag]=str_replace(".","",$this->input->post('qty2')); $itemname[$flag]=$this->input->post('itemname2');	$prc[$flag]=str_replace(".","",$this->input->post('price2'));	$flag+=1;}
					if($this->input->post('itemcode3')){$item[$flag]=$this->input->post('itemcode3'); $qty[$flag]=str_replace(".","",$this->input->post('qty3')); $itemname[$flag]=$this->input->post('itemname3');	$prc[$flag]=str_replace(".","",$this->input->post('price3'));	$flag+=1;}
					if($this->input->post('itemcode4')){$item[$flag]=$this->input->post('itemcode4'); $qty[$flag]=str_replace(".","",$this->input->post('qty4')); $itemname[$flag]=$this->input->post('itemname4');	$prc[$flag]=str_replace(".","",$this->input->post('price4'));	$flag+=1;}

					if($this->input->post('itemcode5')){$item[$flag]=$this->input->post('itemcode5'); $qty[$flag]=str_replace(".","",$this->input->post('qty5')); $itemname[$flag]=$this->input->post('itemname5');	$prc[$flag]=str_replace(".","",$this->input->post('price5'));	$flag+=1;}
					if($this->input->post('itemcode6')){$item[$flag]=$this->input->post('itemcode6'); $qty[$flag]=str_replace(".","",$this->input->post('qty6')); $itemname[$flag]=$this->input->post('itemname6');	$prc[$flag]=str_replace(".","",$this->input->post('price6'));	$flag+=1;}
					if($this->input->post('itemcode7')){$item[$flag]=$this->input->post('itemcode7'); $qty[$flag]=str_replace(".","",$this->input->post('qty7')); $itemname[$flag]=$this->input->post('itemname7');	$prc[$flag]=str_replace(".","",$this->input->post('price7'));	$flag+=1;}
					if($this->input->post('itemcode8')){$item[$flag]=$this->input->post('itemcode8'); $qty[$flag]=str_replace(".","",$this->input->post('qty8')); $itemname[$flag]=$this->input->post('itemname8');	$prc[$flag]=str_replace(".","",$this->input->post('price8'));	$flag+=1;}
					if($this->input->post('itemcode9')){$item[$flag]=$this->input->post('itemcode9'); $qty[$flag]=str_replace(".","",$this->input->post('qty9')); $itemname[$flag]=$this->input->post('itemname9');	$prc[$flag]=str_replace(".","",$this->input->post('price9'));	$flag+=1;}

					$temp = 0;$twin=0;
					$whsid = $this->session->userdata('whsid');
					
					for($i=0;$i<=$flag;$i++){
						if($temp>0){
							for($j=0;$j<$temp;$j++){
								if($brg[$j]==$item[$i]){//jika sama
									$twin = 1;
									$jml[$j]+=$qty[$i];
									//echo $qty[$i]."<br>";
								}
							}
						}			
						if($twin==0){//jika tidak ada yang sama
							$jml[$temp]=$qty[$i];
							$brg[$temp]=$item[$i];
							$price[$temp]=$prc[$i];
							$nama[$temp]=$itemname[$i];
							$temp+=1;
						}else{$twin=0;}
					}
					for($i=0;$i<count($jml)-1;$i++){
						//echo "<br><br>".$brg[$i]." = ".$jml[$i]." ".$nama[$i]." ";
						$readystok=$this->MRo->cekStok($whsid, $brg[$i], $price[$i], $jml[$i], 0);
						if($readystok!='Ready'){$twin=1;$stok="We only have ".$readystok." ".$nama[$i];}
						//echo "Stok = ".$readystok."<br>";
					}
					if($twin > 0){
						$this->form_validation->set_message('_check_min','Out of order ( '.$stok.' )');
						return false;
					}
				}
				//end Updated by Boby (2009-11-18)

            }
        }
        return true;
    }
    
    
    public function _check_pin(){
        if(!$this->MAuth->check_pin($this->session->userdata('username'),$this->input->post('pin'))){
            $this->form_validation->set_message('_check_pin','Sorry, your pin invalid');
            return false;
        }
        return true;
    }
    
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MRo->getRequestOrder($id);
        
        if(!count($row)){
            redirect('order/ro','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MRo->getRequestOrderDetail($id);
		//update by Andrew 20120530
		if($this->session->userdata('group_id')>100){
			$data['flag']= 1;
		}else{
			$data['flag']=0;
		}
		//end update by Andrew 20120530
        $data['page_title'] = 'View Request Order Stockiest';
        $this->load->view('order/requestorder_view',$data);
    }
    public function view2($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
			redirect('error','refresh');
		}
		
        $row = $this->MRo->getRequestOrderTemp($id);
        
        if(!count($row)){
			redirect('order/ro','refresh');
		}
		
        $data['row'] = $row;
        $data['items'] = $this->MRo->getRequestOrderDetailTemp($id);
        $data['page_title'] = 'View Request Order Stockiest';
        $this->load->view('order/requestorder_view',$data);
    }
    public function del(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'delete')){
            redirect('error','refresh');
        }
        
        if(!$this->MMenu->blocked()){
            $this->MRo->requestOrderTempDel();
        }
        redirect('order/ro/','refresh');
    }
    
}
?>