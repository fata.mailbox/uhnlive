<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Event_training extends CI_Controller {
    function __construct()
    {
	parent::__construct();
		$this->load->model('MFrontend');
	}
	
	public function index(){
		$this->load->library('pagination');
		
		//$data['news_archive'] = $this->MFrontend->list_archive('news');
		//$data['promo_archive'] = $this->MFrontend->list_archive('promo');
		$config['base_url'] = site_url().'news/event_training/';
		$config['per_page'] = 10;
		$config['uri_segment'] = 3;
		$config['total_rows'] = $this->MFrontend->count_list_news('event');
		$this->pagination->initialize($config);
		$data['results'] = $this->MFrontend->list_news('event','',$config['per_page'],$this->uri->segment($config['uri_segment']));
		
		$data['banner'] = $this->MFrontend->list_banner(50,0);
		$data['content'] = 'index/event_training';
		$this->load->view('index/index',$data);
	}
	
	public function detail($id=0){
		$row = $this->MFrontend->get_news($id);
		if(!count($row)){
			redirect('event_training/','refresh');
		}
		$data['row'] = $row;
		$this->MFrontend->update_countview($row['id']);
		
		$data['banner'] = $this->MFrontend->list_banner(50,0);
		$data['content'] = 'index/news_detail';
		$this->load->view('index/index',$data);
	}
}
