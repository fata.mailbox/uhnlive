<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class How_to_order extends CI_Controller {
    function __construct()
    {
		parent::__construct();
		$this->load->model(array('MFrontend'));        
    }
        
    public function index(){
	    
	    $data['banner'] = $this->MFrontend->list_banner(50,0);
		
	    $data['content']= 'index/how_to_order';
	    $this->load->view('index/index',$data,1);        
    }
    
}
?>