<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dailyreport extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('MBonus','MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('periode','','');
        if($this->form_validation->run()){
            $data['results']=$this->MBonus->list_active_member($this->input->post('periode'));
            $data['total']=$this->MBonus->total_active_member($this->input->post('periode'));
        }
        
        $data['dropdown'] = $this->MBonus->get_dropdown_daily();
        $data['page_title'] = 'Daily Report';
        $this->load->view('finance/daily_report',$data);
    }
        
}
?>