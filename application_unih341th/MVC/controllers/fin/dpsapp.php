<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dpsapp extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MDeposit'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'fin/dpsapp/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MDeposit->countDeposit($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MDeposit->searchDeposit($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MDeposit->countDeposit($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MDeposit->searchDeposit($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Deposit Confirmation Approval';
        $this->load->view('finance/deposit_approved_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('member_id','Member ID','required');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('flag','Ewalate','required|callback__check_stc');
        $this->form_validation->set_rules('transfer','','');
        $this->form_validation->set_rules('tunai','','');
        $this->form_validation->set_rules('debitcard','','');
        $this->form_validation->set_rules('creditcard','','');
        $this->form_validation->set_rules('total','amount transfer','required|callback__check_total');
        $this->form_validation->set_rules('remark','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MDeposit->addDepositApproved();
                $this->session->set_flashdata('message','Create deposit approved successfully');
            }
            redirect('fin/dpsapp','refresh');
        }

        $data['page_title'] = 'Create Deposit Approval';
        $this->load->view('finance/deposit_approved_create',$data);
    }
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('id','','');
        $this->form_validation->set_rules('amount','amount transfer','required|callback__check_amount');
        $this->form_validation->set_rules('remark','','');
        
        $row =array();
        $row = $this->MDeposit->getDepositApproved($id);
        
        if(!count($row)){
            redirect('fin/dpsapp','refresh');
        }
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MDeposit->approvedDeposit();
                $this->session->set_flashdata('message','Approved deposit successfully');
            }
            redirect('fin/dpsapp','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'Approved Deposit';
        $this->load->view('finance/deposit_approved_form',$data);
    }
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MDeposit->getDeposit($id);
        
        if(!count($row)){
            redirect('fin/dpsapp','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Deposit Approval';
        $this->load->view('finance/deposit_approved_view',$data);
    }
    public function _check_amount(){
        $amount = str_replace(".","",$this->input->post('amount'));
        if($amount < 1){
            $this->form_validation->set_message('_check_amount','Minimum transfer Rp. 10.000,-');
            return false;
        }
        return true;
    }
    public function _check_total(){
        $amount = str_replace(".","",$this->input->post('total'));
        if($amount < 1){
            $this->form_validation->set_message('_check_total','Minimum deposit > Rp. 1,-');
            return false;
        }
        return true;
    }
    public function _check_stc(){
        if($this->input->post('flag') == 'stc'){
            if($this->MDeposit->getStockiest()){
                $this->form_validation->set_message('_check_stc','Please select deposit ewallet member!');
                return false;
            }
        }
        return true;
    }  
    
}
?>