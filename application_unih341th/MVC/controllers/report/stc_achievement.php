<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stc_achievement extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('MSales','MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('tahun','','');
        $this->form_validation->set_rules('quart','','');
        
        if($this->form_validation->run()){
            $data['results']=$this->MSales->viewStcTarget($this->input->post('tahun'), $this->input->post('quart'));
			if(substr($this->input->post('quart'),0,2)=='01'){
				$data['head1']="January";$data['head2']="Febuary";$data['head3']="March";
			}elseif(substr($this->input->post('quart'),0,2)=='04'){
				$data['head1']="April";$data['head2']="May";$data['head3']="Juni";
			}elseif(substr($this->input->post('quart'),0,2)=='07'){
				$data['head1']="July";$data['head2']="August";$data['head3']="September";
			}elseif(substr($this->input->post('quart'),0,2)=='10'){
				$data['head1']="October";$data['head2']="November";$data['head3']="December";
			}
			// $data['head1']=substr($this->input->post('quart'),0,2);
            $data['thn']=$this->input->post('tahun');
			$data['total']=0;
        }else{
            $data['results']=false;
			$data['thn']=date("Y");
            $data['total']=false;
        }
        $data['dropdownyear'] = $this->MSales->get_year_report();
		$data['dropdownq'] = $this->MSales->get_q(0);
        $data['page_title'] = 'Stockiest Achievement';
        $this->load->view('report/stc_acv_list',$data);
    }
	
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
		$this->form_validation->set_rules('tahun','','');
		$this->form_validation->set_rules('quart','','');
		$this->form_validation->set_rules('stc_id','Stockiest ID','required');
		$this->form_validation->set_rules('namestc','','required');
		$this->form_validation->set_rules('no_stc','Stockiest ID','required');
        $this->form_validation->set_rules('target','Omset Target','required|greater_than[0]');
        $groupid = $this->session->userdata('group_id');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
				$period = $this->input->post('tahun');
				$period.= '-';
				$period.= $this->input->post('quart');
				$period = $this->MSales->get_end_date($period);
				$results = $this->MSales->cek_stc_target($period, $this->input->post('stc_id'));
				if($results == 'ok'){
					$target = str_replace('.','',$this->input->post('target'));
					$this->MSales->insert_stc_target($period, $this->input->post('stc_id'), $target);
					$this->session->set_flashdata('message','Create Target Successfully');
					redirect('report/stc_achievement/create/','refresh');
					
				}else{
					$this->session->set_flashdata('message','Data Already Exists');
					redirect('report/stc_achievement/create/','refresh');
				}
            }
            redirect('report/stc_achievement','refresh');
        }
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-green.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['groupid'] = $groupid;
		$data['dropdownyear'] = $this->MSales->get_y();
		$data['dropdownq'] = $this->MSales->get_bln();
		$data['dropdownreg'] = $this->MSales->get_region();
        $data['page_title'] = 'Create Stockiest Target';
        $this->load->view('master/stockiest_acv_form',$data);
    }   
}
?>