<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Monthly_report extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu', 'Report_model'));
	$this->load->library('export');
	        
    }
    public function index(){
	$this->create();
    }
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library('form_validation');
	
        
	$this->form_validation->set_rules('bulan','','');
	$this->form_validation->set_rules('tahun','','');
	$this->form_validation->set_rules('flag','','');
        $this->form_validation->set_rules('type_id','type','required');
	
        if($this->form_validation->run()){
	    $fname=$this->input->post('type_id');
	    $file = $fname."_".date('Ymd_His',now());
        
	    $sql = $this->Report_model->report_convert_to_excel($fname);
	    @$this->export->to_excel($sql, $file); 
	    
	    //$sql = $this->Report_model->report_convert_to_excel();
	    //$sql = $this->Report_model->report_convert_to_excel($fname);
	    //$this->export->to_excel($sql, $fname);

	    //$qry=$this->Report_model->report_convert_to_excel($fname);
	    redirect('','refresh');
		
	}else{
            $data['results']=false;
        }
        
        $data['page_title'] = 'Member Analysis Report';
        $this->load->view('report/monthly_report_xls',$data);
    }
	
	/* Created by Boby 20131112 */
    public function cetak($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
		$this->load->library(array('form_validation'));
        $data['results'] = $this->Report_model->emaildata($id);
		if($id==1){$data['fname'] = 'Stockiest';}
		else{$data['fname'] = 'MStockiest';}
        $this->load->view('member/emailStc',$data);
    }
	/* End created by Boby 20131112 */
	
}
?>