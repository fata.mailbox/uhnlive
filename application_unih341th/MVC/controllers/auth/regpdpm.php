<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Regpdpm extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MAuth'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('member_id','Member ID','trim|required');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MAuth->add_pdpm();
                $this->session->set_flashdata('message','Register complete..');
            }
            redirect('auth/Regpdpm/','refresh');
        }
        
        $data['page_title'] = 'PDPM Register';
        $this->load->view('auth/register_pdpm',$data);
    }
}
?>