<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if($this->session->userdata('group_id') < 1 or $this->session->userdata('group_id') >= 100){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MAuth'));
    }
        
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        
        $config['base_url'] = site_url().'auth/user/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MAuth->countUser($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MAuth->searchUser($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Daftar User';
        $this->load->view('auth/user_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('username','username','required|callback__checkusername');
        $this->form_validation->set_rules('name','name','required');
        $this->form_validation->set_rules('email','email','valid_email');
        $this->form_validation->set_rules('passwd','password','required||min_length[6]|matches[passwd2]');
        $this->form_validation->set_rules('passwd2','','');
        $this->form_validation->set_rules('group_id','','');
        $this->form_validation->set_rules('warehouse_id','','');
        $this->form_validation->set_rules('status','','');
        
        if($this->form_validation->run()){
            $this->MAuth->addUser();
            $this->session->set_flashdata('message','user sukses');
            redirect('auth/user/','refresh');
        }
        $data['group'] = $this->MAuth->groupUserDropDown();
        $data['warehouse'] = $this->MAuth->getDropDownWhs();        
        $data['page_title'] = 'Form User';
        $this->load->view('auth/user_form',$data);
    }
    public function _checkusername(){
        if($this->MAuth->check_username($this->input->post('username'))){
            $this->form_validation->set_message('_checkusername','Username sudah terdaftar!');
            return false;
        }
        return true;      
        
    }
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $data['row'] = $this->MAuth->getUser($id);
            
        if(!count($data['row'])){
            redirect('auth/user/','refresh');
        }
        
        $data['page_title'] = 'View User';
        $this->load->view('auth/user_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('username','username','required|callback__checkeditusername');
        $this->form_validation->set_rules('name','name','required');
        $this->form_validation->set_rules('email','email','valid_email');
        $this->form_validation->set_rules('passwd','password','callback__checkeditpassword');
        $this->form_validation->set_rules('group_id','','');
        $this->form_validation->set_rules('warehouse_id','','');
        $this->form_validation->set_rules('status','','');
        $this->form_validation->set_rules('passwd2','','');
        
        if($this->form_validation->run()){
            $this->MAuth->editUser();
            $this->session->set_flashdata('message','Update user');
            redirect('auth/user','refresh');
        }else{
            $data['row'] = $this->MAuth->getUser($id);
            
            if(!count($data['row'])){
                redirect('auth/user/','refresh');
            }
            
            $data['group'] = $this->MAuth->groupDropDown();
            $data['warehouse'] = $this->MAuth->getDropDownWhs();        
            $data['page_title'] = 'Edit User';
            $this->load->view('auth/user_edit',$data);
        }
    }
    
    public function _checkeditusername(){
        if($this->MAuth->check_editusername($this->input->post('username'),$this->input->post('id'))){
            $this->form_validation->set_message('_checkeditusername','Username sudah terdaftar!');
            return false;
        }
        return true;      
    }
    public function _checkeditpassword(){
        if($this->input->post('passwd')){
            if(strlen($this->input->post('passwd')) < 6){
                $this->form_validation->set_message('_checkeditpassword','Bagian password minimal 6 digit.');
                return false;
            }elseif($this->input->post('passwd') != $this->input->post('passwd2')){
                $this->form_validation->set_message('_checkeditpassword','Bagian password tidak sama dengan ulangi password!');
                return false;
            }
        }
        return true;      
    }
}
?>