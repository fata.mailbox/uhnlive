<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group extends CI_Controller {
    function __construct()
    {
	parent::__construct();
	if($this->session->userdata('group_id') < 1 or $this->session->userdata('group_id') >= 100){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MAuth'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages','pagination'));
        
        $this->form_validation->set_rules('search','','');
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        
        $config['base_url'] = site_url().'auth/group/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MAuth->countGroup($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MAuth->searchGroup($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Group User';
        $this->load->view('auth/group_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('groupid','group id','required|numeric|callback__cekid');
        $this->form_validation->set_rules('groupname','','required');
        $this->form_validation->set_rules('groupname','','required');
        $this->form_validation->set_rules('viewSltdLstId','','');
        $this->form_validation->set_rules('viewLstId','','');
        
        if($this->form_validation->run()){
            $this->MAuth->addGroup();
            $this->session->set_flashdata('message','Group user sukses');
            redirect('auth/group/','refresh');
        }
        $data['results'] = $this->MAuth->all_module('');
        $data['page_title'] = 'Form Group User';
        $this->load->view('auth/group_form',$data);
    }
    public function _cekid(){
        if($this->input->post('groupid') > 255){
            $this->form_validation->set_message('_cekid','Group id maximum 255');
            return false;
        }else{
            if($this->MAuth->_cekid($this->input->post('groupid'))){
                    $this->form_validation->set_message('_cekid','Group user sudah terdaftar!');
                    return false;
            }
        }
        return true;      
        
    }
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $data['row'] = $this->MAuth->getModule($id);
            
        if(!count($data['row'])){
            redirect('auth/group/','refresh');
        }
        
        $data['page_title'] = 'View Group User';
        $this->load->view('auth/group_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('groupid','group name','required');
        $this->form_validation->set_rules('groupname','group name','required');
            
        if($this->form_validation->run()){
            $this->MAuth->editGroup();
            $this->session->set_flashdata('message','Update group user');
            redirect('auth/group','refresh');
        }else{
            
            if($_POST)$id=$this->input->post('groupid');
            $data['group'] = $this->MAuth->_cekid($id);
            
            if(!$data['group']){
                redirect('auth/group/','refresh');
            }
            
            $data['results'] = $this->MAuth->all_module($id);
            $data['results2'] = $this->MAuth->getModule($id);
            $data['page_title'] = 'Edit Group User';
            $this->load->view('auth/group_edit',$data);
        }
    }
    public function editauth($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('groupid','group name','required');
           
        if($this->form_validation->run()){
            $this->MAuth->editAuthoruty();
            $this->session->set_flashdata('message','Update group user');
            redirect('auth/group','refresh');
        }else{
            
            if($_POST)$id=$this->input->post('groupid');
            $data['group'] = $this->MAuth->_cekid($id);
            
            if(!$data['group']){
                redirect('auth/group/','refresh');
            }
            
            $data['results'] = $this->MAuth->getModuleAuth($id);
            $data['page_title'] = 'Edit Authority Group';
            $this->load->view('auth/group_authority',$data);
        }
    }
}
?>