<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cancelso extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MAuth','MSearchadmin'));
    }
    
	public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','pagination'));
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4)) $this->session->unset_userdata('keywords');
        }
        
		$config['base_url'] = site_url().'auth/cancelso/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
		$data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
		
		$keywords = $this->session->userdata('keywords');
		// echo $keywords;
        $config['total_rows'] = $this->MAuth->countCancelSo($keywords);
		$this->pagination->initialize($config);
		$data['base_url'] = $config['base_url'];
		$data['results'] = $this->MAuth->getCancelSo($keywords, $config['per_page'], $this->uri->segment($config['uri_segment']));
		
        $data['page_title'] = 'SO Canceled';
        $this->load->view('auth/cancelso_list',$data);
    }
    
	
    public function create($id=0){
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('so_id','','');
        
        if($this->form_validation->run()){
			$row = $this->MAuth->getInfoSo($this->input->post('so_id'));
			$data['row'] = $row;
			
			if(!count($row)){
				$this->session->set_flashdata('message','SO.'.$this->input->post('so_id').' not found');
				redirect('auth/cancelso/create','refresh');
			}else{
				if($row['note']==0){
					if($_POST['submit'] == 'Submit'){
						if(!$this->MMenu->blocked()){
							$this->MAuth->cancelSo();
							$this->session->set_flashdata('message','SO has been canceled');
						}
						redirect('auth/cancelso','refresh');
					}
				}else if($row['note']==1){
					$this->session->set_flashdata('message','SO.'.$this->input->post('so_id').' cannot canceled twice.');
					redirect('auth/cancelso/create','refresh');
				}else if($row['note']==2){
					$this->session->set_flashdata('message','SO.'.$this->input->post('so_id').' is canceled SO.');
					redirect('auth/cancelso/create','refresh');
				}else if($row['note']==3){
					$this->session->set_flashdata('message','Cannot canceled closed transaction.');
					redirect('auth/cancelso/create','refresh');
				}else if($row['note']==4){
					$this->session->set_flashdata('message','Starter Kit cannot canceled.');
					redirect('auth/cancelso/create','refresh');
				}else{
					redirect('auth/cancelso/create','refresh');
				}
			}
        }
        
        $data['items'] = false;
        $data['page_title'] = 'Canceling SO'; // $row['note']; // print_r($row); // 
        $this->load->view('auth/cancelso_form',$data);
    }
}
?>