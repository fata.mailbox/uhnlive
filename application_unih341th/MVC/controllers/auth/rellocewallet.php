<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rellocewallet extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MAuth','MSearchadmin', 'MEwalletrelocation'));
    }
    
	public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','pagination'));
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4)) $this->session->unset_userdata('keywords');
        }
        
		$config['base_url'] = site_url().'auth/rellocewallet/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
		$data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
		
		$keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MEwalletrelocation->countGrantEwallet($keywords);
		$this->pagination->initialize($config);
		$data['base_url'] = $config['base_url'];
		$data['results'] = $this->MEwalletrelocation->getGrantEwallet($keywords, $config['per_page'], $this->uri->segment($config['uri_segment']));
		
        $data['page_title'] = 'Relocation Ewallet Member';
        $this->load->view('auth/relocewallet',$data);
    }
    
	
    public function create(){
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('member_id0','Member ID','trim|required');
        $this->form_validation->set_rules('name0','','');
        $this->form_validation->set_rules('member_id1','Upline ID','trim|required');
        $this->form_validation->set_rules('name1','','');
		$this->form_validation->set_rules('type','','');
				
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MEwalletrelocation->createGrant();
                $this->session->set_flashdata('message','Register complete..');
            }
            redirect('auth/rellocewallet/','refresh');
        }
        $data['type'] = $this->MEwalletrelocation->getGrantType();
        $data['page_title'] = 'Create Relocation Ewallet';
        $this->load->view('auth/relocewallet_create',$data);
    }
}
?>