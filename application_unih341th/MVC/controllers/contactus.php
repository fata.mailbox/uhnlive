<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contactus extends CI_Controller {
    function __construct()
    {
		parent::__construct();
		$this->load->model(array('MFrontend'));        
    }
        
    public function index(){
	    
	    $data['testi'] = $this->MFrontend->list_testimonial('Y',10,0);
	    $data['content']= 'index/contactus';
	    $this->load->view('index/index',$data,1);        
}

public function cform(){
	$this->load->helper(array('captcha'));
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('firstname','first name','trim|required|min_length[3]');
        $this->form_validation->set_rules('lastname','last name','trim|required');
        $this->form_validation->set_rules('email','email','required|valid_email');
        $this->form_validation->set_rules('phone','phone','trim|numeric|min_length[10]|required');
        $this->form_validation->set_rules('subject','subject','trim|required');
        $this->form_validation->set_rules('message','message','trim|required');
        $this->form_validation->set_rules('confirmCaptcha','security code','required|callback_check_captcha');
        
        if($this->form_validation->run()){
            $this->send_mail();
            $this->session->set_flashdata('message','Your message send successfully, thank you');
            redirect('contactus/','refresh');
        }else{
	    $vals = array(
		'img_path'	 => './captcha/',
		'img_url'	 => base_url().'captcha/',
		'font_path'     => './unih341th_system_files/fonts/texb.ttf',			
		'img_width'	 => '140',
		'img_height' => '40',
		'expiration' => 3600
		);
	    
	    $cap = create_captcha($vals);
	    
	    $data['captcha'] = $cap;
	    $this->session->set_userdata(array('captchaWord'=> $cap['word']));
	    $data['testi'] = $this->MFrontend->list_testimonial('Y',10,0);
	    $data['content']= 'index/contactus_form';
	    $this->load->view('index/index',$data,1);
        }
}

public function check_captcha($confirmCaptcha){
    if ($this->_check_captcha($confirmCaptcha) == 0)
    {
	$this->form_validation->set_message('check_captcha','The confirm security code is not available');
	return false;
    }
    return true;
}
protected function _check_captcha($confirmCaptcha){
    $captchaWord = $this->session->userdata('captchaWord');
    
    $this->session->unset_userdata('captchaWord');
    if(strcasecmp($captchaWord, $confirmCaptcha) == 0){
	return true;
    }
    return false;
}

function send_mail()
{
    $this->load->library(array('form_validation','messages'));
    $this->load->library('email');
        
    $this->email->from($this->input->post('email'), $this->input->post('firstname')." ".$this->input->post('lastname'));
    $this->email->to('info@uni-health.com');
    //$this->email->cc('qtakwa@yahoo.com@yahoo.com');
    //$this->email->bcc('info@smartindo-technology.com');
    $this->email->subject($this->input->post('subject'));
    
    $message = "Telephone Number: ".$this->input->post('phone')." Message: ".$this->input->post('message');
    
    $this->email->message($message);
    $this->email->send();
}
    /*Modified by Boby (2009-12-15)*/
	public function region($id){
		$data['isi'] = $id;
		$data['content']= 'index/contactus';
        $this->load->view('index/index',$data,1);
	}
    /*End modified by Boby (2009-12-15)*/

    
}
?>