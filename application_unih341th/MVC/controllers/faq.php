<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Faq extends CI_Controller {
    function __construct()
    {
	parent::__construct();
	$this->load->model(array('MFrontend'));  // modified by Takwa 2013
    }
	
    public function index(){
	    $data['banner'] = $this->MFrontend->list_banner(50,0);	// modified by Takwa 2013
	    $data['content'] = 'index/faq';
	    $this->load->view('index/index',$data);
    }
	
}?>
