<?php // INSERT INTO `menu`(`id`,`submenu_id`,`title`,`folder`,`url`,`comment`,`sort`)VALUES(NULL,'4','Print Out Address','member','printout','','12');
// side margin = 0 top = 0.1 Bottom = 0.5
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Printout extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        $this->load->model(array('MMenu','MPrintout'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        
        // if($this->form_validation->run()){
			$data['addr'] = $this->MPrintout->getPrintoutAddress();
        // }
		
        /*
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        */
		
        $data['page_title'] = 'Data Print Address';
        $this->load->view('member/printout_address',$data);
    }    
}
?>