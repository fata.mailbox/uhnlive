<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Activity extends CI_Controller {
    function __construct(){
		parent::__construct();
        if(!$this->session->userdata('logged_in')){ redirect('','refresh'); }
        $this->load->model(array('MMenu','MTree','MSalesreport','MBonus'));
    }
    
    public function index(){
        redirect('member/activity/view/','refresh');
    }
	
    public function view(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('member_id','','');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('periode','','');
        
        $this->form_validation->run();
        
        if($this->uri->segment(4))$memberid = $this->uri->segment(4);
        elseif($this->input->post('member_id'))$memberid = $this->input->post('member_id');
        else $memberid = $this->session->userdata('userid');

        $row=$this->MTree->get_member($memberid);
        if($row){
            if($memberid == $this->session->userdata('userid') or $this->session->userdata('group_id') < 100)$check_crosline = 'ok';
            else $check_crosline = $this->MTree->check_upline($memberid,$this->session->userdata('userid'));

            if($check_crosline == 'ok'){
                if($this->input->post('periode')){
					if($this->input->post('periode') == ''){
						$periode = date('Y-m');
					}else{
						$periode = $this->input->post('periode');
					}
                    $data['result'] = $this->MSalesreport->get_tree($periode,$row['auto_id'],$row['parentid'],$row['lft'],$row['rgt'],$row['level']);
					$data['hplink'] = 'member/activity/download/'.$memberid.'/'.$this->input->post('periode');
					// echo anchor('fin/dpsapp/create','create deposit approved');
                }else{
					if($this->input->post('periode') == ''){
						$periode = date('Y-m');
					}else{
						$periode = $this->input->post('periode');
					}
                    $data['result'] = $this->MSalesreport->get_tree($periode,$row['auto_id'],$row['parentid'],$row['lft'],$row['rgt'],$row['level']);
					$data['hplink'] = 'member/activity/download/'.$memberid.'/'.$periode;
                }
            }
        }
        
        $data['periode'] = $this->MTree->dropdown_periode();
        $data['page_title'] = 'Activity Report';
        $this->load->view('member/treeActivity',$data);
    }
	
	public function download($memberid, $periode){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
		$this->load->library(array('form_validation'));
		
		$row=$this->MTree->get_member($memberid);
        $data['results'] = $this->MSalesreport->get_tree($periode,$row['auto_id'],$row['parentid'],$row['lft'],$row['rgt'],$row['level']);
		$data['bns']=$this->MSalesreport->getBonus($memberid, $periode);
		
        $cd = strtotime(date("Y-m-d"));
		$bln = date('F Y', mktime(0,0,0,date('m',$cd)-1,date('d',$cd),date('Y',$cd)));
		
		$data['temp']=1;
		$data['page_title'] = 'UNIHEALTH ACTIVITY REPORT';
		$data['periode'] = $periode;
		$data['mid'] = $memberid;
        $this->load->view('member/activity_report',$data);
    }
}
?>