<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bank extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){ 
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MProfiles'));
		$this->load->model(array('MMenu','MProfile'));
    }
	public function index(){
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
			redirect('error','refresh');
		}
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('member_id','Member ID','required|min_length[8]');
        $this->form_validation->set_rules('nasabah','Nama nasabah','required');
		$this->form_validation->set_rules('norek','Nomor rekening','required');
		$this->form_validation->set_rules('area','Area / cabang','required');
        
        $row =array(); 
		$data["databank"] = $this->MProfiles->getBank($member_id);
		$data['bank'] = $this->MProfile->getDropDownBank();
		$data['page_title'] = 'Edit Bank Account';
        $this->load->view('member/vbank',$data);
	}
	public function edit($member_id){
		if($this->session->userdata('group_id')>100){
			redirect('error','refresh');
		}
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('member_id','Member ID','required|min_length[8]');
        $this->form_validation->set_rules('nasabah','Nama nasabah','required');
		$this->form_validation->set_rules('norek','Nomor rekening','required|min_length[10]');
		$this->form_validation->set_rules('area','Area / cabang','required');
		$this->form_validation->set_rules('flag','flag','required');
        
		if($this->form_validation->run()){
			$this->MProfiles->saveBank();
			$this->session->set_flashdata('message','Update bank account successfully');
			redirect('member/bank/edit/'.$member_id,'refresh');
		}
        $row =array(); 
		$data["databank"] = $this->MProfiles->getBank($member_id);
		$data['bank'] = $this->MProfile->getDropDownBank();
		$data['page_title'] = 'Edit Bank Account';
        $this->load->view('member/vbank',$data);
	}
}
?>