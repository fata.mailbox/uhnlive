<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Memprofile extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MProfile'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        
        $config['base_url'] = site_url().'member/memprofile/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MProfile->countSearchMember($this->input->post('option'),$keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MProfile->searchMember($this->input->post('option'),$keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Search Profile Member';
        $this->load->view('member/member_profile_index',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('noktp','No. KTP','required|min_length[7]');
        $this->form_validation->set_rules('alamat','Alamat','required');
        $this->form_validation->set_rules('kodepos','Kode Pos','required|min_length[5]|numeric');
        $this->form_validation->set_rules('city','Kota','required');
        $this->form_validation->set_rules('email','Email','valid_email');
        $this->form_validation->set_rules('tempatlahir','Tempat Lahir','required');
        $this->form_validation->set_rules('jk','','');
        $this->form_validation->set_rules('date','','');
        $this->form_validation->set_rules('hp','','');
        $this->form_validation->set_rules('telp','','');
        $this->form_validation->set_rules('fax','','');
        $this->form_validation->set_rules('kota_id','','');
        $this->form_validation->set_rules('propinsi','','');
        $this->form_validation->set_rules('ahliwaris','','');
        $this->form_validation->set_rules('npwp','','callback__cek_tgl'); // modified by Boby 20100715
        $this->form_validation->set_rules('bank_id','','');
        $this->form_validation->set_rules('namanasabah','Nama Nasabah','trim|required');
        $this->form_validation->set_rules('norek','No. Rekening','trim|required');
        $this->form_validation->set_rules('area','Cabang','trim|required');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MProfile->updateProfile();
                $this->session->set_flashdata('message','Update your profile successfully');
            }
            redirect('member/memprofile/','refresh');
        }
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-green.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
            
        $row = $this->MProfile->getProfile($id);
        if(!count($row)){
            redirect('error','refresh');
        }
        $data['row'] = $row;
        $data['bank'] = $this->MProfile->getDropDownBank();
        $data['page_title'] = 'Edit Profile Member';
        
        $this->load->view('member/member_profile_edit',$data,1);
    }
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MProfile->getProfile($id);
        if(!count($row)){
            redirect('error','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Profile Member';
        $this->load->view('member/member_profile_view',$data);
    }
    public function banned($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('banned','Banned','callback__check_banned');
        $this->form_validation->set_rules('remark','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MProfile->updateBanned();
            }
            redirect('member/memprofile/','refresh');
        }
        
        $row =array();
        $row = $this->MProfile->getBannedMember($id);
        if(!count($row)){
            redirect('error','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'Banned Profile Member';
        $this->load->view('member/member_profile_banned_form',$data);
    }
    public function _check_banned(){
        if(!$this->input->post('banned')){
            $this->form_validation->set_message('_check_banned','Pilih banned !');
            return false;
        }
        return true;
    }
	
	// Created by Boby 20100715
	public function _cek_tgl(){
		if($this->input->post('npwp')){
			$bln = date('m', now())-1;
			$bnsPeriode = $this->MProfile->cekBonusPeriode();
			if($bnsPeriode!=$bln){
				$this->form_validation->set_message('_cek_tgl','Cannot change NPWP for this time !');
				return false;
			}
		}
	}
	// End created by Boby 20100715    
}
?>