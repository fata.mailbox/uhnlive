<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tree extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){ 
            redirect('','refresh');
        }
                
        $this->load->model(array('MMenu','MTree'));
    }
    
    public function index(){
        redirect('member/tree/view/','refresh');
    }
    public function view(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('member_id','','');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('periode','','');
        
        $this->form_validation->run();
        
        if($this->uri->segment(4))$memberid = $this->uri->segment(4);
        elseif($this->input->post('member_id'))$memberid = $this->input->post('member_id');
        else $memberid = $this->session->userdata('userid');

        $row=$this->MTree->get_member($memberid);
        if($row){
            if($memberid == $this->session->userdata('userid') or $this->session->userdata('group_id') < 100)$check_crosline = 'ok';
            else $check_crosline = $this->MTree->check_upline($memberid,$this->session->userdata('userid'));
            //echo $check_crosline."aa";

            if($check_crosline == 'ok'){
                //$show_level=10;
                //if($this->input->post('level') == '')$show_level=10;
                //else $show_level=$this->input->post('level');
                /*
                $row2 = $this->MTree->get_jumlah_level($show_level,$row['lft'],$row['rgt'],$row['level']);
                
                if($row2['0']['level'] == 1)$data['level_1'] = $row2['0']['jml'];
                else $data['level_1'] = "0";
                
                if($row2['1']['level'] == 2)$data['level_2'] = $row2['1']['jml'];
                else $data['level_2'] = "0";
                
                if($row2['2']['level'] == 3)$data['level_3'] = $row2['2']['jml'];
                else $data['level_3'] = "0";
                
                if($row2['3']['level'] == 4)$data['level_4'] = $row2['3']['jml'];
                else $data['level_4'] = "0";
                
                if($row2['4']['level'] == 5)$data['level_5'] = $row2['4']['jml'];
                else $data['level_5'] = "0";
                
                if($row2['5']['level'] == 6)$data['level_6'] = $row2['5']['jml'];
                else $data['level_6'] = "0";
                
                if($row2['6']['level'] == 7)$data['level_7'] = $row2['6']['jml'];
                else $data['level_7'] = "0";
                
                if($row2['7']['level'] == 8)$data['level_8'] = $row2['7']['jml'];
                else $data['level_8'] = "0";
                
                if($row2['8']['level'] == 9)$data['level_9'] = $row2['8']['jml'];
                else $data['level_9'] = "0";
                
                if($row2['9']['level'] == 10)$data['level_10'] = $row2['9']['jml'];
                else $data['level_10'] = "0";
                
                $data['total']=$data['level_1']+$data['level_2']+$data['level_3']+$data['level_4']+$data['level_5']+$data['level_6']+$data['level_7']+$data['level_8']+$data['level_9']+$data['level_10'];
                */
                if($this->input->post('periode')){
                    $data['result'] = $this->MTree->get_tree($this->input->post('periode'),$row['auto_id'],$row['parentid'],$row['lft'],$row['rgt'],$row['level']);
                }else{
                    $data['result'] = $this->MTree->get_tree('',$row['auto_id'],$row['parentid'],$row['lft'],$row['rgt'],$row['level']);
                }
            }
        }
        
        $data['periode'] = $this->MTree->dropdown_periode();
        $data['page_title'] = 'Genealogy Tree';
        $this->load->view('member/tree',$data);
    }
    
    public function introducerlist(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        $this->form_validation->set_rules('condition','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $this->session->set_userdata('conditions',$this->input->post('condition'));
            $keywords = $this->session->userdata('keywords');
            $conditions = $this->session->userdata('conditions');
            $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
            $config['total_rows'] = $this->MTree->countSearchMember($conditions,$keywords);
            $config['per_page'] = 20;
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MTree->searchMember($conditions,$keywords,$config['per_page'],  $this->uri->segment(4)); 
        }else{
            $keywords = $this->session->userdata('keywords');
            $conditions = $this->session->userdata('conditions');
            $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
            $config['total_rows'] = $this->MTree->countSearchMember($conditions,$keywords);
            $config['per_page'] = 20;
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MTree->searchMember($conditions,$keywords,$config['per_page'],  $this->uri->segment(4));
            
        }
        
        $data['page_title'] = 'Introducer List';
        $this->load->view('member/v_introducer_list',$data);
    }
    
}
?>