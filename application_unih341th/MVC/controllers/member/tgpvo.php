<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tgpvo extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MBonus','MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('periode','','');
        $this->form_validation->set_rules('member_id','Member ID','required');
        $this->form_validation->set_rules('name','','');
        
        if($this->form_validation->run()){
            $data['results']=$this->MBonus->getTGPV($this->input->post('member_id'),$this->input->post('periode'));
            $data['row']=$this->MBonus->getPS($this->input->post('member_id'));
            $data['total']=$this->MBonus->getTotalTGPV($this->input->post('member_id'));
            $data['agps']=$this->MBonus->getTotalAGPV($this->input->post('member_id'));	//Updated by Boby (2009-11-17)
        }else{
            $data['results']=false;
            $data['row']=false;
        }
        $data['page_title'] = 'Total Group Poin Value';
        $this->load->view('member/total_group_pv_o',$data);
    }
    
    public function detail($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $header = $this->MBonus->getSubBonus($id);
        
        if(!count($header)){
            redirect('member/bonus','refresh');
        }
        $data['header'] = $header;
        
        $data['results']=$this->MBonus->getBonusDetail($id);
        
        $data['page_title'] = 'Statement Bonus Detail';
        $this->load->view('member/bonusdetail_statment',$data);
    }
      
}
?>