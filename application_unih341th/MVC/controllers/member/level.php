<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Level extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MBonus','MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('periode','','');
        $this->form_validation->set_rules('member_id','','');
        $this->form_validation->set_rules('name','','');
        if($this->form_validation->run()){
            $memberid = $this->input->post('member_id');
            $data['results']=$this->MBonus->getJenjang($memberid,$this->input->post('periode'));
            $data['rs']=$this->MBonus->kualifikasiLeader($this->input->post('periode'),$memberid);
            if(!$memberid){
                $data['results2']=$this->MBonus->countLeader($this->input->post('periode'));
            }else{
                $data['hightjenjang']=$this->MBonus->get_hightjenjang($memberid);
            }
        }else{
            $memberid = $this->session->userdata('userid');
            $data['row']=$this->MBonus->getPS($memberid,'');
            $data['rs']=$this->MBonus->kualifikasiLeader('',$memberid);
            $data['results']=$this->MBonus->getJenjang($memberid,'');
            $data['hightjenjang']=$this->MBonus->get_hightjenjang($memberid);
        }
        
        $data['dropdown'] = $this->MBonus->get_dropdown();
        $data['page_title'] = 'Member Posision';
        $this->load->view('member/jenjang_history',$data);
    }
    
      
}
?>