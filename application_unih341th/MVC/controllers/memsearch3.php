<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Memsearch3 extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MSearch'));
    }
    
    public function index($id=0){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','Keywords','min_length[3]');
        
        $config['base_url'] = site_url().'memsearch3/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(2))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(3); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countMember($keywords);
        $config['per_page'] = 20;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchMember($keywords,$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Member Search';
		$data['id'] = $id ;
        
        $this->load->view('search/member_search3',$data);
    }
    public function idx($id=0){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','Keywords','min_length[3]');
        
        $config['base_url'] = site_url().'memsearch3/idx/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            //if(!$this->uri->segment(3))
			$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countMember($keywords);
        $config['per_page'] = 20;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchMember($keywords,$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Member Search';
		$data['id'] = $id ;
        
        $this->load->view('search/member_search3',$data);
    }
}
?>