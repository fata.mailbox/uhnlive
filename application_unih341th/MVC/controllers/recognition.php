<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Recognition extends CI_Controller {
    function __construct()
    {
		parent::__construct();
		$this->load->model('MFrontend');
	}
	
	public function index(){
		$data['banner'] = $this->MFrontend->list_banner(50,0);
		
		$data['content'] = 'index/recognition';
		$this->load->view('index/index',$data);
	}
	
}?>
