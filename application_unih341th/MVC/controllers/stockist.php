<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stockist extends CI_Controller {
    function __construct()
    {
	parent::__construct();
	$this->load->model('MFrontend');
    }
    public function index(){
        $this->load->library('pagination');
        
        $config['base_url'] = site_url().'stockist/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['total_rows'] = $this->MFrontend->count_list_stockiest();
        $this->pagination->initialize($config);
        $data['results'] = $this->MFrontend->list_stockiest($config['per_page'],$this->uri->segment($config['uri_segment']));

        $data['banner'] = $this->MFrontend->list_banner(50,0);
	
	$data['content'] = 'index/stockiest';
        $this->load->view('index/index',$data);
    }    
}
?>