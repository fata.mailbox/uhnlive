<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Searchemp extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MSearchadmin'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/searchemp/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
			$keywords = $this->session->userdata('keywords');
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
			$keywords = "";
        }
        //$keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearchadmin->countEmp($keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearchadmin->searchEmp($keywords,$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Employee Search';
        
        $this->load->view('search/employee',$data);
    }
}
?>