<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Itemsearch_sostc extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MSearch'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/itemsearch_sostc/index/'.$this->uri->segment(4);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(5); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countSearchTtpSONew($keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchTtpSONew($keywords,$config['per_page'],$this->uri->segment(5));
        $data['page_title'] = 'Titipan Search';
        
        $this->load->view('search/stock_stc_search',$data);
    }
    
}
?>