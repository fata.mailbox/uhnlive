<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>UNI-HEALTH.COM</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="author" content="takwa"/>
<meta name="description" content="UNI-HEALTH : Multi Level Marketing" />
<meta name="keywords" content="MLM, Multi Level Marketing, apli, magozai, soho group, legress, legresskin, makanan kesehatan, realtime, Web Developer, smartindo, smartindo technology, specialist mlm, spesialis mlm, mlm software indonesia, software developer indonesia, web development indoneisa, programmer mlm, software mlm, MLM Developer, mlm software, programmer mlm, software developer, web design" />

<meta name="revisit-after" content="10 days" />
<meta name="robots" content="all,index,follow" />

<meta name="MSSmartTagsPreventParsing" content="TRUE" />
<meta http-equiv="Content-Language" content="en-us" />

<meta name="verify-v1" content="VCVjXvqlZ2A9LXgEy+EGCclrzw6TmBtD73F/mtMwSaQ=" />
<meta NAME="Distribution" CONTENT="Global" />
<meta NAME="Rating" CONTENT="General" />

<meta name="copyright" content="Copyright (c) 2009-<?php echo date("Y");?> | www.smartindo-technology.com | contact person: Takwa Handphone: +62 817 906 1982 | Telphone: +6221 5435 5600" />

<!--
	Copyright (c) i2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<link rel="icon" type="image/jpg" href="<?=base_url();?>images/frontend/favicon.ico"><!--favicon-->

<?php $uri1 = $this->uri->segment(1); $uri2 = $this->uri->segment(2);?>   

<link rel="stylesheet" type="text/css" 	href="<?=base_url();?>css/custom2.css" />		<!--to layout-->

<script src="<?=base_url();?>js/jquery_v1.4.2.js" 			type="text/javascript"></script>	<!--to navigasi & headerAnimasi & product & img & tooltip-->
<script src="<?=base_url();?>js/main.js" 					type="text/javascript"></script>	<!--to tooltip-->
<script src="<?=base_url();?>js/imgbubbles.js" 				type="text/javascript"></script>	<!--to Menu Navigasi-->

<?php if($uri1 == '' or $uri1 == 'index' or $uri1 == 'login' or $uri1=='welcome'){ ?>
	<!-- <script src="<?=base_url();?>js/superfish.js" 				type="text/javascript"></script>	<!-to headerAnimasi-->
    <script src="<?=base_url();?>js/script.js" 					type="text/javascript"></script>	<!-- Animasi-header -->
	<script src="<?=base_url();?>js/jquery_002.js"  			type="text/javascript"></script>	<!-- Animasi-header -->

    <script src="<?=base_url();?>js/jquery.imagecube.js" 		type="text/javascript"></script>	<!--to product-->
    <script src="<?=base_url();?>js/news_box.js" 				type="text/javascript"></script>	<!--to news_box-->
    <script src="<?=base_url();?>js/promoSlide.js"				type="text/javascript"></script>	<!--to promo slide-->
    
    
<!-- Slide Show ->
<script type="text/javascript">
	function slideSwitch() {
		var $active = $('#slideshoww IMG.active');
		
		if ( $active.length == 0 ) $active = $('#slideshoww IMG:last');
		
		// use this to pull the images in the order they appear in the markup
		var $next =  $active.next().length ? $active.next()
			: $('#slideshoww IMG:first');
		
		// uncomment the 3 lines below to pull the images in random order
		
		// var $sibs  = $active.siblings();
		// var rndNum = Math.floor(Math.random() * $sibs.length );
		// var $next  = $( $sibs[ rndNum ] );
				
		$active.addClass('last-active');
		
		$next.css({opacity: 0.0})
			.addClass('active')
			.animate({opacity: 1.0}, 1500, function() {
				$active.removeClass('active last-active');
			});
		}
		
		$(function() {
		setInterval( "slideSwitch()", 4000 );
	});
</script> -->

<!--to news box-->
<script type="text/javascript">
    //<![CDATA[
    Sys.Application.initialize();
    Sys.Application.add_init(function() {
        $create(Telerik.Web.UI.RadRotator, {"clientStateFieldID":"ctl00_ContentPlaceHolderNewsScoller_ctl00_radHeadlines_ClientState","frameDuration":2000,"items":[{},{}],"scrollDirection":4,"skin":"ZWS"}, null, null, $get("ctl00_ContentPlaceHolderNewsScoller_ctl00_radHeadlines"));
    });
    //]]>
</script>

<?php }?>

<?php if($uri1 == 'product' or $uri1 == 'testi' or $uri2 == 'album' or $uri1 == 'marketingplan'){?>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/highslide-gallery.css" />		<!--to layout-->
<script src="<?=base_url();?>js/highslide-with-gallery.js" 	type="text/javascript"></script> 	<!--to photo-->

	<?php if($uri2 == 'album'){ ?>
		<script type="text/javascript">
	hs.graphicsDir ='<?=base_url();?>images/graphics/';
	hs.align = 'center';
	hs.transitions = ['expand', 'crossfade'];
	hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = 0.8;
	//hs.dimmingOpacity = 0.75;
	
	// Add the controlbar
	hs.addSlideshow({
		//slideshowGroup: 'group1',
		interval: 5000,
		repeat: false,
		useControls: true,
		fixedControls: 'fit',
		overlayOptions: {
			opacity: 0.75,
			position: 'bottom center',
			hideOnMouseOut: true
		}
	});
</script>
    <?php }else{?>
    <!--to  photo product-->
    <script type="text/javascript">
        hs.graphicsDir = '<?=base_url();?>images/graphics/';
        hs.wrapperClassName = 'no-footer';
        hs.dimmingOpacity = 0.8;
    </script>
    <?php }?>
<?php }?>

<script src="<?=base_url();?>js/gotop.js" 					type="text/javascript"></script>	<!-- to back_top-->

<!-- Menu Navigasi -->
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('ul#orbs').imgbubbles({factor:1.6}) //add bubbles effect to UL id="orbs"
		$('ul#squares').imgbubbles({factor:1.6}) //add bubbles effect to UL id="squares"
	})
</script>

<!--to img-->
<script type="text/javascript">
	$(document).ready(function(){ 
	$(".post").css("opacity","1.0");
			
	$(".post").hover(function () {
											  
	$(this).stop().animate({
	opacity: 0.5
	}, "slow");
	},
			
	function () {
				
	$(this).stop().animate({
	opacity: 1.0
	}, "slow");
	});
	});
</script>

</head>
<?php if($uri1 == '' or $uri1 == 'index' or $uri1 == 'login' or $uri1=='welcome'){ 
	if($jmlpromo == '4'){
    	$x = 363;
    }elseif($jmlpromo == '3'){
    	$x = 401;
	}elseif($jmlpromo == '2'){
    	$x = 442;
    }else $x = 363;?>
	<body onload="promoSlide.build('sm',<?=$x;?>,8,8,1)">
<?php }else{ ?>
	<body>
<?php }?>

<!--main-->
<div id="main">
	
    <!--header_panel-->
    <div id="header_panel">
    	
        <!--logo-->
		<div id="logo" class="post"><a href="<?=site_url();?>" class="tooltip" title="Halaman Utama"><img src="<?=base_url();?>images/frontend/logo_white.png" /></a></div>
        <!--end logo-->
        
        <!--login-->
        <div id="login">
<?php echo form_open('login', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
        	<table width="100%" height="140">
       	  		<tr>
                    <td width="143" height="39" valign="top">
                    	<input class="input" name="username" value="<?=set_value('username','Username');?>" tabindex="1" onblur="if (this.value == '') this.value = 'Username';" onfocus="if (this.value == 'Username') this.value = '';" autocomplete="off" type="text">
                        <span style="color:red; font-size:11px;"><?php echo form_error('username');?></span>
                    </td>
		  			<td width="65" rowspan="2" valign="top">
                    	<input class="submit" name="submit" value="LOGIN" type="image" src="<?=base_url();?>images/frontend/login_icon.png" tabindex="3" >
                    </td>
       	  	  	</tr>
                <tr>
                    <td height="58" valign="top">
                      	<input class="input" name="password" value="Password" id="password" onblur="if (this.value == '') this.value = 'Password';" onfocus="if (this.value == 'Password') this.value = '';" tabindex="2" maxlength="50" type="password">
                    </td>
           	  	</tr>
                <tr>
                    <td height="30" colspan="2" align="center">
                    	<label style="font-size:medium"><input name="flag" id="flag" value="1" type="checkbox" <?=set_checkbox('flag', '1') ?>> Login Stockist</label>
                        <span style="color:red; font-size:11px;"><?php echo form_error('password');?></span>
                    </td>
                </tr>
       	  	</table>
            <?php echo form_close();?>
      	</div>
        <!--end login-->
        
        <?php if($uri1 == '' or $uri1 == 'index' or $uri1 == 'login' or $uri1=='welcome'){ ?>
        <!--animasi_panel-->
        <div class="animasi_panel">
            
            <!-- <div class="headerPromoCont">
                <div id="slideshoww">
                    <img src="<?=base_url();?>images/frontend/animasi_04.jpg" width="767" height="430" alt="" class="active" />
                    <img src="<?=base_url();?>images/frontend/animasi_07.jpg" width="767" height="430" alt="" />
                    <img src="<?=base_url();?>images/frontend/animasi_08.jpg" width="767" height="430" alt="" />
                    <img src="<?=base_url();?>images/frontend/animasi_09.jpg" width="767" height="430" alt="" />
                    <img src="<?=base_url();?>images/frontend/animasi_11.jpg" width="767" height="430" alt="" />
                    <img src="<?=base_url();?>images/frontend/animasi_12.jpg" width="767" height="430" alt="" />
                </div>
            </div> -->
            
            <div id="slider">
                <!-- start slideshow -->
                <div id="slideshow">
                    <div class="slider-item"><img src="<?=base_url();?>images/frontend/animasi_01.jpg" width="767" height="430" alt="image" border="0"></div>
                    <div class="slider-item"><img src="<?=base_url();?>images/frontend/animasi_02.jpg" width="767" height="430" alt="image" border="0"></div>
                    <div class="slider-item"><img src="<?=base_url();?>images/frontend/animasi_03.jpg" width="767" height="430" alt="image" border="0"></div>
                    <div class="slider-item"><img src="<?=base_url();?>images/frontend/animasi_04.jpg" width="767" height="430" alt="image" border="0"></div>
                    <div class="slider-item"><img src="<?=base_url();?>images/frontend/animasi_05.jpg" width="767" height="430" alt="image" border="0"></div>
                </div><!-- end #slideshow -->
                
              	<div class="controls-center">
                  	<div id="slider_controls">
                      	<ul id="slider_nav">
                          <li><a class="li_1" href="#">1</a></li><!-- Slide 1 -->
                          <li><a class="li_2" href="#">2</a></li><!-- Slide 2 -->
                          <li><a class="li_3" href="#">3</a></li><!-- Slide 3 -->
                          <li><a class="li_4" href="#">4</a></li><!-- Slide 4 -->
                          <li><a class="li_5" href="#">5</a></li><!-- Slide 4 -->
                      	</ul>
                  	</div>
              	</div>
                
                <!--cover animasi-->
                <div id="cover_animasi"></div>
                <!--end cover animasi-->
            </div><!--/slider -->
        
        </div><!--end animasi_panel-->
        <?php }else{ ?>
        <!--cover animasi-->
        <div id="cover_animasi" style="left:218px; top:-95px; z-index:1;"></div>
        <!--end cover animasi-->
        
        <!--header_banner-->
        <div class="header_banner">
        	<?php if($uri1 == 'aboutus')$p = "profile"; 
				elseif($uri1 == 'register')$p = "register"; 
				elseif($uri1 == 'product')$p = "product"; 
				elseif($uri1 == 'marketingplan')$p = "plan"; 
				elseif($uri1 == 'news')$p = "news"; 
				elseif($uri1 == 'testi')$p = "testimonial"; 
				elseif($uri1 == 'gallery')$p = "gallery"; 
				elseif($uri1 == 'download')$p = "download"; 
				elseif($uri1 == 'contactus')$p = "contact"; 
				elseif($uri1 == 'stockiest')$p = "stokis"; 
				else $p = "profile"; 
			?>
        	<img src="<?=base_url();?>images/frontend/banner_<?=$p;?>.jpg" width="767" height="430" alt="" />
        </div><!--end header_banner-->        
        <?php }?>
        
        <!--menu_panel-->
        <div class="menu_panel">
        	<div class="navigation_sec">
                <ul id="orbs" class="bubblewrap">
                  	<li style="left:-10px; top:-50px;"><a href="<?=site_url();?>"><div><img src="<?=base_url();?>images/frontend/home_icon.png" alt="Home" /></div></a></li>
                  	<li style="left:-0px; top:0px;"><a href="<?=site_url();?>aboutus"><div><img src="<?=base_url();?>images/frontend/profile_icon.png" alt="Company Profile" /></div></a></li>
                  	<li style="left:20px; top:28px;"><a href="<?=site_url();?>product"><div><img src="<?=base_url();?>images/frontend/product_icon.png" alt="Products" /></div></a></li>
                  	<li style="left:43px; top:37px;"><a href="<?=site_url();?>marketingplan"><div><img src="<?=base_url();?>images/frontend/plan_icon.png" alt="Marketing Plan" /></div></a></li>
                  	<li style="left:66px; top:30px;"><a href="<?=site_url();?>news"><div><img src="<?=base_url();?>images/frontend/what_new_icon.png" alt="News" /></div></a></li>
                  	<li style="left:87px; top:5px;"><a href="<?=site_url();?>contactus"><div><img src="<?=base_url();?>images/frontend/contact_icon.png" alt="Contact Us" /></div></a></li>
                  	<li style="left:100px; top:-45px; background:url(<?=base_url();?>images/frontend/reg_li_bg.png) no-repeat;"><a href="<?=site_url();?>register"><div><img src="<?=base_url();?>images/frontend/register_icon.png" style="width:42px; height:47px;" alt="Register" /></div></a></li>
                </ul>
            </div>
        </div>
        <!--end menu_panel-->
        
  	</div><!--end header_panel-->
    
    <!--cont-->
    <div id="cont">
        <!--container-->
        <div class="container">
            
            <!--conten_left-->
            
            <?php if($uri1 == '' or $uri1 == 'index' or $uri1 == 'login' or $uri1=='welcome'){ ?>
            <div class="content_left">
            	<?php $this->load->view('index/box_product');?>
                <?php $this->load->view('index/box_news');?>
                <img src="<?=base_url();?>images/frontend/member-of-soho.gif" class="post" style="margin:3px 0 0 3px;" />
            <?php }else{?>
			 <div class="content_left" style="width:170px;">
				<?php $data = array('uri1'=>$uri1,'uri2'=>$uri2); $this->load->view('index/sub_menu',$data); }?>
              <div class="clearBoth"></div>
            </div><!--end content_left-->
            
            <!--content-->
            <?php $this->load->view($content);?>
            
            <!--content_right-->
            <div class="content_right">
            	<!--contact box-->
                <div class="contact_box">
                	<a class="tooltip" title="Contact Us" href="<?=site_url();?>contactus"><div style="margin-bottom:9px; height:111px;"></div></a>
                    <div style="padding:0 1px;">
                        <a class="tooltip post" title="PT. Universal Health Network<br />No. Rekening: <b>003.305.9008</b><br />Cabang: Gunung Sari - Jakarta"><img src="<?=base_url();?>images/frontend/bca.png" /></a>
                        <a class="tooltip post" title="PT. Universal Health Network<br />No. Rekening: <b>125.000.972.3016</b><br />Cabang: Pulo Gadung - Jakarta"><img src="<?=base_url();?>images/frontend/mandiri.png" /></a>
                        <a class="tooltip post" title="PT. Universal Health Network<br />No. Rekening: <b>197-358-222</b><br />Cabang: JIEP - Jakarta"><img src="<?=base_url();?>images/frontend/bni.png" /></a>
                    </div>
                </div><!--contact box-->
                <?php $this->load->view('index/box_testimonial');?>
                
                <?php if($uri1 == '' or $uri1 == 'index' or $uri1 == 'login' or $uri1=='welcome'){ ?>
                <!--soho group-->
                <div style="margin:15px 0; text-align:center;">
	                <img src="<?=base_url();?>images/frontend/apli_logo.png" alt="">
                	<!-- <a href="#" class="post" target="_blank"><img src="<?=base_url();?>images/frontend/member-of-soho.gif" alt="" height="84" width="196"></a> -->
                </div><!--end soho group-->
                <?php }?>
              <div class="clearBoth"></div>
            </div><!--end content_right-->
            
          <div class="clearBoth"></div>
        </div><!--end container-->
    </div><!--end cont-->
    
  <div class="clearBoth"></div>
</div><!--end main-->


<!--footer Panel-->
<div id="footer_panel">
    <!--footer-->
    <div class="footer">
        <!--footer_copy-->
        <div class="footer_copy">
            <p>Copyright © 2011 UNI-HEALTH All rights reserved. Best preview at Firefox, Safari & Google Chrome dengan resolusi layar > 1024 X 768<!-- <br />
            Powered by: <a href="#" class="tooltip" title="* SMARTiNDO Technology * Specialize in : MLM Software Developtment and eCommerce" target="_blank">SMARTiNDO Technology</a>&nbsp;<a href="#" class="tooltip" title="* SMARTiNDO Technology * Specialize in : MLM Software Developtment and eCommerce" target="_blank"><img src="<?=base_url();?>images/frontend/icont-st.png" alt="" style="vertical-align:middle;" height="17" width="17" border="0"></a> --></p>
        </div><!--end footer_copy-->
      <div class="clearBoth"></div>
    </div><!--end footer-->
</div><!--end footer Panel-->


  	<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>
	<script type="text/javascript">
		_uacct = "UA-1709778-2";
		urchinTracker();
	</script> 
</body>
</html>