<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Logout extends CI_Controller {
    function __construct()
    {
	parent::__construct();
		if(!$this->session->userdata('logged_in')){
                    redirect('','refresh');
                }
	}

	// --------------------------------------------------------------------

	public function index()
	{
                $this->load->model('MAuth');
                
                $this->MAuth->logout();
                $this->session->set_flashdata('message',"you've been logged out!");
		redirect('','refresh');
	}
}
?>