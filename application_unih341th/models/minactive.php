<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Minactive extends CI_Model
{
	
	function __construct()
    {
        parent::__construct();
    }
    public function searchInactive($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.item_id,b.name,a.exp_date,a.status",false);
        $this->db->from('item_inactive a');
        $this->db->join('item b','a.item_id=b.id','left');
        $this->db->like('a.item_id',$keywords,'after');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countInactive($keywords=0){
    	$this->db->like('a.item_id',$keywords,'after');
        $this->db->from('item_inactive a');
        
        return $this->db->count_all_results();
    }
    public function addInactive(){
    	$data = array(
    		'item_id' => $this->input->post('itemcode'), 
    		'exp_date' => $this->input->post('exp_date'));
    	$this->db->insert('item_inactive',$data);
    }
    public function getInactive($id=0){
        $data=array();
        $this->db->select("a.id,a.item_id,b.name,a.exp_date,a.status",false);
        $this->db->from('item_inactive a');
        $this->db->join('item b','a.item_id=b.id','left');
        $this->db->where('a.id',$id);
        
        $q=$this->db->get();
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function editInactive($id=0){
    	$exp_date = $this->input->post('exp_date');
    	$status = $this->input->post('status');

    	$data = array(
        'exp_date' => $exp_date,
        'status' => $status
		);
    	$this->db->where('id',$id);
    	$this->db->update('item_inactive',$data);
    	$this->session->set_flashdata('message','Edit Product Inactive Successfully');
    }
}
?>