<?php
class MMutasi extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Mutasi Stcock antar Warehouse
    |--------------------------------------------------------------------------
    |
    | to sign warehouse
    |
    | @created 2009-04-03
    | @author qtakwa@yahoo.com@yahoo.com
    |
    */
    public function searchMutasi($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.status,a.createdby,b.name as fromwhs,c.name as towhs",false);
        $this->db->from('mutasi a');
        $this->db->join('warehouse b','a.warehouse_id1=b.id','left');
        $this->db->join('warehouse c','a.warehouse_id2=c.id','left');
        if($this->session->userdata('group_id') == 9){
            $whsid = $this->session->userdata('whsid');
            $where = "`a`.`warehouse_id1` = '$whsid' and ( `a`.`id` like '$keywords%' )";
            $this->db->where($where);
        }else{
            $this->db->like('a.id',$keywords,'after');
        }
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countMutasi($keywords=0){
        $this->db->from('mutasi a');
        if($this->session->userdata('group_id') == 9){
            $whsid = $this->session->userdata('whsid');
            $where = "`a`.`warehouse_id1` = '$whsid' and ( `a`.`id` like '$keywords%' )";
            $this->db->where($where);
        }else{
            $this->db->like('a.id',$keywords,'after');
        }
        return $this->db->count_all_results();
    }
    public function addMutasi(){
        $warehouse_id1=$this->input->post('warehouse_id1');
        $warehouse_id2=$this->input->post('warehouse_id2');
        $empid = $this->session->userdata('user');
		
		$totalprice = str_replace(".","",$this->input->post('totalprice'));
		
        $data = array(
            'date' => date('Y-m-d',now()),
            'warehouse_id1' => $warehouse_id1,
            'warehouse_id2' => $warehouse_id2,
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'event_id' => 'MT1',
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $empid,
            'totalharga' => $totalprice,
            'totalharga_act' => $totalprice
            );
       $this->db->insert('mutasi',$data);
       
       $id = $this->db->insert_id();
         
        $qty0 = str_replace(".","",$this->input->post('qty0'));
		$price0 = str_replace(".","",$this->input->post('price0'));
		$tprice0 = str_replace(".","",$this->input->post('tprice0'));
        if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                'harga' => $price0,
                'jmlharga' => $tprice0,
                'qty_act' => $qty0,
                'harga_act' => $price0,
                'jmlharga_act' => $tprice0
            );
            
            $this->db->insert('mutasi_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
		$price1 = str_replace(".","",$this->input->post('price1'));
		$tprice1 = str_replace(".","",$this->input->post('tprice1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => $price1,
                'jmlharga' => $tprice1,
                'qty_act' => $qty1,
                'harga_act' => $price1,
                'jmlharga_act' => $tprice1
            );
            
            $this->db->insert('mutasi_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
	   $price2 = str_replace(".","",$this->input->post('price2'));
	   $tprice2 = str_replace(".","",$this->input->post('tprice2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => $price2,
                'jmlharga' => $tprice2,
                'qty_act' => $qty2,
                'harga_act' => $price2,
                'jmlharga_act' => $tprice2
            );
            
            $this->db->insert('mutasi_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
	   $price3 = str_replace(".","",$this->input->post('price3'));
	   $tprice3 = str_replace(".","",$this->input->post('tprice3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => $price3,
                'jmlharga' => $tprice3,
                'qty_act' => $qty3,
                'harga_act' => $price3,
                'jmlharga_act' => $tprice3
            );
            
            $this->db->insert('mutasi_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
	   $price4 = str_replace(".","",$this->input->post('price4'));
	   $tprice4 = str_replace(".","",$this->input->post('tprice4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => $price4,
                'jmlharga' => $tprice4,
                'qty_act' => $qty4,
                'harga_act' => $price4,
                'jmlharga_act' => $tprice4
            );
            
            $this->db->insert('mutasi_d',$data);
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
	   $price5 = str_replace(".","",$this->input->post('price5'));
	   $tprice5 = str_replace(".","",$this->input->post('tprice5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => $price5,
                'jmlharga' => $tprice5,
                'qty_act' => $qty5,
                'harga_act' => $price5,
                'jmlharga_act' => $tprice5
            );
            
            $this->db->insert('mutasi_d',$data);
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
	   $price6 = str_replace(".","",$this->input->post('price6'));
	   $tprice6 = str_replace(".","",$this->input->post('tprice6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => $price6,
                'jmlharga' => $tprice6,
                'qty_act' => $qty6,
                'harga_act' => $price6,
                'jmlharga_act' => $tprice6
            );
            
            $this->db->insert('mutasi_d',$data);
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
	   $price7 = str_replace(".","",$this->input->post('price7'));
	   $tprice7 = str_replace(".","",$this->input->post('tprice7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => $price7,
                'jmlharga' => $tprice7,
                'qty_act' => $qty7,
                'harga_act' => $price7,
                'jmlharga_act' => $tprice7
            );
            
            $this->db->insert('mutasi_d',$data);
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
	   $price8 = str_replace(".","",$this->input->post('price8'));
	   $tprice8 = str_replace(".","",$this->input->post('tprice8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => $price8,
                'jmlharga' => $tprice8,
                'qty_act' => $qty8,
                'harga_act' => $price8,
                'jmlharga_act' => $tprice8
            );
            
            $this->db->insert('mutasi_d',$data);
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
	   $price9 = str_replace(".","",$this->input->post('price9'));
	   $tprice9 = str_replace(".","",$this->input->post('tprice9'));
       if($this->input->post('itemcode9') and $qty9 > 0){
            $data=array(
                'mutasi_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                'harga' => $price9,
                'jmlharga' => $tprice9,
                'qty_act' => $qty9,
                'harga_act' => $price9,
                'jmlharga_act' => $tprice9
            );
            
            $this->db->insert('mutasi_d',$data);
       }
       //$this->db->query("call sp_mutasi_stock('$id','$warehouse_id1','$warehouse_id2','N','$empid')");        
       $this->db->query("call sp_mutasi_stock_intransit('$id','$warehouse_id1','$warehouse_id2','N','$empid')");        
    }
    public function getDropDownWhs(){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function getMutasi($id){
        $data=array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.remarkapp,a.status,a.createdby,b.name as fromwhs,c.name as towhs,a.approved,a.approvedby, format(a.totalharga,0) as ftotalharga",false);
        $this->db->from('mutasi a');
        $this->db->join('warehouse b','a.warehouse_id1=b.id','left');
        $this->db->join('warehouse c','a.warehouse_id2=c.id','left');
        if($this->session->userdata('group_id') == 9){
            $whsid = $this->session->userdata('whsid');
            $where = "`a`.`warehouse_id1` = '$whsid' and `a`.`id` = '$id'";
            $this->db->where($where);
        }else{
            $this->db->where('a.id',$id);
        }
        $q=$this->db->get();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function getMutasiDetail($id=0){
        $data = array();
        //$q=$this->db->select("d.item_id,format(d.qty,0)as fqty,a.name",false)
        $q=$this->db->select("d.id, d.item_id,format(d.qty,0)as fqty, d.qty, d.harga, d.jmlharga, format(d.harga,0)as fharga, format(d.jmlharga,0)as fjmlharga,a.name",false)
            ->from('mutasi_d d')
            ->join('item a','d.item_id=a.id','left')
            ->where('d.mutasi_id',$id)
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Mutasi Stock antar Warehouse Approvede
    |--------------------------------------------------------------------------
    |
    | @created 2009-04-03
    | @author qtakwa@yahoo.com@yahoo.com
    |
    */
    public function searchMutasiApp($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.status,a.createdby,b.name as fromwhs,c.name as towhs",false);
        $this->db->from('mutasi a');
        $this->db->join('warehouse b','a.warehouse_id1=b.id','left');
        $this->db->join('warehouse c','a.warehouse_id2=c.id','left');
        if($this->session->userdata('group_id') == 9){
            $whsid = $this->session->userdata('whsid');
            $where = "`a`.`warehouse_id2` = '$whsid' and ( `a`.`id` like '$keywords%' )";
            $this->db->where($where);
        }else{
            $this->db->like('a.id',$keywords,'after');
        }
        $this->db->order_by('a.status','asc');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countMutasiApp($keywords=0){
        $this->db->from('mutasi a');
        if($this->session->userdata('group_id') == 9){
            $whsid = $this->session->userdata('whsid');
            $where = "`a`.`warehouse_id2` = '$whsid' and ( `a`.`id` like '$keywords%' )";
            $this->db->where($where);
        }else{
            $this->db->like('a.id',$keywords,'after');
        }
        return $this->db->count_all_results();
    }
    public function appMutasi(){
        $id=$this->input->post('id');
        $warehouse_id1=$this->input->post('warehouse_id1');
        $warehouse_id2=$this->input->post('warehouse_id2');
        $empid = $this->session->userdata('user');
		$totalharga_act = str_replace(".","",$this->input->post('totalprice_act'));
		$totalharga_hilrus = str_replace(".","",$this->input->post('totalprice_hilrus'));
		
        $data = array(
            'approved' => date('Y-m-d',now()),
            'remarkapp' => $this->db->escape_str($this->input->post('remark')),
            'status' => 'Y',
            'approved' => date('Y-m-d H:m:s',now()),
            'approvedby' => $empid,
			'totalharga_act' => str_replace(".","",$this->input->post('totalprice_act')),
			'totalharga_hilrus' => str_replace(".","",$this->input->post('totalprice_hilrus'))
            );
       $this->db->update('mutasi',$data,array('id'=>$id));
	   
       //$this->db->query("call sp_mutasi_stock('$id','$warehouse_id2','Y','$empid')");        
       //$this->db->query("call sp_mutasi_stock('$id','$warehouse_id2','$warehouse_id1','Y','$empid')");        
       $this->db->query("call sp_mutasi_stock_receive('$id','$warehouse_id2','$warehouse_id1','Y','$empid')");   
	   
	   //Start Hilang/Rusak
	   for($i=1; $i<=$this->input->post('counter');$i++) {
		   $data = array(
				'qty_act' => str_replace(".","",$this->input->post('qty'.$i)),
				'harga_act' => str_replace(".","",$this->input->post('price'.$i)),
				'jmlharga_act' => str_replace(".","",$this->input->post('tprice'.$i)),
				'qty_hilrus' => str_replace(".","",$this->input->post('rqty'.$i)),
				'harga_hilrus' => str_replace(".","",$this->input->post('rprice'.$i)),
				'jmlharga_hilrus' => str_replace(".","",$this->input->post('rtprice'.$i))
				);
		   $this->db->update('mutasi_d',$data,array('id'=>$this->input->post('detail_id'.$i)));
	   }
	   if($totalharga_hilrus > 0 ){
			$data_hilrus = array(
				'date' => date('Y-m-d',now()),
				'warehouse_id1' => $warehouse_id2,
				'warehouse_id2' => $warehouse_id1,
				'remark' => 'Retur Hilang/Rusak - '.$id,
				'event_id' => 'MT1',
				'created' => date('Y-m-d H:m:s',now()),
				'createdby' => $empid,
				'totalharga' => $totalharga_hilrus,
				'totalharga_act' => $totalharga_hilrus
				);
		   $this->db->insert('mutasi',$data_hilrus);
		   
		   $id_hilrus = $this->db->insert_id();
		   
		   for($j=1; $j<=$this->input->post('counter');$j++) {
			   $qty_hilrus = 0;$price_hilrus = 0;$tprice_hilrus = 0;
			   $qty_hilrus = str_replace(".","",$this->input->post('rqty'.$j));
			   $price_hilrus = str_replace(".","",$this->input->post('rprice'.$j));
			   $tprice_hilrus = str_replace(".","",$this->input->post('rtprice'.$j));
			   
			   if($qty_hilrus > 0){
					$data=array(
						'mutasi_id' => $id_hilrus,
						'item_id' => $this->input->post('itemcode'.$j),
						'qty' => $qty_hilrus,
						'harga' => $price_hilrus,
						'jmlharga' => $tprice_hilrus,
						'qty_act' => $qty_hilrus,
						'harga_act' => $price_hilrus,
						'jmlharga_act' => $tprice_hilrus
					);
					
					$this->db->insert('mutasi_d',$data);
				}

		   }
       	   $this->db->query("call sp_mutasi_stock_intransit('$id_hilrus','$warehouse_id2','$warehouse_id1','N','$empid')");        
	   }
	   //eof Hilang/Rusak
	   
    }
        
    public function getMutasiApp($id){
        $data=array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.remarkapp,a.status,a.createdby,b.name as fromwhs,c.name as towhs,a.approved,a.approvedby,a.warehouse_id1,a.warehouse_id2, format(a.totalharga,0) as ftotalharga, totalharga, totalharga_act, totalharga_hilrus, format(a.totalharga_act,0) as ftotalharga_act, format(a.totalharga_hilrus,0) as ftotalharga_hilrus",false);
        $this->db->from('mutasi a');
        $this->db->join('warehouse b','a.warehouse_id1=b.id','left');
        $this->db->join('warehouse c','a.warehouse_id2=c.id','left');
        if($this->session->userdata('group_id') == 9){
            $whsid = $this->session->userdata('whsid');
            $where = "`a`.`warehouse_id2` = '$whsid' and `a`.`id` = '$id'";
            $this->db->where($where);
        }else{
            $this->db->where('a.id',$id);
        }
        $q=$this->db->get();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function getMutasiApp2($id){
        $data=array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.remarkapp,a.warehouse_id1,a.warehouse_id2,a.status,a.createdby,b.name as fromwhs,c.name as towhs,a.approved,a.approvedby,a.warehouse_id1,a.warehouse_id2, format(a.totalharga,0) as ftotalharga",false);
        $this->db->from('mutasi a');
        $this->db->join('warehouse b','a.warehouse_id1=b.id','left');
        $this->db->join('warehouse c','a.warehouse_id2=c.id','left');
        if($this->session->userdata('group_id') == 9){
            $whsid = $this->session->userdata('whsid');
            $where = "`a`.`warehouse_id2` = '$whsid' and `a`.`id` = '$id' and `a`.`status` = 'N'";
            $this->db->where($where);
        }else{
            $this->db->where('a.id',$id);
            $this->db->where('a.status','N');
        }
        $q=$this->db->get();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function check_approved($id){
        $q=$this->db->select("id",false)
            ->from('mutasi')
            ->where('id',$id)
            ->where('status','Y')
            ->get();
        return ($q->num_rows() > 0)? true : false;        
    }

    public function cekStok($itemid, $whs){
        $item = $itemid;
        //$qty = str_replace(".","",$this->input->post('qty'));
        $whsid = $whs;
                
        $qry = "SELECT qty FROM stock WHERE item_id = '$item' and warehouse_id = '$whsid'";
        $q = $this->db->query($qry);
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['qty'];
    }

}?>