<?php
class GLobal_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    //START ASP 20190108
    public function getWhPengiriman($id)
    {
        $data=array();
		$warehouse_pengiriman = '';
		/*
		$query = "
		SELECT * FROM warehouse w 
		WHERE w.id IN (
			SELECT rod.warehouse_id FROM ro_d rod
			WHERE rod.ro_id = ".$id."
		)
		";
		*/
		$query = "
		(
			SELECT * FROM warehouse w 
			WHERE w.id IN (
				SELECT rod.warehouse_id FROM ro_d rod
				WHERE rod.ro_id = ".$id."
			)
		)UNION(
			SELECT * FROM warehouse w 
			WHERE w.id IN (
				SELECT n.warehouse_id
				FROM ncm n 
				LEFT JOIN ref_trx_nc a ON a.nc_id = n.id
				WHERE a.trx_id = ".$id."
			)
		)
		";
		
		$qry = $this->db->query($query);
	    
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
				$warehouse_pengiriman.= $row['name']." - ";
			}
        }
		$qry->free_result();
		//return $data;
		$warehouse_pengiriman = rtrim($warehouse_pengiriman," - ");
		return $warehouse_pengiriman;
    }
	
	
    public function getFreeStock($id)
    {
		$whsid = $this->input->post('whsid');
        $data=array();
		$query = "
		SELECT dt.id, dt.item_id, dt.member_id, dt.free_item_id, i.name as free_item_name,dt.hpp, MAX(dt.free_qty) AS free_qty, whs_id
		FROM (
			SELECT ro.id, prm.id AS promo_id, prm.item_id, ro.member_id, prm.free_item_id, i.hpp
				
				, CASE WHEN multiples = 1 THEN (FLOOR(SUM(rod.qty) / prm.item_qty) * prm.free_item_qty) 
				WHEN FLOOR(rod.qty / prm.item_qty) > 0 THEN prm.free_item_qty
				ELSE 0 END AS free_qty
				, CASE WHEN i.warehouse_id = 0 THEN ".$whsid." ELSE i.warehouse_id END AS whs_id
				-- ,rod.warehouse_id as whs_id
			FROM so_check ro
			LEFT JOIN so_check_d rod ON ro.id = rod.so_id
			LEFT JOIN(
				SELECT *
				FROM promo
				WHERE (term = 1 OR term = 3)
					AND (end_periode + INTERVAL 1 DAY) > NOW()
					AND expired = 0
			)AS prm ON rod.item_id = prm.item_id
			LEFT JOIN item i ON prm.free_item_id = i.id
			WHERE ro.id = ".$id."
			GROUP BY prm.id 
			HAVING free_qty > 0
		) AS dt 
		left join item i on i.id = dt.free_item_id
		GROUP BY dt.item_id, dt.free_item_id
		";
		
		$qry = $this->db->query($query);
	    
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
    public function checkFreeStock($itemid,$whsid)
    {
        $data=array();
		$query = "
		SELECT 
		qty as stock
		FROM stock s
		WHERE s.warehouse_id = ".$whsid." AND s.item_id = '".$itemid."'
		";
		
		$qry = $this->db->query($query);
	    
		///echo $this->db->last_query();
        if($qry->num_rows() > 0){
            $data = $qry->row();
        }
        $qry->free_result();
        return $data;
    }
	//END ASP 20190108
    public function check_stock_whs($id,$itemid,$price,$whsid)
    {
        $data=array();
        $q = $this->db->select("sum(d.qty) as qty, format(i.price,0)as fprice, s.qty as stock,i.name")
	    ->from('so_check_d d')
	    ->join('stock s','d.item_id=s.item_id','left')
	    ->join('item i','d.item_id=i.id','left')
	    ->where('d.so_id',$id)
	    ->where('d.item_id',$itemid)
	    ->where('s.warehouse_id',$whsid)
	    ->get();
	    
	///echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
    
    public function check_stock_stc($id,$itemid,$price,$titipan_id,$stcid)
    {
        $data=array();
        $q = $this->db->select("sum(d.qty) as qty, format(s.harga,0)as fprice,s.qty as stock,i.name")
	    ->from('so_check_d d')
	    ->join('titipan s','d.titipan_id=s.id','left')
	    ->join('item i','d.item_id=i.id','left')
	    ->where('d.so_id',$id)
	    ->where('d.item_id',$itemid)
            ->where('d.titipan_id',$titipan_id)
	    ->where('s.member_id',$stcid)
	    ->get();
	    
	//echo $this->db->last_query();
        
	if($q->num_rows() > 0){
            $data = $q->row();
        }
        $q->free_result();
        return $data;
    }

    public function get_ewallet($memberid)
    {
        $data=array();
        $q = $this->db->select("m.nama, format(m.ewallet,0)as fewallet,m.ewallet")
	    ->from('member m')
	    ->where('m.id',$memberid)
	    ->get();
	    
	//echo $this->db->last_query();
        
	if($q->num_rows() > 0){
            $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
    public function get_ewallet_stc($memberid)
    {
        $data=array();
        $q = $this->db->select("s.no_stc,if(s.type=1,6,0)as persen,m.nama, format(s.ewallet,0)as fewallet,s.ewallet")
	    ->from('stockiest s')
	    ->join('member m','s.id=m.id','left')
	    ->where('m.id',$memberid)
	    ->get();
	    
		//echo $this->db->last_query();
			
		if($q->num_rows() > 0){
			$data = $q->row();
        }
        $q->free_result();
        return $data;
    }
    
    public function get_ro_diskon($memberid){
        $data = array();
		/*
        $this->db->select("a.id,k.id as kota_id,k.name as namakota,a.no_stc,m.nama,format(a.ewallet,0)as fewalletstc,a.ewallet,
                , a.alamat, k.timur,k.name as namakota,p.name as propinsi
                , a.type as tipe, if(a.type=1,6,0)as persen
            ",false);
		*/
		/*
        $this->db->select("a.id,k.id as kota_id,k.name as namakota,a.no_stc,m.nama,format(a.ewallet,0)as fewalletstc,a.ewallet,
                , a.alamat, k.timur,k.name as namakota,p.name as propinsi
                , a.type as tipe, 
				CASE WHEN a.type = 1 THEN 6
				WHEN a.type = 2 AND a.id IN ('00000005','00000168') THEN 3 
				ELSE 0 END AS persen
            ",false);
		*/
        $this->db->select("a.id,k.id as kota_id,k.name as namakota,a.no_stc,m.nama,format(a.ewallet,0)as fewalletstc,a.ewallet,
                , a.alamat, k.timur,k.name as namakota,p.name as propinsi
                , a.type as tipe, k.warehouse_id, 
				CASE WHEN a.type = 1 THEN 6
				WHEN a.type = 2 THEN 2 
				ELSE 0 END AS persen
            ",false);
        $this->db->from('stockiest a');
        $this->db->join('member m','a.id=m.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        
        $this->db->where('a.id',$memberid);
        $q = $this->db->get();
            //echo $this->db->last_query();
            if($q->num_rows > 0){
                $data = $q->row();
            }
            $q->free_result();
            return $data;
    }
	
	public function get_ro_delivery($memberid){
        $data = array();
        $this->db->select("a.id,k.name as namakota,a.alamat,p.name as propinsi, k.warehouse_id, a.kecamatan, a.kelurahan, a.kodepos, a.pic_name, a.pic_hp",false);
        $this->db->from('member_delivery a');
        $this->db->join('kota k','a.kota=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        
        $this->db->where('a.member_id',$memberid);
		$this->db->order_by('id','desc');
        $q = $this->db->get();
            //echo $this->db->last_query();
            if($q->num_rows > 0){
				
                $data[$row['']]= 'Alamat baru';
				foreach($q->result_array() as $row){
					$data[$row['id']] = $row['alamat'].', '.$row['namakota'];
				}
            }
            $q->free_result();
            return $data;
    }
	
	public function get_ro_delivery_id($id){
		$data = array();
        $this->db->select("a.id,a.kota as kota_id,k.name as namakota,a.alamat,p.name as propinsi,k.timur, k.warehouse_id, a.kecamatan, a.kelurahan, a.kodepos, a.pic_name, a.pic_hp",false);
        $this->db->from('member_delivery a');
        $this->db->join('kota k','a.kota=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        
        $this->db->where('a.id',$id);
        $q = $this->db->get();
            //echo $this->db->last_query();
            if($q->num_rows > 0){
				$data = $q->row();
            }
            $q->free_result();
            return $data;
	}

}?>