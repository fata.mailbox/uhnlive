<?php
class MMenu extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function numformat ($number){
        return number_format($number, 0, '.', '.');
    }
    /*
    |--------------------------------------------------------------------------
    | check for block web
    |--------------------------------------------------------------------------
    |
    | block website if samthing condision
    |
    | @param void
    | @return void
    | @author takwa
    | @created 2008-12-27
    |
    */
    public function blocked(){
        $q = $this->db->select("b.desc")
            ->from('blocked b')
            ->where('b.status','1')
            ->get();
        if($q->num_rows > 0){
            $row = $q->row_array();
            $this->session->set_flashdata('message',$row['desc']);
            return true;
        }
        return false;
    }
    
    /*
    |--------------------------------------------------------------------------
    | setting access for user
    |--------------------------------------------------------------------------
    |
    | setting permission to access its menu
    |
    | @param void
    | @return void
    | @author takwa
    | @created 2008-12-28
    |
    */
    public function access($group,$url,$aut){
        //select count(*) from modules m left join menu mn on m.menu_id=mn.id where group_id=1 and mn.url='fpb' and m.view='1'
        $condision=array('m.group_id'=>$group,'mn.url'=>$url,'m.'.$aut=>'1'); 
        $q = $this->db->select("count(*)as count",false)
            ->from('modules m')
            ->join('menu mn','m.menu_id=mn.id','left')
            ->where($condision)
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows > 0){
            $row = $q->row_array();
            if($row['count'] <= 0){
                return true;    
            }
        }
        return false;
    }
      
    /*
    |--------------------------------------------------------------------------
    | get group menu exp: Inventory -> report
    |--------------------------------------------------------------------------
    |
    | display grou from menu
    |
    | @param void
    | @return void
    | @author takwa
    | @created 2008-12-27
    |
    */ 
    public function getMenuGroup($group){
        $data = array();
        $this->db->select("s.title,s.id",false);
        $this->db->from('modules a');
        $this->db->join('menu m','a.menu_id=m.id','left');
        $this->db->join('sub_menu s','m.submenu_id=s.id','left');
        $this->db->where('a.group_id',$group);
        $this->db->where('a.view','1');
        $this->db->group_by('m.submenu_id','asc');
        $this->db->order_by('s.sort','asc');
        $q = $this->db->get();
       // echo $this->db->last_query();
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | get group sub menu from menu 
    |--------------------------------------------------------------------------
    |
    | display sub menu from menu
    |
    | @param void
    | @return void
    | @author takwa
    | @created 2008-12-27
    |
    */ 
    public function getSubMenu($group,$submenu){
        $data[]=array();
        $this->db->select("m.title,m.folder,m.url",false);
        $this->db->from('modules a');
        $this->db->join('menu m','a.menu_id=m.id','left');
        $this->db->where('a.group_id',$group);
        $this->db->where('a.view','1');
        $this->db->where('m.submenu_id',$submenu);
        $this->db->order_by('m.sort','asc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        } 
        $q->free_result();
        return $data;
    }
    
    public function getDropDownWhs(){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
      
    public function get_member($username){
        $data[]=array();
        $this->db->select("j.name as jenjang",false);
        $this->db->from('member a');
        $this->db->join('jenjang j','a.jenjang_id=j.id','left');
        $this->db->where('a.id',$username);
        $this->db->limit(1);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows >0){
            $data = $q->row();
        } 
        $q->free_result();
        return $data;
    }
    public function get_member2($username){
        $data[]=array();
        $this->db->select("j.name as jenjang, j.decription as description",false);
        $this->db->from('member a');
        $this->db->join('jenjang j','a.jenjang_id=j.id','left');
        $this->db->where('a.id',$username);
        $this->db->limit(1);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows >0){
            $data = $q->row();
        } 
        $q->free_result();
        return $data;
    }
    public function get_member2_new($username){
        $data[]=array();
        $this->db->select("j.name as jenjang, j.decription as description, s.no_stc, k.name as kotaasal, k.region",false);
        $this->db->from('member a');
        $this->db->join('jenjang j','a.jenjang_id=j.id','left');
        $this->db->join('stockiest s','a.id=s.id','left');
        $this->db->join('kota k','s.kota_id=k.id','left');
        $this->db->where('a.id',$username);
        $this->db->limit(1);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows >0){
            $data = $q->row();
        } 
        $q->free_result();
        return $data;
    }
}
?>