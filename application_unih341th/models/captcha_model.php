<?php
 class Captcha_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
 	private $vals = array();
 	
	private $baseUrl  = 'http://www.uni-health.com/captcha/';
	private $basePath = './home/deploy/source/sohomlm/captcha/';
	
	private $captchaImagePath = '';
	private $captchaImageUrl  = '';
	private $captchaFontPath  = '';
	//private $captchaFontPath  = './system/fonts/FreeSerifBold.ttf';
	
	public function __construct($configVal = array())
	{
		parent::Model();
		
		$this->load->plugin('captcha');		
		
		if(!empty($config))
			$this->initialize($configVal);
		else
			$this->vals = array(
					'word'		=> '',
					'word_length'	=> 5,
					'img_path'	=> $this->basePath . $this->captchaImagePath,
					'img_url'	=> $this->baseUrl . $this->captchaImageUrl,
					'font_path'	=> $this->captchaFontPath,
					'img_width'	=> 125,
					'img_height' 	=> 40,
					'expiration' 	=> 3600
				   );	
	}	
	
	/**
	 * initializes the variables
	 *
	 * @author 	Mohammad Jahedur Rahman <jahed01@gmail.com>
	 * @access 	public
	 * @param 	array 	config
	 */		 	
	public function initialize ($configVal = array())
	{
		$this->vals = $configVal;
	} //end function initialize
	
	//---------------------------------------------------------------
	
	/**
	 * generate the captcha
	 *
	 * @author 	Mohammad Jahedur Rahman <jahed01@gmail.com>
	 * @access 	public
	 * @return 	array
	 */	
	public function generateCaptcha () 
	{
		$cap = create_captcha($this->vals);
		return $cap;	
	} //end function generateCaptcha	
 }
?>