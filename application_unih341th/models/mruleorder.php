<?php
//20160404 - ASP Start
class Mruleorder extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
	public function getRuleOrder($item_id){
		$data = array();
		$str = "
			SELECT 
			i.id,i.name, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
			FROM item i 
			LEFT JOIN item_rule_order_promo ir ON i.id = ir.item_id  AND ir.expireddate >= DATE(NOW())
			where i.id = '$item_id'";
		$qry = $this->db->query($str);
		//echo $this->db->last_query();
		if($qry->num_rows()>0){
			$data=$qry->row_array();
		}
		$qry->free_result();
		return $data['min_order'];
	}
	public function getItemName($item_id){
		$data = array();
		$str = "
			SELECT 
			i.id,i.name, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
			FROM item i 
			LEFT JOIN item_rule_order_promo ir ON i.id = ir.item_id  AND ir.expireddate >= DATE(NOW())
			where i.id  = '$item_id'";
		$qry = $this->db->query($str);
		//echo $this->db->last_query();
		if($qry->num_rows()>0){
			$data=$qry->row_array();
		}
		$qry->free_result();
		return $data['name'];
	}
}
//20160404 - ASP End
?>