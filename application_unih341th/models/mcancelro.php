<?php
class MCancelRO extends CI_Model{
  function __construct()
  {
    parent::__construct();
  }

  /* Created by Boby 201404025 */
  public function countCancelRO($keywords){
    $data = array();
    $this->db->select("
    ro.date, cro.ro_id_canceled AS ro_id, cro.ro_id_execute AS ro_cancel
    , ro.member_id, m.nama, stc.no_stc, s.nama AS namaStc
    , cro.created, cro.createdby
    ",false);
    $this->db->from('cancel_ro cro');
    $this->db->join('ro','cro.ro_id_canceled = ro.id','left');
    $this->db->join('member m','ro.member_id = m.id','left');
    $this->db->join('stockiest stc','ro.stockiest_id = stc.id','left');
    $this->db->join('member s','stc.id = s.id','left');
    $this->db->like('cro.ro_id_canceled',$keywords,'between');
    $this->db->or_like('cro.ro_id_execute',$keywords,'between');
    $this->db->or_like('m.id',$keywords,'between');
    $this->db->or_like('m.nama',$keywords,'between');
    $this->db->or_like('s.id',$keywords,'between');
    $this->db->or_like('s.nama',$keywords,'between');
    $this->db->or_like('stc.no_stc',$keywords,'between');
    $this->db->order_by('cro.id','desc');
    // $this->db->limit($num,$offset);
    // $q = $this->db->get();
    // echo $this->db->last_query();
    return $this->db->count_all_results();
  }

  public function getCancelRO($keywords, $num, $offset){
    $data = array();
    $this->db->select("
    ro.date, cro.ro_id_canceled AS ro_id, cro.ro_id_execute AS ro_cancel
    , ro.member_id, m.nama, stc2.no_stc as no_stc_ybs, stc.no_stc, s.nama AS namaStc
    , cro.created
    , ifnull(crr.remark,'-') as remark
    , cro.createdby
    ",false);
    $this->db->from('cancel_ro cro');
    $this->db->join('ro','cro.ro_id_canceled = ro.id','left');
    $this->db->join('member m','ro.member_id = m.id','left');
    $this->db->join('stockiest stc','ro.stockiest_id = stc.id','left');
    $this->db->join('stockiest stc2','ro.member_id = stc2.id','left');
    $this->db->join('member s','stc.id = s.id','left');
    $this->db->join('cancel_ro_remark crr','cro.ro_id_canceled = crr.ro_id_canceled','left');
    $this->db->like('cro.ro_id_canceled',$keywords,'between');
    $this->db->or_like('cro.ro_id_execute',$keywords,'between');
    $this->db->or_like('m.id',$keywords,'between');
    $this->db->or_like('m.nama',$keywords,'between');
    $this->db->or_like('s.id',$keywords,'between');
    $this->db->or_like('s.nama',$keywords,'between');
    $this->db->or_like('stc.no_stc',$keywords,'between');
    $this->db->order_by('cro.id','desc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    // echo $this->db->last_query();
    if($q->num_rows >0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }

  //20160724 Andi Satya Perdana End
  public function getInfoRo($keyword){
    $data = array();
    //$leader = 1;
    $query = "
    SELECT ro.id, CASE
    WHEN cro.ro_id_canceled > 0 THEN 1
    WHEN cro1.ro_id_execute > 0 THEN 2
    END AS note
    From ro
    LEFT JOIN cancel_ro cro ON ro.id = cro.ro_id_canceled
    LEFT JOIN cancel_ro cro1 ON ro.id = cro1.ro_id_execute
    WHERE ro.id = '$keyword'
    ";

    $qry = $this->db->query($query);

    // echo $this->db->last_query();
    if($qry->num_rows() > 0){
      $data = $qry->row_array();
    }
    $qry->free_result();
    return $data;
  }

  public function getInfoStockStc($keyword){
    $data = array();
    //$leader = 1;
    $query = "
    SELECT 
    rod.item_id, ro.date,rod.qty, t.qty
    , CASE WHEN (ro.date < (LAST_DAY(NOW() - INTERVAL 2 MONTH) + INTERVAL 1 DAY))  THEN 2
           WHEN (ro.date >= (LAST_DAY(NOW() - INTERVAL 2 MONTH) + INTERVAL 1 DAY)) AND rod.qty > t.qty THEN 1
           ELSE 0 END AS note
    FROM ro_d rod 
    LEFT JOIN ro ON rod.ro_id = ro.id
    LEFT JOIN titipan t ON t.item_id = rod.item_id AND t.member_id = ro.member_id AND rod.harga = t.harga
    WHERE ro.id = '$keyword'
    ";

    $qry = $this->db->query($query);

    // echo $this->db->last_query();
    if($qry->num_rows() > 0){
      $data = $qry->row_array();
    }
    $qry->free_result();
    return $data;
  }


  public function CancelRO(){
    $ro_id = $this->input->post('ro_id');
    $remark_in = $this->input->post('remark');
    $empid = $this->session->userdata('username');
    $this->db->query("CALL cancel_ro_procedure('$ro_id','$empid','$remark_in')");
    $this->db->close();
  }

}?>
