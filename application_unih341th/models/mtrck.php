<?php
class MTrck extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
	
	public function addTrck($code,$member_id){
        $data=array(
            'id' => '',
            'code' => $code,
			'check_in' => date('Y-m-d H:i:s',now()),
            'user_id' => $this->session->userdata('user'),
            'member_id' => $member_id,
			'ip' => $this->input->ip_address()
        );
            
        $this->db->insert('so_t',$data);
    }
	public function addTrckAct($code,$member_id){
        $data=array(
            'id' => '',
            'code' => $code,
			'check_in' => date('Y-m-d H:i:s',now()),
            'user_id' => $this->session->userdata('userid'),
            'member_id' => $member_id,
			'ip' => $this->input->ip_address()
        );
            
        $this->db->insert('so_t',$data);
    }
}
?>