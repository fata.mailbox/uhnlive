<?php
class MSc extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Stockiest Consignment
    |--------------------------------------------------------------------------
    |
    | @author budi@nawadata.com
    | @poweredby www.nawadata.com
    | @created 2018-12-26
    |
    */
    
    public function searchSc_item($keywords=0){
		
		$data = array();
        $this->db->select("d.item_id,format(d.qty,0)as fqty,
		format(d.harga,0)as fharga,format(d.pv,0)as fpv,
		format(sum(d.qty*d.harga),0)as fsubtotal,
		format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name",false);
        $this->db->from('pinjaman_titipan_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.pinjaman_titipan_id',$keywords);
        $this->db->group_by('d.id');
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
		
	}
	
    public function searchSc($keywords=0,$num,$offset){
		
        $data = array();
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id IS NOT NULL AND a.id LIKE '$keywords%'";
        }else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.stockiest_id IS NOT NULL  and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
			else $where = "a.stockiest_id IS NOT NULL and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        
		$this->db->select("a.id, a.remark,
		date_format(a.tgl, '%d-%b-%Y') AS date,
		a.tgl AS tgl,
		s.no_stc, s.status , 
		format(SUM(a.totalharga), 0) AS ftotalharga,
		format(SUM(a.totalpv), 0) AS ftotalpv,
		TIME_FORMAT(a.created, '%h %i %s %p') AS created,
		SUM(d.qty) AS qty,
		SUM(d.qty_retur) AS qty_retur,
		SUM(d.qty_total) AS qty_total,
		SUM(d.qty_lunas) AS qty_lunas,
		COUNT(d.status_pinjam) AS jml_pinjam,
		m.nama as nama,
		m.id as member_id,
		m.alamat as alamat",
		false);
        $this->db->from('pinjaman_titipan a');
		$this->db->join('member m','m.id = a.stockiest_id','left');       
	    $this->db->join('stockiest s','s.id = a.stockiest_id','left');
        $this->db->join('pinjaman_titipan_d d','d.pinjaman_titipan_id = a.id','left');
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->group_by('a.id');
        $this->db->limit($num,$offset);
		
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function countSc($keywords=0){
		$this->db->from('pinjaman_titipan');
		$query = $this->db->get();
		$rowcount = $query->num_rows();
		return $query->num_rows();
    }
	
	
	public function searchPinjaman($keywords=0,$num,$offset){
        $data = array();
        $whsid = $this->session->userdata('whsid');
		
		/*
		$subQr="(SELECT
				GROUP_CONCAT(
					DISTINCT usa.`name` SEPARATOR ', '
				) AS warehouse_name
			FROM
				pinjaman_d asu
			INNER JOIN warehouse usa ON usa.id = asu.warehouse_id
			WHERE
				asu.pinjaman_id = a.id
			GROUP BY
				asu.pinjaman_id
			ORDER BY
				asu.pinjaman_id DESC
		)";
		*/
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) and a.warehouse_id = '".$this->session->userdata('keywords_whsid')."'";
						//$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			}
		}
        //echo $where;
        $this->db->select("a.id,a.status_ho,a.status_hub,
		a.tglapproved_ho,a.tglapproved_hub ,
		a.approvedby_ho,a.approvedby_hub ,date_format(a.tgl,'%d-%b-%Y')as tgl,
		a.createdby,a.stockiest_id,s.no_stc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,
		m.id as member_id,
		m.nama,
		m.kecamatan,
		m.kelurahan,
		m.alamat,
		m.kodepos,
		m.kota_id,
		m.ewallet,
		m.hp,
		s.kota_delivery,
		s.type AS tipe,
			CASE
		WHEN s.type = 1 THEN
			6
		WHEN s.type = 2 THEN
			2
		ELSE
			0
		END AS persen ,
		(SELECT format(SUM(pinjaman_d.qty_total),0) as qty_total FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) as qty_total,
		(SELECT format(SUM(pinjaman_d.qty_retur),0) as qty_retur FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) as qty_retur,
		(SELECT format(SUM(pinjaman_d.qty_lunas),0) as qty_lunas FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) as qty_lunas,
		(SELECT format(SUM(pinjaman_d.qty),0) as qty FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) as qty,
		a.remark,
		/*".$subQr." as warehouse_name, */
		w.name as warehouse_name, w.id as warehouse_id,  
		ifnull(date_format(a.tglapproved,'%d-%b-%Y %T'),'-')as appdate,
		ifnull(date_format(a.tglapproved_ho,'%d-%b-%Y %T'),'-')as appdate_ho,
		ifnull(date_format(a.tglapproved_hub,'%d-%b-%Y %T'),'-')as appdate_hub,
		a.status,
		IF(a.approvedby = '' OR a.approvedby IS NULL , '-',a.approvedby ) as approvedby",false);
        $this->db->from('pinjaman a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
		// START ASP 20180525
		$this->db->join('warehouse w', 'a.warehouse_id=w.id','left');
		// EOF ASP 20180525
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        $xSql= $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
				if ($row['status_ho']=='1' AND $row['status_hub']=='1' ){
					$cSql="UPDATE pinjaman SET 
					remarkapp=CONCAT(remarkapp,' Change to Delivery '), 
					status='delivery', 
					approvedby='Pusat,HUB', 
					tglapproved='".date('Y-m-d H:i:s', now())."' 
					WHERE id='".$row['id']."'";
					$query = $this->db->query($cSql);
				}
				
                //$data[] = $row;
				//echo $row['approvedby'].'<br>';
            }
        }
		//echo $xSql;
		$query = $this->db->query($xSql);
        return $query->result_array();
		
		
		
		/*
		$perpage=10;
		
		if (empty($offset)){$offset=0;}else{$offset=$offset;}
		
		$xSql="
		SELECT STRAIGHT_JOIN
			a.status_ho,a.status_hub,a.tglapproved_ho,a.tglapproved_hub,a.approvedby_ho,a.approvedby_hub,
			date_format(a.tgl, '%d-%b-%Y') AS tgl,a.createdby,a.stockiest_id,
			s.no_stc,
			format(a.totalharga, 0) AS ftotalharga,
			format(a.totalpv, 0) AS ftotalpv,
			m.id AS member_id,m.nama,m.kecamatan,m.kelurahan,
			m.alamat,m.kodepos,m.kota_id,m.ewallet,
			m.hp, s.kota_delivery,
		s.type AS tipe,
			CASE
		WHEN s.type = 1 THEN
			6
		WHEN s.type = 2 THEN
			2
		ELSE
			0
		END AS persen,
		 (
			SELECT
				format(
					SUM(pinjaman_d.qty_lunas),
					0
				) AS qty_lunas
			FROM
				pinjaman_d
			WHERE
				pinjaman_d.pinjaman_id = a.id
		) AS qty_lunas,
		 (
			SELECT
				format(SUM(pinjaman_d.qty), 0) AS qty
			FROM
				pinjaman_d
			WHERE
				pinjaman_d.pinjaman_id = a.id
		) AS qty,
		 a.remark,
		 
		 w. NAME AS warehouse_ori,
		 w.id AS warehouse_id,
		 ifnull(
			date_format(
				a.tglapproved,
				'%d-%b-%Y %T'
			),
			'-'
		) AS appdate,
		 ifnull(
			date_format(
				a.tglapproved_ho,
				'%d-%b-%Y %T'
			),
			'-'
		) AS appdate_ho,
		 ifnull(
			date_format(
				a.tglapproved_hub,
				'%d-%b-%Y %T'
			),
			'-'
		) AS appdate_hub,
		 a. STATUS,

		IF (
			a.approvedby = ''
			OR a.approvedby IS NULL,
			'-',
			a.approvedby
		) AS approvedby, 
			preQuery.warehouse_name FROM 
		( SELECT 
				asu.pinjaman_id, 
				GROUP_CONCAT(
					DISTINCT usa.`name` SEPARATOR ', '
				) AS warehouse_name
			FROM
				pinjaman_d asu 
			INNER JOIN warehouse usa ON usa.id = asu.warehouse_id
			INNER JOIN pinjaman sau ON sau.id = asu.pinjaman_id 
			WHERE
				asu.pinjaman_id = a.id
			GROUP BY
				asu.pinjaman_id
			ORDER BY
				asu.pinjaman_id DESC ) preQuery 
		JOIN pinjaman a on PreQuery.pinjaman_id = a.id
		LEFT JOIN stockiest s ON a.stockiest_id = s.id
		LEFT JOIN member m ON a.stockiest_id = m.id
		LEFT JOIN warehouse w ON a.warehouse_id = w.id

		ORDER BY
			a.id DESC
		LIMIT ".$offset.",".$perpage." ";
		
		$q = $this->db->query($xSql);
		if($q->num_rows > 0){
            foreach($q->result_array() as $row){
				if ($row['status_ho']=='1' AND $row['status_hub']=='1' ){
					$cSql="UPDATE pinjaman SET 
					remarkapp=CONCAT(remarkapp,' Change to Delivery '), 
					status='delivery', 
					approvedby='Pusat,HUB', 
					tglapproved='".date('Y-m-d H:i:s', now())."' 
					WHERE id='".$row['id']."'";
					$query = $this->db->query($cSql);
				}
				
                $data[] = $row;
				//echo $row['approvedby'].'<br>';
            }
        }
		
		print_r($q->result_array());
		return $data;
		*/
    }
	
	public function getWhPengiriman($id)
    {
        $data=array();
		$warehouse_pengiriman = '';
		$query = "
		SELECT * FROM warehouse w 
		WHERE w.id IN (
			SELECT rod.warehouse_id FROM pinjaman_d  rod
			WHERE rod.pinjaman_id = ".$id."
		)
		";
		
		$qry = $this->db->query($query);
	    
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
				$warehouse_pengiriman.= $row['name']." - ";
			}
        }
		$qry->free_result();
		//return $data;
		$warehouse_pengiriman = rtrim($warehouse_pengiriman," - ");
		return $warehouse_pengiriman;
    }
	
    
	public function searchPinjaman_Payment($keywords=0,$num,$offset){
        $data = array();
        $whsid = $this->session->userdata('whsid');
        if($whsid > 1){
			//$where = "(SELECT format(SUM(pinjaman_d.qty_lunas),0) as qty_lunas FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id)<=(SELECT format(SUM(pinjaman_d.qty),0) as qty FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) OR
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = " a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = " a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
					else
						$where = " a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) and a.warehouse_id = ".$this->session->userdata('keywords_whsid');
			}
		}
        
        $this->db->select("a.id,a.status_ho,a.status_hub,
		a.tglapproved_ho,a.tglapproved_hub ,
		a.approvedby_ho,a.approvedby_hub ,date_format(a.tgl,'%d-%b-%Y')as tgl,
		a.createdby,a.stockiest_id,s.no_stc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,
		m.id as member_id,
		m.nama,
		m.kecamatan,
		m.kelurahan,
		m.alamat,
		m.kodepos,
		m.kota_id,
		m.ewallet,
		m.hp,
		s.kota_delivery,
		s.type AS tipe,
			CASE
		WHEN s.type = 1 THEN
			6
		WHEN s.type = 2 THEN
			2
		ELSE
			0
		END AS persen ,
		(SELECT format(SUM(pinjaman_d.qty_lunas),0) as qty_lunas FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) as qty_lunas,
		(SELECT format(SUM(pinjaman_d.qty),0) as qty FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) as qty,
		a.remark,w.name as warehouse_name, w.id as warehouse_id,  
		ifnull(date_format(a.tglapproved,'%d-%b-%Y %T'),'-')as appdate,
		ifnull(date_format(a.tglapproved_ho,'%d-%b-%Y %T'),'-')as appdate_ho,
		ifnull(date_format(a.tglapproved_hub,'%d-%b-%Y %T'),'-')as appdate_hub,
		a.status,
		IF(a.approvedby = '' OR a.approvedby IS NULL , '-',a.approvedby ) as approvedby",false);
        $this->db->from('pinjaman a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
		// START ASP 20180525
		$this->db->join('warehouse w', 'a.warehouse_id=w.id','left');
		// EOF ASP 20180525
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        //$query = $this->db->get();
        //$xSql= $this->db->last_query();
		echo $xSql;
	
		$query = $this->db->query($xSql);
        return $query->result_array();
    }
    
	public function countPinjaman_Payment($keywords=0){
        $whsid = $this->session->userdata('whsid');
		
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
		}else{ 
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) and a.warehouse_id = ".$this->session->userdata('keywords_whsid');
			}
		}
        
        $this->db->select("a.id");
        $this->db->from('pinjaman a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->where($where);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
        return $query->num_rows();
    }
	
    
	
    public function countPinjaman($keywords=0){
        $whsid = $this->session->userdata('whsid');
		/*
		$subQr="(SELECT
				GROUP_CONCAT(
					DISTINCT usa.`name` SEPARATOR ', '
				) AS warehouse_name
			FROM
				pinjaman_d asu
			INNER JOIN warehouse usa ON usa.id = asu.warehouse_id
			WHERE
				asu.pinjaman_id = a.id
			GROUP BY
				asu.pinjaman_id
			ORDER BY
				asu.pinjaman_id DESC
		)";
		*/
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) and a.warehouse_id = '".$this->session->userdata('keywords_whsid')."'";
						//$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			}
		}
        $this->db->select("a.id");
        $this->db->from('pinjaman a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->where($where);
		$query = $this->db->get();
		//echo $this->db->last_query();
        //return $this->db->count_all_results();
		return $query->num_rows();;
    }
   
	
	public function search_stock_whs($whsid, $keywords=0, $flag='',$num,$offset){
		$data = array();
		$perpage=10;
		
		if (empty($offset)){$offset=0;}else{$offset=$offset;}
		
		$sql = "( SELECT s.item_id
							, i.name
							, s.qty,FORMAT(s.qty,0)AS fqty
							, i.price,FORMAT(i.price,0)AS fprice
							, i.price2,FORMAT(i.price2,0)AS fprice2
							, i.pv,FORMAT(i.pv,0)AS fpv
							, i.bv
							, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
							, CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
							, w.name AS stock_wh, w.id as whsid, i.warehouse_id
				  FROM stock s
				  LEFT JOIN item i ON s.item_id=i.id
				  LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
				  LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
				  LEFT JOIN warehouse w ON s.warehouse_id = w.id
				  LEFT JOIN(	SELECT i.id
										, IFNULL(ind.price_not_disc,0) AS price_not_disc
										, MIN(ind.end_period) AS end_period
										FROM
										item i
										LEFT JOIN item_not_discount ind ON ind.item_id = i.id
											AND ind.expired = 0
											AND ind.end_period >= DATE(NOW())
										GROUP BY i.id
									) AS dt ON s.item_id = dt.id
				  WHERE s.warehouse_id = ".$whsid." 
				  AND i.sales = 'Yes'
				  AND s.qty > 0
				  AND ( i.id LIKE '%%' OR i.name LIKE '%%')
				  AND i.warehouse_id = 0
				  ORDER BY NAME ASC
				  )
				  UNION (
				  SELECT
				  s.item_id
				  ,i.name
				  ,s.qty,FORMAT(s.qty,0)AS fqty,i.price,FORMAT(i.price,0)AS fprice,i.price2,FORMAT(i.price2,0)AS fprice2
				  ,i.pv,FORMAT(i.pv,0)AS fpv
				  ,i.bv, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
				  , CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
				  , w.name AS st0ck_wh, w.id as whsid,  i.warehouse_id
				  FROM item i
				  LEFT JOIN stock s ON s.item_id=i.id AND s.warehouse_id = i.warehouse_id
				  LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
				  LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
				  LEFT JOIN warehouse w ON s.warehouse_id = w.id
				  LEFT JOIN(
										SELECT
										i.id
										, IFNULL(ind.price_not_disc,0) AS price_not_disc
										, MIN(ind.end_period) AS end_period
										FROM
										item i
										LEFT JOIN item_not_discount ind ON ind.item_id = i.id AND ind.expired = 0 AND ind.end_period >= DATE(NOW())
										GROUP BY i.id
									) AS dt ON s.item_id = dt.id
				  WHERE  s.warehouse_id = 1  
				  AND i.sales = 'Yes' AND s.qty > 0 AND (i.name LIKE '%".$keywords."%' OR  i.id LIKE '%".$keywords."%') 
				  ORDER BY NAME ASC
				  )
				  ORDER BY warehouse_id, NAME ASC LIMIT ".$offset.",".$perpage." 
				  ";
		$q = $this->db->query($sql);
		if($q->num_rows > 0){
		  foreach($q->result_array() as $row){
			$data[] = $row;
		  }
		}
		//$q->free_result();
		//echo $this->db->last_query();
		return $data;
	}

	public function count_search_stock_whs($whsid,$keywords=0){
		
		$sql = "( SELECT s.item_id
							, i.name
							, s.qty,FORMAT(s.qty,0)AS fqty
							, i.price,FORMAT(i.price,0)AS fprice
							, i.price2,FORMAT(i.price2,0)AS fprice2
							, i.pv,FORMAT(i.pv,0)AS fpv
							, i.bv
							, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
							, CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
							, w.name AS stock_wh, i.warehouse_id
				  FROM stock s
				  LEFT JOIN item i ON s.item_id=i.id
				  LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
				  LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
				  LEFT JOIN warehouse w ON s.warehouse_id = w.id
				  LEFT JOIN(	SELECT i.id
										, IFNULL(ind.price_not_disc,0) AS price_not_disc
										, MIN(ind.end_period) AS end_period
										FROM
										item i
										LEFT JOIN item_not_discount ind ON ind.item_id = i.id
											AND ind.expired = 0
											AND ind.end_period >= DATE(NOW())
										GROUP BY i.id
									) AS dt ON s.item_id = dt.id
				  WHERE s.warehouse_id = ".$whsid."
				  AND i.sales = 'Yes'
				  AND s.qty > 0
				  AND ( i.id LIKE '%%' OR i.name LIKE '%%')
				  AND i.warehouse_id = 0
				  ORDER BY NAME ASC
				  )
				  UNION (
				  SELECT
				  s.item_id
				  ,i.name
				  ,s.qty,FORMAT(s.qty,0)AS fqty,i.price,FORMAT(i.price,0)AS fprice,i.price2,FORMAT(i.price2,0)AS fprice2
				  ,i.pv,FORMAT(i.pv,0)AS fpv
				  ,i.bv, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
				  , CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
				  , w.name AS st0ck_wh, i.warehouse_id
				  FROM item i
				  LEFT JOIN stock s ON s.item_id=i.id AND s.warehouse_id = i.warehouse_id
				  LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
				  LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
				  LEFT JOIN warehouse w ON s.warehouse_id = w.id
				  LEFT JOIN(
										SELECT
										i.id
										, IFNULL(ind.price_not_disc,0) AS price_not_disc
										, MIN(ind.end_period) AS end_period
										FROM
										item i
										LEFT JOIN item_not_discount ind ON ind.item_id = i.id AND ind.expired = 0 AND ind.end_period >= DATE(NOW())
										GROUP BY i.id
									) AS dt ON s.item_id = dt.id
				  WHERE i.sales = 'Yes' AND s.qty > 0 AND ( i.id LIKE '%".$keywords."%' OR i.name LIKE '%".$keywords."%')
				  ORDER BY NAME ASC
				  )
				  ORDER BY warehouse_id, NAME ASC
				  ";
		$q = $this->db->query($sql);
		return $q->num_rows;
	}
	
	public function count_search_stock_whs_old($whsid,$keywords=0,$flag){
		$this->db->select("i.id",false);
		$this->db->from('stock s');
		$this->db->join('item i','s.item_id=i.id','left');

		if($flag == 'so'){
		  $where = "s.warehouse_id = '$whsid' and i.sales = 'Yes' and i.type_id > 1 and s.qty > 0 and ( i.id like '%$keywords%' or i.name like '%$keywords%' )";
		  $this->db->where($where);
		}elseif($flag == 'ro'){
		  $where = "s.warehouse_id = '$whsid' and i.sales = 'Yes' and s.qty > 0 and ( i.id like '%$keywords%' or i.name like '%$keywords%' )";
		  $this->db->where($where);
		}
		else{
		  $this->db->like('i.id', $keywords, 'between');
		  $this->db->or_like('i.name', $keywords, 'between');
		}

		return $this->db->count_all_results();
	}

	
	public function getWhs($keywords){
		$data = array();
        $this->db->select("d.name as WhName,a.id,a.name,d.id AS WhId",false);
        $this->db->from('warehouse d');
        $this->db->join('kota a','d.id=a.warehouse_id','INNER');
        $this->db->where('a.id',$keywords);
        //$this->db->group_by('d.id');
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
	}
	
	public function addPinjaman(){
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
        $empid = $this->session->userdata('user');
        $whsid = $this->input->post('whsid');
        $stcid = $this->input->post('member_id');
        
		
        $data = array(
            'stockiest_id'=>$stcid,
            'tgl' => date('Y-m-d',now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'pu' => $this->input->post('pu'),
            'warehouse_id' => $whsid,
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$this->session->userdata('user'),
			/* 
			untuk approval
            'tglapproved_ho'=>date('Y-m-d H:i:s',now()),
            'tglapproved'=>date('Y-m-d H:i:s',now()),
            'approvedby'=>date('Y-m-d H:i:s',now()),
            'status_ho'=>date('Y-m-d H:i:s',now()),
            'approvedby_hub'=>date('Y-m-d H:i:s',now()),
            'approvedby_ho'=>$empid,
			*/
            'status'=>'pending' 
        );
        $this->db->insert('pinjaman',$data);
        
        $id = $this->db->insert_id();
        
		for ($i = 0; $i <= ($this->input->post('counti')-1); $i++) {
			if (!empty($this->input->post('itemcode'.$i))){
				$data_promo=$this->MSc->checkPromo($this->input->post('itemcode'.$i),'aktif','','');
				$promo_id="0";
				foreach ($data_promo as $val_promo){
					$promo_id=$val_promo['id'];
				}
				
				$data=array(
					'warehouse_id' => $this->input->post('det_whsid'.$i),
					'pinjaman_id' => $id,
					'item_id' => $this->input->post('itemcode'.$i),
					'promo_id' => $promo_id,
					'qty' => $this->input->post('qty'.$i),
					'harga' => str_replace(",","",$this->input->post('price'.$i)),
					'jmlharga' => str_replace(".","",$this->input->post('subtotal'.$i)),
					'pv' => str_replace(".","",$this->input->post('pv'.$i)),
					'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$i))
				);
				$this->db->insert('pinjaman_d',$data);
				$id_d = $this->db->insert_id();
				/* Pakai Strore Procedure sp_sc_tail_vwh
				foreach ($data_promo as $val_promo){
					$data=array(
						'promo_id' => $val_promo['id'],
						'pinjaman_id' => $id,
						'pinjaman_d_id' => $id_d,
						'qty' => $this->input->post('qty'.$i),
						'pinjaman_nc_tgl' => date('Y-m-d H:i:s',now()),
						'created' => date('Y-m-d H:i:s',now()),
						'createdby'=>$this->session->userdata('user') 
					);
					$this->db->insert('pinjaman_nc',$data);
				}
				*/
			}
		}
		
        $this->db->query("call sp_sc_pinjaman('$id','$stcid','$totalharga','$whsid','$empid')");
        
        
		// delivery address
		$data = array(
		'member_id' => $this->input->post('member_id'),
		'kota' => $this->input->post('kota_id'),
		'pic_name' => strip_quotes($this->db->escape_str($this->input->post('pic_name'))),
		'pic_hp' => strip_quotes($this->db->escape_str($this->input->post('pic_hp'))),
		'alamat' => strip_quotes($this->db->escape_str($this->input->post('addr'))),
		'kelurahan' => strip_quotes($this->db->escape_str($this->input->post('kelurahan'))),
		'kecamatan' => strip_quotes($this->db->escape_str($this->input->post('kecamatan'))),
		'kodepos' => strip_quotes($this->db->escape_str($this->input->post('kodepos'))),
		'created' => $this->session->userdata('username')
		);
		
		$addr_id = $this->input->post('deli_ad');
		//if(	$this->input->post('addr') != $this->input->post('addr1')	|| 	$this->input->post('kota_id') != $this->input->post('kota_id1')){
			$this->db->insert('member_delivery',$data);
			$addr_id = $this->db->insert_id();
			//$this->db->update('member',array('delivery_addr'=>$addr_id),array('id' => $this->input->post('member_id')));
		//}
		$this->db->update('pinjaman',array('deliv_addr'=>$addr_id),array('id' => $id));
		
		$this->db->query("UPDATE pinjaman_d SET qty_total=(IFNULL(qty,0)-IFNULL(qty_retur,0)-IFNULL(qty_lunas,0))");
    }
	
	
	public function pinjamanApproved($appstat){
        if($appstat=='p_id_ho'){
			$remarkapp=$this->db->escape_str($this->input->post('remark'));
			foreach($this->input->post('p_id_ho') as $key){
				$cSql="UPDATE pinjaman SET 
				remarkapp=CONCAT(remarkapp,'".$remarkapp."'), 
				status_ho='1', 
				approvedby_ho='".$this->session->userdata('user')."', 
				tglapproved_ho='".date('Y-m-d H:i:s', now())."' 
				WHERE id='".$key."'";
				$query = $this->db->query($cSql);
				
				//echo '<br>'.$this->db->last_query();
			}
        }else if($appstat=='p_id_hub'){
			$remarkapp=$this->db->escape_str($this->input->post('remark'));
			foreach($this->input->post('p_id_hub') as $key){
				
				$cSql="UPDATE pinjaman SET 
				remarkapp=CONCAT(remarkapp,'".$remarkapp."'), 
				status_hub='1', 
				approvedby_hub='".$this->session->userdata('user')."', 
				tglapproved_hub='".date('Y-m-d H:i:s', now())."' 
				WHERE id='".$key."'";
				$query = $this->db->query($cSql);
				//echo '<br>'.$this->db->last_query();
			}
			
		}else{
            $this->session->set_flashdata('message','Nothing to delevery approved!');
        }
    }
    
	
  
    protected function _countApproved($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('pinjaman');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	
    public function getPinjaman($id=0){
        $data = array();
        $this->db->select("a.id, a.status_ho as status_ho,a.status_hub as status_hub, date_format(a.tgl,'%d-%b-%Y')as tgl,
		format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,
		a.created as created,a.createdby,
                    s.no_stc,m.nama,m.alamat,m.kodepos,k.name as kota,p.name as propinsi, md.pic_name, md.pic_hp,md.alamat as del_alamat,
					md.kecamatan as del_kecamatan,md.kelurahan as del_kelurahan,
					md.kodepos as del_kodepos,kd.name as del_kota,pd.name as del_propinsi
					",false);
        $this->db->from('pinjaman a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->join('member_delivery md','a.deliv_addr=md.id','left');
        $this->db->join('kota kd','md.kota=kd.id','left');
        $this->db->join('propinsi pd','kd.propinsi_id=pd.id','left');
        
        $whsid = $this->session->userdata('whsid');
        if($whsid > 1)$this->db->where('a.warehouse_id',$whsid); 
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
	public function getDropDownWhs($all){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            if($all == 'all')$data['all']='All Cabang';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function getPinjamanDetail_full($keywords=0){
       $sql = "( SELECT s.item_id
							, i.name
							, s.qty,FORMAT(s.qty,0)AS fqty
							, i.price,FORMAT(i.price,0)AS fprice
							, i.price2,FORMAT(i.price2,0)AS fprice2
							, i.pv,FORMAT(i.pv,0)AS fpv
							, i.bv
							, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
							, CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
							, w.name AS stock_wh, i.warehouse_id
				  FROM stock s
				  LEFT JOIN item i ON s.item_id=i.id
				  LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
				  LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
				  LEFT JOIN warehouse w ON s.warehouse_id = w.id
				  LEFT JOIN(	SELECT i.id
										, IFNULL(ind.price_not_disc,0) AS price_not_disc
										, MIN(ind.end_period) AS end_period
										FROM
										item i
										LEFT JOIN item_not_discount ind ON ind.item_id = i.id
											AND ind.expired = 0
											AND ind.end_period >= DATE(NOW())
										GROUP BY i.id
									) AS dt ON s.item_id = dt.id
				  WHERE i.sales = 'Yes'
				  AND s.qty > 0
				  AND ( i.id LIKE '%%' OR i.name LIKE '%%')
				  AND i.warehouse_id = 0
				  ORDER BY NAME ASC
				  )
				  UNION (
				  SELECT
				  s.item_id
				  ,i.name
				  ,s.qty,FORMAT(s.qty,0)AS fqty,i.price,FORMAT(i.price,0)AS fprice,i.price2,FORMAT(i.price2,0)AS fprice2
				  ,i.pv,FORMAT(i.pv,0)AS fpv
				  ,i.bv, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
				  , CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
				  , w.name AS st0ck_wh, i.warehouse_id
				  FROM item i
				  LEFT JOIN stock s ON s.item_id=i.id AND s.warehouse_id = i.warehouse_id
				  LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
				  LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
				  LEFT JOIN warehouse w ON s.warehouse_id = w.id
				  LEFT JOIN(
										SELECT
										i.id
										, IFNULL(ind.price_not_disc,0) AS price_not_disc
										, MIN(ind.end_period) AS end_period
										FROM
										item i
										LEFT JOIN item_not_discount ind ON ind.item_id = i.id AND ind.expired = 0 AND ind.end_period >= DATE(NOW())
										GROUP BY i.id
									) AS dt ON s.item_id = dt.id
				  WHERE i.sales = 'Yes' AND s.qty > 0 AND (i.name LIKE '%".$keywords."%' OR  i.id LIKE '%".$keywords."%') 
				  ORDER BY NAME ASC
				  )
				  ORDER BY warehouse_id, NAME ASC
				  ";
		$q = $this->db->query($sql);
		if($q->num_rows > 0){
		  foreach($q->result_array() as $row){
			$data[] = $row;
		  }
		}
		//$q->free_result();
		//echo $this->db->last_query();
		return $data;
    }
	
	public function getPinjamanDetail($id=0){
        $data = array();
        $this->db->select("d.id,
		d.item_id,
		d.warehouse_id,
		format(d.qty, 0) AS fqty,
		IFNULL(d.qty_retur, 0) AS fqty_retur,
		IFNULL(d.qty_lunas, 0) AS fqty_lunas,
		format(d.harga,0)as fharga,
		format(d.pv,0)as fpv,
		format((d.qty - IFNULL(d.qty_retur, 0) )* d.harga, 0) AS fsubtotal,
		format((d.qty - IFNULL(d.qty_retur, 0) )* d.pv, 0) AS fsubtotalpv,
		a.name",false);
        $this->db->from('pinjaman_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.pinjaman_id',$id);
        $this->db->group_by('d.id');
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function getPinjamanDetail_Warehouse($id=0){
		$cSql="SELECT d.pinjaman_id as id, d.warehouse_id AS warehouse_id, w.name AS warehouseName
			FROM
				(pinjaman_d d)
			LEFT JOIN item a ON d.item_id = a.id
			LEFT JOIN manufaktur m ON d.item_id = m.manufaktur_id
			LEFT JOIN item b ON m.item_id = b.id
			LEFT JOIN warehouse w ON d.warehouse_id = w.id
			WHERE
				`d`.`pinjaman_id` = '".$id."'
			GROUP BY warehouse_id ORDER BY a.warehouse_id" ;
			$q = $this->db->query($cSql);
			//echo $this->db->last_query();
			$data=array();
			if ($q->num_rows()>0){
				foreach($q->result_array() as $row){
					$data[]=$row;
				}
			}
			$q->free_result();
			return $data;
			
	}
    public function getPinjamanDetail_p($id=0){
        $data = array();
        $this->db->select("IFNULL(m.item_id,d.item_id) AS item_id
			,CASE WHEN m.item_id IS NULL THEN FORMAT(d.qty,0)
			 ELSE FORMAT(m.qty*d.qty,0)
			 END AS fqty
			,CASE WHEN m.item_id IS NULL THEN a.name
			 ELSE b.name
			 END AS name,
			 d.warehouse_id as warehouse_id, w.name AS warehouseName 
			 ",false);
        $this->db->from('pinjaman_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->join('manufaktur m','d.item_id = m.manufaktur_id','left');
        $this->db->join('item b',' m.item_id = b.id','left');
        $this->db->join('warehouse w',' d.warehouse_id = w.id','left');
        $this->db->where('d.pinjaman_id',$id);
        //$this->db->group_by('d.id');
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
   
   
   public function countSearchMember($keywords=0,$stc){
        
        $where = "a.status = 'active' and (a.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%') ";
        if($stc == '0')$where = "a.id != '$stc' and ".$where;
        elseif($stc == 'mstc')$where = "a.type = 1 and ".$where; //untuk browse hanya stockiest tdk termasuk m-stc
        elseif($stc == 'romstc')$where = "a.type = 2 and ".$where; //untuk browse hanya m-stc
        elseif($stc == 'type')$where = "a.type = '$stc' and ".$where;
        else $where =$where;
        
        $this->db->select("a.id");
        $this->db->from('stockiest a');
        $this->db->join('member m','a.id=m.id','left');
        $this->db->where($where);
		$q = $this->db->get();
        return $q->num_rows;

    }
	
	
	
	public function searchMember($keywords=0,$stc,$num,$offset){
		$data = array();
		$this->db->select("
		a.id,k.id AS kota_id,
	k. NAME AS namakota,
	a.no_stc,
	m.nama,
	format(a.ewallet, 0) AS fewalletstc,
	md.alamat,
	k.timur,
	md.kota,
	md.id AS deli_ad,
	md.kecamatan,
	md.kelurahan,
	md.kodepos,
	md.pic_name,
	md.pic_hp,
	m.alamat AS alamat_stc,
	m.kecamatan AS kecamatan_stc,
	m.kelurahan AS kelurahan_stc,
	m.kodepos AS kodepos_stc,
	m.hp AS hp_stc,
	a.type AS tipe,
	CASE
WHEN a.type = 1 THEN
	6
WHEN a.type = 2 THEN
	2
ELSE
	0
END AS persen,
k.warehouse_id, w.`name` as warehouse_nama  
		",false);
		/*
		$this->db->select("a.id,k.id as kota_id,k.name as namakota,a.no_stc,m.nama,format(a.ewallet,0)as fewalletstc
				, md.alamat, k.timur, md.kota, md.id as deli_ad
				, a.type as tipe,a.warehouse_id, w.`name` as warehouse_nama  
			",false);
		*/
		$this->db->from('stockiest a');
        $this->db->join('member m','a.id=m.id','left');
        $this->db->join('member_delivery md','m.delivery_addr=md.id','left');
		$this->db->join('kota k','md.kota=k.id','left');
		$this->db->join('warehouse w','w.id = k.warehouse_id','inner');
		$where = "(a.status = 'active') and (a.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%') ";
        
		if($stc == '0')$where = "a.id != '$stc' and ".$where;
        elseif($stc == 'mstc')$where = "a.type = 1 and ".$where; //untuk browse hanya stockiest tdk termasuk m-stc
        elseif($stc == 'romstc')$where = "a.type = 2 and ".$where; //untuk browse hanya m-stc
        elseif($stc == 'type')$where = "a.type = '$stc' and ".$where;
        
        $this->db->where($where);
        $this->db->order_by('a.no_stc','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $num.' '.$offset.' '.$this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
		
    }
	

	public function count_search_stock_sc_whs($keywords=0){
		$this->db->from('pinjaman_titipan');
		$query = $this->db->get();
		$rowcount = $query->num_rows();
		return $query->num_rows();
    }
	
	public function search_stock_sc_whs($keywords=0,$num,$offset){

        $data = array();
        
		/*
		SELECT
a.id AS Kota_Id,
a.`name` AS City,
a.region AS Region,
a.warehouse_id AS warehouse_id,
a.propinsi_id AS Propinsi_Id,
a.ongkoskirim,
c.`name` as Item_Name 
FROM
kota AS a 
LEFT JOIN item AS c ON c.warehouse_id = a.warehouse_id WHERE a.id=5
		
		*/
		$this->db->select("a.id AS Kota_Id,
a.`name` AS City,
a.region AS Region,
a.warehouse_id AS warehouse_id,
a.propinsi_id AS Propinsi_Id,
a.ongkoskirim,
c.`name` as item_name, c.id as item_id ",false);
        $this->db->from('kota AS a ');
        $this->db->join('item c','c.warehouse_id = a.warehouse_id','left');
        $this->db->where('a.id',$keywords);
        //$this->db->group_by('d.id');
		$this->db->limit($num,$offset);
		
		
        $q = $this->db->get();
		//echo $this->db->last_query();
        $q->free_result();
        return $data;
    }
	
	public function searchMemberDeliver($keywords){
        $data = array();
        $this->db->select("a.id,a.member_id,
		a.alamat as alamat,
		a.kodepos as kodepos,
		a.kecamatan as kecamatan,
		a.kelurahan as kelurahan,
		a.pic_name as pic_name,a.pic_hp as pic_hp,
		p.name as propinsi,
		k.name as kota",false);
        $this->db->from('member_delivery a');
        $this->db->join('kota k','a.kota=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->like('a.member_id', $keywords, 'after');
        $this->db->or_like('a.id', $keywords, 'after');
        $this->db->or_like('k.name', $keywords, 'after');
        $this->db->order_by('a.created','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
	public function checkPromo($keywords=0,$promo_status,$num,$offset){
		$data = array();
		
		$this->db->select("a.*,
		(SELECT b.name as free_item_name FROM item b WHERE b.id=a.free_item_id) as Free_Item_Name , 
		b.name as nama,
		b.id as code",false);
        $this->db->from('promo a');
        $this->db->join('item b','a.item_id=b.id','left');
		$this->db->like('a.item_id', $keywords, 'after');
		if ($promo_status=='aktif'){
			$this->db->where('a.end_periode >=', 'CURDATE()', 'after');
			$this->db->where('a.expired < ', '1', 'after');
		}
        $this->db->order_by('id','desc');
		if(!empty($num)&&!empty($offset)){
			$this->db->limit($num,$offset);
		}
		$q = $this->db->get();
       // echo $num.''.$offset.''.$this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
		
        $q->free_result();
        return $data;
    }
	
    /** Voucher **/
	public function count_search_voucher($memberid,$keywords=0){
		$this->db->select("v.vouchercode",false);
		$this->db->from('voucher v');
		//$this->db->join('item i','s.item_id=i.id','left');

		$where = "v.member_id = '$memberid' and v.status= '0' and expired_date >= '".date('Y-m-d')."' and ( v.vouchercode like '%$keywords%' or v.remark like '%$keywords%' )";
		$this->db->where($where);
		return $this->db->count_all_results();
	}

	public function search_voucher($memberid, $keywords=0,$num,$offset){
		$data = array();
		/*
		$this->db->select("s.item_id,i.name,s.qty,format(s.qty,0)as fqty,i.price,format(i.price,0)as fprice,i.price2,format(i.price2,0)as fprice2,i.pv,format(i.pv,0)as fpv,i.bv",false);
		$this->db->from('stock s');
		$this->db->join('item i','s.item_id=i.id','left');
		*/
		$this->db->select("v.vouchercode, v.price, format(v.price,0) as fprice, format(v.pv,0) as pv, 
		0 as fpv, format(v.bv,0) as bv, 0 as fbv, v.remark",false);
		$this->db->from('voucher v');
		$where = "v.member_id = '$memberid' and v.status= '0' and expired_date >= '".date('Y-m-d')."' and ( v.vouchercode like '%$keywords%' or v.remark like '%$keywords%' )";
		$this->db->where($where);
		$this->db->order_by('v.expired_date','asc');
		$this->db->limit($num,$offset);
		$q = $this->db->get();
		//echo $this->db->last_query();
		if($q->num_rows > 0){
		  foreach($q->result_array() as $row){
			$data[] = $row;
		  }
		}
		$q->free_result();
		return $data;
	}
	
	/** New SC **/
	public function addSc_Head($post_data){
		$reCheck=$this->db->insert('pinjaman_titipan', $post_data);
	    if ($reCheck){
			$insert_id = $this->db->insert_id();
		}else{
			$insert_id = 'FAILED';
		}
	    return  $insert_id;
    }
	
	public function addSc_Stock($post_data){
		$reCheck=$this->db->insert('pinjaman_stock', $post_data);
	    if ($reCheck){
			$insert_id = $this->db->insert_id();
			$insert_id = 'Success';
		}else{
			$insert_id = 'FAILED';
		}
	    return  $insert_id;
	}
	
	public function addSc_Item($post_data){
		$reCheck=$this->db->insert('pinjaman_titipan_d', $post_data);
	    if ($reCheck){
			$insert_id = $this->db->insert_id();
			$insert_id = 'Success';
		}else{
			$insert_id = 'FAILED';
		}
	    return  $insert_id;
    }
	
	
	public function addPaymenySc(){
		
		$data = array(
            'member_id' => $this->input->post('member_id'),
            'stockiest_id'=>$this->session->userdata('member_id_from'),
            'date' => date('Y-m-d',now()),
			'diskon' => $this->input->post('persen'),
			'rpdiskon' => str_replace(".","",$this->input->post('totalrpdiskon')),
			'totalharga' => str_replace(".","",$this->input->post('totalbayar')),
			'totalharga2' => str_replace(".","",$this->input->post('total')),
			'totalpv' => str_replace(".","",$this->input->post('totalpv')),
			'warehouse_id' => str_replace(".","",$this->input->post('whsid')),
            'remark'=>'Create By System From SC '.$this->input->post('sc_id'),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$this->session->userdata('user') 
        );
        $this->db->insert('ro',$data);
		$ro_id = $this->db->insert_id();
        $myData=$this->input->post('iRow');
		for ($i = 0; $i <= ($myData-1); $i++) {
			if (!empty($this->input->post('qty_lunas'.$i))){
				$data_pay = array(
					'pinjaman_payment_tgl' => date('Y-m-d H:i:s',now()),
					'pinjaman_payment_ttl' => str_replace(".","", $this->input->post('subtotal'.$i)),
					'pinjaman_d_id' => $this->input->post('pin_d'.$i),
					'pinjaman_d_qty' => $this->input->post('qty_lunas'.$i),
					'warehouse_id' => $this->input->post('det_whsid'.$i),
					'item_id' => $this->input->post('itemcode'.$i),
					'stockiest_id' => $this->input->post('member_id'),
					'stockiest_id_payment' => $this->input->post('member_id_to'),
					'pinjaman_id' => $this->input->post('sc_id'),
					//'ref_no' => $this->db->escape_str($this->input->post('itemcode')),
					'ro_id' => $ro_id,
					'voucher_price' => $this->db->escape_str($this->input->post('vtotal')),
					'created' => date('Y-m-d H:i:s',now()),
					'createdby' => $this->session->userdata('user')
				);
				$this->db->insert('pinjaman_payment',$data_pay);
				
				/*
				Lewat SP..
				$dSql="UPDATE pinjaman_d SET qty_lunas = '".$this->input->post('qty_lunas'.$i)."' WHERE id='".$this->input->post('pin_d'.$i)."'";
				$q = $this->db->query($dSql);
				
				if($this->input->post('persen') >0){            
					$diskon = $this->input->post('persen');
					$rpdiskon = str_replace(",","",$this->input->post('rpdiskon'));
				}else{
					$diskon=0;
					$rpdiskon=0;
				}
				$qty = str_replace(".","",$this->input->post('qty_lunas'.$i));
				$pv = str_replace(".","",$this->input->post('pv'.$i));
			
				$hrg = str_replace(",","",$this->input->post('price'.$i));
				if(str_replace(".","",$this->input->post('pv'.$i)) > 0){$hrg = $hrg * (1-(($diskon)/100));}
				$data_ro_d=array(
					'ro_id' => $ro_id,
					'item_id' => $this->input->post('itemcode'.$i),
					'qty' => $qty,
					'harga_' => str_replace("","",$this->input->post('price'.$i)),
					'harga' => $hrg,
					'pv' => str_replace(",","",$this->input->post('pv'.$i)),
					'jmlharga' => $hrg*$qty,
					'jmlpv' => str_replace(",","",$this->input->post('totalpv'.$i))
				);
				
				$this->db->insert('ro_d',$data_ro_d);
				*/
			}
		}
         $this->db->query("call sp_sc_payment('".$this->input->post('sc_id')."')");
		
    }
	
	
	/** Voucher **/
	public function count_FreeItem($keywords=0){
		$this->db->select("
		a.*, 
		b.item_id,
		c.`name` AS item_name,
		( SELECT c. NAME AS free_item_name FROM item c WHERE c.id = d.free_item_id ) AS Free_Item_Name, 
		b.warehouse_id,
		d.item_qty,
		d.free_item_id,
		d.free_item_qty
		",false);
		$this->db->from('pinjaman_nc a');
		$this->db->join('pinjaman_d b','b.id = a.pinjaman_d_id','inner');
		$this->db->join('item c','c.id = b.item_id','inner');
		$this->db->join('promo d','d.id = a.promo_id','inner');
		$this->db->like('a.pinjaman_id', $keywords, 'after');
		$this->db->order_by('c.`name`','asc');
		$q = $this->db->get();
		$q->num_rows;
		return $q->num_rows;
	}

	public function search_FreeItem($keywords=0,$num,$offset){
		$data = array();
		/*
		$this->db->select("s.item_id,i.name,s.qty,format(s.qty,0)as fqty,i.price,format(i.price,0)as fprice,i.price2,format(i.price2,0)as fprice2,i.pv,format(i.pv,0)as fpv,i.bv",false);
		$this->db->from('stock s');
		$this->db->join('item i','s.item_id=i.id','left');
		*/
		$cSql="
		SELECT
		a.*, 
		b.item_id,
		c.`name` AS item_name,
		( SELECT c. NAME AS free_item_name FROM item c WHERE c.id = d.free_item_id ) AS Free_Item_Name, 
		b.warehouse_id,
		d.item_qty,
		d.free_item_id,
		d.free_item_qty
		FROM
		pinjaman_nc a 
		INNER JOIN pinjaman_d b ON b.id = a.pinjaman_d_id
		INNER JOIN item c ON c.id = b.item_id
		INNER JOIN promo d ON d.id = a.promo_id
		";
		
		$this->db->select("
		a.*, 
		b.item_id,
		c.`name` AS item_name,
		( SELECT c. NAME AS free_item_name FROM item c WHERE c.id = d.free_item_id ) AS Free_Item_Name, 
		b.warehouse_id,
		d.item_qty,
		d.free_item_id,
		d.free_item_qty,
		a.qty as GetFree 
		",false);
		$this->db->from('pinjaman_nc a');
		$this->db->join('pinjaman_d b','b.id = a.pinjaman_d_id','inner');
		$this->db->join('item c','c.id = b.item_id','inner');
		$this->db->join('promo d','d.id = a.promo_id','inner');
		$this->db->like('a.pinjaman_id', $keywords, 'after');
		$this->db->order_by('c.`name`','asc');
		$this->db->limit($num,$offset);
		$q = $this->db->get();
		//echo $this->db->last_query();
		if($q->num_rows > 0){
		  foreach($q->result_array() as $row){
			$data[] = $row;
		  }
		}
		$q->free_result();
		return $data;
	}
	
	/** Search City With Multiwarehouse **/
	
	public function searchCity($keywords=0,$group='',$num, $offset){
        $data = array();
        
        if($group == 'all'){
            $where = "k.name like '$keywords%' or p.name like '$keywords%' ";
        }
        $this->db->select("k.id,k.name as kota,p.name as propinsi, k.timur, k.warehouse_id,w.name as warehouse_nama",false); // Updated by Boby 20140124
        $this->db->from('kota k');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->join('warehouse w','k.warehouse_id = w.id','left');
        $this->db->where($where);
        $this->db->order_by('k.name','asc');
        $this->db->order_by('p.name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        echo $this->db->last_query();
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countSearchCity($keywords=0,$group=''){
        if($group == 'all'){
            $where = "k.name like '$keywords%' or p.name like '$keywords%' ";
        }
        $this->db->select("k.id",false);
        $this->db->from('kota k');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
	
	
   
   
	
	
}
?>