<?php
class MPromosponsor extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
	public function getPromoList($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.title,a.startdate,a.enddate,a.ps,a.free_item_id,i.name as free_item_name,a.multiples,a.maintain,a.generatelistdate,a.status,a.jmlrec,a.predec_id,a.psrec",false);
        $this->db->from('promo_sponsoring_master a');
        $this->db->join('item i','a.free_item_id=i.id','left');
        $this->db->like('a.id', $keywords, 'after');
        $this->db->or_like('a.title', $keywords, 'after');
        $this->db->or_like('i.name', $keywords, 'after');
        $this->db->order_by('a.created','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
	}
	public function getAllPromoList(){
        $data = array();
        $this->db->select("a.id,a.title,a.startdate,a.enddate,a.ps,a.free_item_id,i.name as free_item_name,a.multiples,a.maintain,a.generatelistdate,a.status,a.jmlrec,a.predec_id,a.psrec",false);
        $this->db->from('promo_sponsoring_master a');
        $this->db->join('item i','a.free_item_id=i.id','left');
        //$this->db->like('a.id', $keywords, 'after');
        //$this->db->or_like('a.title', $keywords, 'after');
        //$this->db->or_like('i.name', $keywords, 'after');
        $this->db->order_by('a.created','desc');
        //$this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
	}
    public function countPromo($keywords=0){
        $this->db->like('a.id', $keywords, 'after');
        $this->db->or_like('a.title', $keywords, 'after');
        $this->db->or_like('i.name', $keywords, 'after');
        $this->db->from('promo_sponsoring_master a');
        $this->db->join('item i','a.free_item_id=i.id','left');
        //$this->db->get();
        
        return $this->db->count_all_results();
    }
	public function add(){
        $data=array(
            'nik' => $this->input->post('nik'),
            'nama' => $this->input->post('name'),
            'grade' => $this->input->post('grade'),
            'getsaldo' => $this->input->post('saldo'),
            'saldo' => $this->input->post('saldo'),
			'active' => 1
        );
        $this->db->insert('uhn_staff_master',$data);
    }
	
	public function edit(){
        $data=array(
            'nama' => $this->input->post('name'),
            'grade' => $this->input->post('grade'),
            'saldo' => $this->input->post('saldo'),
            'active' => $this->input->post('status')
        );
        $this->db->update('uhn_staff_master',$data,array('nik'=>$this->input->post('nik')));
    }
	
	public function getPromoParticipantList($promoid,$keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.member_id,m.nama as member_name,a.ps,a.jmlrec,a.result",false);
        $this->db->from('promo_participant_header a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->where('a.promo_id', $promoid);
        $this->db->like('a.member_id', $keywords, 'after');
        $this->db->or_like('m.nama', $keywords, 'after');
        $this->db->order_by('a.member_id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
	}
	public function countPromoParticipant($promoid,$keywords=0){
        $this->db->like('a.member_id', $keywords, 'after');
        $this->db->or_like('m.nama', $keywords, 'after');
        $this->db->from('promo_participant_header a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->where('a.id',$promoid);
        //$this->db->get();
        
        return $this->db->count_all_results();
    }
}
?>