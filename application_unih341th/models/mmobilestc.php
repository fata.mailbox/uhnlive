<?php
class MMobilestc extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Request Order Stockiest
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-15
    |
    */
    
    public function searchMSTC($keywords=0,$num,$offset){
        $data = array();
        $memberid=$this->session->userdata('userid');
        $where = "a.createdby = '$memberid' AND a.no_stc LIKE '$keywords%'";
        
        $this->db->select("a.id,a.no_stc,m.nama,date_format(a.created,'%d-%b-%Y')as created,a.status",false);
        $this->db->from('stockiest a');
        $this->db->join('member m','a.id=m.id','left');
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countMSTC($keywords=0){
        $memberid=$this->session->userdata('userid');
        $where = "a.createdby = '$memberid' AND a.no_stc LIKE '$keywords%'";
        
        $this->db->select("a.id",false);
        $this->db->from('stockiest a');
        $this->db->where($where);
        return $this->db->count_all_results();
    }

    public function addRequestOrder(){
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
        $empid = $this->session->userdata('userid');
        $whsid = $this->session->userdata('whsid');
        
        $data = array(
            'member_id' => $this->input->post('member_id'),
            'stockiest_id'=>$empid,
            'date' => date('Y-m-d',now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'status' => 'delivery',
            'warehouse_id' => $whsid,
            'remarkapp'=>$this->db->escape_str($this->input->post('remark')),
            'tglapproved'=>date('Y-m-d',now()),
            'approvedby'=>$empid,
            'event_id'=>'SK1',
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('ro_temp',$data);
        
        $id = $this->db->insert_id();
        
        $qty0 = str_replace(".","",$this->input->post('qty0'));
        if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                'harga' => str_replace(".","",$this->input->post('price0')),
                'pv' => str_replace(".","",$this->input->post('pv0')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal0')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv0')),
                'titipan_id' => $this->input->post('titipan_id0')
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => str_replace(".","",$this->input->post('price1')),
                'pv' => str_replace(".","",$this->input->post('pv1')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal1')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv1')),
                'titipan_id' => $this->input->post('titipan_id1')
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => str_replace(".","",$this->input->post('price2')),
                'pv' => str_replace(".","",$this->input->post('pv2')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal2')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv2')),
                'titipan_id' => $this->input->post('titipan_id2')
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => str_replace(".","",$this->input->post('price3')),
                'pv' => str_replace(".","",$this->input->post('pv3')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal3')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv3')),
                'titipan_id' => $this->input->post('titipan_id3')
            );
            
            $this->db->insert('ro_temp_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => str_replace(".","",$this->input->post('price4')),
                'pv' => str_replace(".","",$this->input->post('pv4')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal4')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv4')),
                'titipan_id' => $this->input->post('titipan_id4')
            );
            
            $this->db->insert('ro_temp_d',$data);
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => str_replace(".","",$this->input->post('price5')),
                'pv' => str_replace(".","",$this->input->post('pv5')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal5')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv5')),
                'titipan_id' => $this->input->post('titipan_id5')
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => str_replace(".","",$this->input->post('price6')),
                'pv' => str_replace(".","",$this->input->post('pv6')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal6')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv6')),
                'titipan_id' => $this->input->post('titipan_id6')
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => str_replace(".","",$this->input->post('price7')),
                'pv' => str_replace(".","",$this->input->post('pv7')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal7')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv7')),
                'titipan_id' => $this->input->post('titipan_id7')
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => str_replace(".","",$this->input->post('price8')),
                'pv' => str_replace(".","",$this->input->post('pv8')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal8')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv8')),
                'titipan_id' => $this->input->post('titipan_id8')
            );
            
            $this->db->insert('ro_temp_d',$data);
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
       if($this->input->post('itemcode9') and $qty9 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                'harga' => str_replace(".","",$this->input->post('price9')),
                'pv' => str_replace(".","",$this->input->post('pv9')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal9')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv9')),
                'titipan_id' => $this->input->post('titipan_id9')
            );
            
            $this->db->insert('ro_temp_d',$data);
        }
        return $id;
    }
    public function check_titipan($rqtid)
    {
        $data=array();
        $q = $this->db->query("SELECT f_check_titipan('$rqtid') as l_result");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['l_result'];
    }
    public function addMobileStc($rqtid)
    {
        $empid = $this->session->userdata('userid');
        $hash = sha1(microtime());
        //daftarkan sebagai new stockiest
		//update by Boby 20100615
        $data = array(
            'id'=>$this->input->post('member_id'),
            'group_id'=>103,
            'warehouse_id'=>$this->session->userdata('whsid'),
            'no_stc'=>$this->input->post('no_stc1').$this->input->post('no_stc'),
			'kota_id'=>$this->input->post('kota_id'),
			'leader_id' => $empid,
            'type'=>2,
            'status'=>'active',
            'password' => substr(sha1(sha1(md5($hash.$this->input->post('newpasswd')))),5,15),
            'pin' => substr(sha1(sha1(md5($hash.$this->input->post('newpin')))),3,7),
            'hash' => $hash,
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $empid
        );
		//end update by Boby 20100615
        $this->db->insert('stockiest',$data);
        
        $this->db->query("call sp_ro_mstc('$rqtid','$empid')");
    }
       
    public function getStockiest($field,$member_id){
        $q = $this->db->select("id",false)
            ->from('stockiest')
            ->where($field,$member_id)
            ->get();
        return $var = ($q->num_rows()>0)? $q->row() : false;
    }
    public function getStockiestDetail($id){
        $data=array();
        $this->db->select("a.id,m.nama,a.no_stc,a.warehouse_id,w.name as warehouse,a.type,t.name as typename,a.status,date_format(a.created,'%d-%b-%Y')as created,a.createdby,a.updated,a.updatedby",false);
        $this->db->from('stockiest a');
        $this->db->join('member m','a.id=m.id','left');
        $this->db->join('type_stockiest t','a.type=t.id','left');
        $this->db->join('warehouse w','a.warehouse_id=w.id','left');
        $this->db->where('a.id',$id);
        $q = $this->db->get();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
	
	// created by Boby 20100614
	public function getKota(){
		$empid = $this->session->userdata('userid');
		
		$data=array();
        $q = $this->db->query("SELECT kota_id as l_result FROM stockiest WHERE id = '$empid' ");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['l_result'];
	}
   
}?>