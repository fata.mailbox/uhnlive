<?php
class MSales extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
	// Created by Boby 20130215
	public function get_year_report(){
        $data = array();
		$thn=date("Y");
		for($i=$thn;$i>='2009';$i--){
			$data[$i]=$i;
		}
        return $data;
    }
	public function sales_incentive($thn, $q){
        $data = array();
		$thn_ = $thn-1;
		$br = "";
		if($q!=0){
			$tgl = $q;
			$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
			$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
			
			$qry1="_, bln AS periode ".$br;
			$qry2="_, MONTH(periode) AS q ".$br;
			$qry3="HAVING periode BETWEEN MONTH($awal) AND MONTH($akhir) ".$br;
			//echo $qry3;
		}else{
			$qry1=" ";$qry2=" ";$qry3=" ";
		}
		
		$query = "
			SELECT dt.periode".$br."
				, IFNULL(r.nama,'Staff Order') AS region".$br."
				, IFNULL(SUM(omset1),0)AS omset1, IFNULL(SUM(omset2),0)AS omset2".$br."
				, IFNULL(rt.target,0)AS target".$br."
				, IFNULL(ROUND(IFNULL(SUM(omset2),0)*100 / IFNULL(rt.target,0)),2),0)AS salesVStarget".$br."
				, ROUND(IFNULL(SUM(omset2),0)*100 / IFNULL(SUM(omset1),0)),2)AS newVSold".$br."
				, IFNULL(SUM(dt.nr),0)AS nr_".$br."
				, IFNULL(SUM(dt.sf),0)AS sf".$br."
				, IFNULL(rt.nr,0)AS nr".$br."
				, IFNULL(SUM(kit1),0)AS kit1, IFNULL(SUM(kit2),0)AS kit2".$br."
				, ROUND(IFNULL(SUM(kit2),0)*100 / IFNULL(rt.nr,0)),2)AS kit1vs2".$br."
			FROM(".$br."
				SELECT *".$br."
					, CASE WHEN quart1 = quart1M THEN akota".$br."
						WHEN stockiest_id <> 0 THEN skota".$br."
						ELSE mkota".$br."
					END AS kota1".$br."
					, CASE ".$br."
						WHEN bln BETWEEN 1 AND 3 THEN 1".$br."
						WHEN bln BETWEEN 4 AND 6 THEN 2".$br."
						WHEN bln BETWEEN 7 AND 9 THEN 3".$br."
						WHEN bln BETWEEN 10 AND 12 THEN 4".$br."
					END AS periode".$qry1."".$br."
				FROM(".$br."
".$br."
					SELECT YEAR(so.tgl) AS thn, MONTH(so.tgl)AS bln".$br."
						, CASE ".$br."
							WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 1 AND 3 THEN 'qo1'	WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 4 AND 6 THEN 'qo2'".$br."
							WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 7 AND 9 THEN 'qo3'	WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 10 AND 12 THEN 'qo4'".$br."
						END AS quart1".$br."
						, CASE ".$br."
							WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 1 AND 3 THEN 'qo1'	WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 4 AND 6 THEN 'qo2'".$br."
							WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 7 AND 9 THEN 'qo3'	WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 10 AND 12 THEN 'qo4'".$br."
							ELSE 'qo5'".$br."
						END AS quart1M".$br."
						, CASE ".$br."
							WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 1 AND 3 THEN 'qn1'	WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 4 AND 6 THEN 'qn2'".$br."
							WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 7 AND 9 THEN 'qn3'	WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 10 AND 12 THEN 'qn4'".$br."
						END AS quart2".$br."
						, CASE ".$br."
							WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 1 AND 3 THEN 'qn1'	WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 4 AND 6 THEN 'qn2'".$br."
							WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 7 AND 9 THEN 'qn3'	WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 10 AND 12 THEN 'qn4'".$br."
							ELSE 'qn5'".$br."
						END AS quart2M".$br."
						, so.member_id, m.kota_id AS mkota".$br."
						, so.stockiest_id, s.kota_id AS skota".$br."
						, ma.kota_id AS akota".$br."
						, nr.nr -- , nr.sf".$br."
						, CASE WHEN YEAR(so.tgl) = $thn_ THEN totalharga END AS omset1".$br."
						, CASE WHEN YEAR(so.tgl) = $thn_ AND so.kit = 'y' THEN 1 ELSE 0 END AS kit1".$br."
						, CASE WHEN YEAR(so.tgl) = $thn THEN totalharga END AS omset2".$br."
						, CASE WHEN YEAR(so.tgl) = $thn AND so.kit = 'y' THEN 1 ELSE 0 END AS kit2".$br."
						, CASE WHEN YEAR(so.tgl) = $thn AND so.totalpv > 0 THEN 1 ELSE 0 END AS sf".$br."
					FROM so".$br."
					LEFT JOIN(".$br."
						SELECT member_id, MAX(nr)AS nr -- , 1 AS sf".$br."
						FROM (".$br."
							SELECT so.id, so.member_id, m.nama, m.created, so.tgl, so.kit, so.totalpv".$br."
								, CASE WHEN ".$br."
									MONTH(m.created) = MONTH(so.tgl) ".$br."
									AND YEAR(m.created) = YEAR(so.tgl) ".$br."
									AND so.totalpv > 0".$br."
									THEN 1 ELSE 0 ".$br."
								END AS nr".$br."
							FROM so".$br."
							LEFT JOIN member m ON so.member_id=m.id".$br."
							WHERE YEAR(tgl) = $thn".$br."
						)AS dt".$br."
						GROUP BY member_id".$br."
					)AS nr ON so.member_id = nr.member_id".$br."
					LEFT JOIN member m ON so.member_id = m.id".$br."
					LEFT JOIN stockiest s ON so.stockiest_id = s.id".$br."
					LEFT JOIN member_allocation ma ON so.member_id = ma.member_id AND YEAR(so.tgl) = YEAR(ma.periode)".$br."
					WHERE YEAR(so.tgl) BETWEEN $thn_ AND $thn".$br."
					ORDER BY quart1 DESC, ma.kota_id DESC".$br."
				)AS dt".$br."
			)AS dt".$br."
			LEFT JOIN kota k ON dt.kota1 = k.id".$br."
			LEFT JOIN region r ON k.region = r.id".$br."
			LEFT JOIN(".$br."
				SELECT CASE WHEN MONTH(periode) BETWEEN 1 AND 3 THEN 1".$br."
					WHEN MONTH(periode) BETWEEN 4 AND 6 THEN 2".$br."
					WHEN MONTH(periode) BETWEEN 7 AND 9 THEN 3".$br."
					WHEN MONTH(periode) BETWEEN 10 AND 12 THEN 4".$br."
					END AS q".$qry2."".$br."
					, region_id, SUM(target)as target, SUM(nr)AS nr".$br."
				FROM region_target".$br."
				WHERE YEAR(periode) = $thn".$br."
				GROUP BY region_id, q".$br."
			)AS rt ON r.id = rt.region_id AND dt.periode = rt.q".$br."
			GROUP BY region, periode".$br."
			".$qry3."".$br."
			ORDER BY periode, r.id".$br."
		";
		$qry = $this->db->query($query);
		// echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	// End created by Boby 20130215
    public function viewTarget($thn)
    {
        $data=array();
		$q = $this->db->query("
			SELECT rt.id, rt.periode, rt.region_id, r.nama, rt.target, rt.nr
				, MONTHNAME(rt.periode)AS namaBln, YEAR(rt.periode)AS thn
			FROM region_target rt
			LEFT JOIN region r ON rt.region_id=r.id
			WHERE YEAR(periode) = $thn
		");
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	public function get_y(){
        $data = array();
		$thn=date("Y")+1;
		for($i=$thn;$i>='2009';$i--){
			$data[$i]=$i;
		}
        return $data;
    }
	public function get_q($q){
        $data = array();
		if($q==1){$data['00-00']='All';}
		$data['01-31']='Quarter1';
		$data['04-30']='Quarter2';
		$data['07-31']='Quarter3';
		$data['10-31']='Quarter4';
        return $data;
    }
	
	public function get_region()
    {
        $data=array();
		$qry = "
			SELECT id, nama
			FROM region
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[$row['id']] = $row['nama'];
            }
        }
        $q->free_result();
        return $data;
    }
	public function cek_data_target($periode, $region)
    {
        $data=array();
		$qry = "
			SELECT id
			FROM region_target
			WHERE periode = LAST_DAY('$periode')
			AND region_id = '$region'
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
        if($q->num_rows < 1){
            $temp = "ok";
        }
        $q->free_result();
        return $temp;
    }
	
	public function insert_data_target($periode, $region, $target, $nr)
    {
		$empid = $this->session->userdata('userid');
		$qry = "
			INSERT INTO region_target(periode, region_id, target, nr, createdby)
			VALUES(LAST_DAY('$periode'), '$region', '$target', '$nr', '$empid');
		";
		$q = $this->db->query($qry);
	}
	
	public function viewAlloc($thn)
    {
        $data=array();
		$q = $this->db->query("
			SELECT ma.periode
				, CASE WHEN MONTH(ma.periode) BETWEEN 1 AND 3 THEN 'quart1'
					WHEN MONTH(ma.periode) BETWEEN 4 AND 6 THEN 'quart2'
					WHEN MONTH(ma.periode) BETWEEN 7 AND 9 THEN 'quart3'
					WHEN MONTH(ma.periode) BETWEEN 10 AND 12 THEN 'quart4'
				END AS q
				, ma.member_id, m.nama, m.kota_id, k.name AS kota1, ma.kota_id, k1.name AS kota2
			FROM member_allocation ma
			LEFT JOIN member m ON ma.member_id = m.id
			LEFT JOIN kota k ON m.kota_id = k.id
			LEFT JOIN kota k1 ON ma.kota_id = k1.id
			WHERE YEAR(periode) = $thn
		");
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	public function cek_data_allocation($periode, $member_id)
    {
        $data=array();
		$qry = "
			SELECT id
			FROM member_allocation
			WHERE periode = '$periode'
			AND member_id = '$member_id'
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
        if($q->num_rows < 1){
            $temp = "ok";
        }
        $q->free_result();
        return $temp;
    }
	public function insert_member_allocation($periode, $member_id, $kota)
    {
		$empid = $this->session->userdata('userid');
		$data=array(
			'periode' => $periode,
			'member_id' => $member_id,
			'kota_id' => $kota,
			'createdby' => $empid
		);
		
		$this->db->insert('member_allocation',$data);
	}
	public function viewStcTarget($thn, $quart)
    {
        $data=array();
		//$thn = '2012';
		//$quart = '01';
		$tgl = $thn."-".$quart;
		$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ";
		$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ";
		if($quart != 0){
			$awal_ = "CONCAT(".$awal.", ' 00:00:00')";
			$akhir_ = "CONCAT(".$akhir.", ' 23:59:59')";
		}else{
			$awal_ = "CONCAT(".$thn.", '-01-31 00:00:00')";
			$akhir_ = "CONCAT(".$thn.", '-12-31 23:59:59')";
		}
		// echo $quart;
		if($quart != 0){
			$qry = "
			SELECT s.id, m.nama, s.no_stc, s.type AS tipe, CONCAT('Region',k.region)AS region
				, IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0) AS oms1
				, IFNULL(trg1,0)AS trg1
				, IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0) AS oms2
				, IFNULL(trg2,0)AS trg2
				, IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0) AS oms3
				, IFNULL(trg3,0)AS trg3
				, (IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0))+
				  (IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0))+
				  (IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0)) AS oms
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_id = k.id
			LEFT JOIN member m ON s.id = m.id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS ro1, SUM(oms2)AS ro2, SUM(oms3)AS ro3
				FROM(
					SELECT member_id
						, CASE WHEN (MONTH(ro.`date`)=MONTH($awal) AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH($awal) AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(ro.`date`)=MONTH($awal)+1 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH($awal)+1 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(ro.`date`)=MONTH($awal)+2 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH($awal)+2 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
					FROM ro
					WHERE ro.stockiest_id=0
					AND ro.date BETWEEN $awal AND $akhir
				)AS ro_
				GROUP BY member_id
			)AS ro ON s.id = ro.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS pjm1, SUM(oms2)AS pjm2, SUM(oms3)AS pjm3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (MONTH(pjm.tgl)=MONTH($awal) AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH($awal) AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(pjm.tgl)=MONTH($awal)+1 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH($awal)+1 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(pjm.tgl)=MONTH($awal)+2 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH($awal)+2 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
					FROM pinjaman_titipan pjm
					WHERE pjm.tgl BETWEEN $awal AND $akhir
				)AS pjm
				GROUP BY member_id
			)AS pjm ON s.id = pjm.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS rtr1, SUM(oms2)AS rtr2, SUM(oms3)AS rtr3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (MONTH(rtr.tgl)=MONTH($awal) AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH($awal) AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(rtr.tgl)=MONTH($awal)+1 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH($awal)+1 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(rtr.tgl)=MONTH($awal)+2 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH($awal)+2 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
					FROM retur_titipan rtr
					WHERE rtr.tgl BETWEEN $awal AND $akhir
				)AS rtr
				GROUP BY member_id
			)AS rtr ON s.id = rtr.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS trg1, SUM(oms2)AS trg2, SUM(oms3)AS trg3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN MONTH(trg.periode)=MONTH($awal) THEN target END AS oms1
						, CASE WHEN MONTH(trg.periode)=MONTH($awal)+1 THEN target END AS oms2
						, CASE WHEN MONTH(trg.periode)=MONTH($awal)+2 THEN target END AS oms3
					FROM stockiest_target trg
					WHERE trg.periode BETWEEN $awal AND $akhir
				)AS rtr
				GROUP BY member_id
			)AS trg ON m.id = trg.member_id
			-- WHERE s.status = 'active'
			WHERE (s.created <= $akhir_ AND s.status = 'active')
			OR(s.status = 'inactive' AND s.updated <= $akhir_ AND s.updated >= $awal_)
			-- HAVING oms <> 0
			ORDER BY k.region, s.type, s.no_stc
		";
		}else{
			$qry = "
			SELECT s.id, m.nama, s.no_stc, s.type AS tipe, CONCAT('Region',k.region)AS region
				, IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0) AS oms1
				, IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0) AS oms2
				, IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0) AS oms3
				, IFNULL(ro.ro4,0)+IFNULL(pjm.pjm4,0) - IFNULL(rtr.rtr4,0) AS oms4
				, IFNULL(ro.ro5,0)+IFNULL(pjm.pjm5,0) - IFNULL(rtr.rtr5,0) AS oms5
				, IFNULL(ro.ro6,0)+IFNULL(pjm.pjm6,0) - IFNULL(rtr.rtr6,0) AS oms6
				, IFNULL(ro.ro7,0)+IFNULL(pjm.pjm7,0) - IFNULL(rtr.rtr7,0) AS oms7
				, IFNULL(ro.ro8,0)+IFNULL(pjm.pjm8,0) - IFNULL(rtr.rtr8,0) AS oms8
				, IFNULL(ro.ro9,0)+IFNULL(pjm.pjm9,0) - IFNULL(rtr.rtr9,0) AS oms9
				, IFNULL(ro.ro10,0)+IFNULL(pjm.pjm10,0) - IFNULL(rtr.rtr10,0) AS oms10
				, IFNULL(ro.ro11,0)+IFNULL(pjm.pjm11,0) - IFNULL(rtr.rtr11,0) AS oms11
				, IFNULL(ro.ro12,0)+IFNULL(pjm.pjm12,0) - IFNULL(rtr.rtr12,0) AS oms12
				, (IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0))+
				  (IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0))+
				  (IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0))+
				  (IFNULL(ro.ro4,0)+IFNULL(pjm.pjm4,0) - IFNULL(rtr.rtr4,0))+
				  (IFNULL(ro.ro5,0)+IFNULL(pjm.pjm5,0) - IFNULL(rtr.rtr5,0))+
				  (IFNULL(ro.ro6,0)+IFNULL(pjm.pjm6,0) - IFNULL(rtr.rtr6,0))+
				  (IFNULL(ro.ro7,0)+IFNULL(pjm.pjm7,0) - IFNULL(rtr.rtr7,0))+
				  (IFNULL(ro.ro8,0)+IFNULL(pjm.pjm8,0) - IFNULL(rtr.rtr8,0))+
				  (IFNULL(ro.ro9,0)+IFNULL(pjm.pjm9,0) - IFNULL(rtr.rtr9,0))+
				  (IFNULL(ro.ro10,0)+IFNULL(pjm.pjm10,0) - IFNULL(rtr.rtr10,0))+
				  (IFNULL(ro.ro11,0)+IFNULL(pjm.pjm11,0) - IFNULL(rtr.rtr11,0))+
				  (IFNULL(ro.ro12,0)+IFNULL(pjm.pjm12,0) - IFNULL(rtr.rtr12,0))
				  AS oms
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_id = k.id
			LEFT JOIN member m ON s.id = m.id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS ro1, SUM(oms2)AS ro2, SUM(oms3)AS ro3
					, SUM(oms4)AS ro4, SUM(oms5)AS ro5, SUM(oms6)AS ro6
					, SUM(oms7)AS ro7, SUM(oms8)AS ro8, SUM(oms9)AS ro9
					, SUM(oms10)AS ro10, SUM(oms11)AS ro11, SUM(oms12)AS ro12
				FROM(
					SELECT member_id
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01') AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01') AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+1 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+1 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+2 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+2 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+3 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+3 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms4
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+4 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+4 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms5
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+5 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+5 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms6
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+6 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+6 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms7
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+7 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+7 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms8
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+8 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+8 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms9
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+9 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+9 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms10
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+10 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+10 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms11
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+11 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+11 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms12
					FROM ro
					WHERE ro.stockiest_id=0
					AND YEAR(ro.date) = $thn
				)AS ro_
				GROUP BY member_id
			)AS ro ON s.id = ro.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS pjm1, SUM(oms2)AS pjm2, SUM(oms3)AS pjm3
					, SUM(oms4)AS pjm4, SUM(oms5)AS pjm5, SUM(oms6)AS pjm6
					, SUM(oms7)AS pjm7, SUM(oms8)AS pjm8, SUM(oms9)AS pjm9
					, SUM(oms10)AS pjm10, SUM(oms11)AS pjm11, SUM(oms12)AS pjm12
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01') AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+1 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+1 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+2 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+2 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+3 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+3 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms4
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+4 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+4 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms5
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+5 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+5 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms6
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+6 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+6 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms7
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+7 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+7 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms8
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+8 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+8 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms9
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+9 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+9 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms10
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+10 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+10 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms11
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+11 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+11 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms12
					FROM pinjaman_titipan pjm
					WHERE YEAR(pjm.tgl) = $thn
				)AS pjm
				GROUP BY member_id
			)AS pjm ON s.id = pjm.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS rtr1, SUM(oms2)AS rtr2, SUM(oms3)AS rtr3
					, SUM(oms4)AS rtr4, SUM(oms5)AS rtr5, SUM(oms6)AS rtr6
					, SUM(oms7)AS rtr7, SUM(oms8)AS rtr8, SUM(oms9)AS rtr9
					, SUM(oms10)AS rtr10, SUM(oms11)AS rtr11, SUM(oms12)AS rtr12
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01') AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01') AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+1 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+1 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+2 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+2 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+3 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+3 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms4
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+4 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+4 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms5
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+5 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+5 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms6
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+6 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+6 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms7
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+7 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+7 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms8
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+8 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+8 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms9
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+9 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+9 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms10
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+10 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+10 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms11
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+11 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+11 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms12
					FROM retur_titipan rtr
					WHERE YEAR(rtr.tgl) = $thn
				)AS rtr
				GROUP BY member_id
			)AS rtr ON s.id = rtr.member_id
			-- WHERE s.status = 'active'
			WHERE (s.created <= $akhir_ AND s.status = 'active')
			OR(s.status = 'inactive' AND s.updated <= $akhir_ AND s.updated >= $awal_)
			-- HAVING oms <> 0
			ORDER BY k.region, s.type, s.no_stc
			";
		}
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	public function insert_stc_target($periode, $stc, $target)
    {
		$empid = $this->session->userdata('userid');
		//$periode = "LAST_DAY('$periode') ";
		$data=array(
			'periode' => $periode,
			'stockiest_id' => $stc,
			'target' => $target,
			'createdby' => $empid
		);
		
		$this->db->insert('stockiest_target',$data);
	}
	public function cek_stc_target($periode, $stc)
    {
        $data=array();
		//$periode = "LAST_DAY('$periode') ";
		$qry = "
			SELECT id
			FROM stockiest_target
			WHERE periode = '$periode'
			AND stockiest_id = '$stc'
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
        if($q->num_rows < 1){
            $temp = "ok";
        }
        $q->free_result();
        return $temp;
    }
	public function get_bln(){
        $data = array();
		$data['01-31']='January';
		$data['02-28']='Febuary';
		$data['03-31']='March';
		$data['04-30']='April';
		$data['05-31']='May';
		$data['06-30']='June';
		$data['07-31']='Juli';
		$data['08-31']='August';
		$data['09-30']='September';
		$data['10-31']='October';
		$data['11-30']='November';
		$data['12-31']='December';
        return $data;
    }
	public function get_end_date($periode){
		$data=array();
		//$periode = "LAST_DAY('$periode') ";
		$qry = "
			SELECT LAST_DAY('$periode') AS tgl
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
		
		if($q->num_rows > 0){
			$row = $q->row_array();
			return $row['tgl'];
		}else{
			return $temp;
		}
		
        $q->free_result();
        
    }
	public function update_data_target($periode, $region, $target, $nr)
    {
		$empid = $this->session->userdata('userid');
		$qry = "
			UPDATE region_target SET target='$target', nr='$nr', updated=NOW(), updatedby='$empid'
			WHERE periode=LAST_DAY('$periode') AND region_id='$region';
		";
		$q = $this->db->query($qry);
	}
	
	/* Created by Boby 20130305 */
	public function viewAllocMember($thn, $q){
		$thn_ = $thn-1;
		if($q!=0){
			$qry = "WHERE YEAR(ma.periode) = '$thn' AND ma.periode = '$q' ";
		}else{
			$qry = "WHERE YEAR(ma.periode) = '$thn' ";
		}
        $data=array();
		$qry = "
			SELECT YEAR(ma.periode)AS thn, ma.member_id, m.nama, ma.kota_id, k.name AS kota, k.region
				, CASE 
					WHEN MONTH(ma.periode) BETWEEN 1 AND 3 THEN 'Q1'
					WHEN MONTH(ma.periode) BETWEEN 4 AND 6 THEN 'Q2'
					WHEN MONTH(ma.periode) BETWEEN 7 AND 9 THEN 'Q3'
					WHEN MONTH(ma.periode) BETWEEN 10 AND 12 THEN 'Q4'
				END AS namaBln
			FROM member_allocation ma
			LEFT JOIN member m ON ma.member_id = m.id
			LEFT JOIN kota k ON ma.kota_id = k.id
		".$qry;
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function cek_alloc_member($periode, $member_id){
        $data=array();
		$qry = "
			SELECT id
			FROM member_allocation
			WHERE periode = LAST_DAY('$periode')
			AND member_id = '$member_id'
		";
		$q = $this->db->query($qry);
		$temp = "no";
		//echo $this->db->last_query();
        if($q->num_rows < 1){
            $temp = "ok";
        }
        $q->free_result();
        return $temp;
    }
	
	public function insert_data_alloc_member($periode, $member, $kota){
		$empid = $this->session->userdata('userid');
		$qry = "
			INSERT INTO member_allocation(periode, kota_id, member_id, createdby)
			VALUES(LAST_DAY('$periode'), '$kota', '$member', '$empid');
		";
		$q = $this->db->query($qry);
	}
	/* End created by Boby 20130305 */
	
	/* Created by Boby 20130512 */
	public function getNewMember($periode){
		$thn_ = $thn-1;
        $data=array();
		$qry = "
			SELECT nm
				, SUM(mtd)AS mtd
				, SUM(lmtd)AS lmtd
				, SUM(lytd)AS lytd
			FROM(
				SELECT
					1 AS nm, vm.member_id, joindate, YEAR(joindate)AS thn, MONTH(joindate)AS bln
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') THEN 1 ELSE 0 END AS mtd
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) THEN 1 ELSE 0 END AS lmtd
					, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') THEN 1 ELSE 0 END AS lytd
				FROM v_memberjoin vm
				LEFT JOIN(
					SELECT so.member_id, YEAR(so.tgl)AS thn, MONTH(so.tgl)AS bln, SUM(totalpv)AS pv
					FROM so
					WHERE totalpv = 0
					AND so.member_id <> 'STAFF'
					AND(tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
					OR tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					GROUP BY member_id, thn, bln
				)AS dt ON vm.member_id = dt.member_id AND YEAR(vm.joindate) = dt.thn AND MONTH(vm.joindate) = dt.bln
				WHERE dt.pv = 0
				AND(joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
				OR joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
			)AS dt
			GROUP BY nm
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
		if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getNewMember_($periode){
		$thn_ = $thn-1;
        $data=array();
		$qry = "
			SELECT nm
				, SUM(rmtd)AS rmtd
				, SUM(rlmtd)AS rlmtd
				, SUM(rlytd)AS rlytd
				
				, SUM(ormtd)AS ormtd
				, SUM(orlmtd)AS orlmtd
				, SUM(orlytd)AS orlytd
				
				, SUM(nmtd)AS nmtd
				, SUM(nlmtd)AS nlmtd
				, SUM(nlytd)AS nlytd
				
				, SUM(onmtd)AS onmtd
				, SUM(onlmtd)AS onlmtd
				, SUM(onlytd)AS onlytd
			FROM(
				SELECT 1 AS nm, pv_, dt.member_id, joindate, YEAR(joindate)AS thn, MONTH(joindate)AS bln
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') AND pv_ > 0 THEN 1 ELSE 0 END AS rmtd
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND pv_ > 0 THEN 1 ELSE 0 END AS rlmtd
					, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') AND pv_ > 0 THEN 1 ELSE 0 END AS rlytd
					
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') AND pv_ > 0 THEN oms ELSE 0 END AS ormtd
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND pv_ > 0 THEN oms ELSE 0 END AS orlmtd
					, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') AND pv_ > 0 THEN oms ELSE 0 END AS orlytd
					
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') AND pv_ = 0 THEN 1 ELSE 0 END AS nmtd
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND pv_ = 0 THEN 1 ELSE 0 END AS nlmtd
					, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') AND pv_ = 0 THEN 1 ELSE 0 END AS nlytd
					
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') AND pv_ = 0 THEN oms ELSE 0 END AS onmtd
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND pv_ = 0 THEN oms ELSE 0 END AS onlmtd
					, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') AND pv_ = 0 THEN oms ELSE 0 END AS onlytd
				FROM(
					SELECT so.member_id, MAX(so.kit)AS kit_, SUM(totalpv)AS pv_, SUM(totalharga)AS oms, vm.joindate
					FROM so
					LEFT JOIN v_memberjoin vm ON so.member_id = vm.member_id
					WHERE 
					(joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
					OR joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					AND (tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
					OR tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					GROUP BY vm.member_id
					HAVING kit_ = 'y'
					ORDER BY pv_ DESC
				)AS dt
			)AS dt
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
		if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getNewRecruit($periode){
		$thn_ = $thn-1;
        $data=array();
		$qry = "
			SELECT nr
				, SUM(mtd)AS mtd
				, SUM(lmtd)AS lmtd
				, SUM(lytd)AS lytd
			FROM(
				SELECT
					1 AS nr, vm.member_id, joindate, YEAR(joindate)AS thn, MONTH(joindate)AS bln
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') THEN 1 ELSE 0 END AS mtd
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) THEN 1 ELSE 0 END AS lmtd
					, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') THEN 1 ELSE 0 END AS lytd
				FROM v_memberjoin vm
				LEFT JOIN(
					SELECT member_id, YEAR(so.tgl)AS thn, MONTH(so.tgl)AS bln, SUM(totalpv)AS pv
					FROM so
					WHERE totalpv > 0
					AND member_id <> 'STAFF'
					AND(tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
					OR tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					GROUP BY member_id, thn, bln
				)AS dt ON vm.member_id = dt.member_id AND YEAR(vm.joindate) = dt.thn AND MONTH(vm.joindate) = dt.bln
				WHERE dt.pv > 0
				AND(joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
				OR joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
			)AS dt
			GROUP BY nr
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getOmsetNewMember($periode){
		$thn_ = $thn-1;
        $data=array();
		$qry = "
			SELECT nr
				, SUM(mtd)AS mtd
				, SUM(lmtd)AS lmtd
				, SUM(lytd)AS lytd
			FROM(
				SELECT
					1 AS nr, vm.member_id, joindate, YEAR(joindate)AS thn, MONTH(joindate)AS bln
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') THEN dt.oms ELSE 0 END AS mtd
					, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) THEN dt.oms ELSE 0 END AS lmtd
					, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') THEN dt.oms ELSE 0 END AS lytd
				FROM v_memberjoin vm
				LEFT JOIN(
					SELECT member_id, YEAR(so.tgl)AS thn, MONTH(so.tgl)AS bln, SUM(totalharga)AS oms
					FROM so
					WHERE (tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
					OR tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					GROUP BY member_id, thn, bln
					-- order by member_id
				)AS dt ON vm.member_id = dt.member_id AND YEAR(vm.joindate) = dt.thn AND MONTH(vm.joindate) = dt.bln
				WHERE vm.member_id <> 'STAFF'
				AND (joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
				OR joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
			)AS dt
			GROUP BY nr
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getSponsoring($periode){
		$thn_ = $thn-1;
        $data=array();
		$qry = "
			SELECT 1 AS sp
				, SUM(mtd)AS mtd
				, SUM(lmtd)AS lmtd
				, SUM(lytd)AS lytd
			FROM(
				SELECT enroller_id, thn, bln
					, CASE WHEN thn = YEAR('$periode') AND bln = MONTH('$periode') THEN 1 ELSE 0 END AS mtd
					, CASE WHEN thn = YEAR('$periode') AND bln = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) THEN 1 ELSE 0 END AS lmtd
					, CASE WHEN thn = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') THEN 1 ELSE 0 END AS lytd
				FROM(
					SELECT
						1 AS nm, m.enroller_id, vm.member_id, joindate, YEAR(joindate)AS thn, MONTH(joindate)AS bln
						, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH('$periode') THEN 1 ELSE 0 END AS mtd
						, CASE WHEN YEAR(joindate) = YEAR('$periode') AND MONTH(joindate) = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) THEN 1 ELSE 0 END AS lmtd
						, CASE WHEN YEAR(joindate) = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) = MONTH('$periode') THEN 1 ELSE 0 END AS lytd
					FROM v_memberjoin vm
					LEFT JOIN member m ON vm.member_id = m.id
					WHERE vm.member_id <> 'STAFF'
					AND joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
					OR joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR))
					ORDER BY thn, bln, m.enroller_id
				)AS dt
				GROUP BY enroller_id, thn, bln
				ORDER BY thn, bln, enroller_id
			)AS dt
			GROUP BY sp
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getOmsetOldMember($periode){
		$thn_ = $thn-1;
        $data=array();
		$qry = "
			SELECT 'om' AS om
				, SUM(oldNow)AS oldNow
				, SUM(oldBlnLalu)AS oldBlnLalu
				, SUM(oldThnLalu)AS oldThnLalu
				, SUM(rNow)AS rNow
				, SUM(rBlnLalu)AS rBlnLalu
				, SUM(rThnLalu)AS rThnLalu
				, SUM(ooldNow)AS ooldNow
				, SUM(ooldBlnLalu)AS ooldBlnLalu
				, SUM(ooldThnLalu)AS ooldThnLalu
				, SUM(orNow)AS orNow
				, SUM(orBlnLalu)AS orBlnLalu
				, SUM(orThnLalu)AS orThnLalu
			FROM(
				SELECT member_id
					, CASE WHEN skrg+lalu = 2 THEN 1 ELSE 0 END AS oldNow
					, CASE WHEN lalu+lalu2 = 2 THEN 1 ELSE 0 END AS oldBlnLalu
					, CASE WHEN lalu11+lalu12 = 2 THEN 1 ELSE 0 END AS oldThnLalu
					, CASE WHEN skrg = 1 AND lalu = 0 THEN 1 ELSE 0 END AS rNow
					, CASE WHEN lalu = 1 AND lalu2 = 0 THEN 1 ELSE 0 END AS rBlnLalu
					, CASE WHEN lalu11 = 1 AND lalu12 = 0 THEN 1 ELSE 0 END AS rThnLalu
					, CASE WHEN skrg+lalu = 2 THEN oskrg+olalu ELSE 0 END AS ooldNow
					, CASE WHEN lalu+lalu2 = 2 THEN olalu+olalu2 ELSE 0 END AS ooldBlnLalu
					, CASE WHEN lalu11+lalu12 = 2 THEN olalu11+olalu12 ELSE 0 END AS ooldThnLalu
					, CASE WHEN skrg = 1 AND lalu = 0 THEN oskrg ELSE 0 END AS orNow
					, CASE WHEN lalu = 1 AND lalu2 = 0 THEN olalu ELSE 0 END AS orBlnLalu
					, CASE WHEN lalu11 = 1 AND lalu12 = 0 THEN olalu11 ELSE 0 END AS orThnLalu
				FROM(
					SELECT member_id, MAX(skrg)AS skrg
						, MAX(lalu)AS lalu
						, MAX(lalu2)AS lalu2
						, MAX(lalu11)AS lalu11
						, MAX(lalu12)AS lalu12
						, SUM(oskrg)AS oskrg
						, SUM(olalu)AS olalu
						, SUM(olalu2)AS olalu2
						, SUM(olalu11)AS olalu11
						, SUM(olalu12)AS olalu12
					FROM(
						SELECT member_id, tgl
							, CASE WHEN MONTH(tgl) = MONTH('$periode') AND YEAR(tgl) = YEAR('$periode') THEN 1 ELSE 0 END AS skrg
							, CASE WHEN MONTH(tgl) = MONTH('$periode')-1 AND YEAR(tgl) = YEAR('$periode') THEN 1 ELSE 0 END AS lalu
							, CASE WHEN MONTH(tgl) = MONTH('$periode')-2 AND YEAR(tgl) = YEAR('$periode') THEN 1 ELSE 0 END AS lalu2
							, CASE WHEN MONTH(tgl) = MONTH('$periode') AND YEAR(tgl) = YEAR('$periode')-1 THEN 1 ELSE 0 END AS lalu11
							, CASE WHEN MONTH(tgl) = MONTH('$periode')-1 AND YEAR(tgl) = YEAR('$periode')-1 THEN 1 ELSE 0 END AS lalu12
							, CASE WHEN MONTH(tgl) = MONTH('$periode') AND YEAR(tgl) = YEAR('$periode') THEN totalpv ELSE 0 END AS oskrg
							, CASE WHEN MONTH(tgl) = MONTH('$periode')-1 AND YEAR(tgl) = YEAR('$periode') THEN totalpv ELSE 0 END AS olalu
							, CASE WHEN MONTH(tgl) = MONTH('$periode')-2 AND YEAR(tgl) = YEAR('$periode') THEN totalpv ELSE 0 END AS olalu2
							, CASE WHEN MONTH(tgl) = MONTH('$periode') AND YEAR(tgl) = YEAR('$periode')-1 THEN totalpv ELSE 0 END AS olalu11
							, CASE WHEN MONTH(tgl) = MONTH('$periode')-1 AND YEAR(tgl) = YEAR('$periode')-1 THEN totalpv ELSE 0 END AS olalu12
						FROM so
						WHERE so.member_id <> 'STAFF'
						AND (tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 3 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode')))
						OR (tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 14 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					)AS dt
					GROUP BY member_id
				)AS dt
			)AS dt
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getOmsetOldMember_($periode){
		$thn_ = $thn-1;
        $data=array();
		$qry = "
			SELECT om
				, SUM(mtd)AS mtd
				, SUM(lmtd)AS lmtd
				, SUM(lytd)AS lytd
				
				, SUM(omtd)AS omtd
				, SUM(olmtd)AS olmtd
				, SUM(olytd)AS olytd
			FROM(
				SELECT 1 AS om, pv_, member_id, joindate, YEAR(joindate)AS thn, MONTH(joindate)AS bln
					, CASE WHEN 
						YEAR(joindate) <= YEAR('$periode') AND MONTH(joindate) <= MONTH('$periode')-1 AND pv_ > 0 
						AND tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
						THEN 1 ELSE 0 END AS mtd
					, CASE WHEN 
						YEAR(joindate) <= YEAR('$periode') AND MONTH(joindate) <= MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND pv_ > 0 
						AND tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 MONTH))
						THEN 1 ELSE 0 END AS lmtd
					, CASE WHEN YEAR(joindate) <= YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) <= MONTH('$periode')-1 AND pv_ > 0 
						AND tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR))
						THEN 1 ELSE 0 END AS lytd
					
					, CASE WHEN
						YEAR(joindate) <= YEAR('$periode') AND MONTH(joindate) <= MONTH('$periode')-1 AND pv_ > 0 
						AND tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
						THEN oms ELSE 0 END AS omtd
					, CASE WHEN
						YEAR(joindate) <= YEAR('$periode') AND MONTH(joindate) <= MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND pv_ > 0 
						AND tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 MONTH))
						THEN oms ELSE 0 END AS olmtd
					, CASE WHEN YEAR(joindate) <= YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND MONTH(joindate) <= MONTH('$periode')-1 AND pv_ > 0 
						AND tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR))
						THEN oms ELSE 0 END AS olytd
				FROM(
					SELECT so.member_id, so.tgl, MAX(so.kit)AS kit_, SUM(totalpv)AS pv_, SUM(totalharga)AS oms, vm.joindate
					FROM so
					LEFT JOIN v_memberjoin vm ON so.member_id = vm.member_id
					WHERE so.member_id <> 'STAFF'
						AND (joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 3 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 MONTH))
						OR joindate BETWEEN (LAST_DAY('$periode' - INTERVAL 14 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 13 MONTH)))
						AND (tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
						OR tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					GROUP BY vm.member_id
					HAVING kit_ = 'n'
					ORDER BY pv_ DESC
				)AS dt
			)AS dt
			GROUP BY om
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getOmsetStaff($periode){
		$thn_ = $thn-1;
        $data = array();
		$qry = "
			SELECT 'staff' as staff
				, SUM(mtd)AS mtd
				, SUM(lmtd)AS lmtd
				, SUM(lytd)AS lytd
			FROM(
				SELECT
					1 AS nr -- , vm.member_id, joindate, YEAR(joindate)AS thn, MONTH(joindate)AS bln
					, CASE WHEN thn = YEAR('$periode') AND bln = MONTH('$periode') THEN dt.oms ELSE 0 END AS mtd
					, CASE WHEN thn = YEAR('$periode') AND bln = MONTH(LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) THEN dt.oms ELSE 0 END AS lmtd
					, CASE WHEN thn = YEAR(LAST_DAY('$periode' - INTERVAL 1 YEAR)) AND bln = MONTH('$periode') THEN dt.oms ELSE 0 END AS lytd
				FROM (
					SELECT member_id, YEAR(so.tgl)AS thn, MONTH(so.tgl)AS bln, SUM(totalharga)AS oms
					FROM so
					WHERE member_id LIKE 'STAFF%'
					AND (tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode'))
					OR tgl BETWEEN (LAST_DAY('$periode' - INTERVAL 13 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY('$periode' - INTERVAL 1 YEAR)))
					GROUP BY member_id, thn, bln
				)AS dt
			)AS dt
			GROUP BY nr
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	/* End created by Boby 20130512 */
	
	/* Created by Boby 20130716 */
	
	public function getBaseModel($periode,$flag){
        $data = array();
		if($flag==0){
			$periode = "'$periode'";
		}elseif($flag==1){
			$periode = "('$periode' - INTERVAL 1 MONTH)";
		}elseif($flag==2){
			$periode = "('$periode' - INTERVAL 1 YEAR)";
		}else{
			$periode = "NOW()";
		}
		$qry = "
			SELECT 
				SUM(nm)AS nm
				, SUM(nr)AS nr
				, SUM(om)AS om
				, SUM(rm)AS rm
				, SUM(nm_oms)AS nm_oms
				, SUM(nr_oms)AS nr_oms
				, SUM(om_oms)AS om_oms
				, SUM(rm_oms)AS rm_oms
				, SUM(staff)AS staff
			FROM(
				SELECT so.member_id, m.nama, m.joindate AS joindate, so.oms, so_.pv
					, CASE WHEN m.joindate BETWEEN (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY($periode)) AND so.pv = 0 AND m.member_id <> 'STAFF' THEN 1 ELSE 0 END AS nm
					, CASE WHEN m.joindate BETWEEN (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY($periode)) AND so.pv <> 0 AND m.member_id <> 'STAFF' THEN 1 ELSE 0 END AS nr
					, CASE WHEN m.joindate < (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND so_.pv <> 0 AND m.member_id <> 'STAFF' THEN 1 ELSE 0 END AS om
					, CASE WHEN m.joindate < (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND IFNULL(so_.pv,0) = 0 AND m.member_id <> 'STAFF' THEN 1 ELSE 0 END AS rm
					
					, CASE WHEN m.joindate BETWEEN (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY($periode)) AND so.pv = 0 AND m.member_id <> 'STAFF' THEN so.oms ELSE 0 END AS nm_oms
					, CASE WHEN m.joindate BETWEEN (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY($periode)) AND so.pv <> 0 AND m.member_id <> 'STAFF' THEN so.oms ELSE 0 END AS nr_oms
					, CASE WHEN m.joindate < (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND so_.pv <> 0 AND m.member_id <> 'STAFF' THEN so.oms ELSE 0 END AS om_oms
					, CASE WHEN m.joindate < (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND IFNULL(so_.pv,0) = 0 AND m.member_id <> 'STAFF' THEN so.oms ELSE 0 END AS rm_oms
					, CASE WHEN so.member_id = 'STAFF' THEN so.oms ELSE 0 END AS staff
				FROM(
					SELECT YEAR(so.tgl)AS thn, MONTH(so.tgl)AS bln, so.member_id, SUM(so.totalharga)AS oms, SUM(totalpv)AS pv
					FROM so
					WHERE so.tgl BETWEEN (LAST_DAY($periode - INTERVAL 1 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY($periode))
					GROUP BY member_id, thn, bln
				)AS so
				LEFT JOIN(
					SELECT so.member_id, SUM(so.totalharga)AS pv
					FROM so
					WHERE so.tgl BETWEEN (LAST_DAY($periode - INTERVAL 2 MONTH) + INTERVAL 1 DAY) AND (LAST_DAY($periode - INTERVAL 1 MONTH))
					GROUP BY member_id
				)AS so_ ON so.member_id = so_.member_id
				LEFT JOIN(
					SELECT member_id, nama, CASE WHEN joindate > (LAST_DAY($periode)) THEN (LAST_DAY($periode)) ELSE joindate END AS joindate
					FROM v_memberjoin m 
				)AS m ON so.member_id = m.member_id

			)AS dt
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	
	public function getTarget($periode,$flag){
        $data = array();
		if($flag==0){
			$periode = "'$periode'";
		}elseif($flag==1){
			$periode = "(LAST_DAY('$periode' - INTERVAL 1 MONTH))";
		}elseif($flag==2){
			$periode = "(LAST_DAY('$periode' - INTERVAL 1 YEAR))";
		}else{
			$periode = "(LAST_DAY(NOW()))";
		}
		$qry = "
			SELECT periode, SUM(target)AS target
			FROM region_target
			WHERE periode = $periode
			GROUP BY periode
		";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows()>0){$data=$q->row_array();}
		$q->free_result();
		return $data;
    }
	/* End created by Boby 20130716 */
	
	/* Created by Boby $thn0911 */
	public function sales_incentive_new($thn, $q){
        $data = array();
		$thn_ = $thn-1;
		if($q!=0){
			$tgl = $q;
			$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ";
			$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ";
			
			$qry1="_, bln AS periode ";
			$qry2="_, MONTH(periode) AS q ";
			$qry3="HAVING periode BETWEEN MONTH($awal) AND MONTH($akhir) ";
			//echo $qry3;
		}else{
			$qry1=" ";$qry2=" ";$qry3=" ";
		}
		
		$query = "
			SELECT dt.periode
				, IFNULL(r.nama,'Staff Order') AS region
				, IFNULL(SUM(omset1),0)AS omset1, IFNULL(SUM(omset2),0)AS omset2
				, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(IFNULL(SUM(omset2),0)*100 / IFNULL(rt.target,0)),2),0)AS salesVStarget
				, ROUND(IFNULL(SUM(omset2),0)*100 / IFNULL(SUM(omset1),0)),2)AS newVSold
				, IFNULL(SUM(dt.nr),0)AS nr_
				, IFNULL(SUM(dt.sf),0)AS sf
				, IFNULL(rt.nr,0)AS nr
				, IFNULL(SUM(kit1),0)AS kit1, IFNULL(SUM(kit2),0)AS kit2
				, IFNULL(SUM(nr2),0)AS nr2
				, ROUND(IFNULL(SUM(kit2),0)*100 / IFNULL(rt.nr,0)),2)AS kit1vs2
			FROM(
				SELECT *
					, CASE WHEN quart1 = quart1M THEN akota
						WHEN stockiest_id <> 0 THEN skota
						ELSE mkota
					END AS kota1
					, CASE 
						WHEN bln BETWEEN 1 AND 3 THEN 1
						WHEN bln BETWEEN 4 AND 6 THEN 2
						WHEN bln BETWEEN 7 AND 9 THEN 3
						WHEN bln BETWEEN 10 AND 12 THEN 4
					END AS periode".$qry1."
				FROM(

					SELECT YEAR(so.tgl) AS thn, MONTH(so.tgl)AS bln
						, CASE 
							WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 1 AND 3 THEN 'qo1'	WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 4 AND 6 THEN 'qo2'
							WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 7 AND 9 THEN 'qo3'	WHEN YEAR(so.tgl) = $thn_ AND MONTH(so.tgl) BETWEEN 10 AND 12 THEN 'qo4'
						END AS quart1
						, CASE 
							WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 1 AND 3 THEN 'qo1'	WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 4 AND 6 THEN 'qo2'
							WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 7 AND 9 THEN 'qo3'	WHEN YEAR(ma.periode) = $thn_ AND MONTH(ma.periode) BETWEEN 10 AND 12 THEN 'qo4'
							ELSE 'qo5'
						END AS quart1M
						, CASE 
							WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 1 AND 3 THEN 'qn1'	WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 4 AND 6 THEN 'qn2'
							WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 7 AND 9 THEN 'qn3'	WHEN YEAR(so.tgl) = $thn AND MONTH(so.tgl) BETWEEN 10 AND 12 THEN 'qn4'
						END AS quart2
						, CASE 
							WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 1 AND 3 THEN 'qn1'	WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 4 AND 6 THEN 'qn2'
							WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 7 AND 9 THEN 'qn3'	WHEN YEAR(ma.periode) = $thn AND MONTH(ma.periode) BETWEEN 10 AND 12 THEN 'qn4'
							ELSE 'qn5'
						END AS quart2M
						, so.member_id, m.kota_id AS mkota
						, so.stockiest_id, s.kota_id AS skota
						, ma.kota_id AS akota
						, nr.nr -- , nr.sf
						, CASE WHEN YEAR(so.tgl) = $thn_ THEN totalharga END AS omset1
						, CASE WHEN YEAR(so.tgl) = $thn_ AND so.kit = 'y' THEN 1 ELSE 0 END AS kit1
						, CASE WHEN YEAR(so.tgl) = $thn THEN totalharga END AS omset2
						, CASE WHEN YEAR(so.tgl) = $thn AND so.kit = 'y' THEN 1 ELSE 0 END AS kit2
						, nr2.nr AS nr2
						, CASE WHEN YEAR(so.tgl) = $thn AND so.totalpv > 0 THEN 1 ELSE 0 END AS sf
					FROM so
					LEFT JOIN(
						SELECT member_id, MAX(nr)AS nr -- , 1 AS sf
						FROM (
							SELECT so.id, so.member_id, m.nama, m.created, so.tgl, so.kit, so.totalpv
								, CASE WHEN 
									MONTH(m.created) = MONTH(so.tgl) 
									AND YEAR(m.created) = YEAR(so.tgl) 
									AND so.totalpv > 0
									THEN 1 ELSE 0 
								END AS nr
							FROM so
							LEFT JOIN member m ON so.member_id=m.id
							WHERE YEAR(tgl) = $thn
						)AS dt
						GROUP BY member_id
					)AS nr ON so.member_id = nr.member_id
					LEFT JOIN member m ON so.member_id = m.id
					LEFT JOIN(
						SELECT member_id, MAX(nr)AS nr -- , 1 AS sf
						FROM (
							SELECT so.id
								, YEAR(m.created)AS thn, MONTH(m.created)AS bln
								, so.member_id, m.nama, m.created, so.tgl, so.kit, so.totalpv
								, CASE WHEN 
									MONTH(m.created) = MONTH(so.tgl) 
									AND YEAR(m.created) = YEAR(so.tgl) 
									AND so.totalpv > 0
									THEN 1 ELSE 0 
								END AS nr
							FROM so
							LEFT JOIN member m ON so.member_id=m.id
							WHERE YEAR(tgl) = $thn
						)AS dt
						GROUP BY member_id
					)AS nr2 ON m.id = nr2.member_id AND so.kit = 'Y'
					LEFT JOIN stockiest s ON so.stockiest_id = s.id
					LEFT JOIN member_allocation ma ON so.member_id = ma.member_id AND YEAR(so.tgl) = YEAR(ma.periode)
					WHERE YEAR(so.tgl) BETWEEN $thn_ AND $thn
					ORDER BY quart1 DESC, ma.kota_id DESC
				)AS dt
			)AS dt
			LEFT JOIN kota k ON dt.kota1 = k.id
			LEFT JOIN region r ON k.region = r.id
			LEFT JOIN(
				SELECT CASE WHEN MONTH(periode) BETWEEN 1 AND 3 THEN 1
					WHEN MONTH(periode) BETWEEN 4 AND 6 THEN 2
					WHEN MONTH(periode) BETWEEN 7 AND 9 THEN 3
					WHEN MONTH(periode) BETWEEN 10 AND 12 THEN 4
					END AS q".$qry2."
					, region_id, SUM(target)as target, SUM(nr)AS nr
				FROM region_target
				WHERE YEAR(periode) = $thn
				GROUP BY region_id, q
			)AS rt ON r.id = rt.region_id AND dt.periode = rt.q
			GROUP BY region, periode
			".$qry3."
			ORDER BY periode, r.id
		";
		$qry = $this->db->query($query);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	// End created by Boby 20130215
	
	// Created by Boby $thn0922
	public function viewStcTargetNew($thn, $quart)
    {
        $data=array();
		//$thn = '2012';
		//$quart = '01';
		$tgl = $thn."-".$quart;
		$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ";
		$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ";
		//echo $tgl;
		$q = $this->db->query("
			SELECT s.id, m.nama, s.no_stc, s.type AS tipe, k.region
				, IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0) AS oms1
				, IFNULL(trg1,0)AS trg1
				, IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0) AS oms2
				, IFNULL(trg2,0)AS trg2
				, IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0) AS oms3
				, IFNULL(trg3,0)AS trg3
				, (IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0))+
				  (IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0))+
				  (IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0)) AS oms
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_id = k.id
			LEFT JOIN member m ON s.id = m.id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS ro1, SUM(oms2)AS ro2, SUM(oms3)AS ro3
				FROM(
					SELECT member_id
						, CASE WHEN MONTH(ro.`date`)=MONTH($awal) THEN jmlharga END AS oms1
						, CASE WHEN MONTH(ro.`date`)=MONTH($awal)+1 THEN jmlharga END AS oms2
						, CASE WHEN MONTH(ro.`date`)=MONTH($awal)+2 THEN jmlharga END AS oms3
					FROM ro
					LEFT JOIN ro_d rod ON ro.id = rod.ro_id
					WHERE ro.stockiest_id=0
					AND rod.pv > 0
					AND ro.date BETWEEN $awal AND $akhir
				)AS ro_
				GROUP BY member_id
			)AS ro ON s.id = ro.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS pjm1, SUM(oms2)AS pjm2, SUM(oms3)AS pjm3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN MONTH(pjm.tgl)=MONTH($awal) THEN jmlharga END AS oms1
						, CASE WHEN MONTH(pjm.tgl)=MONTH($awal)+1 THEN jmlharga END AS oms2
						, CASE WHEN MONTH(pjm.tgl)=MONTH($awal) THEN jmlharga END AS oms3
					FROM pinjaman_titipan pjm
					LEFT JOIN pinjaman_titipan_d pd ON pjm.id = pd.pinjaman_titipan_id
					WHERE pjm.tgl BETWEEN $awal AND $akhir
					AND pd.pv > 0
				)AS pjm
				GROUP BY member_id
			)AS pjm ON s.id = pjm.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS rtr1, SUM(oms2)AS rtr2, SUM(oms3)AS rtr3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN MONTH(rtr.tgl)=MONTH($awal) THEN jmlharga END AS oms1
						, CASE WHEN MONTH(rtr.tgl)=MONTH($awal)+1 THEN jmlharga END AS oms2
						, CASE WHEN MONTH(rtr.tgl)=MONTH($awal)+2 THEN jmlharga END AS oms3
					FROM retur_titipan rtr
					LEFT JOIN retur_titipan_d rd ON rtr.id = rd.retur_titipan_id
					WHERE rtr.tgl BETWEEN $awal AND $akhir
					AND rd.pv > 0
				)AS rtr
				GROUP BY member_id
			)AS rtr ON s.id = rtr.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS trg1, SUM(oms2)AS trg2, SUM(oms3)AS trg3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN MONTH(trg.periode)=MONTH($awal) THEN target END AS oms1
						, CASE WHEN MONTH(trg.periode)=MONTH($awal)+1 THEN target END AS oms2
						, CASE WHEN MONTH(trg.periode)=MONTH($awal)+2 THEN target END AS oms3
					FROM stockiest_target trg
					WHERE trg.periode BETWEEN $awal AND $akhir
				)AS rtr
				GROUP BY member_id
			)AS trg ON m.id = trg.member_id
			HAVING oms <> 0
			ORDER BY k.region, s.type, s.no_stc
			
		");
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	// End created by Boby $thn0922
	
	/* Created by Boby $thn1016 */
	public function sales_incentive_rpt($thn, $q){
        $bln =  substr($q, -5, 2);
		
		$qry11 = "
				, rs.omset_ytd AS omset1, rs.omset AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset*100/ omset_ytd)AS newVSold
			";
		
		/*
		if($bln > 9 && $thn > 2013){
			$qry11 = "
				, rs.omset_ytd AS omset1, rs.omset AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset*100/ omset_ytd)AS newVSold
			";
		}else{
			$qry11 = "
				, rs.omset_ytd/1.1 AS omset1, rs.omset/1.1 AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset/1.1)*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset/1.1)*100/ (omset_ytd/1.1))AS newVSold
			";
		}
		*/
		
		$data = array();
		$thn_ = $thn-1;
		$br = "<br>";
		$br = "";
		// echo $q;
		if($q!=0){
			// Jika tidak all
			$tgl = $q;
			$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
			$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
			
			$qry1="_, bln AS periode ".$br;
			$qry2="_, MONTH(rs.periode) AS periode ".$br;
			$qry3="HAVING periode BETWEEN MONTH($awal) AND MONTH($akhir) ".$br;
			
			$bln1_ = date('n')-2;
			$thn1_ = date('Y');

			if($bln >= $bln1_ && $thn = $thn1_){
				$query = "CALL report_sls_inc(NOW()); ";
				$qry = $this->db->query($query);
			}
			
			$query1 = " ";
			$query2 = " ";
			
		}else{
			// Jika all
			$qry1=" ";$qry2=" ";$qry3=" ";
			$awal = "'".$thn."-01-01' ".$br;
			// $akhir = "NOW() ".$br;
			$akhir = "'".$thn."-12-31' ".$br;
			$query = "CALL report_sls_inc(NOW()); ";
			$qry = $this->db->query($query);
			
			$query1 = "
			SELECT periode_ AS periode, region, SUM(omset1)AS omset1, SUM(omset2)AS omset2, SUM(target) AS target, salesVStarget, newVSold
				, SUM(nr2)AS nr2, SUM(kit2)AS kit2, SUM(nr)AS nr, SUM(recruit)AS recruit
			FROM(
			";
			
			$query2 = "
				)AS dt
			GROUP BY periode_, region
			";
		}
		
		$query = $query1."
			SELECT CASE  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 1 AND 3 THEN 1  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 4 AND 6 THEN 2  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 7 AND 9 THEN 3  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 10 AND 12 THEN 4  ".$br."
				END AS periode_, MONTH(rs.periode) AS periode  ".$br."
				, IFNULL(r.nama,'STAFF') AS region  ".$br."
				, rs.omset_ytd AS omset1, rs.omset AS omset2 ".$br."
				, IFNULL(rt.target,0)AS target  ".$br."
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0)),0) AS salesVStarget  ".$br."
				, ROUND(omset*100/ omset_ytd)AS newVSold  ".$br."
				, rs.nr AS nr2  ".$br."
				, rs.nm AS kit2  ".$br."
				, rt.nr AS nr  ".$br."
				, SUM(IFNULL(rs.recruit,0))AS recruit  ".$br."
				, IFNULL(ROUND(rs.nm*100/(rt.nr)),0) AS kit1VSkit2  ".$br."
			FROM report_salesinc rs  ".$br."
			LEFT JOIN region r ON rs.region = r.id  ".$br."
			LEFT JOIN region_target rt ON rs.region = rt.region_id AND rs.periode = rt.periode  ".$br."
			WHERE rs.periode BETWEEN $awal AND $akhir ".$br."
			GROUP BY periode, region  ".$br."
			".$query2
		;
		
		$qry = $this->db->query($query);
		// echo $this->db->last_query();
		
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	/* End created by Boby $thn1016 */
	
	/*
	* START ASP 20150506
	* Sales By City Report
	*/
	public function sales_by_city($thn){	
        $data=array();
		//$thn = '2012';
		//$quart = '01';
		//$tgl = $thn."-".$quart;
		$awal = $thn.'-01-01';//"LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ";
		$akhir = $thn.'-12-31';//"LAST_DAY('$tgl' + INTERVAL 2 MONTH) ";
		//echo $tgl;
		/*
		$query = "
		SELECT 
		id, kota
		,SUM(omset1) AS omset1
		,SUM(omset2) AS omset2
		,SUM(omset3) AS omset3
		,SUM(omset4) AS omset4
		,SUM(omset5) AS omset5
		,SUM(omset6) AS omset6
		,SUM(omset7) AS omset7
		,SUM(omset8) AS omset8
		,SUM(omset9) AS omset9
		,SUM(omset10) AS omset10
		,SUM(omset11) AS omset11
		,SUM(omset12) AS omset12
		FROM (
			SELECT k.id,k.name AS kota, k.kota_exp
				, IFNULL(so.totalharga1,0)+IFNULL(ro.totalharga1,0)+IFNULL(scp.totalharga1,0)-IFNULL(rtr.totalharga1,0) AS omset1
				, IFNULL(so.totalharga2,0)+IFNULL(ro.totalharga2,0)+IFNULL(scp.totalharga2,0)-IFNULL(rtr.totalharga2,0) AS omset2
				, IFNULL(so.totalharga3,0)+IFNULL(ro.totalharga3,0)+IFNULL(scp.totalharga3,0)-IFNULL(rtr.totalharga3,0) AS omset3
				, IFNULL(so.totalharga4,0)+IFNULL(ro.totalharga4,0)+IFNULL(scp.totalharga4,0)-IFNULL(rtr.totalharga4,0) AS omset4
				, IFNULL(so.totalharga5,0)+IFNULL(ro.totalharga5,0)+IFNULL(scp.totalharga5,0)-IFNULL(rtr.totalharga5,0) AS omset5
				, IFNULL(so.totalharga6,0)+IFNULL(ro.totalharga6,0)+IFNULL(scp.totalharga6,0)-IFNULL(rtr.totalharga6,0) AS omset6
				, IFNULL(so.totalharga7,0)+IFNULL(ro.totalharga7,0)+IFNULL(scp.totalharga7,0)-IFNULL(rtr.totalharga7,0) AS omset7
				, IFNULL(so.totalharga8,0)+IFNULL(ro.totalharga8,0)+IFNULL(scp.totalharga8,0)-IFNULL(rtr.totalharga8,0) AS omset8
				, IFNULL(so.totalharga9,0)+IFNULL(ro.totalharga9,0)+IFNULL(scp.totalharga9,0)-IFNULL(rtr.totalharga9,0) AS omset9
				, IFNULL(so.totalharga10,0)+IFNULL(ro.totalharga10,0)+IFNULL(scp.totalharga10,0)-IFNULL(rtr.totalharga10,0) AS omset10
				, IFNULL(so.totalharga11,0)+IFNULL(ro.totalharga11,0)+IFNULL(scp.totalharga11,0)-IFNULL(rtr.totalharga11,0) AS omset11
				, IFNULL(so.totalharga12,0)+IFNULL(ro.totalharga12,0)+IFNULL(scp.totalharga12,0)-IFNULL(rtr.totalharga12,0) AS omset12	
			FROM kota k
			LEFT JOIN(
				SELECT kota
					, SUM(totalharga1)AS totalharga1, SUM(totalhargakit1)AS totalhargakit1
					, SUM(totalharga2)AS totalharga2, SUM(totalhargakit2)AS totalhargakit2
					, SUM(totalharga3)AS totalharga3, SUM(totalhargakit3)AS totalhargakit3
					, SUM(totalharga4)AS totalharga4, SUM(totalhargakit4)AS totalhargakit4
					, SUM(totalharga5)AS totalharga5, SUM(totalhargakit5)AS totalhargakit5
					, SUM(totalharga6)AS totalharga6, SUM(totalhargakit6)AS totalhargakit6
					, SUM(totalharga7)AS totalharga7, SUM(totalhargakit7)AS totalhargakit7
					, SUM(totalharga8)AS totalharga8, SUM(totalhargakit8)AS totalhargakit8
					, SUM(totalharga9)AS totalharga9, SUM(totalhargakit9)AS totalhargakit9
					, SUM(totalharga10)AS totalharga10, SUM(totalhargakit10)AS totalhargakit10
					, SUM(totalharga11)AS totalharga11, SUM(totalhargakit11)AS totalhargakit11
					, SUM(totalharga12)AS totalharga12, SUM(totalhargakit12)AS totalhargakit12
				FROM(
					SELECT MONTH(tgl)AS bln
						, so.statuspu
						, CASE WHEN so.statuspu = '0' THEN 'UHN' ELSE md.kota END AS kota
						, CASE WHEN MONTH(tgl) = 1 AND kit = 'N' THEN totalharga ELSE 0 END AS totalharga1, CASE WHEN MONTH(tgl) = 1 AND kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit1
						, CASE WHEN MONTH(tgl) = 2 AND kit = 'N' THEN totalharga ELSE 0 END AS totalharga2, CASE WHEN MONTH(tgl) = 2 AND kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit2
						, CASE WHEN MONTH(tgl) = 3 AND kit = 'N' THEN totalharga ELSE 0 END AS totalharga3, CASE WHEN MONTH(tgl) = 3 AND kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit3
						, CASE WHEN MONTH(tgl) = 4 AND kit = 'N' THEN totalharga ELSE 0 END AS totalharga4, CASE WHEN MONTH(tgl) = 4 AND kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit4
						, CASE WHEN MONTH(tgl) = 5 AND kit = 'N' THEN totalharga ELSE 0 END AS totalharga5, CASE WHEN MONTH(tgl) = 5 AND kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit5
						, CASE WHEN MONTH(tgl) = 6 AND kit = 'N' THEN totalharga ELSE 0 END AS totalharga6, CASE WHEN MONTH(tgl) = 6 AND kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit6
						, CASE WHEN MONTH(tgl) = 7 AND kit = 'N' THEN totalharga ELSE 0 END AS totalharga7, CASE WHEN MONTH(tgl) = 7 AND kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit7
						, CASE WHEN MONTH(tgl) = 8 AND kit = 'N' THEN totalharga ELSE 0 END AS totalharga8, CASE WHEN MONTH(tgl) = 8 AND kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit8
						, CASE WHEN MONTH(tgl) = 9 AND kit = 'N' THEN totalharga ELSE 0 END AS totalharga9, CASE WHEN MONTH(tgl) = 9 AND kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit9
						, CASE WHEN MONTH(tgl) = 10 AND kit = 'N' THEN totalharga ELSE 0 END AS totalharga10, CASE WHEN MONTH(tgl) = 10 AND kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit10
						, CASE WHEN MONTH(tgl) = 11 AND kit = 'N' THEN totalharga ELSE 0 END AS totalharga11, CASE WHEN MONTH(tgl) = 11 AND kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit11
						, CASE WHEN MONTH(tgl) = 12 AND kit = 'N' THEN totalharga ELSE 0 END AS totalharga12, CASE WHEN MONTH(tgl) = 12 AND kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit12
						
					FROM so
					LEFT JOIN member_delivery md ON so.delivery_addr = md.id
					WHERE tgl BETWEEN '".$awal."' AND '".$akhir."'
					AND stockiest_id = 0
				)AS dt
				LEFT JOIN kota k ON dt.kota = k.id
				GROUP BY kota
			)AS so ON k.id = so.kota
			LEFT JOIN(
				-- RO
				SELECT k.kota_exp, kota, k.name AS kota_
					, SUM(totalharga1)AS totalharga1, SUM(totalhargakit1)AS totalhargakit1
					, SUM(totalharga2)AS totalharga2, SUM(totalhargakit2)AS totalhargakit2
					, SUM(totalharga3)AS totalharga3, SUM(totalhargakit3)AS totalhargakit3
					, SUM(totalharga4)AS totalharga4, SUM(totalhargakit4)AS totalhargakit4
					, SUM(totalharga5)AS totalharga5, SUM(totalhargakit5)AS totalhargakit5
					, SUM(totalharga6)AS totalharga6, SUM(totalhargakit6)AS totalhargakit6
					, SUM(totalharga7)AS totalharga7, SUM(totalhargakit7)AS totalhargakit7
					, SUM(totalharga8)AS totalharga8, SUM(totalhargakit8)AS totalhargakit8
					, SUM(totalharga9)AS totalharga9, SUM(totalhargakit9)AS totalhargakit9
					, SUM(totalharga10)AS totalharga10, SUM(totalhargakit10)AS totalhargakit10
					, SUM(totalharga11)AS totalharga11, SUM(totalhargakit11)AS totalhargakit11
					, SUM(totalharga12)AS totalharga12, SUM(totalhargakit12)AS totalhargakit12
				FROM(
					SELECT MONTH(`date`)AS bln
						, ro.delivery
						-- , CASE WHEN ro.delivery = '0' THEN 'UHN' ELSE md.kota END AS kota
						, stc.kota_id AS kota
						, CASE WHEN MONTH(`date`) = 1 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga1, CASE WHEN MONTH(`date`) = 1 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit1
						, CASE WHEN MONTH(`date`) = 2 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga2, CASE WHEN MONTH(`date`) = 2 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit2
						, CASE WHEN MONTH(`date`) = 3 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga3, CASE WHEN MONTH(`date`) = 3 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit3
						, CASE WHEN MONTH(`date`) = 4 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga4, CASE WHEN MONTH(`date`) = 4 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit4
						, CASE WHEN MONTH(`date`) = 5 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga5, CASE WHEN MONTH(`date`) = 5 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit5
						, CASE WHEN MONTH(`date`) = 6 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga6, CASE WHEN MONTH(`date`) = 6 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit6
						, CASE WHEN MONTH(`date`) = 7 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga7, CASE WHEN MONTH(`date`) = 7 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit7
						, CASE WHEN MONTH(`date`) = 8 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga8, CASE WHEN MONTH(`date`) = 8 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit8
						, CASE WHEN MONTH(`date`) = 9 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga9, CASE WHEN MONTH(`date`) = 9 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit9
						, CASE WHEN MONTH(`date`) = 10 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga10, CASE WHEN MONTH(`date`) = 10 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit10
						, CASE WHEN MONTH(`date`) = 11 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga11, CASE WHEN MONTH(`date`) = 11 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit11
						, CASE WHEN MONTH(`date`) = 12 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga12, CASE WHEN MONTH(`date`) = 12 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit12
					FROM ro
					RIGHT JOIN ro_d rod ON ro.id = rod.ro_id
					-- LEFT JOIN member_delivery md ON ro.deliv_addr= md.id
					LEFT JOIN stockiest stc ON ro.member_id = stc.id
					WHERE `date` BETWEEN '".$awal."' AND '".$akhir."'
					AND stockiest_id = 0
				)AS dt
				LEFT JOIN kota k ON dt.kota = k.id
				GROUP BY kota
			)AS ro ON k.id = ro.kota
			LEFT JOIN(
				-- Retur
				SELECT stc.kota_id AS kota
				-- ,rk.kota_exp
				, ro.tgl, rod.item_id, rod.jmlharga, rod.jmlpv
					, CASE WHEN MONTH(tgl) = 1 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga1, CASE WHEN MONTH(tgl) = 1 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit1
					, CASE WHEN MONTH(tgl) = 2 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga2, CASE WHEN MONTH(tgl) = 2 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit2
					, CASE WHEN MONTH(tgl) = 3 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga3, CASE WHEN MONTH(tgl) = 3 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit3
					, CASE WHEN MONTH(tgl) = 4 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga4, CASE WHEN MONTH(tgl) = 4 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit4
					, CASE WHEN MONTH(tgl) = 5 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga5, CASE WHEN MONTH(tgl) = 5 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit5
					, CASE WHEN MONTH(tgl) = 6 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga6, CASE WHEN MONTH(tgl) = 6 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit6
					, CASE WHEN MONTH(tgl) = 7 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga7, CASE WHEN MONTH(tgl) = 7 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit7
					, CASE WHEN MONTH(tgl) = 8 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga8, CASE WHEN MONTH(tgl) = 8 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit8
					, CASE WHEN MONTH(tgl) = 9 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga9, CASE WHEN MONTH(tgl) = 9 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit9
					, CASE WHEN MONTH(tgl) = 10 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga10, CASE WHEN MONTH(tgl) = 10 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit10
					, CASE WHEN MONTH(tgl) = 11 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga11, CASE WHEN MONTH(tgl) = 11 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit11
					, CASE WHEN MONTH(tgl) = 12 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga12, CASE WHEN MONTH(tgl) = 12 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit12
				FROM retur_titipan ro
				RIGHT JOIN retur_titipan_d rod ON ro.id = rod.retur_titipan_id
				LEFT JOIN stockiest stc ON ro.stockiest_id = stc.id
				-- left join kota rk on stc.kota_id = rk.id
				WHERE tgl BETWEEN '".$awal."' AND '".$akhir."'
			)AS rtr ON k.id = rtr.kota
			LEFT JOIN(
				-- SC Payment
				SELECT stc.kota_id AS kota
				-- ,rk.kota_exp
				,ro.tgl, rod.item_id, rod.jmlharga, rod.jmlpv
					, CASE WHEN MONTH(tgl) = 1 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga1, CASE WHEN MONTH(tgl) = 1 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit1
					, CASE WHEN MONTH(tgl) = 2 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga2, CASE WHEN MONTH(tgl) = 2 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit2
					, CASE WHEN MONTH(tgl) = 3 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga3, CASE WHEN MONTH(tgl) = 3 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit3
					, CASE WHEN MONTH(tgl) = 4 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga4, CASE WHEN MONTH(tgl) = 4 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit4
					, CASE WHEN MONTH(tgl) = 5 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga5, CASE WHEN MONTH(tgl) = 5 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit5
					, CASE WHEN MONTH(tgl) = 6 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga6, CASE WHEN MONTH(tgl) = 6 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit6
					, CASE WHEN MONTH(tgl) = 7 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga7, CASE WHEN MONTH(tgl) = 7 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit7
					, CASE WHEN MONTH(tgl) = 8 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga8, CASE WHEN MONTH(tgl) = 8 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit8
					, CASE WHEN MONTH(tgl) = 9 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga9, CASE WHEN MONTH(tgl) = 9 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit9
					, CASE WHEN MONTH(tgl) = 10 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga10, CASE WHEN MONTH(tgl) = 10 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit10
					, CASE WHEN MONTH(tgl) = 11 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga11, CASE WHEN MONTH(tgl) = 11 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit11
					, CASE WHEN MONTH(tgl) = 12 AND jmlpv <> 0 THEN jmlharga ELSE 0 END AS totalharga12, CASE WHEN MONTH(tgl) = 12 AND jmlpv = 0 THEN jmlharga ELSE 0 END AS totalhargakit12
				FROM pinjaman_titipan ro
				RIGHT JOIN pinjaman_titipan_d rod ON ro.id = rod.pinjaman_titipan_id
				LEFT JOIN stockiest stc ON ro.stockiest_id = stc.id
				-- LEFT JOIN kota rk ON stc.kota_id = rk.id
				WHERE tgl BETWEEN '".$awal."' AND '".$akhir."'
			)AS scp ON k.id = scp.kota
		)AS dt
		GROUP BY id
		";
		*/
		$query = "
		SELECT 
		propinsi_id, propinsi,id, kota
		,SUM(omset1) AS omset1
		,SUM(omset2) AS omset2
		,SUM(omset3) AS omset3
		,SUM(omset4) AS omset4
		,SUM(omset5) AS omset5
		,SUM(omset6) AS omset6
		,SUM(omset7) AS omset7
		,SUM(omset8) AS omset8
		,SUM(omset9) AS omset9
		,SUM(omset10) AS omset10
		,SUM(omset11) AS omset11
		,SUM(omset12) AS omset12
		FROM (
			SELECT k.propinsi_id AS propinsi_id
				,CASE WHEN k.propinsi_id = 0 THEN 'Unknown' ELSE p.name END AS propinsi
				,k.id,k.name AS kota, k.kota_exp
				, IFNULL(so.totalharga1,0) AS omset1
				, IFNULL(so.totalharga2,0) AS omset2
				, IFNULL(so.totalharga3,0) AS omset3
				, IFNULL(so.totalharga4,0) AS omset4
				, IFNULL(so.totalharga5,0) AS omset5
				, IFNULL(so.totalharga6,0) AS omset6
				, IFNULL(so.totalharga7,0) AS omset7
				, IFNULL(so.totalharga8,0) AS omset8
				, IFNULL(so.totalharga9,0) AS omset9
				, IFNULL(so.totalharga10,0) AS omset10
				, IFNULL(so.totalharga11,0) AS omset11
				, IFNULL(so.totalharga12,0) AS omset12	
			FROM kota k
			LEFT JOIN(
				SELECT kota
					, SUM(totalharga1)AS totalharga1, SUM(totalhargakit1)AS totalhargakit1
					, SUM(totalharga2)AS totalharga2, SUM(totalhargakit2)AS totalhargakit2
					, SUM(totalharga3)AS totalharga3, SUM(totalhargakit3)AS totalhargakit3
					, SUM(totalharga4)AS totalharga4, SUM(totalhargakit4)AS totalhargakit4
					, SUM(totalharga5)AS totalharga5, SUM(totalhargakit5)AS totalhargakit5
					, SUM(totalharga6)AS totalharga6, SUM(totalhargakit6)AS totalhargakit6
					, SUM(totalharga7)AS totalharga7, SUM(totalhargakit7)AS totalhargakit7
					, SUM(totalharga8)AS totalharga8, SUM(totalhargakit8)AS totalhargakit8
					, SUM(totalharga9)AS totalharga9, SUM(totalhargakit9)AS totalhargakit9
					, SUM(totalharga10)AS totalharga10, SUM(totalhargakit10)AS totalhargakit10
					, SUM(totalharga11)AS totalharga11, SUM(totalhargakit11)AS totalhargakit11
					, SUM(totalharga12)AS totalharga12, SUM(totalhargakit12)AS totalhargakit12
				FROM(
					SELECT MONTH(tgl)AS bln
						, so.statuspu
						-- , CASE WHEN so.statuspu = '0' THEN 'UHN' ELSE md.kota END AS kota
						, CASE WHEN so.statuspu = '0' THEN m.kota_id ELSE md.kota END AS kota
						, CASE WHEN MONTH(tgl) = 1 AND so.kit = 'N' THEN totalharga ELSE 0 END AS totalharga1, CASE WHEN MONTH(tgl) = 1 AND so.kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit1
						, CASE WHEN MONTH(tgl) = 2 AND so.kit = 'N' THEN totalharga ELSE 0 END AS totalharga2, CASE WHEN MONTH(tgl) = 2 AND so.kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit2
						, CASE WHEN MONTH(tgl) = 3 AND so.kit = 'N' THEN totalharga ELSE 0 END AS totalharga3, CASE WHEN MONTH(tgl) = 3 AND so.kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit3
						, CASE WHEN MONTH(tgl) = 4 AND so.kit = 'N' THEN totalharga ELSE 0 END AS totalharga4, CASE WHEN MONTH(tgl) = 4 AND so.kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit4
						, CASE WHEN MONTH(tgl) = 5 AND so.kit = 'N' THEN totalharga ELSE 0 END AS totalharga5, CASE WHEN MONTH(tgl) = 5 AND so.kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit5
						, CASE WHEN MONTH(tgl) = 6 AND so.kit = 'N' THEN totalharga ELSE 0 END AS totalharga6, CASE WHEN MONTH(tgl) = 6 AND so.kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit6
						, CASE WHEN MONTH(tgl) = 7 AND so.kit = 'N' THEN totalharga ELSE 0 END AS totalharga7, CASE WHEN MONTH(tgl) = 7 AND so.kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit7
						, CASE WHEN MONTH(tgl) = 8 AND so.kit = 'N' THEN totalharga ELSE 0 END AS totalharga8, CASE WHEN MONTH(tgl) = 8 AND so.kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit8
						, CASE WHEN MONTH(tgl) = 9 AND so.kit = 'N' THEN totalharga ELSE 0 END AS totalharga9, CASE WHEN MONTH(tgl) = 9 AND so.kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit9
						, CASE WHEN MONTH(tgl) = 10 AND so.kit = 'N' THEN totalharga ELSE 0 END AS totalharga10, CASE WHEN MONTH(tgl) = 10 AND so.kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit10
						, CASE WHEN MONTH(tgl) = 11 AND so.kit = 'N' THEN totalharga ELSE 0 END AS totalharga11, CASE WHEN MONTH(tgl) = 11 AND so.kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit11
						, CASE WHEN MONTH(tgl) = 12 AND so.kit = 'N' THEN totalharga ELSE 0 END AS totalharga12, CASE WHEN MONTH(tgl) = 12 AND so.kit = 'Y' THEN totalharga ELSE 0 END AS totalhargakit12
						
					FROM so
					LEFT JOIN member_delivery md ON so.delivery_addr = md.id
					LEFT JOIN member m ON so.member_id = m.id
					WHERE tgl BETWEEN '".$awal."' AND '".$akhir."'
					-- AND stockiest_id = 0
				)AS dt
				LEFT JOIN kota k ON dt.kota = k.id
				GROUP BY kota
			)AS so ON k.id = so.kota
			LEFT JOIN propinsi p ON k.propinsi_id = p.id
		)AS dt
		GROUP BY id
		ORDER BY propinsi_id asc
		";
		$q = $this->db->query($query);
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
	}
	/*
	* END ASP 20150506
	*/
	
	/*
	* START ASP 20150707
	*/
	public function get_stc_type(){
        $data = array();
		$data['0']='All';
		$data['1']='Stockiest Area';
		$data['2']='M-Stockiest';
        return $data;
    }
	public function get_stc_status(){
        $data = array();
		$data['all']='All';
		$data['hide_inactive']='Hide Inactive';
        return $data;
    }
	public function viewStcTargetFilter($thn, $quart, $stctype, $stcstat)
    {
        $data=array();
		//$thn = '2012';
		//$quart = '01';
		$tgl = $thn."-".$quart;
		$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ";
		$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ";
		if($quart != 0){
			$awal_ = "CONCAT(".$awal.", ' 00:00:00')";
			$akhir_ = "CONCAT(".$akhir.", ' 23:59:59')";
		}else{
			$awal_ = "CONCAT(".$thn.", '-01-31 00:00:00')";
			$akhir_ = "CONCAT(".$thn.", '-12-31 23:59:59')";
		}
		
		if($stctype!='0'){
			$qstctype = "AND s.type = '".$stctype."'";
		}else{
			$qstctype = "";
		}
		
		if($stcstat=='all'){
			$qstcstat = "((s.created <= $akhir_ AND s.status = 'active')OR(s.status = 'inactive' AND s.updated <= $akhir_ AND s.updated >= $awal_))";
		}else{
			$qstcstat = "((s.created <= $akhir_ AND s.status = 'active')OR(s.status = 'inactive' AND s.updated <= $akhir_ AND s.updated >= $awal_)) 
			AND s.id not in (
				select id from stockiest_inactive si
				where YEAR(si.createddate) = '".$thn."'
			
			)";
		}
		// echo $quart;
		if($quart != 0){
			$qry = "
			SELECT s.id, m.nama, s.no_stc, s.type AS tipe, CONCAT('Region',k.region)AS region
				, IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0) AS oms1
				, IFNULL(trg1,0)AS trg1
				, IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0) AS oms2
				, IFNULL(trg2,0)AS trg2
				, IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0) AS oms3
				, IFNULL(trg3,0)AS trg3
				, (IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0))+
				  (IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0))+
				  (IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0)) AS oms
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_id = k.id
			LEFT JOIN member m ON s.id = m.id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS ro1, SUM(oms2)AS ro2, SUM(oms3)AS ro3
				FROM(
					SELECT member_id
						, CASE WHEN (MONTH(ro.`date`)=MONTH($awal) AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH($awal) AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(ro.`date`)=MONTH($awal)+1 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH($awal)+1 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(ro.`date`)=MONTH($awal)+2 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH($awal)+2 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
					FROM ro
					WHERE ro.stockiest_id=0
					AND ro.date BETWEEN $awal AND $akhir
				)AS ro_
				GROUP BY member_id
			)AS ro ON s.id = ro.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS pjm1, SUM(oms2)AS pjm2, SUM(oms3)AS pjm3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (MONTH(pjm.tgl)=MONTH($awal) AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH($awal) AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(pjm.tgl)=MONTH($awal)+1 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH($awal)+1 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(pjm.tgl)=MONTH($awal)+2 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH($awal)+2 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
					FROM pinjaman_titipan pjm
					WHERE pjm.tgl BETWEEN $awal AND $akhir
				)AS pjm
				GROUP BY member_id
			)AS pjm ON s.id = pjm.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS rtr1, SUM(oms2)AS rtr2, SUM(oms3)AS rtr3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (MONTH(rtr.tgl)=MONTH($awal) AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH($awal) AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(rtr.tgl)=MONTH($awal)+1 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH($awal)+1 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(rtr.tgl)=MONTH($awal)+2 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH($awal)+2 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
					FROM retur_titipan rtr
					WHERE rtr.tgl BETWEEN $awal AND $akhir
				)AS rtr
				GROUP BY member_id
			)AS rtr ON s.id = rtr.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS trg1, SUM(oms2)AS trg2, SUM(oms3)AS trg3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN MONTH(trg.periode)=MONTH($awal) THEN target END AS oms1
						, CASE WHEN MONTH(trg.periode)=MONTH($awal)+1 THEN target END AS oms2
						, CASE WHEN MONTH(trg.periode)=MONTH($awal)+2 THEN target END AS oms3
					FROM stockiest_target trg
					WHERE trg.periode BETWEEN $awal AND $akhir
				)AS rtr
				GROUP BY member_id
			)AS trg ON m.id = trg.member_id
			-- WHERE s.status = 'active'
			-- WHERE (s.created <= $akhir_ AND s.status = 'active')OR(s.status = 'inactive' AND s.updated <= $akhir_ AND s.updated >= $awal_)
			WHERE ".$qstcstat."
			-- HAVING oms <> 0
			".$qstctype."
			ORDER BY k.region, s.type, s.no_stc
		";
		}else{
			$qry = "
			SELECT s.id, m.nama, s.no_stc, s.type AS tipe, CONCAT('Region',k.region)AS region
				, IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0) AS oms1
				, IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0) AS oms2
				, IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0) AS oms3
				, IFNULL(ro.ro4,0)+IFNULL(pjm.pjm4,0) - IFNULL(rtr.rtr4,0) AS oms4
				, IFNULL(ro.ro5,0)+IFNULL(pjm.pjm5,0) - IFNULL(rtr.rtr5,0) AS oms5
				, IFNULL(ro.ro6,0)+IFNULL(pjm.pjm6,0) - IFNULL(rtr.rtr6,0) AS oms6
				, IFNULL(ro.ro7,0)+IFNULL(pjm.pjm7,0) - IFNULL(rtr.rtr7,0) AS oms7
				, IFNULL(ro.ro8,0)+IFNULL(pjm.pjm8,0) - IFNULL(rtr.rtr8,0) AS oms8
				, IFNULL(ro.ro9,0)+IFNULL(pjm.pjm9,0) - IFNULL(rtr.rtr9,0) AS oms9
				, IFNULL(ro.ro10,0)+IFNULL(pjm.pjm10,0) - IFNULL(rtr.rtr10,0) AS oms10
				, IFNULL(ro.ro11,0)+IFNULL(pjm.pjm11,0) - IFNULL(rtr.rtr11,0) AS oms11
				, IFNULL(ro.ro12,0)+IFNULL(pjm.pjm12,0) - IFNULL(rtr.rtr12,0) AS oms12
				, (IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0))+
				  (IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0))+
				  (IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0))+
				  (IFNULL(ro.ro4,0)+IFNULL(pjm.pjm4,0) - IFNULL(rtr.rtr4,0))+
				  (IFNULL(ro.ro5,0)+IFNULL(pjm.pjm5,0) - IFNULL(rtr.rtr5,0))+
				  (IFNULL(ro.ro6,0)+IFNULL(pjm.pjm6,0) - IFNULL(rtr.rtr6,0))+
				  (IFNULL(ro.ro7,0)+IFNULL(pjm.pjm7,0) - IFNULL(rtr.rtr7,0))+
				  (IFNULL(ro.ro8,0)+IFNULL(pjm.pjm8,0) - IFNULL(rtr.rtr8,0))+
				  (IFNULL(ro.ro9,0)+IFNULL(pjm.pjm9,0) - IFNULL(rtr.rtr9,0))+
				  (IFNULL(ro.ro10,0)+IFNULL(pjm.pjm10,0) - IFNULL(rtr.rtr10,0))+
				  (IFNULL(ro.ro11,0)+IFNULL(pjm.pjm11,0) - IFNULL(rtr.rtr11,0))+
				  (IFNULL(ro.ro12,0)+IFNULL(pjm.pjm12,0) - IFNULL(rtr.rtr12,0))
				  AS oms
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_id = k.id
			LEFT JOIN member m ON s.id = m.id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS ro1, SUM(oms2)AS ro2, SUM(oms3)AS ro3
					, SUM(oms4)AS ro4, SUM(oms5)AS ro5, SUM(oms6)AS ro6
					, SUM(oms7)AS ro7, SUM(oms8)AS ro8, SUM(oms9)AS ro9
					, SUM(oms10)AS ro10, SUM(oms11)AS ro11, SUM(oms12)AS ro12
				FROM(
					SELECT member_id
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01') AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01') AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+1 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+1 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+2 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+2 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+3 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+3 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms4
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+4 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+4 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms5
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+5 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+5 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms6
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+6 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+6 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms7
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+7 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+7 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms8
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+8 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+8 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms9
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+9 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+9 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms10
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+10 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+10 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms11
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+11 AND (ro.`date`) <= '2015-02-28') OR (MONTH(ro.`date`)=MONTH('$thn-01-01')+11 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms12
					FROM ro
					WHERE ro.stockiest_id=0
					AND YEAR(ro.date) = $thn
				)AS ro_
				GROUP BY member_id
			)AS ro ON s.id = ro.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS pjm1, SUM(oms2)AS pjm2, SUM(oms3)AS pjm3
					, SUM(oms4)AS pjm4, SUM(oms5)AS pjm5, SUM(oms6)AS pjm6
					, SUM(oms7)AS pjm7, SUM(oms8)AS pjm8, SUM(oms9)AS pjm9
					, SUM(oms10)AS pjm10, SUM(oms11)AS pjm11, SUM(oms12)AS pjm12
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01') AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+1 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+1 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+2 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+2 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+3 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+3 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms4
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+4 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+4 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms5
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+5 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+5 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms6
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+6 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+6 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms7
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+7 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+7 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms8
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+8 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+8 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms9
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+9 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+9 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms10
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+10 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+10 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms11
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+11 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+11 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms12
					FROM pinjaman_titipan pjm
					WHERE YEAR(pjm.tgl) = $thn
				)AS pjm
				GROUP BY member_id
			)AS pjm ON s.id = pjm.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS rtr1, SUM(oms2)AS rtr2, SUM(oms3)AS rtr3
					, SUM(oms4)AS rtr4, SUM(oms5)AS rtr5, SUM(oms6)AS rtr6
					, SUM(oms7)AS rtr7, SUM(oms8)AS rtr8, SUM(oms9)AS rtr9
					, SUM(oms10)AS rtr10, SUM(oms11)AS rtr11, SUM(oms12)AS rtr12
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01') AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01') AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+1 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+1 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+2 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+2 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+3 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+3 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms4
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+4 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+4 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms5
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+5 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+5 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms6
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+6 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+6 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms7
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+7 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+7 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms8
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+8 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+8 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms9
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+9 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+9 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms10
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+10 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+10 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms11
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+11 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+11 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms12
					FROM retur_titipan rtr
					WHERE YEAR(rtr.tgl) = $thn
				)AS rtr
				GROUP BY member_id
			)AS rtr ON s.id = rtr.member_id
			-- WHERE s.status = 'active'
			-- WHERE (s.created <= $akhir_ AND s.status = 'active')OR(s.status = 'inactive' AND s.updated <= $akhir_ AND s.updated >= $awal_)
			WHERE ".$qstcstat."
			-- HAVING oms <> 0
			".$qstctype."
			ORDER BY k.region, s.type, s.no_stc
			";
		}
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	/*
	* END ASP 20150707
	*/
	
	/* 
	* Start ASP 20150818 
	*/
	public function sales_incentive_ro_rpt($thn, $q){
        $bln =  substr($q, -5, 2);
		
		$qry11 = "
				, rs.omset_ytd AS omset1, rs.omset AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset*100/ omset_ytd)AS newVSold
			";
		
		/*
		if($bln > 9 && $thn > 2013){
			$qry11 = "
				, rs.omset_ytd AS omset1, rs.omset AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset*100/ omset_ytd)AS newVSold
			";
		}else{
			$qry11 = "
				, rs.omset_ytd/1.1 AS omset1, rs.omset/1.1 AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset/1.1)*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset/1.1)*100/ (omset_ytd/1.1))AS newVSold
			";
		}
		*/
		
		$data = array();
		$thn_ = $thn-1;
		$br = "<br>";
		$br = "";
		// echo $q;
		if($q!=0){
			// Jika tidak all
			$tgl = $q;
			$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
			$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
			
			$qry1="_, bln AS periode ".$br;
			$qry2="_, MONTH(rs.periode) AS periode ".$br;
			$qry3="HAVING periode BETWEEN MONTH($awal) AND MONTH($akhir) ".$br;
			
			$bln1_ = date('n')-2;
			$thn1_ = date('Y');

			if($bln >= $bln1_ && $thn = $thn1_){
				$query = "CALL report_sls_inc_ro(NOW()); ";
				$qry = $this->db->query($query);
			}
			
			$query1 = " ";
			$query2 = " ";
			
		}else{
			// Jika all
			$qry1=" ";$qry2=" ";$qry3=" ";
			$awal = "'".$thn."-01-01' ".$br;
			// $akhir = "NOW() ".$br;
			$akhir = "'".$thn."-12-31' ".$br;
			$query = "CALL report_sls_inc_ro(NOW()); ";
			$qry = $this->db->query($query);
			
			$query1 = "
			SELECT periode_ AS periode, region, region_pic, SUM(omset1)AS omset1, SUM(omset2)AS omset2, SUM(target) AS target, salesVStarget, newVSold
				, SUM(nr2)AS nr2, SUM(kit2)AS kit2, SUM(nr)AS nr, SUM(recruit)AS recruit
			FROM(
			";
			
			$query2 = "
				)AS dt
			GROUP BY periode_, region
			";
		}
		
		$query = $query1."
			SELECT CASE  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 1 AND 3 THEN 1  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 4 AND 6 THEN 2  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 7 AND 9 THEN 3  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 10 AND 12 THEN 4  ".$br."
				END AS periode_, MONTH(rs.periode) AS periode  ".$br."
				-- , IFNULL(r.nama,'UNIHEALTH') AS region  ".$br."
				, CASE WHEN ISNULL(r.nama) THEN 'UNIHEALTH' ".$br."
					WHEN r.id > 0 THEN concat('REGION ',r.id)  ".$br."
					END as region ".$br."
				, CASE WHEN ISNULL(r.nama) THEN 'UNIHEALTH' ".$br."
					   WHEN r.id = 1 THEN 'Riky Triyadi' ".$br."
					   WHEN r.id = 2 THEN 'Ardi Srijaya' ".$br."
				       WHEN r.id = 3 THEN 'Yusroni Fadli' ".$br."
				       WHEN r.id = 4 THEN 'Yusroni Fadli' ".$br."
				  END AS region_pic  ".$br."
				, rs.omset_ytd AS omset1, rs.omset AS omset2 ".$br."
				, IFNULL(rt.target,0)AS target  ".$br."
				-- , IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0)),0) AS salesVStarget  ".$br."
				-- , ROUND(omset*100/ omset_ytd)AS newVSold  ".$br."
				, IFNULL(ROUND(((rs.omset/ IFNULL(rt.target,0))-1)*100),0) AS salesVStarget  ".$br."
				, ROUND(((omset/ omset_ytd)-1)*100)AS newVSold  ".$br."
				, rs.nr AS nr2  ".$br."
				, rs.nm AS kit2  ".$br."
				, rt.nr AS nr  ".$br."
				, SUM(IFNULL(rs.recruit,0))AS recruit  ".$br."
				, IFNULL(ROUND(rs.nm*100/(rt.nr)),0) AS kit1VSkit2  ".$br."
			FROM report_salesinc_ro rs  ".$br."
			LEFT JOIN region r ON rs.region = r.id  ".$br."
			LEFT JOIN region_target rt ON rs.region = rt.region_id AND rs.periode = rt.periode  ".$br."
			WHERE rs.periode BETWEEN $awal AND $akhir ".$br."
			GROUP BY rs.periode, rs.region  ".$br."
			".$query2
		;
		
		$qry = $this->db->query($query);
		 //echo $this->db->last_query();
		
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	/* 
	* End ASP 20150818 
	*/
	
	/*
	* Start ASP 20150825
	*/
	public function viewStcTargetFilterNew($thn, $quart, $stctype, $stcstat)
    {
        $data=array();
		//$thn = '2012';
		//$quart = '01';
		$tgl = $thn."-".$quart;
		$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ";
		$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ";
		if($quart != 0){
			$awal_ = "CONCAT(".$awal.", ' 00:00:00')";
			$akhir_ = "CONCAT(".$akhir.", ' 23:59:59')";
		}else{
			$awal_ = "CONCAT(".$thn.", '-01-31 00:00:00')";
			$akhir_ = "CONCAT(".$thn.", '-12-31 23:59:59')";
		}
		
		if($stcstat=='all'){
			// $qstcstat = "((s.created <= $akhir_ AND s.status = 'active')OR(s.status = 'inactive' AND s.updated <= $akhir_ AND s.updated >= $awal_))";
			$qstcstat = "";
		}else{
			$qstcstat = "WHERE ((s.created <= $akhir_ AND s.status = 'active')OR(s.status = 'inactive' AND s.updated <= $akhir_ AND s.updated >= $awal_))
			 AND s.id not in (
				select id from stockiest_inactive si
				where YEAR(si.createddate) = '".$thn."'
			)
			";
		}
		
		if($stctype!='0'){
			if($stcstat=='all')
				$qstctype = "WHERE s.type = '".$stctype."'";
			else
				$qstctype = "AND s.type = '".$stctype."'";
		}else{
			$qstctype = "";
		}
		// echo $quart;
		if($quart != 0){
			$qry = "
			SELECT s.id, m.nama, s.no_stc, s.type AS tipe, CONCAT('Region',k.region)AS region
				, IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0) AS oms1
				, IFNULL(trg1,0)AS trg1
				, IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0) AS oms2
				, IFNULL(trg2,0)AS trg2
				, IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0) AS oms3
				, IFNULL(trg3,0)AS trg3
				, (IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0))+
				  (IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0))+
				  (IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0)) AS oms
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_id = k.id
			LEFT JOIN member m ON s.id = m.id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS ro1, SUM(oms2)AS ro2, SUM(oms3)AS ro3
				FROM(
					SELECT member_id
					    /*
						, CASE WHEN (MONTH(ro.`date`)=MONTH($awal) AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH($awal) AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms1
						, CASE WHEN (MONTH(ro.`date`)=MONTH($awal)+1 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH($awal)+1 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms2
						, CASE WHEN (MONTH(ro.`date`)=MONTH($awal)+2 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH($awal)+2 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms3
						*/
						, CASE WHEN (MONTH(ro.`date`)=MONTH($awal) AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH($awal) AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms1
						, CASE WHEN (MONTH(ro.`date`)=MONTH($awal)+1 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH($awal)+1 AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms2
						, CASE WHEN (MONTH(ro.`date`)=MONTH($awal)+2 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH($awal)+2 AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms3
					FROM ro
					RIGHT JOIN ro_d rod ON ro.id = rod.ro_id
					WHERE ro.stockiest_id=0
					-- AND rod.pv > 0
					AND (
						CASE WHEN ro.`date` > '2015-02-28' THEN rod.pv > 0 
						ELSE rod.pv >= 0 
						END
					)
					AND ro.date BETWEEN $awal AND $akhir
					GROUP BY ro.id
				)AS ro_
				GROUP BY member_id
			)AS ro ON s.id = ro.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS pjm1, SUM(oms2)AS pjm2, SUM(oms3)AS pjm3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (MONTH(pjm.tgl)=MONTH($awal) AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH($awal) AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(pjm.tgl)=MONTH($awal)+1 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH($awal)+1 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(pjm.tgl)=MONTH($awal)+2 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH($awal)+2 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
					FROM pinjaman_titipan pjm
					WHERE pjm.tgl BETWEEN $awal AND $akhir
				)AS pjm
				GROUP BY member_id
			)AS pjm ON s.id = pjm.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS rtr1, SUM(oms2)AS rtr2, SUM(oms3)AS rtr3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (MONTH(rtr.tgl)=MONTH($awal) AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH($awal) AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(rtr.tgl)=MONTH($awal)+1 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH($awal)+1 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(rtr.tgl)=MONTH($awal)+2 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH($awal)+2 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
					FROM retur_titipan rtr
					WHERE rtr.tgl BETWEEN $awal AND $akhir
				)AS rtr
				GROUP BY member_id
			)AS rtr ON s.id = rtr.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS trg1, SUM(oms2)AS trg2, SUM(oms3)AS trg3
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN MONTH(trg.periode)=MONTH($awal) THEN target END AS oms1
						, CASE WHEN MONTH(trg.periode)=MONTH($awal)+1 THEN target END AS oms2
						, CASE WHEN MONTH(trg.periode)=MONTH($awal)+2 THEN target END AS oms3
					FROM stockiest_target trg
					WHERE trg.periode BETWEEN $awal AND $akhir
				)AS rtr
				GROUP BY member_id
			)AS trg ON m.id = trg.member_id
			-- WHERE s.status = 'active'
			-- WHERE (s.created <= $akhir_ AND s.status = 'active')OR(s.status = 'inactive' AND s.updated <= $akhir_ AND s.updated >= $awal_)
			-- WHERE 
			".$qstcstat."
			".$qstctype."
			HAVING oms <> 0
			ORDER BY k.region, s.type, s.no_stc
		";
		}else{
			$qry = "
			SELECT s.id, m.nama, s.no_stc, s.type AS tipe, CONCAT('Region',k.region)AS region
				, IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0) AS oms1
				, IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0) AS oms2
				, IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0) AS oms3
				, IFNULL(ro.ro4,0)+IFNULL(pjm.pjm4,0) - IFNULL(rtr.rtr4,0) AS oms4
				, IFNULL(ro.ro5,0)+IFNULL(pjm.pjm5,0) - IFNULL(rtr.rtr5,0) AS oms5
				, IFNULL(ro.ro6,0)+IFNULL(pjm.pjm6,0) - IFNULL(rtr.rtr6,0) AS oms6
				, IFNULL(ro.ro7,0)+IFNULL(pjm.pjm7,0) - IFNULL(rtr.rtr7,0) AS oms7
				, IFNULL(ro.ro8,0)+IFNULL(pjm.pjm8,0) - IFNULL(rtr.rtr8,0) AS oms8
				, IFNULL(ro.ro9,0)+IFNULL(pjm.pjm9,0) - IFNULL(rtr.rtr9,0) AS oms9
				, IFNULL(ro.ro10,0)+IFNULL(pjm.pjm10,0) - IFNULL(rtr.rtr10,0) AS oms10
				, IFNULL(ro.ro11,0)+IFNULL(pjm.pjm11,0) - IFNULL(rtr.rtr11,0) AS oms11
				, IFNULL(ro.ro12,0)+IFNULL(pjm.pjm12,0) - IFNULL(rtr.rtr12,0) AS oms12
				, (IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0))+
				  (IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0))+
				  (IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0))+
				  (IFNULL(ro.ro4,0)+IFNULL(pjm.pjm4,0) - IFNULL(rtr.rtr4,0))+
				  (IFNULL(ro.ro5,0)+IFNULL(pjm.pjm5,0) - IFNULL(rtr.rtr5,0))+
				  (IFNULL(ro.ro6,0)+IFNULL(pjm.pjm6,0) - IFNULL(rtr.rtr6,0))+
				  (IFNULL(ro.ro7,0)+IFNULL(pjm.pjm7,0) - IFNULL(rtr.rtr7,0))+
				  (IFNULL(ro.ro8,0)+IFNULL(pjm.pjm8,0) - IFNULL(rtr.rtr8,0))+
				  (IFNULL(ro.ro9,0)+IFNULL(pjm.pjm9,0) - IFNULL(rtr.rtr9,0))+
				  (IFNULL(ro.ro10,0)+IFNULL(pjm.pjm10,0) - IFNULL(rtr.rtr10,0))+
				  (IFNULL(ro.ro11,0)+IFNULL(pjm.pjm11,0) - IFNULL(rtr.rtr11,0))+
				  (IFNULL(ro.ro12,0)+IFNULL(pjm.pjm12,0) - IFNULL(rtr.rtr12,0))
				  AS oms
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_id = k.id
			LEFT JOIN member m ON s.id = m.id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS ro1, SUM(oms2)AS ro2, SUM(oms3)AS ro3
					, SUM(oms4)AS ro4, SUM(oms5)AS ro5, SUM(oms6)AS ro6
					, SUM(oms7)AS ro7, SUM(oms8)AS ro8, SUM(oms9)AS ro9
					, SUM(oms10)AS ro10, SUM(oms11)AS ro11, SUM(oms12)AS ro12
				FROM(
					SELECT member_id
						/*
						, CASE WHEN (MONTH(ro.`date`)=MONTH('2015-01-01') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('2015-01-01') AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms1
						, CASE WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+1 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+1 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms2
						, CASE WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+2 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+2 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms3
						, CASE WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+3 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+3 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms4
						, CASE WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+4 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+4 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms5
						, CASE WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+5 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+5 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms6
						, CASE WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+6 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+6 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms7
						, CASE WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+7 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+7 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms8
						, CASE WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+8 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+8 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms9
						, CASE WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+9 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+9 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms10
						, CASE WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+10 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+10 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms11
						, CASE WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+11 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('2015-01-01')+11 AND (ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms12
						*/
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms1
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+1 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+1 AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms2
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+2 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+2 AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms3
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+3 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+3 AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms4
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+4 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+4 AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms5
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+5 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+5 AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms6
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+6 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+6 AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms7
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+7 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+7 AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms8
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+8 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+8 AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms9
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+9 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+9 AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms10
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+10 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+10 AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms11
						, CASE WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+11 AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('$thn-01-01')+11 AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms12
					FROM ro
					RIGHT JOIN ro_d rod ON ro.id = rod.ro_id
					WHERE ro.stockiest_id=0
					-- AND rod.pv > 0
					AND (
						CASE WHEN ro.`date` > '2015-02-28' THEN rod.pv > 0 
						ELSE rod.pv >= 0 
						END
					)
					AND YEAR(ro.date) = $thn
					GROUP BY ro.id
				)AS ro_
				GROUP BY member_id
			)AS ro ON s.id = ro.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS pjm1, SUM(oms2)AS pjm2, SUM(oms3)AS pjm3
					, SUM(oms4)AS pjm4, SUM(oms5)AS pjm5, SUM(oms6)AS pjm6
					, SUM(oms7)AS pjm7, SUM(oms8)AS pjm8, SUM(oms9)AS pjm9
					, SUM(oms10)AS pjm10, SUM(oms11)AS pjm11, SUM(oms12)AS pjm12
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01') AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+1 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+1 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+2 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+2 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+3 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+3 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms4
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+4 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+4 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms5
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+5 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+5 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms6
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+6 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+6 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms7
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+7 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+7 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms8
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+8 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+8 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms9
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+9 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+9 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms10
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+10 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+10 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms11
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('$thn-01-01')+11 AND (pjm.tgl) <= '2015-02-28') OR (MONTH(pjm.tgl)=MONTH('$thn-01-01')+11 AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms12
					FROM pinjaman_titipan pjm
					WHERE YEAR(pjm.tgl) = $thn
				)AS pjm
				GROUP BY member_id
			)AS pjm ON s.id = pjm.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS rtr1, SUM(oms2)AS rtr2, SUM(oms3)AS rtr3
					, SUM(oms4)AS rtr4, SUM(oms5)AS rtr5, SUM(oms6)AS rtr6
					, SUM(oms7)AS rtr7, SUM(oms8)AS rtr8, SUM(oms9)AS rtr9
					, SUM(oms10)AS rtr10, SUM(oms11)AS rtr11, SUM(oms12)AS rtr12
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01') AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01') AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+1 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+1 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+2 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+2 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+3 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+3 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms4
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+4 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+4 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms5
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+5 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+5 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms6
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+6 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+6 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms7
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+7 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+7 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms8
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+8 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+8 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms9
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+9 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+9 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms10
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+10 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+10 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms11
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('$thn-01-01')+11 AND (rtr.tgl) <= '2015-02-28') OR (MONTH(rtr.tgl)=MONTH('$thn-01-01')+11 AND (rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms12
					FROM retur_titipan rtr
					WHERE YEAR(rtr.tgl) = $thn
				)AS rtr
				GROUP BY member_id
			)AS rtr ON s.id = rtr.member_id
			-- WHERE s.status = 'active'
			-- WHERE (s.created <= $akhir_ AND s.status = 'active')OR(s.status = 'inactive' AND s.updated <= $akhir_ AND s.updated >= $awal_)
			-- WHERE 
			".$qstcstat."
			".$qstctype."
			HAVING oms <> 0
			ORDER BY k.region, s.type, s.no_stc
			";
		}
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	/*
	* End ASP 20150825
	*/
	
		/* 
	* Start ASP 20160118 
	*/
	public function sales_ro_cluster_rpt($thn, $q){
        $bln =  substr($q, -5, 2);
		
		$qry11 = "
				, rs.omset_ytd AS omset1, rs.omset AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset*100/ omset_ytd)AS newVSold
			";
		
		/*
		if($bln > 9 && $thn > 2013){
			$qry11 = "
				, rs.omset_ytd AS omset1, rs.omset AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset*100/ omset_ytd)AS newVSold
			";
		}else{
			$qry11 = "
				, rs.omset_ytd/1.1 AS omset1, rs.omset/1.1 AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset/1.1)*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset/1.1)*100/ (omset_ytd/1.1))AS newVSold
			";
		}
		*/
		
		$data = array();
		$thn_ = $thn-1;
		$br = "<br>";
		$br = "";
		// echo $q;
		if($q!=0){
			// Jika tidak all
			$tgl = $q;
			$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
			$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
			
			$qry1="_, bln AS periode ".$br;
			$qry2="_, MONTH(rs.periode) AS periode ".$br;
			$qry3="HAVING periode BETWEEN MONTH($awal) AND MONTH($akhir) ".$br;
			
			$bln1_ = date('n')-2;
			$thn1_ = date('Y');

			if($bln >= $bln1_ && $thn = $thn1_){
				$query = "CALL report_sls_ro_cluster_2016(NOW()); ";
				//$qry = $this->db->query($query);
				//$queryother = "CALL report_sls_ro_cluster_other(NOW()); ";
				//$qryother = $this->db->query($queryother);
				$queryotherreg = "CALL report_sls_ro_cluster_other_reg(NOW()); ";
				//$qryotherreg = $this->db->query($queryotherreg);
			}
			
			$query1 = " ";
			$query2 = " ";
			
		}else{
			// Jika all
			$qry1=" ";$qry2=" ";$qry3=" ";
			$awal = "'".$thn."-01-01' ".$br;
			// $akhir = "NOW() ".$br;
			$akhir = "'".$thn."-12-31' ".$br;
			$query = "CALL report_sls_ro_cluster_2016(NOW()); ";
			$qry = $this->db->query($query);
			//$queryother = "CALL report_sls_ro_cluster_other(NOW()); ";
			//$qryother = $this->db->query($queryother);
			$queryotherreg = "CALL report_sls_ro_cluster_other_reg(NOW()); ";
			$qryotherreg = $this->db->query($queryotherreg);

			$query1 = "
			-- per quarter 
			SELECT periode_ AS periode
				, region
				, region_pic
				, cluster_group_id
				, cluster_group
				, cluster_id
				, cluster
				, cluster_alias
				, SUM(sales_target)AS sales_target
				, SUM(omset_mtd)AS omset_mtd
				, vstarget
				, SUM(nr_target)AS nr_target
				, SUM(nr)AS nr
				, vstargetnr
				, SUM(omset_last_month)AS omset_last_month
				, vslm
				, SUM(omset_ytd)AS omset_ytd
				, vsly
				, SUM(currstc)AS currstc
				, SUM(actstc)AS actstc
				, stcratio
				, SUM(nr)AS nr
				, SUM(qspmup)AS qspmup
						FROM(
			-- eof per quarter	
			";
			
			$query2 = "
			)AS dt
			GROUP BY periode_, region, cluster_id, cluster_group_id
			order by periode_, region_sort, cluster_group_id, cluster_id
			";
		}
		/*
		$query = $query1."
			SELECT CASE  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 1 AND 3 THEN 1  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 4 AND 6 THEN 2  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 7 AND 9 THEN 3  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 10 AND 12 THEN 4  ".$br."
				END AS periode_, MONTH(rs.periode) AS periode  ".$br."
				-- , IFNULL(r.nama,'UNIHEALTH') AS region  ".$br."
				, CASE WHEN ISNULL(r.nama) THEN 'UNIHEALTH' ".$br."
					WHEN r.id > 0 THEN concat('REGION ',r.id)  ".$br."
					END as region ".$br."
				, CASE WHEN ISNULL(r.nama) THEN 'UNIHEALTH' ".$br."
					   WHEN r.id = 1 THEN 'Riky Triyadi' ".$br."
					   WHEN r.id = 2 THEN 'Ardi Srijaya' ".$br."
				       WHEN r.id = 3 THEN 'Yusroni Fadli' ".$br."
				       WHEN r.id = 4 THEN 'Yusroni Fadli' ".$br."
				  END AS region_pic  ".$br."
				, rs.omset_ytd AS omset1, rs.omset AS omset2 ".$br."
				, IFNULL(rt.target,0)AS target  ".$br."
				-- , IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0)),0) AS salesVStarget  ".$br."
				-- , ROUND(omset*100/ omset_ytd)AS newVSold  ".$br."
				, IFNULL(ROUND(((rs.omset/ IFNULL(rt.target,0))-1)*100),0) AS salesVStarget  ".$br."
				, ROUND(((omset/ omset_ytd)-1)*100)AS newVSold  ".$br."
				, rs.nr AS nr2  ".$br."
				, rs.nm AS kit2  ".$br."
				, rt.nr AS nr  ".$br."
				, SUM(IFNULL(rs.recruit,0))AS recruit  ".$br."
				, IFNULL(ROUND(rs.nm*100/(rt.nr)),0) AS kit1VSkit2  ".$br."
			FROM report_sales_ro_cluster_2016 rs  ".$br."
			LEFT JOIN region r ON rs.region = r.id  ".$br."
			LEFT JOIN region_target rt ON rs.region = rt.region_id AND rs.periode = rt.periode  ".$br."
			WHERE rs.periode BETWEEN $awal AND $akhir ".$br."
			GROUP BY rs.periode, rs.region  ".$br."
			".$query2
		;
		*/
		
		$query = $query1."
			SELECT CASE  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 1 AND 3 THEN 1  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 4 AND 6 THEN 2  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 7 AND 9 THEN 3  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 10 AND 12 THEN 4 ".$br." 
				END AS periode_, MONTH(rs.periode) AS periode ".$br." 
			,CASE WHEN k.region = 0 THEN 'UNIHEALTH' ".$br."
				   WHEN k.region = 1 THEN 'Jafar Heri Setiawan' ".$br."
				   WHEN k.region = 2 THEN 'Fransisca Josefina' ".$br."
				   -- WHEN k.region = 3 THEN 'Yusroni Fadli' ".$br."
				   WHEN k.region = 3 THEN 'Raymond Harivan Sirait' ".$br."
				   -- WHEN k.region = 4 THEN 'Riky Triadi' ".$br."
				   WHEN k.region = 4 THEN 'Mellyna Atika Rahayu' ".$br."
			 END AS region_pic  ".$br."
			,CONCAT('Region ', k.region) AS region ".$br."
			, rso.sort as region_sort ".$br."
			,pc.cluster_group_id ".$br."
			,ppg.description AS cluster_group ".$br."
			,ppc.cluster_id AS cluster_id ".$br."
			,pc.description AS cluster ".$br."
			,pc.alias AS cluster_alias ".$br."
			, IFNULL(pct.target_omset,0) AS sales_target ".$br."
			, omset AS 'omset_mtd'  ".$br."
			, IFNULL(ROUND(((rs.omset/ IFNULL(pct.target_omset,0))-1)*100),0) AS 'vstarget' ".$br."
			, IFNULL(pct.target_nr,0) AS nr_target ".$br."
			, rs.nr AS 'nr' ".$br."
			, IFNULL(ROUND(((rs.nr/ IFNULL(pct.target_nr,0))-1)*100),0) AS 'vstargetnr' ".$br."
			, rs.omset_last_month ".$br."
			, IFNULL(ROUND(((rs.omset/ IFNULL(rs.omset_last_month,0))-1)*100),0) AS 'vslm' ".$br."
			-- , case when rs.omset_last_month = 0 then 0 else round(((rs.omset/rs.omset_last_month)-1)*100) end as 'vsLM%' ".$br."
			, rs.omset_ytd ".$br."
			, IFNULL(ROUND(((rs.omset/ IFNULL(rs.omset_ytd,0))-1)*100),0) AS 'vsly' ".$br."
			-- , CASE WHEN rs.omset_ytd = 0 THEN 0 ELSE ROUND(((rs.omset/rs.omset_ytd)-1)*100) end AS 'vsLY%' ".$br."
			, IFNULL(rs.currstc,0) AS currstc ".$br."
			, IFNULL(rs.actstc,0) AS actstc ".$br."
			, CASE WHEN rs.currstc = 0 OR rs.currstc IS NULL THEN 0 ELSE ROUND(((rs.actstc/rs.currstc))*100) END AS 'stcratio' ".$br."
			, IFNULL(rs.qspmup,0) AS qspmup ".$br."
			FROM kota k  ".$br."
			LEFT JOIN propinsi p ON k.propinsi_id = p.id ".$br."
			LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id ".$br."
			LEFT JOIN propinsi_cluster pc ON ppc.cluster_id = pc.id ".$br."
			LEFT JOIN propinsi_cluster_group ppg ON pc.cluster_group_id = ppg.id ".$br."
			LEFT JOIN report_sales_ro_cluster_2016 rs ON ppc.cluster_id = rs.cluster AND k.region = rs.region ".$br."
			LEFT JOIN propinsi_cluster_target pct ON rs.cluster = pct.cluster_id AND rs.periode = pct.periode ".$br."
			LEFT JOIN region_sort rso ON k.region = rso.id ".$br."
			WHERE rs.periode BETWEEN $awal AND $akhir ".$br."
			-- AND rs.region != 0 ".$br."
			GROUP BY rs.periode, k.region,ppc.cluster_id, pc.cluster_group_id ".$br."
			ORDER BY rs.periode, rso.sort,pc.cluster_group_id,ppc.cluster_id ".$br."
			".$query2
		;

		$qry = $this->db->query($query);
		 //echo $this->db->last_query();
		
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
	public function sales_ro_cluster_rpt_total($thn, $q){
        $bln =  substr($q, -5, 2);
		
		$qry11 = "
				, rs.omset_ytd AS omset1, rs.omset AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset*100/ omset_ytd)AS newVSold
			";
		
		/*
		if($bln > 9 && $thn > 2013){
			$qry11 = "
				, rs.omset_ytd AS omset1, rs.omset AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset*100/ omset_ytd)AS newVSold
			";
		}else{
			$qry11 = "
				, rs.omset_ytd/1.1 AS omset1, rs.omset/1.1 AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset/1.1)*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset/1.1)*100/ (omset_ytd/1.1))AS newVSold
			";
		}
		*/
		
		$data = array();
		$thn_ = $thn-1;
		$br = "<br>";
		$br = "";
		// echo $q;
		if($q!=0){
			// Jika tidak all
			$tgl = $q;
			$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
			$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
			
			$qry1="_, bln AS periode ".$br;
			$qry2="_, MONTH(rs.periode) AS periode ".$br;
			$qry3="HAVING periode BETWEEN MONTH($awal) AND MONTH($akhir) ".$br;
			
			$bln1_ = date('n')-2;
			$thn1_ = date('Y');

			if($bln >= $bln1_ && $thn = $thn1_){
				//$query = "CALL report_sls_ro_cluster_2016(NOW()); ";
				//$qry = $this->db->query($query);
			}
			
			$query1 = " ";
			$query2 = " ";
			
		}else{
			// Jika all
			$qry1=" ";$qry2=" ";$qry3=" ";
			$awal = "'".$thn."-01-01' ".$br;
			// $akhir = "NOW() ".$br;
			$akhir = "'".$thn."-12-31' ".$br;
			//$query = "CALL report_sls_ro_cluster_2016(NOW()); ";
			//$qry = $this->db->query($query);
			
			$query1 = "
			-- per quarter 
			SELECT periode_ AS periode
				,SUM(omset_nasional) AS omset_nasional
				FROM(
			-- eof per quarter	
			";
			
			$query2 = "
			)AS dt
			GROUP BY periode_
			";
		}
		/*
		$query = $query1."
			SELECT CASE  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 1 AND 3 THEN 1  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 4 AND 6 THEN 2  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 7 AND 9 THEN 3  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 10 AND 12 THEN 4  ".$br."
				END AS periode_, MONTH(rs.periode) AS periode  ".$br."
				-- , IFNULL(r.nama,'UNIHEALTH') AS region  ".$br."
				, CASE WHEN ISNULL(r.nama) THEN 'UNIHEALTH' ".$br."
					WHEN r.id > 0 THEN concat('REGION ',r.id)  ".$br."
					END as region ".$br."
				, CASE WHEN ISNULL(r.nama) THEN 'UNIHEALTH' ".$br."
					   WHEN r.id = 1 THEN 'Riky Triyadi' ".$br."
					   WHEN r.id = 2 THEN 'Ardi Srijaya' ".$br."
				       WHEN r.id = 3 THEN 'Yusroni Fadli' ".$br."
				       WHEN r.id = 4 THEN 'Yusroni Fadli' ".$br."
				  END AS region_pic  ".$br."
				, rs.omset_ytd AS omset1, rs.omset AS omset2 ".$br."
				, IFNULL(rt.target,0)AS target  ".$br."
				-- , IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0)),0) AS salesVStarget  ".$br."
				-- , ROUND(omset*100/ omset_ytd)AS newVSold  ".$br."
				, IFNULL(ROUND(((rs.omset/ IFNULL(rt.target,0))-1)*100),0) AS salesVStarget  ".$br."
				, ROUND(((omset/ omset_ytd)-1)*100)AS newVSold  ".$br."
				, rs.nr AS nr2  ".$br."
				, rs.nm AS kit2  ".$br."
				, rt.nr AS nr  ".$br."
				, SUM(IFNULL(rs.recruit,0))AS recruit  ".$br."
				, IFNULL(ROUND(rs.nm*100/(rt.nr)),0) AS kit1VSkit2  ".$br."
			FROM report_sales_ro_cluster_2016 rs  ".$br."
			LEFT JOIN region r ON rs.region = r.id  ".$br."
			LEFT JOIN region_target rt ON rs.region = rt.region_id AND rs.periode = rt.periode  ".$br."
			WHERE rs.periode BETWEEN $awal AND $akhir ".$br."
			GROUP BY rs.periode, rs.region  ".$br."
			".$query2
		;
		*/
		
		$query = $query1."
			SELECT ".$br."
			periode_ ".$br."
			,periode ".$br."
			,SUM(omset_mtd) AS omset_nasional ".$br."
			FROM(
				SELECT CASE  ".$br."
					WHEN MONTH(rs.periode) BETWEEN 1 AND 3 THEN 1  ".$br."
					WHEN MONTH(rs.periode) BETWEEN 4 AND 6 THEN 2  ".$br."
					WHEN MONTH(rs.periode) BETWEEN 7 AND 9 THEN 3  ".$br."
					WHEN MONTH(rs.periode) BETWEEN 10 AND 12 THEN 4 ".$br." 
					END AS periode_, MONTH(rs.periode) AS periode ".$br." 
				,CASE WHEN k.region = 0 THEN 'UNIHEALTH' ".$br."
					   WHEN k.region = 1 THEN 'Riky Triyadi' ".$br."
					   WHEN k.region = 2 THEN 'Ardi Srijaya' ".$br."
					   -- WHEN k.region = 3 THEN 'Yusroni Fadli' ".$br."
					   WHEN k.region = 3 THEN 'Firdan Tama' ".$br."
				 END AS region_pic  ".$br."
				,CONCAT('Region ', k.region) AS region ".$br."
				,pc.cluster_group_id ".$br."
				,ppg.description AS cluster_group ".$br."
				,ppc.cluster_id AS cluster_id ".$br."
				,pc.description AS cluster ".$br."
				,pc.alias AS cluster_alias ".$br."
				, IFNULL(pct.target_omset,0) AS sales_target ".$br."
				, omset AS 'omset_mtd'  ".$br."
				, IFNULL(ROUND(((rs.omset/ IFNULL(pct.target_omset,0))-1)*100),0) AS 'vstarget' ".$br."
				, IFNULL(pct.target_nr,0) AS nr_target ".$br."
				, rs.nr AS 'nr' ".$br."
				, IFNULL(ROUND(((rs.nr/ IFNULL(pct.target_nr,0))-1)*100),0) AS 'vstargetnr' ".$br."
				, rs.omset_last_month ".$br."
				, IFNULL(ROUND(((rs.omset/ IFNULL(rs.omset_last_month,0))-1)*100),0) AS 'vslm' ".$br."
				-- , case when rs.omset_last_month = 0 then 0 else round(((rs.omset/rs.omset_last_month)-1)*100) end as 'vsLM%' ".$br."
				, rs.omset_ytd ".$br."
				, IFNULL(ROUND(((rs.omset/ IFNULL(rs.omset_ytd,0))-1)*100),0) AS 'vsly' ".$br."
				-- , CASE WHEN rs.omset_ytd = 0 THEN 0 ELSE ROUND(((rs.omset/rs.omset_ytd)-1)*100) end AS 'vsLY%' ".$br."
				, IFNULL(rs.currstc,0) AS currstc ".$br."
				, IFNULL(rs.actstc,0) AS actstc ".$br."
				, CASE WHEN rs.currstc = 0 OR rs.currstc IS NULL THEN 0 ELSE ROUND(((rs.actstc/rs.currstc))*100) END AS 'stcratio' ".$br."
				, IFNULL(rs.qspmup,0) AS qspmup ".$br."
				FROM kota k  ".$br."
				LEFT JOIN propinsi p ON k.propinsi_id = p.id ".$br."
				LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id ".$br."
				LEFT JOIN propinsi_cluster pc ON ppc.cluster_id = pc.id ".$br."
				LEFT JOIN propinsi_cluster_group ppg ON pc.cluster_group_id = ppg.id ".$br."
				LEFT JOIN report_sales_ro_cluster_2016 rs ON ppc.cluster_id = rs.cluster AND k.region = rs.region ".$br."
				LEFT JOIN propinsi_cluster_target pct ON rs.cluster = pct.cluster_id AND rs.periode = pct.periode ".$br."
				LEFT JOIN region_sort rso ON k.region = rso.id ".$br."
				WHERE rs.periode BETWEEN $awal AND $akhir ".$br."
				AND rs.region != 0 ".$br."
				GROUP BY rs.periode, k.region,ppc.cluster_id, pc.cluster_group_id ".$br."
				ORDER BY rs.periode, rso.sort,pc.cluster_group_id,ppc.cluster_id ".$br."
			)AS dt ".$br."
			GROUP BY periode_, periode ".$br."
			".$query2
		;

		$qry = $this->db->query($query);
		 //echo $this->db->last_query();
		
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }


	public function sales_ro_cluster_rpt_othersales_reg($tglawal,$tglakhir,$region,$q){
			$data = array();
			//$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
			//$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
			$awal = "'".$tglawal."' ".$br;
			$akhir = "'".$tglakhir."' ".$br;
			
			if($q==0){
				$query1 = "
				-- per quarter 
				SELECT periode_ AS periode
					,item_group
					,SUM(omset) AS omset
					,SUM(omset_ly) AS omset_ly
					FROM(
				-- eof per quarter	
				";
				
				$query2 = "
				)AS dt
				GROUP BY item_group,periode_
				";
	
			}else{
				$query1 = "";
				$query2 = "";
			}
			/*
			$query = $query1."
			SELECT item_group
			,omset
			,omset_ly
			, IFNULL(ROUND(((omset/ IFNULL(omset_ly,0))-1)*100),0) AS 'vsly'
			FROM report_sales_ro_cluster_other
			WHERE periode BETWEEN $awal AND $akhir
			".$query2;	
			*/
			$query = $query1."
			SELECT CASE
					WHEN MONTH(rs.periode) BETWEEN 1 AND 3 THEN 1  
					WHEN MONTH(rs.periode) BETWEEN 4 AND 6 THEN 2  
					WHEN MONTH(rs.periode) BETWEEN 7 AND 9 THEN 3  
					WHEN MONTH(rs.periode) BETWEEN 10 AND 12 THEN 4  
					END AS periode_, MONTH(rs.periode) AS periode  
			,item_group
			,CONCAT('Region ', region) AS region
			,omset
			,omset_ly
			, IFNULL(ROUND(((omset/ IFNULL(omset_ly,0))-1)*100),0) AS 'vsly'
			FROM report_sales_ro_cluster_other_reg rs
			WHERE periode BETWEEN $awal AND $akhir
			and region = $region
			".$query2;	
			$qry = $this->db->query($query);
			 //echo $this->db->last_query();
			
			if($qry->num_rows()>0){
				foreach($qry->result_array() as $row){
					$data[]=$row;
				}
			}
			$qry->free_result();
			return $data;
			

	}

	public function sales_ro_cluster_rpt_othersales($tglawal,$tglakhir,$q){
			$data = array();
			//$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
			//$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
			$awal = "'".$tglawal."' ".$br;
			$akhir = "'".$tglakhir."' ".$br;
			
			if($q==0){
				$query1 = "
				-- per quarter 
				SELECT periode_ AS periode
					,item_group
					,SUM(omset) AS omset
					,SUM(omset_ly) AS omset_ly
					FROM(
				-- eof per quarter	
				";
				
				$query2 = "
				)AS dt
				GROUP BY item_group,periode_
				";
	
			}else{
				$query1 = "";
				$query2 = "";
			}
			/*
			$query = $query1."
			SELECT item_group
			,omset
			,omset_ly
			, IFNULL(ROUND(((omset/ IFNULL(omset_ly,0))-1)*100),0) AS 'vsly'
			FROM report_sales_ro_cluster_other
			WHERE periode BETWEEN $awal AND $akhir
			".$query2;	
			*/
			$query = $query1."
			SELECT CASE
					WHEN MONTH(rs.periode) BETWEEN 1 AND 3 THEN 1  
					WHEN MONTH(rs.periode) BETWEEN 4 AND 6 THEN 2  
					WHEN MONTH(rs.periode) BETWEEN 7 AND 9 THEN 3  
					WHEN MONTH(rs.periode) BETWEEN 10 AND 12 THEN 4  
					END AS periode_, MONTH(rs.periode) AS periode  
			,item_group
			,omset
			,omset_ly
			, IFNULL(ROUND(((omset/ IFNULL(omset_ly,0))-1)*100),0) AS 'vsly'
			FROM report_sales_ro_cluster_other rs
			WHERE periode BETWEEN $awal AND $akhir
			".$query2;	
			$qry = $this->db->query($query);
			 //echo $this->db->last_query();
			
			if($qry->num_rows()>0){
				foreach($qry->result_array() as $row){
					$data[]=$row;
				}
			}
			$qry->free_result();
			return $data;
			

	}
		
	public function sales_ro_cluster_rpt_othersales_total($tglawal,$tglakhir,$q){
			$data = array();
			//$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
			//$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
			$awal = "'".$tglawal."' ".$br;
			$akhir = "'".$tglakhir."' ".$br;
			
			if($q!=0){
				// Jika tidak all
				$query1 = " ";
				$query2 = " ";
				
			}else{
				// Jika all
				$query1 = "
				-- per quarter 
				SELECT periode_ AS periode
					,SUM(omset_nasional) AS omset_nasional
					FROM(
				-- eof per quarter	
				";
				
				$query2 = "
				)AS dt
				GROUP BY periode_
				";
			}
			
			$query = $query1."
			SELECT 
			CASE  
			WHEN MONTH(rs.periode) BETWEEN 1 AND 3 THEN 1  
			WHEN MONTH(rs.periode) BETWEEN 4 AND 6 THEN 2 
			WHEN MONTH(rs.periode) BETWEEN 7 AND 9 THEN 3  
			WHEN MONTH(rs.periode) BETWEEN 10 AND 12 THEN 4  
			END AS periode_, MONTH(rs.periode) AS periode  
			-- periode
			,sum(omset) as omset_nasional
			FROM report_sales_ro_cluster_other rs
			WHERE periode BETWEEN $awal AND $akhir
			GROUP BY periode_, periode
			".$query2."
			";	
			$qry = $this->db->query($query);
			 //echo $this->db->last_query();
			
			if($qry->num_rows()>0){
				foreach($qry->result_array() as $row){
					$data[]=$row;
				}
			}
			$qry->free_result();
			return $data;
			

	}
	/* 
	* End ASP 20160118 
	*/

	/*
	* Start ASP 20160130
	*/
	public function sales_so_cluster_rpt($thn, $q){
        $bln =  substr($q, -5, 2);
		
		$qry11 = "
				, rs.omset_ytd AS omset1, rs.omset AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset*100/ omset_ytd)AS newVSold
			";
		
		/*
		if($bln > 9 && $thn > 2013){
			$qry11 = "
				, rs.omset_ytd AS omset1, rs.omset AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset*100/ omset_ytd)AS newVSold
			";
		}else{
			$qry11 = "
				, rs.omset_ytd/1.1 AS omset1, rs.omset/1.1 AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset/1.1)*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset/1.1)*100/ (omset_ytd/1.1))AS newVSold
			";
		}
		*/
		
		$data = array();
		$thn_ = $thn-1;
		$br = "<br>";
		$br = "";
		// echo $q;
		if($q!=0){
			// Jika tidak all
			$tgl = $q;
			$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
			$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
			
			$qry1="_, bln AS periode ".$br;
			$qry2="_, MONTH(rs.periode) AS periode ".$br;
			$qry3="HAVING periode BETWEEN MONTH($awal) AND MONTH($akhir) ".$br;
			
			$bln1_ = date('n')-2;
			$thn1_ = date('Y');

			if($bln >= $bln1_ && $thn = $thn1_){
				$query = "CALL report_sls_so_cluster_2016(NOW()); ";
				$qry = $this->db->query($query);
			}
			
			$query1 = " ";
			$query2 = " ";
			
		}else{
			// Jika all
			$qry1=" ";$qry2=" ";$qry3=" ";
			$awal = "'".$thn."-01-01' ".$br;
			// $akhir = "NOW() ".$br;
			$akhir = "'".$thn."-12-31' ".$br;
			$query = "CALL report_sls_so_cluster_2016(NOW()); ";
			$qry = $this->db->query($query);
			$queryother = "CALL report_sls_ro_cluster_other(NOW()); ";
			$qryother = $this->db->query($queryother);

			$query1 = "
			-- per quarter 
			SELECT periode_ AS periode
				, region
				, region_pic
				, cluster_group_id
				, cluster_group
				, cluster_id
				, cluster
				, cluster_alias
				, SUM(omset_ytd)AS omset_ytd
				, SUM(omset_mtd)AS omset_mtd
				, vsly
				, omset_nasional
				, contribution
				, SUM(sales_target)AS sales_target
				, vstarget
				, SUM(omset_nam)AS omset_nam
				, SUM(nam) AS nam
				, nam_productivity
				, SUM(omset_oam)AS omset_oam
				, SUM(oam)AS oam
				, oam_productivity
				, SUM(newsm)AS newsm
				, SUM(newgm)AS newgm
				, SUM(newpm)AS newpm
				, SUM(newspm)AS newspm
				, SUM(oldqspm)AS oldqspm
				, SUM(newl)AS newl
				, SUM(oldql)AS oldql
				, SUM(oldnql)AS oldnql
				, SUM(newsl)AS newsl
				, SUM(oldqsl)AS oldqsl
				, SUM(oldnqsl)AS oldnqsl
				, SUM(qspmup)AS qspmup
						FROM(
			-- eof per quarter	
			";
			
			$query2 = "
			)AS dt
			GROUP BY periode_, region, cluster_id, cluster_group_id
			order by periode_, region_sort, cluster_group_id, cluster_id
			";
		}
		/*
		$query = $query1."
			SELECT CASE  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 1 AND 3 THEN 1  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 4 AND 6 THEN 2  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 7 AND 9 THEN 3  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 10 AND 12 THEN 4  ".$br."
				END AS periode_, MONTH(rs.periode) AS periode  ".$br."
				-- , IFNULL(r.nama,'UNIHEALTH') AS region  ".$br."
				, CASE WHEN ISNULL(r.nama) THEN 'UNIHEALTH' ".$br."
					WHEN r.id > 0 THEN concat('REGION ',r.id)  ".$br."
					END as region ".$br."
				, CASE WHEN ISNULL(r.nama) THEN 'UNIHEALTH' ".$br."
					   WHEN r.id = 1 THEN 'Riky Triyadi' ".$br."
					   WHEN r.id = 2 THEN 'Ardi Srijaya' ".$br."
				       WHEN r.id = 3 THEN 'Yusroni Fadli' ".$br."
				       WHEN r.id = 4 THEN 'Yusroni Fadli' ".$br."
				  END AS region_pic  ".$br."
				, rs.omset_ytd AS omset1, rs.omset AS omset2 ".$br."
				, IFNULL(rt.target,0)AS target  ".$br."
				-- , IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0)),0) AS salesVStarget  ".$br."
				-- , ROUND(omset*100/ omset_ytd)AS newVSold  ".$br."
				, IFNULL(ROUND(((rs.omset/ IFNULL(rt.target,0))-1)*100),0) AS salesVStarget  ".$br."
				, ROUND(((omset/ omset_ytd)-1)*100)AS newVSold  ".$br."
				, rs.nr AS nr2  ".$br."
				, rs.nm AS kit2  ".$br."
				, rt.nr AS nr  ".$br."
				, SUM(IFNULL(rs.recruit,0))AS recruit  ".$br."
				, IFNULL(ROUND(rs.nm*100/(rt.nr)),0) AS kit1VSkit2  ".$br."
			FROM report_sales_ro_cluster_2016 rs  ".$br."
			LEFT JOIN region r ON rs.region = r.id  ".$br."
			LEFT JOIN region_target rt ON rs.region = rt.region_id AND rs.periode = rt.periode  ".$br."
			WHERE rs.periode BETWEEN $awal AND $akhir ".$br."
			GROUP BY rs.periode, rs.region  ".$br."
			".$query2
		;
		*/
		
		$query = $query1."
				SELECT CASE 
					   WHEN MONTH(rs.periode) BETWEEN 1 AND 3 THEN 1  
					   WHEN MONTH(rs.periode) BETWEEN 4 AND 6 THEN 2  
					   WHEN MONTH(rs.periode) BETWEEN 7 AND 9 THEN 3  
					   WHEN MONTH(rs.periode) BETWEEN 10 AND 12 THEN 4  
					   END AS periode_, MONTH(rs.periode) AS periode  
				,CASE WHEN k.region = 0 THEN 'UNIHEALTH' 
					   WHEN k.region = 1 THEN 'Jafar Heri Setiawan' 
					   WHEN k.region = 2 THEN 'Fransisca Josefina' 
					   -- WHEN k.region = 3 THEN 'Yusroni Fadli' 
					   WHEN k.region = 3 THEN 'Raymond Harivan Sirait' 
					   -- WHEN k.region = 4 THEN 'Riky Triadi' 
					   WHEN k.region = 4 THEN 'Mellyna Atika Rahayu' 
				 END AS region_pic  
				 ,CONCAT('Region ', k.region) AS region
				-- ,pc.description AS cluster,pc.alias AS cluster_alias, CONCAT('Region ', rs.region) AS region
				, rso.sort as region_sort
				,pc.cluster_group_id 
				,ppg.description AS cluster_group
				,ppc.cluster_id AS cluster_id
				,pc.description AS cluster
				,pc.alias AS cluster_alias
				, omset_ly AS omset_ytd
				, omset AS omset_mtd
				, IFNULL(ROUND(((rs.omset/ IFNULL(rs.omset_ly,0))-1)*100),0) AS 'vsly'
				, IFNULL(pct.target_omset,0) AS sales_target
				, IFNULL(ROUND(((rs.omset/ IFNULL(pct.target_omset,0))-1)*100),0) AS 'vstarget'
				, IFNULL(dt.omsetnasional,0) AS omset_nasional
				, IFNULL(ROUND((omset/IFNULL(dt.omsetnasional,0)),4),0) AS contribution
				   , rs.omset_nam
				   , rs.nam
				   , CASE WHEN rs.nam = 0 THEN 0 ELSE ROUND(rs.omset_nam / rs.nam) END AS nam_productivity
				   , rs.omset_oam
				   , rs.oam
				   , CASE WHEN rs.oam = 0 THEN 0 ELSE ROUND(rs.omset_oam / rs.oam) END AS oam_productivity
				, newsm
				, newgm
				, newpm
				, newspm
				, oldqspm
				, newl
				, oldql
				, oldnql
				, newsl
				, oldqsl
				, oldnqsl
				, qspmup
				FROM kota k 
				LEFT JOIN propinsi p ON k.propinsi_id = p.id
				LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
				LEFT JOIN propinsi_cluster pc ON ppc.cluster_id = pc.id
				LEFT JOIN propinsi_cluster_group ppg ON pc.cluster_group_id = ppg.id
				LEFT JOIN report_sales_so_cluster_2016 rs ON ppc.cluster_id = rs.cluster AND k.region = rs.region
				LEFT JOIN propinsi_cluster_target pct ON rs.cluster = pct.cluster_id AND rs.periode = pct.periode
				LEFT JOIN region_sort rso ON k.region = rso.id
				LEFT JOIN (
					SELECT periode, SUM(OmsetMTD) AS omsetnasional
					FROM (
						SELECT rs.periode, pc.description AS cluster,pc.alias AS cluster_alias, CONCAT('Region ', rs.region) AS region, omset AS 'OmsetMTD'
						FROM kota k 
						LEFT JOIN propinsi p ON k.propinsi_id = p.id
						LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
						LEFT JOIN propinsi_cluster pc ON ppc.cluster_id = pc.id
						LEFT JOIN propinsi_cluster_group ppg ON pc.cluster_group_id = ppg.id
						LEFT JOIN report_sales_so_cluster_2016 rs ON ppc.cluster_id = rs.cluster AND k.region = rs.region
						LEFT JOIN propinsi_cluster_target pct ON rs.cluster = pct.cluster_id AND rs.periode = pct.periode
						WHERE rs.periode BETWEEN $awal AND $akhir
						GROUP BY k.region,ppc.cluster_id, pc.cluster_group_id
						ORDER BY k.region,pc.cluster_group_id,ppc.cluster_id
					) AS dt 
					GROUP BY periode
				)AS dt ON rs.periode = dt.periode
				WHERE rs.periode BETWEEN $awal AND $akhir
				AND rs.region != 0
				GROUP BY rs.periode, k.region,ppc.cluster_id, pc.cluster_group_id
				ORDER BY rs.periode, rso.sort,pc.cluster_group_id,ppc.cluster_id
			".$query2
		;

		$qry = $this->db->query($query);
		 //echo $this->db->last_query();
		
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
	/*
	* End ASP 20160130
	*/
	
	/* 
	* Start ASP 20160927 
	*/
	public function sales_util_member_rpt($thn, $q){
        $bln =  substr($q, -5, 2);
		
		$qry11 = "
				, rs.omset_ytd AS omset1, rs.omset AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset*100/ omset_ytd)AS newVSold
			";
		
		/*
		if($bln > 9 && $thn > 2013){
			$qry11 = "
				, rs.omset_ytd AS omset1, rs.omset AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset*100/ omset_ytd)AS newVSold
			";
		}else{
			$qry11 = "
				, rs.omset_ytd/1.1 AS omset1, rs.omset/1.1 AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset/1.1)*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset/1.1)*100/ (omset_ytd/1.1))AS newVSold
			";
		}
		*/
		
		$data = array();
		$thn_ = $thn-1;
		$br = "<br>";
		$br = "";
		// echo $q;
		if($q!=0){
			// Jika tidak all
			$tgl = $q;
			$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
			$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
			
			$qry1="_, bln AS periode ".$br;
			$qry2="_, MONTH(rs.periode) AS periode ".$br;
			$qry3="HAVING periode BETWEEN MONTH($awal) AND MONTH($akhir) ".$br;
			
			$bln1_ = date('n')-2;
			$thn1_ = date('Y');

			if($bln >= $bln1_ && $thn = $thn1_){
				$query = "CALL report_sales_util_member(NOW()); ";
				$qry = $this->db->query($query);
			}
			
			$query1 = " ";
			$query2 = " ";
			
		}else{
			// Jika all
			$qry1=" ";$qry2=" ";$qry3=" ";
			$awal = "'".$thn."-01-01' ".$br;
			// $akhir = "NOW() ".$br;
			$akhir = "'".$thn."-12-31' ".$br;
			$query = "CALL report_sales_util_member(NOW()); ";
			$qry = $this->db->query($query);
			//$queryother = "CALL report_sls_ro_cluster_other(NOW()); ";
			//$qryother = $this->db->query($queryother);

			$query1 = "
			-- per quarter 
			SELECT periode_ AS periode
				, region
				, sales_type
				, sales_name
				, SUM(prev_ma) AS prev_ma
				, SUM(signup) AS signup
				, SUM(recruit) AS recruit
				, SUM(qrecruit) AS qrecruit
				, ROUND((SUM(recruit)/SUM(signup)) *100) AS recvsign 
				, ROUND((SUM(recruit)/SUM(prev_ma)) *100) AS recvprevma 
				, CASE 
					WHEN ROUND((SUM(recruit)/SUM(prev_ma)) *100) >= 30 THEN 'Extremly Good'  
					WHEN ROUND((SUM(recruit)/SUM(prev_ma)) *100) BETWEEN 25 AND 30 THEN 'Good'  
					WHEN ROUND((SUM(recruit)/SUM(prev_ma)) *100) BETWEEN 20 AND 24  THEN 'Average'  
					WHEN ROUND((SUM(recruit)/SUM(prev_ma)) *100) BETWEEN 15 AND 19  THEN 'Bad'  
					WHEN ROUND((SUM(recruit)/SUM(prev_ma)) *100) <= 15 THEN 'Extremly Bad'  
				  END AS performance 
				, ROUND((SUM(qrecruit)/SUM(prev_ma)) *100) AS qroftotalmember 
				, SUM(reactive) AS reactive
				, SUM(inactive) AS inactive
				, (SUM(prev_ma)+SUM(recruit)+SUM(reactive)-SUM(inactive)) AS selisihma
				, ROUND((((SUM(prev_ma)+SUM(recruit)+SUM(reactive)-SUM(inactive)))/SUM(prev_ma))*100) AS persenselisihma
				, SUM(ma) + SUM(recruit) + SUM(reactive) - SUM(inactive) AS totalmemberactive
				, SUM(currma) AS currma
				, ROUND((SUM(currma)/(SUM(ma) + SUM(recruit) + SUM(reactive) - SUM(inactive)))*100) AS persenactivity
				, ROUND((SUM(so)/(SUM(ma) + SUM(recruit) + SUM(reactive) - SUM(inactive)))*100) AS salesactive
				, SUM(leader) AS leader
			FROM(
			-- eof per quarter	
			";
			
			$query2 = "
			-- per quarter
			)AS dt
			GROUP BY periode_, region, sales_id
			ORDER BY periode_, region_sort, sales_id
			-- eof perquarter
			";
		}
		/*
		$query = $query1."
			SELECT CASE  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 1 AND 3 THEN 1  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 4 AND 6 THEN 2  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 7 AND 9 THEN 3  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 10 AND 12 THEN 4  ".$br."
				END AS periode_, MONTH(rs.periode) AS periode  ".$br."
				-- , IFNULL(r.nama,'UNIHEALTH') AS region  ".$br."
				, CASE WHEN ISNULL(r.nama) THEN 'UNIHEALTH' ".$br."
					WHEN r.id > 0 THEN concat('REGION ',r.id)  ".$br."
					END as region ".$br."
				, CASE WHEN ISNULL(r.nama) THEN 'UNIHEALTH' ".$br."
					   WHEN r.id = 1 THEN 'Riky Triyadi' ".$br."
					   WHEN r.id = 2 THEN 'Ardi Srijaya' ".$br."
				       WHEN r.id = 3 THEN 'Yusroni Fadli' ".$br."
				       WHEN r.id = 4 THEN 'Yusroni Fadli' ".$br."
				  END AS region_pic  ".$br."
				, rs.omset_ytd AS omset1, rs.omset AS omset2 ".$br."
				, IFNULL(rt.target,0)AS target  ".$br."
				-- , IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0)),0) AS salesVStarget  ".$br."
				-- , ROUND(omset*100/ omset_ytd)AS newVSold  ".$br."
				, IFNULL(ROUND(((rs.omset/ IFNULL(rt.target,0))-1)*100),0) AS salesVStarget  ".$br."
				, ROUND(((omset/ omset_ytd)-1)*100)AS newVSold  ".$br."
				, rs.nr AS nr2  ".$br."
				, rs.nm AS kit2  ".$br."
				, rt.nr AS nr  ".$br."
				, SUM(IFNULL(rs.recruit,0))AS recruit  ".$br."
				, IFNULL(ROUND(rs.nm*100/(rt.nr)),0) AS kit1VSkit2  ".$br."
			FROM report_sales_ro_cluster_2016 rs  ".$br."
			LEFT JOIN region r ON rs.region = r.id  ".$br."
			LEFT JOIN region_target rt ON rs.region = rt.region_id AND rs.periode = rt.periode  ".$br."
			WHERE rs.periode BETWEEN $awal AND $akhir ".$br."
			GROUP BY rs.periode, rs.region  ".$br."
			".$query2
		;
		*/
		/*
		$query = $query1."
			SELECT CASE  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 1 AND 3 THEN 1  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 4 AND 6 THEN 2  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 7 AND 9 THEN 3  ".$br."
				WHEN MONTH(rs.periode) BETWEEN 10 AND 12 THEN 4 ".$br." 
				END AS periode_, MONTH(rs.periode) AS periode ".$br." 
			,CASE WHEN k.region = 0 THEN 'UNIHEALTH' ".$br."
				   WHEN k.region = 1 THEN 'Riky Triyadi' ".$br."
				   WHEN k.region = 2 THEN 'Ardi Srijaya' ".$br."
				   WHEN k.region = 3 THEN 'Yusroni Fadli' ".$br."
			 END AS region_pic  ".$br."
			,CONCAT('Region ', k.region) AS region ".$br."
			, rso.sort as region_sort ".$br."
			,pc.cluster_group_id ".$br."
			,ppg.description AS cluster_group ".$br."
			,ppc.cluster_id AS cluster_id ".$br."
			,pc.description AS cluster ".$br."
			,pc.alias AS cluster_alias ".$br."
			, IFNULL(pct.target_omset,0) AS sales_target ".$br."
			, omset AS 'omset_mtd'  ".$br."
			, IFNULL(ROUND(((rs.omset/ IFNULL(pct.target_omset,0))-1)*100),0) AS 'vstarget' ".$br."
			, IFNULL(pct.target_nr,0) AS nr_target ".$br."
			, rs.nr AS 'nr' ".$br."
			, IFNULL(ROUND(((rs.nr/ IFNULL(pct.target_nr,0))-1)*100),0) AS 'vstargetnr' ".$br."
			, rs.omset_last_month ".$br."
			, IFNULL(ROUND(((rs.omset/ IFNULL(rs.omset_last_month,0))-1)*100),0) AS 'vslm' ".$br."
			-- , case when rs.omset_last_month = 0 then 0 else round(((rs.omset/rs.omset_last_month)-1)*100) end as 'vsLM%' ".$br."
			, rs.omset_ytd ".$br."
			, IFNULL(ROUND(((rs.omset/ IFNULL(rs.omset_ytd,0))-1)*100),0) AS 'vsly' ".$br."
			-- , CASE WHEN rs.omset_ytd = 0 THEN 0 ELSE ROUND(((rs.omset/rs.omset_ytd)-1)*100) end AS 'vsLY%' ".$br."
			, IFNULL(rs.currstc,0) AS currstc ".$br."
			, IFNULL(rs.actstc,0) AS actstc ".$br."
			, CASE WHEN rs.currstc = 0 OR rs.currstc IS NULL THEN 0 ELSE ROUND(((rs.actstc/rs.currstc))*100) END AS 'stcratio' ".$br."
			, IFNULL(rs.qspmup,0) AS qspmup ".$br."
			FROM kota k  ".$br."
			LEFT JOIN propinsi p ON k.propinsi_id = p.id ".$br."
			LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id ".$br."
			LEFT JOIN propinsi_cluster pc ON ppc.cluster_id = pc.id ".$br."
			LEFT JOIN propinsi_cluster_group ppg ON pc.cluster_group_id = ppg.id ".$br."
			LEFT JOIN report_sales_ro_cluster_2016 rs ON ppc.cluster_id = rs.cluster AND k.region = rs.region ".$br."
			LEFT JOIN propinsi_cluster_target pct ON rs.cluster = pct.cluster_id AND rs.periode = pct.periode ".$br."
			LEFT JOIN region_sort rso ON k.region = rso.id ".$br."
			WHERE rs.periode BETWEEN $awal AND $akhir ".$br."
			GROUP BY rs.periode, k.region,ppc.cluster_id, pc.cluster_group_id ".$br."
			ORDER BY rs.periode, rso.sort,pc.cluster_group_id,ppc.cluster_id ".$br."
			".$query2
		;
		*/
		$query =  $query1."
					SELECT
					CASE  
					WHEN MONTH(rsm.periode) BETWEEN 1 AND 3 THEN 1  
					WHEN MONTH(rsm.periode) BETWEEN 4 AND 6 THEN 2 
					WHEN MONTH(rsm.periode) BETWEEN 7 AND 9 THEN 3  
					WHEN MONTH(rsm.periode) BETWEEN 10 AND 12 THEN 4 
					END AS periode_, MONTH(rsm.periode) AS periode
					,CONCAT('Region ', s.region) AS region 
					, rso.sort AS region_sort 
					, s.sales_type
					, s.id AS sales_id -- quarter need
					, s.sales_name
					, rsm.prev_ma
					, rsm.signup
					, rsm.recruit
					, rsm.qrecruit
					, ROUND((rsm.recruit/rsm.signup) *100) AS recvsign 
					, ROUND((rsm.recruit/rsm.prev_ma) *100) AS recvprevma 
					, CASE 
						WHEN ROUND((rsm.recruit/rsm.prev_ma) *100) >= 30 THEN 'Very Good'
						WHEN ROUND((rsm.recruit/rsm.prev_ma) *100) BETWEEN 25 AND 30 THEN 'Good'
						WHEN ROUND((rsm.recruit/rsm.prev_ma) *100) BETWEEN 20 AND 24  THEN 'Average'  
						WHEN ROUND((rsm.recruit/rsm.prev_ma) *100) BETWEEN 15 AND 19  THEN 'Bad'  
						WHEN ROUND((rsm.recruit/rsm.prev_ma) *100) <= 15 THEN 'Very Bad'  
					  END AS performance 
					, ROUND((rsm.qrecruit/rsm.prev_ma) *100) AS qroftotalmember 
					, rsm.reactive
					, rsm.inactive
					, (rsm.prev_ma+rsm.recruit+rsm.reactive-rsm.inactive) AS selisihma
					, ROUND((((rsm.prev_ma+rsm.recruit+rsm.reactive-rsm.inactive))/rsm.prev_ma)*100) AS persenselisihma
					, rsm.ma + recruit + reactive - inactive AS totalmemberactive
					, rsm.currma
					, rsm.ma -- quarter need
					, rsm.so -- quarter need
					, ROUND((rsm.currma/(rsm.ma + recruit + reactive - inactive))*100) AS persenactivity
					, ROUND((rsm.so/(rsm.ma + recruit + reactive - inactive))*100) AS salesactive
					, rsm.leader
					FROM report_sales_members_activity rsm
					LEFT JOIN salesman s ON rsm.sales_id=s.id
					LEFT JOIN region_sort rso ON s.region = rso.id
					WHERE rsm.periode BETWEEN $awal AND $akhir
					AND region IS NOT NULL
					ORDER BY rsm.periode,rso.sort
			".$query2
		;
		$qry = $this->db->query($query);
		 //echo $this->db->last_query();
		
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	/*
	* End ASP 20160927
	*/
	/* 
	* Start ASP 20160927 
	*/
	public function sales_util_stc_rpt($thn, $q){
        $bln =  substr($q, -5, 2);
		
		$qry11 = "
				, rs.omset_ytd AS omset1, rs.omset AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset*100/ omset_ytd)AS newVSold
			";
		
		/*
		if($bln > 9 && $thn > 2013){
			$qry11 = "
				, rs.omset_ytd AS omset1, rs.omset AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset*100/ omset_ytd)AS newVSold
			";
		}else{
			$qry11 = "
				, rs.omset_ytd/1.1 AS omset1, rs.omset/1.1 AS omset2, IFNULL(rt.target,0)AS target
				, IFNULL(ROUND(rs.omset/1.1)*100 / IFNULL(rt.target,0))),0) AS salesVStarget
				, ROUND(omset/1.1)*100/ (omset_ytd/1.1))AS newVSold
			";
		}
		*/
		
		$data = array();
		$thn_ = $thn-1;
		$br = "<br>";
		$br = "";
		// echo $q;
		if($q!=0){
			// Jika tidak all
			$tgl = $q;
			$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
			$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
			
			$qry1="_, bln AS periode ".$br;
			$qry2="_, MONTH(rs.periode) AS periode ".$br;
			$qry3="HAVING periode BETWEEN MONTH($awal) AND MONTH($akhir) ".$br;
			
			$bln1_ = date('n')-2;
			$thn1_ = date('Y');

			if($bln >= $bln1_ && $thn = $thn1_){
				$query = "CALL report_sales_util_stc(NOW()); ";
				$qry = $this->db->query($query);
			}
			
			$query1 = " ";
			$query2 = " ";
			
		}else{
			// Jika all
			$qry1=" ";$qry2=" ";$qry3=" ";
			$awal = "'".$thn."-01-01' ".$br;
			// $akhir = "NOW() ".$br;
			$akhir = "'".$thn."-12-31' ".$br;
			$query = "CALL report_sales_util_stc(NOW()); ";
			$qry = $this->db->query($query);
			//$queryother = "CALL report_sls_ro_cluster_other(NOW()); ";
			//$qryother = $this->db->query($queryother);

			$query1 = "
				-- per quarter
				SELECT periode_ AS periode
				, region
				, sales_type
				, sales_name
				, SUM(prev_stc_act) AS prev_stc_act
				, SUM(new_stc) AS new_stc
				, SUM(new_stc_act) AS new_stc_act
				, IFNULL(ROUND((SUM(new_stc_act)/SUM(new_stc)) *100),0) AS newstc_vs_newstcact 
				, IFNULL(ROUND((SUM(new_stc)/SUM(prev_stc_act)) *100),0) AS newstc_vs_totalstc
				, CASE 
					WHEN IFNULL(ROUND((SUM(new_stc)/SUM(prev_stc_act)) *100),0) >= 10 THEN 'Extremely Good'
					WHEN IFNULL(ROUND((SUM(new_stc)/SUM(prev_stc_act)) *100),0) BETWEEN 6 AND 9 THEN 'Good'
					WHEN IFNULL(ROUND((SUM(new_stc)/SUM(prev_stc_act)) *100),0) BETWEEN 2 AND 5  THEN 'Average'  
					WHEN IFNULL(ROUND((SUM(new_stc)/SUM(prev_stc_act)) *100),0) BETWEEN 1 AND 2  THEN 'Bad'  
					WHEN IFNULL(ROUND((SUM(new_stc)/SUM(prev_stc_act)) *100),0) <= 1 THEN 'Extremely Bad'  
				END AS performance  
				, SUM(reactive_stc) AS reactive_stc
				, SUM(inactive_stc) AS inactive_stc
				, IFNULL(ROUND((SUM(prev_stc_act) + SUM(new_stc) + SUM(reactive_stc))- SUM(prev_stc_act)),0) AS selisih_stc_active
				, IFNULL(ROUND(((((SUM(prev_stc_act) + SUM(new_stc) + SUM(reactive_stc))- SUM(prev_stc_act)) / SUM(prev_stc_act))*100)),0) AS persen_selisih_stc_active
				-- , IFNULL(ROUND((SUM(prev_stc_act) + SUM(new_stc) + SUM(reactive_stc)) - SUM(inactive_stc)),0) AS total_curr_stc_act
				, IFNULL(ROUND((SUM(prev_stc_act) + SUM(new_stc) + SUM(reactive_stc))),0) AS total_curr_stc_act
				, SUM(curr_stc_act)
				-- , IFNULL(ROUND((((SUM(curr_stc_act)) / ((SUM(prev_stc_act) + SUM(new_stc) + SUM(reactive_stc)) - SUM(inactive_stc)))*100)),0) AS persen_activity
				-- , IFNULL(ROUND((SUM(ro) / ((SUM(prev_stc_act) + SUM(new_stc) + SUM(reactive_stc)) - SUM(inactive_stc)))),0) AS sales_per_stc_act
				, IFNULL(ROUND((((SUM(curr_stc_act)) / ((SUM(prev_stc_act) + SUM(new_stc) + SUM(reactive_stc))))*100)),0) AS persen_activity
				, IFNULL(ROUND((SUM(ro) / ((SUM(prev_stc_act) + SUM(new_stc) + SUM(reactive_stc))))),0) AS sales_per_stc_act
				, SUM(ass_lead)
				FROM(
				-- eof per quarter
			";
			
			$query2 = "
			-- per quarter
			)AS dt
			GROUP BY periode_, region, sales_id
			ORDER BY periode_, region_sort, sales_id
			-- eof perquarter
			";
		}
		$query =  $query1."
					SELECT 
					CASE  
					WHEN MONTH(rss.periode) BETWEEN 1 AND 3 THEN 1  
					WHEN MONTH(rss.periode) BETWEEN 4 AND 6 THEN 2 
					WHEN MONTH(rss.periode) BETWEEN 7 AND 9 THEN 3  
					WHEN MONTH(rss.periode) BETWEEN 10 AND 12 THEN 4 
					END AS periode_, MONTH(rss.periode) AS periode
					,CONCAT('Region ', s.region) AS region 
					, rso.sort AS region_sort 
					, s.sales_type
					, s.id AS sales_id -- quarter need
					, s.sales_name
					, rss.prev_stc_act
					, rss.new_stc
					, rss.new_stc_act
					, IFNULL(ROUND((rss.new_stc_act/rss.new_stc) *100),0) AS newstc_vs_newstcact 
					, IFNULL(ROUND((rss.new_stc/rss.prev_stc_act) *100),0) AS newstc_vs_totalstc
					, CASE 
						WHEN IFNULL(ROUND((rss.new_stc/rss.prev_stc_act) *100),0) >= 10 THEN 'Extremely Good'
						WHEN IFNULL(ROUND((rss.new_stc/rss.prev_stc_act) *100),0) BETWEEN 6 AND 9 THEN 'Good'
						WHEN IFNULL(ROUND((rss.new_stc/rss.prev_stc_act) *100),0) BETWEEN 2 AND 5  THEN 'Average'  
						WHEN IFNULL(ROUND((rss.new_stc/rss.prev_stc_act) *100),0) BETWEEN 1 AND 2  THEN 'Bad'  
						WHEN IFNULL(ROUND((rss.new_stc/rss.prev_stc_act) *100),0) <= 1 THEN 'Extremely Bad'  
					END AS performance  
					, rss.reactive_stc
					, rss.inactive_stc
					, IFNULL(ROUND((rss.prev_stc_act + rss.new_stc + rss.reactive_stc)- rss.prev_stc_act),0) AS selisih_stc_active
					, IFNULL(ROUND(((((rss.prev_stc_act + rss.new_stc + rss.reactive_stc)- rss.prev_stc_act) / rss.prev_stc_act)*100)),0) AS persen_selisih_stc_active
					-- , IFNULL(ROUND((rss.prev_stc_act + rss.new_stc + rss.reactive_stc) - rss.inactive_stc),0) AS total_curr_stc_act
					, IFNULL(ROUND((rss.prev_stc_act + rss.new_stc + rss.reactive_stc)),0) AS total_curr_stc_act
					, rss.curr_stc_act
					-- , IFNULL(ROUND((((rss.curr_stc_act) / ((rss.prev_stc_act + rss.new_stc + rss.reactive_stc) - rss.inactive_stc))*100)),0) AS persen_activity
					-- , IFNULL(ROUND((rss.ro / ((rss.prev_stc_act + rss.new_stc + rss.reactive_stc) - rss.inactive_stc))),0) AS sales_per_stc_act
					, IFNULL(ROUND((((rss.curr_stc_act) / ((rss.prev_stc_act + rss.new_stc + rss.reactive_stc)))*100)),0) AS persen_activity
					, IFNULL(ROUND((rss.ro / ((rss.prev_stc_act + rss.new_stc + rss.reactive_stc)))),0) AS sales_per_stc_act
					, rss.ass_lead
					, rss.ro -- quarter need
					FROM report_sales_stc_activity rss
					LEFT JOIN salesman s ON rss.sales_id=s.id
					LEFT JOIN region_sort rso ON s.region = rso.id
					WHERE rss.periode BETWEEN $awal AND $akhir
					AND region != 0
					ORDER BY rss.periode,rso.sort
			".$query2
		;
		$qry = $this->db->query($query);
		 //echo $this->db->last_query();
		
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	/*
	* End ASP 20160927
	*/
	// Start ASP 20170207
	public function getNationalMonthly($thn){
		$query_rocluster = "CALL report_sls_ro_cluster_2016(NOW()); ";
		$qry_rocluster = $this->db->query($query_rocluster);
		$queryother = "CALL report_sls_ro_cluster_other(NOW()); ";
		$qryother = $this->db->query($queryother);
		
		$query = "
		SELECT 
		-- MONTH(dt.periode) AS periode
		dt.periode
		, dt.omset_mtd
		, dt2.omset_mtd_other
		, dt.omset_mtd + dt2.omset_mtd_other AS total_sales
		, dt.omset_ly + dt2.omset_ly_other AS total_sales_ly
		, ROUND((((dt.omset_mtd + dt2.omset_mtd_other)/(dt.omset_ly + dt2.omset_ly_other))-1)*100,2) AS growth
		, dt.nm
		, dt.nr
		, dt.currstc
		, dt.actstc
		, ROUND((dt.actstc/dt.currstc)*100,2) AS persen_stc
		FROM (
			SELECT
			rs.periode
			, SUM(rs.omset) AS 'omset_mtd' 
			, SUM(rs.omset_ytd) AS 'omset_ly' 
			, SUM(rs.nm) AS 'nm' 
			, SUM(rs.nr) AS 'nr' 
			, SUM(rs.currstc) AS 'currstc' 
			, SUM(rs.actstc) AS 'actstc' 
			FROM report_sales_ro_cluster_2016 rs 
			WHERE rs.periode BETWEEN '".$thn."-01-01' AND '".$thn."-12-31'-- $awal AND $akhir 
			GROUP BY rs.periode
			ORDER BY rs.periode
		) AS dt
		LEFT JOIN (
			SELECT
			rso.periode
			, SUM(rso.omset) AS 'omset_mtd_other'
			, SUM(rso.omset_ly) AS 'omset_ly_other'
			FROM report_sales_ro_cluster_other rso 
			GROUP BY rso.periode
			ORDER BY rso.periode
		)AS dt2 ON dt.periode = dt2.periode
		";
		$qry = $this->db->query($query);
		 //echo $this->db->last_query();
		
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
	}
	// EOF ASP 20170207
}
?>