<?php
class MReport extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function activedMember($flag){
        $data = array();
        if($flag == 'tupo'){
            $this->db->select("m.id as member_id,m.nama,format(m.ps,0)as fps,j.decription as jenjang",false)
                ->from('member m')
                ->join('jenjang j','m.jenjang_id=j.id','left')
                ->where('m.ps >',0)
                ->order_by('m.nama','asc');
            $query = $this->db->get();        
        }elseif($flag == 'aktip'){
            $date1 = date('Y-m-d',now());
            $query = $this->db->query("SELECT s.member_id, m.nama, format(sum(s.totalpv), 0)as fps, j.decription as jenjang
                FROM so s LEFT JOIN member m ON s.member_id=m.id LEFT JOIN jenjang j ON m.jenjang_id=j.id
                WHERE s.tgl >= date_add('$date1', interval -12 month) AND s.tgl <= '$date1'
                GROUP BY s.member_id ORDER BY m.nama asc");
        }else{
            $this->db->select("m.id as member_id,m.nama,format(m.pgs,0)as fpgs,j.decription as jenjang",false)
                ->from('member m')
                ->join('jenjang j','m.jenjang_id=j.id','left')
                ->where('m.pgs >=',40000000)
                ->order_by('m.pgs','desc')
                ->order_by('m.nama','asc');
            $query = $this->db->get();        
        }
        echo $this->db->last_query();
        
        if($query->num_rows()>0){
            $i=0;
            foreach($query->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $query->free_result();
        return $data;
    }
    
    public function sumActivedMember($flag){
        $data = array();
        if($flag == 'tupo'){
            $this->db->select("format(sum(ps),0)as fps",false)
                ->from('member')
                ->where('ps >',0);
        }
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    
    
    
    public function getSubBonus($id){
        $data = array();
        $this->db->from('v_bonus')->where('id',$id);
        if($this->session->userdata('group_id')>100)$this->db->where('member_id',$this->session->userdata('userid'));
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
                $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getBonusDetail($id){
        $data = array();
        $q = $this->db->select("d.downline_id,m.nama,format(pv,0)as fpv,d.persen,format(d.nominal,0)as fnominal",false)
            ->from('bonus_d d')->join('member m','d.downline_id=m.id','left')->where('d.bonus_id',$id)->order_by('m.nama','asc')->get();
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getDropDrownPeriode(){
        $data = array();
        $this->db->select("periode,tgl",false);
        $this->db->from('v_bonus');
        if($this->session->userdata('group_id')>100){
            $this->db->where('display','1');
            $this->db->where('member_id',$this->session->userdata('userid'));
        }
        $this->db->group_by('tgl');
        $this->db->order_by('periode','desc');
        $q=$this->db->get();
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $data[$row['periode']] = $row['tgl'];
            }
        }
        $q->free_result();
        return $data;
    }
    public function getComisionAnalysis($periode1,$periode2){
        $data = array();
        
        $total = $this->_getOmzet($periode1,$periode2);
        
        $q = $this->db->select("v.title, v.titlebonus,v.persen,sum(v.nominal)as nominal,format(sum(v.nominal),0)as fnominal,format(sum(v.bv),0)as fbv",false)
            ->from('v_bonus v')
            ->where('periode >=',$periode1)
            ->where('periode <=',$periode2)
            ->group_by('title')->order_by('title','asc')->get();
        
       // echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $row['ftotalbv'] = $total['ftotalbv'];
                $row['ftotalharga'] = $total['ftotalharga'];
                $row['totalharga'] = $total['totalharga'];
                $row['totalbv'] = $total['totalbv'];
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    protected function _getOmzet($periode1,$periode2){
        $data=array();
        $q=$this->db->select("sum(totalbv)as totalbv,format(sum(totalbv),0)as ftotalbv,format(sum(totalharga),0)as ftotalharga,sum(totalharga)as totalharga",false)
            ->from('so')
            ->where('date_format(tgl,"%Y-%m") >=',substr($periode1,0,7))
            ->where('date_format(tgl,"%Y-%m") <=',substr($periode2,0,7))
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getTotalCA($periode1,$periode2){
        $data = array();
        $q = $this->db->select("sum(nominal)as nominal,format(sum(nominal),0)as fnominal",false)
            ->from('v_bonus')
            ->where(array('periode >='=>$periode1))
            ->where(array('periode <='=>$periode2))->get();
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getECB($periode){
        $data = array();
        
        $q = $this->db->select("a.member_id,a.hadiah,m.nama",false)
            ->from('ecb a')->join('member m','a.member_id=m.id','left')->where('tgl',$periode)->order_by('m.nama','asc')->get();
        
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Report Bonus Statment
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @development by www.smartindo-technology.com
    | @created 2009-06-02
    |
    */
    
    public function getJenjang($memberid,$periode){
        $data = array();
        $this->db->select("j.member_id,m.nama,date_format(j.tgl,'%M %Y')as ftgl,t.name as jenjanglama,t.decription as desc_jenjanglama,
                          t2.name as jenjangbaru,t2.decription as desc_jenjangbaru,date_format(j.tglakhir,'%M %Y')as ftglakhir",false)
            ->from('jenjang_history j')->join('member m','j.member_id=m.id','left')->join('jenjang t','j.jenjanglama=t.id','left')
            ->join('jenjang t2','j.jenjangbaru=t2.id','left');
        if($memberid){
            $this->db->where('member_id',$memberid);            
        }else{
            $this->db->where('j.tgl',$periode);
        }
        $this->db->order_by('j.id','desc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getPS($memberid){
        $data=array();
        $q = $this->db->select("format(m.ps,0)as fps,format(m.apgs,0)as fapgs,
                               t.name as jenjang,t2.name as jenjang2",false)
            ->from('member m')
            ->join('jenjang t','m.jenjang_id=t.id','left')
            ->join('jenjang t2','m.jenjang_id2=t2.id','left')
            ->where('m.id',$memberid)->get();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getTGPV($memberid,$periode){
        $data = array();
        $this->db->select("m.id,m.nama,format(m.ps,0)as fps,format(m.psbak,0)as fpsbak,format(m.pgs,0)as fpgs,format(m.pgsbak,0)as fpgsbak,t.name as jenjang",false)
            ->from('member m')->join('jenjang t','m.jenjang_id=t.id','left')->where('m.sponsor_id',$memberid);
        if($periode == '1')$this->db->order_by('m.pgs','desc');
        else $this->db->order_by('m.pgsbak','desc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getTotalTGPV($memberid){
        $data=array();
        $q = $this->db->select("format(sum(m.pgs),0)as fpgs,format(sum(m.pgsbak),0)as fpgsbak",false)
            ->from('member m')->where('m.sponsor_id',$memberid)->get();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
/*Updated by Boby (2009-11-17)*/
    public function getTotalAGPV($memberid){
        $data=array();
        $q = $this->db->select("format(sum(m.apgs),0)as apgs",false)
            ->from('member m')->where('m.id',$memberid)->get();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
/*Updated by Boby (2009-11-17)*/


  /*
    |--------------------------------------------------------------------------
    | Report Summary Bonus
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @development by www.smartindo-technology.com
    | @created 2009-06-25
    |
    */
    
    public function sumBonus($periode,$sort){
        $data = array();
        $this->db->select("v.member_id,v.jenjang,v.nama,sum(v.nominal)as nominal,format(sum(v.nominal),0)as fnominal,v.npwp",false)
            ->from('v_bonus v');
        $this->db->where('v.periode',$periode);
        $this->db->group_by('v.member_id');
        if($sort == 'nama')$this->db->order_by('v.nama','asc');
        else$this->db->order_by('nominal','desc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }  
    public function get_downline($page,$member_id,$fromdate,$todate){
        $data = array();
        $this->db->select("n.id,n.member_id,n.leftval as lft,n.rightval as rgt,m.posisi as level,
                          m.nama as name,m.sponsor_id,date_format(m.tglaplikasi,'%d-%m-%Y')as ftglaplikasi",false);
        $this->db->from('networks n');
	$this->db->join('member m','n.member_id=m.id','left');
        $this->db->where('m.sponsor_id',$member_id);
        $this->db->order_by('n.id','asc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
		$row['level0'] = $this->get_jumlah_level_khusus($row['member_id'],$fromdate,$todate);
		$row['level1'] = $this->get_jumlah_level($page+1,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level2'] = $this->get_jumlah_level($page+2,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level3'] = $this->get_jumlah_level($page+3,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level4'] = $this->get_jumlah_level($page+4,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level5'] = $this->get_jumlah_level($page+5,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level6'] = $this->get_jumlah_level($page+6,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level7'] = $this->get_jumlah_level($page+7,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level8'] = $this->get_jumlah_level($page+8,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level9'] = $this->get_jumlah_level($page+9,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['level10'] = $this->get_jumlah_level($page+10,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$row['fjml'] = number_format($row['level1']['total']+$row['level2']['total']+$row['level3']['total']+$row['level4']['total']+$row['level5']['total']+$row['level6']['total']+$row['level7']['total']+$row['level8']['total']+$row['level9']['total']+$row['level10']['total'],'0','',',');
				
		$row['total'] = $this->get_jumlah_level(0,$row['lft'],$row['rgt'],$row['level'],$fromdate,$todate);
		$data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function get_jumlah_level($posisi,$lft=0,$rgt=0,$level=0,$fromdate,$todate){
        $data = array();
	    
	    if($posisi > 0)$where = "n.leftval >= $lft and n.rightval <= $rgt and (m.posisi - $level) = $posisi and (m.tglaplikasi between '$fromdate' and '$todate')";
	    else $where = "n.leftval >= $lft and n.rightval <= $rgt and (m.tglaplikasi between '$fromdate' and '$todate')";
	    
	    $this->db->select("count(*) as total,count(*)as ftotal",false);
            
	    $this->db->from('networks n');
            $this->db->join('member m','n.member_id=m.id','left');
	
	$this->db->where($where);
	//$this->db->group_by('level');
	//$this->db->limit($show_level,1);
	$q=$this->db->get();
	//echo $this->db->last_query()."<br>";
	if($q->num_rows()>0){
	    $data = $q->row_array();
	}else{
	    $data['fjml'] = 0;
	    $data['total']['ftotal'] = 0;
	}
	$q->free_result();
	return $data;
    }   
      
    public function get_jumlah_level_khusus($memberid,$fromdate,$todate){
        $data = array();
	    
	    $where = "n.member_id = '$memberid' and (m.tglaplikasi between '$fromdate' and '$todate')";
	    $this->db->select("count(*) as total,count(*)as ftotal",false);
	    
	    $this->db->from('networks n');
            $this->db->join('member m','n.member_id=m.id','left');
	
	$this->db->where($where);
	$q=$this->db->get();
	//echo $this->db->last_query()."<br>";
	if($q->num_rows()>0){
	    $data = $q->row_array();
	}
	$q->free_result();
	return $data;
    }
    
    public function kualifikasiLeader($periode,$memberid){
        $data = array();
        $this->db->select("q.member_id,m.nama,date_format(q.tgl,'%M %Y')as ftgl,q.qty",false)
            ->from('leader_qualified q')->join('member m','q.member_id=m.id','left');
        if($memberid){
            $this->db->where('member_id',$memberid);            
        }else{
            $this->db->where('q.tgl',$periode);
        }
        $this->db->order_by('q.tgl','desc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countLeader($periode){
        $data = array();
        $q = "SELECT q.member_id, m.nama, sum(q.qty) as lqty 
                FROM (leader_qualified q) 
                LEFT JOIN member m ON q.member_id=m.id 
                WHERE substr(q.tgl,1,7) >= substr(DATE_ADD('$periode', INTERVAL -11 month),1,7) 
	and substr(q.tgl,1,7) <= substr('$periode',1,7) GROUP BY q.member_id ORDER BY lqty desc";
        $q = $this->db->query($q);
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function get_dropdown(){
        $data = array();
        $this->db->select("date_format(periode,'%Y-%m-%d') as tgl,date_format(periode,'%M - %Y')as ftgl",false);
        $this->db->from('bonus');
        $this->db->group_by('periode');
        $this->db->order_by('periode','desc');
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['tgl']]=$row['ftgl'];    
            }
        }
        $q->free_result();
        return $data;
    }
    public function get_hightjenjang($memberid){
        $data = array();
        $q = "j.jenjangbaru,a.name,a.decription as jenjang from jenjang_history j left join jenjang a on j.jenjangbaru=a.id where j.member_id='$memberid' order by jenjangbaru desc limit 0,1";
        $this->db->select($q);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
                $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
	/* Created by Boby 2013 */
	public function omsetPerKota($fromdate, $todate, $leader){
        $data = array();
		//$leader = 1;
		$query = "
		SELECT *, dt4.1+dt4.2+dt4.3+dt4.4+dt4.5+dt4.6+dt4.7+dt4.8+dt4.9+dt4.10+dt4.11+dt4.12 AS total
		FROM (
			SELECT ";
				if($leader == 1){$query .= " leader, nmLeader as kota";}else{$query .= " kota ";} $query.="
				, IFNULL(SUM(bln1),0) AS '1' , IFNULL(SUM(bln2),0) AS '2'
				, IFNULL(SUM(bln3),0) AS '3' , IFNULL(SUM(bln4),0) AS '4'
				, IFNULL(SUM(bln5),0) AS '5' , IFNULL(SUM(bln6),0) AS '6'
				, IFNULL(SUM(bln7),0) AS '7' , IFNULL(SUM(bln8),0) AS '8'
				, IFNULL(SUM(bln9),0) AS '9' , IFNULL(SUM(bln10),0) AS '10'
				, IFNULL(SUM(bln11),0) AS '11' , IFNULL(SUM(bln12),0) AS '12'
			FROM (
				SELECT";
					if($leader == 1){$query .= " bln, leader, nmLeader ";}else{$query .= " bln, kota ";} $query.="
					, CASE WHEN bln = 1 THEN omset END AS 'bln1' , CASE WHEN bln = 2 THEN omset END AS 'bln2'
					, CASE WHEN bln = 3 THEN omset END AS 'bln3' , CASE WHEN bln = 4 THEN omset END AS 'bln4'
					, CASE WHEN bln = 5 THEN omset END AS 'bln5' , CASE WHEN bln = 6 THEN omset END AS 'bln6'
					, CASE WHEN bln = 7 THEN omset END AS 'bln7' , CASE WHEN bln = 8 THEN omset END AS 'bln8'
					, CASE WHEN bln = 9 THEN omset END AS 'bln9' , CASE WHEN bln = 10 THEN omset END AS 'bln10'
					, CASE WHEN bln = 11 THEN omset END AS 'bln11' , CASE WHEN bln = 12 THEN omset END AS 'bln12'
				FROM (
					SELECT bln, txt, num, SUM(omset)AS omset, ";
					if($leader == 1){$query .= " leader, nmLeader ";}else{$query .= " kota_id, kota ";} $query.="
					FROM (
						SELECT 
							MONTH(so.tgl)AS bln, CONCAT(MONTHNAME(so.tgl), ' ', YEAR(so.tgl)) AS txt, CONCAT(YEAR(so.tgl), MONTH(so.tgl)) AS num
							, so.stockiest_id, IFNULL(SUM(so.totalharga),0) AS omset, ";
							if($leader == 1){$query .= " stc.leader_id AS leader, l.nama AS nmLeader ";}
							else{$query .= " stc.kota_id, IFNULL(k.`name`, '__Unknown__') AS kota ";} $query.="
						FROM so
						LEFT JOIN stockiest stc ON so.stockiest_id = stc.id ";
						if($leader != 1){$query .= " LEFT JOIN kota k ON stc.kota_id = k.id ";}
						else{$query .= " LEFT JOIN member l ON stc.leader_id = l.id ";} 
						$query.="
						WHERE so.tgl BETWEEN '$fromdate' AND ";
						if($todate==$fromdate){
							// $query .= "LAST_DAY('$fromdate' + INTERVAL 5 MONTH)";
							$query .= "'$todate' ";
						}else{
							$query .= "'$todate' ";
						}
						$query .="
						GROUP BY MONTHNAME(so.tgl), so.stockiest_id";
					if($leader == 1){$query .= " 
					)AS dt1
					GROUP BY bln, leader
					ORDER BY num, leader
				)AS dt2
				ORDER BY leader, num
			)AS dt3
			GROUP BY leader
		)AS dt4
		ORDER BY total DESC ";}else{$query .= " 
					)AS dt1
					GROUP BY bln, kota
					ORDER BY num, kota_id
				)AS dt2
				ORDER BY kota, num
			)AS dt3
			GROUP BY kota
		)AS dt4
		ORDER BY total DESC";} $query.="
		";
		
		$qry = $this->db->query($query);
		
		// echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	/* End created by Boby 2013 */
	
	/* Created by Boby 20141020 */
	public function get_q($q){
        $data = array();
		$data['01-01']='January';
		$data['02-01']='February';
		$data['03-01']='March';
		$data['04-01']='April';
		$data['05-01']='May';
		$data['06-01']='June';
		$data['07-01']='July';
		$data['08-01']='August';
		$data['09-01']='September';
		$data['10-01']='October';
		$data['11-01']='November';
		$data['12-01']='December';
        return $data;
    }
	public function get_year_report(){
        $data = array();
		$thn=date("Y");
		for($i=$thn;$i>='2009';$i--){
			$data[$i]=$i;
		}
        return $data;
    }
	public function viewStcAcv($thn, $bln)
    {
        $data=array();
		$tgl = $thn."-".$bln;
		
		$bln = substr($tgl, -5, 2);
		// echo $bln;
		/*
		if($bln > 9 && $thn > 2013){
			$qry1 = "_, IFNULL(so.mkt/1.1,0) AS mktSo";
			$qry2 = "_, IFNULL(so.oms/1.1,0) AS omsSo";
			$qry3 = "_, IFNULL(ro.mkt/1.1,0) - IFNULL(rtr.mkt/1.1,0) AS mktRo";
			$qry4 = "_, IFNULL(ro.oms/1.1,0) - IFNULL(rtr.oms/1.1,0) AS omsRo";
		}else{
			$qry1 = "";
			$qry2 = "";
			$qry3 = "";
			$qry4 = "";
		}
		*/
		
		$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ";
		$akhir = "LAST_DAY('$tgl') ";
		//echo $tgl;
		$q = $this->db->query("
			SELECT s.id, m.nama, s.no_stc, pb.ps, pb.tgpv, pb.ba, CONCAT('Region', k.region) AS region -- _, r.nama AS region
				, IFNULL(so.mkt,0) AS mktSo".$qry1."
				, IFNULL(so.oms,0) AS omsSo".$qry2."
				, IFNULL(ro.mkt,0) - IFNULL(rtr.mkt,0) AS mktRo".$qry3."
				, IFNULL(ro.oms,0) - IFNULL(rtr.oms,0) AS omsRo".$qry4."
				, IFNULL(so.jml,0)+IFNULL(ro.jml,0)+IFNULL(rtr.jml,0) AS jmldt
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_id = k.id
			LEFT JOIN region r on k.region = r.id
			LEFT JOIN member m ON s.id = m.id
			LEFT JOIN(
				SELECT tgl, member_id AS id, ps, pgs+ps-pgs_cut AS tgpv, sumbangan/20000000 AS ba
				FROM pgs_bulanan
				WHERE tgl = $akhir
			)AS pb ON s.id = pb.id
			LEFT JOIN(
				SELECT id, SUM(mkt)AS mkt, SUM(oms)AS oms, SUM(jmlharga)AS jml
				FROM(
					SELECT stockiest_id AS id
						, CASE WHEN pv = 0 THEN jmlharga ELSE 0 END AS mkt
						, CASE WHEN pv <> 0 THEN jmlharga ELSE 0 END AS oms
						, jmlharga
					FROM so
					RIGHT JOIN so_d sod ON so.id = sod.so_id
					WHERE tgl BETWEEN $awal AND $akhir
					AND stockiest_id <> 0
				)AS dt
				GROUP BY id
			)AS so ON s.id = so.id
			LEFT JOIN(
				SELECT id, SUM(mkt)AS mkt, SUM(oms)AS oms, SUM(jmlharga)AS jml
				FROM(
					SELECT member_id AS id
						, CASE WHEN pv = 0 THEN jmlharga ELSE 0 END AS mkt
						, CASE WHEN pv <> 0 THEN jmlharga ELSE 0 END AS oms
						, jmlharga
					FROM ro
					RIGHT JOIN ro_d rod ON ro.id = rod.ro_id
					WHERE `date` BETWEEN $awal AND $akhir
					AND stockiest_id = 0
				)AS dt
				GROUP BY id
			)AS ro ON s.id = ro.id
			LEFT JOIN(
				SELECT id, SUM(mkt)AS mkt, SUM(oms)AS oms, SUM(jmlharga)AS jml
				FROM(
					SELECT stockiest_id AS id
						, CASE WHEN pv = 0 THEN jmlharga ELSE 0 END AS mkt
						, CASE WHEN pv <> 0 THEN jmlharga ELSE 0 END AS oms
						, jmlharga
					FROM retur_titipan rt
					RIGHT JOIN retur_titipan_d rtd ON rt.id = rtd.retur_titipan_id
					WHERE tgl BETWEEN $awal AND $akhir
					AND stockiest_id <> 0
				)AS dt
				GROUP BY id
			)AS rtr ON s.id = rtr.id
			WHERE s.status = 'active'
			ORDER BY region, no_stc
		");
		// echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	public function stockiest_acv_export($thn, $bln){
        $data = array();
		$tgl = $thn."-".$bln;
		$bln = substr($tgl, -5, 2);
		echo $bln;
		/*
		if($bln > 9 && $thn > 2013){
			$qry1 = ", IFNULL(so.mkt/1.1,0) AS mktSo";
			$qry2 = ", IFNULL(so.oms/1.1,0) AS omsSo";
			$qry3 = ", IFNULL(ro.mkt/1.1,0) - IFNULL(rtr.mkt/1.1,0) AS mktRo";
			$qry4 = ", IFNULL(ro.oms/1.1,0) - IFNULL(rtr.oms/1.1,0) AS omsRo";
		}else{
			$qry1 = ", IFNULL(so.mkt,0) AS mktSo";
			$qry2 = ", IFNULL(so.oms,0) AS omsSo";
			$qry3 = ", IFNULL(ro.mkt,0) - IFNULL(rtr.mkt,0) AS mktRo";
			$qry4 = ", IFNULL(ro.oms,0) - IFNULL(rtr.oms,0) AS omsRo";
		}
		
		if($bln > 9 && $thn > 2013){
			$qry1 = "-- , IFNULL(so.mkt/1.1,0) AS mktSo";
			$qry2 = ", IFNULL(so.oms/1.1,0) AS omsSo";
			$qry3 = "-- , IFNULL(ro.mkt/1.1,0) - IFNULL(rtr.mkt/1.1,0) AS mktRo";
			$qry4 = ", IFNULL(ro.oms/1.1,0) - IFNULL(rtr.oms/1.1,0) AS omsRo";
		}else{
			$qry1 = "-- , IFNULL(so.mkt,0) AS mktSo";
			$qry2 = ", IFNULL(so.oms,0) AS omsSo";
			$qry3 = "-- , IFNULL(ro.mkt,0) - IFNULL(rtr.mkt,0) AS mktRo";
			$qry4 = ", IFNULL(ro.oms,0) - IFNULL(rtr.oms,0) AS omsRo";
		}
		*/
		
		$qry1 = "-- , IFNULL(so.mkt,0) AS mktSo";
		$qry2 = ", IFNULL(so.oms,0) AS omsSo";
		$qry3 = "-- , IFNULL(ro.mkt,0) - IFNULL(rtr.mkt,0) AS mktRo";
		$qry4 = ", IFNULL(ro.oms,0) - IFNULL(rtr.oms,0) AS omsRo";
		
		$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ";
		$akhir = "LAST_DAY('$tgl') ";
        $qry = "
			SELECT s.id, m.nama, s.no_stc, pb.ps, pb.tgpv, pb.ba, k.region
				".$qry1."
				".$qry2."
				".$qry3."
				".$qry4."
				, IFNULL(so.jml,0)+IFNULL(ro.jml,0)+IFNULL(rtr.jml,0) AS jmldt
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_id = k.id
			LEFT JOIN member m ON s.id = m.id
			LEFT JOIN(
				SELECT tgl, member_id AS id, ps, pgs+ps-pgs_cut AS tgpv, sumbangan/20000000 AS ba
				FROM pgs_bulanan
				WHERE tgl = $akhir
			)AS pb ON s.id = pb.id
			LEFT JOIN(
				SELECT id, SUM(mkt)AS mkt, SUM(oms)AS oms, SUM(jmlharga)AS jml
				FROM(
					SELECT stockiest_id AS id
						, CASE WHEN pv = 0 THEN jmlharga ELSE 0 END AS mkt
						, CASE WHEN pv <> 0 THEN jmlharga ELSE 0 END AS oms
						, jmlharga
					FROM so
					RIGHT JOIN so_d sod ON so.id = sod.so_id
					WHERE tgl BETWEEN $awal AND $akhir
					AND stockiest_id <> 0
				)AS dt
				GROUP BY id
			)AS so ON s.id = so.id
			LEFT JOIN(
				SELECT id, SUM(mkt)AS mkt, SUM(oms)AS oms, SUM(jmlharga)AS jml
				FROM(
					SELECT member_id AS id
						, CASE WHEN pv = 0 THEN jmlharga ELSE 0 END AS mkt
						, CASE WHEN pv <> 0 THEN jmlharga ELSE 0 END AS oms
						, jmlharga
					FROM ro
					RIGHT JOIN ro_d rod ON ro.id = rod.ro_id
					WHERE `date` BETWEEN $awal AND $akhir
					AND stockiest_id = 0
				)AS dt
				GROUP BY id
			)AS ro ON s.id = ro.id
			LEFT JOIN(
				SELECT id, SUM(mkt)AS mkt, SUM(oms)AS oms, SUM(jmlharga)AS jml
				FROM(
					SELECT stockiest_id AS id
						, CASE WHEN pv = 0 THEN jmlharga ELSE 0 END AS mkt
						, CASE WHEN pv <> 0 THEN jmlharga ELSE 0 END AS oms
						, jmlharga
					FROM retur_titipan rt
					RIGHT JOIN retur_titipan_d rtd ON rt.id = rtd.retur_titipan_id
					WHERE tgl BETWEEN $awal AND $akhir
					AND stockiest_id <> 0
				)AS dt
				GROUP BY id
			)AS rtr ON s.id = rtr.id
			HAVING jmldt <> 0
			ORDER BY region, no_stc
		";
        // echo $qry;
        $data = $this->db->query($qry);
        return $data;
    }
	/* End created by Boby 20141020 */
}
?>