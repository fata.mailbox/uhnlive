<?php
class MTree extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
     /*
    |--------------------------------------------------------------------------
    | Networks List
    |--------------------------------------------------------------------------
    |
    | for display network list
    |
    | @param member id default #1
    | @return void
    | @author taQwa
    | @author 2008-11-05
    |
    */
    public function get_tree($show_level='',$auto=0,$parentid=0,$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$this->db->select("a.id - $auto as auto_id,a.parentid - $auto as parentid,a.member_id,m.jenjang_id,
						m.nama as name,m.posisi - $level as level,j.name as jenjang,format(m.ps,0)as fps,
						format(((m.ps+m.pgs)-m.pgs_cut),0)as fpgs,
						if((((m.ps+m.pgs)-m.pgs_cut)+m.sumbangan) >= 60000000 and m.ps >= 1000000,'qualified green',if((m.ps+m.pgs) >= 60000000,'qualified',''))as qualified,
						,format(m.aps+m.apgs,0)as apgs",false)
			->from('networks a')
			->join('member m','a.member_id=m.id','left')
			->join('jenjang j','m.jenjang_id=j.id','left')
			/* ->join('jenjang j2','m.jenjang_id2=j2.id','left') */
				/* update by Boby 2011-01-03 */
				->join('users u','m.id=u.id','left')
				/*->where('u.banned_id = ', '0')
				 end update by Boby 2011-01-03 */
			->where('a.leftval >=',$leftval)
			->where('a.rightval <=',$rightval)
			->where('a.active',1);
			/* if($show_level != ''){
				$this->db->where("m.posisi - $level<=",$show_level);
			} */
			
			//$this->db->order_by('m.posisi','asc');
			$q=$this->db->get();
			//echo $this->db->last_query();
		}else{
		 
			/*	
			 $this->db->select("a.id - $auto as auto_id, a.parentid - $auto as parentid, a.member_id, p.jenjang_id, m.nama as name,
								 m.posisi - $level as level, j.name as jenjang, format(ifnull(p.ps,0), 0)as fps, 
							   format(ifnull(m.ps+p.pgs,0), 0)as fpgs",false)
		  ->from('pgs_bulanan p')
		  ->join('networks n','p.member_id=n.member_id','left')
		  ->join('member m','p.member_id=m.id','left')
		  ->join('jenjang j','p.jenjang_id=j.id','left')
			->where('a.leftval >=', $leftval)
			->where('a.rightval <=', $rightval)
			->where('a.active', 1)
		  ->where('substr(m.tglaplikasi,1,7) <=', $show_level);
			*/
			$q = "SELECT 
				-- a.id - $auto as auto_id
				-- , a.parentid - $auto as parentid
				a.id - $auto as auto_id
				, a.parentid - $auto as parentid
				, a.member_id, p.jenjang_id, m.nama as name
				, m.posisi - $level as level
				, j.name as jenjang
				, format(ifnull(p.ps,0), 0)as fps
				, format(ifnull(p.ps+p.pgs,0), 0)as fpgs
				, format(((p.ps+p.pgs)-p.pgs_cut)+p.sumbangan,0)as fpgs
				, if((((p.ps+p.pgs)-p.pgs_cut)+p.sumbangan) >= 60000000 and p.ps >= 1000000,'qualified green',if((p.ps+p.pgs) >= 60000000,'qualified',''))as qualified
			FROM (pgs_bulanan p)
			LEFT JOIN networks a ON a.member_id=p.member_id
			LEFT JOIN member m ON a.member_id=m.id
			LEFT JOIN jenjang j ON p.jenjang_id=j.id
			LEFT JOIN users u ON m.id=u.id
			WHERE `a`.`leftval` >= $leftval AND `a`.`rightval` <= $rightval and substr(p.tgl,1,7) = '$show_level'
			AND substr(m.tglaplikasi,1,7) <= '$show_level' and a.active = 1
			ORDER BY auto_id ";
			// echo $q;
			//$this->db->query($q);
			
			$q = $this->db->query($q);
			//  ORDER BY m.posisi asc `u`.`banned_id` = '0' AND 
		}
		
		// echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
			$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
    }
    public function get_member($memberid=''){
	$data=array();
	$q=$this->db->select("n.id as auto_id,n.parentid,n.leftval as lft,n.rightval as rgt,m.posisi as level",false)
	    ->from('networks n')
	    ->join('member m','n.member_id=m.id','left')
	    ->where('n.member_id',$memberid)
	    ->get();
	    
	//echo $this->db->last_query();
	if($q->num_rows()>0){
	    $data = $q->row_array();
	}
	$q->free_result();
	return $data;
    }
   /**
    * check crossline OK?
    *
    * @access public
    * @param string $placementid, $introducerid
    * @return bool
    **/
    public function check_upline($p,$i){
        $data =array();
        $q = $this->db->query("SELECT f_check_crossline('$p','$i') as l_result");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        //print_r($data);
        $q->free_result();
        return $data['l_result'];
    } 
    public function dropdown_autoid($auto=0,$leftval=0,$rightval=0){
        $data = array();
	$q=$this->db->select("parentid,count(*)as l_count",false)
	    ->from('networks')
	    ->where('leftval >=',$leftval)
	    ->where('rightval <=',$rightval)
	    ->group_by('parentid')
	    ->order_by('count(*)','asc')
	    ->get();
	//echo $this->db->last_query();
	if($q->num_rows()>0){
	    foreach($q->result_array()as $row){
		$data[$row['l_count']]=$row['parentid'];
	    }
	}
	$q->free_result();
	return $data;
    }
    
    
    
    
    
    
    
    
    
    
    public function check_member($memberid){
        $i = $this->db->select('id')
            ->from('membermaster')
            ->where('id', $placementid)
            ->get();
	return $var = ($i->num_rows() > 0) ? false : true;
    }
    
    
    
    
    
    
    
    public function getNetworks($memberid){
        $data = array();
        //$where = "m.user_id = '".$memberid."' and m.leftval >=m.leftval and m.rightval <= m.rightval";
        $q = $this->db->select('m.user_id,mm.name,date_format(mm.tglaplikasi, "%d/%m/%y")as date,mm.account_type,m.sponsor,
                               m.downline_left,m.downline_right,m.level,m.total_left_0,m.upgrade_left_1, m.total_left_1 - m.upgrade_left_1 as total_left_1, m.total_left_d,m.total_left_0 + m.total_left_1 as
                               total_left,m.total_right_0 + m.total_right_1 as total_right,
                               m.total_right_0, m.upgrade_right_1, m.total_right_1 - m.upgrade_right_1 as total_right_1, m.total_right_d,m.sisa_pb0_left,m.sisa_pb0_right,sisa_pb1_left,sisa_pb1_right',false)
            ->from('member m')
            ->join('membermaster mm','m.member_id=mm.id','left')
            ->where('m.user_id',$memberid)
            ->limit(1)
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows() >0){
                $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function dropdown_periode(){
        $data = array();
		//$bulan = date('');
        $q = $this->db->select("date_format(tgl,'%Y-%m')as tgl,date_format(tgl,'%M - %Y')as ftgl",false)
            ->from('pgs_bulanan')->group_by('tgl')->order_by('tgl','desc')->get();
	
		$data[''] = 'Bulan Sekarang'; // Modified by Boby 20121205
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['tgl']]=$row['ftgl'];    
            }
        }
		/* Created by Boby 20121205 */
		// unset($data);
		// $data[''] = 'Bulan Sekarang';
		/* End created by Boby 20121205 */
        $q->free_result();
        return $data;
    }
    public function getNetworksList($level,$leftval,$rightval){
        $data = array();
        //$where = substr('m.user_id',9,1)." = 1 and m.leftval >=m.leftval and m.rightval <= m.rightval";
        $q = $this->db->select("m.member_id as user_id,mm.name,p.name as paket,sponsor,sponsor_lr,substr(introducerid,1,8)as introducerid,date_format(mm.tglaplikasi, '%d-%m-%y')as date,m.sponsor,m.level-$level as level",false)
            ->from('member m')
            ->join('membermaster mm','m.member_id=mm.id','left')
            ->join('paket p','mm.paket_id=p.id','left')
            ->where('m.leftval >=',$leftval)
            ->where('m.rightval <=',$rightval)
            ->order_by('m.level','asc')
            ->order_by('m.id','asc')
            ->group_by('mm.id')
            ->limit(500)
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows() >0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    /** 
    * get sponsor
    *
    * @access public
    * @param string $sponsor
    * @return user_id
    **/
    public function getSponsor($sponsor){
        $i = $this->db->select("m.sponsor,mm.name,date_format(mm.tglaplikasi, '%d/%m/%y')as date",false)
            ->from('member m')->join('membermaster mm','m.member_id=mm.id','left')
            ->where('user_id', $sponsor)
            ->get();
            //echo $this->db->last_query();
	return $var = ($i->num_rows() > 0) ? $i->row_array() : false;
    }
    public function getUp($sponsor){
        $i = $this->db->select("m.sponsor,mm.name,date_format(mm.tglaplikasi, '%d/%m/%y')as date",false)
            ->from('member m')->join('membermaster mm','m.member_id=mm.id','left')
            ->where('user_id', $sponsor)
            ->get();
            //echo $this->db->last_query();
	return $var = ($i->num_rows() > 0) ? $i->row_array() : false;
    }
    /**
    * check_placementid
    *
    * @access public
    * @param string $placementid
    * @return bool
    **/
    
    public function check_member_downline($memberid){
        $i = $this->db->select('n.member_id,n.leftval,n.rightval,m.posisi as level')
            ->from('networks n')
	    ->join('member m','n.member_id=m.id','left')
            ->where('member_id', $memberid)
            ->get();
            //echo $this->db->last_query();
	return $var = ($i->num_rows() > 0) ? $i->row() : false;
    }
    public function sumMember($leftval,$rightval){
        $data = array();
        //$where = "m.user_id = '".$memberid."' and m.leftval >=m.leftval and m.rightval <= m.rightval";
        $q = $this->db->select("format(count(*),0) as ftotal",false)
            ->from('member m')
            ->where('m.leftval >=',$leftval)
            ->where('m.rightval <=',$rightval)
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows() >0){
                $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function get_jumlah_level($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
	$q=$this->db->select("m.posisi - $level as level,count(*)as jml",false)
	    ->from('networks n')
	    ->join('member m','n.member_id=m.id','left')
	    ->where('n.leftval >=',$leftval)
	    ->where('n.rightval <=',$rightval)
	    ->group_by('m.posisi')
	    ->limit($show_level,1)
	    ->get();
	// echo $this->db->last_query();
	if($q->num_rows()>0){
	    foreach($q->result_array()as $row){
		$data[]=$row;
	    }
	}
	$q->free_result();
	return $data;
    }   
    
    public function searchMember($option, $keywords=0,$num, $offset){
        $data = array();
        $this->db->select('d.member_id,m.account,m.name,d.introducerid,d.introducer_lr,m.created',false);
        $this->db->from('member d');
        $this->db->join('membermaster m','d.member_id=m.id','left');
        $this->db->where('substr(d.introducerid,1,6)',$this->session->userdata('username'));
        if($option ==='memberid'){
            $this->db->like('m.id', $keywords, 'after');
        }
        else{
            $this->db->like('m.name', $keywords, 'after');
        }
        $this->db->group_by('d.member_id');
        $this->db->order_by('d.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countSearchMember($option=0,$keywords=0){
        $this->db->select('count(*)',false);
        $this->db->from('member d');
        $this->db->join('membermaster m','d.member_id=m.id','left');
        $this->db->where('substr(d.introducerid,1,6)',$this->session->userdata('username'));
        if($option ==='memberid'){
            $this->db->like('m.id', $keywords, 'after');
        }
        else{
            $this->db->like('m.name', $keywords, 'after');
        }
        $this->db->group_by('d.member_id');
        $q = $this->db->get();
        return $var = ($q->num_rows()>0) ? $q->num_rows() : false;
    }
    
    public function getLevel($member_id){
        $data = array();
        $q = $this->db->select('level',false)->from('membermaster')->where('id',$member_id)->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    public function getIntroducerList($member_id,$level=0,$offset){
        $data = array();
        $this->db->select("m.id,m.level - ".$level." as level,m.name,m.enroller_id,m.created",false);
        $this->db->from('membermaster m');
        $this->db->where('m.enroller_id',$member_id);
        $this->db->where("m.id !=",$member_id);
        $this->db->order_by('m.level','asc');
        $this->db->limit($offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $i++;
                $row['i'] = $i;
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function get_network_generasi($generasi,$level,$lft,$rgt){
        $data = array();
	if($generasi){
	    $where = "n.leftval >=".$lft." and n.rightval <= ".$rgt." and m.posisi-$level <= 11" ;
	    $where = $where." and m.posisi - $level = $generasi";
	    
	    
	    $this->db->select("m.id as member_id,m.nama,m2.id as sponsorid,m2.nama as namasponsor,j.name as jenjang,
			      date_format(m.created, '%d/%m/%Y')as ftglaplikasi,if(m.jenjang_id <= 4 and m.ps >= 200000,'y',if(m.jenjang_id > 4 and m.ps >= 1000000,'y','x'))as tupo,
			      format(m.ps,0)as fps,format(m.pgs+m.ps,0)as fpgs",false);
	    $this->db->from('networks n');
	    $this->db->join('member m','n.member_id=m.id','left');
	    $this->db->join('jenjang j','m.jenjang_id=j.id','left');	
	    $this->db->join('member m2','m.enroller_id=m2.id','left');
	    $this->db->where($where);
	    $this->db->order_by('m.posisi','asc');
	    $this->db->order_by('m.nama','asc');
	    $this->db->limit(5000);
	    $q = $this->db->get();
	    //echo $level." - ".$this->db->last_query()."<br><br>";
	    if($q->num_rows() >0){
		$i = 0 ;
		foreach($q->result_array() as $row){
		    $i++;
		    $row['i'] = $i;
		    
		    $data[] = $row;
		}
	    }
	    $q->free_result();
	    return $data;
	}else return false;
    }
    
}
?>