<?php
class MTracking extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    public function get_stc_trips(){
        $data = array();
		$q = "
			select 
			id, trip_name, start_date, end_date 
			from stockiest_trip
			order by id asc
		";
        $q=$this->db->query($q);
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function getTracking($member_id,$startdate,$enddate){
        $data = array();
		$q = "
			SELECT s.id, m.nama, s.no_stc, s.created, s.type AS tipe, CONCAT('Region',k.region)AS region
				, IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0) AS oms1, IFNULL(so_.so1,0) AS so1, IFNULL(po_.so1,0) AS po1, CASE WHEN IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0) >= 35000000 AND IFNULL(po_.so1,0) >= 1000000 THEN 1 ELSE 0 END AS Q1
				, IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0) AS oms2, IFNULL(so_.so2,0) AS so2, IFNULL(po_.so2,0) AS po2, CASE WHEN IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0) >= 35000000 AND IFNULL(po_.so2,0) >= 1000000 THEN 1 ELSE 0 END AS Q2
				, IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0) AS oms3, IFNULL(so_.so3,0) AS so3, IFNULL(po_.so3,0) AS po3, CASE WHEN IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0) >= 35000000 AND IFNULL(po_.so3,0) >= 1000000 THEN 1 ELSE 0 END AS Q3
				, IFNULL(ro.ro4,0)+IFNULL(pjm.pjm4,0) - IFNULL(rtr.rtr4,0) AS oms4, IFNULL(so_.so4,0) AS so4, IFNULL(po_.so4,0) AS po4, CASE WHEN IFNULL(ro.ro4,0)+IFNULL(pjm.pjm4,0) - IFNULL(rtr.rtr4,0) >= 35000000 AND IFNULL(po_.so4,0) >= 1000000 THEN 1 ELSE 0 END AS Q4
				, IFNULL(ro.ro5,0)+IFNULL(pjm.pjm5,0) - IFNULL(rtr.rtr5,0) AS oms5, IFNULL(so_.so5,0) AS so5, IFNULL(po_.so5,0) AS po5, CASE WHEN IFNULL(ro.ro5,0)+IFNULL(pjm.pjm5,0) - IFNULL(rtr.rtr5,0) >= 35000000 AND IFNULL(po_.so5,0) >= 1000000 THEN 1 ELSE 0 END AS Q5
				, IFNULL(ro.ro6,0)+IFNULL(pjm.pjm6,0) - IFNULL(rtr.rtr6,0) AS oms6, IFNULL(so_.so6,0) AS so6, IFNULL(po_.so6,0) AS po6, CASE WHEN IFNULL(ro.ro6,0)+IFNULL(pjm.pjm6,0) - IFNULL(rtr.rtr6,0) >= 35000000 AND IFNULL(po_.so6,0) >= 1000000 THEN 1 ELSE 0 END AS Q6
				, IFNULL(ro.ro7,0)+IFNULL(pjm.pjm7,0) - IFNULL(rtr.rtr7,0) AS oms7, IFNULL(so_.so7,0) AS so7, IFNULL(po_.so7,0) AS po7, CASE WHEN IFNULL(ro.ro7,0)+IFNULL(pjm.pjm7,0) - IFNULL(rtr.rtr7,0) >= 35000000 AND IFNULL(po_.so7,0) >= 1000000 THEN 1 ELSE 0 END AS Q7
				, IFNULL(ro.ro8,0)+IFNULL(pjm.pjm8,0) - IFNULL(rtr.rtr8,0) AS oms8, IFNULL(so_.so8,0) AS so8, IFNULL(po_.so8,0) AS po8, CASE WHEN IFNULL(ro.ro8,0)+IFNULL(pjm.pjm8,0) - IFNULL(rtr.rtr8,0) >= 35000000 AND IFNULL(po_.so8,0) >= 1000000 THEN 1 ELSE 0 END AS Q8
				, IFNULL(ro.ro9,0)+IFNULL(pjm.pjm9,0) - IFNULL(rtr.rtr9,0) AS oms9, IFNULL(so_.so9,0) AS so9, IFNULL(po_.so9,0) AS po9, CASE WHEN IFNULL(ro.ro9,0)+IFNULL(pjm.pjm9,0) - IFNULL(rtr.rtr9,0) >= 35000000 AND IFNULL(po_.so9,0) >= 1000000 THEN 1 ELSE 0 END AS Q9
				, IFNULL(ro.ro10,0)+IFNULL(pjm.pjm10,0) - IFNULL(rtr.rtr10,0) AS oms10, IFNULL(so_.so10,0) AS so10, IFNULL(po_.so10,0) AS po10, CASE WHEN IFNULL(ro.ro10,0)+IFNULL(pjm.pjm10,0) - IFNULL(rtr.rtr10,0) >= 35000000 AND IFNULL(po_.so10,0) >= 1000000 THEN 1 ELSE 0 END AS Q10
				, IFNULL(ro.ro11,0)+IFNULL(pjm.pjm11,0) - IFNULL(rtr.rtr11,0) AS oms11, IFNULL(so_.so11,0) AS so11, IFNULL(po_.so11,0) AS po11, CASE WHEN IFNULL(ro.ro11,0)+IFNULL(pjm.pjm11,0) - IFNULL(rtr.rtr11,0) >= 35000000 AND IFNULL(po_.so11,0) >= 1000000 THEN 1 ELSE 0 END AS Q11
				, IFNULL(ro.ro12,0)+IFNULL(pjm.pjm12,0) - IFNULL(rtr.rtr12,0) AS oms12, IFNULL(so_.so12,0) AS so12, IFNULL(po_.so12,0) AS po12, CASE WHEN IFNULL(ro.ro12,0)+IFNULL(pjm.pjm12,0) - IFNULL(rtr.rtr12,0) >= 35000000 AND IFNULL(po_.so12,0) >= 1000000 THEN 1 ELSE 0 END AS Q12
				, (IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0))+
				  (IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0))+
				  (IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0))+
				  (IFNULL(ro.ro4,0)+IFNULL(pjm.pjm4,0) - IFNULL(rtr.rtr4,0))+
				  (IFNULL(ro.ro5,0)+IFNULL(pjm.pjm5,0) - IFNULL(rtr.rtr5,0))+
				  (IFNULL(ro.ro6,0)+IFNULL(pjm.pjm6,0) - IFNULL(rtr.rtr6,0))+
				  (IFNULL(ro.ro7,0)+IFNULL(pjm.pjm7,0) - IFNULL(rtr.rtr7,0))+
				  (IFNULL(ro.ro8,0)+IFNULL(pjm.pjm8,0) - IFNULL(rtr.rtr8,0))+
				  (IFNULL(ro.ro9,0)+IFNULL(pjm.pjm9,0) - IFNULL(rtr.rtr9,0))+
				  (IFNULL(ro.ro10,0)+IFNULL(pjm.pjm10,0) - IFNULL(rtr.rtr10,0))+
				  (IFNULL(ro.ro11,0)+IFNULL(pjm.pjm11,0) - IFNULL(rtr.rtr11,0))+
				  (IFNULL(ro.ro12,0)+IFNULL(pjm.pjm12,0) - IFNULL(rtr.rtr12,0))
				  AS oms
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_id = k.id
			LEFT JOIN member m ON s.id = m.id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS ro1, SUM(oms2)AS ro2, SUM(oms3)AS ro3
					, SUM(oms4)AS ro4, SUM(oms5)AS ro5, SUM(oms6)AS ro6
					, SUM(oms7)AS ro7, SUM(oms8)AS ro8, SUM(oms9)AS ro9
					, SUM(oms10)AS ro10, SUM(oms11)AS ro11, SUM(oms12)AS ro12
				FROM(
					SELECT member_id
						, CASE WHEN (MONTH(ro.`date`)=MONTH('".$startdate."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('".$startdate."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms1
						, CASE WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms2
						, CASE WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms3
						, CASE WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms4
						, CASE WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms5
						, CASE WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms6
						, CASE WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms7
						, CASE WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms8
						, CASE WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms9
						, CASE WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms10
						, CASE WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms11
						, CASE WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms12
					FROM ro
					RIGHT JOIN ro_d rod ON ro.id = rod.ro_id
					WHERE ro.stockiest_id=0
					-- AND rod.pv > 0
					AND (
						CASE WHEN ro.`date` > '2015-02-28' THEN rod.pv > 0 
						ELSE rod.pv > 0 
						END
					)
					-- AND YEAR(ro.date) = 2017
					AND ro.date between '".$startdate."' and '".$enddate."'
					GROUP BY ro.id
				)AS ro_
				GROUP BY member_id
			)AS ro ON s.id = ro.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS pjm1, SUM(oms2)AS pjm2, SUM(oms3)AS pjm3
					, SUM(oms4)AS pjm4, SUM(oms5)AS pjm5, SUM(oms6)AS pjm6
					, SUM(oms7)AS pjm7, SUM(oms8)AS pjm8, SUM(oms9)AS pjm9
					, SUM(oms10)AS pjm10, SUM(oms11)AS pjm11, SUM(oms12)AS pjm12
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('".$startdate."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(pjm.tgl)=MONTH('".$startdate."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms4
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms5
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms6
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms7
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms8
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms9
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms10
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms11
						, CASE WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms12
					   FROM pinjaman_titipan pjm
					-- WHERE YEAR(pjm.tgl) = 2017
					WHERE pjm.tgl between '".$startdate."' and '".$enddate."'
				)AS pjm
				GROUP BY member_id
			)AS pjm ON s.id = pjm.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS rtr1, SUM(oms2)AS rtr2, SUM(oms3)AS rtr3
					, SUM(oms4)AS rtr4, SUM(oms5)AS rtr5, SUM(oms6)AS rtr6
					, SUM(oms7)AS rtr7, SUM(oms8)AS rtr8, SUM(oms9)AS rtr9
					, SUM(oms10)AS rtr10, SUM(oms11)AS rtr11, SUM(oms12)AS rtr12
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('".$startdate."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(rtr.tgl)=MONTH('".$startdate."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms1
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms2
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms3
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms4
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms5
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms6
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms7
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms8
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms9
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms10
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms11
						, CASE WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms12
					FROM retur_titipan rtr
					-- WHERE YEAR(rtr.tgl) = 2017
					WHERE rtr.tgl between '".$startdate."' and '".$enddate."'
				)AS rtr
				GROUP BY member_id
			)AS rtr ON s.id = rtr.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS so1, SUM(oms2)AS so2, SUM(oms3)AS so3
					, SUM(oms4)AS so4, SUM(oms5)AS so5, SUM(oms6)AS so6
					, SUM(oms7)AS so7, SUM(oms8)AS so8, SUM(oms9)AS so9
					, SUM(oms10)AS so10, SUM(oms11)AS so11, SUM(oms12)AS so12
				FROM(
					SELECT so.stockiest_id AS member_id
						, CASE WHEN (MONTH(so.tgl)=MONTH('".$startdate."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (MONTH(so.tgl)=MONTH('".$startdate."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms1
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms2
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms3
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms4
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms5
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms6
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms7
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms8
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms9
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms10
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms11
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms12
					FROM so
					RIGHT JOIN so_d sod ON so.id = sod.so_id
					WHERE 
					so.stockiest_id != 0
					AND sod.pv > 0
					-- AND YEAR(so.tgl) = 2017
					-- AND 
					AND so.tgl between '".$startdate."' and '".$enddate."'
					GROUP BY so.id
				)AS so
				GROUP BY member_id
			)AS so_ ON s.id = so_.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS so1, SUM(oms2)AS so2, SUM(oms3)AS so3
					, SUM(oms4)AS so4, SUM(oms5)AS so5, SUM(oms6)AS so6
					, SUM(oms7)AS so7, SUM(oms8)AS so8, SUM(oms9)AS so9
					, SUM(oms10)AS so10, SUM(oms11)AS so11, SUM(oms12)AS so12
				FROM(
					SELECT so.member_id AS member_id
						, CASE WHEN (MONTH(so.tgl)=MONTH('".$startdate."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (MONTH(so.tgl)=MONTH('".$startdate."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms1
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms2
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms3
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms4
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms5
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms6
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms7
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms8
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms9
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms10
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms11
						, CASE WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms12
					FROM so
					RIGHT JOIN so_d sod ON so.id = sod.so_id
					WHERE -- so.stockiest_id!=0
					-- AND rod.pv > 0
					-- AND YEAR(so.tgl) = 2017
					-- AND 
					so.tgl between '".$startdate."' and '".$enddate."'
					GROUP BY so.id
				)AS so
				GROUP BY member_id
			)AS po_ ON s.id = po_.member_id
			where s.id = '".$member_id."'
			ORDER BY k.region, s.type, s.no_stc
		";
        $q=$this->db->query($q);
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function getTracking2019($member_id,$startdate,$enddate){
        $data = array();
		$q = "
			SELECT s.id, m.nama, s.no_stc, s.created, s.type AS tipe, CONCAT('Region',k.region)AS region
				, IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0) AS oms1, IFNULL(so_.so1,0) AS so1, IFNULL(po_.so1,0) AS po1, CASE WHEN IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0) >= 35000000 AND IFNULL(po_.so1,0) >= 1000000 THEN 1 ELSE 0 END AS Q1
				, IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0) AS oms2, IFNULL(so_.so2,0) AS so2, IFNULL(po_.so2,0) AS po2, CASE WHEN IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0) >= 35000000 AND IFNULL(po_.so2,0) >= 1000000 THEN 1 ELSE 0 END AS Q2
				, IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0) AS oms3, IFNULL(so_.so3,0) AS so3, IFNULL(po_.so3,0) AS po3, CASE WHEN IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0) >= 35000000 AND IFNULL(po_.so3,0) >= 1000000 THEN 1 ELSE 0 END AS Q3
				, IFNULL(ro.ro4,0)+IFNULL(pjm.pjm4,0) - IFNULL(rtr.rtr4,0) AS oms4, IFNULL(so_.so4,0) AS so4, IFNULL(po_.so4,0) AS po4, CASE WHEN IFNULL(ro.ro4,0)+IFNULL(pjm.pjm4,0) - IFNULL(rtr.rtr4,0) >= 35000000 AND IFNULL(po_.so4,0) >= 1000000 THEN 1 ELSE 0 END AS Q4
				, IFNULL(ro.ro5,0)+IFNULL(pjm.pjm5,0) - IFNULL(rtr.rtr5,0) AS oms5, IFNULL(so_.so5,0) AS so5, IFNULL(po_.so5,0) AS po5, CASE WHEN IFNULL(ro.ro5,0)+IFNULL(pjm.pjm5,0) - IFNULL(rtr.rtr5,0) >= 35000000 AND IFNULL(po_.so5,0) >= 1000000 THEN 1 ELSE 0 END AS Q5
				, IFNULL(ro.ro6,0)+IFNULL(pjm.pjm6,0) - IFNULL(rtr.rtr6,0) AS oms6, IFNULL(so_.so6,0) AS so6, IFNULL(po_.so6,0) AS po6, CASE WHEN IFNULL(ro.ro6,0)+IFNULL(pjm.pjm6,0) - IFNULL(rtr.rtr6,0) >= 35000000 AND IFNULL(po_.so6,0) >= 1000000 THEN 1 ELSE 0 END AS Q6
				, IFNULL(ro.ro7,0)+IFNULL(pjm.pjm7,0) - IFNULL(rtr.rtr7,0) AS oms7, IFNULL(so_.so7,0) AS so7, IFNULL(po_.so7,0) AS po7, CASE WHEN IFNULL(ro.ro7,0)+IFNULL(pjm.pjm7,0) - IFNULL(rtr.rtr7,0) >= 35000000 AND IFNULL(po_.so7,0) >= 1000000 THEN 1 ELSE 0 END AS Q7
				, IFNULL(ro.ro8,0)+IFNULL(pjm.pjm8,0) - IFNULL(rtr.rtr8,0) AS oms8, IFNULL(so_.so8,0) AS so8, IFNULL(po_.so8,0) AS po8, CASE WHEN IFNULL(ro.ro8,0)+IFNULL(pjm.pjm8,0) - IFNULL(rtr.rtr8,0) >= 35000000 AND IFNULL(po_.so8,0) >= 1000000 THEN 1 ELSE 0 END AS Q8
				, IFNULL(ro.ro9,0)+IFNULL(pjm.pjm9,0) - IFNULL(rtr.rtr9,0) AS oms9, IFNULL(so_.so9,0) AS so9, IFNULL(po_.so9,0) AS po9, CASE WHEN IFNULL(ro.ro9,0)+IFNULL(pjm.pjm9,0) - IFNULL(rtr.rtr9,0) >= 35000000 AND IFNULL(po_.so9,0) >= 1000000 THEN 1 ELSE 0 END AS Q9
				, IFNULL(ro.ro10,0)+IFNULL(pjm.pjm10,0) - IFNULL(rtr.rtr10,0) AS oms10, IFNULL(so_.so10,0) AS so10, IFNULL(po_.so10,0) AS po10, CASE WHEN IFNULL(ro.ro10,0)+IFNULL(pjm.pjm10,0) - IFNULL(rtr.rtr10,0) >= 35000000 AND IFNULL(po_.so10,0) >= 1000000 THEN 1 ELSE 0 END AS Q10
				, IFNULL(ro.ro11,0)+IFNULL(pjm.pjm11,0) - IFNULL(rtr.rtr11,0) AS oms11, IFNULL(so_.so11,0) AS so11, IFNULL(po_.so11,0) AS po11, CASE WHEN IFNULL(ro.ro11,0)+IFNULL(pjm.pjm11,0) - IFNULL(rtr.rtr11,0) >= 35000000 AND IFNULL(po_.so11,0) >= 1000000 THEN 1 ELSE 0 END AS Q11
				, IFNULL(ro.ro12,0)+IFNULL(pjm.pjm12,0) - IFNULL(rtr.rtr12,0) AS oms12, IFNULL(so_.so12,0) AS so12, IFNULL(po_.so12,0) AS po12, CASE WHEN IFNULL(ro.ro12,0)+IFNULL(pjm.pjm12,0) - IFNULL(rtr.rtr12,0) >= 35000000 AND IFNULL(po_.so12,0) >= 1000000 THEN 1 ELSE 0 END AS Q12
				, IFNULL(ro.ro13,0)+IFNULL(pjm.pjm13,0) - IFNULL(rtr.rtr13,0) AS oms13, IFNULL(so_.so13,0) AS so13, IFNULL(po_.so13,0) AS po13, CASE WHEN IFNULL(ro.ro13,0)+IFNULL(pjm.pjm13,0) - IFNULL(rtr.rtr13,0) >= 35000000 AND IFNULL(po_.so13,0) >= 1000000 THEN 1 ELSE 0 END AS Q13
				, IFNULL(ro.ro14,0)+IFNULL(pjm.pjm14,0) - IFNULL(rtr.rtr14,0) AS oms14, IFNULL(so_.so14,0) AS so14, IFNULL(po_.so14,0) AS po14, CASE WHEN IFNULL(ro.ro14,0)+IFNULL(pjm.pjm14,0) - IFNULL(rtr.rtr14,0) >= 35000000 AND IFNULL(po_.so14,0) >= 1000000 THEN 1 ELSE 0 END AS Q14
				, IFNULL(ro.ro15,0)+IFNULL(pjm.pjm15,0) - IFNULL(rtr.rtr15,0) AS oms15, IFNULL(so_.so15,0) AS so15, IFNULL(po_.so15,0) AS po15, CASE WHEN IFNULL(ro.ro15,0)+IFNULL(pjm.pjm15,0) - IFNULL(rtr.rtr15,0) >= 35000000 AND IFNULL(po_.so15,0) >= 1000000 THEN 1 ELSE 0 END AS Q15
				, IFNULL(ro.ro16,0)+IFNULL(pjm.pjm16,0) - IFNULL(rtr.rtr16,0) AS oms16, IFNULL(so_.so16,0) AS so16, IFNULL(po_.so16,0) AS po16, CASE WHEN IFNULL(ro.ro16,0)+IFNULL(pjm.pjm16,0) - IFNULL(rtr.rtr16,0) >= 35000000 AND IFNULL(po_.so16,0) >= 1000000 THEN 1 ELSE 0 END AS Q16
				, (IFNULL(ro.ro1,0)+IFNULL(pjm.pjm1,0) - IFNULL(rtr.rtr1,0))+
				  (IFNULL(ro.ro2,0)+IFNULL(pjm.pjm2,0) - IFNULL(rtr.rtr2,0))+
				  (IFNULL(ro.ro3,0)+IFNULL(pjm.pjm3,0) - IFNULL(rtr.rtr3,0))+
				  (IFNULL(ro.ro4,0)+IFNULL(pjm.pjm4,0) - IFNULL(rtr.rtr4,0))+
				  (IFNULL(ro.ro5,0)+IFNULL(pjm.pjm5,0) - IFNULL(rtr.rtr5,0))+
				  (IFNULL(ro.ro6,0)+IFNULL(pjm.pjm6,0) - IFNULL(rtr.rtr6,0))+
				  (IFNULL(ro.ro7,0)+IFNULL(pjm.pjm7,0) - IFNULL(rtr.rtr7,0))+
				  (IFNULL(ro.ro8,0)+IFNULL(pjm.pjm8,0) - IFNULL(rtr.rtr8,0))+
				  (IFNULL(ro.ro9,0)+IFNULL(pjm.pjm9,0) - IFNULL(rtr.rtr9,0))+
				  (IFNULL(ro.ro10,0)+IFNULL(pjm.pjm10,0) - IFNULL(rtr.rtr10,0))+
				  (IFNULL(ro.ro11,0)+IFNULL(pjm.pjm11,0) - IFNULL(rtr.rtr11,0))+
				  (IFNULL(ro.ro12,0)+IFNULL(pjm.pjm12,0) - IFNULL(rtr.rtr12,0))+
				  (IFNULL(ro.ro13,0)+IFNULL(pjm.pjm13,0) - IFNULL(rtr.rtr13,0))+
				  (IFNULL(ro.ro14,0)+IFNULL(pjm.pjm14,0) - IFNULL(rtr.rtr14,0))+
				  (IFNULL(ro.ro15,0)+IFNULL(pjm.pjm15,0) - IFNULL(rtr.rtr15,0))+
				  (IFNULL(ro.ro16,0)+IFNULL(pjm.pjm16,0) - IFNULL(rtr.rtr16,0))
				  AS oms
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_id = k.id
			LEFT JOIN member m ON s.id = m.id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS ro1, SUM(oms2)AS ro2, SUM(oms3)AS ro3
					, SUM(oms4)AS ro4, SUM(oms5)AS ro5, SUM(oms6)AS ro6
					, SUM(oms7)AS ro7, SUM(oms8)AS ro8, SUM(oms9)AS ro9
					, SUM(oms10)AS ro10, SUM(oms11)AS ro11, SUM(oms12)AS ro12
					, SUM(oms13)AS ro13, SUM(oms14)AS ro14, SUM(oms15)AS ro15
					, SUM(oms16)AS ro16
				FROM(
					SELECT member_id
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".$startdate."') AND MONTH(ro.`date`)=MONTH('".$startdate."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".$startdate."') AND MONTH(ro.`date`)=MONTH('".$startdate."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms1
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms2
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms3
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms4
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms5
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms6
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms7
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms8
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms9
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms10
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms11
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms12
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms13
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms14
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms15
						, CASE WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND (ro.`date`) <= '2015-02-28') THEN totalharga WHEN (YEAR(ro.`date`)=YEAR('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND MONTH(ro.`date`)=MONTH('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND (ro.`date`) > '2015-02-28') THEN SUM(rod.jmlharga) END AS oms16
					FROM ro
					RIGHT JOIN ro_d rod ON ro.id = rod.ro_id
					WHERE ro.stockiest_id=0
					-- AND rod.pv > 0
					AND (
						CASE WHEN ro.`date` > '2015-02-28' THEN rod.pv > 0 
						ELSE rod.pv > 0 
						END
					)
					-- AND YEAR(ro.date) = 2017
					AND ro.date between '".$startdate."' and '".$enddate."'
					GROUP BY ro.id
				)AS ro_
				GROUP BY member_id
			)AS ro ON s.id = ro.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS pjm1, SUM(oms2)AS pjm2, SUM(oms3)AS pjm3
					, SUM(oms4)AS pjm4, SUM(oms5)AS pjm5, SUM(oms6)AS pjm6
					, SUM(oms7)AS pjm7, SUM(oms8)AS pjm8, SUM(oms9)AS pjm9
					, SUM(oms10)AS pjm10, SUM(oms11)AS pjm11, SUM(oms12)AS pjm12
					, SUM(oms13)AS pjm13, SUM(oms14)AS pjm14, SUM(oms15)AS pjm15
					, SUM(oms16)AS pjm16
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".$startdate."') AND MONTH(pjm.tgl)=MONTH('".$startdate."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".$startdate."') AND MONTH(pjm.tgl)=MONTH('".$startdate."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms2
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms3
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms4
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms5
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms6
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms7
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms8
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms9
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms10
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms11
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms12
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms13
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms14
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms15
						, CASE WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND (pjm.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(pjm.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND MONTH(pjm.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND (pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms16
					   FROM pinjaman_titipan pjm
					-- WHERE YEAR(pjm.tgl) = 2017
					WHERE pjm.tgl between '".$startdate."' and '".$enddate."'
				)AS pjm
				GROUP BY member_id
			)AS pjm ON s.id = pjm.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS rtr1, SUM(oms2)AS rtr2, SUM(oms3)AS rtr3
					, SUM(oms4)AS rtr4, SUM(oms5)AS rtr5, SUM(oms6)AS rtr6
					, SUM(oms7)AS rtr7, SUM(oms8)AS rtr8, SUM(oms9)AS rtr9
					, SUM(oms10)AS rtr10, SUM(oms11)AS rtr11, SUM(oms12)AS rtr12
					, SUM(oms13)AS rtr13, SUM(oms14)AS rtr14, SUM(oms15)AS rtr15
					, SUM(oms16)AS rtr16
				FROM(
					SELECT stockiest_id AS member_id
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".$startdate."') AND MONTH(rtr.tgl)=MONTH('".$startdate."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".$startdate."') AND MONTH(rtr.tgl)=MONTH('".$startdate."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms1
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms2
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms3
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms4
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms5
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms6
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms7
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms8
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms9
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms10
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms11
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms12
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms13
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms14
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms15
						, CASE WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND (rtr.tgl) <= '2015-02-28') THEN totalharga WHEN (YEAR(rtr.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND MONTH(rtr.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND (rtr.tgl) > '2015-02-28') THEN totalharga END AS oms16
					FROM retur_titipan rtr
					-- WHERE YEAR(rtr.tgl) = 2017
					WHERE rtr.tgl between '".$startdate."' and '".$enddate."'
				)AS rtr
				GROUP BY member_id
			)AS rtr ON s.id = rtr.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS so1, SUM(oms2)AS so2, SUM(oms3)AS so3
					, SUM(oms4)AS so4, SUM(oms5)AS so5, SUM(oms6)AS so6
					, SUM(oms7)AS so7, SUM(oms8)AS so8, SUM(oms9)AS so9
					, SUM(oms10)AS so10, SUM(oms11)AS so11, SUM(oms12)AS so12
					, SUM(oms13)AS so13, SUM(oms14)AS so14, SUM(oms15)AS so15
					, SUM(oms16)AS so16
				FROM(
					SELECT so.stockiest_id AS member_id
						, CASE WHEN (YEAR(so.tgl)=YEAR('".$startdate."') AND MONTH(so.tgl)=MONTH('".$startdate."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".$startdate."') AND MONTH(so.tgl)=MONTH('".$startdate."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms1
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms2
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms3
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms4
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms5
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms6
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms7
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms8
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms9
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms10
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms11
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms12
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms13
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms14
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms15
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlharga) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlharga) END AS oms16
					FROM so
					RIGHT JOIN so_d sod ON so.id = sod.so_id
					WHERE 
					so.stockiest_id != 0
					AND sod.pv > 0
					-- AND YEAR(so.tgl) = 2017
					-- AND 
					AND so.tgl between '".$startdate."' and '".$enddate."'
					GROUP BY so.id
				)AS so
				GROUP BY member_id
			)AS so_ ON s.id = so_.member_id
			LEFT JOIN(
				SELECT member_id
					, SUM(oms1)AS so1, SUM(oms2)AS so2, SUM(oms3)AS so3
					, SUM(oms4)AS so4, SUM(oms5)AS so5, SUM(oms6)AS so6
					, SUM(oms7)AS so7, SUM(oms8)AS so8, SUM(oms9)AS so9
					, SUM(oms10)AS so10, SUM(oms11)AS so11, SUM(oms12)AS so12
					, SUM(oms13)AS so13, SUM(oms14)AS so14, SUM(oms15)AS so15
					, SUM(oms16)AS so16
				FROM(
					SELECT so.member_id AS member_id
						, CASE WHEN (YEAR(so.tgl)=YEAR('".$startdate."') AND MONTH(so.tgl)=MONTH('".$startdate."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".$startdate."') AND MONTH(so.tgl)=MONTH('".$startdate."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms1
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms2
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms3
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms4
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms5
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms6
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms7
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms8
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms9
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms10
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms11
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms12
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+12 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms13
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+13 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms14
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+14 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms15
						, CASE WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND (so.tgl) <= '2015-02-28') THEN SUM(sod.jmlpv) WHEN (YEAR(so.tgl)=YEAR('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND MONTH(so.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+15 Month"))."') AND (so.tgl) > '2015-02-28') THEN SUM(sod.jmlpv) END AS oms16
					FROM so
					RIGHT JOIN so_d sod ON so.id = sod.so_id
					WHERE -- so.stockiest_id!=0
					-- AND rod.pv > 0
					-- AND YEAR(so.tgl) = 2017
					-- AND 
					so.tgl between '".$startdate."' and '".$enddate."'
					GROUP BY so.id
				)AS so
				GROUP BY member_id
			)AS po_ ON s.id = po_.member_id
			where s.id = '".$member_id."'
			ORDER BY k.region, s.type, s.no_stc
		";
        $q=$this->db->query($q);
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }


}
?>