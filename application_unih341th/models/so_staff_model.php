<?php
class So_staff_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    public function check_stock($id,$itemid,$whsid)
    {
        $data=array();
        $q = $this->db->select("sum(d.qty) as qty,s.qty as stock,i.name")
	    ->from('so_temp_d d')
	    ->join('stock s','d.item_id=s.item_id','left')
	    ->join('item i','d.item_id=i.id','left')
	    ->where('d.so_id',$id)
	    ->where('d.item_id',$itemid)
	    ->where('s.warehouse_id',$whsid)
	    ->get();
	    
	//echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
    
    public function search_so_staff($keywords=0,$num,$offset){
        $data = array();
        /* Modified by Boby 2011-02-24 */
		$select = "a.id,sf.nik,st.name,date_format(a.tgl,'%d-%b-%Y')as tgl,a.kit,a.member_id,format(a.totalpv,0)as ftotalpv,m.nama,remark";
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND a.id LIKE '%$keywords%'";
			$select .= ",format(a.totalharga_,0)as ftotalharga";
        }else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.stockiest_id = '0' and a.member_id = 'STAFF' and a.warehouse_id = '$whsid' and (st.name LIKE '%$keywords%' or a.id LIKE '%$keywords%' or s.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')";
            else $where = "a.member_id = 'STAFF' and ( st.name LIKE '%$keywords%' or a.id LIKE '%$keywords%' OR m.nama LIKE '%$keywords%' )";
			$select .= ",format(a.totalharga,0)as ftotalharga";
        }
		// $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.kit,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama,remark",false);
		$this->db->select($select,false);
		/* End modified by Boby 2011-02-24 */
		
        $this->db->from('so_staff sf');
        $this->db->join('so a','sf.so_id=a.id','left');
	$this->db->join('member m','a.member_id=m.id','left');
	$this->db->join('staff st','sf.nik=st.nik','left');
	$this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function count_so_staff($keywords=0){
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND a.id LIKE '%$keywords%'";
        }
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.stockiest_id = '0' and a.member_id = 'STAFF' and a.warehouse_id = '$whsid' and (st.name LIKE '%$keywords%' or a.id LIKE '%$keywords%' or s.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')";
            else $where = "a.member_id = 'STAFF' and (st.name LIKE '%$keywords%' or a.id LIKE '%$keywords%' or m.nama LIKE '%$keywords%' )";
        }
        
        $this->db->from('so_staff sf');
        $this->db->join('so a','sf.so_id=a.id','left');
	$this->db->join('member m','a.member_id=m.id','left');
	$this->db->join('staff st','sf.nik=st.nik','left');
        $this->db->where($where);
        //$q=$this->db->get();
        //echo $this->db->last_query();
        return $this->db->count_all_results();
    }

    public function add_so_staff(){
        //$member_id = $this->input->post('member_id');
	$member_id = "STAFF";
        $totalharga = str_replace(".","",$this->input->post('total'));
        //$totalpv = str_replace(".","",$this->input->post('totalpv'));
	//$totalbv = str_replace(".","",$this->input->post('totalbv'));
        $totalbv = 0;
	$totalpv = 0;
	
        $cash = str_replace(".","",$this->input->post('tunai'));
        $debit = str_replace(".","",$this->input->post('debit'));
        $credit = str_replace(".","",$this->input->post('credit'));
        $totalbayar = str_replace(".","",$this->input->post('totalbayar'));
        
        $empid = $this->session->userdata('user');
        $whsid = $this->session->userdata('whsid');
        $groupid = $this->session->userdata('group_id');
        $tgl = $this->input->post('date');
        
        $data = array(
            'member_id' => $member_id,
            'stockiest_id'=>'0',
            'tgl' => $tgl,
            'totalharga' => $totalharga,
            'totalharga_' => $totalharga, // updated by Boby 20140128
            'totalpv' => $totalpv,
	    	'totalbv' => $totalbv,
            'kit'=>'N',
            'warehouse_id' => $whsid,
			'statuspu' => $this->input->post('pu'),
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('so',$data);
        
        $id = $this->db->insert_id();
        
	$key=0;
        while($key < count($_POST['counter'])){
            $qty = str_replace(".","",$this->input->post('qty'.$key));
	    $itemid = $this->input->post('itemcode'.$key);
	    
            if($itemid and $qty > 0){
                $data=array(
                    'so_id' => $id,
                    'item_id' => $itemid,
                    'qty' => $qty,
                    'harga' => str_replace(".","",$this->input->post('price'.$key)),
                    'pv' => 0,
                    'jmlharga' =>str_replace(".","",$this->input->post('subtotal'.$key)),
                    'jmlpv' => 0,
                    'warehouse_id' => 1
                 );
                 $this->db->insert('so_d',$data);
            }
            $key++;
        }
        
	$this->db->insert('so_staff',array('nik'=>$this->input->post('member_id'),'so_id'=>$id));
	
        if($totalbayar > 0){
            $data=array(
                'member_id' => $member_id,
                'tunai' => $cash,
                'debit_card' => $debit,
                'credit_card' => $credit,
                'total' => $totalbayar,
                'total_approved' => $totalbayar,
                'remark_fin' => 'SO STAFF',
                'approved' => 'approved',
                'tgl_approved' => date('Y-m-d H:i:s',now()),
                'approvedby' => $this->session->userdata('user'),
                'flag' => 'mem',
                'event_id' => 'DP1',
                'created' => date('Y-m-d H:i:s',now()),
                'createdby' => $empid
            );
            $this->db->insert('deposit',$data);
        
            $id_deposit = $this->db->insert_id();
            $this->db->query("call sp_deposit('$id_deposit','$member_id','$totalbayar','mem','$empid')");
        }
	
        $this->db->query("call so_staff_procedure('$id','$tgl','$member_id','0','$totalharga','0','0','$whsid','$groupid','$empid')");
    }
    
    public function get_so($id=0){
        $data = array();
        
		/* Modified by Boby 2011-02-24 */
		// $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.kit,date_format(a.created,'%d-%b-%Y')as created,a.createdby,s.no_stc,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
		$select = "	
			f.nik,a.id,f.name,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,format(a.totalpv,0)as ftotalpv,a.remark,a.kit,date_format(a.created,'%d-%b-%Y')as created,a.createdby";
		if($this->session->userdata('group_id')>100){
			$select .= ",format(a.totalharga_,0)as ftotalharga";
		}else{
			$select .= ",format(a.totalharga,0)as ftotalharga";
		}
        $this->db->select($select,false);
		/* End modified by Boby 2011-02-24 */
		
        $this->db->from('so a');
        $this->db->join('so_staff s','a.id=s.so_id','left');
        $this->db->join('staff f','s.nik=f.nik','left');
        if($this->session->userdata('group_id')>100){
			$this->db->where('a.stockiest_id',$this->session->userdata('userid'));
	}else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$this->db->where('a.warehouse_id',$whsid); 
        }
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function get_so_detail($id=0){
        $data = array();
        
		/* Modified by Boby 2011-02-24 */
		// $this->db->select("d.item_id,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.pv,0)as fpv,format(sum(d.qty*d.harga),0)as fsubtotal,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name",false);
		$select = "	d.item_id,format(d.qty,0)as fqty,format(sum(d.jmlpv),0)as fsubtotalpv,format(d.pv,0)as fpv,a.name";
		if($this->session->userdata('group_id')>100){
			$select .="	,format(d.harga_,0)as fharga,format(sum(d.jmlharga_),0)as fsubtotal";
		}else{
			$select .="	,format(d.harga,0)as fharga,format(sum(d.jmlharga),0)as fsubtotal";
		}
        $this->db->select($select,false);
		/* End modified by Boby 2011-02-24 */
		
		$this->db->from('so_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.so_id',$id);
        $this->db->group_by('d.id');
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getEwallet($member_id){
        $q = $this->db->select("ewallet,format(ewallet,0)as fewallet",false)
            ->from('member')
            ->where('id',$member_id)
            ->get();
        return $var = ($q->num_rows()>0)? $q->row() : false;
    }

    public function get_blocked(){
        $q=$this->db->select('tglrunning',false)
            ->from('blocked')
            ->where('blockid',1)
            ->limit(1)
            ->get();
        if($q->num_rows()>0){
            $data=$q->row();
        }$q->free_result();
        return $data;
    }
	
	/* Created by Boby 20131222 */
    public function getNpwp($id=0){
        $data = array();
        $select = "	
			n.nama,n.alamat
			,  IFNULL(CONCAT(LEFT(n.npwp,2),'.',RIGHT(LEFT(n.npwp,5),3),'.',RIGHT(LEFT(n.npwp,8),3),'.',RIGHT(LEFT(n.npwp,9),1),'-',LEFT(RIGHT(n.npwp,6),3),'.',LEFT(RIGHT(n.npwp,3),3)),'00.000.000.0-000.000')
			AS npwp
		";
        $this->db->select($select,false);
		$this->db->from('so');
        $this->db->join('npwp n','so.member_id = n.member_id','left');
        $this->db->where('so.id',$id);
        $this->db->where('n.created_date <= ','so.tgl');
        $this->db->order_by('n.id', 'DESC');
        $this->db->limit(1);
		
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }else{
			$data['nama']='';
			$data['alamat']='';
			$data['npwp']='00.000.000.0-000.000';
		}
        $q->free_result();
        return $data;
    }
	/* End created by Boby 20131222 */ 
}?>