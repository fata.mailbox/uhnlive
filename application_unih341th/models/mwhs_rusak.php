<?php
class MWhs_rusak extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Mutasi Stcock antar Warehouse
    |--------------------------------------------------------------------------
    |
    | Annisa
    |
    */
    public function searchRusak($keywords=0,$num,$offset){
        $data = array();
        $wrsk = $this->session->userdata('wrsk');
        
        if($wrsk > 1){
            $where = "a.id_whr = '$wrsk' and (a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%')";
        }else{
            if(!$this->session->userdata('keywords_wrsk')){
                $where = "( a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%' )";
            }else{
                    if($this->session->userdata('keywords_wrsk')=='all')
                        $where = "( a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%' )";
                    else
                        $where = "( a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%' ) and a.id_whr = ".$this->session->userdata('keywords_wrsk');
            }
        }
        $this->db->select("a.id,date_format(a.created,'%d-%b-%Y')as date,a.remark,a.createdby,a.approved,b.nama,a.approveddate as appdate,a.approvedby as approvedby",false);
        $this->db->from('transaksi_rusak a');
        $this->db->join('master_rusak b','a.id_whr=b.id','left');
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countRusak($keywords=0){
        $wrsk = $this->session->userdata('wrsk');
        
        if($wrsk > 1){
            $where = "a.id_whr = '$wrsk' and (a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%')";
        }else{
            if(!$this->session->userdata('keywords_wrsk')){
                $where = "( a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%' )";
            }else{
                    if($this->session->userdata('keywords_wrsk')=='all')
                        $where = "( a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%' )";
                    else
                        $where = "( a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%' ) and a.id_whr = ".$this->session->userdata('keywords_wrsk');
            }
        }
        $this->db->from('transaksi_rusak a');
        $this->db->join('master_rusak b','a.id_whr=b.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }

    public function addRusak(){
        $id_whr=$this->input->post('id_whr');
        $empid = $this->session->userdata('user');
        
        $totalprice = str_replace(".","",$this->input->post('totalprice'));
        
        $data = array(
            'id_whr' => $id_whr,
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $empid,
            'total' => $totalprice
            );
       $this->db->insert('transaksi_rusak',$data);
       
       $id = $this->db->insert_id();
         
        $qty0 = str_replace(".","",$this->input->post('qty0'));
        $price0 = str_replace(".","",$this->input->post('price0'));
        $tprice0 = str_replace(".","",$this->input->post('tprice0'));
        $itemcode0 = $this->input->post('itemcode0');
        if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'transaksi_id' => $id,
                'item_id' => $itemcode0,
                'qty' => $qty0,
                'harga' => $price0,
                'jmlharga' => $tprice0
            );
            
            $this->db->insert('transaksi_rusak_d',$data);
            //$this->db->query("call sp_update_stock('$id_whr','$itemcode0','$qty0','$empid')");
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
        $price1 = str_replace(".","",$this->input->post('price1'));
        $tprice1 = str_replace(".","",$this->input->post('tprice1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'transaksi_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => $price1,
                'jmlharga' => $tprice1
            );
            
            $this->db->insert('transaksi_rusak_d',$data);
            //$this->db->query("call sp_update_stock('$id_whr','$itemcode1','$qty1','$empid')");
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       $price2 = str_replace(".","",$this->input->post('price2'));
       $tprice2 = str_replace(".","",$this->input->post('tprice2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'transaksi_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => $price2,
                'jmlharga' => $tprice2
            );
            
            $this->db->insert('transaksi_rusak_d',$data);
            //$this->db->query("call sp_update_stock('$id_whr','$itemcode2','$qty2','$empid')");
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       $price3 = str_replace(".","",$this->input->post('price3'));
       $tprice3 = str_replace(".","",$this->input->post('tprice3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'transaksi_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => $price3,
                'jmlharga' => $tprice3
            );
            
            $this->db->insert('transaksi_rusak_d',$data);
            //$this->db->query("call sp_update_stock('$id_whr','$itemcode3','$qty3','$empid')");
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
       $price4 = str_replace(".","",$this->input->post('price4'));
       $tprice4 = str_replace(".","",$this->input->post('tprice4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'transaksi_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => $price4,
                'jmlharga' => $tprice4
            );
            
            $this->db->insert('transaksi_rusak_d',$data);
            //$this->db->query("call sp_update_stock('$id_whr','$itemcode4','$qty4','$empid')");
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
       $price5 = str_replace(".","",$this->input->post('price5'));
       $tprice5 = str_replace(".","",$this->input->post('tprice5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
            $data=array(
                'transaksi_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => $price5,
                'jmlharga' => $tprice5
            );
            
            $this->db->insert('transaksi_rusak_d',$data);
            //$this->db->query("call sp_update_stock('$id_whr','$itemcode5','$qty5','$empid')");
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
       $price6 = str_replace(".","",$this->input->post('price6'));
       $tprice6 = str_replace(".","",$this->input->post('tprice6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
            $data=array(
                'transaksi_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => $price6,
                'jmlharga' => $tprice6
            );
            
            $this->db->insert('transaksi_rusak_d',$data);
            //$this->db->query("call sp_update_stock('$id_whr','$itemcode6','$qty6','$empid')");
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
       $price7 = str_replace(".","",$this->input->post('price7'));
       $tprice7 = str_replace(".","",$this->input->post('tprice7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
            $data=array(
                'transaksi_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => $price7,
                'jmlharga' => $tprice7
            );
            
            $this->db->insert('transaksi_rusak_d',$data);
            //$this->db->query("call sp_update_stock('$id_whr','$itemcode7','$qty7','$empid')");
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
       $price8 = str_replace(".","",$this->input->post('price8'));
       $tprice8 = str_replace(".","",$this->input->post('tprice8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
            $data=array(
                'transaksi_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => $price8,
                'jmlharga' => $tprice8
            );
            
            $this->db->insert('transaksi_rusak_d',$data);
            //$this->db->query("call sp_update_stock('$id','$itemcode8','$qty8','$empid')");
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
       $price9 = str_replace(".","",$this->input->post('price9'));
       $tprice9 = str_replace(".","",$this->input->post('tprice9'));
       $itemcode9 = $this->input->post('itemcode9');
       if($this->input->post('itemcode9') and $qty9 > 0){
            $data=array(
                'transaksi_id' => $id,
                'item_id' => $itemcode9,
                'qty' => $qty9,
                'harga' => $price9,
                'jmlharga' => $tprice9
            );
            
            $this->db->insert('transaksi_rusak_d',$data);
            //$this->db->query("call sp_update_stock('$id','$itemcode9','$qty9','$empid')");
       }      
        $this->db->query("call sp_rusak_intransit('$id','$id_whr','$empid')");
    }

    public function getDropDownWhs(){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function getRusakId($id){
        $data = array();
        $this->db->select("tr.id,date_format(tr.created,'%d-%b-%Y')as date,tr.remark,tr.createdby,tr.approved,b.nama,tr.approveddate as appdate,tr.approvedby as approvedby,format(tr.total,0) as total",false);
        $this->db->from('transaksi_rusak tr');
        $this->db->join('master_rusak b','tr.id_whr=b.id','left');
        $this->db->where('tr.id',$id);

       $q=$this->db->get();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
        
    }
    public function getRusakDetail($id=0){
        $data = array();
        $q=$this->db->select("d.id,d.transaksi_id,d.item_id,d.qty,format(d.harga,0) as harga,format(d.jmlharga,0) as jmlharga,a.name as barang",false)
            ->from('transaksi_rusak_d d')
            ->join('item a','d.item_id=a.id','left')
            ->where('d.transaksi_id',$id)
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }

     
    public function getMrusak(){
       $data = array();
        $q=$this->db->select("id,nama",false)
            ->from('master_rusak')
            ->order_by('id','asc')
            ->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[$row['id']] = $row['nama'];
            }
        }
        $q->free_result();
        return $data;
    }

    public function getDropDownWhsAll($all){
        $data = array();
        $q = $this->db->get('master_rusak');
        if($q->num_rows >0){
            if($all == 'all')$data['all']='All Cabang';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['nama'];    
            }
        }
        $q->free_result();
        return $data;
    }

    public function WRApproved(){
        if($this->input->post('p_id')){
            $row = array();
            
            $empid = $this->session->userdata('user');
            $idlist = implode(",",array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";
            
            $option = $where. " and approved = 'N'";
            $row = $this->_countWRApproved($option);
            $remarkapp=$this->db->escape_str($this->input->post('remark'));
            
            if($row){
                $data = array(
                    'approved'=> 'Y',
                    'remarkapp'=>$remarkapp,
                    'approvedby'=>$this->session->userdata('user'),
                    'approveddate' => date('Y-m-d H:i:s', now())
                );
                $this->db->update('transaksi_rusak',$data,$option);
                
                $this->session->set_flashdata('message','Approved successfully');
            }else{
                $this->session->set_flashdata('message','Nothing  approved!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing approved!');
        }
    }
    protected function _countWRApproved($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('transaksi_rusak');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function adjusmentApproved(){
        if($this->input->post('p_id')){
            $row = array();
            
            $empid = $this->session->userdata('user');
            $idlist = implode(",",array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";
            
            $option = $where. " and status = 'N'";
            $row = $this->_countAdjustmentApproved($option);
            $remarkapp=$this->db->escape_str($this->input->post('remark'));
            
            if($row){
                $data = array(
                    'status'=> 'Y',
                    'remarkapp'=>$remarkapp,
                    'approvedby'=>$this->session->userdata('user'),
                    'approveddate' => date('Y-m-d H:i:s', now())
                );
                $this->db->update('adjusment_rusak',$data,$option);
                
                $this->session->set_flashdata('message','Approved successfully');
            }else{
                $this->session->set_flashdata('message','Nothing  approved!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing approved!');
        }
    }
    protected function _countAdjustmentApproved($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('adjusment_rusak');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function addAdjustmentRusak(){
        $flag = $this->input->post('flag');
        $id_whr = $this->input->post('id_whr');
        if ($flag = 'Minus') {
            $noted = $this->input->post('noted');
        }else{
            $noted = 'Barang Masuk Stock';
        }
        
       $data = array(
            'date' => date('Y-m-d',now()),
            'id_whr' => $id_whr,
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'flag' => $flag,
            'note' => $noted,
            'status' => 'N',
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $this->session->userdata('user'),
            'approvedby' => '-'
            );
       $this->db->insert('adjusment_rusak',$data);
       
       $id = $this->db->insert_id();
       
       $qty0 = str_replace(".","",$this->input->post('qty0'));
       if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'id_adj_rusak' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0
            );
            
            $this->db->insert('adjusment_rusak_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'id_adj_rusak' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1
            );
            
            $this->db->insert('adjusment_rusak_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'id_adj_rusak' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2
            );
            
            $this->db->insert('adjusment_rusak_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'id_adj_rusak' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3
            );
            
            $this->db->insert('adjusment_rusak_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
       if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'id_adj_rusak' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4
            );
            
            $this->db->insert('adjusment_rusak_d',$data);
       }
       $empid = $this->session->userdata('user');
       $this->db->query("call sp_adjustment_rusak('$id','$id_whr','$flag','$noted','$empid')");
    }
    
    public function searchAdjRusak($keywords=0,$num,$offset){
        $data = array();
        $wrsk = $this->session->userdata('wrsk');
        
        if($wrsk > 1){
            $where = "a.id_whr = '$wrsk' and (a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%')";
        }else{
            if(!$this->session->userdata('keywords_wrsk')){
                $where = "( a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%' )";
            }else{
                    if($this->session->userdata('keywords_wrsk')=='all')
                        $where = "( a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%' )";
                    else
                        $where = "( a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%' ) and a.id_whr = ".$this->session->userdata('keywords_wrsk');
            }
        }
        $this->db->select("a.id,date_format(a.created,'%d-%b-%Y')as date,a.remark,a.createdby,a.flag,b.nama,a.approveddate as appdate,a.approvedby as approvedby, a.status",false);
        $this->db->from('adjusment_rusak a');
        $this->db->join('master_rusak b','a.id_whr=b.id','left');
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countAdjRusak($keywords=0){
        $wrsk = $this->session->userdata('wrsk');
        
        if($wrsk > 1){
            $where = "a.id_whr = '$wrsk' and (a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%')";
        }else{
            if(!$this->session->userdata('keywords_wrsk')){
                $where = "( a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%' )";
            }else{
                    if($this->session->userdata('keywords_wrsk')=='all')
                        $where = "( a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%' )";
                    else
                        $where = "( a.id_whr LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.nama LIKE '$keywords%' ) and a.id_whr = ".$this->session->userdata('keywords_wrsk');
            }
        }
        $this->db->from('adjusment_rusak a');
        $this->db->join('master_rusak b','a.id_whr=b.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }

}?>