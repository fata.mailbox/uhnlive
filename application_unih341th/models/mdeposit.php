<?php
class MDeposit extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Deposit Confirmation for Stockiest
    |--------------------------------------------------------------------------
    |
    | @created 2009-04-12
    | @author qtakwa@yahoo.com@yahoo.com
    |
    */
    public function searchDeposit($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.verified,a.id,a.flag,a.member_id,m.nama,s.no_stc,format(a.total,0)as ftotal,format(a.total_approved,0)as ftotal_approved,a.remark_fin,a.approved,date_format(a.tgl_approved,'%d-%b-%Y')as tgl_approved
			,date_format(a.created,'%d-%b-%Y') as created_, a.created, CASE WHEN LENGTH(a.bank_id)>1 THEN a.bank_id ELSE '-' END AS bank_id ",false);
        $this->db->from('deposit a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        if($this->session->userdata('group_id')>100){
            if($this->session->userdata('group_id') == 101)$flag = 'mem';
            else $flag = 'stc'; 
            $where = "a.flag = '$flag' and `a`.`member_id` = '".$this->session->userdata('userid')."' AND (s.no_stc LIKE '%$keywords%' OR a.id LIKE '%$keywords%')";
        }else{
            $where = "(s.no_stc LIKE '%$keywords%' OR a.id LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')";
        }
        $this->db->where($where);
       // $this->db->order_by('a.approved','asc');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
       // echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countDeposit($keywords=0){
        $this->db->from('deposit a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        if($this->session->userdata('group_id')>100){
            if($this->session->userdata('group_id') == 101)$flag = 'mem';
            else $flag = 'stc'; 
            $where = "a.flag = '$flag' and `a`.`member_id` = '".$this->session->userdata('userid')."' AND (s.no_stc LIKE '%$keywords%' OR a.id LIKE '%$keywords%')";
        }else{
            $where = "(s.no_stc LIKE '%$keywords%' OR a.id LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')";
        }
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    public function addDeposit(){
        $amount = str_replace(".","",$this->input->post('amount'));
        if($this->session->userdata('group_id') == 101)$flag = 'mem';
        else $flag = 'stc';
        
        $data=array(
            'member_id' => $this->session->userdata('userid'),
            'transfer' => $amount,
            'total' => $amount,
            'bank_id' => $this->input->post('bank_id'),
            'tgl_transfer' => $this->input->post('date'),
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'flag' => $flag,
            'event_id' => 'DP1',
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $this->session->userdata('userid')
        );
            
        $this->db->insert('deposit',$data);
    }
    public function getDeposit($id){
        $data=array();
        $this->db->select("a.id,a.member_id,m.nama,s.no_stc,format(a.transfer,0)as ftransfer,format(a.tunai,0)as ftunai,format(a.debit_card,0)as fdebit_card,format(a.credit_card,0)as fcredit_card
						, a.bank_id
                        , format(a.kembali,0)as fkembali,format(a.total,0)as ftotal,format(a.total_approved,0)as ftotal_approved,a.tgl_transfer,a.remark,a.remark_fin,a.approved,a.tgl_approved,a.approvedby,date_format(a.created,'%d-%b-%Y %H:%i:%s')as created,a.createdby",false);
        $this->db->from('deposit a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        if($this->session->userdata('group_id')>100){
            if($this->session->userdata('group_id') == 101)$flag = 'mem';
            else $flag = 'stc';
            $where = "`a`.`id` = '$id' and `a`.`flag` = '$flag' and `a`.`member_id` = '".$this->session->userdata('userid')."'";
        }else{
            $where = "`a`.`id` = '$id'";
        }
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function getDropDownBank(){
        $data = array();
        $this->db->where('status','Y');
        $q = $this->db->get('bank');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    public function getDepositApproved($id){
        $data=array();
        $this->db->select("a.id,a.flag,a.tgl_transfer,a.member_id,m.nama,s.no_stc,format(a.transfer,0)as ftransfer,format(a.tunai,0)as ftunai,format(a.debit_card,0)as fdebit_card,format(a.credit_card,0)as fcredit_card
							, a.bank_id
							, format(a.kembali,0)as fkembali,a.total,format(a.total,0)as ftotal,a.tgl_transfer,a.remark,a.remark_fin,a.approved,a.tgl_approved,a.approvedby,date_format(a.created,'%d-%b-%Y %H:%i:%s')as created,a.createdby",false);
        $this->db->from('deposit a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->where('a.id',$id);
        $this->db->where('a.approved','pending');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function approvedDeposit(){
        if($this->input->post('status') == 'reject') $total=0;
        else $total = str_replace(".","",$this->input->post('amount'));
        $id = $this->input->post('id');
        $empid =$this->session->userdata('user');
        $member_id =$this->input->post('member_id');
        $flag =$this->input->post('flag');
        
        $data=array(
            'total_approved'=>$total,
            'approved' => $this->input->post('status'),
			'bank_id' => $this->input->post('bank_id'),
            'remark_fin' => $this->db->escape_str($this->input->post('remark')),
            'tgl_approved' => date('Y-m-d H:i:s',now()),
            'approvedby' => $empid,
            'warehouse_id' => $this->session->userdata('whsid')
        );
        $this->db->update('deposit',$data,array('id'=>$id));
        if($total >0)$this->db->query("call sp_deposit('$id','$member_id','$total','$flag','$empid')");
        
    }
    public function addDepositApproved(){
        $member_id=$this->input->post('member_id');
        $bank_id=$this->input->post('bank_id'); // created by Boby 20141010
        
        $total = str_replace(".","",$this->input->post('total'));
        $flag = $this->input->post('flag');
        $empid=$this->session->userdata('user');
		
		$type = $this->input->post('payment_type');
		if($type=='TRF'){
			$transfer = $total;
			$tunai = 0;
			$debitcard = 0;
			$creditcard = 0;
		}elseif($type=='CASH'){
			$transfer = 0;
			$tunai = $total;
			$debitcard = 0;
			$creditcard = 0;
		}elseif($type=='DC'){
			$transfer = 0;
			$tunai = 0;
			$debitcard = $total;
			$creditcard = 0;
		}elseif($type=='CC'){
			$transfer = 0;
			$tunai = 0;
			$debitcard = 0;
			$creditcard = $total;
		}
        $now = date('Y-m-d H:i:s',now());
        $data=array(
            'member_id' => $member_id,
            'transfer' => $transfer,
            'tunai' => $tunai,
            'debit_card' => $debitcard,
            'credit_card' => $creditcard,
            'total' => $total,
            'total_approved' => $total,
            'bank_id' => $bank_id, // created by Boby 20141010
            'remark_fin' => $this->db->escape_str($this->input->post('remark')),
            'approved' => 'approved',
            'tgl_transfer' => $this->input->post('fromdate'),
            'tgl_approved' => $now,
            'approvedby' => $this->session->userdata('user'),
            'flag' => $flag,
            'warehouse_id' => $this->session->userdata('whsid'),
            'event_id' => 'DP1',
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $empid
        );
            
        $this->db->insert('deposit',$data);
		$dp_id = $this->db->insert_id();
		
		$logs = array(
			'deposit_id'	=> $dp_id,
			'payment_type'	=> $type,
			'amount'		=> $total,
			'bank_id'		=> $bank_id,
			'tgl_transfer'	=> $this->input->post('fromdate'),
			'status'		=> 'approved',
			'tgl_update'	=> $now,
			'updated_by'	=> $this->session->userdata('user'),
			'remarks'		=> $this->db->escape_str($this->input->post('remark'))
		);
        $this->db->insert('deposit_log',$logs);
		
        $id = $this->db->insert_id();
        $this->db->query("call sp_deposit('$dp_id','$member_id','$total','$flag','$empid')");
    }
    public function getStockiest(){
        $q=$this->db->select("id",false)
            ->from('stockiest')
            ->where('id',$this->input->post('member_id'))
            ->where('status','active')
            ->get();
        return $var = ($q->num_rows()>0) ? false : true;
    }
    
    
}?>