<?php
class MSc_payment extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
     /*
    |--------------------------------------------------------------------------
    | Stockiest Consignment Payment
    |--------------------------------------------------------------------------
    |
    | @author budi@nawadata.com
    | @poweredby www.nawadata.com
    | @created 2019-01-01
    |
    */
	
	public function searchPinjaman($keywords=0,$num,$offset){
        $data = array();
        $whsid = $this->session->userdata('whsid');
	
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) and a.warehouse_id = '".$this->session->userdata('keywords_whsid')."'";
						//$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			}
		}
        //echo $where;
        $this->db->select("a.id,a.status_ho,a.status_hub,
		a.tglapproved_ho,a.tglapproved_hub ,
		a.approvedby_ho,a.approvedby_hub ,date_format(a.tgl,'%d-%b-%Y')as tgl,
		a.createdby,a.stockiest_id,s.no_stc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,
		m.id as member_id,
		m.nama,
		m.kecamatan,
		m.kelurahan,
		m.alamat,
		m.kodepos,
		m.kota_id,
		m.ewallet,
		m.hp,
		s.kota_delivery,
		s.type AS tipe,
			CASE
		WHEN s.type = 1 THEN
			6
		WHEN s.type = 2 THEN
			2
		ELSE
			0
		END AS persen ,
		(SELECT format(SUM(pinjaman_d.qty_lunas),0) as qty_lunas FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) as qty_lunas,
		(SELECT format(SUM(pinjaman_d.qty),0) as qty FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) as qty,
		(SELECT format(SUM(pinjaman_d.qty_retur),0) as qty FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) as qty_retur,
		(SELECT format(SUM(pinjaman_d.qty_total),0) as qty FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) as qty_total,
		a.remark,
		/*".$subQr." as warehouse_name, */
		w.name as warehouse_name, w.id as warehouse_id,  
		ifnull(date_format(a.tglapproved,'%d-%b-%Y %T'),'-')as appdate,
		ifnull(date_format(a.tglapproved_ho,'%d-%b-%Y %T'),'-')as appdate_ho,
		ifnull(date_format(a.tglapproved_hub,'%d-%b-%Y %T'),'-')as appdate_hub,
		a.status,
		IF(a.approvedby = '' OR a.approvedby IS NULL , '-',a.approvedby ) as approvedby",false);
        $this->db->from('pinjaman a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
		// START ASP 20180525
		$this->db->join('warehouse w', 'a.warehouse_id=w.id','left');
		// EOF ASP 20180525
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        $xSql= $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
				if ($row['status_ho']=='1' AND $row['status_hub']=='1' ){
					$cSql="UPDATE pinjaman SET 
					remarkapp=CONCAT(remarkapp,' Change to Delivery '), 
					status='delivery', 
					approvedby='Pusat,HUB', 
					tglapproved='".date('Y-m-d H:i:s', now())."' 
					WHERE id='".$row['id']."'";
					$query = $this->db->query($cSql);
				}
				
                //$data[] = $row;
				//echo $row['approvedby'].'<br>';
            }
        }
		//echo $xSql;
		$query = $this->db->query($xSql);
        return $query->result_array();
		
    }
	
		
    public function countPinjaman($keywords=0){
        $whsid = $this->session->userdata('whsid');
		/*
		$subQr="(SELECT
				GROUP_CONCAT(
					DISTINCT usa.`name` SEPARATOR ', '
				) AS warehouse_name
			FROM
				pinjaman_d asu
			INNER JOIN warehouse usa ON usa.id = asu.warehouse_id
			WHERE
				asu.pinjaman_id = a.id
			GROUP BY
				asu.pinjaman_id
			ORDER BY
				asu.pinjaman_id DESC
		)";
		*/
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) and a.warehouse_id = '".$this->session->userdata('keywords_whsid')."'";
						//$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			}
		}
        $this->db->select("a.id");
        $this->db->from('pinjaman a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->where($where);
		$query = $this->db->get();
		//echo $this->db->last_query();
        //return $this->db->count_all_results();
		return $query->num_rows();;
    }
   
	public function getPinjamanDetail($id=0){
        $data = array();
        $this->db->select("d.id,
		d.item_id,
		d.warehouse_id,
		format(d.qty, 0) AS fqty,
		IFNULL(d.qty_retur, 0) AS fqty_retur,
		IFNULL(d.qty_lunas, 0) AS fqty_lunas,
		format(d.harga,0)as fharga,
		format(d.pv,0)as fpv,
		format(a.bv,0)as fbv,
		format((d.qty - IFNULL(d.qty_retur, 0) )* d.harga, 0) AS fsubtotal,
		format((d.qty - IFNULL(d.qty_retur, 0) )* d.pv, 0) AS fsubtotalpv,
		format((d.qty - IFNULL(d.qty_retur, 0) )* a.bv, 0) AS fsubtotalbv,
		a.name",false);
        $this->db->from('pinjaman_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.pinjaman_id',$id);
        $this->db->group_by('d.id');
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
		
    }
	
	public function getPriceItem($whs,$item){
		$sql = "SELECT i.id AS item_id
					, i.name
					, i.price,FORMAT(i.price,0)AS fprice
					, i.price2,FORMAT(i.price2,0)AS fprice2
					, i.pv,FORMAT(i.pv,0)AS fpv
					, i.bv
					, i.hpp
					, CASE
						WHEN i.type_id = 1 THEN 'SO4'
						WHEN i.type_id = 2 THEN 'SO1'
						WHEN i.type_id = 3 THEN 'SO5'
						ELSE 'SO4'
					  END AS event_id
					, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
					, CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
		  FROM item i
		  LEFT JOIN item_rule_order_promo ir ON i.id =ir.item_id AND ir.expireddate >= DATE(NOW())
		  LEFT JOIN item_non_stc_fee inon ON i.id =inon.id
		  LEFT JOIN(	SELECT i.id
								, IFNULL(ind.price_not_disc,0) AS price_not_disc
								, MIN(ind.end_period) AS end_period
								FROM
								item i
								LEFT JOIN item_not_discount ind ON ind.item_id = i.id
									AND ind.expired = 0
									AND ind.end_period >= DATE(NOW())
								GROUP BY i.id
							) AS dt ON i.id = dt.id 
		  WHERE 
		  i.id = '".$item."'
		  ORDER BY NAME ASC
		  ";
		  
		  /*
		  $sql="
		  SELECT
			s.item_id
			,i.name
			,s.qty,FORMAT(s.qty,0)AS fqty,i.price,FORMAT(i.price,0)AS fprice,i.price2,FORMAT(i.price2,0)AS fprice2
			,i.pv,FORMAT(i.pv,0)AS fpv
			,i.bv, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
			, CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
			, w.name AS st0ck_wh, w.id as whsid,  i.warehouse_id
			FROM item i
			LEFT JOIN stock s ON s.item_id=i.id AND s.warehouse_id = i.warehouse_id
			LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
			LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
			LEFT JOIN warehouse w ON s.warehouse_id = w.id
			LEFT JOIN(
								SELECT
								i.id
								, IFNULL(ind.price_not_disc,0) AS price_not_disc
								, MIN(ind.end_period) AS end_period
								FROM
								item i
								LEFT JOIN item_not_discount ind ON ind.item_id = i.id AND ind.expired = 0 AND ind.end_period >= DATE(NOW())
								GROUP BY i.id
							) AS dt ON s.item_id = dt.id
			WHERE  s.warehouse_id = 1  
			AND i.sales = 'Yes' AND (i.name LIKE '%".$item."%' OR  i.id LIKE '%".$item."%') 
			ORDER BY NAME ASC
		  ";
		  */
		$q = $this->db->query($sql);
		if($q->num_rows > 0){
		  foreach($q->result_array() as $row){
			$data[] = $row;
		  }
		}
		//$q->free_result();
		//echo $this->db->last_query();
		return $data;
	}
    
	public function getPriceItemOld($whs,$item){
		
		$sql = "( SELECT s.item_id
					, i.name
					, s.qty,FORMAT(s.qty,0)AS fqty
					, i.price,FORMAT(i.price,0)AS fprice
					, i.price2,FORMAT(i.price2,0)AS fprice2
					, i.pv,FORMAT(i.pv,0)AS fpv
					, i.bv
					, i.hpp
					, CASE
						WHEN i.type_id = 1 THEN 'SO4'
						WHEN i.type_id = 2 THEN 'SO1'
						WHEN i.type_id = 3 THEN 'SO5'
						ELSE 'SO4'
					  END AS event_id
					, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
					, CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
					, w.name AS stock_wh, w.id as whsid, i.warehouse_id
		  FROM stock s
		  LEFT JOIN item i ON s.item_id=i.id
		  LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
		  LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
		  LEFT JOIN warehouse w ON s.warehouse_id = w.id
		  LEFT JOIN(	SELECT i.id
								, IFNULL(ind.price_not_disc,0) AS price_not_disc
								, MIN(ind.end_period) AS end_period
								FROM
								item i
								LEFT JOIN item_not_discount ind ON ind.item_id = i.id
									AND ind.expired = 0
									AND ind.end_period >= DATE(NOW())
								GROUP BY i.id
							) AS dt ON s.item_id = dt.id 
		  WHERE i.sales = 'Yes' 
		  AND ( i.id LIKE '%".$item."%' OR i.name LIKE '%".$item."%')
		  AND i.warehouse_id = 0
		  ORDER BY NAME ASC
		  )
		  UNION (
		  SELECT
		  s.item_id
		  ,i.name
		  ,s.qty,FORMAT(s.qty,0)AS fqty,i.price,FORMAT(i.price,0)AS fprice,i.price2,FORMAT(i.price2,0)AS fprice2
		  ,i.pv,FORMAT(i.pv,0)AS fpv
		  ,i.bv
		  ,i.hpp
		  , CASE
			WHEN i.type_id = 1 THEN 'SO4'
			WHEN i.type_id = 2 THEN 'SO1'
			WHEN i.type_id = 3 THEN 'SO5'
			ELSE 'SO4'
			END AS event_id
		  , CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
		  , CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
		  , w.name AS st0ck_wh, w.id as whsid,  i.warehouse_id
		  FROM item i
		  LEFT JOIN stock s ON s.item_id=i.id AND s.warehouse_id = i.warehouse_id
		  LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
		  LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
		  LEFT JOIN warehouse w ON s.warehouse_id = w.id
		  LEFT JOIN(
								SELECT
								i.id
								, IFNULL(ind.price_not_disc,0) AS price_not_disc
								, MIN(ind.end_period) AS end_period
								FROM
								item i
								LEFT JOIN item_not_discount ind ON ind.item_id = i.id AND ind.expired = 0 AND ind.end_period >= DATE(NOW())
								GROUP BY i.id
							) AS dt ON s.item_id = dt.id
		  WHERE  s.warehouse_id = 1  
		  AND i.sales = 'Yes' AND (i.name LIKE '%".$item."%' OR  i.id LIKE '%".$item."%') 
		  ORDER BY NAME ASC
		  )
		  ORDER BY warehouse_id, NAME ASC  
		  ";
		  
		  /*
		  $sql="
		  SELECT
			s.item_id
			,i.name
			,s.qty,FORMAT(s.qty,0)AS fqty,i.price,FORMAT(i.price,0)AS fprice,i.price2,FORMAT(i.price2,0)AS fprice2
			,i.pv,FORMAT(i.pv,0)AS fpv
			,i.bv, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
			, CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
			, w.name AS st0ck_wh, w.id as whsid,  i.warehouse_id
			FROM item i
			LEFT JOIN stock s ON s.item_id=i.id AND s.warehouse_id = i.warehouse_id
			LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
			LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
			LEFT JOIN warehouse w ON s.warehouse_id = w.id
			LEFT JOIN(
								SELECT
								i.id
								, IFNULL(ind.price_not_disc,0) AS price_not_disc
								, MIN(ind.end_period) AS end_period
								FROM
								item i
								LEFT JOIN item_not_discount ind ON ind.item_id = i.id AND ind.expired = 0 AND ind.end_period >= DATE(NOW())
								GROUP BY i.id
							) AS dt ON s.item_id = dt.id
			WHERE  s.warehouse_id = 1  
			AND i.sales = 'Yes' AND (i.name LIKE '%".$item."%' OR  i.id LIKE '%".$item."%') 
			ORDER BY NAME ASC
		  ";
		  */
		$q = $this->db->query($sql);
		if($q->num_rows > 0){
		  foreach($q->result_array() as $row){
			$data[] = $row;
		  }
		}
		//$q->free_result();
		//echo $this->db->last_query();
		return $data;
	}

	public function searchSc_Pay($keywords=0,$num,$offset){
        $data = array();
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "d.id = '$memberid' AND d.id LIKE '$keywords%'";
        }else{
			/*
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1){
				$where = "a.stockiest_id = '0' and b.warehouse_id = '$whsid' and (a.pinjaman_payment_id LIKE '$keywords%'  OR c.`name` LIKE '$keywords%')";
			}else {
				if(!$this->session->userdata('keywords_whsid')){
					$where = "a.stockiest_id = '0' and ( a.pinjaman_payment_id LIKE '$keywords%' OR c.`name` LIKE '$keywords%' )";
				}else{
					if($this->session->userdata('keywords_whsid')=='all')
					$where = "a.stockiest_id = '0' and ( a.pinjaman_payment_id LIKE '$keywords%' OR c.`name` LIKE '$keywords%' )";
					else
					$where = "a.stockiest_id = '0' and ( a.pinjaman_payment_id LIKE '$keywords%' OR c.`name` LIKE '$keywords%' ) and b.warehouse_id = ".$this->session->userdata('keywords_whsid');
				}
			}
			*/
			$where = "a.pinjaman_payment_id LIKE '$keywords%'  OR c.`name` LIKE '$keywords%'";
        }
        
        $this->db->select(" 
		a.pinjaman_payment_id as id,
		date_format(a.pinjaman_payment_tgl, '%d-%b-%Y') AS date,
		a.pinjaman_payment_tgl AS tgl,
		a.stockiest_id as no_stc,
		TIME_FORMAT(a.created, '%h %i %s %p') AS created,
		b.warehouse_id,
		c.`name` AS warehouse_name,
		d.id AS member_id,
		d.nama AS nama,
		b.totalharga AS ftotalharga,
		b.totalpv AS ftotalpv,
		b.deliv_addr as alamat",false);
        $this->db->from('pinjaman_payment a');
        $this->db->join('pinjaman b','b.id = a.pinjaman_id','inner');
        $this->db->join('warehouse c','c.id = b.warehouse_id','inner');
		// START ASP 20180410
        $this->db->join('member d','d.stockiest_id = a.stockiest_id','inner');
		// EOF ASP 20180410
        $this->db->where($where);
        $this->db->order_by('a.pinjaman_payment_id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countSc_Pay($keywords=0){
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "d.id = '$memberid' AND d.id LIKE '$keywords%'";
        }
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1){
				$where = "a.stockiest_id = '0' and b.warehouse_id = '$whsid' and (a.pinjaman_payment_id LIKE '$keywords%'  OR c.`name` LIKE '$keywords%')";
			}else {
				if(!$this->session->userdata('keywords_whsid')){
					$where = "a.stockiest_id = '0' and ( a.pinjaman_payment_id LIKE '$keywords%' OR c.`name` LIKE '$keywords%' )";
				}else{
					if($this->session->userdata('keywords_whsid')=='all')
					$where = "a.stockiest_id = '0' and ( a.pinjaman_payment_id LIKE '$keywords%' OR c.`name` LIKE '$keywords%' )";
					else
					$where = "a.stockiest_id = '0' and ( a.pinjaman_payment_id LIKE '$keywords%' OR c.`name` LIKE '$keywords%' ) and b.warehouse_id = ".$this->session->userdata('keywords_whsid');
				}
			}
        }
        
        
        $this->db->from('pinjaman_payment a');
        $this->db->join('pinjaman b','b.id = a.pinjaman_id','inner');
        $this->db->join('warehouse c','c.id = b.warehouse_id','inner');
		// START ASP 20180410
        $this->db->join('member d','d.stockiest_id = a.stockiest_id','inner');
		// EOF ASP 20180410
        $this->db->where($where);
        $this->db->order_by('a.pinjaman_payment_id','desc');
        return $this->db->count_all_results();
    }

	
    public function getPayment($keywords){
		$where = "a.pinjaman_id LIKE '%".$keywords."%'";
		$this->db->select(" 
		a.pinjaman_payment_id as id, a.pinjaman_d_id,
		a.ro_id as ro_id,
		date_format(a.pinjaman_payment_tgl, '%d-%b-%Y') AS date,
		a.pinjaman_payment_tgl AS tgl,
		a.stockiest_id as member_id, a.stockiest_id_payment as member_id_to, 
		a.pinjaman_d_qty as qty,
		FORMAT(a.pinjaman_payment_ttl,0) as ftotal_pay,
		m.nama as member_nama_to,
		pd.warehouse_id as warehouse_id , 
		wh.`name` as warehouse_nama,
		i.`name` as nama_item 
		",false);
        $this->db->from('pinjaman_payment a');
        $this->db->join('member m','m.id = a.stockiest_id_payment','inner');
        $this->db->join('pinjaman_d pd','pd.id = a.pinjaman_d_id','inner');
        $this->db->join('warehouse wh','wh.id = pd.warehouse_id','inner');
        $this->db->join('item i','i.id = pd.item_id','inner');
		// START ASP 20180410
        //$this->db->join('member d','d.stockiest_id = a.stockiest_id_payment','LEFT');
		// EOF ASP 20180410
        $this->db->where($where);
        $this->db->order_by('a.pinjaman_payment_id','desc');
        //$this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
	}
	
	
	public function addPaymenySc(){
		$ttlpv=str_replace(".","",$this->input->post('totalpv'));
		if (empty($this->input->post('totalpv'))){$ttlpv='0';};
		
		$data = array(
            'member_id' => $this->input->post('member_id_to'),
            'stockiest_id'=>0,
            'date' => date('Y-m-d',now()),
			'diskon' => $this->input->post('persen'),
			'rpdiskon' => str_replace(".","",$this->input->post('totalrpdiskon')),
			'totalharga' => str_replace(".","",$this->input->post('totalbayar')),
			'totalharga2' => str_replace(".","",$this->input->post('total')),
			'totalpv' => str_replace(".","",$this->input->post('totalpv')),
			'warehouse_id' => str_replace(".","",$this->input->post('whsid')),
			'approvedby' => $this->session->userdata('user'),
			'approvedby1' => $this->session->userdata('user'),
			'approvedby2' => $this->session->userdata('user'),
			'tglapproved' => date('Y-m-d H:i:s',now()),
			'tglapproved1' => date('Y-m-d H:i:s',now()),
			'tglapproved2' => date('Y-m-d H:i:s',now()),
			'status' => 'delivery',
			'status1' => 'delivery',
			'status2' => 'delivery',
            'remark'=>'Create By System From SC '.$this->input->post('sc_id'),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$this->session->userdata('user') 
        );
        $this->db->insert('ro',$data);
		$ro_id = $this->db->insert_id();
		
        $myData=$this->input->post('iRow');
		for ($i = 0; $i <= ($myData-1); $i++) {
			$subtotalnya = str_replace(".","", $this->input->post('subtotal'.$i));
			$subtotalrpdiskonnya = str_replace(".","",$this->input->post('subtotalrpdiskon'.$i));
			if (!empty($this->input->post('qty_lunas'.$i))){
				$data_pay = array(
					'pinjaman_payment_tgl' => date('Y-m-d H:i:s',now()),
					'pinjaman_payment_ttl' => $subtotalnya - $subtotalrpdiskonnya,//str_replace(".","", $this->input->post('subtotal'.$i)),
					'pinjaman_payment_ttl_pv' => str_replace(".","", $this->input->post('subtotalpv'.$i)),
					'pinjaman_d_id' => $this->input->post('pin_d'.$i),
					'pinjaman_d_qty' => $this->input->post('qty_lunas'.$i),
					'warehouse_id' => $this->input->post('det_whsid'.$i),
					'item_id' => $this->input->post('itemcode'.$i),
					'stockiest_id' => $this->input->post('member_id'),
					'stockiest_id_payment' => $this->input->post('member_id_to'),
					'pinjaman_id' => $this->input->post('sc_id'),
					//'ref_no' => $this->db->escape_str($this->input->post('itemcode')),
					'ro_id' => $ro_id,
					'voucher_price' => $this->db->escape_str($this->input->post('vtotal')),
					'created' => date('Y-m-d H:i:s',now()),
					'createdby' => $this->session->userdata('user')
				);
				$this->db->insert('pinjaman_payment',$data_pay);
				
				/*
				Lewat SP..
				$dSql="UPDATE pinjaman_d SET qty_lunas = '".$this->input->post('qty_lunas'.$i)."' WHERE id='".$this->input->post('pin_d'.$i)."'";
				$q = $this->db->query($dSql);
				*/
				/*
				if($this->input->post('persen') >0){            
					$diskon = $this->input->post('persen');
					$rpdiskon = str_replace(",","",$this->input->post('rpdiskon'));
				}else{
					$diskon=0;
					$rpdiskon=0;
				}
				*/
				$qty = str_replace(".","",$this->input->post('qty_lunas'.$i));
				$pv = str_replace(".","",$this->input->post('pv'.$i));
				$hrg = str_replace(",","",$this->input->post('price'.$i));
				
				$rpdiskon = str_replace(".","",$this->input->post('rpdiskon'.$i));


				if(str_replace(".","",$this->input->post('pv'.$i)) > 0){$hrg = $hrg * (1-(($diskon)/100));}
				$data_ro_d=array(
					'ro_id' => $ro_id,
					'warehouse_id' => $this->input->post('det_whsid'.$i),
					'item_id' => $this->input->post('itemcode'.$i),
					'qty' => $qty,
					'harga_' => str_replace(",","",$this->input->post('price'.$i)),
					'harga' => $hrg-$rpdiskon,
					'pv' => str_replace(",","",$this->input->post('pv'.$i)),
					'bv' => str_replace(",","",$this->input->post('bv'.$i)),
					//'jmlharga' => $hrg*$qty,
					'hpp' => $this->input->post('hpp'.$i),
					'event_id' => $this->input->post('event_id'.$i),
					'jmlharga' =>$qty*($hrg-$rpdiskon),
					'jmlharga_' =>str_replace(".","",$this->input->post('subtotal'.$i)),
					'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$i)),
					'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$i))
				);
				
				$this->db->insert('ro_d',$data_ro_d);
			}
		}
        
		$this->db->query("call sp_sc_payment('".$this->input->post('sc_id')."','".$ro_id."','".$this->session->userdata('user')."','".$this->input->post('whsid')."')"); 
        //$this->db->query("call sp_sc_ewallet_transfer('in_debet','in_kredit','in_desc','in_noref','in_nominal','".$this->session->userdata('user')."')"); -- Di pindah ke dalam sp_sc_payment
		
		$this->db->query("UPDATE pinjaman_d SET qty_total=(IFNULL(qty,0)-IFNULL(qty_retur,0)-IFNULL(qty_lunas,0))");
		
    }
	
	
    public function ro_add_admin(){
        $amount = str_replace(".","",$this->input->post('total'));
		$totalharga2 = $amount;
		
        if($this->input->post('persen') >0){            
            $diskon = $this->input->post('persen');
            $rpdiskon = str_replace(".","",$this->input->post('totalrpdiskon'));
        }else{
            $diskon=0;
            $rpdiskon=0;
        }
		
        $totalharga = $amount - $rpdiskon;
			
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
        $empid = $this->session->userdata('user');
        $whsid = $this->session->userdata('whsid');
        $member_id = $this->input->post('member_id');
        
        $data = array(
            'member_id' => $member_id,
            'stockiest_id'=>'0',
            'date' => date('Y-m-d',now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'diskon' => $diskon,
            'rpdiskon' => $rpdiskon,
            'totalharga2' => $totalharga2,
            'warehouse_id' => $whsid,
	    'delivery' => $this->input->post('pu'),
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('ro',$data);
        
        $id = $this->db->insert_id();
        
	$key=0;
        while($key < count($_POST['counter'])){
            $qty = str_replace(".","",$this->input->post('qty'.$key));
	    $harga = str_replace(".","",$this->input->post('price'.$key));
	    $diskon = str_replace(".","",$this->input->post('rpdiskon'.$key));
            if($this->input->post('itemcode'.$key) and $qty > 0){
                $data=array(
                    'ro_id' => $id,
                    'item_id' => $this->input->post('itemcode'.$key),
                    'qty' => $qty,
                    'harga_' => str_replace(".","",$this->input->post('price'.$key)),
                    'harga' => $harga-$diskon,
                    'pv' => str_replace(".","",$this->input->post('pv'.$key)),
		    'bv' => str_replace(".","",$this->input->post('bv'.$key)),
                    'jmlharga_' =>str_replace(".","",$this->input->post('subtotal'.$key)),
		    'jmlharga' =>$qty*($harga-$diskon),
                    'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$key)),
		    'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$key))
                 );
                 $this->db->insert('ro_d',$data);
            }
            $key++;
        }
	
	$this->db->query("CALL request_order_procedure('$id','$whsid','$member_id','$totalharga','$empid')");
	
	$data = array(
		'member_id' => $member_id,
		'kota' => $this->input->post('kota_id'),
		'alamat' => strip_quotes($this->db->escape_str($this->input->post('addr'))),
		'created' => $this->session->userdata('username')
	);
	
		$addr_id = $this->input->post('deli_ad');
		if(	$this->input->post('addr') != $this->input->post('addr1')	|| 	$this->input->post('kota_id') != $this->input->post('kota_id1')){
			$this->db->insert('member_delivery',$data);
			$addr_id = $this->db->insert_id();
			$this->db->update('member',array('delivery_addr'=>$addr_id),array('id' => $this->input->post('member_id')));
		}
		$this->db->update('ro',array('deliv_addr'=>$addr_id),array('id' => $id));
    }
       
	
	public function getEwallet($member_id){
        $q = $this->db->select("ewallet,format(ewallet,0)as fewallet",false)
            ->from('stockiest')
            ->where('id',$member_id)
            ->get();
        return $var = ($q->num_rows()>0)? $q->row() : false;
    }
	
	public function getECB($member_id){
        $q = $this->db->select("saldo_ecb as saldoecb,format(saldo_ecb,0)as fsaldoecb",false)
            ->from('deposit_exclusive')
            ->where('member_id',$member_id)
            ->get();
        return $var = ($q->num_rows()>0)? $q->row() : false;
    }
    
    public function searchSc_PayTemp(){
        $data = array();
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            if($this->session->userdata('group_id')==102){
                $where = "a.stockiest_id = '$memberid'";
            }else{
                $where = "a.member_id = '$memberid'";
            }
        }
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.member_id,s.no_stc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama,a.remarkapp,a.status",false);
        $this->db->from('ro_temp a');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        if($this->session->userdata('group_id')>100)$this->db->where($where);
        $this->db->order_by('a.id','desc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
       
    /*
    |--------------------------------------------------------------------------
    | Request Order Stockiest
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-04-15
    |
    */
    public function searchSc_PayMSTC($keywords=0,$num,$offset){
        $data = array();
        if($this->session->userdata('group_id')==102){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        else{
			/*Modified by Boby 2009-11-23*/
			//$where = "a.stockiest_id != '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			/*end Modified by Boby 2009-11-23*/
            $where = "a.stockiest_id = '0' and s.type = '2' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.member_id,s.no_stc,x.no_stc as no_stc2,z.nama as namastc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama,a.remarkapp,a.status",false);
        $this->db->from('ro a');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('stockiest x','a.stockiest_id=x.id','left');
        $this->db->join('member z','a.stockiest_id=z.id','left');
        $this->db->where($where);
        $this->db->order_by('a.status','asc');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countSc_PayMSTC($keywords=0){
        if($this->session->userdata('group_id')==102){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        else{
            $where = "a.stockiest_id = '0' and s.type = '2' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        $this->db->from('ro a');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    public function getRequestOrderMSTC($id=0){
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.diskon,format(a.rpdiskon,0)as frpdiskon,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.remarkapp,a.status,a.tglapproved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    s.no_stc,x.no_stc as no_stc2,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('ro a');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->join('stockiest x','a.stockiest_id=x.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('member z','a.stockiest_id=z.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->session->userdata('group_id')==102)$this->db->where('a.stockiest_id',$this->session->userdata('userid'));
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getRequestOrderTempMSTC($id=0){
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.stockiest_id,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.remarkapp,a.status,a.tglapproved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    s.no_stc,x.no_stc as no_stc2,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('ro_temp a');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->join('stockiest x','a.stockiest_id=x.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('member z','a.stockiest_id=z.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->session->userdata('group_id')>100)$this->db->where('a.stockiest_id',$this->session->userdata('userid'));
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
	public function check_titipan_ro($rqtid,$stcid){
        $data=array();
        $q = $this->db->query("SELECT f_check_titipan_ro('$rqtid','$stcid') as l_result");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['l_result'];
    }
    

	/* Created by Budi 20190110 */
	public function getFree($id){
        $data = array();
        $q = "
			SELECT rf.trx_id, rf.nc_id, ncd.item_id, i.name AS prd, ncd.qty
			FROM ref_trx_nc rf
			LEFT JOIN ncm nc ON rf.nc_id = nc.id
			LEFT JOIN ncm_d ncd ON nc.id = ncd.ncm_id
			LEFT JOIN item i ON ncd.item_id = i.id
			WHERE rf.trx = 'SC'
			AND rf.trx_id = $id
			";
        $q = $this->db->query($q);
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	/* End created by Budi 20190110 */
  
	public function getDropDownWhs($all){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            if($all == 'all')$data['all']='All Cabang';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
	
    public function getWhsName($id){
        $data = array();
        $q = $this->db->get_where('warehouse',array('id' => $id));
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[]=$row;    
            }
        }
        $q->free_result();
        return $data;
    }
	// EOF Created By ASP 20180308
	
	public function count_FreeItem($keywords=0){
		$this->db->select("
		a.*, 
		b.item_id,
		c.`name` AS item_name,
		( SELECT c. NAME AS free_item_name FROM item c WHERE c.id = d.free_item_id ) AS Free_Item_Name, 
		b.warehouse_id,
		d.item_qty,
		d.free_item_id,
		d.free_item_qty
		",false);
		$this->db->from('pinjaman_nc a');
		$this->db->join('pinjaman_d b','b.id = a.pinjaman_d_id','inner');
		$this->db->join('item c','c.id = b.item_id','inner');
		$this->db->join('promo d','d.id = a.promo_id','inner');
		$this->db->like('a.pinjaman_id', $keywords, 'after');
		$this->db->order_by('c.`name`','asc');
		$q = $this->db->get();
		$q->num_rows;
		return $q->num_rows;
	}

	public function get_FreeItem_Full($keywords=0,$num,$offset){
		$perpage=10;
		
		if (empty($offset)){$offset=0;}else{$offset=$offset;}
		
		$sql="
		SELECT
		s.item_id
		,i.name
		,s.qty,FORMAT(s.qty,0)AS fqty,i.price,FORMAT(i.price,0)AS fprice,i.price2,FORMAT(i.price2,0)AS fprice2
		,i.pv,FORMAT(i.pv,0)AS fpv
		,i.bv, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
		, CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
		, w.name AS st0ck_wh, w.id as whsid,  i.warehouse_id
		FROM item i
		LEFT JOIN stock s ON s.item_id=i.id AND s.warehouse_id = i.warehouse_id
		LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
		LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
		LEFT JOIN warehouse w ON s.warehouse_id = w.id
		LEFT JOIN(
							SELECT
							i.id
							, IFNULL(ind.price_not_disc,0) AS price_not_disc
							, MIN(ind.end_period) AS end_period
							FROM
							item i
							LEFT JOIN item_not_discount ind ON ind.item_id = i.id AND ind.expired = 0 AND ind.end_period >= DATE(NOW())
							GROUP BY i.id
						) AS dt ON s.item_id = dt.id
		WHERE  s.warehouse_id = 1  
		AND i.sales = 'Yes' AND s.qty > 0 AND (i.name LIKE '%".$keywords."%' OR  i.id LIKE '%".$keywords."%') 
		ORDER BY NAME ASC LIMIT ".$offset.",".$perpage;
		$q = $this->db->query($sql);
		if($q->num_rows > 0){
		  foreach($q->result_array() as $row){
			$data[] = $row;
		  }
		}
		//$q->free_result();
		//echo $this->db->last_query();
		return $data;
	}
	
	public function search_FreeItem($keywords=0,$num,$offset){
		$data = array();
		/*
		$this->db->select("s.item_id,i.name,s.qty,format(s.qty,0)as fqty,i.price,format(i.price,0)as fprice,i.price2,format(i.price2,0)as fprice2,i.pv,format(i.pv,0)as fpv,i.bv",false);
		$this->db->from('stock s');
		$this->db->join('item i','s.item_id=i.id','left');
		*/
		$cSql="
		SELECT
		a.*, 
		b.item_id,
		c.`name` AS item_name,
		( SELECT c. NAME AS free_item_name FROM item c WHERE c.id = d.free_item_id ) AS Free_Item_Name, 
		b.warehouse_id,
		d.item_qty,
		d.free_item_id,
		d.free_item_qty
		FROM
		pinjaman_nc a 
		INNER JOIN pinjaman_d b ON b.id = a.pinjaman_d_id
		INNER JOIN item c ON c.id = b.item_id
		INNER JOIN promo d ON d.id = a.promo_id
		";
		
		$this->db->select("
		a.*, 
		b.item_id,
		c.`name` AS item_name,
		( SELECT c. NAME AS free_item_name FROM item c WHERE c.id = d.free_item_id ) AS Free_Item_Name, 
		b.warehouse_id,
		d.item_qty,
		d.free_item_id,
		d.free_item_qty,
		a.qty as GetFree 
		",false);
		$this->db->from('pinjaman_nc a');
		$this->db->join('pinjaman_d b','b.id = a.pinjaman_d_id','inner');
		$this->db->join('item c','c.id = b.item_id','inner');
		$this->db->join('promo d','d.id = a.promo_id','inner');
		$this->db->like('a.pinjaman_id', $keywords, 'after');
		$this->db->order_by('c.`name`','asc');
		$this->db->limit($num,$offset);
		$q = $this->db->get();
		//echo $this->db->last_query();
		if($q->num_rows > 0){
		  foreach($q->result_array() as $row){
			$data[] = $row;
		  }
		}
		$q->free_result();
		return $data;
	}
	
	
}
?>