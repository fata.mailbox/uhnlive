<?php

class Mmarketing extends CI_Model
{
	
	 function __construct()
    {
        parent::__construct();
    }
		/* created by Annisa R
		20190821
		*/


public function acv_reports($tahun,$quart)
    {
    	$bln =  substr($quart, -5, 2);
				
		$data = array();
		$thn_ = $thn-1;
		$br = "<br>";
		$br = "";
		 //echo $q;
		if($quart  !=0){
			// Jika tidak all
			$tgl = $quart;
			$awal = substr($tgl, 0,10);
			$akhir = $tahun.substr($tgl, -6,10);
			
			//$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ";
			//$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ";
			//echo $awal." & ".$akhir;
			//echo $bln ;
			$qry1="_, bln AS periode ".$br;
			$qry2="_, MONTH(rs.periode) AS periode ".$br;
			$qry3="HAVING periode BETWEEN MONTH($awal) AND MONTH($akhir) ".$br;
			
			$bln1_ = date('n')-2;
			$thn1_ = date('Y');
			//echo $bln1_;
			
			if($bln >= $bln1_ && $tahun = $thn1_){
				$query = "CALL sp_report_item_group_marketing('2019-01-01'); ";
				$qry = $this->db->query($query);
			}
			
			
		}else{
			// Jika all
			$qry1=" ";$qry2=" ";$qry3=" ";
			$awal = "'".$thn."-01-01' ".$br;
			// $akhir = "NOW() ".$br;
			$akhir = "'".$thn."-12-31' ".$br;
			$query = "CALL sp_report_item_group_marketing(NOW()); ";
			$qry = $this->db->query($query);
		}


    	
		$flag = 1;
		$q = "	
		SELECT 
			IFNULL(ig.id,99) as group_id
			, CASE when ig.id is not null THEN ig.name ELSE 'Others' END as group_name
			, newdata.id, newdata.nama
			-- , FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			-- adj stock
			, jmladjstock, FORMAT(totadjstock,0)AS totadjstock, totadjstock AS totadjstock1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			-- , ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, ((jml + jmlsc + 0) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
			, (total + totsc + 0) + totretur AS totalnominal1_
			-- topup event
			, jml_topup, FORMAT(total_topup,0)AS total_topup, total_topup as total_topup1
			, jml_event, FORMAT(total_event,0)AS total_event, total_event as total_event1
			, ((jml + jmlsc + 0 + jml_topup + jml_event) - (jmlretur)) AS totalqty_all
			, FORMAT((total + totsc + 0 + total_topup + total_event) - (totretur),0) AS totalnominal_all
			, (total + totsc + 0 + total_topup + total_event) - (totretur) AS totalnominal_all1
			, (total + totsc + 0 + total_topup + total_event) + (totretur) AS totalnominal_all1_
			, ig.target_group AS qty
			, rgm.periode AS periode
			/* CASE  
				WHEN MONTH('.$awal.') BETWEEN 1 AND 3 THEN 1  
				WHEN MONTH('.$awal.') BETWEEN 4 AND 6 THEN 2  
				WHEN MONTH('.$awal.') BETWEEN 7 AND 9 THEN 3  
				WHEN MONTH('.$awal.') BETWEEN 10 AND 12 THEN 4 
				END AS periode_, MONTH(rs.periode) AS periode  
			*/
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama -- , dta.harga AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtg.jml,0) AS jmladjstock, IFNULL(dtg.total,0) AS totadjstock
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
				-- topup event
				, (IFNULL(dtb_topup.jml,0) + IFNULL(dtc_topup.jml,0) - IFNULL(dte_topup.jml,0)) AS jml_topup
				, (IFNULL(dtb_topup.total,0) + IFNULL(dtc_topup.total,0) - IFNULL(dte_topup.total,0)) AS total_topup
				, (IFNULL(dtb_event.jml,0) + IFNULL(dtc_event.jml,0) - IFNULL(dte_event.jml,0)) AS jml_event
				, (IFNULL(dtb_event.total,0) + IFNULL(dtc_event.total,0) - IFNULL(dte_event.total,0)) AS total_event
			FROM(
				SELECT i.id, i.`name` -- , sod.harga
				FROM item i
				-- LEFT JOIN so_d sod ON i.id = sod.item_id
				-- GROUP BY sod.item_id -- , sod.harga
			)AS dta
			LEFT JOIN(
				SELECT periode, itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct (so.id),
					CONCAT(YEAR(so.tgl),'-',MONTH(so.tgl),'-',DAY(LAST_DAY(so.tgl))) AS periode
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.jmlharga END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
					
					FROM so_d sod
					LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga = rp.package_price
					LEFT JOIN so ON sod.so_id = so.id
					WHERE 
						so.tgl BETWEEN '$awal' AND '$akhir'
						AND so.stockiest_id = '0'
						AND sod.item_id NOT IN (SELECT id FROM item_topup)
						AND sod.item_id NOT IN (SELECT id FROM item_event)
						
				) as dt
				GROUP BY periode,itemo_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT periode, itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct (so.id),
					CONCAT(YEAR(so.tgl),'-',MONTH(so.tgl),'-',DAY(LAST_DAY(so.tgl))) AS periode
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.jmlharga END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
					FROM so_d sod
					LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga = rp.package_price
					LEFT JOIN so ON sod.so_id = so.id
					WHERE 
						so.tgl BETWEEN '$awal' AND '$akhir'
						AND so.stockiest_id = '0'
						AND sod.item_id IN (SELECT id FROM item_topup)
						-- GROUP BY so.tgl
				) as dt
				GROUP BY periode,itemo_id -- , sod.harga
			)AS dtb_topup ON dta.id = dtb_topup.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT periode, itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct (so.id),
					CONCAT(YEAR(so.tgl),'-',MONTH(so.tgl),'-',DAY(LAST_DAY(so.tgl))) AS periode
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.jmlharga END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
					FROM so_d sod
					LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga = rp.package_price
					LEFT JOIN so ON sod.so_id = so.id
					WHERE 
						so.tgl BETWEEN '$awal' AND '$akhir'
						AND so.stockiest_id = '0'
						AND sod.item_id IN (SELECT id FROM item_event)
				) as dt
				GROUP BY periode,itemo_id -- , sod.harga
			)AS dtb_event ON dta.id = dtb_event.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT periode, itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(rod.jmlharga_)AS total";}else{$q.="	, SUM(1*rod.jmlharga_)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct(ro.id),
					CONCAT(YEAR(ro.date),'-',MONTH(ro.date),'-',DAY(LAST_DAY(ro.date))) AS periode
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE rod.jmlharga_ END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga_*rod.qty) END AS total
					FROM ro_d rod
					LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga_ = rp.package_price
					LEFT JOIN ro ON rod.ro_id = ro.id
					WHERE 
						ro.`date` BETWEEN '$awal' AND '$akhir'
						AND ro.stockiest_id = '0'
						AND rod.item_id NOT IN (SELECT id FROM item_topup)
						AND rod.item_id NOT IN (SELECT id FROM item_event)
				) as dt
				GROUP BY periode,itemo_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT periode, itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(rod.jmlharga_)AS total";}else{$q.="	, SUM(1*rod.jmlharga_)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct(ro.id),
					CONCAT(YEAR(ro.date),'-',MONTH(ro.date),'-',DAY(LAST_DAY(ro.date))) AS periode
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE rod.jmlharga_ END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga_*rod.qty) END AS total
					FROM ro_d rod
					LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga_ = rp.package_price
					LEFT JOIN ro ON rod.ro_id = ro.id
					WHERE 
						ro.`date` BETWEEN '$awal' AND '$akhir'
						AND ro.stockiest_id = '0'
						AND rod.item_id IN (SELECT id FROM item_topup)
						GROUP BY ro.date
				) as dt
				GROUP BY periode,itemo_id -- , rod.harga
			)AS dtc_topup ON dta.id = dtc_topup.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT periode, itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(rod.jmlharga_)AS total";}else{$q.="	, SUM(1*rod.jmlharga_)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct(ro.id),
					CONCAT(YEAR(ro.date),'-',MONTH(ro.date),'-',DAY(LAST_DAY(ro.date))) AS periode
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE rod.jmlharga_ END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga_*rod.qty) END AS total
					FROM ro_d rod
					LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga_ = rp.package_price
					LEFT JOIN ro ON rod.ro_id = ro.id
					WHERE 
						ro.`date` BETWEEN '$awal' AND '$akhir'
						AND ro.stockiest_id = '0'
						AND rod.item_id IN (SELECT id FROM item_event)
				) as dt
				GROUP BY periode,itemo_id -- , rod.harga
			)AS dtc_event ON dta.id = dtc_event.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT periode, itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					CONCAT(YEAR(pj.tgl),'-',MONTH(pj.tgl),'-',DAY(LAST_DAY(pj.tgl))) AS periode
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ptd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE ptd.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*ptd.qty ELSE ptd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*ptd.qty ELSE ptd.jmlharga END AS total
					FROM pinjaman_titipan_d ptd
					LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
					LEFT JOIN reff_package rp ON ptd.item_id = rp.package_id AND ptd.harga = rp.package_price
					WHERE pj.tgl BETWEEN '$awal' AND '$akhir'
				) AS dt
				GROUP BY periode,itemo_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
			LEFT JOIN(
				SELECT periode, itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- DISTINCT (rt.id),
					CONCAT(YEAR(rt.tgl),'-',MONTH(rt.tgl),'-',DAY(LAST_DAY(rt.tgl))) AS periode
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
					FROM retur_titipan_d rtd
					LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
					LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
					WHERE rt.tgl BETWEEN '$awal' AND '$akhir'
					AND rtd.item_id NOT IN (SELECT id FROM item_topup)
					AND rtd.item_id NOT IN (SELECT id FROM item_event)
					
				) as dt
				GROUP BY periode,itemo_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT periode, itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- DISTINCT (rt.id),
					CONCAT(YEAR(rt.tgl),'-',MONTH(rt.tgl),'-',DAY(LAST_DAY(rt.tgl))) AS periode
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
					FROM retur_titipan_d rtd
					LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
					LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
					WHERE rt.tgl BETWEEN '$awal' AND '$akhir'
					AND rtd.item_id IN (SELECT id FROM item_topup)
					-- GROUP BY rt.tgl
				) as dt
				GROUP BY periode,itemo_id -- , rtd.harga
			)AS dte_topup ON dta.id = dte_topup.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT periode, itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- DISTINCT (rt.id),
					CONCAT(YEAR(rt.tgl),'-',MONTH(rt.tgl),'-',DAY(LAST_DAY(rt.tgl))) AS periode
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
					FROM retur_titipan_d rtd
					LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
					LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
					WHERE rt.tgl BETWEEN '$awal' AND '$akhir'
					AND rtd.item_id IN (SELECT id FROM item_event)
					-- GROUP BY rt.tgl
				) as dt
				GROUP BY periode,itemo_id -- , rtd.harga
			)AS dte_event ON dta.id = dte_event.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT periode, itemo_id AS item_id, FLOOR(SUM(qty)) AS jml, hargao AS harga, SUM(total) AS total
				FROM (
					SELECT
					CONCAT(YEAR(nc.date),'-',MONTH(nc.date),'-',DAY(LAST_DAY(nc.date))) AS periode,
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
					FROM ncm_d ncd
					LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
					LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
					WHERE nc.`date` BETWEEN '$awal' AND '$akhir'
					AND (remark NOT LIKE 'Free Of RO%'
					AND remark NOT LIKE 'CFRO no.%')
					-- GROUP BY nc.date
				) AS dt	
				GROUP BY periode,itemo_id
			)AS dtf ON dta.id = dtf.item_id
			-- adj free RO
			LEFT JOIN(
				SELECT periode, itemo_id AS item_id, FLOOR(SUM(qty)) AS jml, hargao AS harga, SUM(total) AS total
				FROM (
					SELECT
					CONCAT(YEAR(nc.date),'-',MONTH(nc.date),'-',DAY(LAST_DAY(nc.date))) AS periode
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
					FROM ncm_d ncd
					LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
					LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
					WHERE nc.`date` BETWEEN '$awal' AND '$akhir'
					AND (remark LIKE 'Free Of RO%'
					OR remark LIKE 'CFRO no.%')
					-- GROUP BY nc.date
				) AS dt	
				GROUP BY periode,itemo_id
			)AS dtg ON dta.id = dtg.item_id
		)AS newdata
		LEFT JOIN item_to_item_group_marketing iig on newdata.id = iig.id
		LEFT JOIN item_group_marketing ig on iig.group_id = ig.id
		LEFT JOIN report_item_group_marketing rgm on rgm.item_id=iig.id
		WHERE periode BETWEEN '$awal' AND '$akhir'
		HAVING totalnominal1_ <> 0 OR jmlnc <> 0 OR jmladjstock <> 0 OR totalnominal_all1_ <> 0 OR jml_topup <> 0 OR jml_event <> 0
		
		order by group_id, newdata.id,newdata.nama
		";
        $data = array();
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }

    public function get_year_report(){
        $data = array();
		$thn=date("Y");
		for($i=$thn;$i>='2009';$i--){
			$data[$i]=$i;
		}
        return $data;
    }

    public function get_q($q){
        $data = array();
		if($q==1){//$data['00-00']='All';}
		$data['01-01-03-31']='Quarter1';}
		$data['04-01-06-30']='Quarter2';
		$data['07-01-09-30']='Quarter3';
		$data['10-01-12-31']='Quarter4';
        return $data;
    }

    public function report($tahun,$quart){
    	$bln_awal = substr($quart, 0,5);
    	$bln_akhir = substr($quart, -5);
    	$awal = $tahun.'-'.$bln_awal;
    	$akhir = $tahun.'-'.$bln_akhir;
    	//echo $bln_akhir." & ".$awal;
    	$data  = array();
    	$qry = $this->db->query("SELECT * FROM report_item_group_marketing WHERE periode BETWEEN $awal AND $akhir");
    	if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
    public function acv_reports_2(){
    	$data = array();
    	$query = "SELECT itgm.id id, i.name produk, t.qty target_qty, t.value target_value, igm.`name` grup
				FROM item_to_item_group_marketing itgm LEFT JOIN item i ON itgm.`id`=i.`id`
				LEFT JOIN item_group_marketing igm ON itgm.`group_id`=igm.`id`
				LEFT JOIN target t ON itgm.`id`=t.`item_id`";
		$qry = $this->db->query($query);
		 //echo $this->db->last_query();
		
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }

    public function acv_reports3($bln)
    {
    	$awal = $bln;
		$akhir = date("Y-m-t", strtotime($bln));

    	$query ="SELECT 
						IFNULL(ig.id,99) AS group_id
						, CASE WHEN ig.id IS NOT NULL THEN ig.name ELSE 'Other Group' END AS group_name
						, newdata.id, newdata.nama
						
						, jml, FORMAT(total,0)AS total, total AS total1
						, jmlnc, FORMAT(totnc,0)AS totnc, totnc AS totnc1
						, jmlsc, FORMAT(totsc,0)AS totsc, totsc AS totsc1
						, jmlretur, FORMAT(totretur,0)AS totretur, totretur AS totretur1
						
						, ((jml + jmlsc + 0) - jmlretur) AS totalqty
						, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
						, (total + totsc + 0) - totretur AS totalnominal1
						, (total + totsc + 0) + totretur AS totalnominal1_
						, igtm.target_qty AS qty_group
						, itm.target_qty AS qty_item
						, igtm.target_val AS val_group
						, itm.target_val AS val_item

					FROM(
						SELECT 
							dta.id, dta.`name` AS nama 
							, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
							, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
							, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
							, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
							, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
						FROM(
							SELECT i.id, i.`name` 
							FROM item i
							
							
						)AS dta
						LEFT JOIN(
							SELECT itemo_id AS item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total
							FROM (
								SELECT
								
								CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
								,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
								,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
								
								,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
								FROM so_d sod
								LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga = rp.package_price
								LEFT JOIN so ON sod.so_id = so.id
								WHERE 
									so.tgl BETWEEN '$awal' AND '$akhir'
									AND so.stockiest_id = '0'
							) AS dt
							GROUP BY itemo_id 
						)AS dtb ON dta.id = dtb.item_id  
						LEFT JOIN(
							SELECT itemo_id AS item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total
							FROM (
								SELECT
								
								CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
								,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
								,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
								
								,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga_*rod.qty) END AS total
								FROM ro_d rod
								LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga_ = rp.package_price
								LEFT JOIN ro ON rod.ro_id = ro.id
								WHERE 
									ro.`date` BETWEEN '$awal' AND '$akhir'
									AND ro.stockiest_id = '0'
							) AS dt
							GROUP BY itemo_id 
						)AS dtc ON dta.id = dtc.item_id  
						LEFT JOIN(
							SELECT itemo_id AS item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total
							FROM (
								SELECT
								CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ptd.item_id END AS itemo_id
								,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE ptd.harga END AS hargao
								,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*ptd.qty ELSE ptd.qty END AS qty
								,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*ptd.qty ELSE ptd.jmlharga END AS total
								FROM pinjaman_titipan_d ptd
								LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
								LEFT JOIN reff_package rp ON ptd.item_id = rp.package_id AND ptd.harga = rp.package_price
								WHERE pj.tgl BETWEEN '$awal' AND '$akhir'
							) AS dt
							GROUP BY itemo_id 
						)AS dtd ON dta.id = dtd.item_id  
						LEFT JOIN(
							SELECT itemo_id AS item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total
							FROM (
								SELECT
								
								CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
								,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
								,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
								,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
								FROM retur_titipan_d rtd
								LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
								LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
								WHERE rt.tgl BETWEEN '$awal' AND '$akhir'
							) AS dt
							GROUP BY itemo_id 
						)AS dte ON dta.id = dte.item_id  
						LEFT JOIN(
							SELECT itemo_id AS item_id, FLOOR(SUM(qty)) AS jml, hargao AS harga, SUM(total) AS total
							FROM (
								SELECT
								CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
								,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
								,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
								,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
								FROM ncm_d ncd
								LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
								LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
								-- LEFT JOIN item_id_oldnew io ON io.item_id_old =  
								WHERE nc.`date` BETWEEN '$awal' AND '$akhir'
							) AS dt	
							GROUP BY itemo_id
						)AS dtf ON dta.id = dtf.item_id
					)AS newdata
					LEFT JOIN item_to_item_group_marketing iig ON newdata.id = iig.id
					LEFT JOIN item_group_marketing ig ON iig.group_id = ig.id
					LEFT JOIN item_group_target_marketing igtm ON igtm.item_group_id = iig.group_id AND igtm.periode = '$akhir'
					LEFT JOIN item_target_marketing itm ON itm.item_id = iig.id AND itm.periode = '$akhir'
					WHERE group_id != 99
					HAVING totalnominal1_ <> 0 OR jmlnc <> 0 OR qty_group <> 0
					ORDER BY group_id, newdata.id,newdata.nama";

		$qry = $this->db->query($query);
		 //echo $this->db->last_query();
		
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }

}
?>