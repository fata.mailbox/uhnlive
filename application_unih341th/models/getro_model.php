    <?php



defined('BASEPATH') OR exit('No direct script access allowed');

class getro_model extends CI_Model {
	var $table = 'ro_staging'; 
    var $column_order = array('U_UHINV');
    var $column_search = array('U_UHINV','U_POSNR','U_SUBNR','U_KUNNR','U_KUNNR','U_CTYPE','U_PL_WE','VKBUR','WERKS','MATNR','ARKTX','MATKL','CHARG','KWMENG','COGS','ZUHN','YYBANDCD'); 
    var $order = array('U_UHINV' => 'ASC');
    
    public function __construct()
    {
        parent::__construct();
        
    }

    private function _get_datatables_query($id,$act,$param)
    {
        
        $this->db->where('U_UHINV', $id);
        
        
        if($act == 'RequestOrder'){
            $this->db->where('U_AUART','ZU01');    
        }else if($act == 'SalesOrder'){
            $this->db->where('U_AUART','ZU02');
        }
        else if($act == 'SC'){
            $this->db->where('U_AUART','ZU03');
        }else if($act == 'SCPayment'){
            $this->db->where('U_AUART','ZU05');
        }else if($act == 'SCRetur'){
            $this->db->where('U_AUART','ZU04');
        }else if($act == 'RORetur'){
            $this->db->where('U_AUART','ZU07');
        }else if($act == 'ScAdmin'){
            $this->db->where('U_AUART','ZU03');
        }else if($act == 'ScAdminRetur'){
            $this->db->where('U_AUART','ZU04');
        }else if ($act == 'CancelBilling') {
            $this->db->where('U_AUART','ZU06');
            if ($param == 'SalesOrder') {
                $this->db->where('U_REF_CNCL_AUART','ZU02');
            }else{
                $this->db->where('U_REF_CNCL_AUART','ZU01');
            }
        }
        
        
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i) 
                 $this->db->group_end(); 
            }
            $i++;
        }
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    public function get_datatables($id,$act,$param)
    {
        $this->_get_datatables_query($id,$act,$param);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
    
    
        if($act == 'RequestOrder'){
            $this->db->where('U_AUART','ZU01');    
        }else if($act == 'SalesOrder'){
            $this->db->where('U_AUART','ZU02');
        }
        else if($act == 'SC'){
            $this->db->where('U_AUART','ZU03');
        }else if($act == 'SCPayment'){
            $this->db->where('U_AUART','ZU05');
        }else if($act == 'SCRetur'){
            $this->db->where('U_AUART','ZU04');
        }else if($act == 'RORetur'){
            $this->db->where('U_AUART','ZU07');
        }else if($act == 'ScAdmin'){
            $this->db->where('U_AUART','ZU03');
        }else if($act == 'ScAdminRetur'){
            $this->db->where('U_AUART','ZU04');
        }else if ($act == 'CancelBilling') {
            $this->db->where('U_AUART','ZU06');
            if ($param == 'SalesOrder') {
                $this->db->where('U_REF_CNCL_AUART','ZU02');
            }else{
                $this->db->where('U_REF_CNCL_AUART','ZU01');
            }
        }
    
    
        $this->db->where('U_UHINV', $id);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function count_filtered($id,$act,$param)
    {
        $this->_get_datatables_query($id,$act,$param);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($id,$act)
    {
        
     
        if($act == 'RequestOrder'){
            $this->db->where('U_AUART','ZU01');    
        }else if($act == 'SalesOrder'){
            $this->db->where('U_AUART','ZU02');
        }
        else if($act == 'SC'){
            $this->db->where('U_AUART','ZU03');
        }else if($act == 'SCPayment'){
            $this->db->where('U_AUART','ZU05');
        }else if($act == 'SCRetur'){
            $this->db->where('U_AUART','ZU04');
        }else if($act == 'RORetur'){
            $this->db->where('U_AUART','ZU07');
        }else if($act == 'ScAdmin'){
            $this->db->where('U_AUART','ZU03');
        }else if($act == 'ScAdminRetur'){
            $this->db->where('U_AUART','ZU04');
        }else if ($act == 'CancelBilling') {
            $this->db->where('U_AUART','ZU06');
            if ($param == 'SalesOrder') {
                $this->db->where('U_REF_CNCL_AUART','ZU02');
            }else{
                $this->db->where('U_REF_CNCL_AUART','ZU01');
            }
        }
     
         $this->db->where('U_UHINV', $id);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
}

/* End of file wallet_model.php */
