<?php
/**
* 
*/
class MReset extends CI_Model
{
    
    function __construct()
    {
        parent::__construct();
    }
    private function _getResetPin(){
        $angka_master = '0123456789012345678901234567890';
        
        $angka = '';
        for ($i=0; $i<6; $i++) {
            $angka .= $angka_master{rand(0,30)};
        }
        
        return $var = $angka;
    }
    private function _getResetPassword(){
        $angka_master = '01234567890abcdefghjkmnpqrstuvwxyz';
        
        $angka = '';
        for ($i=0; $i<8; $i++) {
            $angka .= $angka_master{rand(0,30)};
        }
        
        return $var = $angka;
    }
    
    public function addResetMember(){
        $member_id = $this->input->post('member_id');
        $empid = $this->session->userdata('username');
        
        $hash = $this->_getHash($member_id);

        $data = array(
				'id' => 'NULL',
                'member_id' => $member_id,
                //'member'  => $this->input->post('member'),
                'remark'  => $this->input->post('remark'),
                'created' => date('Y-m-d H:i:s',now()),
                'createdby' => $empid
            );  
        
        if($this->input->post('passwd') && $this->input->post('pin')){
            $password = $this->_getResetPassword();
            $pin = $this->_getResetPin();
            
            $password_enc = substr(sha1(sha1(md5($hash->hash.$password))),5,15);
            $pin_enc = substr(sha1(sha1(md5($hash->hash.$pin))),3,7);
            
            $this->db->update('users',array('password'=>$password_enc,'pin'=>$pin_enc),array('id'=>$member_id));
        }else{
            if($this->input->post('passwd')){
                $password = $this->_getResetPassword();
                              
                $password_enc = substr(sha1(sha1(md5($hash->hash.$password))),5,15);
                $this->db->update('users',array('password'=>$password_enc),array('id'=>$member_id));
            }else{
                $pin = $this->_getResetPin();
            
                $pin_enc = substr(sha1(sha1(md5($hash->hash.$pin))),3,7);
                $this->db->update('users',array('pin'=>$pin_enc),array('id'=>$member_id));
            }
        }
        $this->db->insert('reset_member',$data);
    }
    private function _getHash($id){
        $q=$this->db->select("hash",false)
            ->from('users')
            ->where('id',$id)
            ->get();
        return ($q->num_rows() > 0)? $q->row() : false;    
    }

    public function searchResetMember($keywords=0,$num,$offset){
        
        $data = array();
        $this->db->select('rm.*');
        $this->db->from('reset_member rm');
        $this->db->join('member m','m.id= rm.member_id','left');
        $this->db->like('rm.member_id', $keywords);
        $this->db->like('m.nama', $keywords);     
               
        $this->db->order_by('member_id','asc');
        $this->db->limit($num,$offset);

        $q = $this->db->get();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
        
    }
    public function countResetMember($keywords=0){
        $this->db->select('rm.*');
        $this->db->from('reset_member rm');
        $this->db->join('member m','m.id= rm.member_id','left');
        $this->db->like('rm.member_id', $keywords);
        $this->db->like('m.nama', $keywords);     

        return $this->db->count_all_results();
    }

}
?>