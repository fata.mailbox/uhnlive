<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adjmove_model extends CI_Model {
	var $table = 'temp_materialncm'; 
    var $column_order = array('YMBLNR_UHN','STATUS','BUDAT_MKPF');
    var $column_search = array('YMBLNR_UHN','STATUS','BUDAT_MKPF'); 
    var $order = array('YMBLNR_UHN' => 'ASC');

	  private function _get_datatables_query($status,$from,$to)
	  {

		$this->db->select('distinct(YMBLNR_UHN),STATUS,BUDAT_MKPF');
		if (empty($from)) {
			$this->db->where('ACT', 'ADJ MOVE');
		}else {

			if ($status == 'all') {
				$this->db->where('ACT', 'ADJ MOVE');
			$this->db->where('BUDAT_MKPF >=', $from);
			$this->db->where('BUDAT_MKPF <=', $to);	
			}else {
				$this->db->where('ACT', 'ADJ MOVE');
				$this->db->where('STATUS', $status);
			$this->db->where('BUDAT_MKPF >=', $from);
			$this->db->where('BUDAT_MKPF <=', $to);
			}
			
		}
		
		$this->db->from($this->table);
		$this->db->group_by("YMBLNR_UHN"); 
		$this->db->order_by('BUDAT_MKPF', 'DESC');	
		  $i = 0;
		  foreach ($this->column_search as $item) // looping awal
		  {
			  if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
			  {
				  if($i===0) // looping awal
				  {
					  $this->db->group_start(); 
					  $this->db->like($item, $_POST['search']['value']);
				  }
				  else
				  {
					  $this->db->or_like($item, $_POST['search']['value']);
				  }
				  if(count($this->column_search) - 1 == $i) 
				   $this->db->group_end(); 
			  }
			  $i++;
		  }
		  if(isset($_POST['order'])) 
		  {
			  $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		  } 
		  else if(isset($this->order))
		  {
			  $order = $this->order;
			  $this->db->order_by(key($order), $order[key($order)]);
		  }
	  }
	  public function get_datatables($status,$from,$to)
	  {
		  $this->_get_datatables_query($status,$from,$to);
		  if($_POST['length'] != -1)
		  $this->db->limit($_POST['length'], $_POST['start']);
		  if (empty($from)) {
			$this->db->where('ACT', 'ADJ MOVE');
		}else {
			if ($status == 'all') {
				$this->db->where('ACT', 'ADJ MOVE');
			$this->db->where('BUDAT_MKPF >=', $from);
			$this->db->where('BUDAT_MKPF <=', $to);	
			}else {
				$this->db->where('ACT', 'ADJ MOVE');
				$this->db->where('STATUS', $status);
			$this->db->where('BUDAT_MKPF >=', $from);
			$this->db->where('BUDAT_MKPF <=', $to);
			}
		}
		  $query = $this->db->get();
		  return $query->result_array();
	  }
	  public function count_filtered($status,$from,$to)
	  {
		  $this->_get_datatables_query($status,$from,$to);
		  $query = $this->db->get();
		  return $query->num_rows();
	  }
   
	  public function count_all($status,$from,$to)
	  {
		if (empty($from)) {
			$this->db->where('ACT', 'ADJ MOVE');
		}else {
			if ($status == 'all') {
				$this->db->where('ACT', 'ADJ MOVE');
			$this->db->where('BUDAT_MKPF >=', $from);
			$this->db->where('BUDAT_MKPF <=', $to);	
			}else {
				$this->db->where('ACT', 'ADJ MOVE');
				$this->db->where('STATUS', $status);
			$this->db->where('BUDAT_MKPF >=', $from);
			$this->db->where('BUDAT_MKPF <=', $to);
			}
		}
		  $this->db->from($this->table);
		  return $this->db->count_all_results();
	  }
  
}

/* End of file topupbydemand_model.php */
/* Location: ./application_unih341th/models/topupbydemand_model.php */