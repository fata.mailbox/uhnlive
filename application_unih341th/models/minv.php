<?php
class MInv extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Form Penerimaan Barang / inventory IN
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-03-30
    |
    */
    public function searchFPB($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.invoiceno,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.createdby",false);
        $this->db->from('inventoryin a');
        $this->db->like('a.id', $keywords, 'after');
        $this->db->or_like('a.invoiceno', $keywords, 'after');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countFPB($keywords=0){
        $this->db->like('id', $keywords, 'after');
        $this->db->or_like('invoiceno', $keywords, 'after'); 
        $this->db->from('inventoryin');
        return $this->db->count_all_results();
    }
    public function addInventoryIn(){
       $data = array(
            'date' => date('Y-m-d',now()),
            'invoiceno' => $this->db->escape_str($this->input->post('noinvoice')),
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'event_id' => 'IV1',
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $this->session->userdata('user')
            );
       $this->db->insert('inventoryin',$data);
       
       $inventoryin_id = $this->db->insert_id();
       
       $qty0 = str_replace(".","",$this->input->post('qty0'));
       if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'inventoryin_id' => $inventoryin_id,
                'item_id' => $this->input->post('itemcode0'),
                'harga' => str_replace(".","",$this->input->post('price0')),
                'qty' => $qty0
            );
            
            $this->db->insert('inventoryin_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'inventoryin_id' => $inventoryin_id,
                'item_id' => $this->input->post('itemcode1'),
                'harga' => str_replace(".","",$this->input->post('price1')),
                'qty' => $qty1
            );
            
            $this->db->insert('inventoryin_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'inventoryin_id' => $inventoryin_id,
                'item_id' => $this->input->post('itemcode2'),
                'harga' => str_replace(".","",$this->input->post('price2')),
                'qty' => $qty2
            );
            
            $this->db->insert('inventoryin_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'inventoryin_id' => $inventoryin_id,
                'item_id' => $this->input->post('itemcode3'),
                'harga' => str_replace(".","",$this->input->post('price3')),
                'qty' => $qty3
            );
            
            $this->db->insert('inventoryin_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
       if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'inventoryin_id' => $inventoryin_id,
                'item_id' => $this->input->post('itemcode4'),
                'harga' => str_replace(".","",$this->input->post('price4')),
                'qty' => $qty4
            );
            
            $this->db->insert('inventoryin_d',$data);
       }
       $empid = $this->session->userdata('user');
       $this->db->query("call sp_inventoryin('$inventoryin_id','$empid')");
    }
    public function getFPB($id=0){
        $data=array();
        $q=$this->db->get_where('inventoryin',array('id'=>$id));
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getFPBDetail($id=0){
        $data = array();
        $q=$this->db->select("d.item_id,format(d.qty,0)as qty,format(d.harga,0)as fharga,a.name",false)
            ->from('inventoryin_d d')
            ->join('item a','d.item_id=a.id','left')
            ->where('d.inventoryin_id',$id)
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Adjustment Stock Warehouse
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-04
    |
    */
    public function searchAdjustment($keywords=0,$num,$offset){
        $data = array();
		$whsid = $this->session->userdata('whsid');
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' ) and a.warehouse_id = ".$this->session->userdata('keywords_whsid');
			}
		}
		
        $this->db->select("a.id,b.name,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.flag,a.createdby,b.name as warehouse_name, ifnull(date_format(a.tglapproved,'%d-%b-%Y %T'),'-')as appdate,a.status,ifnull(a.approvedby,'-') as approvedby",false);
        $this->db->from('adjustment a');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
        //$this->db->like('a.id', $keywords, 'after');
        //$this->db->or_like('b.name', $keywords, 'after');
		$this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countAdjustment($keywords=0){
		$whsid = $this->session->userdata('whsid');
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' ) and a.warehouse_id = ".$this->session->userdata('keywords_whsid');
			}
		}		
        $this->db->from('adjustment a');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
        //$this->db->like('a.id', $keywords, 'after');
        //$this->db->or_like('b.name', $keywords, 'after'); 
		$this->db->where($where);
        return $this->db->count_all_results();
    }
	
    public function getDropDownWhsAll($all){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            if($all == 'all')$data['all']='All Cabang';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
	
    public function addAdjustment(){
        $flag = $this->input->post('flag');
        $warehouse_id = $this->input->post('warehouse_id');
       $data = array(
            'date' => date('Y-m-d',now()),
            'warehouse_id' => $warehouse_id,
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'flag' => $flag,
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $this->session->userdata('user')
            );
       $this->db->insert('adjustment',$data);
       
       $id = $this->db->insert_id();
       
       $qty0 = str_replace(".","",$this->input->post('qty0'));
       if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'adjustment_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0
            );
            
            $this->db->insert('adjustment_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'adjustment_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1
            );
            
            $this->db->insert('adjustment_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'adjustment_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2
            );
            
            $this->db->insert('adjustment_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'adjustment_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3
            );
            
            $this->db->insert('adjustment_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
       if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'adjustment_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4
            );
            
            $this->db->insert('adjustment_d',$data);
       }
       $empid = $this->session->userdata('user');
       $this->db->query("call sp_adjustment('$id','$warehouse_id','$flag','$empid')");
    }
    public function getAdjustment($id=0){
        $data=array();
        $this->db->select("a.id,b.name,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.flag,a.createdby",false);
        $this->db->from('adjustment a');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getAdjustmentDetail($id=0){
        $data = array();
        $q=$this->db->select("d.item_id,format(d.qty,0)as qty,a.name",false)
            ->from('adjustment_d d')
            ->join('item a','d.item_id=a.id','left')
            ->where('d.adjustment_id',$id)
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getAdjustmentRusak($id=0){
        $data=array();
        $this->db->select("a.id,b.nama,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.flag,a.createdby",false);
        $this->db->from('adjusment_rusak a');
        $this->db->join('master_rusak b','a.id_whr=b.id','left');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getAdjustmentRusakDetail($id=0){
        $data = array();
        $q=$this->db->select("d.item_id,format(d.qty,0)as qty,a.name",false)
            ->from('adjusment_rusak_d d')
            ->join('item a','d.item_id=a.id','left')
            ->where('d.id_adj_rusak',$id)
            ->get();
            
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getDropDownWhs(){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | No Comercial
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-04
    |
    */
    public function searchNCM($keywords=0,$num,$offset){
        $data = array();
		$whsid = $this->session->userdata('whsid');
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' ) and a.warehouse_id = ".$this->session->userdata('keywords_whsid');
			}
		}
		
        $this->db->select("a.id,b.name,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.createdby,b.name as warehouse_name, ifnull(date_format(a.tglapproved,'%d-%b-%Y %T'),'-')as appdate,a.status,ifnull(a.approvedby,'-') as approvedby",false);
        $this->db->from('ncm a');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
        //$this->db->like('a.id', $keywords, 'after');
        //$this->db->or_like('b.name', $keywords, 'after');
		$this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countNCM($keywords=0){
		$whsid = $this->session->userdata('whsid');
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' ) and a.warehouse_id = ".$this->session->userdata('keywords_whsid');
			}
		}
		
        $this->db->from('ncm a');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
        //$this->db->like('a.id', $keywords, 'after');
        //$this->db->or_like('b.name', $keywords, 'after');
		$this->db->where($where);
        return $this->db->count_all_results();
    }
    public function addNCM(){
        $warehouse_id = $this->input->post('warehouse_id');
       $data = array(
            'date' => date('Y-m-d',now()),
            'warehouse_id' => $warehouse_id,
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $this->session->userdata('user')
            );
       $this->db->insert('ncm',$data);
       
       $id = $this->db->insert_id();
       
       $qty0 = str_replace(".","",$this->input->post('qty0'));
       if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'ncm_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0
            );
            
            $this->db->insert('ncm_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'ncm_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1
            );
            
            $this->db->insert('ncm_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'ncm_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2
            );
            
            $this->db->insert('ncm_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'ncm_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3
            );
            
            $this->db->insert('ncm_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
       if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'ncm_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4
            );
            
            $this->db->insert('ncm_d',$data);
       }
       $empid = $this->session->userdata('user');
       $this->db->query("call sp_ncm('$id','$warehouse_id','$empid')");
    }
    public function getNCM($id=0){
        $data=array();
        $this->db->select("a.id,b.name,date_format(a.date,'%d-%b-%Y')as date,a.remark,a.createdby",false);
        $this->db->from('ncm a');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getNCMDetail($id=0){
        $data = array();
        $q=$this->db->select("d.item_id,format(d.qty,0)as qty,a.name",false)
            ->from('ncm_d d')
            ->join('item a','d.item_id=a.id','left')
            ->where('d.ncm_id',$id)
            ->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
   
    public function adjustmentApproved(){
        if($this->input->post('p_id')){
            $row = array();
            
            $empid = $this->session->userdata('user');
            $idlist = implode(",",array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";
            
            $option = $where. " and status = 'pending'";
            $row = $this->_countAdjustmentApproved($option);
            $remarkapp=$this->db->escape_str($this->input->post('remark'));
            
            if($row){
                $data = array(
                    'status'=> 'delivery',
                    'remarkapp'=>$remarkapp,
                    'approvedby'=>$this->session->userdata('user'),
                    'tglapproved' => date('Y-m-d H:i:s', now())
                );
                $this->db->update('adjustment',$data,$option);
                
                $this->session->set_flashdata('message','Delivery approved successfully');
            }else{
                $this->session->set_flashdata('message','Nothing to delevery approved!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing to delevery approved!');
        }
    }
    protected function _countAdjustmentApproved($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('adjustment');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
    public function ncmApproved(){
        if($this->input->post('p_id')){
            $row = array();
            
            $empid = $this->session->userdata('user');
            $idlist = implode(",",array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";
            
            $option = $where. " and status = 'pending'";
            $row = $this->_countNcmApproved($option);
            $remarkapp=$this->db->escape_str($this->input->post('remark'));
            
            if($row){
                $data = array(
                    'status'=> 'delivery',
                    'remarkapp'=>$remarkapp,
                    'approvedby'=>$this->session->userdata('user'),
                    'tglapproved' => date('Y-m-d H:i:s', now())
                );
                $this->db->update('ncm',$data,$option);
                
                $this->session->set_flashdata('message','Delivery approved successfully');
            }else{
                $this->session->set_flashdata('message','Nothing to delevery approved!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing to delevery approved!');
        }
    }
    protected function _countNcmApproved($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('ncm');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
}?>