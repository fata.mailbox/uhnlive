<?php
class MPromo extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
	public function getDataPribadi($id){
		$data = array();
		$qry = $this->db->select("m.id, m.nama, m.apgs, m.ipgs, m.psbak",false)->from('member m')->where(array('m.id'=>$id))->get();
		//echo $this->db->last_query();
		if($qry->num_rows()>0){
            	$data=$qry->row_array();
        	}
		$qry->free_result();
		return $data;
	}
	public function getPlusPoin($id){
		$data = array();
		$qry = $this->db->query("
		SELECT member_id, SUM(totalpv)AS totalpv, SUM(pv_lama)AS pv_lama, SUM(pv_baru)AS pv_baru, SUM(curr_pv)AS curr_pv
		FROM(
			SELECT so.member_id, so.totalpv, IFNULL(up.pv_lama,0)AS pv_lama, IFNULL(up.pv_baru,0)AS pv_baru,
				(so.totalpv - IFNULL(up.pv_lama,0) + IFNULL(up.pv_baru,0))as curr_pv
			FROM so 
			LEFT JOIN update_pv up ON so.id = up.so_id
			WHERE 
				MONTH(so.tgl)>8 
				AND MONTH(so.tgl)<12
				AND so.member_id = '".$id."' 
		)AS databaru
		GROUP BY member_id
		");
		//echo $this->db->last_query();
		if($qry->num_rows()>0){$data=$qry->row_array();}
		$qry->free_result();
		return $data;
	}
	public function getTopThree($id){
		$data = array();
		$qry = $this->db->select("sp.id , sp.nama, sp.apgs, sp.ipgs, (sp.apgs - sp.ipgs)as sorting ",false)
			->from('member m')
			->join('member sp','sp.sponsor_id = m.id', left)
			->where(array('m.id'=>$id))
			->order_by('sorting', 'desc')
			->get();
		//echo $this->db->last_query();
		if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
	}
	
	/*Created by Boby 2010-06-07 UIG*/
	public function uigResult($id){
		$data = array();
		$qry = $this->db->query("
			SELECT member_id, pvg, b2+b3+b4+b5+b6+b7+b8+b9 AS pv
			FROM z_uig_result
			WHERE member_id = '$id'
		");
		//echo $this->db->last_query();
		if($qry->num_rows()>0){$data=$qry->row_array();}
		$qry->free_result();
		return $data;
	}
	
	
	public function uigDet($id){
		$data = array();
		$qry = $this->db->query("
			SELECT z.member_id as id, z.pvg as pvg_true, m.nama
			FROM z_uig_result z
			LEFT JOIN member m ON z.member_id = m.id
			WHERE z.upline_id = '$id'
			ORDER by z.pvg DESC
		");
		//echo $this->db->last_query();
		$i=0;
		if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            	$i++;
                $row['i'] = $i;
                $data[]=$row;
				
			}
        }
		$qry->free_result();
		return $data;
	}
	
	public function currShop($id){
		$data = array();
		$tglawal = date('Y-m-')."01";
		$tglakhir = date('Y-m-')."31";
		
		$qry = $this->db->query("
			SELECT IFNULL(SUM(totalpv), 0) AS pv 
			FROM (so) 
			WHERE `member_id` = '$id' AND `tgl` BETWEEN '$tglawal' AND '$tglakhir' 
		");
		//echo $this->db->last_query();
		if($qry->num_rows()>0){$data=$qry->row_array();}
		$qry->free_result();
		return $data;
	}
	/*End created by Boby 2010-06-07*/
	
	/* Created by Boby 2010-12-10 */
	public function uigResult2($id){
		$data = array();
		$qry = $this->db->query("
			SELECT *
			FROM z_uig2_result
			WHERE member_id = '$id'
		");
		//echo $this->db->last_query();
		if($qry->num_rows()>0){$data=$qry->row_array();}
		$qry->free_result();
		return $data;
	}
	
	
	public function uigDet2($id){
		$data = array();
		$qry = $this->db->query("
			SELECT z.member_id as id, m.nama, z.totalpv
			FROM z_uig2_result z
			LEFT JOIN member m ON z.member_id = m.id
			WHERE z.upline_id = '$id'
			ORDER by z.totalpv DESC
		");
		//echo $this->db->last_query();
		$i=0;
		if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            	$i++;
                $row['i'] = $i;
                $data[]=$row;
				
			}
        }
		$qry->free_result();
		return $data;
	}
	/* End created by Boby 2010-12-10 */
	
	/* Created by Boby 2011-05-16 */
	public function uigResult3($id){
		$data = array();
		$qry = $this->db->query("
			SELECT *
			FROM z_201101_uig3
			WHERE member_id = '$id'
		");
		//echo $this->db->last_query();
		if($qry->num_rows()>0){$data=$qry->row_array();}
		$qry->free_result();
		return $data;
	}
	
	
	public function uigDet3($id){
		$data = array();
		$qry = $this->db->query("
			SELECT z.member_id as id, m.nama, z.pvg as totalpv
			FROM z_201101_uig3 z
			LEFT JOIN member m ON z.member_id = m.id
			WHERE m.sponsor_id = '$id'
			ORDER by z.pvg DESC
		");
		//echo $this->db->last_query();
		$i=0;
		if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            	$i++;
                $row['i'] = $i;
                $data[]=$row;
				
			}
        }
		$qry->free_result();
		return $data;
	}
	/* End created by Boby 2011-05-16 */
	
	/*created by edwin 2011-07-21*/
	public function compress($id){
		$data = array();
		$qry = $this->db->query("
		SELECT m.id AS id, m.nama AS nama, com.pvg as totalpv
		FROM member m
		LEFT JOIN z_201106_compress_d com ON m.id=com.from_id 
		LEFT JOIN member s ON com.for_id=s.id
		WHERE com.periode='2011-06-30'
		AND com.levelg=1 AND com.ps>0 and com.for_id = '$id'
		ORDER by com.pvg DESC
	");
	$i=0;
		if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            	$i++;
                $row['i'] = $i;
                $data[]=$row;
				
			}
        }
		$qry->free_result();
		return $data;
	}
	/*----end----*/
}
?>