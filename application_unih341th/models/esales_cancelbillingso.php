<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class esales_cancelbillingso extends CI_Model {
	var $table = 'so_staging'; 
    var $column_order = array('U_UHINV','status','U_SODAT');
    var $column_search = array('U_UHINV','status','U_SODAT'); 
    var $order = array('U_SODAT' => 'DESC');

	  private function _get_datatables_query($status,$from,$to)
	  {
		$this->db->select('U_UHINV,status,U_SODAT');
		if (empty($from)) {

		}else {
			
			if ($status == 'all') {
			$this->db->where("STR_TO_DATE(U_SODAT,'%d.%m.%Y') >=", $from);
			$this->db->where("STR_TO_DATE(U_SODAT,'%d.%m.%Y') <=", $to);	
			}else {
			$this->db->where("STR_TO_DATE(U_SODAT,'%d.%m.%Y') >=", $from);
			$this->db->where("STR_TO_DATE(U_SODAT,'%d.%m.%Y') <=", $to);	
			$this->db->where('status', $status);
			}
		}
		$this->db->where('U_AUART','ZU06');
		$this->db->from($this->table);
		$this->db->group_by("U_UHINV"); 
		$this->db->order_by('U_SODAT', 'DESC');	
	    $i = 0;
		  foreach ($this->column_search as $item) // looping awal
		  {
			  if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
			  {
				  if($i===0) // looping awal
				  {
					  $this->db->group_start(); 
					  $this->db->like($item, $_POST['search']['value']);
				  }
				  else
				  {
					  $this->db->or_like($item, $_POST['search']['value']);
				  }
				  if(count($this->column_search) - 1 == $i) 
				   $this->db->group_end(); 
			  }
			  $i++;
		  }
		  if(isset($_POST['order'])) 
		  {
			  $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		  } 
		  else if(isset($this->order))
		  {
			  $order = $this->order;
			  $this->db->order_by(key($order), $order[key($order)]);
		  }
	  }
	  public function get_datatables($status,$from,$to)
	  {
		  $this->_get_datatables_query($status,$from,$to);
		  if($_POST['length'] != -1)
		  $this->db->limit($_POST['length'], $_POST['start']);
		  if (empty($from)) {

		}else {
			if ($status == 'all') {
					$this->db->where("STR_TO_DATE(U_SODAT,'%d.%m.%Y') >=", $from);
			$this->db->where("STR_TO_DATE(U_SODAT,'%d.%m.%Y') <=", $to);	
				}else {
					$this->db->where("STR_TO_DATE(U_SODAT,'%d.%m.%Y') >=", $from);
			$this->db->where("STR_TO_DATE(U_SODAT,'%d.%m.%Y') <=", $to);
				$this->db->where('status', $status);
				}
		}
		  $this->db->where('U_AUART','ZU06');
		  $query = $this->db->get();
		  $this->db->order_by('U_SODAT', 'DESC');
		  return $query->result_array();
	  }
	  public function count_filtered($status,$from,$to)
	  {
		  $this->_get_datatables_query($status,$from,$to);
		  $query = $this->db->get();
		  return $query->num_rows();
	  }
   
	  public function count_all($status,$from,$to)
	  {
		if (empty($from)) {

		}else {
			if ($status == 'all') {
				$this->db->where("STR_TO_DATE(U_SODAT,'%d.%m.%Y') >=", $from);
			$this->db->where("STR_TO_DATE(U_SODAT,'%d.%m.%Y') <=", $to);
				}else {
				$this->db->where("STR_TO_DATE(U_SODAT,'%d.%m.%Y') >=", $from);
			$this->db->where("STR_TO_DATE(U_SODAT,'%d.%m.%Y') <=", $to);	
				$this->db->where('status', $status);
				}
		}
		
		  $this->db->where('U_AUART','ZU06');
		  $this->db->from($this->table);
		  return $this->db->count_all_results();
	  }
  
}

/* End of file esales_cancelbillingso.php */
/* Location: ./application_unih341th/models/topupbydemand_model.php */