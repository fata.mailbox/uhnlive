<?php
class Maccountvalidation extends CI_Model{
    function __construct(){
        parent::__construct();
    }
	
	public function getAccVal($keyword, $offset, $num, $type){
        $data = array();
		if($num){$num=$num.', ';}
		
		if($type==1){$qry = 'AND flag = 0 ';}
		else if($type==2){$qry = 'AND flag <> 0 ';}
		else{$qry = ' ';}
		
		$query = "
			SELECT *
			FROM(
				SELECT id, acc_no, nama
					, CASE WHEN flag = 0 AND flag_sys = 2 THEN FORMAT(2500,0)
						WHEN flag = 0 AND flag_sys = 1 THEN FORMAT(0,0)
						WHEN flag = 2 THEN FORMAT(2500,0) 
						WHEN flag = 1 THEN FORMAT(0,0) 
						ELSE 'n/a' END AS flag
					, CASE WHEN flag = 0 AND flag_sys = 2 THEN 2500
						WHEN flag = 0 AND flag_sys = 1 THEN 0
						WHEN flag = 2 THEN 2500
						WHEN flag = 1 THEN 0
						ELSE '-' END AS flag_
					, CASE 
						WHEN updated = '0000-00-00 00:00:00' THEN created 
						ELSE updated 
					END AS created
					, CASE 
						WHEN updatedby = '0' THEN createdby
						ELSE updatedby
					END AS createdby
					, CASE 
						WHEN updatedby = '0' THEN 1
						ELSE 0
					END AS need
				FROM account_update
				WHERE 
				(nama LIKE '%$keyword%' OR acc_no LIKE '%$keyword%'	OR createdby LIKE '%$keyword%' OR updatedby LIKE '%$keyword%')
				".$qry."
				LIMIT $num $offset
			)AS dt
			ORDER BY created DESC
		";
		$qry = $this->db->query($query);
		// echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		// $data['jml'] = $qry->num_rows();
		$qry->free_result();
		return $data;
    }
	public function countAccVal($keyword, $type){
        $data = array();
		
		if($type==1){$qry = 'AND flag = 0 ';}
		else if($type==2){$qry = 'AND flag <> 0 ';}
		else{$qry = ' ';}
		
		$query = "
			SELECT acc_no, nama
				, CASE WHEN flag = 2 THEN 2500 WHEN flag = 1 THEN 0 ELSE 'n/a' END AS flag
				, created, createdby
			FROM account_update
			WHERE (nama LIKE '%$keyword%' OR acc_no LIKE '%$keyword%' OR createdby LIKE '%$keyword%' OR updatedby LIKE '%$keyword%')
			".$qry."
		";
		$qry = $this->db->query($query);
		// echo $this->db->last_query();
		$a = $qry->num_rows();
		$qry->free_result();
		return $a;
    }
	
	public function getValType(){
        $data = array();
		$data[1] = 'Rp.0,-';
		$data[2] = 'Rp.2.500,-';
        return $data;
    }
	
	public function updateAccVal($id, $ch){
		$empId = $this->session->userdata('username');
		$this->db->query("UPDATE account_update SET flag = '$ch', updated = NOW(), updatedby = '$empId' WHERE acc_no = '$id'; ");
		$this->db->query("UPDATE account SET flag = '$ch' WHERE bank_id = 'BCA' AND `no` = '$id'; ");
    }
	
	public function createAccount(){
		$norek = $this->input->post('norek');
		$nama = $this->input->post('nama');
		$nama = strtoupper($nama);
		$type = $this->input->post('type');
		$empid = $this->session->userdata('user');
		
		// INSERT INTO account_update(nama, acc_no, flag, createdby) VALUES('A. Cicillia Tjandra, SH', '8730055499', 2, 'system')
		$data = array(
            'nama' => $nama,
            'acc_no'=>$norek,
            'flag' => $type,
            'created'=>date('Y-m-d H:i:s',now()),
			'createdby'=>$empid,
            'updated'=>date('Y-m-d H:i:s',now()),            
            'updatedby'=>$empid
        );
        $this->db->insert('account_update',$data);
        // $this->db->query("UPDATE account SET flag = '$type', nama = '$nama', updated = NOW(), updatedby = '$empId' WHERE bank_id = 'BCA' AND `no` = '$norek'; ");
        $this->db->query("CALL sp_account_validation_add('$norek', '$nama', '$type', '$empid') ");
    }
	
	public function getExistAcc($keyword){
        $data = array();
		$query = "
			SELECT acc_no
			FROM account_update
			WHERE acc_no = '$keyword'
		";
		$qry = $this->db->query($query);
		// echo $this->db->last_query();
		$a = $qry->num_rows();
		$qry->free_result();
		if($a){return true;}
		else{return false;}
    }
	
	public function getSearchType(){
        $data = array();
		$data[0] = '-- === All === --';
		$data[1] = '-= Newest data =-';
		$data[2] = 'Exist Valid Data ';
        return $data;
    }
}
?>