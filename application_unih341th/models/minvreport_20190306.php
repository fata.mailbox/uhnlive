<?php
class MInvreport extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | report QOH - Stock Card - Stock Summary
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-08
    |
    */
    public function getQOH($whsid = 1){
        $data=array();
        $q=$this->db->select("i.id,i.name,format(s.qty,0)as stock",false)
            ->from('stock s')
            ->join('item i','s.item_id=i.id','left')
            ->where('s.warehouse_id',$whsid)
			->where('s.qty <> 0')
            ->order_by('i.name','asc')
            ->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getQOHperProd($whsid = 1){
        $data=array();
        $query="
		SELECT 
		id_ AS id
		, name_ AS name
		, FORMAT(SUM(stock),0) AS stock
		FROM
		(
			SELECT 
			-- i.id,i.name,format(s.qty,0)as stock
			i.id, i.name,
			CASE WHEN m.manufaktur_id  IS NOT NULL THEN m.item_id ELSE i.id END AS id_
			, CASE WHEN m.manufaktur_id  IS NOT NULL THEN im.name ELSE i.name END AS name_
			-- , CASE WHEN m.manufaktur_id  IS NOT NULL THEN FORMAT(m.qty*s.qty,0) ELSE FORMAT(s.qty,0) END AS stock
			, CASE WHEN m.manufaktur_id  IS NOT NULL THEN m.qty*s.qty ELSE s.qty END AS stock
			FROM stock s
			LEFT JOIN item i ON s.item_id=i.id
			LEFT JOIN manufaktur m ON m.manufaktur_id = s.item_id
			LEFT JOIN item im ON im.id = m.item_id
			WHERE s.warehouse_id = ".$whsid."
			AND s.qty <> 0
			ORDER BY i.name ASC
		) AS dt
		GROUP BY dt.id_
		ORDER BY dt.name_
		";
		
		$q = $this->db->query($query);
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
    public function getStockSummary($whsid = 1,$fromdate,$todate){
        $data=array();
        $where = "s.warehouse_id = '$whsid' and (s.tgl between '$fromdate' and '$todate') ";
        $q=$this->db->select("s.item_id,s.tgl,i.name,format(s.saldoawal,0)as fsaldoawal,format(sum(s.saldoin),0)as fsaldoin,format(sum(s.saldoout),0)as fsaldoout,format((s.saldoawal+sum(s.saldoin))-sum(s.saldoout),0)as fsaldoakhir", false)
            ->from('stock_summary s')
            ->join('item i','s.item_id=i.id','left')
            ->where($where)
            ->group_by('s.item_id')
            ->order_by('s.tgl','asc')
            ->order_by('i.name','asc')
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getStockCard($whsid = 1,$itemid,$fromdate,$todate){
        $data=array();
        $where = array('s.warehouse_id'=>$whsid,'s.item_id'=>$itemid,'s.date >=' => $fromdate,'s.date <=' => $todate);
        $q=$this->db->select("
				concat(s.desc_id,' - ',s.description)as description,s.date,createdby, s.description as trxtype, s.desc_id as trxid
				,format(s.saldo_awal,0)as saldo_awal, s.saldo_awal as saldoawal
				,format(s.saldo_in,0)as saldo_in, s.saldo_in as saldoin
				,format(s.saldo_out,0)as saldo_out, s.saldo_out as saldoout
				,format(s.saldo_akhir,0)as saldo_akhir, s.saldo_akhir as saldoakhir
				", false)
            ->from('stock_card s')
            ->where($where)
            ->order_by('s.id','asc')
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getWarehouse(){
        $data = array();
        $q=$this->db->select("id,name",false)
            ->from('warehouse')
            ->order_by('id','asc')
            ->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[$row['id']] = $row['name'];
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function getDetRO($id){
        $data = array();
        $q=$this->db->select("s.no_stc, ms.nama as stc_name",false)
            ->from('ro a')
            ->join('stockiest s','a.member_id = s.id','left')
            ->join('member ms','s.id = ms.id','left')
            ->where('a.id = '.$id)
            ->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
               $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function getDetSO($id){
        $data = array();
        $q=$this->db->select("a.member_id, ms.nama as member_name",false)
            ->from('so a')
            ->join('member ms','a.member_id = ms.id','left')
            ->where('a.id = '.$id)
            ->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
               $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }


    public function getHandlingFeeRep($whsid = 1,$fromdate,$todate, $trxtype){
        $data=array();
		if($trxtype == 'ro'){
			$query="
			SELECT 
			ro.warehouse_id
			, w.name AS warehouse_name
			, rod.ro_id as inv_no
			, ro.date as inv_date
			, s.no_stc AS stc_id
			, mem.nama AS stc_name
			, i.id AS 'sku_id'
			, i.name AS 'sku_name'
			, CASE WHEN m.item_id IS NOT NULL THEN i2.id  ELSE i.id END AS item_id
			, CASE WHEN m.item_id IS NOT NULL THEN i2.name  ELSE i.name END AS item_name
			, CASE WHEN m.item_id IS NOT NULL THEN rod.qty*m.qty  ELSE rod.qty END AS qty
			, ro.remark
			FROM ro_d rod 
			LEFT JOIN ro ON rod.ro_id = ro.id
			LEFT JOIN stockiest s ON ro.member_id = s.id
			LEFT JOIN member mem ON mem.id = s.id
			LEFT JOIN manufaktur m ON m.manufaktur_id = rod.item_id AND m.manufaktur_id NOT IN ( SELECT id FROM item WHERE item.type_id =1)  
			LEFT JOIN item i ON i.id = rod.item_id
			LEFT JOIN item i2 ON i2.id = m.item_id
			LEFT JOIN warehouse w ON ro.warehouse_id = w.id
			WHERE ro.date BETWEEN '".$fromdate."' AND '".$todate."'
			AND rod.warehouse_id = ".$whsid."
			AND (rod.pv > 0
			OR i.type_id = 1)
			ORDER BY rod.ro_id
			";
		} else if($trxtype == 'sc'){
			$query="
			SELECT 
			ro.warehouse_id
			, w.name AS warehouse_name
			, rod.pinjaman_id AS inv_no
			, ro.tgl AS inv_date
			, s.no_stc AS stc_id
			, mem.nama AS stc_name
			, i.id AS 'sku_id'
			, i.name AS 'sku_name'
			, CASE WHEN m.item_id IS NOT NULL THEN i2.id  ELSE i.id END AS item_id
			, CASE WHEN m.item_id IS NOT NULL THEN i2.name  ELSE i.name END AS item_name
			, CASE WHEN m.item_id IS NOT NULL THEN rod.qty*m.qty  ELSE rod.qty END AS qty
			, ro.remark
			FROM pinjaman_d rod 
			LEFT JOIN pinjaman ro ON rod.pinjaman_id = ro.id
			LEFT JOIN stockiest s ON ro.stockiest_id = s.id
			LEFT JOIN member mem ON mem.id = s.id
			LEFT JOIN manufaktur m ON m.manufaktur_id = rod.item_id
			LEFT JOIN item i ON i.id = rod.item_id
			LEFT JOIN item i2 ON i2.id = m.item_id
			LEFT JOIN warehouse w ON ro.warehouse_id = w.id
			WHERE ro.tgl BETWEEN '".$fromdate."' AND '".$todate."'
			AND ro.warehouse_id = ".$whsid."
			AND rod.pv > 0
			ORDER BY rod.pinjaman_id
		";
		} else if($trxtype == 'retur'){
			$query="
			SELECT 
			ro.warehouse_id
			, w.name AS warehouse_name
			, rod.retur_titipan_id AS inv_no
			, ro.tgl AS inv_date
			, s.no_stc AS stc_id
			, mem.nama AS stc_name
			, i.id AS 'sku_id'
			, i.name AS 'sku_name'
			, CASE WHEN m.item_id IS NOT NULL THEN i2.id  ELSE i.id END AS item_id
			, CASE WHEN m.item_id IS NOT NULL THEN i2.name  ELSE i.name END AS item_name
			, CASE WHEN m.item_id IS NOT NULL THEN rod.qty*m.qty  ELSE rod.qty END AS qty
			, ro.remark
			FROM retur_titipan_d rod 
			LEFT JOIN retur_titipan ro ON rod.retur_titipan_id = ro.id
			LEFT JOIN stockiest s ON ro.stockiest_id = s.id
			LEFT JOIN member mem ON mem.id = s.id
			LEFT JOIN manufaktur m ON m.manufaktur_id = rod.item_id
			LEFT JOIN item i ON i.id = rod.item_id
			LEFT JOIN item i2 ON i2.id = m.item_id
			LEFT JOIN warehouse w ON ro.warehouse_id = w.id
			WHERE ro.tgl BETWEEN '".$fromdate."' AND '".$todate."'
			AND ro.warehouse_id = ".$whsid."
			AND rod.pv > 0
			ORDER BY rod.retur_titipan_id
		";
		} else if($trxtype == 'scretur'){
			$query="
			SELECT 
			ro.warehouse_id
			, w.name AS warehouse_name
			, rod.retur_pinjaman_id AS inv_no
			, ro.tgl AS inv_date
			, s.no_stc AS stc_id
			, mem.nama AS stc_name
			, i.id AS 'sku_id'
			, i.name AS 'sku_name'
			, CASE WHEN m.item_id IS NOT NULL THEN i2.id  ELSE i.id END AS item_id
			, CASE WHEN m.item_id IS NOT NULL THEN i2.name  ELSE i.name END AS item_name
			, CASE WHEN m.item_id IS NOT NULL THEN rod.qty*m.qty  ELSE rod.qty END AS qty
			, ro.remark
			FROM retur_pinjaman_d rod 
			LEFT JOIN retur_pinjaman ro ON rod.retur_pinjaman_id = ro.id
			LEFT JOIN stockiest s ON ro.stockiest_id = s.id
			LEFT JOIN member mem ON mem.id = s.id
			LEFT JOIN manufaktur m ON m.manufaktur_id = rod.item_id
			LEFT JOIN item i ON i.id = rod.item_id
			LEFT JOIN item i2 ON i2.id = m.item_id
			LEFT JOIN warehouse w ON ro.warehouse_id = w.id
			WHERE ro.tgl BETWEEN '".$fromdate."' AND '".$todate."'
			AND ro.warehouse_id = ".$whsid."
			AND rod.pv > 0
			ORDER BY rod.retur_pinjaman_id
		";
		}
		$q = $this->db->query($query);
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
}
?>