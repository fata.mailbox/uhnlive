<?php
class Search_model2 extends CI_Model{
  function __construct()
  {
    parent::__construct();
  }

  public function search($keywords=0, $group='',$num,$offset){
    $data = array();
    $this->db->select("id, name",false);
    $this->db->from('item');
    if($group){
      $where = "group = '$group' and ( id like '%$keywords%' or name like '%$keywords%' )";
      $this->db->where($where);
    }
    else{
      $this->db->like('id', $keywords, 'between');
      $this->db->or_like('name', $keywords, 'between');
    }
    $this->db->order_by('name','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }

  public function countSearch($keywords=0,$group){
    $this->db->from('item');
    if($group){
      $where = "group = '$group' and ( id like '%$keywords%' or name like '%$keywords%' )";
      $this->db->where($where);
    }
    else{
      $this->db->like('id', $keywords, 'between');
      $this->db->or_like('name', $keywords, 'between');
    }

    return $this->db->count_all_results();
  }

  public function search_stock_whs($whsid, $keywords=0, $flag='',$num,$offset){
    $data = array();
    //$this->db->select("s.item_id,i.name,s.qty,format(s.qty,0)as fqty,i.price,format(i.price,0)as fprice,i.price2,format(i.price2,0)as fprice2,i.pv,format(i.pv,0)as fpv,i.bv, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order, CASE WHEN inon.id is not null then 1 else 0 end as nonstcfee",false);
    //Start ASP 20170928
    if($offset == '')$offset = 0;
		$query ="
		(
		SELECT s.item_id,i.name,s.qty,FORMAT(s.qty,0)AS fqty,i.price,FORMAT(i.price,0)AS fprice,i.price2,FORMAT(i.price2,0)AS fprice2
		,i.pv,FORMAT(i.pv,0)AS fpv,i.bv, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
		, CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
		, w.name AS stock_wh, w.id AS whsid, i.warehouse_id
		FROM stock s
		LEFT JOIN item i ON s.item_id=i.id
		LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
		LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
		LEFT JOIN warehouse w ON s.warehouse_id = w.id
		LEFT JOIN(
								SELECT
								i.id
								, IFNULL(ind.price_not_disc,0) AS price_not_disc
								, MIN(ind.end_period) AS end_period
								FROM
								item i
								LEFT JOIN item_not_discount ind ON ind.item_id = i.id AND ind.expired = 0 AND ind.end_period >= DATE(NOW())
								GROUP BY i.id
							) AS dt ON s.item_id = dt.id
		WHERE s.warehouse_id = ".$whsid." and i.sales = 'Yes' AND s.qty > 0 AND ( i.id LIKE '%".$keywords."%' OR i.name LIKE '%".$keywords."%')
		AND i.warehouse_id = 0
		-- ORDER BY NAME ASC
		)
		UNION (
		SELECT
		s.item_id,i.name,s.qty,FORMAT(s.qty,0)AS fqty,i.price,FORMAT(i.price,0)AS fprice,i.price2,FORMAT(i.price2,0)AS fprice2
		,i.pv,FORMAT(i.pv,0)AS fpv,i.bv, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
		, CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
		, w.name AS stock_wh, w.id AS whsid, i.warehouse_id
		FROM item i
		LEFT JOIN stock s  ON s.item_id=i.id AND s.warehouse_id = i.warehouse_id
		LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
		LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
		LEFT JOIN warehouse w ON s.warehouse_id = w.id
		LEFT JOIN(
								SELECT
								i.id
								, IFNULL(ind.price_not_disc,0) AS price_not_disc
								, MIN(ind.end_period) AS end_period
								FROM
								item i
								LEFT JOIN item_not_discount ind ON ind.item_id = i.id AND ind.expired = 0 AND ind.end_period >= DATE(NOW())
								GROUP BY i.id
							) AS dt ON s.item_id = dt.id
		WHERE i.sales = 'Yes' AND s.qty > 0 AND ( i.id LIKE '%".$keywords."%' OR i.name LIKE '%".$keywords."%')
		-- ORDER BY NAME ASC
		)
		ORDER BY warehouse_id, NAME ASC
		limit ".$offset.",".$num."
		";

    $q = $this->db->query($query);

    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }

  public function count_search_stock_whs($whsid,$keywords=0,$flag){
    $query ="(
    SELECT s.item_id,i.name,s.qty,FORMAT(s.qty,0)AS fqty,i.price
          ,FORMAT(i.price,0)AS fprice,i.price2,FORMAT(i.price2,0)AS fprice2
          ,i.pv,FORMAT(i.pv,0)AS fpv,i.bv, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
          ,CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
          ,w.name AS stock_wh, w.id AS whsid, i.warehouse_id
    FROM stock s
    LEFT JOIN item i ON s.item_id=i.id
    LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
    LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
    LEFT JOIN warehouse w ON s.warehouse_id = w.id
    LEFT JOIN(
                SELECT
                i.id
                , IFNULL(ind.price_not_disc,0) AS price_not_disc
                , MIN(ind.end_period) AS end_period
                FROM
                item i
                LEFT JOIN item_not_discount ind ON ind.item_id = i.id AND ind.expired = 0 AND ind.end_period >= DATE(NOW())
                GROUP BY i.id
              ) AS dt ON s.item_id = dt.id
    WHERE s.warehouse_id = ".$whsid." and i.sales = 'Yes' AND s.qty > 0 AND ( i.id LIKE '%".$keywords."%' OR i.name LIKE '%".$keywords."%')
    AND i.warehouse_id = 0
    -- ORDER BY NAME ASC
    )
    UNION (
    SELECT
    s.item_id,i.name,s.qty,FORMAT(s.qty,0)AS fqty,i.price,FORMAT(i.price,0)AS fprice,i.price2,FORMAT(i.price2,0)AS fprice2
    ,i.pv,FORMAT(i.pv,0)AS fpv,i.bv, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
    , CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
    , w.name AS stock_wh, w.id AS whsid, i.warehouse_id
    FROM item i
    LEFT JOIN stock s  ON s.item_id=i.id AND s.warehouse_id = i.warehouse_id
    LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
    LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
    LEFT JOIN warehouse w ON s.warehouse_id = w.id
    LEFT JOIN(
                SELECT
                i.id
                , IFNULL(ind.price_not_disc,0) AS price_not_disc
                , MIN(ind.end_period) AS end_period
                FROM
                item i
                LEFT JOIN item_not_discount ind ON ind.item_id = i.id AND ind.expired = 0 AND ind.end_period >= DATE(NOW())
                GROUP BY i.id
              ) AS dt ON s.item_id = dt.id
    WHERE i.sales = 'Yes' AND s.qty > 0 AND ( i.id LIKE '%".$keywords."%' OR i.name LIKE '%".$keywords."%')
    -- ORDER BY NAME ASC
    )
    ORDER BY warehouse_id, NAME ASC
    ";
    $q = $this->db->query($query);
		//echo $q->num_rows();
        return $q->num_rows();
        //return $this->db->count_all_results();

	//END ASP 20181231
  }

  public function itemSearch($keywords=0, $group='',$num,$offset){
    $data = array();
    $this->db->select("id,name",false);
    $this->db->from('item');
    if($group == 'ass'){
      // $where = "manufaktur = 'Yes' and ( id like '$keywords%' or name like '$keywords%' )";
      // updated by Boby 2010-05-27
      $where = "manufaktur = 'Yes' and sales = 'Yes' and ( id like '$keywords%' or name like '$keywords%' )";
    }elseif($group == 'all'){
      $where = "id like '$keywords%' or name like '$keywords%' ";
    }else{
      $where = "manufaktur = 'No' and ( id like '$keywords%' or name like '$keywords%' )";
    }
    $this->db->where($where);
    $this->db->order_by('name','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    // echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function countItemSearch($keywords=0,$group){
    $this->db->select("id",false);
    $this->db->from('item');
    if($group == 'ass'){
      // $where = "manufaktur = 'Yes' and ( id like '$keywords%' or name like '$keywords%' )";
      // updated by Boby 2010-05-27
      $where = "manufaktur = 'Yes' and sales = 'Yes' and ( id like '$keywords%' or name like '$keywords%' )";
    }elseif($group == 'all'){
      $where = "id like '$keywords%' or name like '$keywords%' ";
    }else{
      $where = "manufaktur = 'No' and ( id like '$keywords%' or name like '$keywords%' )";
    }
    $this->db->where($where);
    return $this->db->count_all_results();
  }
  public function searchStockiestAct($keywords=0,$stc,$num,$offset){
    $data = array();
    /*
    $this->db->select("a.id,k.id as kota_id,k.name as namakota,a.no_stc,m.nama,format(a.ewallet,0)as fewalletstc
    , md.alamat, k.timur, md.kota, md.id as deli_ad
    , a.type as tipe,if(a.type = 1,6,0)as persen
    ",false);
    */
    $this->db->select("a.id,k.id as kota_id,k.name as namakota,a.no_stc,m.nama,format(a.ewallet,0)as fewalletstc
    , md.alamat, k.timur, md.kota, md.id as deli_ad, md.kecamatan, md.kelurahan, md.kodepos, md.pic_name, md.pic_hp
    , km.id as kota_id_stc, km.name as namakota_stc, km.timur as timur_stc
    , m.alamat as alamat_stc, m.kecamatan as kecamatan_stc, m.kelurahan as kelurahan_stc, m.kodepos as kodepos_stc
    , m.hp as hp_stc
    , a.type as tipe,
    CASE WHEN a.type = 1 THEN 6
    WHEN a.type = 2 THEN 2
    ELSE 0 END AS persen
    ",false);
    $this->db->from('stockiest a');
    $this->db->join('member m','a.id=m.id','left');
    $this->db->join('member_delivery md','m.delivery_addr=md.id','left');
    $this->db->join('kota k','md.kota=k.id','left');
    $this->db->join('kota km','m.kota_id=km.id','left');
    $where = "(a.status = 'active') and (a.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%') ";

    if($stc == '0')$where = "a.id != '$stc' and ".$where;
    elseif($stc == 'mstc')$where = "a.type = 1 and ".$where; //untuk browse hanya stockiest tdk termasuk m-stc
    elseif($stc == 'romstc')$where = "a.type = 2 and ".$where; //untuk browse hanya m-stc
    elseif($stc == 'type')$where = "a.type = '$stc' and ".$where;

    $this->db->where($where);
    $this->db->order_by('a.no_stc','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    // echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function countStockiestAct($keywords=0,$stc){

    $where = "a.status = 'active' and (a.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%') ";
    if($stc == '0')$where = "a.id != '$stc' and ".$where;
    elseif($stc == 'mstc')$where = "a.type = 1 and ".$where; //untuk browse hanya stockiest tdk termasuk m-stc
    elseif($stc == 'romstc')$where = "a.type = 2 and ".$where; //untuk browse hanya m-stc
    elseif($stc == 'type')$where = "a.type = '$stc' and ".$where;
    else $where =$where;

    $this->db->select("a.id");
    $this->db->from('stockiest a');
    $this->db->join('member m','a.id=m.id','left');
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  /*
  |--------------------------------------------------------------------------
  | Search city & propinsi to profile
  |--------------------------------------------------------------------------
  |
  | @author taQwa
  | @author 2008-12-28
  |
  */
  public function searchCity($keywords=0,$group='',$num, $offset){
    $data = array();

    if($group == 'all'){
      $where = "k.name like '$keywords%' or p.name like '$keywords%' ";
    }
    $this->db->select("k.id,k.name as kota,p.name as propinsi, k.timur",false); // Updated by Boby 20140124
    $this->db->from('kota k');
    $this->db->join('propinsi p','k.propinsi_id=p.id','left');
    $this->db->where($where);
    $this->db->order_by('k.name','asc');
    $this->db->order_by('p.name','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();

    if($q->num_rows >0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }

  public function countSearchCity($keywords=0,$group=''){
    if($group == 'all'){
      $where = "k.name like '$keywords%' or p.name like '$keywords%' ";
    }
    $this->db->select("k.id",false);
    $this->db->from('kota k');
    $this->db->join('propinsi p','k.propinsi_id=p.id','left');
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  public function searchMember($keywords=0,$num,$offset){
    $data = array();
    if($keywords){
      // updated by Boby 20100616
      // $this->db->select("id,nama,ewallet,format(ewallet,0)as fewallet",false);
      $this->db->select("
      m.id
      , m.nama
      , ifnull(k.id,0) as kota_id
      , ifnull(k.name,0) as namakota
      , m.alamat
      , m.hp
      , m.telp
      , m.email
      , m.ewallet
      , format(m.ewallet,0)as fewallet
      , m.kodepos",false);
      // end updated by Boby 20100616

      $this->db->from('member m');

      // updated by Boby 20100616
      $this->db->join ('kota k','m.kota_id = k.id','left');
      // end updated by Boby 20100616
      /*
      $this->db->join('users u','m.id=u.id','left');
      $this->db->where("`u`.`banned_id` < 1 AND (m.id LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')");
      */
      $this->db->like('m.id',$keywords,'between');
      $this->db->or_like('m.nama',$keywords,'between');
      $this->db->order_by('m.nama','asc');
      $this->db->limit($num,$offset);
      $q = $this->db->get();
      //echo $this->db->last_query();
      if($q->num_rows > 0){
        foreach($q->result_array() as $row){
          $data[] = $row;
        }
      }
      $q->free_result();
    }
    return $data;
  }

  public function searchMemberSoMandiri($keywords=0,$num,$offset){
    $data = array();
    if($keywords){
      // updated by Boby 20100616
      // $this->db->select("id,nama,ewallet,format(ewallet,0)as fewallet",false);
      $this->db->select("
      m.id
      , m.nama as personname
      , ifnull(k.id,0) as kota_id
      , ifnull(k.name,0) as namakota
      , m.alamat as alamat
      , m.hp as kontakhp
      , m.telp as kontaktelp
      , m.email as email
      , m.ewallet as ewallet
      , format(m.ewallet,0)as fewallet",false);
      // end updated by Boby 20100616

      $this->db->from('member m');

      // updated by Boby 20100616
      $this->db->join ('kota k','m.kota_id = k.id','left');
      // end updated by Boby 20100616
      $this->db->join('users u','m.id=u.id','left');
      $this->db->where("`u`.`banned_id` < 1 AND (m.id LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')");
      //$this->db->like('m.id',$keywords,'between');
      //$this->db->or_like('m.nama',$keywords,'between');
      $this->db->order_by('m.nama','asc');
      $this->db->limit($num,$offset);
      $q = $this->db->get();
      if($q->num_rows > 0){
        foreach($q->result_array() as $row){
          $data[] = $row;
        }
      }
      $q->free_result();
    }
    return $data;
  }


  public function countMember($keywords=0){
    if($keywords){
      $this->db->select("id,nama,ewallet,format(ewallet,0)as fewallet",false);
      $this->db->from('member');
      $this->db->like('id',$keywords,'between');
      $this->db->or_like('nama',$keywords,'between');
      $this->db->order_by('nama','asc');
      return $this->db->count_all_results();
    }else return 0;
  }

  /*
  |--------------------------------------------------------------------------
  | search item inventory for request titipan
  |--------------------------------------------------------------------------
  |
  | to popup for all user
  |
  | @author qtakwa@yahoo.com@yahoo.com
  | @created 2008-12-27
  |
  */
  public function itemROSearch($keywords=0,$num,$offset){
    $data = array();
    $where = "a.sales = 'Yes' and ( a.id like '$keywords%' or a.name like '$keywords%' )";
    // Modified by Boby 20140207
    $this->db->select("a.id,a.name,a.price,a.price2,a.pv,format(a.price,0)as fprice,format(a.pv,0)as fpv,b.name as type",false);
    $this->db->from('item a');
    $this->db->join('type b','a.type_id=b.id','left');

    $this->db->where($where);
    $this->db->order_by('name','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function countItemROSearch($keywords=0){
    $where = "a.sales = 'Yes' and ( a.id like '$keywords%' or a.name like '$keywords%' )";
    $this->db->select("a.id",false);
    $this->db->from('item a');
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  /*
  |--------------------------------------------------------------------------
  | search Stockiest
  |--------------------------------------------------------------------------
  |
  | @author qtakwa@yahoo.com@yahoo.com
  | @created 2009-04-16
  |
  */
  public function searchStockiest($keywords=0,$stc,$num,$offset){

    $data = array();
    $this->db->select("a.id,a.no_stc,k.id as kota_id,k.name as namakota,a.no_stc,m.nama,format(a.ewallet,0)as fewalletstc",false);
    $this->db->from('stockiest a');
    $this->db->join('member m','a.id=m.id','left');
    $this->db->join('kota k','a.kota_id=k.id','left');
    $where = "(a.status = 'active' or a.status = 'inactive') and (a.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%') ";

    if($stc == '0')$where = "a.id != '$stc' and ".$where;
    elseif($stc == 'mstc')$where = "a.type = 1 and ".$where; //untuk browse hanya stockiest tdk termasuk m-stc
    elseif($stc == 'romstc')$where = "a.type = 2 and ".$where; //untuk browse hanya m-stc
    elseif($stc == 'type')$where = "a.type = '$stc' and ".$where;

    $this->db->where($where);
    $this->db->order_by('a.no_stc','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    // echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function countStockiest($keywords=0,$stc){

    $where = "(a.status = 'active' or a.status = 'inactive') and (a.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%') ";
    if($stc == '0')$where = "a.id != '$stc' and ".$where;
    elseif($stc == 'mstc')$where = "a.type = 1 and ".$where; //untuk browse hanya stockiest tdk termasuk m-stc
    elseif($stc == 'romstc')$where = "a.type = 2 and ".$where; //untuk browse hanya m-stc
    elseif($stc == 'type')$where = "a.type = '$stc' and ".$where;
    else $where =$where;

    $this->db->select("a.id");
    $this->db->from('stockiest a');
    $this->db->join('member m','a.id=m.id','left');
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  /*
  |--------------------------------------------------------------------------
  | search item Titipan stockiest
  |--------------------------------------------------------------------------
  |
  | to popup for all stockiest and admin
  |
  | @author qtakwa@yahoo.com@yahoo.com
  | @created 2009-04-21
  |
  */
  public function itemSearchTitipan($keywords=0, $group='',$num,$offset){
    $data = array();

    $this->db->select("id,item_id,name,harga,fharga,fpv,fqty",false);
    $this->db->from('v_titipan v');
    if($group == 'ttp'){
      $where = "item_id like '$keywords%' or name like '$keywords%' ";
    }
    $this->db->where($where);
    $this->db->group_by('item_id');
    $this->db->group_by('harga');
    $this->db->order_by('name','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function countItemSearchTitipan($keywords=0,$group){
    $data = array();
    $this->db->select("item_id as total",false);
    $this->db->from('v_titipan');
    if($group == 'ttp'){
      $where = "item_id like '$keywords%' or name like '$keywords%' ";
    }
    $this->db->where($where);
    $this->db->group_by('item_id');
    $this->db->group_by('harga');
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows()>0){
      $data = $q->num_rows();
    }
    $q->free_result();
    return $data;
  }

  /*
  |--------------------------------------------------------------------------
  | search item Titipan stockiest
  |--------------------------------------------------------------------------
  |
  | to popup for all stockiest and admin
  | @poweredby www.smartindo-technology.com
  | @author qtakwa@yahoo.com@yahoo.com
  | @created 2009-05-10
  |
  */
  public function searchTtp($keywords=0,$num,$offset){
    $data = array();
    $where = "member_id = '".$this->session->userdata('userid')."' and ( item_id like '$keywords%' or name like '$keywords%' )";

    $this->db->select("id,item_id,name,harga,pv,fharga,fpv,fqty",false);
    $this->db->from('v_titipan');
    $this->db->where($where);
    $this->db->order_by('name','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function countSearchTtp($keywords=0){
    $data = array();
    $where = "member_id = '".$this->session->userdata('userid')."' and ( item_id like '$keywords%' or name like '$keywords%' )";

    $this->db->select("id",false);
    $this->db->from('v_titipan');
    $this->db->where($where);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows()>0){
      $data = $q->num_rows();
    }
    $q->free_result();
    return $data;
  }

  /*
  |--------------------------------------------------------------------------
  | search item inventory for request titipan
  |--------------------------------------------------------------------------
  |
  | to popup for all user
  |
  | @author qtakwa@yahoo.com@yahoo.com
  | @poweredby www.smartindo-technology.com
  | @created 2009-05-15
  |
  */
  public function itemSOSearch($keywords=0,$num,$offset){
    $data = array();
    $where = "a.sales = 'Yes' and a.type_id > 1 and ( a.id like '$keywords%' or a.name like '$keywords%' )";

    $this->db->select("a.id,a.name,a.price,a.pv,a.bv,format(a.price,0)as fprice,format(a.pv,0)as fpv,format(a.bv,0)as fbv,b.name as type",false);
    $this->db->from('item a');
    $this->db->join('type b','a.type_id=b.id','left');

    $this->db->where($where);
    $this->db->order_by('name','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function countItemSOSearch($keywords=0){
    $where = "a.sales = 'Yes' and a.type_id > 1 and ( a.id like '$keywords%' or a.name like '$keywords%' )";
    $this->db->select("a.id",false);
    $this->db->from('item a');
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  /*
  |--------------------------------------------------------------------------
  | search item Titipan stockiest for SO
  |--------------------------------------------------------------------------
  |
  | to popup for all stockiest
  | @poweredby www.smartindo-technology.com
  | @author qtakwa@yahoo.com@yahoo.com
  | @created 2009-05-18
  |
  */
  public function searchTtpSO($keywords=0,$num,$offset){
    $data = array();
    $where = "t.member_id = '".$this->session->userdata('userid')."' and t.qty > 0 and i.type_id > 1 and ( t.item_id like '$keywords%' or i.name like '$keywords%' )";

    // $this->db->select("t.id,t.item_id,i.name,t.harga,t.pv,i.bv,format(t.harga,0)as fharga,format(t.pv,0)as fpv,format(i.bv,0)as fbv,format(t.qty,0)as fqty",false);
    $this->db->select("t.id,t.item_id,i.name,t.harga,i.pv,i.bv,format(t.harga,0)as fharga,format(i.pv,0)as fpv,format(i.bv,0)as fbv,format(t.qty,0)as fqty",false);
    $this->db->from('titipan t');
    $this->db->join('item i','t.item_id=i.id','left');
    $this->db->where($where);
    $this->db->order_by('i.name','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function countSearchTtpSO($keywords=0){
    $data = array();
    $where = "t.member_id = '".$this->session->userdata('userid')."' and t.qty > 0 and i.type_id > 1 and ( t.item_id like '$keywords%' or i.name like '$keywords%' )";

    $this->db->select("t.id",false);
    $this->db->from('titipan t');
    $this->db->join('item i','t.item_id=i.id','left');
    $this->db->where($where);
    $this->db->where($where);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows()>0){
      $data = $q->num_rows();
    }
    $q->free_result();
    return $data;
  }

  /*
  |--------------------------------------------------------------------------
  | search item Titipan stockiest for SO
  |--------------------------------------------------------------------------
  |
  | to popup for all stockiest
  | @poweredby www.smartindo-technology.com
  | @author qtakwa@yahoo.com@yahoo.com
  | @created 2009-05-18
  |
  */
  public function searchTtpStock($keywords=0,$num,$offset,$stcid){
    $data = array();
    if($stcid){
      //$where = "stockiest_id = '$stcid' and type_id > 1 and ( item_id like '$keywords%' or name like '$keywords%' )";
      $where = "stockiest_id = '$stcid' and ( item_id like '$keywords%' or name like '$keywords%' )";

      $this->db->select("id,item_id,name,harga,pv,fharga,fpv,fqty",false);
      $this->db->from('v_pinjaman');
      $this->db->where($where);
      $this->db->order_by('name','asc');
      $this->db->limit($num,$offset);
      $q = $this->db->get();
      //echo $this->db->last_query();
      if($q->num_rows() > 0){
        foreach($q->result_array() as $row){
          $data[] = $row;
        }
      }
      $q->free_result();
    }
    return $data;
  }
  public function countSearchTtpStock($keywords=0,$stcid){
    $data = array();
    //$where = "stockiest_id = '$stcid' and type_id > 1 and ( item_id like '$keywords%' or name like '$keywords%' )";
    $where = "stockiest_id = '$stcid' and ( item_id like '$keywords%' or name like '$keywords%' )";

    $this->db->select("id",false);
    $this->db->from('v_pinjaman');
    $this->db->where($where);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows()>0){
      $data = $q->num_rows();
    }
    $q->free_result();
    return $data;
  }

  /*
  |--------------------------------------------------------------------------
  | search item Titipan stockiest
  |--------------------------------------------------------------------------
  |
  | to popup for all stockiest and admin
  | @poweredby www.smartindo-technology.com
  | @author qtakwa@yahoo.com@yahoo.com
  | @created 2009-05-10
  |
  */
  public function searchReturTtp($keywords=0,$num,$offset,$stcid){
    $data = array();
    $where = "member_id = '$stcid' and ( item_id like '$keywords%' or name like '$keywords%' )";

    $this->db->select("id,item_id,name,harga,pv,fharga,fpv,fqty",false);
    $this->db->from('v_titipan');
    $this->db->where($where);
    $this->db->order_by('name','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function countReturTtp($keywords=0,$stcid){
    $data = array();
    $where = "member_id = '$stcid' and ( item_id like '$keywords%' or name like '$keywords%' )";

    $this->db->select("id",false);
    $this->db->from('v_titipan');
    $this->db->where($where);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows()>0){
      $data = $q->num_rows();
    }
    $q->free_result();
    return $data;
  }

  public function search_inv_bom($manufaktur,$keywords=0,$num,$offset){
    $data = array();
    $this->db->select("id,name,format(price,0)as fprice,price",false);
    $this->db->from('item');
    $where = "manufaktur = '$manufaktur' and ( id like '%$keywords%' or name like '%$keywords%' )";
    $this->db->where($where);
    $this->db->order_by('name','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function count_inv_bom($manufaktur,$keywords=0){
    $this->db->from('item');
    $where = "manufaktur = '$manufaktur' and ( id like '%$keywords%' or name like '%$keywords%' )";
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  public function countSearchTtpSONew($keywords=0){
    $data = array();
    //$where = "t.member_id = '".$this->session->userdata('userid')."' and i.type_id > 1 and ( t.item_id like '$keywords%' or i.name like '$keywords%' )";
    $where = "t.member_id = '".$this->session->userdata('userid')."'
    and i.type_id > 1
    and ( t.item_id like '$keywords%' or i.name like '$keywords%' )
    and t.qty > 0
    ";

    $this->db->select("t.id",false);
    $this->db->from('titipan t');
    $this->db->join('item i','t.item_id=i.id','left');
    $this->db->where($where);
    $this->db->where($where);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows()>0){
      $data = $q->num_rows();
    }
    $q->free_result();
    return $data;
  }

  public function searchTtpSONew($keywords=0,$num,$offset){
    $data = array();
    $where = "t.member_id = '".$this->session->userdata('userid')."'
    and i.type_id > 1
    and ( t.item_id like '$keywords%' or i.name like '$keywords%' )
    and t.qty > 0
    ";
    $this->db->select("
    t.id,t.item_id,i.name
    ,t.harga,t.harga_
    ,format(t.harga,0)as fharga, ,format(t.harga_,0)as fharga_
    ,t.pv,i.bv,format(t.pv,0)as fpv,format(i.bv,0)as fbv,format(t.qty,0)as fqty",false);
    $this->db->from('titipan t');
    $this->db->join('item i','t.item_id=i.id','left');
    $this->db->where($where);
    $this->db->order_by('i.name','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }



  /* Created by Boby 20140120 */
  public function searchMemberSo($keywords=0,$num,$offset){
    $data = array();
    if($keywords){
      $this->db->select("
      m.id
      , m.nama
      , ifnull(md.kota,0) as kota_id
      , ifnull(k.name,0) as namakota
      , ifnull(k.timur,0)as timur
      , m.alamat as alamat
      , md.id as deli_ad
      , m.kecamatan as kecamatan
      , m.kelurahan as kelurahan
      , m.kodepos as kodepos
      , m.hp as handphone
      , w.id as warehouse
      , p.name as propinsi
      , m.ewallet
      , format(m.ewallet,0)as fewallet",false);

      $this->db->from('member m');
      $this->db->join ('member_delivery md','m.delivery_addr = md.id','left');
      $this->db->join ('kota k','m.kota_id = k.id','left');
      $this->db->join ('propinsi p','k.propinsi_id = p.id','left');
      $this->db->join('users u','m.id=u.id','left');
      $this->db->join('warehouse w','k.warehouse_id = w.id','left');
      $this->db->where("`u`.`banned_id` < 1 AND (m.id LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')");
      // $this->db->like('m.id',$keywords,'between');
      // $this->db->or_like('m.nama',$keywords,'between');
      //$this->db->order_by('m.nama','asc');
      $this->db->limit($num,$offset);
      $q = $this->db->get();
      // echo $this->db->last_query();
      if($q->num_rows > 0){
        foreach($q->result_array() as $row){
          $data[] = $row;
        }
      }
      $q->free_result();
    }
    return $data;
  }
  /* End created by Boby 20140120 */

  /* Created by Boby 20140310 */
  public function itemROSearch2($keywords=0,$num,$offset){
    $data = array();
    $memberid=$this->session->userdata('userid');
    $timur = 0;
    $data2 = array();
    $q2 = "
    SELECT s.id, k.timur
    FROM stockiest s
    LEFT JOIN kota k ON s.kota_delivery = k.id
    WHERE s.id = '$memberid'
    ORDER BY timur DESC
    ";
    $q2 = $this->db->query($q2);
    // echo $this->db->last_query();
    // $timur=$row['timur'];

    if($q2->num_rows()>0){
      foreach($q2->result_array()as $row2){
        $timur=$row2['timur'];
      }
    }

    $q2->free_result();
    // echo "a".$timur;
    if($timur<1){
      $this->db->select(" a.id, a.name, a.pv, format(a.pv,0)as fpv, b.name as type, a.price, format(a.price,0)as fprice ",false);
    }else{
      $this->db->select(" a.id, a.name, a.pv, format(a.pv,0)as fpv, b.name as type, a.price2 as price, format(a.price2,0)as fprice ",false);
    }

    $this->db->from('item a');
    $this->db->join('type b','a.type_id=b.id','left');
    $where = "a.sales = 'Yes' and ( a.id like '$keywords%' or a.name like '$keywords%' )";
    $this->db->where($where);
    $this->db->order_by('name','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    // echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  /* End created by Boby 20140310 */

  // Created by Takwa 20140616
  public function search_staff($keywords=0,$num,$offset){
    $data = array();
    if($keywords){
      $this->db->select("s.nik as id,s.name",false);
      $this->db->from('staff s');
      $this->db->like('s.nik',$keywords,'between');
      $this->db->or_like('s.name',$keywords,'between');
      $this->db->order_by('s.name','asc');
      $this->db->limit($num,$offset);
      $q = $this->db->get();
      //echo $this->db->last_query();
      if($q->num_rows > 0){
        foreach($q->result_array() as $row){
          $data[] = $row;
        }
      }
      $q->free_result();
    }
    return $data;
  }

  public function count_search_staff($keywords=0){
    if($keywords){
      $this->db->select("nik",false);
      $this->db->from('staff');
      $this->db->like('nik',$keywords,'between');
      $this->db->or_like('name',$keywords,'between');
      $this->db->order_by('name','asc');
      return $this->db->count_all_results();
    }else return 0;
  }

  public function item_staff($keywords=0,$num,$offset){
    $data = array();
    $where = "a.sales = 'Yes' and a.type_id > 1 and ( a.id like '$keywords%' or a.name like '$keywords%' )";

    $this->db->select("a.id,a.name,floor(a.price*0.70)as price,a.pv,a.bv,format(floor(a.price*0.70),0)as fprice,format(a.pv,0)as fpv,format(a.bv,0)as fbv,b.name as type",false);
    $this->db->from('item a');
    $this->db->join('type b','a.type_id=b.id','left');

    $this->db->where($where);
    $this->db->order_by('name','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function count_item_staff($keywords=0){
    $where = "a.sales = 'Yes' and a.type_id > 1 and ( a.id like '$keywords%' or a.name like '$keywords%' )";
    $this->db->select("a.id",false);
    $this->db->from('item a');
    $this->db->where($where);
    return $this->db->count_all_results();
  }
  // End created by Takwa 20140616


  //Created By ASP 20151130
  public function count_search_voucher_so($memberid,$keywords=0){
    $this->db->select("v.vouchercode",false);
    $this->db->from('voucher v');
    //$this->db->join('item i','s.item_id=i.id','left');

    $where = "v.member_id = '$memberid' and v.status= '0' and expired_date >= '".date('Y-m-d')."' and ( v.vouchercode like '%$keywords%' or v.remark like '%$keywords%' )";
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  public function search_voucher_so($memberid, $keywords=0,$num,$offset){
    $data = array();
    /*
    $this->db->select("s.item_id,i.name,s.qty,format(s.qty,0)as fqty,i.price,format(i.price,0)as fprice,i.price2,format(i.price2,0)as fprice2,i.pv,format(i.pv,0)as fpv,i.bv",false);
    $this->db->from('stock s');
    $this->db->join('item i','s.item_id=i.id','left');
    */
    $this->db->select("v.vouchercode, v.price, format(v.price,0) as fprice, v.pv, format(v.pv,0) as fpv, v.bv, format(v.bv,0) as fbv, v.remark, v.minorder, format(v.minorder,0) as fminorder",false);
    $this->db->from('voucher v');
    $where = "v.member_id = '$memberid' and v.status= '0' and expired_date >= '".date('Y-m-d')."' and ( v.vouchercode like '%$keywords%' or v.remark like '%$keywords%' )";
    $this->db->where($where);
    $this->db->order_by('v.expired_date','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function count_search_voucher_ro($memberid,$keywords=0){
    $this->db->select("v.vouchercode",false);
    $this->db->from('voucher v');
    //$this->db->join('item i','s.item_id=i.id','left');

    $where = "v.stc_id = '$memberid' and v.status= '1' and v.posisi='1' and expired_date >= '".date('Y-m-d')."' and ( v.vouchercode like '%$keywords%' or v.remark like '%$keywords%' )";
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  public function search_voucher_ro($memberid, $keywords=0,$num,$offset){
    $data = array();
    /*
    $this->db->select("s.item_id,i.name,s.qty,format(s.qty,0)as fqty,i.price,format(i.price,0)as fprice,i.price2,format(i.price2,0)as fprice2,i.pv,format(i.pv,0)as fpv,i.bv",false);
    $this->db->from('stock s');
    $this->db->join('item i','s.item_id=i.id','left');
    */
    $this->db->select("v.vouchercode, v.price, format(v.price,0) as fprice, 0 as pv, 0 as fpv, 0 as bv, 0 as fbv, v.remark",false);
    $this->db->from('voucher v');
    $where = "v.stc_id = '$memberid' and v.status= '1' and v.posisi='1' and expired_date >= '".date('Y-m-d')."' and ( v.vouchercode like '%$keywords%' or v.remark like '%$keywords%' )";
    $this->db->where($where);
    $this->db->order_by('v.expired_date','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }

  public function count_search_voucher_sostc($memberid,$stcid,$keywords=0){
    $this->db->select("v.vouchercode",false);
    $this->db->from('voucher v');
    //$this->db->join('item i','s.item_id=i.id','left');

    $where = "v.member_id = '$memberid' and v.stc_id = '$stcid' and v.status= '0' and expired_date >= '".date('Y-m-d')."' and ( v.vouchercode like '%$keywords%' or v.remark like '%$keywords%' )";
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  public function search_voucher_sostc($memberid,$stcid, $keywords=0,$num,$offset){
    $data = array();
    /*
    $this->db->select("s.item_id,i.name,s.qty,format(s.qty,0)as fqty,i.price,format(i.price,0)as fprice,i.price2,format(i.price2,0)as fprice2,i.pv,format(i.pv,0)as fpv,i.bv",false);
    $this->db->from('stock s');
    $this->db->join('item i','s.item_id=i.id','left');
    */
    $this->db->select("v.vouchercode, v.price, format(v.price,0) as fprice, v.pv, format(v.pv,0) as fpv, v.bv, format(v.bv,0) as fbv, v.remark, v.minorder, format(v.minorder,0) as fminorder",false);
    $this->db->from('voucher v');
    $where = "v.member_id = '$memberid' and v.stc_id = '$stcid' and v.status= '0' and expired_date >= '".date('Y-m-d')."' and ( v.vouchercode like '%$keywords%' or v.remark like '%$keywords%' )";
    $this->db->where($where);
    $this->db->order_by('v.expired_date','asc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  // End Created By ASP 20151130

  //Start Created By ASP 20160115
  public function searchStockiestSalesMtd($tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
    $data = array();
    //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
    //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
    $awal = "'".$tglawal."' ".$br;
    $akhir = "'".$tglakhir."' ".$br;

    if($omset=='yes'){
      $qryoms = 'HAVING omset <> 0'	;
    }else{
      $qryoms = ''	;
    }
    if($act=='yes'){
      $qryact = 'AND omset >= 35000000'	;
    }else{
      $qryact = ''	;
    }
    $query = $query1."
    SELECT s.id, s.no_stc,m.nama
    , IFNULL(ro1,0)+IFNULL(pjm1,0)-IFNULL(rtr1,0) AS omset
    FROM stockiest_active sa
    LEFT JOIN stockiest s ON sa.id = s.id
    LEFT JOIN kota k ON s.kota_id = k.id
    LEFT JOIN member m ON s.id = m.id
    LEFT JOIN propinsi p ON k.propinsi_id = p.id
    LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
    LEFT JOIN propinsi_cluster pc ON ppc.cluster_id = pc.id
    LEFT JOIN propinsi_cluster_group ppg ON pc.cluster_group_id = ppg.id
    LEFT JOIN(
      SELECT member_id
      , SUM(oms1)AS ro1
      FROM(
        SELECT member_id
        , CASE WHEN ((ro.`date`) <= '2015-02-28') THEN totalharga WHEN ((ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms1
        FROM ro
        RIGHT JOIN ro_d rod ON ro.id = rod.ro_id
        WHERE ro.stockiest_id=0
        -- AND rod.pv > 0
        AND (
          CASE WHEN ro.`date` > '2015-02-28' THEN rod.pv > 0
          ELSE rod.pv >= 0
          END
          )
          AND ro.date BETWEEN $awal AND $akhir
          GROUP BY ro.id
          )AS ro_
          GROUP BY member_id
          )AS ro ON s.id = ro.member_id
          LEFT JOIN(
            SELECT member_id
            , SUM(oms1)AS pjm1
            FROM(
              SELECT stockiest_id AS member_id
              , CASE WHEN ((pjm.tgl) <= '2015-02-28') OR ((pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
              FROM pinjaman_titipan pjm
              WHERE pjm.tgl BETWEEN $awal AND $akhir
              )AS pjm
              GROUP BY member_id
              )AS pjm ON s.id = pjm.member_id
              LEFT JOIN(
                SELECT member_id
                , SUM(oms1)AS rtr1
                FROM(
                  SELECT stockiest_id AS member_id
                  , CASE WHEN ((rtr.tgl) <= '2015-02-28') OR ((rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
                  FROM retur_titipan rtr
                  WHERE rtr.tgl BETWEEN $awal AND $akhir
                  )AS rtr
                  GROUP BY member_id
                  )AS rtr ON s.id = rtr.member_id
                  WHERE pc.id = '$cluster_id'
                  AND sa.periode BETWEEN $awal AND $akhir
                  ".$qryoms."
                  ".$qryact."
                  order by s.no_stc
                  ";
                  $qry = $this->db->query($query);
                  //echo $this->db->last_query();

                  if($qry->num_rows()>0){
                    foreach($qry->result_array() as $row){
                      $data[]=$row;
                    }
                  }
                  $qry->free_result();
                  return $data;


                }
                public function searchNewRecruit($tglawal,$tglakhir,$cluster_id,$omset='no'){
                  $data = array();
                  //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                  //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                  $awal = "'".$tglawal."' ".$br;
                  $akhir = "'".$tglakhir."' ".$br;

                  if($omset=='yes'){
                    $qryoms = 'AND nr = 1'	;
                  }else{
                    $qryoms = ''	;
                  }
                  $query = $query1."
                  SELECT member_id,nama,
                  SUM(blj) AS omset
                  ,nr
                  ,created
                  ,cluster
                  FROM (
                    SELECT so.stockiest_id, member_id, m.nama , SUM(sod.jmlharga)AS blj, MAX(so.kit)AS kit_
                    , CASE WHEN MAX(so.kit) = 'Y' AND SUM(totalpv) > 0 THEN 1 ELSE 0 END AS nr
                    , CASE WHEN MAX(so.kit) = 'Y' THEN 1 ELSE 0 END AS nm
                    , CASE WHEN so.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                    , CASE WHEN so.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                    , m.created
                    FROM so
                    RIGHT JOIN so_d sod ON so.id = sod.so_id
                    LEFT JOIN member m ON so.member_id = m.id
                    LEFT JOIN stockiest s ON so.stockiest_id = s.id
                    LEFT JOIN kota k ON m.kota_id = k.id
                    LEFT JOIN kota ks ON s.kota_id = ks.id
                    LEFT JOIN propinsi p ON k.propinsi_id = p.id
                    LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                    LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                    LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                    WHERE tgl BETWEEN $awal AND $akhir
                    GROUP BY member_id
                    ) AS dt
                    WHERE cluster = '$cluster_id'
                    ".$qryoms."
                    GROUP BY member_id
                    order by nama
                    ";
                    $qry = $this->db->query($query);
                    //echo $this->db->last_query();

                    if($qry->num_rows()>0){
                      foreach($qry->result_array() as $row){
                        $data[]=$row;
                      }
                    }
                    $qry->free_result();
                    return $data;


                  }

                  public function searchQspmup($tglawal,$tglakhir,$cluster_id){
                    $data = array();
                    //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                    //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                    $awal = "'".$tglawal."' ".$br;
                    $akhir = "'".$tglakhir."' ".$br;

                    $query = $query1."
                    SELECT
                    lq.member_id, m.nama,lq.jenjang, j.decription as title, lq.pgs
                    -- , CASE WHEN m.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                    -- , CASE WHEN m.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                    , k.region AS region
                    , ppc.cluster_id AS cluster
                    FROM leader_qualified lq
                    LEFT JOIN pgs_bulanan pb ON lq.member_id = pb.member_id AND lq.tgl = pb.tgl
                    LEFT JOIN member m ON lq.member_id = m.id
                    LEFT JOIN stockiest s ON m.stockiest_id = s.id
                    LEFT JOIN kota k ON m.kota_id = k.id
                    LEFT JOIN kota ks ON s.kota_id = ks.id
                    LEFT JOIN propinsi p ON k.propinsi_id = p.id
                    LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                    LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                    LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                    LEFT JOIN jenjang j on lq.jenjang = j.id
                    WHERE
                    -- lq.jenjang >= 4
                    pb.jenjang_id >= 4
                    AND lq.qty >= 1
                    -- AND lq.tgl = $akhir
                    AND lq.tgl BETWEEN $awal AND $akhir
                    AND ppc.cluster_id = $cluster_id
                    ";
                    $qry = $this->db->query($query);
                    //echo $this->db->last_query();

                    if($qry->num_rows()>0){
                      foreach($qry->result_array() as $row){
                        $data[]=$row;
                      }
                    }
                    $qry->free_result();
                    return $data;


                  }

                  public function searchStockiestSalesLastYear($tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                    $data = array();
                    //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                    //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                    $awal = "'".$tglawal."' ".$br;
                    $akhir = "'".$tglakhir."' ".$br;

                    if($omset=='yes'){
                      $qryoms = 'HAVING omset <> 0'	;
                    }else{
                      $qryoms = ''	;
                    }
                    if($act=='yes'){
                      $qryact = 'AND omset >= 35000000'	;
                    }else{
                      $qryact = ''	;
                    }
                    $query = $query1."
                    SELECT s.id, s.no_stc,m.nama
                    , IFNULL(ro1,0)+IFNULL(pjm1,0)-IFNULL(rtr1,0) AS omset
                    FROM stockiest s
                    LEFT JOIN kota k ON s.kota_id = k.id
                    LEFT JOIN member m ON s.id = m.id
                    LEFT JOIN propinsi p ON k.propinsi_id = p.id
                    LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                    LEFT JOIN propinsi_cluster pc ON ppc.cluster_id = pc.id
                    LEFT JOIN propinsi_cluster_group ppg ON pc.cluster_group_id = ppg.id
                    LEFT JOIN(
                      SELECT member_id
                      , SUM(oms1)AS ro1
                      FROM(
                        SELECT member_id
                        , CASE WHEN ((ro.`date`) <= '2015-02-28') THEN totalharga WHEN ((ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms1
                        FROM ro
                        RIGHT JOIN ro_d rod ON ro.id = rod.ro_id
                        WHERE ro.stockiest_id=0
                        -- AND rod.pv > 0
                        AND (
                          CASE WHEN ro.`date` > '2015-02-28' THEN rod.pv > 0
                          ELSE rod.pv >= 0
                          END
                          )
                          AND ro.date BETWEEN $awal AND $akhir
                          GROUP BY ro.id
                          )AS ro_
                          GROUP BY member_id
                          )AS ro ON s.id = ro.member_id
                          LEFT JOIN(
                            SELECT member_id
                            , SUM(oms1)AS pjm1
                            FROM(
                              SELECT stockiest_id AS member_id
                              , CASE WHEN ((pjm.tgl) <= '2015-02-28') OR ((pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
                              FROM pinjaman_titipan pjm
                              WHERE pjm.tgl BETWEEN $awal AND $akhir
                              )AS pjm
                              GROUP BY member_id
                              )AS pjm ON s.id = pjm.member_id
                              LEFT JOIN(
                                SELECT member_id
                                , SUM(oms1)AS rtr1
                                FROM(
                                  SELECT stockiest_id AS member_id
                                  , CASE WHEN ((rtr.tgl) <= '2015-02-28') OR ((rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
                                  FROM retur_titipan rtr
                                  WHERE rtr.tgl BETWEEN $awal AND $akhir
                                  )AS rtr
                                  GROUP BY member_id
                                  )AS rtr ON s.id = rtr.member_id
                                  WHERE pc.id = $cluster_id
                                  ".$qryoms."
                                  ".$qryact."
                                  order by s.no_stc
                                  ";
                                  $qry = $this->db->query($query);
                                  //echo $this->db->last_query();

                                  if($qry->num_rows()>0){
                                    foreach($qry->result_array() as $row){
                                      $data[]=$row;
                                    }
                                  }
                                  $qry->free_result();
                                  return $data;


                                }

                                public function searchOtherSales($tglawal,$tglakhir,$type){
                                  $data = array();
                                  //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                  //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                  $awal = "'".$tglawal."' ".$br;
                                  $akhir = "'".$tglakhir."' ".$br;
                                  if($type=='starterkit'){
                                    $qother = "WHERE group_id = 6 AND newdata.id NOT IN ('SK010036','SK010037')";
                                    $add = '';
                                  }
                                  else if($type=='printing'){
                                    $qother = "WHERE group_id = 7 AND UPPER(nama) NOT LIKE '%TIKET%' AND newdata.id NOT IN ('SK010036','SK010037')";
                                    $add = '';
                                  }
                                  if($type=='tiket'){
                                    $qother = "WHERE UPPER(nama) LIKE '%TIKET%'";
                                    $add = '';
                                  }
                                  $query = $query1."
                                  SELECT
                                  IFNULL(ig.id,99) AS group_id
                                  , CASE WHEN ig.id IS NOT NULL THEN ig.name ELSE 'Package' END AS group_name
                                  , newdata.id, newdata.nama
                                  -- , FORMAT(harga,0)AS harga, harga as harga1
                                  , jml, FORMAT(total,0)AS total, total AS total1
                                  , jmlnc, FORMAT(totnc,0)AS totnc, totnc AS totnc1
                                  , jmlsc, FORMAT(totsc,0)AS totsc, totsc AS totsc1
                                  , jmlretur, FORMAT(totretur,0)AS totretur, totretur AS totretur1
                                  -- , ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
                                  , ((jml + jmlsc + 0) - jmlretur) AS totalqty
                                  , FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
                                  , (total + totsc + 0) - totretur AS totalnominal1
                                  , (total + totsc + 0) + totretur AS totalnominal1_
                                  FROM(
                                    SELECT
                                    dta.id, dta.`name` AS nama -- , dta.harga AS harga
                                    , (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
                                    , (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
                                    , IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
                                    , IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
                                    , IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
                                    FROM(
                                      SELECT i.id, i.`name` -- , sod.harga
                                      FROM item i
                                      -- LEFT JOIN so_d sod ON i.id = sod.item_id
                                      -- GROUP BY sod.item_id -- , sod.harga
                                      )AS dta
                                      LEFT JOIN(
                                        SELECT itemo_id AS item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total
                                        FROM (
                                          SELECT
                                          -- distinct (so.id),
                                          CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
                                          ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
                                          ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
                                          -- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.jmlharga END AS total
                                          ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
                                          FROM so_d sod
                                          LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga_ = rp.package_price
                                          LEFT JOIN so ON sod.so_id = so.id
                                          WHERE
                                          so.tgl BETWEEN $awal AND $akhir
                                          AND so.stockiest_id = '0'
                                          ) AS dt
                                          GROUP BY itemo_id -- , sod.harga
                                          )AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
                                          LEFT JOIN(
                                            SELECT itemo_id AS item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total
                                            FROM (
                                              SELECT
                                              -- distinct(ro.id),
                                              CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
                                              ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
                                              ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
                                              -- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE rod.jmlharga_ END AS total
                                              ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga*rod.qty) END AS total
                                              FROM ro_d rod
                                              LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga_ = rp.package_price
                                              LEFT JOIN ro ON rod.ro_id = ro.id
                                              WHERE
                                              ro.`date` BETWEEN $awal AND $akhir
                                              AND ro.stockiest_id = '0'
                                              ) AS dt
                                              GROUP BY itemo_id -- , rod.harga
                                              )AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
                                              LEFT JOIN(
                                                SELECT itemo_id AS item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total
                                                FROM (
                                                  SELECT
                                                  CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ptd.item_id END AS itemo_id
                                                  ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE ptd.harga END AS hargao
                                                  ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*ptd.qty ELSE ptd.qty END AS qty
                                                  ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*ptd.qty ELSE ptd.jmlharga END AS total
                                                  FROM pinjaman_titipan_d ptd
                                                  LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
                                                  LEFT JOIN reff_package rp ON ptd.item_id = rp.package_id AND ptd.harga = rp.package_price
                                                  WHERE pj.tgl BETWEEN $awal AND $akhir
                                                  ) AS dt
                                                  GROUP BY itemo_id -- , ptd.harga
                                                  )AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
                                                  LEFT JOIN(
                                                    SELECT itemo_id AS item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total
                                                    FROM (
                                                      SELECT
                                                      -- DISTINCT (rt.id),
                                                      CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
                                                      ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
                                                      ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
                                                      ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
                                                      FROM retur_titipan_d rtd
                                                      LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
                                                      LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
                                                      WHERE rt.tgl BETWEEN $awal AND $akhir
                                                      ) AS dt
                                                      GROUP BY itemo_id -- , rtd.harga
                                                      )AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
                                                      LEFT JOIN(
                                                        SELECT itemo_id AS item_id, FLOOR(SUM(qty)) AS jml, hargao AS harga, SUM(total) AS total
                                                        FROM (
                                                          SELECT
                                                          CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
                                                          ,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
                                                          ,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
                                                          ,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
                                                          FROM ncm_d ncd
                                                          LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
                                                          LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
                                                          WHERE nc.`date` BETWEEN $awal AND $akhir
                                                          ) AS dt
                                                          GROUP BY itemo_id
                                                          )AS dtf ON dta.id = dtf.item_id
                                                          )AS newdata
                                                          LEFT JOIN item_to_item_group iig ON newdata.id = iig.id
                                                          LEFT JOIN item_group ig ON iig.group_id = ig.id
                                                          ".$qother."
                                                          HAVING totalnominal1_ <> 0 OR jmlnc <> 0
                                                          ORDER BY group_id, newdata.id,newdata.nama
                                                          ";

                                                          if ($type=='topup'){
                                                            $query = $query1."
                                                            SELECT
                                                            IFNULL(ig.id,99) AS group_id
                                                            , CASE WHEN ig.id IS NOT NULL THEN ig.name ELSE 'Package' END AS group_name
                                                            , newdata.id, newdata.nama
                                                            , jml, FORMAT(total,0)AS total, total AS total1
                                                            -- , jmlnc, FORMAT(totnc,0)AS totnc, totnc AS totnc1
                                                            , jmlsc, FORMAT(totsc,0)AS totsc, totsc AS totsc1
                                                            , jmlretur, FORMAT(totretur,0)AS totretur, totretur AS totretur1
                                                            , ((jml + jmlsc + 0) - jmlretur) AS totalqty
                                                            , FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
                                                            , (total + totsc + 0) - totretur AS totalnominal1
                                                            -- , (total + totsc + 0) AS totalnominal1
                                                            , (total + totsc + 0) + totretur AS totalnominal1_
                                                            FROM(
                                                              SELECT
                                                              dta.id, dta.`name` AS nama
                                                              , (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
                                                              , (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
                                                              -- , (IFNULL(dtc.jml,0)) AS jml
                                                              -- , (IFNULL(dtc.total,0)) AS total
                                                              -- , IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
                                                              , IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
                                                              , IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
                                                              FROM(
                                                                SELECT i.id, i.`name`
                                                                FROM item i


                                                                )AS dta
                                                                LEFT JOIN(
                                                                  SELECT IFNULL(group_id,99) AS group_id,itemo_id AS item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total
                                                                  FROM (
                                                                    SELECT
                                                                    sod.so_id
                                                                    ,sod.pv,i.name,sod.item_id
                                                                    ,sod.item_id AS itemo_id
                                                                    ,sod.harga AS hargao
                                                                    ,sod.qty AS qty
                                                                    ,sod.harga*sod.qty AS total
                                                                    /*
                                                                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
                                                                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
                                                                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty

                                                                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
                                                                    */
                                                                    FROM so_d sod
                                                                    LEFT JOIN item i ON sod.item_id = i.id
                                                                    -- LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga_ = rp.package_price
                                                                    LEFT JOIN so ON sod.so_id = so.id
                                                                    -- LEFT JOIN item_to_item_group iig ON  sod.item_id = iig.id
                                                                    -- LEFT JOIN item_group ig ON iig.group_id = ig.id
                                                                    WHERE
                                                                    so.tgl BETWEEN $awal AND $akhir
                                                                    AND so.stockiest_id = '0'
                                                                    AND sod.pv = 0
                                                                    -- and group_id NOT IN (6,7)
                                                                    AND so.member_id != 'STAFF'
                                                                    ) AS dt
                                                                    LEFT JOIN item_to_item_group iig ON dt.itemo_id = iig.id
                                                                    LEFT JOIN item_group ig ON iig.group_id = ig.id
                                                                    WHERE group_id NOT IN (6,7) OR group_id IS NULL
                                                                    GROUP BY itemo_id
                                                                    )AS dtb ON dta.id = dtb.item_id
                                                                    LEFT JOIN(
                                                                      SELECT IFNULL(group_id,99) AS group_id, itemo_id AS item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total
                                                                      FROM (
                                                                        SELECT
                                                                        rod.item_id AS itemo_id
                                                                        ,rod.harga AS hargao
                                                                        ,rod.qty AS qty
                                                                        ,(rod.harga*rod.qty) AS total
                                                                        /*
                                                                        CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
                                                                        ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
                                                                        ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty

                                                                        ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga*rod.qty) END AS total
                                                                        */
                                                                        FROM ro_d rod
                                                                        -- LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga_ = rp.package_price
                                                                        LEFT JOIN ro ON rod.ro_id = ro.id
                                                                        WHERE
                                                                        ro.`date` BETWEEN $awal AND $akhir
                                                                        AND ro.stockiest_id = '0'
                                                                        AND rod.pv = 0

                                                                        ) AS dt
                                                                        LEFT JOIN item_to_item_group iig ON dt.itemo_id = iig.id
                                                                        LEFT JOIN item_group ig ON iig.group_id = ig.id
                                                                        WHERE group_id NOT IN (6,7) OR group_id IS NULL
                                                                        GROUP BY itemo_id
                                                                        )AS dtc ON dta.id = dtc.item_id
                                                                        LEFT JOIN(
                                                                          SELECT IFNULL(group_id,99) AS group_id, itemo_id AS item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total
                                                                          FROM (
                                                                            SELECT
                                                                            ptd.item_id AS itemo_id
                                                                            ,ptd.harga AS hargao
                                                                            ,ptd.qty AS qty
                                                                            ,ptd.jmlharga AS total
                                                                            /*
                                                                            CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ptd.item_id END AS itemo_id
                                                                            ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE ptd.harga END AS hargao
                                                                            ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*ptd.qty ELSE ptd.qty END AS qty
                                                                            ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*ptd.qty ELSE ptd.jmlharga END AS total
                                                                            */
                                                                            FROM pinjaman_titipan_d ptd
                                                                            LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
                                                                            -- LEFT JOIN reff_package rp ON ptd.item_id = rp.package_id AND ptd.harga = rp.package_price
                                                                            WHERE pj.tgl BETWEEN $awal AND $akhir
                                                                            AND ptd.pv = 0
                                                                            ) AS dt
                                                                            LEFT JOIN item_to_item_group iig ON dt.itemo_id = iig.id
                                                                            LEFT JOIN item_group ig ON iig.group_id = ig.id
                                                                            WHERE group_id NOT IN (6,7) OR group_id IS NULL
                                                                            GROUP BY itemo_id
                                                                            )AS dtd ON dta.id = dtd.item_id
                                                                            LEFT JOIN(
                                                                              SELECT IFNULL(group_id,99) AS group_id, itemo_id AS item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total
                                                                              FROM (
                                                                                SELECT
                                                                                rtd.item_id AS itemo_id
                                                                                ,rtd.harga AS hargao
                                                                                ,rtd.qty AS qty
                                                                                ,rtd.jmlharga AS total
                                                                                /*
                                                                                CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
                                                                                ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
                                                                                ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
                                                                                ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
                                                                                */
                                                                                FROM retur_titipan_d rtd
                                                                                -- LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
                                                                                LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
                                                                                WHERE rt.tgl BETWEEN $awal AND $akhir
                                                                                AND rtd.pv = 0
                                                                                ) AS dt
                                                                                LEFT JOIN item_to_item_group iig ON dt.itemo_id = iig.id
                                                                                LEFT JOIN item_group ig ON iig.group_id = ig.id
                                                                                WHERE group_id NOT IN (6,7) OR group_id IS NULL
                                                                                GROUP BY itemo_id
                                                                                )AS dte ON dta.id = dte.item_id
                                                                                LEFT JOIN(
                                                                                  SELECT IFNULL(group_id,99) AS group_id, itemo_id AS item_id, FLOOR(SUM(qty)) AS jml, hargao AS harga, SUM(total) AS total
                                                                                  FROM (
                                                                                    SELECT
                                                                                    ncd.item_id AS itemo_id
                                                                                    ,ncd.hpp AS hargao
                                                                                    ,ncd.qty AS qty
                                                                                    ,ncd.qty*ncd.hpp AS total
                                                                                    /*
                                                                                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
                                                                                    ,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
                                                                                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
                                                                                    ,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
                                                                                    */
                                                                                    FROM ncm_d ncd
                                                                                    LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
                                                                                    LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
                                                                                    WHERE nc.`date` BETWEEN $awal AND $akhir
                                                                                    ) AS dt
                                                                                    LEFT JOIN item_to_item_group iig ON dt.itemo_id = iig.id
                                                                                    LEFT JOIN item_group ig ON iig.group_id = ig.id
                                                                                    WHERE group_id NOT IN (6,7) OR group_id IS NULL
                                                                                    GROUP BY itemo_id
                                                                                    )AS dtf ON dta.id = dtf.item_id
                                                                                    )AS newdata
                                                                                    LEFT JOIN item_to_item_group iig ON newdata.id = iig.id
                                                                                    LEFT JOIN item_group ig ON iig.group_id = ig.id
                                                                                    WHERE group_id NOT IN (6,7) OR group_id IS NULL
                                                                                    AND newdata.id NOT IN ('SK010036','SK010037')
                                                                                    HAVING totalnominal1_ <> 0-- OR jmlnc <> 0
                                                                                    ORDER BY group_id, newdata.id,newdata.nama
                                                                                    ";
                                                                                  }

                                                                                  if($type=='UHN'){
                                                                                    $query = $query1."
                                                                                    SELECT
                                                                                    IFNULL(iig.group_id,'99') AS group_id
                                                                                    ,CASE WHEN ig.id IS NOT NULL THEN ig.name ELSE 'Package' END AS group_name
                                                                                    , sod.item_id as id, i.name AS nama
                                                                                    ,  SUM(sod.jmlharga) AS totalnominal1
                                                                                    FROM so_d sod
                                                                                    LEFT JOIN item i ON sod.item_id = i.id
                                                                                    LEFT JOIN so ON sod.so_id = so.id
                                                                                    LEFT JOIN item_to_item_group iig ON  sod.item_id = iig.id
                                                                                    LEFT JOIN item_group ig ON iig.group_id = ig.id
                                                                                    WHERE
                                                                                    so.stockiest_id = '0'
                                                                                    AND so.member_id != 'STAFF'
                                                                                    AND sod.pv > 0
                                                                                    AND so.tgl BETWEEN  $awal AND $akhir
                                                                                    -- GROUP BY group_id
                                                                                    GROUP BY sod.item_id
                                                                                    HAVING group_id NOT IN (6,7) AND totalnominal1 > 0
                                                                                    ORDER BY iig.group_id
                                                                                    ";
                                                                                  }
                                                                                  $qry = $this->db->query($query);
                                                                                  //echo $this->db->last_query();

                                                                                  if($qry->num_rows()>0){
                                                                                    foreach($qry->result_array() as $row){
                                                                                      $data[]=$row;
                                                                                    }
                                                                                  }
                                                                                  $qry->free_result();
                                                                                  return $data;


                                                                                }
                                                                                //End Created By ASP 20160115

                                                                                //Start Created By ASP 20160125
                                                                                public function searchSOSalesMtd($tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                                                                                  $data = array();
                                                                                  //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                  //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                  $awal = "'".$tglawal."' ".$br;
                                                                                  $akhir = "'".$tglakhir."' ".$br;

                                                                                  if($omset=='yes'){
                                                                                    $qryoms = 'HAVING omset <> 0'	;
                                                                                  }else{
                                                                                    $qryoms = ''	;
                                                                                  }
                                                                                  if($act=='yes'){
                                                                                    $qryact = 'AND omset >= 0'	;
                                                                                  }else{
                                                                                    $qryact = ''	;
                                                                                  }
                                                                                  $query = $query1."
                                                                                  select member_id,nama, cluster , blj as omset
                                                                                  from
                                                                                  (
                                                                                    SELECT so.stockiest_id, m.id as member_id, m.nama, SUM(ifnull(sod.jmlharga,0))AS blj, MAX(so.kit)AS kit_
                                                                                    , CASE WHEN MAX(so.kit) = 'Y' AND SUM(sod.jmlharga) > 0 THEN SUM(sod.jmlharga) ELSE 0 END AS blj_nam
                                                                                    , CASE WHEN MAX(so.kit) = 'N' AND SUM(sod.jmlharga) > 0 THEN SUM(sod.jmlharga) ELSE 0 END AS blj_oam
                                                                                    , CASE WHEN MAX(so.kit) = 'Y' AND SUM(sod.jmlharga) > 0 THEN 1 ELSE 0 END AS nam
                                                                                    , CASE WHEN MAX(so.kit) = 'N' AND SUM(sod.jmlharga) > 0 THEN 1 ELSE 0 END AS oam
                                                                                    , CASE WHEN so.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                                                                                    , CASE WHEN so.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                                                                                    FROM member m
                                                                                    LEFT JOIN so ON m.id = so.member_id
                                                                                    RIGHT JOIN so_d sod ON so.id = sod.so_id
                                                                                    -- LEFT JOIN pgs_bulanan pb ON m.id = pb.member_id and $akhir = pb.tgl
                                                                                    LEFT JOIN stockiest s ON so.stockiest_id = s.id
                                                                                    LEFT JOIN kota k ON m.kota_id = k.id
                                                                                    LEFT JOIN kota ks ON s.kota_id = ks.id
                                                                                    LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                    LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                    LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                                                                                    LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                                                                                    WHERE so.tgl BETWEEN $awal AND $akhir
                                                                                    -- and ppc.cluster_id = $cluster_id
                                                                                    GROUP BY so.member_id
                                                                                    ) as dt
                                                                                    WHERE cluster = $cluster_id
                                                                                    ".$qryoms."
                                                                                    ".$qryact."
                                                                                    order by nama
                                                                                    ";
                                                                                    $qry = $this->db->query($query);
                                                                                    //echo $this->db->last_query();

                                                                                    if($qry->num_rows()>0){
                                                                                      foreach($qry->result_array() as $row){
                                                                                        $data[]=$row;
                                                                                      }
                                                                                    }
                                                                                    $qry->free_result();
                                                                                    return $data;


                                                                                  }

                                                                                  public function searchSOSalesLastYear($tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                                                                                    $data = array();
                                                                                    //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                    //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                    $awal = "'".$tglawal."' ".$br;
                                                                                    $akhir = "'".$tglakhir."' ".$br;

                                                                                    if($omset=='yes'){
                                                                                      $qryoms = 'HAVING omset <> 0'	;
                                                                                    }else{
                                                                                      $qryoms = ''	;
                                                                                    }
                                                                                    if($act=='yes'){
                                                                                      //$qryact = 'AND omset >= 0'	;
                                                                                      $qryact = ''	;
                                                                                    }else{
                                                                                      $qryact = ''	;
                                                                                    }
                                                                                    $query = $query1."
                                                                                    select member_id,nama, cluster , blj as omset
                                                                                    from
                                                                                    (
                                                                                      SELECT so.stockiest_id, m.id as member_id, m.nama, SUM(ifnull(sod.jmlharga,0))AS blj, MAX(so.kit)AS kit_
                                                                                      , CASE WHEN MAX(so.kit) = 'Y' AND SUM(sod.jmlharga) > 0 THEN SUM(sod.jmlharga) ELSE 0 END AS blj_nam
                                                                                      , CASE WHEN MAX(so.kit) = 'N' AND SUM(sod.jmlharga) > 0 THEN SUM(sod.jmlharga) ELSE 0 END AS blj_oam
                                                                                      , CASE WHEN MAX(so.kit) = 'Y' AND SUM(sod.jmlharga) > 0 THEN 1 ELSE 0 END AS nam
                                                                                      , CASE WHEN MAX(so.kit) = 'N' AND SUM(sod.jmlharga) > 0 THEN 1 ELSE 0 END AS oam
                                                                                      , CASE WHEN so.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                                                                                      , CASE WHEN so.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                                                                                      FROM member m
                                                                                      LEFT JOIN so ON m.id = so.member_id
                                                                                      RIGHT JOIN so_d sod ON so.id = sod.so_id
                                                                                      -- LEFT JOIN pgs_bulanan pb ON m.id = pb.member_id and $akhir = pb.tgl
                                                                                      LEFT JOIN stockiest s ON so.stockiest_id = s.id
                                                                                      LEFT JOIN kota k ON m.kota_id = k.id
                                                                                      LEFT JOIN kota ks ON s.kota_id = ks.id
                                                                                      LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                      LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                      LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                                                                                      LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                                                                                      WHERE so.tgl BETWEEN $awal AND $akhir
                                                                                      -- and ppc.cluster_id = $cluster_id
                                                                                      GROUP BY so.member_id
                                                                                      ) as dt
                                                                                      WHERE cluster = $cluster_id
                                                                                      ".$qryoms."
                                                                                      ".$qryact."
                                                                                      order by nama
                                                                                      ";
                                                                                      $qry = $this->db->query($query);
                                                                                      //echo $this->db->last_query();

                                                                                      if($qry->num_rows()>0){
                                                                                        foreach($qry->result_array() as $row){
                                                                                          $data[]=$row;
                                                                                        }
                                                                                      }
                                                                                      $qry->free_result();
                                                                                      return $data;


                                                                                    }

                                                                                    public function searchnam($tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                                                                                      $data = array();
                                                                                      //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                      //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                      $awal = "'".$tglawal."' ".$br;
                                                                                      $akhir = "'".$tglakhir."' ".$br;

                                                                                      if($omset=='yes'){
                                                                                        $qryoms = 'HAVING omset <> 0'	;
                                                                                      }else{
                                                                                        $qryoms = ''	;
                                                                                      }
                                                                                      if($act=='yes'){
                                                                                        $qryact = 'AND omset >= 0'	;
                                                                                      }else{
                                                                                        $qryact = ''	;
                                                                                      }
                                                                                      $query = $query1."
                                                                                      select member_id,nama, cluster , blj as omset , nam
                                                                                      from
                                                                                      (
                                                                                        SELECT so.stockiest_id, m.id as member_id, m.nama, SUM(ifnull(sod.jmlharga,0))AS blj, MAX(so.kit)AS kit_
                                                                                        , CASE WHEN MAX(so.kit) = 'Y' AND SUM(sod.jmlharga) > 0 THEN SUM(sod.jmlharga) ELSE 0 END AS blj_nam
                                                                                        , CASE WHEN MAX(so.kit) = 'N' AND SUM(sod.jmlharga) > 0 THEN SUM(sod.jmlharga) ELSE 0 END AS blj_oam
                                                                                        , CASE WHEN MAX(so.kit) = 'Y' AND SUM(sod.jmlharga) > 0 THEN 1 ELSE 0 END AS nam
                                                                                        , CASE WHEN MAX(so.kit) = 'N' AND SUM(sod.jmlharga) > 0 THEN 1 ELSE 0 END AS oam
                                                                                        , CASE WHEN so.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                                                                                        , CASE WHEN so.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                                                                                        FROM member m
                                                                                        LEFT JOIN so ON m.id = so.member_id
                                                                                        RIGHT JOIN so_d sod ON so.id = sod.so_id
                                                                                        -- LEFT JOIN pgs_bulanan pb ON m.id = pb.member_id and $akhir = pb.tgl
                                                                                        LEFT JOIN stockiest s ON so.stockiest_id = s.id
                                                                                        LEFT JOIN kota k ON m.kota_id = k.id
                                                                                        LEFT JOIN kota ks ON s.kota_id = ks.id
                                                                                        LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                        LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                        LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                                                                                        LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                                                                                        WHERE so.tgl BETWEEN $awal AND $akhir
                                                                                        -- and ppc.cluster_id = $cluster_id
                                                                                        GROUP BY so.member_id
                                                                                        ) as dt
                                                                                        WHERE cluster = $cluster_id
                                                                                        and nam = 1
                                                                                        ".$qryoms."
                                                                                        ".$qryact."
                                                                                        order by nama
                                                                                        ";
                                                                                        $qry = $this->db->query($query);
                                                                                        //echo $this->db->last_query();

                                                                                        if($qry->num_rows()>0){
                                                                                          foreach($qry->result_array() as $row){
                                                                                            $data[]=$row;
                                                                                          }
                                                                                        }
                                                                                        $qry->free_result();
                                                                                        return $data;


                                                                                      }
                                                                                      public function searchoam($tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                                                                                        $data = array();
                                                                                        //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                        //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                        $awal = "'".$tglawal."' ".$br;
                                                                                        $akhir = "'".$tglakhir."' ".$br;

                                                                                        if($omset=='yes'){
                                                                                          $qryoms = 'HAVING omset <> 0'	;
                                                                                        }else{
                                                                                          $qryoms = ''	;
                                                                                        }
                                                                                        if($act=='yes'){
                                                                                          $qryact = 'AND omset >= 0'	;
                                                                                        }else{
                                                                                          $qryact = ''	;
                                                                                        }
                                                                                        $query = $query1."
                                                                                        select member_id,nama, cluster , blj as omset , oam
                                                                                        from
                                                                                        (
                                                                                          SELECT so.stockiest_id, m.id as member_id, m.nama, SUM(ifnull(sod.jmlharga,0))AS blj, MAX(so.kit)AS kit_
                                                                                          , CASE WHEN MAX(so.kit) = 'Y' AND SUM(sod.jmlharga) > 0 THEN SUM(sod.jmlharga) ELSE 0 END AS blj_nam
                                                                                          , CASE WHEN MAX(so.kit) = 'N' AND SUM(sod.jmlharga) > 0 THEN SUM(sod.jmlharga) ELSE 0 END AS blj_oam
                                                                                          , CASE WHEN MAX(so.kit) = 'Y' AND SUM(sod.jmlharga) > 0 THEN 1 ELSE 0 END AS nam
                                                                                          , CASE WHEN MAX(so.kit) = 'N' AND SUM(sod.jmlharga) > 0 THEN 1 ELSE 0 END AS oam
                                                                                          , CASE WHEN so.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                                                                                          , CASE WHEN so.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                                                                                          FROM member m
                                                                                          LEFT JOIN so ON m.id = so.member_id
                                                                                          RIGHT JOIN so_d sod ON so.id = sod.so_id
                                                                                          -- LEFT JOIN pgs_bulanan pb ON m.id = pb.member_id and $akhir = pb.tgl
                                                                                          LEFT JOIN stockiest s ON so.stockiest_id = s.id
                                                                                          LEFT JOIN kota k ON m.kota_id = k.id
                                                                                          LEFT JOIN kota ks ON s.kota_id = ks.id
                                                                                          LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                          LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                          LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                                                                                          LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                                                                                          WHERE so.tgl BETWEEN $awal AND $akhir
                                                                                          -- and ppc.cluster_id = $cluster_id
                                                                                          GROUP BY so.member_id
                                                                                          ) as dt
                                                                                          WHERE cluster = $cluster_id
                                                                                          and oam = 1
                                                                                          ".$qryoms."
                                                                                          ".$qryact."
                                                                                          order by nama
                                                                                          ";
                                                                                          $qry = $this->db->query($query);
                                                                                          //echo $this->db->last_query();

                                                                                          if($qry->num_rows()>0){
                                                                                            foreach($qry->result_array() as $row){
                                                                                              $data[]=$row;
                                                                                            }
                                                                                          }
                                                                                          $qry->free_result();
                                                                                          return $data;


                                                                                        }

                                                                                        public function searchnew($tglawal,$tglakhir,$cluster_id,$jenjang,$omset='no',$act='no'){
                                                                                          $data = array();
                                                                                          //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                          //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                          $awal = "'".$tglawal."' ".$br;
                                                                                          $akhir = "'".$tglakhir."' ".$br;

                                                                                          if($omset=='yes'){
                                                                                            $qryoms = 'HAVING omset <> 0'	;
                                                                                          }else{
                                                                                            $qryoms = ''	;
                                                                                          }
                                                                                          if($act=='yes'){
                                                                                            $qryact = 'AND omset >= 0'	;
                                                                                          }else{
                                                                                            $qryact = ''	;
                                                                                          }
                                                                                          $query = $query1."
                                                                                          SELECT
                                                                                          member_id
                                                                                          ,nama
                                                                                          ,cluster
                                                                                          ,region
                                                                                          ,jenjang_id
                                                                                          , ps as omset
                                                                                          , pgs as tgpv
                                                                                          -- ,
                                                                                          FROM (
                                                                                            SELECT pb.member_id, m.nama, pb.ps, pb.pgs
                                                                                            , CASE WHEN m.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                                                                                            , CASE WHEN m.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                                                                                            -- , k.region AS region
                                                                                            -- , ppc.cluster_id AS cluster
                                                                                            , pb.jenjang_id
                                                                                            FROM pgs_bulanan pb
                                                                                            LEFT JOIN jenjang_history jh ON pb.member_id = jh.member_id AND pb.tgl = jh.tgl
                                                                                            LEFT JOIN member m ON pb.member_id = m.id
                                                                                            LEFT JOIN stockiest s ON m.stockiest_id = s.id
                                                                                            LEFT JOIN kota k ON m.kota_id = k.id
                                                                                            LEFT JOIN kota ks ON s.kota_id = ks.id
                                                                                            LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                            LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                            LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                                                                                            LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                                                                                            LEFT JOIN (
                                                                                              SELECT jhs.member_id , jenjangbaru, COUNT(*) AS jmljenjang FROM jenjang_history jhs
                                                                                              GROUP BY jhs.member_id , jenjangbaru
                                                                                              ) AS jhs ON jhs.member_id = pb.member_id AND jhs.jenjangbaru = jh.jenjangbaru
                                                                                              WHERE pb.tgl between $awal AND $akhir
                                                                                              AND jh.jenjangbaru = $jenjang
                                                                                              AND jh.jenjangbaru > jh.jenjanglama
                                                                                              AND jhs.jmljenjang = 1
                                                                                              ORDER BY region,cluster,member_id
                                                                                              ) AS dt
                                                                                              WHERE cluster = $cluster_id
                                                                                              ORDER BY nama
                                                                                              ";
                                                                                              $qry = $this->db->query($query);
                                                                                              //echo $this->db->last_query();

                                                                                              if($qry->num_rows()>0){
                                                                                                foreach($qry->result_array() as $row){
                                                                                                  $data[]=$row;
                                                                                                }
                                                                                              }
                                                                                              $qry->free_result();
                                                                                              return $data;


                                                                                            }

                                                                                            public function searchnewspm($tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                                                                                              $data = array();
                                                                                              //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                              //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                              $awal = "'".$tglawal."' ".$br;
                                                                                              $akhir = "'".$tglakhir."' ".$br;

                                                                                              if($omset=='yes'){
                                                                                                $qryoms = 'HAVING omset <> 0'	;
                                                                                              }else{
                                                                                                $qryoms = ''	;
                                                                                              }
                                                                                              if($act=='yes'){
                                                                                                $qryact = 'AND omset >= 0'	;
                                                                                              }else{
                                                                                                $qryact = ''	;
                                                                                              }
                                                                                              $query = $query1."
                                                                                              SELECT
                                                                                              member_id
                                                                                              ,nama
                                                                                              ,cluster
                                                                                              ,region
                                                                                              FROM (
                                                                                                SELECT
                                                                                                lq.member_id, m.nama
                                                                                                -- , CASE WHEN m.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                                                                                                -- , CASE WHEN m.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                                                                                                , k.region AS region
                                                                                                , ppc.cluster_id AS cluster
                                                                                                FROM leader_qualified lq
                                                                                                LEFT JOIN pgs_bulanan pb ON lq.member_id = pb.member_id AND lq.tgl = pb.tgl
                                                                                                LEFT JOIN member m ON lq.member_id = m.id
                                                                                                LEFT JOIN stockiest s ON m.stockiest_id = s.id
                                                                                                LEFT JOIN kota k ON m.kota_id = k.id
                                                                                                LEFT JOIN kota ks ON s.kota_id = ks.id
                                                                                                LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                                LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                                LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                                                                                                LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                                                                                                LEFT JOIN (
                                                                                                  SELECT lq.member_id , SUM(lq.qty) AS qualified
                                                                                                  FROM leader_qualified lq
                                                                                                  LEFT JOIN pgs_bulanan pb ON lq.member_id = pb.member_id AND lq.tgl = pb.tgl
                                                                                                  WHERE
                                                                                                  -- lq.jenjang = 4
                                                                                                  pb.jenjang_id = 4
                                                                                                  AND lq.qty >= 1
                                                                                                  AND lq.tgl BETWEEN '2009-01-01' AND $akhir
                                                                                                  GROUP BY lq.member_id
                                                                                                  ) AS dt ON lq.member_id = dt.member_id
                                                                                                  WHERE
                                                                                                  -- lq.jenjang = 4
                                                                                                  pb.jenjang_id = 4
                                                                                                  AND lq.qty >= 1
                                                                                                  AND lq.tgl between $awal AND $akhir
                                                                                                  AND dt.qualified = 1
                                                                                                  ) AS dt
                                                                                                  WHERE cluster = $cluster_id
                                                                                                  -- ".$qryoms."
                                                                                                  -- ".$qryact."
                                                                                                  ORDER BY nama -- cluster, region
                                                                                                  ";
                                                                                                  $qry = $this->db->query($query);
                                                                                                  //echo $this->db->last_query();

                                                                                                  if($qry->num_rows()>0){
                                                                                                    foreach($qry->result_array() as $row){
                                                                                                      $data[]=$row;
                                                                                                    }
                                                                                                  }
                                                                                                  $qry->free_result();
                                                                                                  return $data;


                                                                                                }
                                                                                                public function searcholdqspm($tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                                                                                                  $data = array();
                                                                                                  //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                                  //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                                  $awal = "'".$tglawal."' ".$br;
                                                                                                  $akhir = "'".$tglakhir."' ".$br;

                                                                                                  if($omset=='yes'){
                                                                                                    $qryoms = 'HAVING omset <> 0'	;
                                                                                                  }else{
                                                                                                    $qryoms = ''	;
                                                                                                  }
                                                                                                  if($act=='yes'){
                                                                                                    $qryact = 'AND omset >= 0'	;
                                                                                                  }else{
                                                                                                    $qryact = ''	;
                                                                                                  }
                                                                                                  $query = $query1."
                                                                                                  SELECT
                                                                                                  member_id
                                                                                                  ,nama
                                                                                                  ,cluster
                                                                                                  ,region
                                                                                                  FROM (
                                                                                                    SELECT
                                                                                                    lq.member_id, m.nama
                                                                                                    -- , CASE WHEN m.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                                                                                                    -- , CASE WHEN m.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                                                                                                    , k.region AS region
                                                                                                    , ppc.cluster_id AS cluster
                                                                                                    FROM leader_qualified lq
                                                                                                    LEFT JOIN pgs_bulanan pb ON lq.member_id = pb.member_id AND lq.tgl = pb.tgl
                                                                                                    LEFT JOIN member m ON lq.member_id = m.id
                                                                                                    LEFT JOIN stockiest s ON m.stockiest_id = s.id
                                                                                                    LEFT JOIN kota k ON m.kota_id = k.id
                                                                                                    LEFT JOIN kota ks ON s.kota_id = ks.id
                                                                                                    LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                                    LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                                    LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                                                                                                    LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                                                                                                    LEFT JOIN (
                                                                                                      SELECT lq.member_id , SUM(lq.qty) AS qualified
                                                                                                      FROM leader_qualified lq
                                                                                                      LEFT JOIN pgs_bulanan pb ON lq.member_id = pb.member_id AND lq.tgl = pb.tgl
                                                                                                      WHERE
                                                                                                      -- lq.jenjang = 4
                                                                                                      pb.jenjang_id = 4
                                                                                                      AND lq.qty >= 1
                                                                                                      AND lq.tgl BETWEEN '2009-01-01' AND $akhir
                                                                                                      GROUP BY lq.member_id
                                                                                                      ) AS dt ON lq.member_id = dt.member_id
                                                                                                      WHERE
                                                                                                      -- lq.jenjang = 4
                                                                                                      pb.jenjang_id = 4
                                                                                                      AND lq.qty >= 1
                                                                                                      AND lq.tgl between $awal AND $akhir
                                                                                                      AND dt.qualified > 1
                                                                                                      ) AS dt
                                                                                                      WHERE cluster = $cluster_id
                                                                                                      -- ".$qryoms."
                                                                                                      -- ".$qryact."
                                                                                                      ORDER BY nama -- cluster, region
                                                                                                      ";
                                                                                                      $qry = $this->db->query($query);
                                                                                                      //echo $this->db->last_query();

                                                                                                      if($qry->num_rows()>0){
                                                                                                        foreach($qry->result_array() as $row){
                                                                                                          $data[]=$row;
                                                                                                        }
                                                                                                      }
                                                                                                      $qry->free_result();
                                                                                                      return $data;


                                                                                                    }
                                                                                                    public function searchnewl($tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                                                                                                      $data = array();
                                                                                                      //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                                      //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                                      $awal = "'".$tglawal."' ".$br;
                                                                                                      $akhir = "'".$tglakhir."' ".$br;

                                                                                                      if($omset=='yes'){
                                                                                                        $qryoms = 'HAVING omset <> 0'	;
                                                                                                      }else{
                                                                                                        $qryoms = ''	;
                                                                                                      }
                                                                                                      if($act=='yes'){
                                                                                                        $qryact = 'AND omset >= 0'	;
                                                                                                      }else{
                                                                                                        $qryact = ''	;
                                                                                                      }
                                                                                                      $query = $query1."
                                                                                                      SELECT
                                                                                                      member_id
                                                                                                      ,nama
                                                                                                      ,cluster
                                                                                                      ,region
                                                                                                      FROM (
                                                                                                        SELECT
                                                                                                        lq.member_id, m.nama
                                                                                                        -- , CASE WHEN m.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                                                                                                        -- , CASE WHEN m.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                                                                                                        , k.region AS region
                                                                                                        , ppc.cluster_id AS cluster
                                                                                                        FROM leader_qualified lq
                                                                                                        LEFT JOIN member m ON lq.member_id = m.id
                                                                                                        LEFT JOIN stockiest s ON m.stockiest_id = s.id
                                                                                                        LEFT JOIN kota k ON m.kota_id = k.id
                                                                                                        LEFT JOIN kota ks ON s.kota_id = ks.id
                                                                                                        LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                                        LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                                        LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                                                                                                        LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                                                                                                        LEFT JOIN (
                                                                                                          SELECT lq.member_id , SUM(lq.qty) AS qualified
                                                                                                          FROM leader_qualified lq
                                                                                                          WHERE lq.jenjang = 5
                                                                                                          AND lq.qty >= 1
                                                                                                          AND lq.tgl BETWEEN '2009-01-01' AND $akhir
                                                                                                          GROUP BY lq.member_id
                                                                                                          ) AS dt ON lq.member_id = dt.member_id
                                                                                                          WHERE
                                                                                                          lq.jenjang = 5
                                                                                                          AND lq.qty >= 1
                                                                                                          AND lq.tgl between $awal AND $akhir
                                                                                                          AND dt.qualified = 1
                                                                                                          ) AS dt
                                                                                                          WHERE cluster = $cluster_id
                                                                                                          -- ".$qryoms."
                                                                                                          -- ".$qryact."
                                                                                                          ORDER BY nama -- cluster, region
                                                                                                          ";
                                                                                                          $qry = $this->db->query($query);
                                                                                                          //echo $this->db->last_query();

                                                                                                          if($qry->num_rows()>0){
                                                                                                            foreach($qry->result_array() as $row){
                                                                                                              $data[]=$row;
                                                                                                            }
                                                                                                          }
                                                                                                          $qry->free_result();
                                                                                                          return $data;


                                                                                                        }
                                                                                                        public function searcholdql($tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                                                                                                          $data = array();
                                                                                                          //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                                          //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                                          $awal = "'".$tglawal."' ".$br;
                                                                                                          $akhir = "'".$tglakhir."' ".$br;

                                                                                                          if($omset=='yes'){
                                                                                                            $qryoms = 'HAVING omset <> 0'	;
                                                                                                          }else{
                                                                                                            $qryoms = ''	;
                                                                                                          }
                                                                                                          if($act=='yes'){
                                                                                                            $qryact = 'AND omset >= 0'	;
                                                                                                          }else{
                                                                                                            $qryact = ''	;
                                                                                                          }
                                                                                                          $query = $query1."
                                                                                                          SELECT
                                                                                                          member_id
                                                                                                          ,nama
                                                                                                          ,cluster
                                                                                                          ,region
                                                                                                          FROM (
                                                                                                            SELECT
                                                                                                            lq.member_id, m.nama
                                                                                                            -- , CASE WHEN m.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                                                                                                            -- , CASE WHEN m.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                                                                                                            , k.region AS region
                                                                                                            , ppc.cluster_id AS cluster
                                                                                                            FROM leader_qualified lq
                                                                                                            LEFT JOIN pgs_bulanan pb ON lq.member_id = pb.member_id AND lq.tgl = pb.tgl
                                                                                                            LEFT JOIN member m ON lq.member_id = m.id
                                                                                                            LEFT JOIN stockiest s ON m.stockiest_id = s.id
                                                                                                            LEFT JOIN kota k ON m.kota_id = k.id
                                                                                                            LEFT JOIN kota ks ON s.kota_id = ks.id
                                                                                                            LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                                            LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                                            LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                                                                                                            LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                                                                                                            LEFT JOIN (
                                                                                                              SELECT lq.member_id , SUM(lq.qty) AS qualified
                                                                                                              FROM leader_qualified lq
                                                                                                              LEFT JOIN pgs_bulanan pb ON lq.member_id = pb.member_id AND lq.tgl = pb.tgl
                                                                                                              WHERE
                                                                                                              -- lq.jenjang = 5
                                                                                                              pb.jenjang_id = 5
                                                                                                              AND lq.qty >= 1
                                                                                                              AND lq.tgl BETWEEN '2009-01-01' AND $akhir
                                                                                                              GROUP BY lq.member_id
                                                                                                              ) AS dt ON lq.member_id = dt.member_id
                                                                                                              WHERE
                                                                                                              -- lq.jenjang = 5
                                                                                                              pb.jenjang_id = 5
                                                                                                              AND lq.qty >= 1
                                                                                                              AND lq.tgl between $awal AND $akhir
                                                                                                              AND dt.qualified > 1
                                                                                                              ) AS dt
                                                                                                              WHERE cluster = $cluster_id
                                                                                                              -- ".$qryoms."
                                                                                                              -- ".$qryact."
                                                                                                              ORDER BY nama -- cluster, region
                                                                                                              ";
                                                                                                              $qry = $this->db->query($query);
                                                                                                              //echo $this->db->last_query();

                                                                                                              if($qry->num_rows()>0){
                                                                                                                foreach($qry->result_array() as $row){
                                                                                                                  $data[]=$row;
                                                                                                                }
                                                                                                              }
                                                                                                              $qry->free_result();
                                                                                                              return $data;


                                                                                                            }
                                                                                                            public function searcholdnql($tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                                                                                                              $data = array();
                                                                                                              //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                                              //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                                              $awal = "'".$tglawal."' ".$br;
                                                                                                              $akhir = "'".$tglakhir."' ".$br;

                                                                                                              if($omset=='yes'){
                                                                                                                $qryoms = 'HAVING omset <> 0'	;
                                                                                                              }else{
                                                                                                                $qryoms = ''	;
                                                                                                              }
                                                                                                              if($act=='yes'){
                                                                                                                $qryact = 'AND omset >= 0'	;
                                                                                                              }else{
                                                                                                                $qryact = ''	;
                                                                                                              }
                                                                                                              $query = $query1."
                                                                                                              SELECT
                                                                                                              member_id
                                                                                                              ,nama
                                                                                                              ,cluster
                                                                                                              ,region
                                                                                                              FROM (
                                                                                                                SELECT
                                                                                                                pb.member_id
                                                                                                                , m.nama
                                                                                                                -- , CASE WHEN m.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                                                                                                                -- , CASE WHEN m.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                                                                                                                , k.region AS region
                                                                                                                , ppc.cluster_id AS cluster
                                                                                                                FROM pgs_bulanan pb
                                                                                                                LEFT JOIN member m ON pb.member_id = m.id
                                                                                                                LEFT JOIN stockiest s ON m.stockiest_id = s.id
                                                                                                                LEFT JOIN kota k ON m.kota_id = k.id
                                                                                                                LEFT JOIN kota ks ON s.kota_id = ks.id
                                                                                                                LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                                                LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                                                LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                                                                                                                LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                                                                                                                WHERE pb.jenjang_id = 5
                                                                                                                AND pb.member_id NOT IN (
                                                                                                                  SELECT lq.member_id
                                                                                                                  FROM leader_qualified lq
                                                                                                                  LEFT JOIN pgs_bulanan pb ON lq.member_id = pb.member_id AND lq.tgl = pb.tgl
                                                                                                                  WHERE lq.tgl BETWEEN $awal AND $akhir
                                                                                                                  -- AND lq.jenjang = 5
                                                                                                                  AND pb.jenjang_id = 5
                                                                                                                  AND lq.qty >= 1
                                                                                                                  )
                                                                                                                  AND pb.tgl BETWEEN $awal AND $akhir
                                                                                                                  AND pb.member_id NOT IN ('00000001')
                                                                                                                  ) AS dt
                                                                                                                  WHERE cluster = $cluster_id
                                                                                                                  -- ".$qryoms."
                                                                                                                  -- ".$qryact."
                                                                                                                  ORDER BY nama -- cluster, region
                                                                                                                  ";
                                                                                                                  $qry = $this->db->query($query);
                                                                                                                  //echo $this->db->last_query();

                                                                                                                  if($qry->num_rows()>0){
                                                                                                                    foreach($qry->result_array() as $row){
                                                                                                                      $data[]=$row;
                                                                                                                    }
                                                                                                                  }
                                                                                                                  $qry->free_result();
                                                                                                                  return $data;


                                                                                                                }

                                                                                                                public function searchnewsl($tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                                                                                                                  $data = array();
                                                                                                                  //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                                                  //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                                                  $awal = "'".$tglawal."' ".$br;
                                                                                                                  $akhir = "'".$tglakhir."' ".$br;

                                                                                                                  if($omset=='yes'){
                                                                                                                    $qryoms = 'HAVING omset <> 0'	;
                                                                                                                  }else{
                                                                                                                    $qryoms = ''	;
                                                                                                                  }
                                                                                                                  if($act=='yes'){
                                                                                                                    $qryact = 'AND omset >= 0'	;
                                                                                                                  }else{
                                                                                                                    $qryact = ''	;
                                                                                                                  }
                                                                                                                  $query = $query1."
                                                                                                                  SELECT
                                                                                                                  member_id
                                                                                                                  ,nama
                                                                                                                  ,cluster
                                                                                                                  ,region
                                                                                                                  FROM (
                                                                                                                    SELECT
                                                                                                                    lq.member_id,m.nama
                                                                                                                    -- , CASE WHEN m.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                                                                                                                    -- , CASE WHEN m.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                                                                                                                    , k.region AS region
                                                                                                                    , ppc.cluster_id AS cluster
                                                                                                                    FROM qualified_sl lq
                                                                                                                    LEFT JOIN member m ON lq.member_id = m.id
                                                                                                                    LEFT JOIN stockiest s ON m.stockiest_id = s.id
                                                                                                                    LEFT JOIN kota k ON m.kota_id = k.id
                                                                                                                    LEFT JOIN kota ks ON s.kota_id = ks.id
                                                                                                                    LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                                                    LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                                                    LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                                                                                                                    LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                                                                                                                    LEFT JOIN (
                                                                                                                      SELECT lq.member_id , SUM(lq.q_sl) AS qualified
                                                                                                                      FROM qualified_sl lq
                                                                                                                      WHERE lq.jenjang = 6
                                                                                                                      AND lq.q_sl >= 1
                                                                                                                      AND lq.periode BETWEEN '2009-01-01' AND $akhir
                                                                                                                      GROUP BY lq.member_id
                                                                                                                      ) AS dt ON lq.member_id = dt.member_id
                                                                                                                      WHERE
                                                                                                                      lq.jenjang = 6
                                                                                                                      AND lq.q_sl >= 1
                                                                                                                      AND lq.periode BETWEEN $awal AND $akhir
                                                                                                                      AND dt.qualified = 1
                                                                                                                      ) AS dt
                                                                                                                      WHERE cluster = $cluster_id
                                                                                                                      -- ".$qryoms."
                                                                                                                      -- ".$qryact."
                                                                                                                      ORDER BY nama -- cluster, region
                                                                                                                      ";
                                                                                                                      $qry = $this->db->query($query);
                                                                                                                      //echo $this->db->last_query();

                                                                                                                      if($qry->num_rows()>0){
                                                                                                                        foreach($qry->result_array() as $row){
                                                                                                                          $data[]=$row;
                                                                                                                        }
                                                                                                                      }
                                                                                                                      $qry->free_result();
                                                                                                                      return $data;


                                                                                                                    }
                                                                                                                    public function searcholdqsl($tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                                                                                                                      $data = array();
                                                                                                                      //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                                                      //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                                                      $awal = "'".$tglawal."' ".$br;
                                                                                                                      $akhir = "'".$tglakhir."' ".$br;

                                                                                                                      if($omset=='yes'){
                                                                                                                        $qryoms = 'HAVING omset <> 0'	;
                                                                                                                      }else{
                                                                                                                        $qryoms = ''	;
                                                                                                                      }
                                                                                                                      if($act=='yes'){
                                                                                                                        $qryact = 'AND omset >= 0'	;
                                                                                                                      }else{
                                                                                                                        $qryact = ''	;
                                                                                                                      }
                                                                                                                      $query = $query1."
                                                                                                                      SELECT
                                                                                                                      member_id
                                                                                                                      ,nama
                                                                                                                      ,cluster
                                                                                                                      ,region
                                                                                                                      FROM (
                                                                                                                        SELECT
                                                                                                                        lq.member_id,m.nama
                                                                                                                        -- , CASE WHEN m.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                                                                                                                        -- , CASE WHEN m.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                                                                                                                        , k.region AS region
                                                                                                                        , ppc.cluster_id AS cluster
                                                                                                                        FROM qualified_sl lq
                                                                                                                        LEFT JOIN pgs_bulanan pb ON lq.member_id = pb.member_id AND lq.periode = pb.tgl
                                                                                                                        LEFT JOIN member m ON lq.member_id = m.id
                                                                                                                        LEFT JOIN stockiest s ON m.stockiest_id = s.id
                                                                                                                        LEFT JOIN kota k ON m.kota_id = k.id
                                                                                                                        LEFT JOIN kota ks ON s.kota_id = ks.id
                                                                                                                        LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                                                        LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                                                        LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                                                                                                                        LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                                                                                                                        LEFT JOIN (
                                                                                                                          SELECT lq.member_id , SUM(lq.q_sl) AS qualified
                                                                                                                          FROM qualified_sl lq
                                                                                                                          LEFT JOIN pgs_bulanan pb ON lq.member_id = pb.member_id AND lq.periode = pb.tgl
                                                                                                                          WHERE
                                                                                                                          -- lq.jenjang = 6
                                                                                                                          pb.jenjang_id = 6
                                                                                                                          AND lq.q_sl >= 1
                                                                                                                          AND lq.periode BETWEEN '2009-01-01' AND $akhir
                                                                                                                          GROUP BY lq.member_id
                                                                                                                          ) AS dt ON lq.member_id = dt.member_id
                                                                                                                          WHERE
                                                                                                                          -- lq.jenjang = 6
                                                                                                                          pb.jenjang_id = 6
                                                                                                                          AND lq.q_sl >= 1
                                                                                                                          AND lq.periode BETWEEN $awal AND $akhir
                                                                                                                          AND dt.qualified > 1
                                                                                                                          ) AS dt
                                                                                                                          WHERE cluster = $cluster_id
                                                                                                                          -- ".$qryoms."
                                                                                                                          -- ".$qryact."
                                                                                                                          ORDER BY nama -- cluster, region
                                                                                                                          ";
                                                                                                                          $qry = $this->db->query($query);
                                                                                                                          //echo $this->db->last_query();

                                                                                                                          if($qry->num_rows()>0){
                                                                                                                            foreach($qry->result_array() as $row){
                                                                                                                              $data[]=$row;
                                                                                                                            }
                                                                                                                          }
                                                                                                                          $qry->free_result();
                                                                                                                          return $data;


                                                                                                                        }
                                                                                                                        public function searcholdnqsl($tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                                                                                                                          $data = array();
                                                                                                                          //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                                                          //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                                                          $awal = "'".$tglawal."' ".$br;
                                                                                                                          $akhir = "'".$tglakhir."' ".$br;

                                                                                                                          if($omset=='yes'){
                                                                                                                            $qryoms = 'HAVING omset <> 0'	;
                                                                                                                          }else{
                                                                                                                            $qryoms = ''	;
                                                                                                                          }
                                                                                                                          if($act=='yes'){
                                                                                                                            $qryact = 'AND omset >= 0'	;
                                                                                                                          }else{
                                                                                                                            $qryact = ''	;
                                                                                                                          }
                                                                                                                          $query = $query1."
                                                                                                                          SELECT
                                                                                                                          member_id
                                                                                                                          ,nama
                                                                                                                          ,cluster
                                                                                                                          ,region
                                                                                                                          FROM (
                                                                                                                            SELECT
                                                                                                                            pb.member_id
                                                                                                                            , m.nama
                                                                                                                            -- , CASE WHEN m.stockiest_id <> 0 THEN ks.region ELSE k.region END AS region
                                                                                                                            -- , CASE WHEN m.stockiest_id <> 0 THEN ppcs.cluster_id ELSE ppc.cluster_id END AS cluster
                                                                                                                            , k.region AS region
                                                                                                                            , ppc.cluster_id AS cluster
                                                                                                                            FROM pgs_bulanan pb
                                                                                                                            LEFT JOIN member m ON pb.member_id = m.id
                                                                                                                            LEFT JOIN stockiest s ON m.stockiest_id = s.id
                                                                                                                            LEFT JOIN kota k ON m.kota_id = k.id
                                                                                                                            LEFT JOIN kota ks ON s.kota_id = ks.id
                                                                                                                            LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                                                            LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                                                            LEFT JOIN propinsi ps ON ks.propinsi_id = ps.id
                                                                                                                            LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ps.id = ppcs.id
                                                                                                                            WHERE pb.jenjang_id = 6
                                                                                                                            AND pb.member_id NOT IN (
                                                                                                                              SELECT lq.member_id
                                                                                                                              FROM qualified_sl lq
                                                                                                                              LEFT JOIN pgs_bulanan pb ON lq.member_id = pb.member_id AND lq.periode = pb.tgl
                                                                                                                              WHERE lq.periode BETWEEN $awal AND $akhir
                                                                                                                              -- AND lq.jenjang = 6
                                                                                                                              AND pb.jenjang_id = 6
                                                                                                                              AND lq.q_sl >= 1
                                                                                                                              )
                                                                                                                              AND pb.tgl BETWEEN $awal AND $akhir
                                                                                                                              AND pb.member_id NOT IN ('00000001')
                                                                                                                              ) AS dt
                                                                                                                              WHERE cluster = $cluster_id
                                                                                                                              -- ".$qryoms."
                                                                                                                              -- ".$qryact."
                                                                                                                              ORDER BY nama -- cluster, region
                                                                                                                              ";
                                                                                                                              $qry = $this->db->query($query);
                                                                                                                              //echo $this->db->last_query();

                                                                                                                              if($qry->num_rows()>0){
                                                                                                                                foreach($qry->result_array() as $row){
                                                                                                                                  $data[]=$row;
                                                                                                                                }
                                                                                                                              }
                                                                                                                              $qry->free_result();
                                                                                                                              return $data;


                                                                                                                            }

                                                                                                                            //End Created By ASP 20160125
                                                                                                                            //Start Created By ASP 20160620
                                                                                                                            public function searchStockiestSalesMtdOthers($region='',$tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                                                                                                                              $data = array();
                                                                                                                              //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                                                              //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                                                              $awal = "'".$tglawal."' ".$br;
                                                                                                                              $akhir = "'".$tglakhir."' ".$br;

                                                                                                                              if($omset=='yes'){
                                                                                                                                $qryoms = 'HAVING omset <> 0'	;
                                                                                                                              }else{
                                                                                                                                $qryoms = ''	;
                                                                                                                              }
                                                                                                                              if($act=='yes'){
                                                                                                                                $qryact = 'AND omset >= 35000000'	;
                                                                                                                              }else{
                                                                                                                                $qryact = ''	;
                                                                                                                              }
                                                                                                                              if($region!=''){
                                                                                                                                $qryreg = 'AND k.region = '.$region	;
                                                                                                                              }else{
                                                                                                                                $qryreg = ''	;
                                                                                                                              }
                                                                                                                              $query = $query1."
                                                                                                                              SELECT s.id, s.no_stc,m.nama
                                                                                                                              , IFNULL(ro1,0)+IFNULL(pjm1,0)-IFNULL(rtr1,0) AS omset
                                                                                                                              FROM stockiest_active sa
                                                                                                                              LEFT JOIN stockiest s ON sa.id = s.id
                                                                                                                              LEFT JOIN kota k ON s.kota_id = k.id
                                                                                                                              LEFT JOIN member m ON s.id = m.id
                                                                                                                              LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                                                              LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                                                              LEFT JOIN propinsi_cluster pc ON ppc.cluster_id = pc.id
                                                                                                                              LEFT JOIN propinsi_cluster_group ppg ON pc.cluster_group_id = ppg.id
                                                                                                                              LEFT JOIN(
                                                                                                                                SELECT member_id
                                                                                                                                , SUM(oms1)AS ro1
                                                                                                                                FROM(
                                                                                                                                  SELECT member_id
                                                                                                                                  , CASE WHEN ((ro.`date`) <= '2015-02-28') THEN totalharga WHEN ((ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms1
                                                                                                                                  FROM ro
                                                                                                                                  RIGHT JOIN ro_d rod ON ro.id = rod.ro_id
                                                                                                                                  WHERE ro.stockiest_id=0
                                                                                                                                  -- AND rod.pv > 0
                                                                                                                                  AND (
                                                                                                                                    CASE WHEN ro.`date` > '2015-02-28' THEN rod.pv > 0
                                                                                                                                    ELSE rod.pv >= 0
                                                                                                                                    END
                                                                                                                                    )
                                                                                                                                    AND ro.date BETWEEN $awal AND $akhir
                                                                                                                                    GROUP BY ro.id
                                                                                                                                    )AS ro_
                                                                                                                                    GROUP BY member_id
                                                                                                                                    )AS ro ON s.id = ro.member_id
                                                                                                                                    LEFT JOIN(
                                                                                                                                      SELECT member_id
                                                                                                                                      , SUM(oms1)AS pjm1
                                                                                                                                      FROM(
                                                                                                                                        SELECT stockiest_id AS member_id
                                                                                                                                        , CASE WHEN ((pjm.tgl) <= '2015-02-28') OR ((pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
                                                                                                                                        FROM pinjaman_titipan pjm
                                                                                                                                        WHERE pjm.tgl BETWEEN $awal AND $akhir
                                                                                                                                        )AS pjm
                                                                                                                                        GROUP BY member_id
                                                                                                                                        )AS pjm ON s.id = pjm.member_id
                                                                                                                                        LEFT JOIN(
                                                                                                                                          SELECT member_id
                                                                                                                                          , SUM(oms1)AS rtr1
                                                                                                                                          FROM(
                                                                                                                                            SELECT stockiest_id AS member_id
                                                                                                                                            , CASE WHEN ((rtr.tgl) <= '2015-02-28') OR ((rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
                                                                                                                                            FROM retur_titipan rtr
                                                                                                                                            WHERE rtr.tgl BETWEEN $awal AND $akhir
                                                                                                                                            )AS rtr
                                                                                                                                            GROUP BY member_id
                                                                                                                                            )AS rtr ON s.id = rtr.member_id
                                                                                                                                            WHERE pc.id = '$cluster_id'
                                                                                                                                            AND sa.periode BETWEEN $awal AND $akhir
                                                                                                                                            ".$qryact."
                                                                                                                                            ".$qryreg."
                                                                                                                                            ".$qryoms."
                                                                                                                                            order by s.no_stc
                                                                                                                                            ";
                                                                                                                                            $qry = $this->db->query($query);
                                                                                                                                            //echo $this->db->last_query();

                                                                                                                                            if($qry->num_rows()>0){
                                                                                                                                              foreach($qry->result_array() as $row){
                                                                                                                                                $data[]=$row;
                                                                                                                                              }
                                                                                                                                            }
                                                                                                                                            $qry->free_result();
                                                                                                                                            return $data;


                                                                                                                                          }
                                                                                                                                          public function searchStockiestSalesLastYearOthers($region='',$tglawal,$tglakhir,$cluster_id,$omset='no',$act='no'){
                                                                                                                                            $data = array();
                                                                                                                                            //$awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                                                                            //$akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                                                                            $awal = "'".$tglawal."' ".$br;
                                                                                                                                            $akhir = "'".$tglakhir."' ".$br;

                                                                                                                                            if($omset=='yes'){
                                                                                                                                              $qryoms = 'HAVING omset <> 0'	;
                                                                                                                                            }else{
                                                                                                                                              $qryoms = ''	;
                                                                                                                                            }
                                                                                                                                            if($act=='yes'){
                                                                                                                                              $qryact = 'AND omset >= 35000000'	;
                                                                                                                                            }else{
                                                                                                                                              $qryact = ''	;
                                                                                                                                            }
                                                                                                                                            if($region!=''){
                                                                                                                                              $qryreg = 'AND k.region = '.$region	;
                                                                                                                                            }else{
                                                                                                                                              $qryreg = ''	;
                                                                                                                                            }
                                                                                                                                            $query = $query1."
                                                                                                                                            SELECT s.id, s.no_stc,m.nama
                                                                                                                                            , IFNULL(ro1,0)+IFNULL(pjm1,0)-IFNULL(rtr1,0) AS omset
                                                                                                                                            FROM stockiest s
                                                                                                                                            LEFT JOIN kota k ON s.kota_id = k.id
                                                                                                                                            LEFT JOIN member m ON s.id = m.id
                                                                                                                                            LEFT JOIN propinsi p ON k.propinsi_id = p.id
                                                                                                                                            LEFT JOIN propinsi_to_propinsi_cluster ppc ON p.id = ppc.id
                                                                                                                                            LEFT JOIN propinsi_cluster pc ON ppc.cluster_id = pc.id
                                                                                                                                            LEFT JOIN propinsi_cluster_group ppg ON pc.cluster_group_id = ppg.id
                                                                                                                                            LEFT JOIN(
                                                                                                                                              SELECT member_id
                                                                                                                                              , SUM(oms1)AS ro1
                                                                                                                                              FROM(
                                                                                                                                                SELECT member_id
                                                                                                                                                , CASE WHEN ((ro.`date`) <= '2015-02-28') THEN totalharga WHEN ((ro.`date`) > '2015-02-28' AND totalpv > 0) THEN SUM(rod.jmlharga) END AS oms1
                                                                                                                                                FROM ro
                                                                                                                                                RIGHT JOIN ro_d rod ON ro.id = rod.ro_id
                                                                                                                                                WHERE ro.stockiest_id=0
                                                                                                                                                -- AND rod.pv > 0
                                                                                                                                                AND (
                                                                                                                                                  CASE WHEN ro.`date` > '2015-02-28' THEN rod.pv > 0
                                                                                                                                                  ELSE rod.pv >= 0
                                                                                                                                                  END
                                                                                                                                                  )
                                                                                                                                                  AND ro.date BETWEEN $awal AND $akhir
                                                                                                                                                  GROUP BY ro.id
                                                                                                                                                  )AS ro_
                                                                                                                                                  GROUP BY member_id
                                                                                                                                                  )AS ro ON s.id = ro.member_id
                                                                                                                                                  LEFT JOIN(
                                                                                                                                                    SELECT member_id
                                                                                                                                                    , SUM(oms1)AS pjm1
                                                                                                                                                    FROM(
                                                                                                                                                      SELECT stockiest_id AS member_id
                                                                                                                                                      , CASE WHEN ((pjm.tgl) <= '2015-02-28') OR ((pjm.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
                                                                                                                                                      FROM pinjaman_titipan pjm
                                                                                                                                                      WHERE pjm.tgl BETWEEN $awal AND $akhir
                                                                                                                                                      )AS pjm
                                                                                                                                                      GROUP BY member_id
                                                                                                                                                      )AS pjm ON s.id = pjm.member_id
                                                                                                                                                      LEFT JOIN(
                                                                                                                                                        SELECT member_id
                                                                                                                                                        , SUM(oms1)AS rtr1
                                                                                                                                                        FROM(
                                                                                                                                                          SELECT stockiest_id AS member_id
                                                                                                                                                          , CASE WHEN ((rtr.tgl) <= '2015-02-28') OR ((rtr.tgl) > '2015-02-28' AND totalpv > 0) THEN totalharga END AS oms1
                                                                                                                                                          FROM retur_titipan rtr
                                                                                                                                                          WHERE rtr.tgl BETWEEN $awal AND $akhir
                                                                                                                                                          )AS rtr
                                                                                                                                                          GROUP BY member_id
                                                                                                                                                          )AS rtr ON s.id = rtr.member_id
                                                                                                                                                          WHERE pc.id = $cluster_id
                                                                                                                                                          ".$qryact."
                                                                                                                                                          ".$qryreg."
                                                                                                                                                          ".$qryoms."
                                                                                                                                                          order by s.no_stc
                                                                                                                                                          ";
                                                                                                                                                          $qry = $this->db->query($query);
                                                                                                                                                          //echo $this->db->last_query();

                                                                                                                                                          if($qry->num_rows()>0){
                                                                                                                                                            foreach($qry->result_array() as $row){
                                                                                                                                                              $data[]=$row;
                                                                                                                                                            }
                                                                                                                                                          }
                                                                                                                                                          $qry->free_result();
                                                                                                                                                          return $data;


                                                                                                                                                        }
                                                                                                                                                        //End Created By ASP 20160620
                                                                                                                                                        //START Created By ASP 20170928
                                                                                                                                                        public function priceNotDisc($item_id = ''){
                                                                                                                                                          $data = array();
                                                                                                                                                          $awal = "LAST_DAY('$tgl' - INTERVAL 1 MONTH)+ INTERVAL 1 DAY ".$br;
                                                                                                                                                          $akhir = "LAST_DAY('$tgl' + INTERVAL 2 MONTH) ".$br;
                                                                                                                                                          //$awal = "'".$tglawal."' ".$br;
                                                                                                                                                          //$akhir = "'".$tglakhir."' ".$br;


                                                                                                                                                          $query ="
                                                                                                                                                          SELECT
                                                                                                                                                          i.id
                                                                                                                                                          , IFNULL(ind.price_not_disc,0) AS price_not_disc
                                                                                                                                                          , MIN(ind.end_period) AS end_period
                                                                                                                                                          FROM
                                                                                                                                                          item i
                                                                                                                                                          LEFT JOIN item_not_discount ind ON ind.item_id = i.id AND ind.expired = 0 AND ind.end_period >= DATE(NOW())
                                                                                                                                                          WHERE i.id = '".$item_id."'
                                                                                                                                                          GROUP BY i.id
                                                                                                                                                          ";
                                                                                                                                                          $qry = $this->db->query($query);
                                                                                                                                                          //echo $this->db->last_query();

                                                                                                                                                          if($qry->num_rows()>0){
                                                                                                                                                            foreach($qry->result_array() as $row){
                                                                                                                                                              $data[]=$row;
                                                                                                                                                            }
                                                                                                                                                          }
                                                                                                                                                          $qry->free_result();
                                                                                                                                                          return $data;
                                                                                                                                                        }
                                                                                                                                                        //END Created By ASP 20170928
                                                                                                                                                      }?>
