<?php
class Video_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function getDataList($limit = '', $offset = '', $id = '', $title = '', $description = '', $youtube_link = '', $category_id = '', $sort = '', $feature_video = '', $publish = '', $download_link='', $keyword = '')
	{
			$wQuery = 'WHERE';

			if($id != ''){
				$wQuery.= " video_id=".$id;
			}

			if($title != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " title='".$title."'";
				}else{
					$wQuery.= " AND title='".$title."'";
				}
			}

			if($description != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " description='".$description."'";
				}else{
					$wQuery.= " AND description='".$description."'";
				}
			}

			if($youtube_link != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " youtube_link='".$youtube_link."'";
				}else{
					$wQuery.= " AND youtube_link='".$youtube_link."'";
				}
			}

			if($category_id != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " cat_id='".$category_id."'";
				}else{
					$wQuery.= " AND cat_id='".$category_id."'";
				}
			}

			if($sort != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " sort_video='".$sort."'";
				}else{
					$wQuery.= " AND sort_video='".$sort."'";
				}
			}

			if($feature_video != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " feature_video='".$feature_video."'";
				}else{
					$wQuery.= " AND feature_video='".$feature_video."'";
				}
			}

			if($publish != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " publish='".$publish."'";
				}else{
					$wQuery.= " AND publish='".$publish."'";
				}
			}

			if($download_link != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " download_link='".$download_link."'";
				}else{
					$wQuery.= " AND download_link='".$download_link."'";
				}
			}

			if($keyword != '')
			{
				if($wQuery=='WHERE'){
					$wQuery.= " video_id LIKE '%".$keyword."%' OR title LIKE '%".$keyword."%' OR name LIKE '%".$keyword."%'";
				}else{
					$wQuery.= " AND video_id LIKE '%".$keyword."%' OR title LIKE '%".$keyword."%' OR name LIKE '%".$keyword."%'";
				}
			}

			if($limit!=''&&$offset!='')$lQuery = ' LIMIT '.$limit.','.$offset; else $lQuery = '';

			if($wQuery=='WHERE')$wQuery = '';

			$rs = $this->db->query("SELECT * FROM master_video a LEFT JOIN master_category b ON a.cat_id = b.category_id ".$wQuery." ORDER BY sort_video ASC" .$lQuery);
	        
	        $result = array();

	        if ($rs->num_rows() > 0) 
	        {
	            foreach($rs->result_array() as $row ) {
	                $result['data'][] = $row;
	            }

	            foreach($rs->result_object() as $row2 ) {
	                $result['dataObject'] = $row2;
	            }
	        }
	        else
	        {
	        	return FALSE;
	        }

	        $result['countResult']=$rs->num_rows();
	        $rs->free_result();
			return $result;
		}

		public function create($title = '', $description = '', $link = '', $category = '', $sort = '', $thumbnail = '', $feature_video='', $publish='',$download_link='')
		{
			$query1 = "SELECT * FROM master_video WHERE title = '$title'";
			$query2 = "INSERT INTO master_video(title, description, youtube_link, cat_id, sort_video, thumbnail,feature_video,publish,download_link) VALUES('$title','$description', '$link', '$category', '$sort', '$thumbnail', '$feature_video', '$publish','$download_link')";

			$rs1 = $this->db->query($query1);

			if($rs1->num_rows() > 0) {
				return false;
			} else {
				$rs2 = $this->db->query($query2);

				if($rs2) {
					return true;
				} else {
					return false;
				}
			}
		}

		public function edit($id = '', $title = '', $description = '', $link = '', $category = '', $sort = '', $thumbnail = '',$feature_video='', $publish='',$download_link='')
		{
			$query1 = "SELECT * FROM master_video WHERE title = '$title' AND video_id != '$id'";
			$query2 = "UPDATE master_video SET title='$title', description='$description', youtube_link='$link', cat_id='$category', sort_video='$sort', thumbnail='$thumbnail', feature_video='$feature_video', publish='$publish', download_link='$download_link' WHERE video_id='$id'";
			$rs1 = $this->db->query($query1);

			if($rs1->num_rows() > 0) {
				return false;
			} else {
				$rs2 = $this->db->query($query2);

				if($rs2) {
					return true;
				} else {
					return false;
				}
			}
		}

		public function delete($id = '')
		{
			$query = "DELETE FROM master_video WHERE video_id='$id'";
			$rs =  $this->db->query($query);

			if($rs){
				return true;
			} else {
				return false;
			}
		}

}