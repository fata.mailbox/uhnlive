<?php
class MWhatnew extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function searchWhatnew($keywords=0,$num,$offset){
        $data = array();
        $this->db->select('id,title,status,created,createdby',false)
            ->from('whatnew')
            ->like('title', $keywords, 'match')
            ->order_by('id','desc')
            ->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countWhatnew($keywords=0){
        $this->db->like('title', $keywords, 'match');
        $this->db->from('whatnew');
        return $this->db->count_all_results();
    }
    public function addWhatnew(){
        if($this->input->post('status') == 'active'){
            $this->db->update('whatnew',array('status'=>'inactive'));
        }
        
        $data = array(
            'title' => $this->db->escape_str($this->input->post('title')),
            'longdesc' => $_POST['longdesc'],
            'status' => $this->input->post('status'),
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $this->session->userdata('user')
        );
        $this->db->insert('whatnew',$data);
    }
    
    public function updateWhatnew($id){
        $this->db->update('whatnew',array('status'=>'inactive'),array('id'=>$this->input->post('id')));
        
        $data = array(
            'title' => $this->db->escape_str($this->input->post('title')),
            'longdesc' => $_POST['longdesc'],
            'status' => $this->input->post('status'),
            'updated' => date('Y-m-d H:m:s',now()),
            'updatedby' => $this->session->userdata('user')
        );
        
        $this->db->where('id',$id);
        $this->db->update('whatnew',$data);
    }
    
    public function get($id=0){
        $data = array();
        $option = array('id' => $id);
        $q = $this->db->get_where('whatnew',$option,1);
        if($q->num_rows() > 0){
                $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
}
?>