<?php
class MCancelso extends CI_Model{
  function __construct()
  {
    parent::__construct();
  }

  /* Created by Boby 201404025 */
  public function countCancelSo($keywords){
    $data = array();
    $this->db->select("
    so.tgl, cso.so_id_canceled AS so_id, cso.so_id_execute AS so_cancel
    , so.member_id, m.nama, stc.no_stc, s.nama AS namaStc
    , cso.created, cso.createdby
    ",false);
    $this->db->from('canceled_so cso');
    $this->db->join('so','cso.so_id_canceled = so.id','left');
    $this->db->join('member m','so.member_id = m.id','left');
    $this->db->join('stockiest stc','so.stockiest_id = stc.id','left');
    $this->db->join('member s','stc.id = s.id','left');
    $this->db->like('cso.so_id_canceled',$keywords,'between');
    $this->db->or_like('cso.so_id_execute',$keywords,'between');
    $this->db->or_like('m.id',$keywords,'between');
    $this->db->or_like('m.nama',$keywords,'between');
    $this->db->or_like('s.id',$keywords,'between');
    $this->db->or_like('s.nama',$keywords,'between');
    $this->db->or_like('stc.no_stc',$keywords,'between');
    $this->db->order_by('cso.id','desc');
    // $this->db->limit($num,$offset);
    // $q = $this->db->get();
    // echo $this->db->last_query();
    return $this->db->count_all_results();
  }

  public function getCancelSo($keywords, $num, $offset){
    $data = array();
    $this->db->select("
    so.tgl, cso.so_id_canceled AS so_id, cso.so_id_execute AS so_cancel
    , so.member_id, m.nama, stc.no_stc, s.nama AS namaStc
    , cso.created, cso.createdby
    ",false);
    $this->db->from('canceled_so cso');
    $this->db->join('so','cso.so_id_canceled = so.id','left');
    $this->db->join('member m','so.member_id = m.id','left');
    $this->db->join('stockiest stc','so.stockiest_id = stc.id','left');
    $this->db->join('member s','stc.id = s.id','left');
    $this->db->like('cso.so_id_canceled',$keywords,'between');
    $this->db->or_like('cso.so_id_execute',$keywords,'between');
    $this->db->or_like('m.id',$keywords,'between');
    $this->db->or_like('m.nama',$keywords,'between');
    $this->db->or_like('s.id',$keywords,'between');
    $this->db->or_like('s.nama',$keywords,'between');
    $this->db->or_like('stc.no_stc',$keywords,'between');
    $this->db->order_by('cso.id','desc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    // echo $this->db->last_query();
    if($q->num_rows >0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }

  //20160724 Andi Satya Perdana Start
  public function getCancelSoNew($keywords, $num, $offset){
    $data = array();
    $this->db->select("
    so.tgl, cso.so_id_canceled AS so_id, cso.so_id_execute AS so_cancel
    , so.member_id, m.nama, stc.no_stc, s.nama AS namaStc
    , cso.created
    , ifnull(csr.remark,'-') as remark
    , cso.createdby
    ",false);
    $this->db->from('canceled_so cso');
    $this->db->join('so','cso.so_id_canceled = so.id','left');
    $this->db->join('member m','so.member_id = m.id','left');
    $this->db->join('stockiest stc','so.stockiest_id = stc.id','left');
    $this->db->join('member s','stc.id = s.id','left');
    $this->db->join('canceled_so_remark csr','cso.so_id_canceled = csr.so_id_canceled','left');
    $this->db->like('cso.so_id_canceled',$keywords,'between');
    $this->db->or_like('cso.so_id_execute',$keywords,'between');
    $this->db->or_like('m.id',$keywords,'between');
    $this->db->or_like('m.nama',$keywords,'between');
    $this->db->or_like('s.id',$keywords,'between');
    $this->db->or_like('s.nama',$keywords,'between');
    $this->db->or_like('stc.no_stc',$keywords,'between');
    $this->db->order_by('cso.id','desc');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    // echo $this->db->last_query();
    if($q->num_rows >0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }

  //20160724 Andi Satya Perdana End
  public function getInfoSo($keyword){
    $data = array();
    //$leader = 1;
    $query = "
    SELECT so.id, CASE
    WHEN cso.so_id_canceled > 0 THEN 1
    WHEN cso1.so_id_execute > 0 THEN 2
    WHEN so.tgl <= (SELECT MAX(periode) FROM bonus LIMIT 0,1) THEN 3
    WHEN so.kit = 'y' THEN 4
    ELSE 0
    END AS note
    FROM so
    LEFT JOIN canceled_so cso ON so.id = cso.so_id_canceled
    LEFT JOIN canceled_so cso1 ON so.id = cso1.so_id_execute
    WHERE so.id = '$keyword'
    ";

    $qry = $this->db->query($query);

    // echo $this->db->last_query();
    if($qry->num_rows() > 0){
      $data = $qry->row_array();
    }
    $qry->free_result();
    return $data;
  }

  public function cancelSo(){
    $so_id = $this->input->post('so_id');
    $empid = $this->session->userdata('username');
    $this->db->query("CALL cancel_so_procedure('$so_id','$empid')");
    $this->db->close(); // updated by Boby 20141129
  }

  //20160724 Andi Satya Perdana Start
  public function cancelSoNew(){
    $so_id = $this->input->post('so_id');
    $remark_in = $this->input->post('remark');
    $empid = $this->session->userdata('username');
    $this->db->query("CALL cancel_so_procedure_new('$so_id','$empid','$remark_in')");
    $this->db->close(); // updated by Boby 20141129
  }
  //20160724 Andi Satya Perdana End
  /* End created by Boby 201404025 */

  public function cancelSoNew2(){
    $so_id = $this->input->post('so_id');
    $remark_in = $this->input->post('remark');
    $empid = $this->session->userdata('username');

	  $checkVoucher = $this->db->query("SELECT * FROM voucher WHERE so_id = ".$so_id)->result();
  
  
    $qry = $this->db->query("SELECT id,topup_id from so_d where so_id = '$so_id' and topup_id != '' ")->row_array();
  
    if (!empty($qry)) {
  
      $topup_id = $qry["topup_id"];
      $r = $this->db->query("SELECT multiple from topupbyvalue where topupno = '$topup_id' ")->row_array();
      if ($r['multiple'] == 0 ) {
           $this->db->set('topup_id','');
           $this->db->where('id', $qry['id']);
           $this->db->update('so_d');
           
      }
      
   
      
    }
  
    if(count($checkVoucher) > 0){
		foreach($checkVoucher as $rows){

    $cari = $this->db->query("SELECT id_voucher from voucher_user_detail where vouchercode = '$rows->vouchercode' ")->row_array();
	
			$dataV = array(
				'price' 		=> $rows->price,
				'pv' 			=> $rows->pv,
				'bv'			=> $rows->bv,
				'member_id'		=> $rows->member_id,
				'stc_id'		=> $rows->stc_id,
				'status'		=> 0,
        'posisi'		=> 0,
        'remark'		=> 'ex voucher code '.$rows->vouchercode.'<=>'.nl2br('SO => '.$so_id).'<=>'.nl2br($remark_in),
				'expired_date'	=> $rows->expired_date,
				'so_id'			=> null,
				'sod_id'		=> null,
				'so_item_id'	=> null,
				'so_item_qty'	=> null,
				'ro_id' 		=> null,
				'rod_id'		=> null,
				'ro_item_id' 	=> null,
				'ro_item_qty' 	=> null,
				'minorder'		=> $rows->minorder,
			);
      $this->db->insert('voucher', $dataV);
      
      $insertId = $this->db->insert_id();

      $data = 
      [
         'vouchercode' =>  $insertId ,
         'id_voucher' =>$cari['id_voucher'], 
      ];
      
      $this->db->insert('voucher_user_detail', $data);
		}
	}
	
    $this->db->query("CALL cancel_so_procedure_new_d('$so_id','$empid','$remark_in')");
    $this->db->close(); // updated by Boby 20141129



$qry = "SELECT so_id_execute FROM canceled_so where so_id_canceled = '$so_id' order by id desc";
$exec = $this->db->query($qry)->row()->so_id_execute;
$ZUO2 = $this->db->query("SELECT * FROM ro_staging where U_UHINV = '$so_id' AND U_AUART = 'ZU02' ")->result_array();
$i = 0;





foreach ($ZUO2 as $key) {
  if ($key['U_SUBNR'] == 0) {
      $subnr = 0; 
  }else{
      $item = $key['MATNR'];
      $select = $this->db->query("SELECT id from so_d where so_id = '$exec' and item_id = '$item' ")->row();    
      if (empty($select)) {
        $item_prod = $key['YYBANDCD'];
        $cekyymb = $this->db->query("SELECT id from so_d where so_id = '$exec' and item_id = '$item_prod' ")->row();
        $subnr = $cekyymb->id;
      }else{
        $subnr = $select->id;
      }
  }


  $stg = array(
        'U_AUART' => 'ZU06',
        'U_UHINV' => $exec,
        'U_POSNR' => $key['U_POSNR'],
        'U_SUBNR' => $subnr,
        'U_SODAT' => $key['U_SODAT'],
        'U_KUNNR' => $key['U_KUNNR'],
        'U_CTYPE' => $key['U_CTYPE'],
        'U_PL_WE' => $key['U_PL_WE'],
        'U_TAXCLASS' => $key['U_TAXCLASS'],
        'VKBUR' => $key['VKBUR'],
        'WERKS' => $key['WERKS'],
        'MATNR' => $key['MATNR'],
        'ARKTX' => $key['ARKTX'],
        'MATKL' => $key['MATKL'],
        'CHARG' => $key['CHARG'],
        'KWMENG' => $key['KWMENG'],
        'COGS' => $key['COGS'],
        'ZUHN' => $key['ZUHN'],
        'ZUA1' => $key['ZUA1'],
        'ZUA2' => $key['ZUA2'],
        'ZUA4' => $key['ZUA4'],
        'U_VCDNO' => $key['U_VCDNO'],
        'U_FGOOD' => $key['U_FGOOD'],
        'PV' => $key['PV'],
        'BV' => $key['BV'],
        'YYBANDCD' => $key['YYBANDCD'],
        'EWALLET_HDR' => $key['EWALLET_HDR'],
        'EWALLET_ITM' => $key['EWALLET_ITM'],
        'ITEM_TYPE' => $key['ITEM_TYPE'],
        'WAERS' => $key['WAERS'],
        'AUGRU' => 'ZU2',
        'U_REF_CNCL_INV' => $key['U_UHINV'],
        'U_REF_CNCL_SUBNR' => $subnr,
        'U_REF_CNCL_AUART' => 'ZU02',
        'UHDAT' => $key['UHDAT'],
        'UHZET' => $key['UHZET'],
        'status' => 'N',
        'description' => 'New Data Cancel SO'
    );
 
   $this->db->insert('ro_staging', $stg);
   $i++;
 
}



     //Push To API
     header('Content-Type: application/json');
     $url =  "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/outbound/outbound_api/cancel_order";
     $ch = curl_init($url);

     $data_post = array(
       "so_id_be" => $so_id,
       "remark" => $remark_in
     );

     $post = json_encode($data_post);

     $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     curl_setopt($ch, CURLOPT_POST, 1);
     curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
     curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
     $result = curl_exec($ch);
     curl_close($ch);

  }

}
