<?php



defined('BASEPATH') OR exit('No direct script access allowed');

class Wallet_model extends CI_Model {
	var $table = 'temp_ewallet'; 
    var $column_order = array('doc_head');
    var $column_search = array('doc_head'); 
    var $order = array('doc_head' => 'ASC');
    
    public function __construct()
    {
        parent::__construct();
        
    }

    private function _get_datatables_query($id)
    {
        $this->db->where('doc_head', $id);
        $this->db->from($this->table);
  
        $i = 0;
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i) 
                 $this->db->group_end(); 
            }
            $i++;
        }
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    public function get_datatables($id)
    {
        $this->_get_datatables_query($id);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $this->db->where('doc_head', $id);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function count_filtered($id)
    {
        $this->_get_datatables_query($id);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
}

/* End of file wallet_model.php */
