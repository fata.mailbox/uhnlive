<?php
// insert into menu (submenu_id, title, folder, url, `comment`, sort)values(5, 'Stockiest Report', 'stc', 'stc_report', '', 6);
class MStc extends CI_Model{
    function __construct(){parent::__construct();}

	public function dataStc($keyword){
        $data = array();
		$q = "
			SELECT s.id, s.no_stc, m.nama, IFNULL(s.alamat,'-')AS alamat, CONCAT(IFNULL(s.telp,'-'),' / ',IFNULL(s.hp,'-'))AS telp, IFNULL(s.hp,'-')AS hp, IFNULL(k.name,'Unknown') AS kota
			FROM stockiest s
			LEFT JOIN member m ON s.id = m.id
			LEFT JOIN kota k ON s.kota_id = k.id
			WHERE s.id = '$keyword'
		";
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        return $row = ($qry->num_rows() > 0) ? $qry->row_array() : false;
    }
	
	public function omset1($keyword, $periode, $sem){
        $data = array();
		$bln = substr($periode, -5, 2);
		if($sem>1){ 
			$jan = $bln-2;
			if($jan<1) $jan += 12;
		}else{ 
			$jan = $bln-5;
			if($jan<1) $jan += 12;
		}
		//echo ($periode)." ".$bln." ".$jan." ";
		$q = "
SELECT item_id, i.name AS prd
	, IFNULL(SUM(jml1_1),0)AS jml1_1, IFNULL(SUM(oms1_1),0)AS oms1_1
	, IFNULL(SUM(jml1_2),0)AS jml1_2, IFNULL(SUM(oms1_2),0)AS oms1_2
	, IFNULL(SUM(jml1_3),0)AS jml1_3, IFNULL(SUM(oms1_3),0)AS oms1_3
	, IFNULL(SUM(jml2_1),0)AS jml2_1, IFNULL(SUM(oms2_1),0)AS oms2_1
	, IFNULL(SUM(jml2_2),0)AS jml2_2, IFNULL(SUM(oms2_2),0)AS oms2_2
	, IFNULL(SUM(jml2_3),0)AS jml2_3, IFNULL(SUM(oms2_3),0)AS oms2_3
FROM(
	SELECT thn, bln, item_id, jml, omset
		, CASE WHEN thn = ((YEAR('$periode'))-1) AND bln = $jan THEN jml END AS jml1_1, CASE WHEN thn = ((YEAR('$periode'))-1) AND bln = $jan THEN omset END AS oms1_1
		, CASE WHEN thn = ((YEAR('$periode'))-1) AND bln = $jan+1 THEN jml END AS jml1_2, CASE WHEN thn = ((YEAR('$periode'))-1) AND bln = $jan+1 THEN omset END AS oms1_2
		, CASE WHEN thn = ((YEAR('$periode'))-1) AND bln = $jan+2 THEN jml END AS jml1_3, CASE WHEN thn = ((YEAR('$periode'))-1) AND bln = $jan+2 THEN omset END AS oms1_3
		, CASE WHEN thn = (YEAR('$periode')) AND bln = $jan THEN jml END AS jml2_1, CASE WHEN thn = (YEAR('$periode')) AND bln = $jan THEN omset END AS oms2_1
		, CASE WHEN thn = (YEAR('$periode')) AND bln = $jan+1 THEN jml END AS jml2_2, CASE WHEN thn = (YEAR('$periode')) AND bln = $jan+1 THEN omset END AS oms2_2
		, CASE WHEN thn = (YEAR('$periode')) AND bln = $jan+2 THEN jml END AS jml2_3, CASE WHEN thn = (YEAR('$periode')) AND bln = $jan+2 THEN omset END AS oms2_3
	FROM(
		SELECT YEAR(so.tgl)AS thn, MONTH(so.tgl)AS bln, sod.item_id, SUM(sod.qty)AS jml, SUM(sod.jmlharga)AS omset
		FROM so
		LEFT JOIN so_d sod ON so.id = sod.so_id
		WHERE so.stockiest_id = '$keyword'
		AND MONTH(so.tgl) >= $jan
		AND MONTH(so.tgl) <= $jan+2
		AND (YEAR(so.tgl) = (YEAR('$periode')) OR YEAR(so.tgl) = ((YEAR('$periode'))-1))
		GROUP BY thn, bln, sod.item_id
	)AS dt
)AS dt
LEFT JOIN item i ON dt.item_id = i.id
GROUP BY item_id
		";
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        // return $row = ($qry->num_rows() > 0) ? $qry->row_array() : false;
		if($qry->num_rows > 0){
            foreach($qry->result_array() as $row){
                $data[] = $row;
            }
        }
        $qry->free_result();
        return $data;
    }
	
	public function periodeOmset($fromdate,$flag){
		
		if($flag > 0){$fromdate = $fromdate."' - INTERVAL 1 YEAR";
		}else{$fromdate = $fromdate."'";}
		
		$data = array();
		$qry = $this->db->query("
			SELECT CONCAT(MONTHNAME('$fromdate), ' ',YEAR('$fromdate)) AS bln1
			, CONCAT(MONTHNAME('$fromdate + INTERVAL 1 MONTH), ' ',YEAR('$fromdate + INTERVAL 1 MONTH)) AS bln2
			, CONCAT(MONTHNAME('$fromdate + INTERVAL 2 MONTH), ' ',YEAR('$fromdate + INTERVAL 2 MONTH)) AS bln3
			, CONCAT(MONTHNAME('$fromdate + INTERVAL 3 MONTH), ' ',YEAR('$fromdate + INTERVAL 3 MONTH)) AS bln4
			, CONCAT(MONTHNAME('$fromdate + INTERVAL 4 MONTH), ' ',YEAR('$fromdate + INTERVAL 4 MONTH)) AS bln5
			, CONCAT(MONTHNAME('$fromdate + INTERVAL 5 MONTH), ' ',YEAR('$fromdate + INTERVAL 5 MONTH)) AS bln6
			, YEAR('$fromdate - INTERVAL 5 MONTH) AS y1
			, YEAR('$fromdate - INTERVAL 4 MONTH) AS y2
			, YEAR('$fromdate - INTERVAL 3 MONTH) AS y3
			, YEAR('$fromdate - INTERVAL 2 MONTH) AS y4
			, YEAR('$fromdate - INTERVAL 1 MONTH) AS y5
			, YEAR('$fromdate) AS y6
			, MONTHNAME('$fromdate - INTERVAL 5 MONTH) AS b1
			, MONTHNAME('$fromdate - INTERVAL 4 MONTH) AS b2
			, MONTHNAME('$fromdate - INTERVAL 3 MONTH) AS b3
			, MONTHNAME('$fromdate - INTERVAL 2 MONTH) AS b4
			, MONTHNAME('$fromdate - INTERVAL 1 MONTH) AS b5
			, MONTHNAME('$fromdate) AS b6
		");
        if($qry->num_rows()>0){
            $data = $qry->row_array();
        }
        $qry->free_result();
        return $data;
    }
	
	public function newMember($keyword, $periode){
		//substr($periode, -5, 2);
		$jan = (substr($periode, -5, 2))-5;
		$data = array();
		$qry = $this->db->query("
			SELECT stockiest_id
				, IFNULL(SUM(jml1_1),0)AS jml1_1
				, IFNULL(SUM(jml1_2),0)AS jml1_2
				, IFNULL(SUM(jml1_3),0)AS jml1_3
				, IFNULL(SUM(jml1_4),0)AS jml1_4
				, IFNULL(SUM(jml1_5),0)AS jml1_5
				, IFNULL(SUM(jml1_6),0)AS jml1_6
				, IFNULL(SUM(jml2_1),0)AS jml2_1
				, IFNULL(SUM(jml2_2),0)AS jml2_2
				, IFNULL(SUM(jml2_3),0)AS jml2_3
				, IFNULL(SUM(jml2_4),0)AS jml2_4
				, IFNULL(SUM(jml2_5),0)AS jml2_5
				, IFNULL(SUM(jml2_6),0)AS jml2_6
			FROM(
				SELECT stockiest_id
					, CASE WHEN bln = $jan AND thn = ((YEAR('$periode'))-1) THEN jml END AS jml1_1
					, CASE WHEN bln = $jan+1 AND thn = ((YEAR('$periode'))-1) THEN jml END AS jml1_2
					, CASE WHEN bln = $jan+2 AND thn = ((YEAR('$periode'))-1) THEN jml END AS jml1_3
					, CASE WHEN bln = $jan+3 AND thn = ((YEAR('$periode'))-1) THEN jml END AS jml1_4
					, CASE WHEN bln = $jan+4 AND thn = ((YEAR('$periode'))-1) THEN jml END AS jml1_5
					, CASE WHEN bln = $jan+5 AND thn = ((YEAR('$periode'))-1) THEN jml END AS jml1_6
					, CASE WHEN bln = $jan AND thn = YEAR('$periode') THEN jml END AS jml2_1
					, CASE WHEN bln = $jan+1 AND thn = YEAR('$periode') THEN jml END AS jml2_2
					, CASE WHEN bln = $jan+2 AND thn = YEAR('$periode') THEN jml END AS jml2_3
					, CASE WHEN bln = $jan+3 AND thn = YEAR('$periode') THEN jml END AS jml2_4
					, CASE WHEN bln = $jan+4 AND thn = YEAR('$periode') THEN jml END AS jml2_5
					, CASE WHEN bln = $jan+5 AND thn = YEAR('$periode') THEN jml END AS jml2_6
				FROM(
					SELECT stockiest_id, MONTH(so.tgl)AS bln, YEAR(so.tgl)AS thn, COUNT(id)AS jml
					FROM so
					WHERE kit = 'y'
					AND stockiest_id = '$keyword'
					AND MONTH(so.tgl) >= $jan
					AND MONTH(so.tgl) <= $jan+5
					AND (YEAR(so.tgl) = (YEAR('$periode')) OR YEAR(so.tgl) = ((YEAR('$periode'))-1))
					GROUP BY MONTH(so.tgl), YEAR(so.tgl)
				)AS dt
			)AS dt
		");
		//echo $this->db->last_query();
		if($qry->num_rows()>0){
            $data = $qry->row_array();
        }
        $qry->free_result();
        return $data;
	}
	
	public function activeMember($fromdate){
		
		if($flag > 0){$fromdate = $fromdate."' - INTERVAL 1 YEAR";
		}else{$fromdate = $fromdate."'";}
		
		$data = array();
		$qry = $this->db->query("
			
		");
        if($qry->num_rows()>0){
            $data = $qry->row_array();
        }
        $qry->free_result();
        return $data;
    }
}?>