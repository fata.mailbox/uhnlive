<?php
class MItem extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }

  /*
  |--------------------------------------------------------------------------
  | Master Item
  |--------------------------------------------------------------------------
  |
  | @created 2009-03-29
  | @author qtakwa@yahoo.com@yahoo.com
  |
  */
  public function searchItem($keywords = 0, $num, $offset)
  {
    $data = array();
    $this->db->select("a.id,a.name,t.name as type,a.sales,a.display,a.manufaktur", false);
    $this->db->from('item a');
    $this->db->join('type t', 'a.type_id=t.id', 'left');
    $this->db->like('a.id', $keywords, 'between');
    $this->db->or_like('a.name', $keywords, 'between');
    $this->db->order_by('a.id', 'DESC');
    $this->db->limit($num, $offset);
    $q = $this->db->get();
    //echo $this->db->last_query();
    if ($q->num_rows > 0) {
      foreach ($q->result_array() as $row) {
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function countItem($keywords = 0)
  {
    $this->db->like('a.id', $keywords, 'between');
    $this->db->or_like('a.name', $keywords, 'between');
    $this->db->from('item a');
    return $this->db->count_all_results();
  }
  public function addItem($image)
  {
    $id = $this->input->post('id');
    $price = str_replace(".", "", $this->input->post('price'));
    $price2 = str_replace(".", "", $this->input->post('price2')); // created by Boby 2014-04-04
    $pricecust = str_replace(".", "", $this->input->post('pricecust'));
    $pv = str_replace(".", "", $this->input->post('pv'));
    $bv = str_replace(".", "", $this->input->post('bv'));
    $topup = str_replace(".", "", $this->input->post('topup'));
    if ($image) {
      $data = array(
        'id' => $id,
        'name' => $this->input->post('name'),
        'headline' => $this->db->escape_str($this->input->post('headline')),
        'description' => $_POST['longdesc'],
        'image' => $image,
        'sales' => $this->input->post('sales'),
        'display' => $this->input->post('display'),
        'type_id' => $this->input->post('type'),
        'manufaktur' => $this->input->post('manufaktur'),
        'price' => $price,
        'price2' => $price2, // created by Boby 2014-04-04
        'pv' => $pv,
        'bv' => $bv,
        'pricecust' => $pricecust,
        'minbuy' => $minbuy, //created by Andri Pratama 2018
        'warehouse_id' => $this->input->post('warehouse_id'), //created by Andri Pratama 2018
        'promo' => $this->input->post('promo'),           // created by Takwa 2013
        'new' => $this->input->post('new'),              // created by Takwa 2013
        'subcategory_id' => $this->input->post('subcategory_id'),   // created by Takwa 2013
        'category_id' => $this->input->post('category_id'),
        'created' => date('Y-m-d H:i:s', now()),
        'createdby' => $this->session->userdata('user'),
        'topup' => $topup
      );
    } else {
      $data = array(
        'id' => $id,
        'name' => $this->input->post('name'),
        'headline' => $this->db->escape_str($this->input->post('headline')),
        'description' => $_POST['longdesc'],
        'sales' => $this->input->post('sales'),
        'display' => $this->input->post('display'),
        'type_id' => $this->input->post('type'),
        'manufaktur' => $this->input->post('manufaktur'),
        'price' => $price,
        'price2' => $price2,
        'pv' => $pv,
        'bv' => $bv,
        'pricecust' => $pricecust,
        'minbuy' => $minbuy, //created by Andri Pratama 2018
        'warehouse_id' => $this->input->post('warehouse_id'), //created by Andri Pratama 2018
        'promo' => $this->input->post('promo'),           // created by Takwa 2013
        'new' => $this->input->post('new'),              // created by Takwa 2013
        'subcategory_id' => $this->input->post('subcategory_id'),  // created by Takwa 2013
        'category_id' => $this->input->post('category_id'),      // created by Takwa 2013
        'created' => date('Y-m-d H:i:s', now()),
        'createdby' => $this->session->userdata('user'),
        'topup' => $topup
      );
    }
    $this->db->insert('item', $data);

    $this->db->query("call sp_item('$id')");
  }

  public function getItem($id)
  {
    $data = array();
    /* Modified by Andri Pratama 2018 */
    $this->db->select("a.id,a.name,a.type_id,t.name as type,a.price,a.pv,a.bv,a.pricecust,format(a.price,0)as fprice,
    a.price2,format(a.price2,0)as fprice2,
    format(a.pv,0)as fpv,format(a.bv,0)as fbv,format(a.pricecust,0)as fpricecust,a.sales,a.display,
    a.manufaktur,a.image,a.headline,a.description
    ,a.new,a.promo,a.category_id,a.subcategory_id,a.minbuy
    ,a.warehouse_id,a.topup
    ,date_format(a.created,'%d-%b-%Y')as created,a.createdby,a.updated,a.updatedby", false);
    /* End modified by Andri pratama 2018 */
    $this->db->from('item a');
    $this->db->join('type t', 'a.type_id=t.id', 'left');
    $this->db->where('a.id', $id);
    $q = $this->db->get();

    if ($q->num_rows() > 0) {
      $data = $q->row();
    }

    $q->free_result();
    return $data;
  }
  public function editItem($image)
  {
    $id = $this->input->post('id');
    $price = str_replace(".", "", $this->input->post('price'));
    $price2 = str_replace(".", "", $this->input->post('price2')); // created by Boby 2014-04-04
    $pricecust = str_replace(".", "", $this->input->post('pricecust'));
    $pv = str_replace(".", "", $this->input->post('pv'));
    $bv = str_replace(".", "", $this->input->post('bv'));
    $topup = str_replace(".", "", $this->input->post('topup'));
    if ($image) {
      $data = array(
        'name' => $this->input->post('name'),
        'headline' => $this->db->escape_str($this->input->post('headline')),
        'description' => $_POST['longdesc'],
        'image' => $image,
        'sales' => $this->input->post('sales'),
        'display' => $this->input->post('display'),
        'type_id' => $this->input->post('type'),
        'manufaktur' => $this->input->post('manufaktur'),
        'price' => $price,
        'price2' => $price2, // created by Boby 2014-04-04
        'pv' => $pv,
        'bv' => $bv,
        /* Modified by Takwa 2013 */
        'pricecust' => $pricecust,
        'promo' => $this->input->post('promo'),
        'new' => $this->input->post('new'),
        'subcategory_id' => $this->input->post('subcategory_id'),
        'category_id' => $this->input->post('category_id'),
        'minbuy' => $this->input->$minbuy, //created by Andri Pratama 2018
        'warehouse_id' => $this->input->post('warehouse_id'), //Created by Andri Pratama 2018
        /* End modified by Takwa 2013 */
        /* Update by Boby 2011-02-22 */
        //'created' => date('Y-m-d H:i:s',now()),
        //'createdby' => $this->session->userdata('user')
        'updated' => date('Y-m-d H:i:s', now()),
        'updatedby' => $this->session->userdata('user'),
        'topup' => $topup
        /* End update by Boby 2011-02-22 */
      );
    } else {
      $data = array(
        'name' => $this->input->post('name'),
        'headline' => $this->db->escape_str($this->input->post('headline')),
        'description' => $_POST['longdesc'],
        'sales' => $this->input->post('sales'),
        'display' => $this->input->post('display'),
        'type_id' => $this->input->post('type'),
        'manufaktur' => $this->input->post('manufaktur'),
        'price' => $price,
        'price2' => $price2, // created by Boby 2014-04-04
        'pv' => $pv,
        'bv' => $bv,
        'pricecust' => $pricecust,
        /* Modified by Takwa 2013 */
        'promo' => $this->input->post('promo'),
        'new' => $this->input->post('new'),
        'subcategory_id' => $this->input->post('subcategory_id'),
        'category_id' => $this->input->post('category_id'),
        'minbuy' => $this->input->post('minbuy'), //Created by Andri Pratama 2018
        'warehouse_id' => $this->input->post('warehouse_id'), //Created by Andri Pratama 2018
        /* End modified by Takwa 2013 */
        /* Update by Boby 2011-02-22 */
        //'created' => date('Y-m-d H:i:s',now()),
        //'createdby' => $this->session->userdata('user')
        'updated' => date('Y-m-d H:i:s', now()),
        'updatedby' => $this->session->userdata('user'),
        'topup' => $topup
        /* End update by Boby 2011-02-22 */
      );
    }

    $this->db->update('item', $data, array('id' => $this->input->post('id')));
  }
  public function check_productid($id)
  {
    $q = $this->db->select("id", false)
      ->from('item')
      ->where('id', $id)
      ->get();
    return ($q->num_rows() > 0) ? true : false;
  }
  public function getDropDownType()
  {
    $data = array();
    $this->db->order_by('id', 'asc');
    $q = $this->db->get('type');
    if ($q->num_rows > 0) {
      foreach ($q->result_array() as $row) {
        $data[$row['id']] = $row['name'];
      }
    }
    $q->free_result();
    return $data;
  }

  /* Created by Takwa 2013 */
  public function getDropDownCategory()
  {
    $data = array();
    $this->db->order_by('id', 'asc');
    $q = $this->db->get('category_item');
    if ($q->num_rows > 0) {
      foreach ($q->result_array() as $row) {
        $data[$row['id']] = $row['name'];
      }
    }
    $q->free_result();
    return $data;
  }
  public function getDropDownSubCategory()
  {
    $data = array();
    $this->db->order_by('id', 'asc');
    $q = $this->db->get('subcategory_item');
    if ($q->num_rows > 0) {
      $data[$row['id']['0']] = 'Tidak ada sub category';
      foreach ($q->result_array() as $row) {
        $data[$row['id']] = $row['name'];
      }
    }
    $q->free_result();
    return $data;
  }
  /* End created by Takwa 2013 */

  /* Begin Created by Andri Pratama 2018 */
  public function getDropDownWarehouse()
  {
    $data = array();
    $this->db->order_by('id', 'asc');
    $q = $this->db->get('warehouse');
    if ($q->num_rows > 0) {
      $data[$row['id']['0']] = 'Warehouse Transaksi';
      foreach ($q->result_array() as $row) {
        $data[$row['id']] = $row['name'];
      }
    }
    $q->free_result();
    return $data;
  }
  /* End Created by Andri Pratama 2018 */
}
