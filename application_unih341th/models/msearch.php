<?php
class MSearch extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | search item inventory
    |--------------------------------------------------------------------------
    |
    | to popup for all user
    |
    | @param void
    | @return void
    | @author takwa
    | @created 2008-12-27
    |
    */
    public function search($keywords=0, $group='',$num,$offset){
        $data = array();
        $this->db->select("id,name",false);
        $this->db->from('item');
        if($group){
            $where = "group = '$group' and ( id like '%$keywords%' or name like '%$keywords%' )";
            $this->db->where($where);
        }
        else{
            $this->db->like('id', $keywords, 'between');
            $this->db->or_like('name', $keywords, 'between');     
        }
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countSearch($keywords=0,$group){
        $this->db->from('item');
        if($group){
            $where = "group = '$group' and ( id like '%$keywords%' or name like '%$keywords%' )";
            $this->db->where($where);
        }
        else{
            $this->db->like('id', $keywords, 'between');
            $this->db->or_like('name', $keywords, 'between');     
        }

        return $this->db->count_all_results();
    }
    
    /*
    |--------------------------------------------------------------------------
    | search item inventory
    |--------------------------------------------------------------------------
    |
    | to popup for all user
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2008-12-27
    |
    */
    public function itemSearch($keywords=0, $group='',$num,$offset){
        $data = array();
        $this->db->select("id,name,price",false);
        $this->db->from('item');
        if($group == 'ass'){
            // $where = "manufaktur = 'Yes' and ( id like '$keywords%' or name like '$keywords%' )";
		// updated by Boby 2010-05-27
            $where = "manufaktur = 'Yes' and sales = 'Yes' and ( id like '$keywords%' or name like '$keywords%' )";
        }elseif($group == 'all'){
            $where = "id like '$keywords%' or name like '$keywords%' ";
        }elseif($group == 'invin'){
            $where = "manufaktur = 'No' and ( id like '$keywords%' or name like '$keywords%' )";
        }else{
            $where = "manufaktur = 'No' and ( id like '$keywords%' or name like '$keywords%' )";
        }
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        // echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countItemSearch($keywords=0,$group){
        $this->db->select("id",false);
        $this->db->from('item');
        if($group == 'ass'){
            // $where = "manufaktur = 'Yes' and ( id like '$keywords%' or name like '$keywords%' )";
		// updated by Boby 2010-05-27
            $where = "manufaktur = 'Yes' and sales = 'Yes' and ( id like '$keywords%' or name like '$keywords%' )";
        }elseif($group == 'all'){
            $where = "id like '$keywords%' or name like '$keywords%' ";
        }else{
            $where = "manufaktur = 'No' and ( id like '$keywords%' or name like '$keywords%' )";
        }
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Search city & propinsi to profile
    |--------------------------------------------------------------------------
    |
    | @author taQwa
    | @author 2008-12-28
    |
    */
    public function searchCity($keywords=0,$group='',$num, $offset){
        $data = array();
        
        if($group == 'all'){
            $where = "k.name like '$keywords%' or p.name like '$keywords%' ";
        }
        $this->db->select("k.id,k.name as kota,p.name as propinsi, k.timur, k.warehouse_id",false); // Updated by Boby 20140124
        $this->db->from('kota k');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->where($where);
        $this->db->order_by('k.name','asc');
        $this->db->order_by('p.name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countSearchCity($keywords=0,$group=''){
        if($group == 'all'){
            $where = "k.name like '$keywords%' or p.name like '$keywords%' ";
        }
        $this->db->select("k.id",false);
        $this->db->from('kota k');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
    public function searchMember($keywords=0,$num,$offset){
        $data = array();
        if($keywords){
			// updated by Boby 20100616
            // $this->db->select("id,nama,ewallet,format(ewallet,0)as fewallet",false);
			$this->db->select("
				m.id
				, IFNULL(CONCAT(m.nama, '(', s.no_stc, ')'), m.nama)AS nama_
				, m.nama
				, ifnull(k.id,0) as kota_id
				, ifnull(k.name,0) as namakota
				, m.alamat
				, m.hp
				, m.telp
				, m.email
				, m.ewallet
				, format(m.ewallet,0)as fewallet",false);
			// end updated by Boby 20100616
			
            $this->db->from('member m');
			
			// updated by Boby 20100616
			$this->db->join ('kota k','m.kota_id = k.id','left');
			// updated by Boby 20140904
			$this->db->join ('stockiest s','m.id = s.id','left');
			// updated by Boby 20150414
			$this->db->join('users u','m.id=u.id','left');
			$this->db->where("`u`.`banned_id` < 1 AND (m.id LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')");
			// end updated by Boby 20150414
			// end updated by Boby 20140904
			// end updated by Boby 20100616
            
			// $this->db->like('m.id',$keywords,'between');
            // $this->db->or_like('m.nama',$keywords,'between');
            $this->db->order_by('m.nama','asc');
            $this->db->limit($num,$offset);
            $q = $this->db->get();
            //echo $this->db->last_query();
            if($q->num_rows > 0){
                foreach($q->result_array() as $row){
                    $data[] = $row;
                }
            }
            $q->free_result();
        }
        return $data;
    }
	
    public function countMember($keywords=0){
        if($keywords){
		$this->db->select("id,nama,ewallet,format(ewallet,0)as fewallet",false);
            $this->db->from('member');
            $this->db->like('id',$keywords,'between');
            $this->db->or_like('nama',$keywords,'between');
            $this->db->order_by('nama','asc');
            return $this->db->count_all_results();
        }else return 0;    
    }
    
     /*
    |--------------------------------------------------------------------------
    | search item inventory for request titipan
    |--------------------------------------------------------------------------
    |
    | to popup for all user
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2008-12-27
    |
    */
    public function itemROSearch($keywords=0,$num,$offset){
        $data = array();
        $where = "a.sales = 'Yes' and ( a.id like '$keywords%' or a.name like '$keywords%' )";
        // Modified by Boby 20140207
        $this->db->select("a.id,a.name,a.price,a.price2,a.pv,format(a.price,0)as fprice,format(a.pv,0)as fpv,b.name as type",false);
        $this->db->from('item a');
        $this->db->join('type b','a.type_id=b.id','left');
        
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countItemROSearch($keywords=0){
        $where = "a.sales = 'Yes' and ( a.id like '$keywords%' or a.name like '$keywords%' )";
        $this->db->select("a.id",false);
        $this->db->from('item a');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
    /*
    |--------------------------------------------------------------------------
    | search Stockiest
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-16
    |
    */
    public function searchStockiest($keywords=0,$stc,$num,$offset){
	  //$stc = 100;
        $data = array();
		// updated by Boby 20100617
        // $this->db->select("a.id,a.no_stc,m.nama,format(a.ewallet,0)as fewalletstc",false);
		$this->db->select("a.id,k.id as kota_id,k.name as namakota,a.no_stc,m.nama,format(a.ewallet,0)as fewalletstc",false);
		// end updated by Boby 20100617
        
		$this->db->from('stockiest a');
        $this->db->join('member m','a.id=m.id','left');
        
		// updated by Boby 20100617
		$this->db->join('kota k','a.kota_id=k.id','left');
		// end updated by Boby 20100617
		
        $where = "(a.status = 'active' or a.status = 'inactive') and (a.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%') ";
	  //$where .= " and a.id <> '0' ";
        if($stc == '0')$where = "a.id != '$stc' and ".$where;
        elseif($stc == 'mstc')$where = "a.type = 1 and ".$where; //untuk browse hanya stockiest tdk termasuk m-stc
        elseif($stc == 'romstc')$where = "a.type = 2 and ".$where; //untuk browse hanya m-stc
        elseif($stc == 'type')$where = "a.type = '$stc' and ".$where;
        
        $this->db->where($where);
        $this->db->order_by('a.no_stc','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        // echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countStockiest($keywords=0,$stc){
        
        $where = "(a.status = 'active' or a.status = 'inactive') and (a.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%') ";
	  //$where .= " and a.id <> '0' ";
        if($stc == '0')$where = "a.id != '$stc' and ".$where;
        elseif($stc == 'mstc')$where = "a.type = 1 and ".$where; //untuk browse hanya stockiest tdk termasuk m-stc
        elseif($stc == 'romstc')$where = "a.type = 2 and ".$where; //untuk browse hanya m-stc
        elseif($stc == 'type')$where = "a.type = '$stc' and ".$where;
        else $where =$where;
        
        $this->db->select("a.id");
        $this->db->from('stockiest a');
        $this->db->join('member m','a.id=m.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
    /*
    |--------------------------------------------------------------------------
    | search item Titipan stockiest
    |--------------------------------------------------------------------------
    |
    | to popup for all stockiest and admin
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-21
    |
    */
    public function itemSearchTitipan($keywords=0, $group='',$num,$offset){
        $data = array();
        
        $this->db->select("id,item_id,name,harga,fharga,fpv,fqty",false);
        $this->db->from('v_titipan v');
        if($group == 'ttp'){
            $where = "item_id like '$keywords%' or name like '$keywords%' ";
        }
        $this->db->where($where);
        $this->db->group_by('item_id');
        $this->db->group_by('harga');
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countItemSearchTitipan($keywords=0,$group){
        $data = array();
        $this->db->select("item_id as total",false);   
        $this->db->from('v_titipan');
        if($group == 'ttp'){
            $where = "item_id like '$keywords%' or name like '$keywords%' ";
        }
        $this->db->where($where);
        $this->db->group_by('item_id');
        $this->db->group_by('harga');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->num_rows();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | search item Titipan stockiest
    |--------------------------------------------------------------------------
    |
    | to popup for all stockiest and admin
    | @poweredby www.smartindo-technology.com
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-05-10
    |
    */
    public function searchTtp($keywords=0,$num,$offset){
        $data = array();
        $where = "member_id = '".$this->session->userdata('userid')."' and ( item_id like '$keywords%' or name like '$keywords%' )";
        
        $this->db->select("id,item_id,name,harga,pv,fharga,fpv,fqty",false);
        $this->db->from('v_titipan');
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countSearchTtp($keywords=0){
        $data = array();
        $where = "member_id = '".$this->session->userdata('userid')."' and ( item_id like '$keywords%' or name like '$keywords%' )";
        
        $this->db->select("id",false);   
        $this->db->from('v_titipan');
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->num_rows();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | search item inventory for request titipan
    |--------------------------------------------------------------------------
    |
    | to popup for all user
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-05-15
    |
    */
    public function itemSOSearch($keywords=0,$num,$offset){
        $data = array();
        $where = "a.sales = 'Yes' and a.type_id > 1 and ( a.id like '$keywords%' or a.name like '$keywords%' )";
        
        $this->db->select("a.id,a.name,a.price,a.pv,a.bv,format(a.price,0)as fprice,format(a.pv,0)as fpv,format(a.bv,0)as fbv,b.name as type",false);
        $this->db->from('item a');
        $this->db->join('type b','a.type_id=b.id','left');
        
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countItemSOSearch($keywords=0){
        $where = "a.sales = 'Yes' and a.type_id > 1 and ( a.id like '$keywords%' or a.name like '$keywords%' )";
        $this->db->select("a.id",false);
        $this->db->from('item a');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
    /*
    |--------------------------------------------------------------------------
    | search item Titipan stockiest for SO
    |--------------------------------------------------------------------------
    |
    | to popup for all stockiest
    | @poweredby www.smartindo-technology.com
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-05-18
    |
    */
    public function searchTtpSO($keywords=0,$num,$offset){
        $data = array();
        $where = "t.member_id = '".$this->session->userdata('userid')."' and i.type_id > 1 and ( t.item_id like '$keywords%' or i.name like '$keywords%' )";
        
        $this->db->select("t.id,t.item_id,i.name,t.harga,t.pv,i.bv,format(t.harga,0)as fharga,format(t.pv,0)as fpv,format(i.bv,0)as fbv,format(t.qty,0)as fqty",false);
        $this->db->from('titipan t');
	$this->db->join('item i','t.item_id=i.id','left');
        $this->db->where($where);
        $this->db->order_by('i.name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countSearchTtpSO($keywords=0){
        $data = array();
        $where = "t.member_id = '".$this->session->userdata('userid')."' and i.type_id > 1 and ( t.item_id like '$keywords%' or i.name like '$keywords%' )";
        
        $this->db->select("t.id",false);
        $this->db->from('titipan t');
	$this->db->join('item i','t.item_id=i.id','left');
        $this->db->where($where);
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->num_rows();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | search item Titipan stockiest for SO
    |--------------------------------------------------------------------------
    |
    | to popup for all stockiest
    | @poweredby www.smartindo-technology.com
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-05-18
    |
    */
    public function searchTtpStock($keywords=0,$num,$offset,$stcid){
        $data = array();
        if($stcid){
            //$where = "stockiest_id = '$stcid' and type_id > 1 and ( item_id like '$keywords%' or name like '$keywords%' )";
            $where = "stockiest_id = '$stcid' and ( item_id like '$keywords%' or name like '$keywords%' )";
            
            $this->db->select("id,item_id,name,harga,pv,fharga,fpv,fqty",false);
            $this->db->from('v_pinjaman');
            $this->db->where($where);
            $this->db->order_by('name','asc');
            $this->db->limit($num,$offset);
            $q = $this->db->get();
            //echo $this->db->last_query();
            if($q->num_rows() > 0){
                foreach($q->result_array() as $row){
                    $data[] = $row;
                }
            }
            $q->free_result();
        }
        return $data;
    }
    public function countSearchTtpStock($keywords=0,$stcid){
        $data = array();
        //$where = "stockiest_id = '$stcid' and type_id > 1 and ( item_id like '$keywords%' or name like '$keywords%' )";
        $where = "stockiest_id = '$stcid' and ( item_id like '$keywords%' or name like '$keywords%' )";
        
        $this->db->select("id",false);   
        $this->db->from('v_pinjaman');
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->num_rows();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | search item Titipan stockiest
    |--------------------------------------------------------------------------
    |
    | to popup for all stockiest and admin
    | @poweredby www.smartindo-technology.com
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-05-10
    |
    */
    public function searchReturTtp($keywords=0,$num,$offset,$stcid){
        $data = array();
        $where = "member_id = '$stcid' and ( item_id like '$keywords%' or name like '$keywords%' )";
        
        $this->db->select("id,item_id,name,harga,pv,fharga,fpv,fqty",false);
        $this->db->from('v_titipan');
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countReturTtp($keywords=0,$stcid){
        $data = array();
        $where = "member_id = '$stcid' and ( item_id like '$keywords%' or name like '$keywords%' )";
        
        $this->db->select("id",false);   
        $this->db->from('v_titipan');
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->num_rows();
        }
        $q->free_result();
        return $data;
    }
    
    public function search_inv_bom($manufaktur,$keywords=0,$num,$offset){
        $data = array();
        $this->db->select("id,name,format(price,0)as fprice,price",false);
        $this->db->from('item');
        $where = "manufaktur = '$manufaktur' and ( id like '%$keywords%' or name like '%$keywords%' )";
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function count_inv_bom($manufaktur,$keywords=0){
        $this->db->from('item');
        $where = "manufaktur = '$manufaktur' and ( id like '%$keywords%' or name like '%$keywords%' )";
        $this->db->where($where);
        return $this->db->count_all_results();
    }
	
		public function countSearchTtpSONew($keywords=0){
        $data = array();
        //$where = "t.member_id = '".$this->session->userdata('userid')."' and i.type_id > 1 and ( t.item_id like '$keywords%' or i.name like '$keywords%' )";
        $where = "t.member_id = '".$this->session->userdata('userid')."' 
			and i.type_id > 1 
			and ( t.item_id like '$keywords%' or i.name like '$keywords%' )
			and t.qty > 0
		";
		
        $this->db->select("t.id",false);
        $this->db->from('titipan t');
		$this->db->join('item i','t.item_id=i.id','left');
        $this->db->where($where);
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->num_rows();
        }
        $q->free_result();
        return $data;
    }
	
	public function searchTtpSONew($keywords=0,$num,$offset){
        $data = array();
        $where = "t.member_id = '".$this->session->userdata('userid')."' 
			and i.type_id > 1 
			and ( t.item_id like '$keywords%' or i.name like '$keywords%' )
			and t.qty > 0
		";
        $this->db->select("
			t.id,t.item_id,i.name
			,t.harga,t.harga_
			,format(t.harga,0)as fharga, ,format(t.harga_,0)as fharga_
			,t.pv,i.bv,format(t.pv,0)as fpv,format(i.bv,0)as fbv,format(t.qty,0)as fqty",false);
        $this->db->from('titipan t');
		$this->db->join('item i','t.item_id=i.id','left');
        $this->db->where($where);
        $this->db->order_by('i.name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	/* Created by Boby 20130429 */
	public function searchStockiestAct($keywords=0,$stc,$num,$offset){
        $data = array();
		$this->db->select("a.id,k.id as kota_id,k.name as namakota,a.no_stc,m.nama,format(a.ewallet,0)as fewalletstc
				, md.alamat, k.timur, md.kota, md.id as deli_ad
				, a.type as tipe
			",false);
		$this->db->from('stockiest a');
        $this->db->join('member m','a.id=m.id','left');
        $this->db->join('member_delivery md','m.delivery_addr=md.id','left');
		$this->db->join('kota k','md.kota=k.id','left');
		$where = "(a.status = 'active') and (a.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%') ";
        
		if($stc == '0')$where = "a.id != '$stc' and ".$where;
        elseif($stc == 'mstc')$where = "a.type = 1 and ".$where; //untuk browse hanya stockiest tdk termasuk m-stc
        elseif($stc == 'romstc')$where = "a.type = 2 and ".$where; //untuk browse hanya m-stc
        elseif($stc == 'type')$where = "a.type = '$stc' and ".$where;
        
        $this->db->where($where);
        $this->db->order_by('a.no_stc','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        // echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	/* End created by Boby 20130429 */
	
	/* Created by Boby 20130429 */
	public function countStockiestAct($keywords=0,$stc){
        
        $where = "a.status = 'active' and (a.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%') ";
	  //$where .= " and a.id <> '0' ";
        if($stc == '0')$where = "a.id != '$stc' and ".$where;
        elseif($stc == 'mstc')$where = "a.type = 1 and ".$where; //untuk browse hanya stockiest tdk termasuk m-stc
        elseif($stc == 'romstc')$where = "a.type = 2 and ".$where; //untuk browse hanya m-stc
        elseif($stc == 'type')$where = "a.type = '$stc' and ".$where;
        else $where =$where;
        
        $this->db->select("a.id");
        $this->db->from('stockiest a');
        $this->db->join('member m','a.id=m.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
	/* End created by Boby 20130429 */
	
	/* Created by Boby 20140120 */
	public function searchMemberSo($keywords=0,$num,$offset){
        $data = array();
        if($keywords){
			$this->db->select("
				m.id
				, m.nama
				, ifnull(md.kota,0) as kota_id
				, ifnull(k.name,0) as namakota
				, ifnull(k.timur,0)as timur
				, md.alamat
				, md.id as deli_ad
				, m.hp
				, m.telp
				, m.email
				, m.ewallet
				, format(m.ewallet,0)as fewallet",false);
			
            $this->db->from('member m');
			$this->db->join ('member_delivery md','m.delivery_addr = md.id','left');
			$this->db->join ('kota k','md.kota = k.id','left');
			$this->db->like('m.id',$keywords,'between');
            $this->db->or_like('m.nama',$keywords,'between');
            $this->db->order_by('m.nama','asc');
            $this->db->limit($num,$offset);
            $q = $this->db->get();
            // echo $this->db->last_query();
            if($q->num_rows > 0){
                foreach($q->result_array() as $row){
                    $data[] = $row;
                }
            }
            $q->free_result();
        }
        return $data;
    }
	/* End created by Boby 20140120 */
	
	/* Created by Boby 20140310 */
	public function itemROSearch2($keywords=0,$num,$offset){
        $data = array();
        $memberid=$this->session->userdata('userid');
		$timur = 0;
		$data2 = array();
        $q2 = "
			SELECT s.id, k.timur
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_delivery = k.id
			WHERE s.id = '$memberid'
			ORDER BY timur DESC
			";
        $q2 = $this->db->query($q2);
        // echo $this->db->last_query();
		// $timur=$row['timur'];
		
        if($q2->num_rows()>0){
            foreach($q2->result_array()as $row2){
                $timur=$row2['timur'];
            }
        }
		
        $q2->free_result();
        // echo "a".$timur;
		if($timur<1){
			$this->db->select(" a.id, a.name, a.pv, format(a.pv,0)as fpv, b.name as type, a.price, format(a.price,0)as fprice ",false);
        }else{
			$this->db->select(" a.id, a.name, a.pv, format(a.pv,0)as fpv, b.name as type, a.price2 as price, format(a.price2,0)as fprice ",false);
		}
		
        $this->db->from('item a');
        $this->db->join('type b','a.type_id=b.id','left');
        $where = "a.sales = 'Yes' and ( a.id like '$keywords%' or a.name like '$keywords%' )";
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        // echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	/* End created by Boby 20140310 */
	
	/* Created by ASP 20180405 */
	public function itemROSearchpjm($keywords=0,$num,$offset){
        $data = array();
        $memberid=$this->session->userdata('userid');
		$timur = 0;
		$data2 = array();
        $q2 = "
			SELECT s.id, k.timur
			FROM stockiest s
			LEFT JOIN kota k ON s.kota_delivery = k.id
			WHERE s.id = '$memberid'
			ORDER BY timur DESC
			";
        $q2 = $this->db->query($q2);
        // echo $this->db->last_query();
		// $timur=$row['timur'];
		
        if($q2->num_rows()>0){
            foreach($q2->result_array()as $row2){
                $timur=$row2['timur'];
            }
        }
		
        $q2->free_result();
        // echo "a".$timur;
		/*	
		if($timur<1){
			$this->db->select(" a.id, a.name, a.pv, format(a.pv,0)as fpv, b.name as type, a.price, format(a.price,0)as fprice ",false);
        }else{
			$this->db->select(" a.id, a.name, a.pv, format(a.pv,0)as fpv, b.name as type, a.price2 as price, format(a.price2,0)as fprice ",false);
		}
		*/
		$this->db->select(" a.id, a.name, a.pv, format(a.pv,0)as fpv, b.name as type, a.price, format(a.price,0)as fprice, a.price2, format(a.price2,0)as fprice2 ",false);

		
        $this->db->from('item a');
        $this->db->join('type b','a.type_id=b.id','left');
        $where = "a.sales = 'Yes' and ( a.id like '$keywords%' or a.name like '$keywords%' )";
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        // echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	/* End created by ASP 20180405 */
	
	// Created by Takwa 20140616
    public function search_staff($keywords=0,$num,$offset){
        $data = array();
        if($keywords){
			$this->db->select("s.nik as id,s.name",false);	
            $this->db->from('staff s');
			$this->db->like('s.nik',$keywords,'between');
            $this->db->or_like('s.name',$keywords,'between');
            $this->db->order_by('s.name','asc');
            $this->db->limit($num,$offset);
            $q = $this->db->get();
            //echo $this->db->last_query();
            if($q->num_rows > 0){
                foreach($q->result_array() as $row){
                    $data[] = $row;
                }
            }
            $q->free_result();
        }
        return $data;
    }
	
    public function count_search_staff($keywords=0){
        if($keywords){
		$this->db->select("nik",false);
            $this->db->from('staff');
            $this->db->like('nik',$keywords,'between');
            $this->db->or_like('name',$keywords,'between');
            $this->db->order_by('name','asc');
            return $this->db->count_all_results();
        }else return 0;    
    }
   
   public function item_staff($keywords=0,$num,$offset){
        $data = array();
        //$where = "a.sales = 'Yes' and a.type_id > 1 and ( a.id like '$keywords%' or a.name like '$keywords%' )";
        $where = "( a.id like '$keywords%' or a.name like '$keywords%' )";
        
        $this->db->select("a.id,a.name,a.price - floor(a.price*0.30)as price,a.pv,a.bv,format(a.price - floor(a.price*0.30),0)as fprice,format(a.pv,0)as fpv,format(a.bv,0)as fbv,b.name as type",false);
        $this->db->from('item a');
        $this->db->join('type b','a.type_id=b.id','left');
        
        $this->db->where($where);
        $this->db->order_by('name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function count_item_staff($keywords=0){
        //$where = "a.sales = 'Yes' and a.type_id > 1 and ( a.id like '$keywords%' or a.name like '$keywords%' )";
        $where = "( a.id like '$keywords%' or a.name like '$keywords%' )";
        $this->db->select("a.id",false);
        $this->db->from('item a');
        $this->db->where($where);
        return $this->db->count_all_results();
    } 
	// End created by Takwa 20140616

	/* Created by Boby 20140905 */
	public function searchMemberAlloc($keywords=0,$num,$offset){
        $data = array();
        if($keywords){
			$this->db->select("
				m.id, m.nama, m.created,
				up.id as up_id, up.nama as up_nama, up.created as up_created,
				sp.id as sp_id, sp.nama as sp_nama, sp.created as sp_created
				",false);
			$this->db->from('member m');
			$this->db->join('member up','m.sponsor_id=up.id','left');
			$this->db->join('member sp','m.enroller_id=sp.id','left');
			$this->db->like('m.id',$keywords,'between');
            $this->db->or_like('m.nama',$keywords,'between');
            $this->db->order_by('m.nama','asc');
            $this->db->limit($num,$offset);
            $q = $this->db->get();
            //echo $this->db->last_query();
            if($q->num_rows > 0){
                foreach($q->result_array() as $row){
                    $data[] = $row;
                }
            }
            $q->free_result();
        }
        return $data;
    }
	/* Created by Boby 20140905 */

    /*-----------------------------------------
    |
    |   search rusak
    |   By Annisa Rahmawaty 2018
    |
    ==========================================*/

    public function searchRusak($keywords=0,$idwhr,$num,$offset){
        
        $data = array();
        $this->db->select('s.item_id,i.name,i.price as price,s.qty,s.warehouse_id,mr.id,i.id');
        $this->db->from('stock s');
        $this->db->join('item i','s.item_id=i.id','left');
        $this->db->join('master_rusak mr', 's.warehouse_id=mr.id_wh','inner');
        //$where = "mr.id='$idwhr' and (s.item_id like '$keywords%' or i.name like '$keywords%')"; 
        $this->db->where('mr.id',$idwhr);  
        $this->db->where("(`s`.`item_id` LIKE '$keywords%'");
        $this->db->or_where("`i`.`name` like '$keywords%')");       
               
        $this->db->order_by('s.item_id','asc');
        $this->db->limit($num,$offset);

        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
        
    }
    public function countSearchRusak($keywords=0,$idwhr){
        //$this->db->select('s.id');
        $this->db->from('stock s');
        $this->db->join('item i','s.item_id=i.id','left');
        $this->db->join('master_rusak mr', 's.warehouse_id=mr.id_wh', 'left');
        //$where = "mr.id='$idwhr' and (s.item_id like '$keywords%' or i.name like '$keywords%')"; 
        $this->db->where('mr.id',$idwhr); 
        $this->db->where("(`s`.`item_id` LIKE '$keywords%'");
        $this->db->or_where("`i`.`name` like '$keywords%')");   
        //$this->db->like('i.id', $keywords, 'between');
        //$this->db->or_like('i.name', $keywords, 'between');   

        return $this->db->count_all_results();
    }

    public function searchStockRusak($keywords=0,$idwhr,$num,$offset){
        
        $data = array();
        $this->db->select('s.item_id,i.name,i.price as price,s.qty,s.id_whr,i.id');
        $this->db->from('stock_rusak s');
        $this->db->join('item i','s.item_id=i.id','left');
        $this->db->where('s.id_whr',$idwhr);  
        $this->db->where("(`s`.`item_id` LIKE '$keywords%'");
        $this->db->or_where("`i`.`name` like '$keywords%')");       
               
        $this->db->order_by('s.item_id','asc');
        $this->db->limit($num,$offset);

        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
        
    }
    public function countSearchStockRusak($keywords=0,$idwhr){
        $this->db->from('stock_rusak s');
        $this->db->join('item i','s.item_id=i.id','left');
        $this->db->where('s.id_whr',$idwhr); 
        $this->db->where("(`s`.`item_id` LIKE '$keywords%'");
        $this->db->or_where("`i`.`name` like '$keywords%')");   

        return $this->db->count_all_results();
    }

}?>