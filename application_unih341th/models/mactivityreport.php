<?php
/* created by andi 2018-02-19 */
class Mactivityreport extends CI_Model{
    function __construct()
    { parent::__construct(); }
	
	public function getpenghasilan($member_id='',$periode=''){
        $data = array();
        $q = "
			SELECT 
				b.member_id
				,SUM(b.nominal) AS bonus
			FROM bonus b
			WHERE b.periode = '".$periode."'
			AND b.member_id = '".$member_id."'
			GROUP BY b.member_id
		";
        $q = $this->db->query($q);
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }

	public function getperingkatMember($member_id=''){
        $data = array();
        $q = "
			SELECT m.id AS member_id, m.jenjang_id, j.name as jenjang_name 
			FROM member m
			LEFT JOIN jenjang j ON j.id = m.jenjang_id
			WHERE m.id = '".$member_id."'
		";
        $q = $this->db->query($q);
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function getperingkatPgs($member_id='',$periode=''){
        $data = array();
        $q = "
			SELECT pb.member_id, pb.jenjang_id, j.name as jenjang_name
			FROM pgs_bulanan pb
			LEFT JOIN jenjang j ON j.id = pb.jenjang_id
			WHERE pb.tgl = '".$periode."'
			AND pb.member_id = '".$member_id."'
		";
        $q = $this->db->query($q);
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }


	public function getpsPgs($member_id='',$periode=''){
        $data = array();
        $q = "
			SELECT pb.member_id, pb.ps, (pb.ps+pb.pgs)-pb.pgs_cut as tgpv
			FROM pgs_bulanan pb
			LEFT JOIN jenjang j ON j.id = pb.jenjang_id
			WHERE pb.tgl = '".$periode."'
			AND pb.member_id = '".$member_id."'
		";
        $q = $this->db->query($q);
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }


	public function get_active($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		$where = "AND(";
		$where2 = "OR(";
		
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		
		$got = 0;
		$new = 0;
		$br = "";
		
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				$l = $row['leftval']+1;	
				$l1 = $row['leftval'];
				$r = $row['rightval']-1;
				$r1 = $row['rightval'];
				if($new==1){$where2 .= "OR ";$where .= "AND ";} // Updated by Boby 20131119
				$where .= "(n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ) ".$br;
				$where2 .= "(n.leftval = $l1 AND n.rightval = $r1) ".$br; // Updated by Boby 20131119
				$new = 1;
			}
		}
		$where .= ")";
		$where2 .= ")";
		
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
			$where = " ";
			$where2 = " ";
		}
		
		$q->free_result();
		
		$q = "
			SELECT n.member_id-- , m.nama
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id 
			WHERE n.member_id IN(
				SELECT DISTINCT so.member_id
				FROM so 
				WHERE so.tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 4 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
				AND so.totalpv > 0
				GROUP BY so.member_id
			)
			AND n.leftval >= $leftval AND n.rightval <= $rightval  
			-- AND n.leftval >= 2519 AND n.rightval <= 39008  
			AND m.created <= LAST_DAY('$tgl')
			AND n.active = 1
			AND SUBSTR(m.tglaplikasi,1,7) <= '$show_level'
			AND m.id NOT IN (SELECT id FROM member_terminate) 
			".$where.$where2."
			ORDER BY n.leftval, n.rightval DESC 
		";
		
		 //echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
	}
	
	public function get_active_count($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		$where = "AND(";
		$where2 = "OR(";
		
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		
		$got = 0;
		$new = 0;
		$br = "";
		
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				$l = $row['leftval']+1;	
				$l1 = $row['leftval'];
				$r = $row['rightval']-1;
				$r1 = $row['rightval'];
				if($new==1){$where2 .= "OR ";$where .= "AND ";} // Updated by Boby 20131119
				$where .= "(n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ) ".$br;
				$where2 .= "(n.leftval = $l1 AND n.rightval = $r1) ".$br; // Updated by Boby 20131119
				$new = 1;
			}
		}
		$where .= ")";
		$where2 .= ")";
		
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
			$where = " ";
			$where2 = " ";
		}
		
		$q->free_result();
		
		$q = "
			SELECT COUNT(*) AS jml FROM (
			SELECT n.member_id-- , m.nama
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id 
			WHERE n.member_id IN(
				SELECT DISTINCT so.member_id
				FROM so 
				WHERE so.tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 4 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
				AND so.totalpv > 0
				GROUP BY so.member_id
			)
			AND n.leftval >= $leftval AND n.rightval <= $rightval  
			-- AND n.leftval >= 2519 AND n.rightval <= 39008  
			AND m.created <= LAST_DAY('$tgl')
			AND n.active = 1
			AND SUBSTR(m.tglaplikasi,1,7) <= '$show_level'
			AND m.id NOT IN (SELECT id FROM member_terminate) 
			".$where.$where2."
			ORDER BY n.leftval, n.rightval DESC 
			) AS dt
		";
		
		 //echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
	}
	
	public function get_inactive_count($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		$where = "AND(";
		$where2 = "OR(";
		
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		
		$got = 0;
		$new = 0;
		$br = "";
		
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				$l = $row['leftval']+1;	
				$l1 = $row['leftval'];
				$r = $row['rightval']-1;
				$r1 = $row['rightval'];
				if($new==1){$where2 .= "OR ";$where .= "AND ";} // Updated by Boby 20131119
				$where .= "(n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ) ".$br;
				$where2 .= "(n.leftval = $l1 AND n.rightval = $r1) ".$br; // Updated by Boby 20131119
				$new = 1;
			}
		}
		$where .= ")";
		$where2 .= ")";
		
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
			$where = " ";
			$where2 = " ";
		}
		
		$q->free_result();
		
		$q = "
			SELECT COUNT(*) AS jml FROM (
			SELECT n.member_id-- , m.nama
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id 
			WHERE n.member_id NOT IN(
				SELECT DISTINCT so.member_id
				FROM so 
				WHERE so.tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 4 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
				AND so.totalpv > 0
				GROUP BY so.member_id
			)
			AND n.leftval >= $leftval AND n.rightval <= $rightval  
			-- AND n.leftval >= 2519 AND n.rightval <= 39008  
			AND m.created <= LAST_DAY('$tgl')
			AND n.active = 1
			AND SUBSTR(m.tglaplikasi,1,7) <= '$show_level'
			AND m.id NOT IN (SELECT id FROM member_terminate) 
			".$where.$where2."
			ORDER BY n.leftval, n.rightval DESC 
			) AS dt
		";
		
		 //echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
	}
	
	public function get_reactive_count($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		$where = "AND(";
		$where2 = "OR(";
		
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		
		$got = 0;
		$new = 0;
		$br = "";
		
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				$l = $row['leftval']+1;	
				$l1 = $row['leftval'];
				$r = $row['rightval']-1;
				$r1 = $row['rightval'];
				if($new==1){$where2 .= "OR ";$where .= "AND ";} // Updated by Boby 20131119
				$where .= "(n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ) ".$br;
				$where2 .= "(n.leftval = $l1 AND n.rightval = $r1) ".$br; // Updated by Boby 20131119
				$new = 1;
			}
		}
		$where .= ")";
		$where2 .= ")";
		
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
			$where = " ";
			$where2 = " ";
		}
		
		$q->free_result();
		
		$q = "
			SELECT COUNT(*) AS jml FROM (
			SELECT n.member_id-- , m.nama
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id 
			WHERE n.member_id NOT IN(
				SELECT DISTINCT so.member_id
				FROM so 
				WHERE so.tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 8 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl' - INTERVAL 4 MONTH))
				AND so.totalpv > 0
				GROUP BY so.member_id
			)
			AND n.member_id IN(
				SELECT DISTINCT so.member_id
				FROM so 
				WHERE so.tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 4 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
				AND so.totalpv > 0
				GROUP BY so.member_id
			)
			AND n.leftval >= $leftval AND n.rightval <= $rightval  
			-- AND n.leftval >= 2519 AND n.rightval <= 39008  
			AND m.created <= LAST_DAY('$tgl' - INTERVAL 4 MONTH)
			AND n.active = 1
			AND SUBSTR(m.tglaplikasi,1,7) <= '$show_level'
			AND m.id NOT IN (SELECT id FROM member_terminate) 
			".$where.$where2."
			ORDER BY n.leftval, n.rightval DESC 
			) AS dt
		";
		
		 //echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
	}
	
	public function get_sf($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		$where = "AND(";
		$where2 = "OR(";
		
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		
		$got = 0;
		$new = 0;
		$br = "";
		
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				$l = $row['leftval']+1;	
				$l1 = $row['leftval'];
				$r = $row['rightval']-1;
				$r1 = $row['rightval'];
				if($new==1){$where2 .= "OR ";$where .= "AND ";} // Updated by Boby 20131119
				$where .= "(n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ) ".$br;
				$where2 .= "(n.leftval = $l1 AND n.rightval = $r1) ".$br; // Updated by Boby 20131119
				$new = 1;
			}
		}
		$where .= ")";
		$where2 .= ")";
		
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
			$where = " ";
			$where2 = " ";
		}
		
		$q->free_result();
		
		$q = "
			SELECT n.member_id-- , m.nama
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id 
			WHERE n.member_id IN(
				SELECT DISTINCT so.member_id
				FROM so 
				WHERE so.tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
				AND so.totalpv > 0
				GROUP BY so.member_id
			)
			AND n.leftval >= $leftval AND n.rightval <= $rightval  
			-- AND n.leftval >= 2519 AND n.rightval <= 39008  
			AND m.created <= LAST_DAY('$tgl')
			AND n.active = 1
			AND SUBSTR(m.tglaplikasi,1,7) <= '$show_level'
			AND m.id NOT IN (SELECT id FROM member_terminate) 
			".$where.$where2."
			ORDER BY n.leftval, n.rightval DESC 
		";
		
		 //echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
	}
	
	public function get_sf_count($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		$where = "AND(";
		$where2 = "OR(";
		
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		
		$got = 0;
		$new = 0;
		$br = "";
		
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				$l = $row['leftval']+1;	
				$l1 = $row['leftval'];
				$r = $row['rightval']-1;
				$r1 = $row['rightval'];
				if($new==1){$where2 .= "OR ";$where .= "AND ";} // Updated by Boby 20131119
				$where .= "(n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ) ".$br;
				$where2 .= "(n.leftval = $l1 AND n.rightval = $r1) ".$br; // Updated by Boby 20131119
				$new = 1;
			}
		}
		$where .= ")";
		$where2 .= ")";
		
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
			$where = " ";
			$where2 = " ";
		}
		
		$q->free_result();
		
		$q = "
			SELECT COUNT(*) AS jml FROM (
			SELECT n.member_id-- , m.nama
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id 
			WHERE n.member_id IN(
				SELECT DISTINCT so.member_id
				FROM so 
				WHERE so.tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
				AND so.totalpv > 0
				GROUP BY so.member_id
			)
			AND n.leftval >= $leftval AND n.rightval <= $rightval  
			-- AND n.leftval >= 2519 AND n.rightval <= 39008  

			AND m.created <= LAST_DAY('$tgl')
			AND n.active = 1
			AND SUBSTR(m.tglaplikasi,1,7) <= '$show_level'
			AND m.id NOT IN (SELECT id FROM member_terminate) 
			".$where.$where2."
			ORDER BY n.leftval, n.rightval DESC 
			) AS dt
		";
		
		 //echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
	}

	public function get_sp($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		$where = "AND(";
		$where2 = "OR(";
		
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		
		$got = 0;
		$new = 0;
		$br = "";
		
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				$l = $row['leftval']+1;	
				$l1 = $row['leftval'];
				$r = $row['rightval']-1;
				$r1 = $row['rightval'];
				if($new==1){$where2 .= "OR ";$where .= "AND ";} // Updated by Boby 20131119
				$where .= "(n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ) ".$br;
				$where2 .= "(n.leftval = $l1 AND n.rightval = $r1) ".$br; // Updated by Boby 20131119
				$new = 1;
			}
		}
		$where .= ")";
		$where2 .= ")";
		
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
			$where = " ";
			$where2 = " ";
		}
		
		$q->free_result();
		
		$q = "
			SELECT n.member_id-- , m.nama
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id 
			WHERE n.member_id IN(
				SELECT dt.enroller_id AS member_id 
			      FROM (
				SELECT dt.enroller_id,dt2.oms, COUNT(dt.member_id)AS rekrut 
				FROM( 
					SELECT m.enroller_id, so.member_id, so_.oms, MAX(so.kit)AS kit1, SUM(so.totalpv)AS pv 
					FROM so 
					LEFT JOIN member m ON so.member_id = m.id 
					LEFT JOIN( 
						SELECT member_id, SUM(totalpv)AS oms 
						FROM so 
						WHERE tgl BETWEEN '$tgl' AND (LAST_DAY('$tgl')) 
						-- WHERE tgl BETWEEN '2018-01-01' AND (LAST_DAY('2018-01-01')) 
						AND kit = 'N' 
						GROUP BY member_id 
					)AS so_ ON m.id = so_.member_id 
					-- WHERE tgl BETWEEN '$tgl' AND (LAST_DAY('$tgl')) 
					-- WHERE tgl BETWEEN '2018-01-01' AND (LAST_DAY('2018-01-01')) 
					GROUP BY member_id 
					HAVING kit1 = 'y' AND so_.oms >= 1000000 
				)AS dt 
				LEFT JOIN (
					SELECT member_id, SUM(totalpv)AS oms 
					FROM so 
					WHERE tgl BETWEEN '$tgl' AND (LAST_DAY('$tgl')) 
					-- WHERE tgl BETWEEN '2018-01-01' AND (LAST_DAY('2018-01-01')) 
					AND kit = 'N' 
					GROUP BY member_id 
				)AS dt2 ON dt2.member_id = dt.enroller_id
				-- WHERE pv > 0 
				GROUP BY dt.enroller_id 
				HAVING dt2.oms >= 1000000
			      ) AS dt
			)
			AND n.leftval >= $leftval AND n.rightval <= $rightval  
			-- AND n.leftval >= 2519 AND n.rightval <= 39008  
			AND m.created <= LAST_DAY('$tgl')
			AND n.active = 1
			AND SUBSTR(m.tglaplikasi,1,7) <= '$show_level'
			AND m.id NOT IN (SELECT id FROM member_terminate) 
			".$where.$where2."
			ORDER BY n.leftval, n.rightval DESC 
		";
		
		 //echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
	}
	
	public function get_sp_count($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		$where = "AND(";
		$where2 = "OR(";
		
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		
		$got = 0;
		$new = 0;
		$br = "";
		
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				$l = $row['leftval']+1;	
				$l1 = $row['leftval'];
				$r = $row['rightval']-1;
				$r1 = $row['rightval'];
				if($new==1){$where2 .= "OR ";$where .= "AND ";} // Updated by Boby 20131119
				$where .= "(n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ) ".$br;
				$where2 .= "(n.leftval = $l1 AND n.rightval = $r1) ".$br; // Updated by Boby 20131119
				$new = 1;
			}
		}
		$where .= ")";
		$where2 .= ")";
		
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
			$where = " ";
			$where2 = " ";
		}
		
		$q->free_result();
		
		$q = "
			SELECT COUNT(*) AS jml FROM (
			SELECT n.member_id-- , m.nama
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id 
			WHERE n.member_id IN(
				SELECT dt.enroller_id AS member_id 
			      FROM (
				SELECT dt.enroller_id,dt2.oms, COUNT(dt.member_id)AS rekrut 
				FROM( 
					SELECT m.enroller_id, so.member_id, so_.oms, MAX(so.kit)AS kit1, SUM(so.totalpv)AS pv 
					FROM so 
					LEFT JOIN member m ON so.member_id = m.id 
					LEFT JOIN( 
						SELECT member_id, SUM(totalpv)AS oms 
						FROM so 
						WHERE tgl BETWEEN '$tgl' AND (LAST_DAY('$tgl')) 
						-- WHERE tgl BETWEEN '2018-01-01' AND (LAST_DAY('2018-01-01')) 
						AND kit = 'N' 
						GROUP BY member_id 
					)AS so_ ON m.id = so_.member_id 
					-- WHERE tgl BETWEEN '$tgl' AND (LAST_DAY('$tgl')) 
					-- WHERE tgl BETWEEN '2018-01-01' AND (LAST_DAY('2018-01-01')) 
					GROUP BY member_id 
					HAVING kit1 = 'y' AND so_.oms >= 1000000 
				)AS dt 
				LEFT JOIN (
					SELECT member_id, SUM(totalpv)AS oms 
					FROM so 
					WHERE tgl BETWEEN '$tgl' AND (LAST_DAY('$tgl')) 
					-- WHERE tgl BETWEEN '2018-01-01' AND (LAST_DAY('2018-01-01')) 
					AND kit = 'N' 
					GROUP BY member_id 
				)AS dt2 ON dt2.member_id = dt.enroller_id
				-- WHERE pv > 0 
				GROUP BY dt.enroller_id 
				HAVING dt2.oms >= 1000000
			      ) AS dt
			)
			AND n.leftval >= $leftval AND n.rightval <= $rightval  
			-- AND n.leftval >= 2519 AND n.rightval <= 39008  

			AND m.created <= LAST_DAY('$tgl')
			AND n.active = 1
			AND SUBSTR(m.tglaplikasi,1,7) <= '$show_level'
			AND m.id NOT IN (SELECT id FROM member_terminate) 
			".$where.$where2."
			ORDER BY n.leftval, n.rightval DESC 
			) AS dt
		";
		
		 //echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
	}

	public function get_nm($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		$where = "AND(";
		$where2 = "OR(";
		
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		
		$got = 0;
		$new = 0;
		$br = "";
		
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				$l = $row['leftval']+1;	
				$l1 = $row['leftval'];
				$r = $row['rightval']-1;
				$r1 = $row['rightval'];
				if($new==1){$where2 .= "OR ";$where .= "AND ";} // Updated by Boby 20131119
				$where .= "(n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ) ".$br;
				$where2 .= "(n.leftval = $l1 AND n.rightval = $r1) ".$br; // Updated by Boby 20131119
				$new = 1;
			}
		}
		$where .= ")";
		$where2 .= ")";
		
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
			$where = " ";
			$where2 = " ";
		}
		
		$q->free_result();
		
		$q = "
			SELECT n.member_id-- , m.nama
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id 
			WHERE n.member_id IN(
				SELECT member_id FROM (
					SELECT so.member_id, MAX(so.kit) AS kit1, SUM(so.totalpv) AS oms
					FROM so 
					WHERE so.tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
					-- WHERE so.tgl BETWEEN (LAST_DAY('2018-01-31' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('2018-01-31'))
					-- AND so.totalpv > 0
					GROUP BY so.member_id
					HAVING kit1 = 'y'
				) AS dt
			)
			AND n.leftval >= $leftval AND n.rightval <= $rightval  
			-- AND n.leftval >= 2519 AND n.rightval <= 39008  
			AND m.created <= LAST_DAY('$tgl')
			AND n.active = 1
			AND SUBSTR(m.tglaplikasi,1,7) <= '$show_level'
			AND m.id NOT IN (SELECT id FROM member_terminate) 
			".$where.$where2."
			ORDER BY n.leftval, n.rightval DESC 
		";
		
		 //echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
	}
	
	public function get_nm_count($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		$where = "AND(";
		$where2 = "OR(";
		
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		
		$got = 0;
		$new = 0;
		$br = "";
		
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				$l = $row['leftval']+1;	
				$l1 = $row['leftval'];
				$r = $row['rightval']-1;
				$r1 = $row['rightval'];
				if($new==1){$where2 .= "OR ";$where .= "AND ";} // Updated by Boby 20131119
				$where .= "(n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ) ".$br;
				$where2 .= "(n.leftval = $l1 AND n.rightval = $r1) ".$br; // Updated by Boby 20131119
				$new = 1;
			}
		}
		$where .= ")";
		$where2 .= ")";
		
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
			$where = " ";
			$where2 = " ";
		}
		
		$q->free_result();
		
		$q = "
			SELECT COUNT(*) AS jml FROM (
			SELECT n.member_id-- , m.nama
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id 
			WHERE n.member_id IN(
				SELECT member_id FROM (
					SELECT so.member_id, MAX(so.kit) AS kit1, SUM(so.totalpv) AS oms
					FROM so 
					WHERE so.tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
					-- WHERE so.tgl BETWEEN (LAST_DAY('2018-01-31' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('2018-01-31'))
					-- AND so.totalpv > 0
					GROUP BY so.member_id
					HAVING kit1 = 'y'
				) AS dt
			)
			AND n.leftval >= $leftval AND n.rightval <= $rightval  
			-- AND n.leftval >= 2519 AND n.rightval <= 39008  

			AND m.created <= LAST_DAY('$tgl')
			AND n.active = 1
			AND SUBSTR(m.tglaplikasi,1,7) <= '$show_level'
			AND m.id NOT IN (SELECT id FROM member_terminate) 
			".$where.$where2."
			ORDER BY n.leftval, n.rightval DESC 
			) AS dt
		";
		
		 //echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
	}

	public function get_nr($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		$where = "AND(";
		$where2 = "OR(";
		
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		
		$got = 0;
		$new = 0;
		$br = "";
		
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				$l = $row['leftval']+1;	
				$l1 = $row['leftval'];
				$r = $row['rightval']-1;
				$r1 = $row['rightval'];
				if($new==1){$where2 .= "OR ";$where .= "AND ";} // Updated by Boby 20131119
				$where .= "(n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ) ".$br;
				$where2 .= "(n.leftval = $l1 AND n.rightval = $r1) ".$br; // Updated by Boby 20131119
				$new = 1;
			}
		}
		$where .= ")";
		$where2 .= ")";
		
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
			$where = " ";
			$where2 = " ";
		}
		
		$q->free_result();
		
		$q = "
			SELECT n.member_id-- , m.nama
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id 
			WHERE n.member_id IN(
				SELECT member_id FROM (
					SELECT so.member_id, MAX(so.kit) AS kit1, SUM(so.totalpv) AS oms
					FROM so 
					WHERE so.tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
					-- WHERE so.tgl BETWEEN (LAST_DAY('2018-01-31' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('2018-01-31'))
					-- AND so.totalpv > 0
					GROUP BY so.member_id
					HAVING kit1 = 'y' and oms > 0
				) AS dt
			)
			AND n.leftval >= $leftval AND n.rightval <= $rightval  
			-- AND n.leftval >= 2519 AND n.rightval <= 39008  
			AND m.created <= LAST_DAY('$tgl')
			AND n.active = 1
			AND SUBSTR(m.tglaplikasi,1,7) <= '$show_level'
			AND m.id NOT IN (SELECT id FROM member_terminate) 
			".$where.$where2."
			ORDER BY n.leftval, n.rightval DESC 
		";
		
		 //echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
	}
	
	public function get_nr_count($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		$where = "AND(";
		$where2 = "OR(";
		
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		
		$got = 0;
		$new = 0;
		$br = "";
		
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				$l = $row['leftval']+1;	
				$l1 = $row['leftval'];
				$r = $row['rightval']-1;
				$r1 = $row['rightval'];
				if($new==1){$where2 .= "OR ";$where .= "AND ";} // Updated by Boby 20131119
				$where .= "(n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ) ".$br;
				$where2 .= "(n.leftval = $l1 AND n.rightval = $r1) ".$br; // Updated by Boby 20131119
				$new = 1;
			}
		}
		$where .= ")";
		$where2 .= ")";
		
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
			$where = " ";
			$where2 = " ";
		}
		
		$q->free_result();
		
		$q = "
			SELECT COUNT(*) AS jml FROM (
			SELECT n.member_id-- , m.nama
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id 
			WHERE n.member_id IN(
				SELECT member_id FROM (
					SELECT so.member_id, MAX(so.kit) AS kit1, SUM(so.totalpv) AS oms
					FROM so 
					WHERE so.tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
					-- WHERE so.tgl BETWEEN (LAST_DAY('2018-01-31' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('2018-01-31'))
					-- AND so.totalpv > 0
					GROUP BY so.member_id
					HAVING kit1 = 'y' and oms > 0
				) AS dt
			)
			AND n.leftval >= $leftval AND n.rightval <= $rightval  
			-- AND n.leftval >= 2519 AND n.rightval <= 39008  

			AND m.created <= LAST_DAY('$tgl')
			AND n.active = 1
			AND SUBSTR(m.tglaplikasi,1,7) <= '$show_level'
			AND m.id NOT IN (SELECT id FROM member_terminate) 
			".$where.$where2."
			ORDER BY n.leftval, n.rightval DESC 
			) AS dt
		";
		
		 //echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
	}

	public function get_nrq($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		$where = "AND(";
		$where2 = "OR(";
		
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		
		$got = 0;
		$new = 0;
		$br = "";
		
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				$l = $row['leftval']+1;	
				$l1 = $row['leftval'];
				$r = $row['rightval']-1;
				$r1 = $row['rightval'];
				if($new==1){$where2 .= "OR ";$where .= "AND ";} // Updated by Boby 20131119
				$where .= "(n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ) ".$br;
				$where2 .= "(n.leftval = $l1 AND n.rightval = $r1) ".$br; // Updated by Boby 20131119
				$new = 1;
			}
		}
		$where .= ")";
		$where2 .= ")";
		
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
			$where = " ";
			$where2 = " ";
		}
		
		$q->free_result();
		
		$q = "
			SELECT n.member_id-- , m.nama
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id 
			WHERE n.member_id IN(
				SELECT member_id FROM (
					SELECT so.member_id, MAX(so.kit) AS kit1, SUM(so.totalpv) AS oms
					FROM so 
					WHERE so.tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
					-- WHERE so.tgl BETWEEN (LAST_DAY('2018-01-31' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('2018-01-31'))
					-- AND so.totalpv > 0
					GROUP BY so.member_id
					HAVING kit1 = 'y' and oms >= 1000000
				) AS dt
			)
			AND n.leftval >= $leftval AND n.rightval <= $rightval  
			-- AND n.leftval >= 2519 AND n.rightval <= 39008  
			AND m.created <= LAST_DAY('$tgl')
			AND n.active = 1
			AND SUBSTR(m.tglaplikasi,1,7) <= '$show_level'
			AND m.id NOT IN (SELECT id FROM member_terminate) 
			".$where.$where2."
			ORDER BY n.leftval, n.rightval DESC 
		";
		
		 //echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
	}
	
	public function get_nrq_count($show_level='',$leftval=0,$rightval=0,$level=0){
        $data = array();
		if($show_level == ''){
			$show_level = date('Y-m');
		}
		$tgl = $show_level.'-01';
		$where = "AND(";
		$where2 = "OR(";
		
		$q = "
			SELECT lq.member_id, n.leftval, n.rightval
			FROM leader_qualified lq
			LEFT JOIN networks n ON lq.member_id = n.member_id
			WHERE lq.tgl = LAST_DAY('$tgl')
			AND n.leftval > $leftval
			AND n.rightval < $rightval
		";
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		
		$got = 0;
		$new = 0;
		$br = "";
		
		if($q->num_rows()>0){
			$got = 1;
			foreach($q->result_array()as $row){
				$l = $row['leftval']+1;	
				$l1 = $row['leftval'];
				$r = $row['rightval']-1;
				$r1 = $row['rightval'];
				if($new==1){$where2 .= "OR ";$where .= "AND ";} // Updated by Boby 20131119
				$where .= "(n.leftval NOT BETWEEN $l AND $r AND n.rightval NOT BETWEEN $l AND $r ) ".$br;
				$where2 .= "(n.leftval = $l1 AND n.rightval = $r1) ".$br; // Updated by Boby 20131119
				$new = 1;
			}
		}
		$where .= ")";
		$where2 .= ")";
		
		if($got>0){
			$sel = ", case ".$sel."	else 0 end as note ";
		}else{
			$sel = ", 0 as note";
			$where = " ";
			$where2 = " ";
		}
		
		$q->free_result();
		
		$q = "
			SELECT COUNT(*) AS jml FROM (
			SELECT n.member_id-- , m.nama
			FROM networks n
			LEFT JOIN member m ON n.member_id = m.id 
			WHERE n.member_id IN(
				SELECT member_id FROM (
					SELECT so.member_id, MAX(so.kit) AS kit1, SUM(so.totalpv) AS oms
					FROM so 
					WHERE so.tgl BETWEEN (LAST_DAY('$tgl' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('$tgl'))
					-- WHERE so.tgl BETWEEN (LAST_DAY('2018-01-31' - INTERVAL 1 MONTH)+INTERVAL 1 DAY) AND (LAST_DAY('2018-01-31'))
					-- AND so.totalpv > 0
					GROUP BY so.member_id
					HAVING kit1 = 'y' and oms >= 1000000
				) AS dt
			)
			AND n.leftval >= $leftval AND n.rightval <= $rightval  
			-- AND n.leftval >= 2519 AND n.rightval <= 39008  

			AND m.created <= LAST_DAY('$tgl')
			AND n.active = 1
			AND SUBSTR(m.tglaplikasi,1,7) <= '$show_level'
			AND m.id NOT IN (SELECT id FROM member_terminate) 
			".$where.$where2."
			ORDER BY n.leftval, n.rightval DESC 
			) AS dt
		";
		
		 //echo $q;
		$this->db->query($q);
		$q = $this->db->query($q);
		
		//echo $this->db->last_query();
		if($q->num_rows()>0){
			foreach($q->result_array()as $row){
				$data[]=$row;
			}
		}
		$q->free_result();
		return $data;
	}
	public function getBonusKu($member_id,$periode,$title){
        $data = array();
		$periode = "LAST_DAY('".$periode."')";
		$q = "
			SELECT *
			FROM bonus
			WHERE member_id = '$member_id'
			AND periode = $periode
			AND title = $title
		";
		$q = $this->db->query($q);
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
}
?>