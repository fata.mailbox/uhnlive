<?php
class Mpromo_prod extends CI_Model{
  function __construct()
  {
    parent::__construct();
  }

  /*
  |--------------------------------------------------------------------------
  | Promo Product
  |--------------------------------------------------------------------------
  |
  | @created 2019
  | Annisa Rahmawaty
  |
  */
  public function searchPromo($keywords=0,$num,$offset){
    $data = array();
    $this->db->select("p.id,p.item_id,p.free_item_id,p.item_qty,p.free_item_qty,p.multiples,a.name,p.end_periode",false);
    $this->db->from('promo p');
    $this->db->join('item a','a.id=p.item_id','left');
    $this->db->like('p.item_id',$keywords,'between');
    $this->db->or_like('a.name',$keywords,'between');
    $this->db->order_by('p.id','DESC');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function countPromo($keywords=0){
    $this->db->like('a.item_id',$keywords,'between');
    $this->db->or_like('i.name',$keywords,'between');
    $this->db->from('promo a');
    $this->db->join('item i','i.id=a.item_id','left');
    return $this->db->count_all_results();
  }

  public function addPromo(){    
    $data=array(
        'item_id' => $this->input->post('item_id'),
        'item_qty' => $this->input->post('item_qty'),
        'free_item_id' => $this->input->post('free_item_id'),
        'free_item_qty' => $this->input->post('free_item_qty'),
        'multiples' => $this->input->post('multiples'),
        'term' => 3,
        'end_periode' => $this->input->post('end_periode'),  
        'created_date' => date('Y-m-d H:i:s',now()),
        'created' => $this->session->userdata('user')
    );
    $this->db->insert('promo',$data);
  }

  public function getPromo($id=0){
    $data = array();
    $this->db->select("p.*,a.name as item_name,(SELECT i.name from item i where i.id=p.free_item_id) as free_item_name",false);
    $this->db->from('promo p');
    $this->db->join('item a','a.id=p.item_id','left');
    $this->db->where('p.id',$id);
    
    $q = $this->db->get();

    if($q->num_rows()>0){
      $data=$q->row();
    }
    $q->free_result();
    return $data;
  }

  public function editPromo($id){
    $data=array(
        'item_id' => $this->input->post('item_id'),
        'item_qty' => $this->input->post('item_qty'),
        'free_item_id' => $this->input->post('free_item_id'),
        'free_item_qty' => $this->input->post('free_item_qty'),
        'multiples' => $this->input->post('multiples'),
        'term' => 3,
        'end_periode' => $this->input->post('end_periode'),  
        'created_date' => date('Y-m-d H:i:s',now()),
        'created' => $this->session->userdata('user')
      );

    $this->db->where('id',$id);
    $this->db->update('promo',$data);
    $this->session->set_flashdata('message','Edit Promo Product Successfully');
  }
  
}?>
