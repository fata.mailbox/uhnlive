<?php
class MProfiles extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
	
	public function getAlamat($id){
		$data = array();
		$str = "
			SELECT m.alamat
			FROM member m
			WHERE m.id = '$id'
			LIMIT 0,1";
		$qry = $this->db->query($str);
		//echo $this->db->last_query();
		if($qry->num_rows()>0){
			$data=$qry->row_array();
		}
		$qry->free_result();
		return $data['alamat'];
	}
	
	public function getNama($id){
		$data = array();
		$str = "
			SELECT m.nama
			FROM member m
			WHERE m.id = '$id'
			LIMIT 0,1";
		$qry = $this->db->query($str);
		//echo $this->db->last_query();
		if($qry->num_rows()>0){
			$data=$qry->row_array();
		}
		$qry->free_result();
		return $data['nama'];
	}
	
	public function getNoNpwp($id){
		$data = array();
		$str = "
			SELECT IFNULL(m.npwp,'0') as no_npwp
			FROM member m
			WHERE m.id = '$id'
			LIMIT 0,1";
		$qry = $this->db->query($str);
		//echo $this->db->last_query();
		if($qry->num_rows()>0){
			$data=$qry->row_array();
		}
		$qry->free_result();
		return $data['no_npwp'];
	}
	
	public function getBank($id){
		$data = array();
		$str = "
			SELECT acc.member_id, acc.bank_id, acc.`name`, acc.`no`, acc.`area`, acc.flag, m.nama
			FROM account acc
			LEFT join member m on acc.member_id = m.id
			WHERE acc.member_id = '$id'
			ORDER BY acc.id DESC
			LIMIT 0,1";
		$qry = $this->db->query($str);
		//echo $this->db->last_query();
		if($qry->num_rows()>0){
			$data=$qry->row_array();
		}else{
			$data['member_id']=$id;
			$data['nama']=$this->getNama($id);
		}
		$qry->free_result();
		return $data;
	}
	
	public function saveBank(){
		if($this->input->post('bank_id') != $this->input->post('currbankid')){$flag = 0; }
		else if($this->input->post('nasabah') != $this->input->post('currnasabah')){$flag = 0;}
		else if($this->input->post('norek') != $this->input->post('currnorek')){$flag = 0;}
		else{$flag = $this->input->post('flag');}
		//echo "Flag = ".$flag;
		
		$data = array(
			'bank_id' => $this->input->post('bank_id'),
			'member_id' => $this->input->post('member_id'),
			'name' => $this->input->post('nasabah'),
			'no' => $this->input->post('norek'),
			'area' => $this->input->post('area'),
			'flag' => $flag,	// Modified by Boby (2009-11-19)
			'createdby' => $this->session->userdata('username'),
			'created' => date('Y-m-d H:i:s', now())
		);
		$this->db->insert('account',$data);
		$id = $this->db->insert_id();
		$this->db->update('member',array('account_id'=>$id),array('id' => $this->input->post('member_id')));
	}
	
	public function getNPWP($id){
		$data = array();
		$str = "
			SELECT 
				npwp.member_id, npwp.nama, npwp.npwp, DATE_FORMAT(npwp.npwp_date, '%Y-%m-%d' )as npwpdate
				, npwp.alamat, npwp.menikah, npwp.anak, npwp.office
			FROM npwp
			WHERE npwp.member_id = '$id'
			ORDER BY npwp.id DESC
			LIMIT 0,1";
		$qry = $this->db->query($str);
		//echo $this->db->last_query();
		if($qry->num_rows()>0){
			$data=$qry->row_array();
		}else{
			$data['member_id']=$id;
			$data['nama']			=$this->getNama($id);
			$data['alamat']			=$this->getAlamat($id);
			$data['alamatmember']	=$this->getAlamat($id);
			$data['npwp']			=$this->getNoNpwp($id);
			$data['npwpdate']="";$data['menikah']="";$data['anak']="";$data['office']="";
		}
		$qry->free_result();
		return $data;
	}
	
	public function cekNPWP($npwp, $id){
		$str = "
			SELECT npwp.npwp
			FROM npwp
			WHERE npwp.npwp = '$npwp'
			AND npwp.member_id <> '$id'";
		$qry = $this->db->query($str);
		//echo $this->db->last_query();
		if($qry->num_rows()>0){
			return false;
		}else{
			return true;
		}
	}
	
	public function saveNPWP(){
		$office = $this->input->post('office');
		if(strlen($office)<=1){$office = '-';}
		//echo $office;
		$data = array(
			'npwp' => $this->input->post('npwp'),
			'npwp_date' => $this->input->post('npwpdate'),
			'member_id' => $this->input->post('member_id'),
			'nama' => $this->input->post('nama'),
			'alamat' => $this->input->post('address'),
			'office' => $office,
			'menikah' => $this->input->post('menikah'),
			'anak' => $this->input->post('anak'),
			'created_date' => date('Y-m-d H:i:s', now()),
			'created_by' => $this->session->userdata('username')
		);
		$this->db->insert('npwp',$data);
		/*
		$this->db->update('member'
			,array('office'=>$this->input->post('office'))
			,array('menikah' => $this->input->post('menikah'))
			,array('anak' => $this->input->post('anak'))
			,array('npwp' => $this->input->post('npwp'))
		);
		*/
	}
	
}
?>