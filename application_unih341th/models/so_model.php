<?php
class SO_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function searchSO($keywords=0,$num,$offset){
        $data = array();
	$select = "a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.kit,a.member_id,format(a.totalpv,0)as ftotalpv,m.nama,remark,ifnull(w.name,'-') as warehouse_name, date_format(a.created,'%d-%b-%Y %T')as created, ifnull(date_format(a.tglapproved,'%d-%b-%Y %T'),'-')as appdate";
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND (a.id LIKE '%$keywords%' or m.id LIKE '%$keywords%' or m.nama LIKE '%$keywords%')";
	    $select .= ",format(a.totalharga_,0)as ftotalharga";
        }else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.stockiest_id = '0' and a.warehouse_id = '$whsid' and (a.id LIKE '%$keywords%' or m.id LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')";
            else $where = "( a.id LIKE '%$keywords%' or m.id LIKE '%$keywords%' OR m.nama LIKE '%$keywords%' )";
		$select .= ",format(a.totalharga,0)as ftotalharga";
        }
	$this->db->select($select,false);
		
        $this->db->from('so a');
        $this->db->join('member m','a.member_id=m.id','left');
		// START ASP 20180410
        $this->db->join('warehouse w','a.warehouse_id=w.id','left');
		// EOF ASP 20180410
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
// Created 20151210 ASP
    public function searchSO_v($keywords=0,$num,$offset){
        $data = array();
	$select = "a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.kit,a.member_id,format(a.totalpv,0)as ftotalpv,m.nama,remark";
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND (a.id LIKE '%$keywords%' or m.id LIKE '%$keywords%' or m.nama LIKE '%$keywords%')";
	    $select .= ",format((a.totalharga_-(ifnull(dt.tvprice,0))),0)as ftotalharga";
        }else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.stockiest_id = '0' and a.warehouse_id = '$whsid' and (a.id LIKE '%$keywords%' or s.no_stc LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')";
            else $where = "( a.id LIKE '%$keywords%' OR m.nama LIKE '%$keywords%' )";
		$select .= ",format(a.totalharga,0)as ftotalharga";
        }
	$this->db->select($select,false);
		
        $this->db->from('so a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('
						(
						 	select so_id, sum(price) as tvprice
							from voucher v
							group by v.so_id
						 ) as dt
						','a.id=dt.so_id','left');
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }

// EOF Created 20151210 ASP
    public function countSO($keywords=0){
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND (a.id LIKE '%$keywords%' or m.id LIKE '%$keywords%' or m.nama LIKE '%$keywords%')";
        }
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.stockiest_id = '0' and a.warehouse_id = '$whsid' and (a.id LIKE '%$keywords%' or m.id LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')";
            else $where = "(a.id LIKE '%$keywords%' or m.id LIKE '%$keywords%' or m.nama LIKE '%$keywords%')";
        }
        
        $this->db->from('so a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->where($where);
        //$q=$this->db->get();
        //echo $this->db->last_query();
        return $this->db->count_all_results();
    }

    public function so_add_stc(){
        $stcid = $this->session->userdata('userid');
	$member_id = $this->input->post('member_id');
	
	$totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
	$totalbv = str_replace(".","",$this->input->post('totalbv'));
        
        $empid = $this->session->userdata('user');
        $whsid = $this->session->userdata('whsid');
        $groupid = $this->session->userdata('group_id');
        $tgl = $this->input->post('date');
        
        $data = array(
            'member_id' => $member_id,
            'stockiest_id'=>$stcid,
            'tgl' => $tgl,
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
	    'totalbv' => $totalpv,
            'kit'=>'N',
            'warehouse_id' => $whsid,
	    'statuspu' => '0',
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('so',$data);
        
        $id = $this->db->insert_id();
        
	$key=0;
        while($key < count($_POST['counter'])){
            $qty = str_replace(".","",$this->input->post('qty'.$key));
            if($this->input->post('itemcode'.$key) and $qty > 0){
                $data=array(
                    'so_id' => $id,
                    'item_id' => $this->input->post('itemcode'.$key),
                    'qty' => $qty,
                    'harga' => str_replace(".","",$this->input->post('price'.$key)),
                    'pv' => str_replace(".","",$this->input->post('pv'.$key)),
		    'bv' => str_replace(".","",$this->input->post('bv'.$key)),
                    'jmlharga' =>str_replace(".","",$this->input->post('subtotal'.$key)),
                    'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$key)),
		    'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$key)),
		    'titipan_id' => $this->input->post('titipan_id'.$key)
                 );
                 $this->db->insert('so_d',$data);
            }
            $key++;
        }
	
	$this->db->query("call so_stc_procedure('$id','$tgl','$member_id','$stcid','$totalharga','$totalpv','$totalbv','$whsid','$groupid','$empid')");
    }
    
    public function check_so_invalid($stcid)
    {
        $data=array();
	$where = "so.tgl BETWEEN DATE_FORMAT(NOW() ,'%Y-%m-01') AND LAST_DAY(DATE_FORMAT(NOW() ,'%Y-%m-01')) AND so.totalharga_ = 0 AND so.totalharga > 0 and so.stockiest_id = '$stcid'";
        $q = $this->db->select("COUNT(*) as qty")
	    ->from('so')
	    ->where($where)
	    ->get();
	    
	//echo $this->db->last_query();
        
	if($q->num_rows() > 0){
            $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
    
    public function check_titipan($soid,$stcid)
    {
        $data=array();
        $q = $this->db->query("SELECT f_check_titipan_so('$soid','$stcid') as l_result");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['l_result'];
    }
    public function getSalesOrder($id=0){
        $data = array();
        
		/* Modified by Boby 2011-02-24 */
		// $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.kit,date_format(a.created,'%d-%b-%Y')as created,a.createdby,s.no_stc,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
		$select = "	
			a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,format(a.totalpv,0)as ftotalpv,a.remark,a.kit,date_format(a.created,'%d-%b-%Y')as created,a.createdby,s.no_stc,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi
			, m.noktp
		";
		if($this->session->userdata('group_id')>100){
			$select .= ",format(a.totalharga_,0)as ftotalharga";
		}else{
			$select .= ",format(a.totalharga,0)as ftotalharga";
		}
        $this->db->select($select,false);
		/* End modified by Boby 2011-02-24 */
		
        $this->db->from('so a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('member z','a.stockiest_id=z.id','left');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->session->userdata('group_id')>100){
			$this->db->where('a.stockiest_id',$this->session->userdata('userid'));
		}else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$this->db->where('a.warehouse_id',$whsid); 
        }
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getSalesOrderDetail($id=0){
        $data = array();
        
		/* Modified by Boby 2011-02-24 */
		// $this->db->select("d.item_id,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.pv,0)as fpv,format(sum(d.qty*d.harga),0)as fsubtotal,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name",false);
		$select = "	d.item_id,format(d.qty,0)as fqty,format(sum(d.jmlpv),0)as fsubtotalpv,format(d.pv,0)as fpv,a.name";
		if($this->session->userdata('group_id')>100){
			$select .="	,format(d.harga_,0)as fharga,format(sum(d.jmlharga_),0)as fsubtotal";
		}else{
			$select .="	,format(d.harga,0)as fharga,format(sum(d.jmlharga),0)as fsubtotal";
		}
        $this->db->select($select,false);
		/* End modified by Boby 2011-02-24 */
		
		$this->db->from('so_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.so_id',$id);
        $this->db->group_by('d.id');
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function so_add_admin(){
        $member_id = $this->input->post('member_id');
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
	$totalbv = str_replace(".","",$this->input->post('totalbv'));
        
        $cash = str_replace(".","",$this->input->post('tunai'));
        $debit = str_replace(".","",$this->input->post('debit'));
        $credit = str_replace(".","",$this->input->post('credit'));
        $totalbayar = str_replace(".","",$this->input->post('totalbayar'));
        
        $empid = $this->session->userdata('user');
        $whsid = $this->session->userdata('whsid');
        $groupid = $this->session->userdata('group_id');
        //$tgl = $this->input->post('date');
        $tgl = date('Y-m-d',now());
	
        $data = array(
            'member_id' => $member_id,
            'stockiest_id'=>'0',
            'tgl' => $tgl,
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
			'totalbv' => $totalpv,
            'kit'=>'N',
            'warehouse_id' => $whsid,
			'statuspu' => $this->session->userdata('s_pu'), // $this->input->post('pu'), updated by Boby 20141129
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('so',$data);
        
        $id = $this->db->insert_id();
        
		$key=0;
        while($key < count($_POST['counter'])){
            $qty = str_replace(".","",$this->input->post('qty'.$key));
            if($this->input->post('itemcode'.$key) and $qty > 0){
                $data=array(
                    'so_id' => $id,
                    'item_id' => $this->input->post('itemcode'.$key),
                    'qty' => $qty,
                    'harga' => str_replace(".","",$this->input->post('price'.$key)),
                    'pv' => str_replace(".","",$this->input->post('pv'.$key)),
		    'bv' => str_replace(".","",$this->input->post('bv'.$key)),
                    'jmlharga' =>str_replace(".","",$this->input->post('subtotal'.$key)),
                    'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$key)),
		    'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$key))
                 );
                 $this->db->insert('so_d',$data);
            }
            $key++;
        }
	
        if($totalbayar > 0){
            $data=array(
                'member_id' => $member_id,
                'tunai' => $cash,
                'debit_card' => $debit,
                'credit_card' => $credit,
                'total' => $totalbayar,
                'total_approved' => $totalbayar,
                'remark_fin' => 'Sales Order',
                'approved' => 'approved',
                'tgl_approved' => date('Y-m-d H:i:s',now()),
                'approvedby' => $this->session->userdata('user'),
                'flag' => 'mem',
                'event_id' => 'DP1',
                'created' => date('Y-m-d H:i:s',now()),
                'createdby' => $empid
            );
            $this->db->insert('deposit',$data);
        
            $id_deposit = $this->db->insert_id();
            $this->db->query("call sp_deposit('$id_deposit','$member_id','$totalbayar','mem','$empid')");
        }
	
		$this->db->query("call so_admin_procedure('$id','$tgl','$member_id','0','$totalharga','$totalpv','$totalbv','$whsid','$groupid','$empid')");
		
		/* Created by Boby 20140127 */
		$data = array(
			'member_id' => $this->input->post('member_id'),
			'kota' => $this->input->post('kota_id'),
			'alamat' => strip_quotes($this->db->escape_str($this->input->post('addr'))),
			'created' => $this->session->userdata('username')
		);
		
		$addr_id = $this->input->post('deli_ad');
		if(	$this->input->post('addr') != $this->input->post('addr1')	|| 	$this->input->post('kota_id') != $this->input->post('kota_id1')){
			$this->db->insert('member_delivery',$data);
			$addr_id = $this->db->insert_id();
			$this->db->update('member',array('delivery_addr'=>$addr_id),array('id' => $this->input->post('member_id')));
		}
		$this->db->update('so',array('delivery_addr'=>$addr_id),array('id' => $id));
    }
    
   // Created By ASP 20151201
    public function so_add_admin_v(){
        $member_id = $this->input->post('member_id');
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
	$totalbv = str_replace(".","",$this->input->post('totalbv'));
        
        $cash = str_replace(".","",$this->input->post('tunai'));
        $debit = str_replace(".","",$this->input->post('debit'));
        $credit = str_replace(".","",$this->input->post('credit'));
        $totalbayar = str_replace(".","",$this->input->post('totalbayar'));
        
        $empid = $this->session->userdata('user');
        //$whsid = $this->session->userdata('whsid');
        $whsid = $this->input->post('whsid');
        $groupid = $this->session->userdata('group_id');
        //$tgl = $this->input->post('date');
        $tgl = date('Y-m-d',now());
	
        $data = array(
            'member_id' => $member_id,
            'stockiest_id'=>'0',
            'tgl' => $tgl,
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
			'totalbv' => $totalpv,
            'kit'=>'N',
            'warehouse_id' => $whsid,
			'statuspu' => $this->session->userdata('s_pu'), // $this->input->post('pu'), updated by Boby 20141129
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('so',$data);
        
        $id = $this->db->insert_id();
        
		$key=0;
		if($this->input->post('vouchercode0')) {
			$vprice = (int)str_replace(".","",$this->input->post('vprice0'));
			$vpv = (int)str_replace(".","",$this->input->post('vpv0'));
			$vbv = (int)str_replace(".","",$this->input->post('vbv0'));
			$usedv = false;
			$updatesodv=false;
		}
        while($key < count($_POST['counter'])){
            $qty = str_replace(".","",$this->input->post('qty'.$key));
            if($this->input->post('itemcode'.$key) and $qty > 0){
				if($this->input->post('vouchercode0') and $vprice < (int)str_replace(".","",$this->input->post('price'.$key))*$qty and !$usedv){
					$harga = (int)str_replace(".","",$this->input->post('price'.$key)) - round($vprice/$qty);
					$finpv = (int)str_replace(".","",$this->input->post('pv'.$key)) - round($vpv/$qty);
					$finbv = (int)str_replace(".","",$this->input->post('bv'.$key)) - round($vbv/$qty);
					if($finpv<0)$finpv = 0;
					if($finbv<0)$finbv = 0;
					$finsubtotal = $qty*$harga;
					$finsubtotalpv = $qty*$finpv;
					$finsubtotalbv = $qty*$finbv;
					$data=array(
						'so_id' => $id,
						'item_id' => $this->input->post('itemcode'.$key),
						'qty' => $qty,
						'harga' => $harga,
						'pv' => $finpv,
						'bv' => $finbv,
						'jmlharga' =>$finsubtotal,
						'jmlpv' => $finsubtotalpv,
				'jmlbv' => $finsubtotalbv
					 );
					$usedv=true;
					$updatesodv=true;
					// Update status voucher
					$this->db->query('update voucher set status = 1 , posisi = 2, so_id = '.$id.', so_item_id = "'.$this->input->post('itemcode'.$key).'", so_item_qty = '.$qty.' where vouchercode = "'.$this->input->post('vouchercode0').'"');
				}else{
                $data=array(
                    'so_id' => $id,
                    'item_id' => $this->input->post('itemcode'.$key),
                    'qty' => $qty,
                    'harga' => str_replace(".","",$this->input->post('price'.$key)),
                    'pv' => str_replace(".","",$this->input->post('pv'.$key)),
		    'bv' => str_replace(".","",$this->input->post('bv'.$key)),
                    'jmlharga' =>str_replace(".","",$this->input->post('subtotal'.$key)),
                    'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$key)),
		    'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$key))
                 );
				 $updatesodv=false;
				}
                $this->db->insert('so_d',$data);
				 if($updatesodv){
					 $sodid = $this->db->insert_id();
					 // Update sodid voucher
					$this->db->query('update voucher set sod_id = '.$sodid.' where vouchercode = "'.$this->input->post('vouchercode0').'"');
				 }
            }
            $key++;
        }
	
        if($totalbayar > 0){
            $data=array(
                'member_id' => $member_id,
                'tunai' => $cash,
                'debit_card' => $debit,
                'credit_card' => $credit,
                'total' => $totalbayar,
                'total_approved' => $totalbayar,
                'remark_fin' => 'Sales Order',
                'approved' => 'approved',
                'tgl_approved' => date('Y-m-d H:i:s',now()),
                'approvedby' => $this->session->userdata('user'),
                'flag' => 'mem',
                'event_id' => 'DP1',
                'created' => date('Y-m-d H:i:s',now()),
                'createdby' => $empid
            );
            $this->db->insert('deposit',$data);
        
            $id_deposit = $this->db->insert_id();
            $this->db->query("call sp_deposit('$id_deposit','$member_id','$totalbayar','mem','$empid')");
        }
	
		$this->db->query("call so_admin_procedure('$id','$tgl','$member_id','0','$totalharga','$totalpv','$totalbv','$whsid','$groupid','$empid')");
		
		/* Created by Boby 20140127 */
		$data = array(
			'member_id' => $this->input->post('member_id'),
			'kota' => $this->input->post('kota_id'),
			'alamat' => strip_quotes($this->db->escape_str($this->input->post('addr'))),
			// START ASP 20180409
			'pic_name' => strip_quotes($this->db->escape_str($this->input->post('pic_name'))),
			'pic_hp' => strip_quotes($this->db->escape_str($this->input->post('pic_hp'))),
			'kelurahan' => strip_quotes($this->db->escape_str($this->input->post('kelurahan'))),
			'kecamatan' => strip_quotes($this->db->escape_str($this->input->post('kecamatan'))),
			'kodepos' => strip_quotes($this->db->escape_str($this->input->post('kodepos'))),
			// END ASP 20180409
			'created' => $this->session->userdata('username')
		);
		
		$addr_id = $this->input->post('deli_ad');
		if(	$this->input->post('addr') != $this->input->post('addr1')	|| 	$this->input->post('kota_id') != $this->input->post('kota_id1')){
			$this->db->insert('member_delivery',$data);
			$addr_id = $this->db->insert_id();
			$this->db->update('member',array('delivery_addr'=>$addr_id),array('id' => $this->input->post('member_id')));
		}
		$this->db->update('so',array('delivery_addr'=>$addr_id),array('id' => $id));
    }
	//Created By ASP 20151130
	public function count_search_voucher_so($so_id){
        $this->db->select("v.vouchercode",false);
        $this->db->from('voucher v');
		//$this->db->join('item i','s.item_id=i.id','left');
		
		$where = "v.so_id = $so_id";
		$this->db->where($where);
        return $this->db->count_all_results();
    }
	
    public function search_voucher_so($so_id){
        $data = array();
		/*
        $this->db->select("s.item_id,i.name,s.qty,format(s.qty,0)as fqty,i.price,format(i.price,0)as fprice,i.price2,format(i.price2,0)as fprice2,i.pv,format(i.pv,0)as fpv,i.bv",false);
        $this->db->from('stock s');
	$this->db->join('item i','s.item_id=i.id','left');
		*/
        $this->db->select("v.vouchercode, v.price, format(v.price,0) as fprice, v.pv, format(v.pv,0) as fpv, v.bv, format(v.bv,0) as fbv, v.remark",false);
        $this->db->from('voucher v');
		$where = "v.so_id = $so_id";
		$this->db->where($where);
        $this->db->order_by('v.expired_date','asc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getSalesOrder_v($id=0){
        $data = array();
        
		/* Modified by Boby 2011-02-24 */
		// $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.kit,date_format(a.created,'%d-%b-%Y')as created,a.createdby,s.no_stc,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
		$select = "	
			a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,format(a.totalpv,0)as ftotalpv,format(a.totalpv+ifnull(v.pv,0),0)as ftotalpv_,a.remark,a.kit,date_format(a.created,'%d-%b-%Y')as created,a.createdby,s.no_stc,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi
			, m.noktp
			, md.alamat as dalamat,k2.name as dkota,p2.name as dpropinsi
			, md.pic_name, md.pic_hp,md.alamat as del_alamat,md.kecamatan as del_kecamatan,md.kelurahan as del_kelurahan,md.kodepos as del_kodepos,k2.name as del_kota,p2.name as del_propinsi
		";
		if($this->session->userdata('group_id')>100){
			$select .= ",format(a.totalharga_-(ifnull(v.tvprice,0)),0)as ftotalharga,format(a.totalharga_,0)as ftotalharga_";
		}else{
			$select .= ",format(a.totalharga,0)as ftotalharga,format(a.totalharga_,0)as ftotalharga_";
		}
        $this->db->select($select,false);
		/* End modified by Boby 2011-02-24 */
		
        $this->db->from('so a');
        $this->db->join('
						(
						 	select so_id, sum(price) as tvprice, sum(pv) as pv
							from voucher v
							group by v.so_id
						 ) as v
						','a.id=v.so_id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('member z','a.stockiest_id=z.id','left');
		$this->db->join('member_delivery md','a.delivery_addr=md.id','left');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
		$this->db->join('kota k2','md.kota=k2.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
		$this->db->join('propinsi p2','k2.propinsi_id=p2.id','left');
        if($this->session->userdata('group_id')>100){
			$this->db->where('a.stockiest_id',$this->session->userdata('userid'));
		}else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$this->db->where('a.warehouse_id',$whsid); 
        }
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getSalesOrderDetail_v($id=0){
        $data = array();
        
		/* Modified by Boby 2011-02-24 */
		// $this->db->select("d.item_id,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.pv,0)as fpv,format(sum(d.qty*d.harga),0)as fsubtotal,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name",false);
		$select = "	d.item_id,format(d.qty,0)as fqty,format(sum(d.jmlpv+ifnull(v.pv,0)),0)as fsubtotalpv,format(d.pv+ifnull(v.pv,0),0)as fpv,a.name";
		if($this->session->userdata('group_id')>100){
			$select .="	,format(d.harga_,0)as fharga,format(sum(d.jmlharga_),0)as fsubtotal,format(d.harga_,0)as fharga_,format(sum(d.jmlharga_),0)as fsubtotal_";
		}else{
			$select .="	,format(d.harga+ifnull(v.price,0),0)as fharga,format(sum(d.jmlharga+ifnull(v.price,0)),0)as fsubtotal,format(d.harga_,0)as fharga_,format(sum(d.jmlharga_),0)as fsubtotal_";
		}
        $this->db->select($select,false);
		/* End modified by Boby 2011-02-24 */
		
		$this->db->from('so_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->join('voucher v','d.so_id = v.so_id and d.id = v.sod_id and v.so_item_id=d.item_id','left');
        $this->db->where('d.so_id',$id);
        $this->db->group_by('d.id');
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
    public function so_add_stc_v(){
        $stcid = $this->session->userdata('userid');
	$member_id = $this->input->post('member_id');
	
	$totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
	$totalbv = str_replace(".","",$this->input->post('totalbv'));
        
        $empid = $this->session->userdata('user');
        $whsid = $this->session->userdata('whsid');
        $groupid = $this->session->userdata('group_id');
        $tgl = $this->input->post('date');
		$tgltime = date('Y-m-d H:i:s');
        
        $data = array(
            'member_id' => $member_id,
            'stockiest_id'=>$stcid,
            'tgl' => $tgl,
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
	    	'totalbv' => $totalpv,
            'kit'=>'N',
            'warehouse_id' => $whsid,
	    	'statuspu' => '0',
			//START ASP 20190118
			'status' => 'delivery',
			'status1' => 'delivery',
			'status2' => 'delivery',
			'tglapproved' => $tgl,
			'approvedby' => $empid,
			'tglapproved1' => $tgltime,
			'approvedby1' => $empid,
			'tglapproved2' => $tgltime,
			'approvedby2' => $empid,
			//END ASP 20190118
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('so',$data);
        
        $id = $this->db->insert_id();
        
	$key=0;
	//Voucher
	if($this->input->post('vouchercode0')) {
		$vprice = (int)str_replace(".","",$this->input->post('vprice0'));
		$vpv = (int)str_replace(".","",$this->input->post('vpv0'));
		$vbv = (int)str_replace(".","",$this->input->post('vbv0'));
		$usedv = false;
		$updatesodv = false;
	}
	//eof Voucher
        while($key < count($_POST['counter'])){
            $qty = str_replace(".","",$this->input->post('qty'.$key));
            if($this->input->post('itemcode'.$key) and $qty > 0){
				if($this->input->post('vouchercode0') and $vprice < (int)str_replace(".","",$this->input->post('price'.$key))*$qty and !$usedv){
					$harga = (int)str_replace(".","",$this->input->post('price'.$key)) - round($vprice/$qty);
					$finpv = (int)str_replace(".","",$this->input->post('pv'.$key)) - round($vpv/$qty);
					$finbv = (int)str_replace(".","",$this->input->post('bv'.$key)) - round($vbv/$qty);
					if($finpv<0)$finpv = 0;
					if($finbv<0)$finbv = 0;
					$finsubtotal = $qty*$harga;
					$finsubtotalpv = $qty*$finpv;
					$finsubtotalbv = $qty*$finbv;
					$data=array(
						'so_id' => $id,
						'item_id' => $this->input->post('itemcode'.$key),
						'qty' => $qty,
						'harga' => $harga,
						'pv' => $finpv,
						'bv' => $finbv,
						'jmlharga' =>$finsubtotal,
						'jmlpv' => $finsubtotalpv,
						'jmlbv' => $finsubtotalbv,
						'warehouse_id' => $whsid,
						'titipan_id' => $this->input->post('titipan_id'.$key)
					 );
					$usedv=true;
					$updatesodv=true;
					// Update status voucher
					$this->db->query('update voucher set status = 1 , posisi = 1, so_id = '.$id.', so_item_id = "'.$this->input->post('itemcode'.$key).'", so_item_qty = '.$qty.' where vouchercode = "'.$this->input->post('vouchercode0').'"');
				}else{
					$data=array(
						'so_id' => $id,
						'item_id' => $this->input->post('itemcode'.$key),
						'qty' => $qty,
						'harga' => str_replace(".","",$this->input->post('price'.$key)),
						'pv' => str_replace(".","",$this->input->post('pv'.$key)),
						'bv' => str_replace(".","",$this->input->post('bv'.$key)),
						'jmlharga' =>str_replace(".","",$this->input->post('subtotal'.$key)),
						'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$key)),
						'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$key)),
						'warehouse_id' => $whsid,
						'titipan_id' => $this->input->post('titipan_id'.$key)
					 );
					$updatesodv=false;
				}
                 $this->db->insert('so_d',$data);
				 if($updatesodv){
					 $sodid = $this->db->insert_id();
					 // Update sodid voucher
					$this->db->query('update voucher set sod_id = '.$sodid.' where vouchercode = "'.$this->input->post('vouchercode0').'"');
				 }
            }
            $key++;
        }
	if($this->input->post('vouchercode0')) {
		$this->db->query("call so_stc_procedure_v('$id','$tgl','$member_id','$stcid','$totalharga','$totalpv','$totalbv','$whsid','$groupid','$empid')");
	}else{
		$this->db->query("call so_stc_procedure('$id','$tgl','$member_id','$stcid','$totalharga','$totalpv','$totalbv','$whsid','$groupid','$empid')");
	}
    }
	// End Created By ASP 20151130
	
	// Created By ASP 20180308
    public function getSalesOrderDetail_p_list_v($id=0){
        $data = array();
        
		/* Modified by Boby 2011-02-24 */
		// $this->db->select("d.item_id,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.pv,0)as fpv,format(sum(d.qty*d.harga),0)as fsubtotal,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name",false);
		$select = "	
			IFNULL(m.item_id,d.item_id) AS item_id
			,CASE WHEN m.item_id IS NULL THEN FORMAT(d.qty,0)
			 ELSE FORMAT(m.qty*d.qty,0)
			 END AS fqty
			,CASE WHEN m.item_id IS NULL THEN a.name
			 ELSE b.name
			 END AS name		";
        $this->db->select($select,false);
		/* End modified by Boby 2011-02-24 */
		
		$this->db->from('so_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->join('manufaktur m','d.item_id = m.manufaktur_id','left');
        $this->db->join('item b',' m.item_id = b.id','left');
        $this->db->where('d.so_id',$id);
        //$this->db->group_by('d.id');
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }

	public function getDropDownWhs($all){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            if($all == 'all')$data['all']='All Cabang';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
	
    public function getWhsName($id){
        $data = array();
        $q = $this->db->get_where('warehouse',array('id' => $id));
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[]=$row;    
            }
        }
        $q->free_result();
        return $data;
    }
	// EOF Created By ASP 20180308
}?>