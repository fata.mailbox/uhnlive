<?php

class Mpointemp extends CI_Model{

    function __construct()

    {

        parent::__construct();

    }

    

    public function saveEmployee($empid, $empname, $member_id, $uid){

		if($this->_getAccount($member_id)){

			$query = "CALL sp_employee_save('$empid', '$empname', '$member_id', '$uid')";

			$q = $this->db->query($query);

			$this->session->set_flashdata('message','Register complete..');

		}else{

			$this->session->set_flashdata('message','Member id already registered!');

		}

	}

	

	public function cekSaldo($member_id, $blj){

		$this->db->query("CALL sp_point_expa();");

		

		$query = "

			SELECT 

				e.nik, e.saldo

			FROM uhn_staff_master e

			WHERE e.nik = '$member_id' AND e.saldo >= $blj

		";

		

		$q = $this->db->query($query);

        if($q->num_rows() > 0){

            return $q->num_rows();

        }

        return $q->num_rows();

    }

	

	public function countAdmin($keywords=0){

        $this->db->select("nik");

        $this->db->from('uhn_staff_master');

		$where = "(nik LIKE '%$keywords%' OR nama LIKE '%$keywords%') AND active = 1 ";

        $this->db->where($where);

		// echo $this->db->last_query();

        return $this->db->count_all_results();

    }

	public function searchAdmin($keywords=0,$num,$offset){

        $data = array();

		$this->db->select("nik, nama, saldo, active");

        $this->db->from('uhn_staff_master');

		$where = "(nik LIKE '%$keywords%' OR nama LIKE '%$keywords%') AND active = 1 ";

        $this->db->where($where);

		$this->db->limit($num,$offset);

        

		$q = $this->db->get();

        // echo $this->db->last_query();

        if($q->num_rows > 0){

            foreach($q->result_array() as $row){

                $data[] = $row;

            }

        }

        $q->free_result();

        return $data;

    }

	

	public function addRedeem(){

        $totalharga = str_replace(".","",$this->input->post('total'));

        $empid = $this->session->userdata('user');

        $whsid = $this->input->post('whsid');

        $admin_id = $this->input->post('member_id');

        

        $data = array(

            'admin_id'=>$admin_id,

            'tgl' => date('Y-m-d',now()),

            'totalharga' => $totalharga,

            'warehouse_id' => $whsid,

            'remark'=>$this->db->escape_str($this->input->post('remark')),

            'created'=>date('Y-m-d H:i:s',now()),

            'createdby'=>$empid

        );

        $this->db->insert('uhn_staff_point',$data);

        

        $id = $this->db->insert_id();

        

		for($i=0;$i<10;$i++){

			$qty = str_replace(".","",$this->input->post('qty'.$i));

			if($this->input->post('itemcode'.$i) and $qty > 0){

				$data=array(

					'uhn_staff_point_id' => $id,

					'item_id' => $this->input->post('itemcode'.$i),

					'qty' => $qty,

					'harga' => str_replace(".","",$this->input->post('price'.$i)),

					'jmlharga' => str_replace(".","",$this->input->post('subtotal'.$i))

				);

				

				$this->db->insert('uhn_staff_point_d',$data);

			}

		}

       
		//echo "CALL sp_point_redem('$id','$admin_id','$totalharga','$whsid','$empid')";
        $this->db->query("CALL sp_point_redem('$id','$admin_id','$totalharga','$whsid','$empid')");

    }

	

	public function searchRedeem($keywords=0,$num,$offset){

        $data = array();

        $whsid = $this->session->userdata('whsid');

        if($whsid > 1)$where = "pnt.warehouse_id = '$whsid' and (pnt.id LIKE '$keywords%' or adm.nama LIKE '$keywords%' OR adm.nik LIKE '$keywords%')";

        else $where = "( pnt.id LIKE '$keywords%' or adm.nama LIKE '$keywords%' OR adm.nik LIKE '$keywords%' )";

        

        $this->db->select("pnt.id,date_format(pnt.tgl,'%d-%b-%Y')as tgl,pnt.createdby,pnt.admin_id,adm.nik,format(pnt.totalharga,0)as ftotalharga,format(0,0)as ftotalpv,adm.nama,pnt.remark",false);

        $this->db->from('uhn_staff_point pnt');

        $this->db->join('uhn_staff_master adm','pnt.admin_id=adm.nik','left');

        $this->db->where($where);

        $this->db->order_by('pnt.id','desc');

        $this->db->limit($num,$offset);

        $q = $this->db->get();

        //echo $this->db->last_query();

        if($q->num_rows > 0){

            foreach($q->result_array() as $row){

                $data[] = $row;

            }

        }

        $q->free_result();

        return $data;

    }

	

	public function countRedeem($keywords=0){

        $whsid = $this->session->userdata('whsid');

        if($whsid > 1)$where = "pnt.warehouse_id = '$whsid' and (pnt.id LIKE '$keywords%' or adm.nama LIKE '$keywords%' OR adm.nik LIKE '$keywords%')";

        else $where = "( pnt.id LIKE '$keywords%' or adm.nama LIKE '$keywords%' OR adm.nik LIKE '$keywords%' )";

        

        $this->db->select("pnt.id");

        $this->db->from('uhn_staff_point pnt');

        $this->db->join('uhn_staff_master adm','pnt.admin_id=adm.nik','left');

        $this->db->where($where);

        return $this->db->count_all_results();

    }

	

	public function getRedeem($id=0){

        $data = array();

        $this->db->select("

			pjm.id, date_format(pjm.tgl,'%d-%b-%Y')as tgl, format(pjm.totalharga,0)as ftotalharga, format(0,0)as ftotalpv, pjm.remark, date_format(pjm.created,'%d-%b-%Y')as created,pjm.createdby

            , adm.nik as no_stc

			, adm.nama as nama, whs.address as alamat, '123' as kodepos

			, 'UHN Kota' as kota

			, 'UHN Propinsi' as propinsi

			",false);

        $this->db->from('uhn_staff_point pjm');

        $this->db->join('uhn_staff_master adm','pjm.admin_id=adm.nik','left');

		$this->db->join('warehouse whs','pjm.warehouse_id=whs.id','left');

        //$this->db->join('member m','a.stockiest_id=m.id','left');

        //$this->db->join('kota k','m.kota_id=k.id','left');

        //$this->db->join('propinsi p','k.propinsi_id=p.id','left');

        

        $whsid = $this->session->userdata('whsid');

        if($whsid > 1)$this->db->where('pjm.warehouse_id',$whsid); 

        $this->db->where('pjm.id',$id);

        $q=$this->db->get();

        //echo $this->db->last_query();

        if($q->num_rows() > 0){

            $data = $q->row_array();

        }

        $q->free_result();

        return $data;

    }

	

	public function getRedeemD($id=0){

        $data = array();

        $this->db->select("

			d.item_id,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(0,0)as fpv,format(sum(d.qty*d.harga),0)as fsubtotal,format(sum(d.qty*0),0)as fsubtotalpv

			,a.name

			",false);

        $this->db->from('uhn_staff_point_d d');

        $this->db->join('item a','d.item_id=a.id','left');

        $this->db->where('d.uhn_staff_point_id',$id);

        $this->db->group_by('d.id');

        $q=$this->db->get();

            //echo $this->db->last_query();

        if($q->num_rows()>0){

            foreach($q->result_array() as $row){

                $data[]=$row;

            }

        }

        $q->free_result();

        return $data;

    }

	

	public function getBalanceEwalletMember($memberid,$fromdate,$todate){

        $data=array();

        $where = array('s.nik'=>$memberid,'s.tgl >='=>$fromdate,'s.tgl <='=>$todate);

        

        

        $q=$this->db->select("concat(s.noref,' - ',s.description)as description,s.created,createdby,format(s.saldoawal,0)as fsaldoawal,format(s.kredit,0)as fkredit,format(s.debet,0)as fdebet,format(s.saldoakhir,0)as fsaldoakhir", false)

            ->from('uhn_staff_saldo_his s')

            ->where($where)

            ->order_by('s.id','asc')

            ->get();

        //echo $this->db->last_query();

        if($q->num_rows()>0){

            foreach($q->result_array() as $row){

                $data[]=$row;

            }

        }

        $q->free_result();

        return $data;

    }

	

	public function sumBalanceEwalletMember($memberid,$fromdate,$todate){

        $data=array();

        $where = array('s.nik'=>$memberid,'s.tgl >='=>$fromdate,'s.tgl <='=>$todate);

		

        $q=$this->db->select("format(sum(s.kredit),0)as fkredit,format(sum(s.debet),0)as fdebet", false)

            ->from('uhn_staff_saldo_his s')

            ->where($where)

            ->get();

        //echo $this->db->last_query();

        if($q->num_rows()>0){

            $data=$q->row_array();

        }

        $q->free_result();

        return $data;

    }

	

	public function search_emp($keywords=0,$num,$offset){

        $data = array();

        $this->db->select("a.nik,a.nama AS name",false);

        $this->db->from('uhn_staff_master a');

        $this->db->like('a.nama',$keywords,'between');

        $this->db->or_like('a.nik',$keywords,'between');

        $this->db->order_by('a.nama','asc');

        $this->db->limit($num,$offset);

        $q = $this->db->get();

        //echo $this->db->last_query();

        if($q->num_rows > 0){

            foreach($q->result_array() as $row){

                $data[] = $row;

            }

        }

        $q->free_result();

        return $data;

    }

	

    public function count_emp($keywords=0){

        $this->db->like('a.nama',$keywords,'between');        

        $this->db->or_like('a.nik',$keywords,'between');

        $this->db->from('uhn_staff_master a');

        return $this->db->count_all_results();

    }

	

	public function add(){

        $data=array(

            'nik' => $this->input->post('nik'),

            'nama' => $this->input->post('name'),

            'grade' => $this->input->post('grade'),

            'getsaldo' => $this->input->post('saldo'),

            'saldo' => $this->input->post('saldo'),

			'active' => 1

        );

            

        $this->db->insert('uhn_staff_master',$data);

    }

	

	public function check_nik($id){

        $q=$this->db->select("nik",false)

            ->from('uhn_staff_master')

            ->where('nik',$id)

            ->get();

        return ($q->num_rows() > 0)? true : false;    

    }

	

	public function get($id){

        $data=array();

        $q = $this->db->get_where('uhn_staff_master',array('nik'=>$id));

        if($q->num_rows()>0){

            $data=$q->row();

        }

        $q->free_result();

        return $data;

    }

	

	public function edit(){

        $data=array(

            'nama' => $this->input->post('name'),

            'grade' => $this->input->post('grade'),

            'saldo' => $this->input->post('saldo'),

            'active' => $this->input->post('status')

        );

        $this->db->update('uhn_staff_master',$data,array('nik'=>$this->input->post('nik')));

    }

}?>

