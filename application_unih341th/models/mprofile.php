<?php
class MProfile extends CI_Model{
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('my'));
        $this->load->helper('url');

    }
    
    /*
    |--------------------------------------------------------------------------
    | profile member
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @copyright www.smartindo-technology.com
    | @created 2009-04-08
    |
    */
    
    public function getProfile($id=0){
        $q = $this->db->select("m.id,m.lengkap,m.nama,m.tgllahir,m.sponsor_id,x.nama as namastc,z.no_stc,m.enroller_id,m.stockiest_id,m.account_id,date_format(m.tglaplikasi,'%d-%m-%Y')as ftglaplikasi,m.jk,m.tempatlahir,
                date_format(m.tgllahir,'%d-%m-%Y')as ftgllahir,m.alamat,m.kota_id,m.email,m.hp,m.telp,m.fax,m.kodepos,m.noktp,m.created,
                m.npwp,m.ahliwaris,b.bank_id,r.name as namabank,b.flag,b.name as namanasabah,b.no,b.area,k.name as kota,p.name as propinsi,e.nama as namaenroller,s.nama as namasponsor, m.kecamatan, m.kelurahan, mk.form, mk.rek, mk.ktp",false)
            ->from('member m')
            ->join('account b','m.account_id = b.id','left')
            ->join('member_kelengkapan mk','m.id = mk.id','left')
            ->join('kota k','m.kota_id = k.id','left')
            ->join('propinsi p','k.propinsi_id = p.id','left')
            ->join('member e','m.enroller_id = e.id','left')
            ->join('member s','m.sponsor_id = s.id','left')
            ->join('member x','m.stockiest_id = x.id','left')
            ->join('stockiest z','m.stockiest_id = z.id','left')
            ->join('bank r','b.bank_id = r.id','left')
            ->where('m.id',$id)
            ->limit(1)
            ->get();
            //echo $this->db->last_query();
			// b.flag Modified by Boby (2009-11-19)
        return $row = ($q->num_rows() > 0) ? $q->row_array() : false;
    }
    public function getDropDownBank(){
        $data = array();
        
        $q = $this->db->select("id,name",false)
            ->from('bank')
            ->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[$row['id']] = $row['name'];
            }
        }
        $q->free_result();
        return $data;
    }
    public function updateKelengkapan(){
		if($this->session->userdata('group_id') <= 100){
			if($this->input->post('data_form') == '1')$data_form = 1;
			else $data_form = 0;
			if($this->input->post('data_rek') == '1')$data_rek = 1;
			else $data_rek = 0;
			if($this->input->post('data_ktp') == '1')$data_ktp = 1;
			else $data_ktp = 0;

			$data = array(
				'form'  => $data_form,
				'rek'  => $data_rek,
				'ktp'  => $data_ktp,
				);
			$this->db->update('member_kelengkapan',$data,array('id' => $this->input->post('id')));
		}
	}
    public function updateProfile(){
	
	/*Modified by Boby (2009-11-19)*/	
	/*
	echo "<br>1 ".$this->input->post('bank_id')." ".$this->input->post('currbank_id');
	echo "<br>2 ".$this->input->post('namanasabah')." ".$this->input->post('currnamanasabah');
	echo "<br>3 ".$this->input->post('norek')." ".$this->input->post('currnorek');
	*/
	if($this->input->post('bank_id') != $this->input->post('currbank_id')){$flag = 0; }
	else if($this->input->post('namanasabah') != $this->input->post('currnamanasabah')){$flag = 0;}
	else if($this->input->post('norek') != $this->input->post('currnorek')){$flag = 0;}
	else{$flag = $this->input->post('flag');}
	
	if($this->session->userdata('group_id') <= 100){
		/*
		if($this->input->post('lengkap') == '1')$lengkap = 1;
		else $lengkap = 0;
		*/
		/*end Modified by Boby (2009-11-19)*/
		// 20160913 ASP Start
		if($this->input->post('data_form') == '1'&&$this->input->post('data_rek') == '1'&&$this->input->post('data_ktp') == '1')$lengkap = 1;
		else $lengkap = 0;
		// 20160913 ASP End

        $data = array(
            'alamat'  => $this->input->post('alamat'),
			//20160327 - ASP Start
            'kecamatan'  => $this->input->post('kecamatan'),
            'kelurahan'  => $this->input->post('kelurahan'),
			//20160327 - ASP End
            'alamat'  => $this->input->post('alamat'),
            'kota_id'  => $this->input->post('kota_id'),
            'kodepos'  => $this->input->post('kodepos'),
            'noktp'  => $this->input->post('noktp'),
            'tempatlahir'  => $this->input->post('tempatlahir'),
            'tgllahir'  => $this->input->post('date'),
            'jk'    => $this->input->post('jk'),
            'telp'  => $this->input->post('telp'),
            'fax'    => $this->input->post('fax'),
            'hp'    => $this->input->post('hp'),
            'email'    => $this->input->post('email'),
            'ahliwaris'    => $this->input->post('ahliwaris'),
            'npwp'    => $this->input->post('npwp'),
			'lengkap'    => $lengkap,
			'updated'   => date('Y-m-d', now()),
            'updatedby'    => $this->session->userdata('username')
        );
	}else{
		$data = array(
            'alamat'  => $this->input->post('alamat'),
			//20160327 - ASP Start
            'kecamatan'  => $this->input->post('kecamatan'),
            'kelurahan'  => $this->input->post('kelurahan'),
			//20160327 - ASP End
            'kota_id'  => $this->input->post('kota_id'),
            'kodepos'  => $this->input->post('kodepos'),
            'noktp'  => $this->input->post('noktp'),
            'tempatlahir'  => $this->input->post('tempatlahir'),
            'tgllahir'  => $this->input->post('date'),
            'jk'    => $this->input->post('jk'),
            'telp'  => $this->input->post('telp'),
            'fax'    => $this->input->post('fax'),
            'hp'    => $this->input->post('hp'),
            'email'    => $this->input->post('email'),
            'ahliwaris'    => $this->input->post('ahliwaris'),
            'npwp'    => $this->input->post('npwp'),
			'updated'   => date('Y-m-d', now()),
            'updatedby'    => $this->session->userdata('username')
        );
	}
	
        $this->db->update('member',$data,array('id' => $this->input->post('id')));
        
        if($this->input->post('area') && $this->input->post('norek')){
			// Update by Boby 20130920
			$char_ = array(".","-"," ");
			$norek = str_replace($char_, "", $this->input->post('norek'));
			$namanasabah = strtoupper($this->input->post('namanasabah'));
			// End update by Boby 20130920
			
            $data = array(
                'bank_id' => $this->input->post('bank_id'),
                'member_id' => $this->input->post('id'),
                'name' => $namanasabah,
                'no' => $norek,
                'area' => $this->input->post('area'),
				'flag' => $flag,	// Modified by Boby (2009-11-19)
                'createdby' => $this->session->userdata('username'),
                'created' => date('Y-m-d H:i:s', now())
            );
            
            if($this->input->post('account_id') > 0){
                $row = $this->_getAccountBank($this->input->post('account_id'));
                if($row){
                    if($row->bank_id != $this->input->post('bank_id')){
                        $this->db->insert('account',$data);
                        
                        $id = $this->db->insert_id();
                        $this->db->update('member',array('account_id'=>$id),array('id' => $this->input->post('id')));
                    }else{
                        if($row->name != $namanasabah){
                            $this->db->insert('account',$data);
                            $id = $this->db->insert_id();
                            $this->db->update('member',array('account_id'=>$id),array('id' => $this->input->post('id')));
                        }else{
                            if($row->no != $norek){
                                $this->db->insert('account',$data);
                                
                                $id = $this->db->insert_id();
                                $this->db->update('member',array('account_id'=>$id),array('id' => $this->input->post('id')));
                            }else{
                                if($row->area != $this->input->post('area')){
                                    $this->db->insert('account',$data);
                                    
                                    $id = $this->db->insert_id();
                                    $this->db->update('member',array('account_id'=>$id),array('id' => $this->input->post('id')));
                                }
                            }
                        }
                    }
                }
            }else{
                $this->db->insert('account',$data);
                
                $id = $this->db->insert_id();
                $this->db->update('member',array('account_id'=>$id),array('id' => $this->input->post('id')));
            }

        }

        //START Proses Insert To APi Ecommerce
        $bank_id = $this->get_bank_id_by_act($this->input->post('bank_id'));
        $kota_id_fe = $this->getKotaIdByIdBe($this->input->post('kota_id'));
        $kota_name = $this->getKotaNameByIdBe($this->input->post('kota_id'));
        $member_id_fe = $this->getMemberIdFe($this->input->post('id'));
        $member_name = $this->getMemberName($this->input->post('id'));
        $propinsi = $this->getPropinsiId($this->input->post('kota_id'));
        $placementid = $this->getMemberIdFe($this->getUplineId($this->input->post('id')));
        $introducerid = $this->getMemberIdFe($this->getSponsorId($this->input->post('id')));
        $member_id_address = $this->getMemberAddressIdFe($this->input->post('id'));

        $firstname  =   explode(" ", $member_name)[0];
        $last  =   explode(" ", $member_name)[1]  . ' ' . explode(" ", $member_name)[2];
        if($last==' '){
            $lastname = '-';
        }else{
            $lastname = $last;
        }

        if($this->input->post('jk') == 'Perempuan'){
            $jeniskelamin = '2';
        }else{
            $jeniskelamin = '1';
        }



        $customer_array = array(
            "id"=>$member_id_fe,
            "email"=>$email,
            "firstname"=>$firstname,
            "lastname"=>$lastname,
            "website_id"=>1,
            "dob"=>$this->input->post('date')
            
        );

        $is_anonymized = array("attribute_code"=>"is_anonymized","value"=>"0");
        $ca_jenis_kelamin = array("attribute_code"=>"ca_jenis_kelamin","value"=>$jeniskelamin);
        $ca_bank = array("attribute_code"=>"ca_bank","value"=>$bank_id);
        $ca_status_aktif_member = array("attribute_code"=>"ca_status_aktif_member","value"=>"0");
        $ca_direct_upline_id = array("attribute_code"=>"ca_direct_upline_id","value"=>$placementid);
        $ca_id_sponsor = array("attribute_code"=>"ca_id_sponsor","value"=>$introducerid);
        $ca_id_anggota = array("attribute_code"=>"ca_id_anggota","value"=>$this->input->post('id'));
        $ca_kode_aktivasi = array("attribute_code"=>"ca_kode_aktivasi","value"=>$activation);
        $ca_tempat_lahir = array("attribute_code"=>"ca_tempat_lahir","value"=>$this->input->post('tempatlahir'));
        $ca_nomor_ktp = array("attribute_code"=>"ca_nomor_ktp","value"=>$this->input->post('noktp'));
        $ca_nomor_handphone = array("attribute_code"=>"ca_nomor_handphone","value"=>$this->input->post('hp'));
        $ca_ahli_waris = array("attribute_code"=>"ca_ahli_waris","value"=>$this->input->post('ahliwaris'));
        $ca_nomor_rekening = array("attribute_code"=>"ca_nomor_rekening","value"=>$norek);
        $ca_cabang = array("attribute_code"=>"ca_cabang","value"=>$this->input->post('area'));
        $ca_tanggal_lahir = array("attribute_code"=>"ca_tanggal_lahir","value"=>$this->input->post('tgllahir'));
        $ca_kecamatan = array("attribute_code"=>"ca_kecamatan","value"=>$this->input->post('kecamatan'));
        $ca_kelurahan = array("attribute_code"=>"ca_kelurahan","value"=>$this->input->post('kelurahan'));
        $ca_kode_pos = array("attribute_code"=>"ca_kode_pos","value"=>$this->input->post('kodepos'));
        $ca_provinsi = array("attribute_code"=>"ca_provinsi","value"=>$propinsi);

        $customer_array['custom_attributes'] = array($is_anonymized, $ca_jenis_kelamin, $ca_bank,$ca_status_aktif_member, $ca_direct_upline_id
        , $ca_id_sponsor, $ca_id_sponsor, $ca_id_anggota, $ca_kode_aktivasi, $ca_tempat_lahir, $ca_nomor_ktp
        , $ca_nomor_handphone, $ca_ahli_waris, $ca_nomor_rekening, $ca_cabang, $ca_tanggal_lahir, $ca_kecamatan
        , $ca_kecamatan, $ca_kelurahan, $ca_kode_pos, $ca_provinsi);

        
        $adress_arr = array("id"=>$member_id_address, "customer_id"=>$member_id_fe,  "firstname"=>$firstname,  "lastname"=>$lastname, 
        "region"=>array("region_code"=>"$kota_name", "region_id"=>$kota_id_fe, "region"=>"$kota_name"), "countryId"=> "ID", "postcode"=> $kodepos, "city"=> $kota_id_fe,
        "street"=>array($alamat));

        $customer_array['addresses'] = array($adress_arr);



        $customer['customer'] = $customer_array;
        //$customer['password'] = $password;
        //var_dump($customer); die();
        //$data = "'" . json_encode($customer) . "'";    
        $data = json_encode($customer); 

        // print_r(json_encode($customer, JSON_PRETTY_PRINT) . " <br/>"); die();
        $token = get_token();

        $update_customer = $this->update_customer_api($token, $data, $member_id_fe);


        //EOF Proses Insert To APi Ecommerce
        
    }

    // START API Ecommerce
    public function getKotaIdByIdBe($kota_id){
        $data = array();
        $q=$this->db->select("id_fe",false)
            ->from('kota')
            ->where('id',$kota_id)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('id_fe');
        }
        return $data;
    }
    public function getKotaNameByIdBe($kota_id){
        $data = array();
        $q=$this->db->select("name",false)
            ->from('kota')
            ->where('id',$kota_id)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('name');
        }
        return $data;
    }
    public function getPropinsiId($kota_id){
        $data = array();
        $q=$this->db->select("propinsi_id",false)
            ->from('kota')
            ->where('id',$kota_id)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('propinsi_id');
        }
        return $data;
    }

    function get_bank_id_by_act($bank_id){

        $array = array('id'=>$bank_id);
        $q = $this->db->select("bank_id_fe")   
            ->from('bank')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row()->bank_id_fe : false;

    }
    public function getMemberAddressIdFe($member_id){
        $data = array();
        $q=$this->db->select("address_id_fe",false)
            ->from('member')
            ->where('id',$member_id)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('address_id_fe');
        }
        return $data;
    }


    public function getMemberIdFe($member_id){
        $data = array();
        $q=$this->db->select("id_fe",false)
            ->from('member')
            ->where('id',$member_id)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('id_fe');
        }
        return $data;
    }
    public function getMemberName($member_id){
        $data = array();
        $q=$this->db->select("nama",false)
            ->from('member')
            ->where('id',$member_id)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('nama');
        }
        return $data;
    }
    public function getUplineId($member_id){
        $data = array();
        $q=$this->db->select("sponsor_id",false)
            ->from('member m')
            ->where('id',$member_id)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('sponsor_id');
        }
        return $data;
    }
    public function getSponsorId($member_id){
        $data = array();
        $q=$this->db->select("enroller_id",false)
            ->from('member')
            ->where('id',$member_id)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('enroller_id');
        }
        return $data;
    }

    function update_customer_api($token, $data, $member_id_fe){

        header('Content-Type: application/json'); 
        $ch = curl_init('http://149.129.232.205/unidev/rest/default/V1/customers/'.$member_id_fe); 
    
        //$post = json_encode($post);
        
        //$authorization = "Authorization: Bearer fg1uuipy1ttro5r3wnrmyi6ywo5bq8cq"; 
        $authorization = "Authorization: Bearer " . $token; 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
        $result = curl_exec($ch); 

        curl_close($ch); 

        return $result;

    }

    
    // EOF API Ecommerce

    protected function _getAccountBank($id){
        $q=$this->db->get_where('account',array('id'=>$id));
        //echo $this->db->last_query();
        return $var = ($q->num_rows>0) ? $q->row() : false;
    }
    public function searchMember($option,$keywords=0,$num,$offset){
        $data = array();
        $this->db->select("m.id,m.nama, m.lengkap,m.noktp, k.name as kota,format(m.ewallet,0)as fewallet,format(m.ps,0)as fps,j.name as jenjang,b.reason
						, CASE WHEN IFNULL(b.reason,0)=0 THEN 0 ELSE 1 END AS note
						",false);
        $this->db->from('member m');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('jenjang j','m.jenjang_id=j.id','left');
        $this->db->join('users u','m.id=u.id','left');
        $this->db->join('banned b','u.banned_id=b.id','left');
        if($option ==='member_id'){
            $this->db->like('m.id', $keywords, 'between');
        }
        else{
            $this->db->like('m.nama', $keywords, 'between');
        }
        $this->db->order_by('u.banned_id','desc');
        $this->db->order_by('m.nama','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        // echo $this->db->last_query();
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countSearchMember($option=0,$keywords=0){
        $this->db->select("m.id",false);
        $this->db->from('member m');
        if($option === 'member_id')$this->db->like('m.id', $keywords, 'between');
        else $this->db->like('m.nama', $keywords, 'between');
        
        return $this->db->count_all_results();
    }
    public function getBannedMember($id){
        $q=$this->db->select("m.id,m.nama,u.banned_id,b.reason",false)
            ->from('member m')
            ->join('users u','m.id=u.id','left')
            ->join('banned b','u.banned_id=b.id','left')
            ->where('m.id',$id)->get();
        return $var = ($q->num_rows()>0)? $q->row() : false;
    }
    
    public function updateBanned(){
        if($this->input->post('banned') == '1'){
            $data = array(
                'reason' => $this->db->escape_str($this->input->post('remark'))
            );
            if($this->input->post('banned_id') >0){
                $this->db->update('banned',$data,array('id'=>$this->input->post('banned_id')));
                $this->session->set_flashdata('message','Updated banned member successfully');
            }else{
                $this->db->insert('banned',$data);
                $id = $this->db->insert_id();
                $this->db->update('users',array('banned_id'=>$id),array('id'=>$this->input->post('member_id')));
                $this->session->set_flashdata('message','Banned member successfully');
            }
        }else{
            if($this->input->post('banned') == '2' && $this->input->post('banned_id') >0){
                $this->db->delete('banned',array('id'=>$this->input->post('banned_id')));
                $this->db->update('users',array('banned_id'=>0),array('id'=>$this->input->post('member_id')));
                $this->session->set_flashdata('message','Active banned member successfully');
            }else{
                $this->session->set_flashdata('message','Cancel banned member successfully');
            }
            
        }
    }
	
	// Created by Boby 20100715
    public function cekBonusPeriode(){
		$this->db->select("month(max(periode)) as bln",false);
        $q = $this->db->get('bonus');
        if($q->num_rows() > 0){
			$row = $q->row_array();
        }
        $q->free_result();
        return $row["bln"];
	}
	// End created by Boby 20100715
	
	// Created by Boby 20110323
    public function getNama($id){
		$qry = "
			SELECT nama
			FROM member
			WHERE id = '$id'
		";
        $q = $this->db->query($qry);
        if($q->num_rows() > 0){
			$row = $q->row_array();
        }
        $q->free_result();
        return $row['nama'];
	}
	
	public function cekOdp($id){
		$q = $this->db->query("
			SELECT m.nama
			FROM z_201102_odp z 
			LEFT JOIN member m ON z.reff_id = m.id 
			WHERE z.member_id = '$id'
		");
		//echo $this->db->last_query();
        if($q->num_rows() > 0){
			$row = $q->row_array();
        }else{
			$row['nama']='None';
		}
        $q->free_result();
        return $row['nama'];
	}
	
	public function saveOdp($id){
		$data = array(
            'member_id' => $this->input->post('member_id'),
            'reff_id'=> $id,
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$this->session->userdata('username')
        );
        $this->db->insert('z_201102_odp',$data);
	}
	
	// End created by Boby 20110323
	
	// Created by Boby 20130909
	public function getNpwp($id){
        $data = array();
        $q = "
			SELECT nama, npwp, npwp_date as a, alamat
				, date_format(npwp_date,'%d-%m-%Y') as npwp_date
			FROM npwp
			WHERE member_id = '$id'
			ORDER BY id DESC
			LIMIT 0,1
			";
        $q = $this->db->query($q);
        // echo $this->db->last_query();
        if($q->num_rows() > 0){
			$row = $q->row_array();
        }
        $q->free_result();
        return $row;
    }
	// End created by Boby 20130909
	
	public function profile_lengkap(){
        if($this->input->post('p_id')){
            $row = array();
            
            $empid = $this->session->userdata('user');
            $idlist = implode(",",array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";
            
            $option = $where. " and lengkap = '0'";
            $row = $this->_countApproved($option);
            
            if($row){
                $data = array(
                    'lengkap'=> '1'
                );
                $this->db->update('member',$data,$option);
                
                $this->session->set_flashdata('message','update profile complete successfully');
            }else{
                $this->session->set_flashdata('message','Nothing to update profile!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing to update profile!');
        }
    }
    protected function _countApproved($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('member');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	
}
?>