<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ValueTopup_model extends CI_Model {


  function __construct()
  {
    parent::__construct();
  }

    
    function GetTopup_Value()
    {
        return $this->db->query("SELECT * from topupbyvalue join topupbyvalue_detail ON topupbyvalue.topupno = topupbyvalue_detail.parent_id order by topupbyvalue.id DESC")->result();
    }

    function SaveValue()
    {
        $getjml = $this->db->query("SELECT COUNT(*) AS jml FROM topupbyvalue")->row()->jml + 1;
		if ($getjml < 10) {
			$codec = '0000' + $getjml;
		} else if ($getjml >= 10 and $getjml < 100) {
			$codec = '000' + $getjml;
		} else if ($getjml >= 100 and $getjml < 1000) {
			$codec = '00' + $getjml;
		} else if ($getjml >= 1000 and $getjml < 10000) {
			$codec = '0' + $getjml;
		} else if ($getjml >= 10000 and $getjml < 100000) {
			$codec = $getjml;
		}

		$topupno = $codec;
		$datefrom = $this->input->post('datefrom');
		$dateto = $this->input->post('dateto');
		$cond_type = $this->input->post('cond_type');
		$cond_value = str_replace(',', '', $this->input->post('cond_value'));
		$multiple = $this->input->post('multiple');
		$validfor = $this->input->post('validfor');
		$loop = $this->input->post('loop');
		$status = $this->input->post('status');
		$validlist = implode(";", $validfor);
		$data = array(
			'valid_from' => $datefrom,
			'valid_to' => $dateto,
			'multiple' => $multiple,
			'condition_type' => $cond_type,
			'condition_value' => $cond_value,
			'topupno' => $topupno,
			'valid_for' => $validlist,
			'status' => $status,
			'last_update' => date('Y-m-d h:i:s'),
			'update_by' => $this->session->userdata('username'),
			'kelipatan' => $loop
		);

		$a = $this->db->insert('topupbyvalue', $data);
		$itemcode = $this->input->post('itemcode');
		$qty = $this->input->post('itemqty');
		$datas = array();
		for ($i = 0; $i < count($itemcode); $i++) {
			$datas[] = array(
				'parent_id' => $codec,
				'item_code' => $itemcode[$i],
				'qty' => $qty[$i]
			);
		}

		$a = $this->db->insert_batch('topupbyvalue_detail', $datas);
	
    }


    function result_edit($id)
    {
        return $this->db->query("SELECT * FROM topupbyvalue where topupno = '" . $id . "' ")->row();
    }



    function detail_edit($id)
    {
        return $this->db->query("SELECT * FROM topupbyvalue_detail where parent_id = '" . $id . "'")->result();
    }

    function update_detail()
    {
        $topupno = $this->input->post('id');
		$datefrom = $this->input->post('datefrom');
		$dateto = $this->input->post('dateto');
		$cond_type = $this->input->post('cond_type');
		$cond_value = str_replace(',', '', $this->input->post('cond_value'));
		$multiple = $this->input->post('multiple');
		$validfor = $this->input->post('validfor');
		$status = $this->input->post('status');
		$loop = $this->input->post('loop');
		$validlist = implode(";", $validfor);
		$data = array(
			'valid_from' => $datefrom,
			'valid_to' => $dateto,
			'multiple' => $multiple,
			'condition_type' => $cond_type,
			'condition_value' => $cond_value,
			'valid_for' => $validlist,
			'status' => $status,
			'last_update' => date('Y-m-d H:i:s'),
			'update_by' => $this->session->userdata('username'),
			'kelipatan'	=> $loop
		);

		$this->db->where('topupno', $topupno);
		$this->db->update('topupbyvalue', $data);

		$itemcode = $this->input->post('itemcode');
		$qty = $this->input->post('itemqty');
		$datas = array();
		for ($i = 0; $i < count($itemcode); $i++) {
			$datas[] = array(
				'parent_id' => $topupno,
				'item_code' => $itemcode[$i],
				'qty' => $qty[$i]
			);
		}
		$this->db->query("DELETE FROM topupbyvalue_detail where parent_id = '" . $topupno . "'");
		$a = $this->db->insert_batch('topupbyvalue_detail', $datas);
    }

    function delete_topup_value($id)
    {
        $this->db->query("DELETE FROM topupbyvalue where topupno = '" . $id . "'");
		$this->db->query("DELETE FROM topupbyvalue_detail where parent_id = '" . $id . "'");
		if ($this->db->affected_rows() > 0) {
			echo '<script language="javascript">alert("Sukses Hapus Data")</script>';
			redirect('smartindo/topupvalue', 'refresh');	
		}else {
			echo '<script language="javascript">alert("Gagal Hapus Data")</script>';
			redirect('', 'refresh');	
		}
    }


    function viewresult_datavalue($codevalue)
    {
        return $this->db->query("SELECT multiple,topupno FROM topupbyvalue where topupno IN (" . implode(',', $codevalue) . ") ")->row()->multiple;
    }

    function viewresult_parent($codevalue)
    {
        return $this->db->query("SELECT * FROM topupbyvalue_detail join topupbyvalue on topupbyvalue_detail.parent_id = topupbyvalue.topupno  WHERE parent_id IN (" . implode(',', $codevalue) . ") ORDER BY parent_id ASC")->result();
    }

    function topup_id($parent_id,$member_id)
    {
        return $this->db->query("SELECT topup_id FROM so_d join so ON so_d.so_id = so.id  where topup_id = '$parent_id' AND member_id ='$member_id' ")->result_array();
    }

    function multiple_topup($parent_id)
    {
        return $this->db->query("SELECT multiple FROM topupbyvalue where topupno = '$parent_id' ")->row_array();
    }

    function item_id($id)
    {
    return $this->db->query("SELECT * FROM item WHERE id = '" . $id . "'")->row();
    }

	function item_get()
	{
		return $this->db->query("SELECT * FROM item where topup = 'Yes' and sales = 'Yes' ")->result();
	}

}

/* End of file ModelName.php */
