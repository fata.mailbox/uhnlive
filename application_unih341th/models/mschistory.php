<?php
class MSchistory extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
	public function getSCHistory($tglawal, $tglakhir){
		$data = array();
		$qry = "
		SELECT 
			item.id
			, item.name as pname
			, IFNULL(dta.jml,0) AS jmla
			, IFNULL(dta.total,0) AS tota
			, IFNULL(dtb.jml,0) AS jmlb
			, IFNULL(dtb.total,0) AS totb
			, (IFNULL(dta.jml,0) - IFNULL(dtb.jml,0)) AS qty
			, (IFNULL(dta.total,0) - IFNULL(dtb.total,0)) AS hrg
		FROM item
		LEFT JOIN(
			SELECT pd.item_id, SUM(pd.qty)AS jml, SUM(pd.jmlharga)AS total
			FROM pinjaman_d pd
			LEFT JOIN pinjaman p ON pd.pinjaman_id = p.id
			WHERE p.tgl BETWEEN '$tglawal' AND '$tglakhir'
			GROUP BY pd.item_id
		) AS dta ON item.id = dta.item_id
		LEFT JOIN(
			SELECT rpd.item_id, SUM(rpd.qty)AS jml, SUM(rpd.jmlharga)AS total
			FROM retur_pinjaman_d rpd
			LEFT JOIN retur_pinjaman rp ON rpd.retur_pinjaman_id = rp.id
			WHERE rp.tgl BETWEEN '$tglawal' AND '$tglakhir'
			GROUP BY rpd.item_id
		) AS dtb ON item.id = dtb.item_id
		";
		$rs = $this->db->query($qry);
		//echo $this->db->last_query();
		if($rs->num_rows()>0){
			foreach($rs->result_array() as $row){
				$data[]=$row;
			}
		}
		$rs->free_result();
		return $data;
		
	}
	public function getSCHistory2($tglawal, $tglakhir){
		$data = array();
		$qry = "
		SELECT 
			item.id
			, item.name as pname
			, IFNULL(dta.jml,0) AS jmla
			, IFNULL(dta.total,0) AS tota
			, IFNULL(dtb.jml,0) AS jmlb
			, IFNULL(dtb.total,0) AS totb
			, (IFNULL(dta.jml,0) - IFNULL(dtb.jml,0)) AS qty
			, (IFNULL(dta.total,0) - IFNULL(dtb.total,0)) AS hrg
		FROM item
		LEFT JOIN(
			SELECT pd.item_id, SUM(pd.qty)AS jml, SUM(pd.harga*pd.qty)AS total
			FROM pinjaman_d pd
			LEFT JOIN pinjaman p ON pd.pinjaman_id = p.id
			WHERE p.tgl BETWEEN '$tglawal' AND '$tglakhir'
			GROUP BY pd.item_id
		) AS dta ON item.id = dta.item_id
		LEFT JOIN(
			SELECT rpd.item_id, SUM(rpd.qty)AS jml, SUM(rpd.harga*rpd.qty)AS total
			FROM retur_pinjaman_d rpd
			LEFT JOIN retur_pinjaman rp ON rpd.retur_pinjaman_id = rp.id
			WHERE rp.tgl BETWEEN '$tglawal' AND '$tglakhir'
			GROUP BY rpd.item_id
		) AS dtb ON item.id = dtb.item_id
		";
		$rs = $this->db->query($qry);
		//echo $this->db->last_query();
		if($rs->num_rows()>0){
			foreach($rs->result_array() as $row){
				$data[]=$row;
			}
		}
		$rs->free_result();
		return $data;
		
	}
	public function getSCHistoryPerProducts($tglawal, $tglakhir){
		$data = array();
		$qry = "
		SELECT 
			IFNULL(ig.id,99) AS group_id
			, CASE WHEN ig.id IS NOT NULL THEN ig.name ELSE 'Package' END AS group_name
			, item.id
			, item.name AS pname
			, IFNULL(dta.jml,0) AS jmla
			, IFNULL(dta.total,0) AS tota
			, IFNULL(dtb.jml,0) AS jmlb
			, IFNULL(dtb.total,0) AS totb
			, (IFNULL(dta.jml,0) - IFNULL(dtb.jml,0)) AS qty
			, (IFNULL(dta.total,0) - IFNULL(dtb.total,0)) AS hrg
		FROM item
		LEFT JOIN(
			SELECT itemo_id AS item_id, SUM(qty)AS jml, SUM(total) AS total
			FROM (
					SELECT -- pd.item_id, SUM(pd.qty)AS jml, SUM(pd.jmlharga)AS total
						CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE pd.item_id END AS itemo_id
						,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE pd.harga END AS hargao
						,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*pd.qty ELSE pd.qty END AS qty
						,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*pd.qty ELSE pd.jmlharga END AS total
					FROM pinjaman_d pd
					LEFT JOIN pinjaman p ON pd.pinjaman_id = p.id
					LEFT JOIN reff_package rp ON pd.item_id = rp.package_id AND pd.harga = rp.package_price
					WHERE p.tgl BETWEEN '$tglawal' AND '$tglakhir'
			) AS dt	
			GROUP BY itemo_id
			-- GROUP BY pd.item_id
		) AS dta ON item.id = dta.item_id
		LEFT JOIN(
			SELECT itemo_id AS item_id, SUM(qty)AS jml, SUM(total) AS total
			FROM (
				SELECT -- rpd.item_id, SUM(rpd.qty)AS jml, SUM(rpd.jmlharga)AS total
						CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rpd.item_id END AS itemo_id
						,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rpd.harga END AS hargao
						,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rpd.qty ELSE rpd.qty END AS qty
						,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rpd.qty ELSE rpd.jmlharga END AS total
				FROM retur_pinjaman_d rpd
				LEFT JOIN retur_pinjaman rpi ON rpd.retur_pinjaman_id = rpi.id
				LEFT JOIN reff_package rp ON rpd.item_id = rp.package_id AND rpd.harga = rp.package_price
				WHERE rpi.tgl BETWEEN '$tglawal' AND '$tglakhir'
			) AS dt	
			GROUP BY itemo_id
			-- GROUP BY rpd.item_id
		) AS dtb ON item.id = dtb.item_id
		LEFT JOIN item_to_item_group iig ON item.id = iig.id
		LEFT JOIN item_group ig ON iig.group_id = ig.id
		HAVING tota <> 0 OR totb <> 0
		ORDER BY group_id, item.id,item.name
		";
		$rs = $this->db->query($qry);
		//echo $this->db->last_query();
		if($rs->num_rows()>0){
			foreach($rs->result_array() as $row){
				$data[]=$row;
			}
		}
		$rs->free_result();
		return $data;
		
	}
}
?>