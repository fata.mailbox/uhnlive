<?php
class Catalogue_model extends CI_Model
{
	    function __construct()
	    {
	        parent::__construct();
	    }

	    public function getDataList($limit = '', $offset = '', $id = '', $title = '', $start_period = '', $end_period='', $pdf_file = '', $thumbnail='', $sort = '', $category_id = '', $feature_catalogue = '', $publish = '',$keyword = '', $sorting='')
		{
	
			$wQuery = 'WHERE';
			

			if($id != ''){
				$wQuery.= " catalogue_id=".$id;
			}

			if($title != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " title='".$title."'";
				}else{
					$wQuery.= " AND title='".$title."'";
				}
			}

			if($start_period != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " start_period='".$start_period."'";
				}else{
					$wQuery.= " AND start_period='".$start_period."'";
				}
			}

			if($end_period != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " end_period='".$end_period."'";
				}else{
					$wQuery.= " AND end_period='".$end_period."'";
				}
			}

			if($pdf_file != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " pdf_file='".$pdf_file."'";
				}else{
					$wQuery.= " AND pdf_file='".$pdf_file."'";
				}
			}

			if($thumbnail != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " thumbnail='".$thumbnail."'";
				}else{
					$wQuery.= " AND thumbnail='".$thumbnail."'";
				}
			}

			if($sort != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " sort_catalogue='".$sort."'";
				}else{
					$wQuery.= " AND sort_catalogue='".$sort."'";
				}
			}

			if($category_id != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " category_id='".$category_id."'";
				}else{
					$wQuery.= " AND category_id='".$category_id."'";
				}
			}

			if($feature_catalogue != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " feature_catalogue='".$feature_catalogue."'";
				}else{
					$wQuery.= " AND feature_catalogue='".$feature_catalogue."'";
				}
			}

			if($publish != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " publish='".$publish."'";
				}else{
					$wQuery.= " AND publish='".$publish."'";
				}
			}

			if($sorting != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " ORDER BY catalogue_id ".$sorting;
				}else{
					$wQuery.= " ORDER BY catalogue_id ".$sorting;
				}
			}

			if($keyword != '')
			{
				if($wQuery=='WHERE'){
					$wQuery.= " title LIKE '%".$keyword."%'";
				}else{
					$wQuery.= " AND title LIKE '%".$keyword."%'";
				}
			}

			if($limit!=''&&$offset!='')$lQuery = ' LIMIT '.$limit.','.$offset; else $lQuery = '';

			if($wQuery=='WHERE')$wQuery = '';

			$rs = $this->db->query("SELECT * 
	    				    FROM master_catalogue a LEFT JOIN catalogue_category b ON a.category_id = b.catalogue_category_id
	                        ".$wQuery." ORDER BY sort_catalogue ASC" .$lQuery);
	        
	        $result = array();

	        if ($rs->num_rows() > 0) 
	        {
	            foreach($rs->result_array() as $row ) {
	                $result['data'][] = $row;
	            }

	            foreach($rs->result_object() as $row2 ) {
	                $result['dataObject'] = $row2;
	            }
	        }
	        else
	        {
	        	return FALSE;
	        }

	        $result['countResult']=$rs->num_rows();
	        $rs->free_result();
			return $result;
		}

		public function getDetail($cat_id = '')
		{
			$query = "SELECT * FROM detail_catalogue WHERE cat_id = '$cat_id' ORDER BY page_number";
			$rs = $this->db->query($query);

			$result = array();

	        if ($rs->num_rows() > 0) 
	        {
	            foreach($rs->result_array() as $row ) {
	                $result['data'][] = $row;
	            }

	            foreach($rs->result_object() as $row2 ) {
	                $result['dataObject'] = $row2;
	            }
	        }
	        else
	        {
	        	return FALSE;
	        }

	        $result['countResult']=$rs->num_rows();
	        $rs->free_result();
			return $result;

		}

		public function create_main($title="",$start_period="",$end_period="",$pdf_file="",$thumbnail="",$sort="",$category_id="",$feature_catalogue="",$publish="")
		{
			$query1 = "SELECT * FROM master_catalogue WHERE title = '$title'";
			$query2 = "INSERT INTO master_catalogue(title,start_period,end_period,pdf_file,thumbnail,sort_catalogue,category_id,feature_catalogue,publish) VALUES('$title','$start_period','$end_period','$pdf_file','$thumbnail','$sort','$category_id','$feature_catalogue','$publish')";

			$rs1 = $this->db->query($query1);

			if($rs1->num_rows() > 0) {
				return false;
			} else {
				$rs2 = $this->db->query($query2);

				if($rs2) {
					return $this->db->insert_id();
				} else {
					return false;
				}
			}
		}

		public function create_page($catalogue_id="",$page_number="",$image="")
		{
			$query = "INSERT INTO detail_catalogue(cat_id,page_number,image) VALUES('$catalogue_id','$page_number','$image')";
			$rs = $this->db->query($query);

			if($rs)
			{
				return true;
			} else {
				return false;
			}
		}

		public function edit_main($id="",$title="",$start_period="",$end_period="",$pdf_file="",$thumbnail="",$sort="",$category_id="")
		{
			$query1 = "SELECT * FROM master_catalogue WHERE title = '$title' AND catalogue_id != '$id'";
			$query2 = "UPDATE master_catalogue SET title='$title', start_period='$start_period', end_period='$end_period',pdf_file='$pdf_file', thumbnail='$thumbnail', sort_catalogue='$sort', category_id='$category_id' WHERE catalogue_id = '$id'";
			$rs1 = $this->db->query($query1);

			if($rs1->num_rows() > 0) {
				return false;
			} else {
				$rs2 = $this->db->query($query2);

				if($rs2) {
					return true;
				} else {
					return false;
				}
			}
		}

		public function clear_field($catalogue_id)
		{
			$query = "DELETE FROM detail_catalogue WHERE cat_id = '$catalogue_id'";
			$rs = $this->db->query($query);

			if ($rs) {
				return true;
			}
		}

		public function delete($id = '')
		{
			$query1 = "DELETE FROM master_catalogue WHERE catalogue_id = '$id'";
			$query2 = "DELETE FROM detail_catalogue WHERE cat_id = '$id' ";
			$rs1 = $this->db->query($query1);
			$rs2 = $this->db->query($query2);

			if ($rs1) {
				if ($rs2) {
					return true;
				} else {
					return false;
				}
				return false;
			}


		}

		public function select_page($catalogue_id = '')
		{
			$query = "SELECT * FROM detail_catalogue WHERE cat_id = '$catalogue_id' ORDER BY page_number ASC";
			$rs = $this->db->query($query);

			if($rs) {
				return $rs->result_array();
			} else {
				return false;
			}
		}

		public function getDataDetail($cat_id='',$page_number='')
		{
			$query = "SELECT * FROM detail_catalogue WHERE cat_id = $cat_id AND page_number = $page_number";
			$rs = $this->db->query($query);

			if($rs) {
				return $rs->result_object();
			} else {
				return false;
			}
		}
}