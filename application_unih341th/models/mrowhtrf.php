<?php
class MRowhtrf extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
	
    public function searchRowhtrf($keywords=0,$num,$offset){
        $data = array();
		$whsid = $this->session->userdata('whsid');
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' ) and a.warehouse_id = ".$this->session->userdata('keywords_whsid');
			}
		}
		
        $this->db->select("a.id, a.ro_id, b.name,date_format(a.createddate,'%d-%b-%Y')as date,a.remark,a.createdby,b.name as warehouse1_name,c.name as warehouse2_name",false);
        $this->db->from('ro_whtrf a');
        $this->db->join('warehouse b','a.whs1=b.id','left');
        $this->db->join('warehouse c','a.whs2=c.id','left');
        //$this->db->like('a.id', $keywords, 'after');
        //$this->db->or_like('b.name', $keywords, 'after');
		$this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countRowhtrf($keywords=0){
		$whsid = $this->session->userdata('whsid');
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%')";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' )";
					else
						$where = "( a.id LIKE '$keywords%' or a.remark LIKE '$keywords%' OR b.name LIKE '$keywords%' ) and a.warehouse_id = ".$this->session->userdata('keywords_whsid');
			}
		}		
        $this->db->select("a.id, a.ro_id, b.name,date_format(a.createddate,'%d-%b-%Y')as date,a.remark,a.createdby,b.name as warehouse1_name,c.name as warehouse2_name",false);
        $this->db->from('ro_whtrf a');
        $this->db->join('warehouse b','a.whs1=b.id','left');
        $this->db->join('warehouse c','a.whs2=c.id','left');
        //$this->db->like('a.id', $keywords, 'after');
        //$this->db->or_like('b.name', $keywords, 'after'); 
		$this->db->where($where);
        return $this->db->count_all_results();
    }
	
    public function getDropDownWhsAll($all){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            if($all == 'all')$data['all']='All Cabang';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
	
    public function cekDataRO($ro_id){
        $this->db->select("a.id, a.warehouse_id, b.name as warehouse_name",false);
        $this->db->from('ro a');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
		$this->db->where('a.id = '.$ro_id);
        return $this->db->count_all_results();
    }
    public function getDataRO($ro_id){
        $data = array();
        $this->db->select("a.id, a.warehouse_id, b.name as warehouse_name",false);
        $this->db->from('ro a');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
		$this->db->where('a.id = '.$ro_id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getDataRODetail($ro_id,$whs2){
        $data = array();
        $this->db->select("rod.id, rod.item_id, i.name as item_name, rod.qty, s.qty as qty_stock_wh",false);
        $this->db->from('ro_d rod');
        $this->db->join('item i', 'rod.item_id = i.id','left');
        $this->db->join('ro', 'rod.ro_id = ro.id','left');
        $this->db->join('stock s', 's.item_id = rod.item_id AND s.warehouse_id = '.$whs2,'left');
		$this->db->where('rod.ro_id = '.$ro_id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function getFree($id,$whs2){
        $data = array();
        $q = "
			SELECT rf.trx_id, rf.nc_id, ncd.item_id, i.name AS prd, ncd.qty, s.qty as qty_stock_wh
			FROM ref_trx_nc rf
			LEFT JOIN ncm nc ON rf.nc_id = nc.id
			LEFT JOIN ncm_d ncd ON nc.id = ncd.ncm_id
			LEFT JOIN item i ON ncd.item_id = i.id
			LEFT JOIN stock s on s.item_id = ncd.item_id AND s.warehouse_id = ".$whs2."
			WHERE rf.trx = 'RO'
			AND rf.trx_id = $id
			";
        $q = $this->db->query($q);
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function addRowhtrf(){
		// insert table ro_whtrf
		   $data = array(
				'ro_id' => $this->input->post('ro_id'),
				'whs1' =>  $this->input->post('whs1'),
				'whs2' =>  $this->input->post('whs2'),
				'remark' => $this->db->escape_str($this->input->post('remark')),
				'createddate' => date('Y-m-d H:m:s',now()),
				'createdby' => $this->session->userdata('user')
				);
		   $this->db->insert('ro_whtrf',$data);
		   
		   $id = $this->db->insert_id();
		   /*
		echo "call sp_ro_whtrf(". $id.",".$this->input->post('ro_id').",".$this->input->post('whs1').",".$this->input->post('whs1').",'".$this->session->userdata('user')."') <br>";
		echo "call sp_ro_whtrf_tail(". $id.",".$this->input->post('ro_id').",".$this->input->post('whs1').",".$this->input->post('whs1').",'".$this->session->userdata('user')."')";
		*/
		// trf item utama
       	$this->db->query("call sp_ro_whtrf(". $id.",".$this->input->post('ro_id').",".$this->input->post('whs1').",".$this->input->post('whs2').",'".$this->session->userdata('user')."')");
		// trf support item
		if($this->input->post('bns') == '1'){
			$this->db->query("call sp_ro_whtrf_tail(". $id.",".$this->input->post('ro_id').",".$this->input->post('whs1').",".$this->input->post('whs2').",'".$this->session->userdata('user')."')");
		}
	}
	
	//START ASP 20190110
	public function cekWhRO($id)
    {
        $data=array();
		$warehouse_pengiriman = 0;
		$query = "
		SELECT * FROM warehouse w 
		WHERE w.id IN (
			SELECT rod.warehouse_id FROM ro_d rod
			WHERE rod.ro_id = ".$id."
		)
		";
		
		$qry = $this->db->query($query);
	    
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
				$warehouse_pengiriman++;
			}
        }
		$qry->free_result();
		//return $data;
		$warehouse_pengiriman = rtrim($warehouse_pengiriman," - ");
		return $warehouse_pengiriman;
    }
	//END ASP 20190110
}?>