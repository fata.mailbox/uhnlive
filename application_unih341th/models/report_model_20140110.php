<?php
class Report_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function report_convert_to_excel($type_id){
        $data = array();
        $date1 = date('Y-m-d',now());
            
        if($type_id == 'royalty_magozai'){
            $query = "SELECT so.tgl, sod.so_id, sod.item_id, i.name AS product, i.manufaktur, IFNULL(m.item_id,'-')AS item, sod.harga
                    , CASE WHEN m.qty IS NULL THEN sod.qty ELSE sod.qty * m.qty END AS qty
                    FROM so_d sod
                    LEFT JOIN so ON sod.so_id = so.id
                    LEFT JOIN item i ON sod.item_id = i.id
                    LEFT JOIN manufaktur m ON i.id = m.manufaktur_id
                    WHERE 
                    so.tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                    AND (sod.item_id IN ('NT020003')OR m.item_id = 'NT020003')
                    ORDER BY tgl DESC";
        }elseif($type_id == 'so_ke_uhn'){
            $query = "SELECT s.id,s.tgl,member_id,sod.item_id,i.name,qty, sod.jmlharga
                    FROM so_d sod
                    LEFT JOIN so s ON sod.so_id=s.id 
                    LEFT JOIN item i ON sod.item_id=i.id
                    LEFT JOIN member m ON s.member_id=m.id
                    WHERE s.tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH) AND s.stockiest_id=0";
        }elseif($type_id == 'ro_stc'){
            $query = "SELECT s.id,s.date,s.member_id,sod.item_id,i.name,qty,(sod.harga_*qty)AS totalsales,(sod.harga_*qty)-sod.jmlharga AS diskon,sod.jmlharga AS netsales
                    FROM ro_d sod
                    LEFT JOIN ro s ON sod.ro_id=s.id 
                    LEFT JOIN item i ON sod.item_id=i.id
                    LEFT JOIN stockiest stc ON s.member_id=stc.id
                    WHERE s.date BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH) AND s.stockiest_id=0 AND stc.type='1'
                    ";
        }elseif($type_id == 'ro_mstc'){
            $query = "SELECT s.id,s.date,s.member_id,sod.item_id,i.name,qty,(sod.harga_*qty)AS totalsales,(sod.harga_*qty)-sod.jmlharga AS diskon,sod.jmlharga AS netsales
                FROM ro_d sod
                LEFT JOIN ro s ON sod.ro_id=s.id 
                LEFT JOIN item i ON sod.item_id=i.id
                LEFT JOIN stockiest stc ON s.member_id=stc.id
                WHERE s.date BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH) AND s.stockiest_id=0 AND stc.type='2'
            ";
        }elseif($type_id == 'scp'){
            $query = "SELECT s.id,s.stockiest_id,m.nama,s.tgl,sod.item_id,i.name,qty,(sod.harga*qty)AS total
                FROM pinjaman_titipan_d sod
                LEFT JOIN pinjaman_titipan s ON sod.pinjaman_titipan_id=s.id 
                LEFT JOIN item i ON sod.item_id=i.id
                LEFT JOIN member m ON s.stockiest_id=m.id
                LEFT JOIN stockiest stc ON s.stockiest_id=stc.id
                WHERE s.tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH) 
                ";
        }elseif($type_id == 'retur'){
            $query = "SELECT s.id,s.stockiest_id,m.nama,s.tgl,sod.item_id,i.name,qty,(sod.harga*qty)AS total
                FROM retur_titipan_d sod
                LEFT JOIN retur_titipan s ON sod.retur_titipan_id=s.id
                LEFT JOIN item i ON sod.item_id=i.id
                LEFT JOIN member m ON s.stockiest_id=m.id
                LEFT JOIN stockiest stc ON s.stockiest_id=stc.id
                WHERE s.tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)";
        }elseif($type_id == 'member_transaction_history'){
            $petik = "\'";
			//20140828 ASP Start
			$bln_query = '';
			$curr_month = (integer)date('m');
			$year = date('Y') -1;
			for($i=12;$i>0;$i--){
				if($curr_month==13){
					$curr_month=1;
					$year++;
				}
				$bln_query.=", IFNULL(dt3.bln_".$curr_month.",0)AS 'bln_".$i." (".date('M',mktime(0,0,0,$curr_month,1,$year))." ".$year.")'";
				$curr_month++;
			}
			//echo $bln_query;
			//20140828 ASP End
            $query = "SELECT concat('".$petik."',dt.member_id)as member_id, dt.nama, m.alamat, IFNULL(k.name,'-')AS kota, concat('".$petik."',m.telp)as telp
                    , concat('".$petik."',m.hp)as hp, dt.joindate, me.id, me.nama AS nama_sponsor, j.decription
					".$bln_query."
                    -- , IFNULL(dt3.bln_11,0)AS bln_12
                    -- , IFNULL(dt3.bln_12,0)AS bln_11
                    -- , IFNULL(dt3.bln_1,0)AS bln_10
                    -- , IFNULL(dt3.bln_2,0)AS bln_9
                    -- , IFNULL(dt3.bln_3,0)AS bln_8
                    -- , IFNULL(dt3.bln_4,0)AS bln_7
                    -- , IFNULL(dt3.bln_5,0)AS bln_6
                    -- , IFNULL(dt3.bln_6,0)AS bln_5
                    -- , IFNULL(dt3.bln_7,0)AS bln_4
                    -- , IFNULL(dt3.bln_8,0)AS bln_3
                    -- , IFNULL(dt3.bln_9,0)AS bln_2
                    -- , IFNULL(dt3.bln_10,0)AS bln_1
                    , IFNULL(dt3.bln_1,0) + IFNULL(dt3.bln_2,0) + IFNULL(dt3.bln_3,0) + IFNULL(dt3.bln_4,0) + IFNULL(dt3.bln_5,0) + IFNULL(dt3.bln_6,0)
                    + IFNULL(dt3.bln_7,0) + IFNULL(dt3.bln_8,0) + IFNULL(dt3.bln_9,0) + IFNULL(dt3.bln_10,0) + IFNULL(dt3.bln_11,0) + IFNULL(dt3.bln_12,0)
                            AS total
            FROM( 
                    SELECT member_id,nama,joindate
                    FROM v_memberjoin
            )AS dt
            LEFT JOIN (
                    SELECT member_id
                            , IFNULL(SUM(bln1),0)AS bln_1	, IFNULL(SUM(bln2),0)AS bln_2	, IFNULL(SUM(bln3),0)AS bln_3
                            , IFNULL(SUM(bln4),0)AS bln_4	, IFNULL(SUM(bln5),0)AS bln_5	, IFNULL(SUM(bln6),0)AS bln_6
                            , IFNULL(SUM(bln7),0)AS bln_7	, IFNULL(SUM(bln8),0)AS bln_8	, IFNULL(SUM(bln9),0)AS bln_9
                            , IFNULL(SUM(bln10),0)AS bln_10	, IFNULL(SUM(bln11),0)AS bln_11	, IFNULL(SUM(bln12),0)AS bln_12		
                    FROM (
                            SELECT member_id
                                    ,CASE WHEN bln=1 THEN totalharga END AS bln1	,CASE WHEN bln=2 THEN totalharga END AS bln2	,CASE WHEN bln=3 THEN totalharga END AS bln3
                                    ,CASE WHEN bln=4 THEN totalharga END AS bln4	,CASE WHEN bln=5 THEN totalharga END AS bln5	,CASE WHEN bln=6 THEN totalharga END AS bln6
                                    ,CASE WHEN bln=7 THEN totalharga END AS bln7	,CASE WHEN bln=8 THEN totalharga END AS bln8	,CASE WHEN bln=9 THEN totalharga END AS bln9
                                    ,CASE WHEN bln=10 THEN totalharga END AS bln10	,CASE WHEN bln=11 THEN totalharga END AS bln11	,CASE WHEN bln=12 THEN totalharga END AS bln12
                            FROM(
                                    SELECT s.member_id,IFNULL(SUM(sod.jmlharga_),0)AS totalharga,MONTH(s.tgl)AS bln	
                                    FROM so_d sod
                                    LEFT JOIN so s ON sod.so_id=s.id
                                    WHERE s.tgl BETWEEN (LAST_DAY((NOW() - INTERVAL 1 YEAR)- INTERVAL 1 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                                    AND s.totalpv<>0
                                    GROUP BY s.member_id,MONTH(s.tgl)
                            )AS dt1
                    )AS dt2 
                    GROUP BY member_id
            )AS dt3 ON dt.member_id=dt3.member_id
            LEFT JOIN member m ON dt.member_id=m.id
            LEFT JOIN kota k ON m.kota_id=k.id
            LEFT JOIN member me ON m.sponsor_id=me.id
            LEFT JOIN jenjang j ON me.jenjang_id=j.id
            WHERE IFNULL(dt3.bln_2,0)+IFNULL(dt3.bln_3,0)+IFNULL(dt3.bln_4,0)
                    +IFNULL(dt3.bln_5,0)+IFNULL(dt3.bln_6,0)+IFNULL(dt3.bln_7,0)
                    +IFNULL(dt3.bln_8,0)+IFNULL(dt3.bln_9,0)+IFNULL(dt3.bln_10,0)
                    +IFNULL(dt3.bln_11,0)+IFNULL(dt3.bln_12,0)+IFNULL(dt3.bln_1,0)<>0
            ORDER BY total DESC";
			//echo $query;
        }elseif($type_id == 'member_active_old_1_tahun'){
            $query = "SELECT dt.member_id, dt.nama, m.alamat, IFNULL(k.name,'-')AS kota
                    , m.telp, m.hp, dt.joindate, me.id, me.nama, j.decription
                    
                    , IFNULL(dt3.bln_11,0)AS bln_11_2012
                    , IFNULL(dt3.bln_12,0)AS bln_12_2012
                    , IFNULL(dt3.bln_1,0)AS bln_1_2013
                    , IFNULL(dt3.bln_2,0)AS bln_2_2013
                    , IFNULL(dt3.bln_3,0)AS bln_3_2013
                    , IFNULL(dt3.bln_4,0)AS bln_4_2013
                    , IFNULL(dt3.bln_5,0)AS bln_5_2013
                    , IFNULL(dt3.bln_6,0)AS bln_6_2013
                    , IFNULL(dt3.bln_7,0)AS bln_7_2013
                    , IFNULL(dt3.bln_8,0)AS bln_8_2013
                    , IFNULL(dt3.bln_9,0)AS bln_9_2013
                    , IFNULL(dt3.bln_10,0)AS bln_10_2013
                    , IFNULL(dt3.bln_1,0)	+ IFNULL(dt3.bln_2,0)	+ IFNULL(dt3.bln_3,0)	+ IFNULL(dt3.bln_4,0)	+ IFNULL(dt3.bln_5,0)	+ IFNULL(dt3.bln_6,0)
                    + IFNULL(dt3.bln_7,0)	+ IFNULL(dt3.bln_8,0)	+ IFNULL(dt3.bln_9,0)	+ IFNULL(dt3.bln_10,0)	+ IFNULL(dt3.bln_11,0)	+ IFNULL(dt3.bln_12,0)
                    AS total
            FROM( 
                    SELECT member_id,nama,joindate
                    FROM v_memberjoin
                    WHERE joindate BETWEEN '2009-01-01' AND (LAST_DAY((NOW() - INTERVAL 1 YEAR)- INTERVAL 1 MONTH))
            )AS dt
            LEFT JOIN (
                    SELECT member_id
                            ,IFNULL(SUM(bln1),0)AS bln_1	,IFNULL(SUM(bln2),0)AS bln_2	,IFNULL(SUM(bln3),0)AS bln_3
                            ,IFNULL(SUM(bln4),0)AS bln_4	,IFNULL(SUM(bln5),0)AS bln_5	,IFNULL(SUM(bln6),0)AS bln_6
                            ,IFNULL(SUM(bln7),0)AS bln_7	,IFNULL(SUM(bln8),0)AS bln_8	,IFNULL(SUM(bln9),0)AS bln_9
                            ,IFNULL(SUM(bln10),0)AS bln_10	,IFNULL(SUM(bln11),0)AS bln_11	,IFNULL(SUM(bln12),0)AS bln_12
                    FROM (
                            SELECT member_id
                                    ,CASE WHEN bln=1 THEN totalharga END AS bln1
                                    ,CASE WHEN bln=2 THEN totalharga END AS bln2
                                    ,CASE WHEN bln=3 THEN totalharga END AS bln3
                                    ,CASE WHEN bln=4 THEN totalharga END AS bln4
                                    ,CASE WHEN bln=5 THEN totalharga END AS bln5
                                    ,CASE WHEN bln=6 THEN totalharga END AS bln6
                                    ,CASE WHEN bln=7 THEN totalharga END AS bln7
                                    ,CASE WHEN bln=8 THEN totalharga END AS bln8
                                    ,CASE WHEN bln=9 THEN totalharga END AS bln9
                                    ,CASE WHEN bln=10 THEN totalharga END AS bln10
                                    ,CASE WHEN bln=11 THEN totalharga END AS bln11
                                    ,CASE WHEN bln=12 THEN totalharga END AS bln12
                            FROM(
                                    SELECT s.member_id,IFNULL(SUM(sod.jmlharga_),0)AS totalharga,MONTH(s.tgl)AS bln	
                                    FROM so_d sod
                                    LEFT JOIN so s ON sod.so_id=s.id
                                    WHERE s.tgl BETWEEN -- '2012-05-01' AND '2013-04-31' 
                                    (LAST_DAY((NOW() - INTERVAL 1 YEAR)- INTERVAL 1 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                                    AND s.totalpv<>0
                                    GROUP BY s.member_id,MONTH(s.tgl)
                            )AS dt1
                    )AS dt2 
                    GROUP BY member_id
            )AS dt3 ON dt.member_id=dt3.member_id
            LEFT JOIN member m ON dt.member_id=m.id
            LEFT JOIN kota k ON m.kota_id=k.id
            LEFT JOIN member me ON m.sponsor_id=me.id
            LEFT JOIN jenjang j ON me.jenjang_id=j.id
            WHERE IFNULL(dt3.bln_1,0)+IFNULL(dt3.bln_2,0)+IFNULL(dt3.bln_3,0)+IFNULL(dt3.bln_4,0)+IFNULL(dt3.bln_5,0)+IFNULL(dt3.bln_6,0)
                    +IFNULL(dt3.bln_7,0)+IFNULL(dt3.bln_8,0)+IFNULL(dt3.bln_9,0)+IFNULL(dt3.bln_10,0)+IFNULL(dt3.bln_11,0)+IFNULL(dt3.bln_12,0)<>0
            ORDER BY total DESC 
        ";
		
        }elseif($type_id == 'recruiting_history'){
            $petik = "\'";
			//20140828 ASP Start
			$bln_query = '';
			$curr_month = (integer)date('m');
			$year = date('Y') -1;
			for($i=12;$i>0;$i--){
				if($curr_month==13){
					$curr_month=1;
					$year++;
				}
				$bln_query.=", IFNULL(SUM(dt.bln".$curr_month."),'0')AS 'bln_".$i." (".date('M',mktime(0,0,0,$curr_month,1,$year))." ".$year.")'";//bln_".$i;
				$curr_month++;
			}
			//echo $bln_query;
			//20140828 ASP End
            $query = "SELECT concat('".$petik."',dt.id_sponsor)as id_sponsor
                    ,dt.nama_sponsor
					".$bln_query."
                    -- ,IFNULL(SUM(dt.bln2),'0')AS bln_12
                    -- ,IFNULL(SUM(dt.bln3),'0')AS bln_11
                    -- ,IFNULL(SUM(dt.bln4),'0')AS bln_10
                    -- ,IFNULL(SUM(dt.bln5),'0')AS bln_9
                    -- ,IFNULL(SUM(dt.bln6),'0')AS bln_8
                    -- ,IFNULL(SUM(dt.bln7),'0')AS bln_7
                    -- ,IFNULL(SUM(dt.bln8),'0')AS bln_6
                    -- ,IFNULL(SUM(dt.bln9),'0')AS bln_5
                    -- ,IFNULL(SUM(dt.bln10),'0')AS bln_4
                    -- ,IFNULL(SUM(dt.bln11),'0')AS bln_3
                    -- ,IFNULL(SUM(dt.bln12),'0')AS bln_2
                    -- ,IFNULL(SUM(dt.bln1),'0')AS bln_1
                    ,IFNULL(SUM(dt.bln1),'0')+IFNULL(SUM(dt.bln2),'0')+IFNULL(SUM(dt.bln3),'0')+IFNULL(SUM(dt.bln4),'0')+IFNULL(SUM(dt.bln5),'0')+IFNULL(SUM(dt.bln6),'0')
                            +IFNULL(SUM(dt.bln7),'0')+IFNULL(SUM(dt.bln8),'0')+IFNULL(SUM(dt.bln9),'0')+IFNULL(SUM(dt.bln10),'0')+IFNULL(SUM(dt.bln11),'0')+IFNULL(SUM(dt.bln12),'0')
                            AS total
            FROM(
                    SELECT dt.id_sponsor
                            ,dt.nama_sponsor
                            ,CASE WHEN dt.bln=1 THEN jml END AS bln1
                            ,CASE WHEN dt.bln=2 THEN jml END AS bln2
                            ,CASE WHEN dt.bln=3 THEN jml END AS bln3
                            ,CASE WHEN dt.bln=4 THEN jml END AS bln4
                            ,CASE WHEN dt.bln=5 THEN jml END AS bln5
                            ,CASE WHEN dt.bln=6 THEN jml END AS bln6
                            ,CASE WHEN dt.bln=7 THEN jml END AS bln7
                            ,CASE WHEN dt.bln=8 THEN jml END AS bln8
                            ,CASE WHEN dt.bln=9 THEN jml END AS bln9
                            ,CASE WHEN dt.bln=10 THEN jml END AS bln10
                            ,CASE WHEN dt.bln=11 THEN jml END AS bln11
                            ,CASE WHEN dt.bln=12 THEN jml END AS bln12
                    FROM (
                            SELECT dt.id_sponsor,dt.nama_sponsor,COUNT(dt.id)AS jml, MONTH(dt.joindate)AS bln
                            FROM(
                                    SELECT mr.id AS id_sponsor,mr.nama AS nama_sponsor,m.id,m.nama,vm.joindate
                                    FROM v_memberjoin vm 
                                    LEFT JOIN member m ON vm.member_id=m.id
                                    LEFT JOIN member mr ON m.enroller_id=mr.id
                                    WHERE vm.joindate BETWEEN--  '2012-05-01' AND '2013-04-31' 
                                    (LAST_DAY((NOW() - INTERVAL 1 YEAR)- INTERVAL 1 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                                    AND mr.id NOT IN ('0',00000001,10000001)
                                    ORDER BY vm.joindate
                            )AS dt
                            GROUP BY dt.id_sponsor,bln
                    )AS dt
            )AS dt
            GROUP BY dt.id_sponsor
            ORDER BY total DESC ";
        }elseif($type_id == 'cetak_stc'){
            $query = "SELECT m.id AS idOmsetStc, m.nama
                        , CASE WHEN s.type = 1 THEN 'Stc' ELSE 'M-Stc' END AS tipe
                        , IFNULL(ro.ro,0)AS ro, IFNULL(scp.scp,0)AS scp, IFNULL(rtr.rtr,0)AS rtr
                        , IFNULL(ro.ro,0)+IFNULL(scp.scp,0)-IFNULL(rtr.rtr,0) AS oms
                        ,ROUND((IFNULL(ro.ro,0)+IFNULL(scp.scp,0)-IFNULL(rtr.rtr,0))/1.1) nettSales
                FROM member m
                LEFT JOIN stockiest s ON m.id = s.id
                LEFT JOIN(
                        SELECT member_id AS id, SUM(totalharga)AS ro
                        FROM ro
                        WHERE `date` BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                        GROUP BY member_id
                )AS ro ON m.id = ro.id
                LEFT JOIN(
                        SELECT stockiest_id AS id, SUM(totalharga)AS scp
                        FROM pinjaman_titipan
                        WHERE tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                        GROUP BY stockiest_id
                )AS scp ON m.id = scp.id
                LEFT JOIN(
                        SELECT stockiest_id AS id, SUM(totalharga)AS rtr
                        FROM retur_titipan
                        WHERE tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                        GROUP BY stockiest_id
                )AS rtr ON m.id = rtr.id
                -- WHERE s.`type` = 1
                HAVING oms > 0
                ORDER BY oms DESC, s.type DESC
                ";
        }elseif($type_id == 'one_hit'){
            $query = "SELECT stc.created, ro.member_id, m.nama, SUM(ro.totalharga)AS omset, IFNULL(bns.jml,0)AS hitOnSemester
                        , CASE WHEN IFNULL(bns.jml,0) = 0 AND SUM(ro.totalharga2-(ro.totalharga2*0.06)) >= 60000000 THEN 'Get' ELSE '' END AS note
                FROM ro
                LEFT JOIN stockiest stc ON ro.member_id = stc.id
                LEFT JOIN member m ON stc.id = m.id
                LEFT JOIN(
                        SELECT member_id, COUNT(id)AS jml
                        FROM bonus
                        WHERE periode BETWEEN '2014-01-01' AND '2014-06-30'
                        AND title = 57
                        GROUP BY member_id
                )AS bns ON ro.member_id = bns.member_id
                WHERE ro.`date` BETWEEN LAST_DAY(NOW() - INTERVAL 2 MONTH) + INTERVAL 1 DAY AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                AND stc.status = 'active'
                GROUP BY ro.member_id
                HAVING omset >= 0 -- AND jml_ >= 1
                ";
        }elseif($type_id == 'pph21_new'){
            $query = "SELECT -- 'F113301' AS Kode_Form
                    MONTH(pph.periode) AS 'Masa Pajak'
                    , YEAR(pph.periode) AS 'Tahun Pajak'
                    , 0 AS Pembetulan
                    , '' AS 'Nomor Bukti Potong'
                    , CASE WHEN LENGTH(REPLACE(REPLACE(pph.npwp,'-',''), '.', '')) >= 15 THEN REPLACE(REPLACE(pph.npwp,'/',''), '.', '') ELSE '000000000000000' END AS NPWP
                    , CASE WHEN LENGTH(REPLACE(REPLACE(m.noktp,'-',''), '.', '')) >= 15 THEN REPLACE(REPLACE(m.noktp,'/',''), '.', '') ELSE '0' END AS NIK
                    , pph.nama AS 'Nama'
                    , pph.alamat AS 'Alamat'
                    , 'N' AS 'WP Luar Negeri'
                    , '' AS 'Kode Negara' -- Created Table Kode Negara
                    , '21-100-04' AS 'Kode Pajak'
                    , pph.bonus AS 'Jumlah Bruto'
                    -- , (FLOOR(pph.pkp/1000)*1000) AS 'Jumlah DPP'
                    , pph.pkp AS 'Jumlah DPP'
                    , CASE WHEN pph.tarif_npwp > 100 THEN 'Y' ELSE 'N' END AS 'Tanpa NPWP'
                    , pph.tarif AS Tarif
                    , pph.pph21 AS 'Jumlah PPH'
                    , REPLACE(REPLACE('02.313.286.3-044.001','-',''), '.', '') AS 'NPWP Pemotong'
                    , 'PT. UNIVERSAL HEALTH NETWORK' AS 'Nama Pemotong'
                    , DATE_FORMAT(pph.periode, '%d/%m/%Y') AS 'Tanggal Bukti Potong'
                    , akumulasi_bruto
            FROM pph
            LEFT JOIN member m ON pph.member_id = m.id
            WHERE YEAR(pph.periode) = YEAR(LAST_DAY(NOW() - INTERVAL 1 MONTH))
            AND MONTH(pph.periode) = MONTH(LAST_DAY(NOW() - INTERVAL 1 MONTH))
            ";
        }elseif($type_id == 'list_transfer'){
            $query = "SELECT *
                FROM(
                        SELECT namaNasabah, bank_id, MAX(cabang)AS cabang, `no`, SUM(ewallet)AS payoutTransfer
                                , MAX(biayaTransfer)AS biayaTransfer
                                , SUM(ewallet) -  MAX(biayaTransfer) AS tkHomePayed
                                -- , urut2, flag
                                , CASE 
                                        WHEN MAX(urut) = 0 THEN 'BCA'
                                        WHEN MAX(urut) = 1 THEN 'Non BCA'
                                        ELSE 'NoValid'
                                END AS urut
                        FROM
                        (
                                SELECT dtbr.memberId AS memberId, dtbr.account_id
                                        , CASE WHEN dtbr.nama <> dtbr.namaNasabah THEN 'Cek' ELSE '' END AS note
                                        , dtbr.ewallet - IFNULL(bns.bns,0) AS b4, IFNULL(bns.bns,0)AS bns
                                        , dtbr.ewallet - 5000 AS ewallet, dtbr.nama AS nama, dtbr.namaNasabah
                                        , dtbr.bank_id AS bank_id, dtbr.cabang AS cabang
                                        , dtbr.urut AS urut2, flag
                                        , CASE 
                                                WHEN dtbr.urut THEN 5000
                                                WHEN flag = 2 THEN 2500
                                                WHEN flag = 1 THEN 0
                                                ELSE 'n/a'
                                        END AS biayaTransfer
                                        , IFNULL(REPLACE(REPLACE(REPLACE(dtbr.no,'-',''),'.',''),' ',''),'-') AS NO
                                        -- , dtbr.no as no
                                        , dtbr.urut
                                FROM(
                                        SELECT dt.*, CASE 
                                                        WHEN 
                                                                `no` LIKE '%000000%' 
                                                                OR namaNasabah LIKE '%000000%' 
                                                                OR `no` IS NULL 
                                                                OR (bank_id = 'BCA' AND LENGTH(REPLACE(REPLACE(REPLACE(`no`,'.',''),' ',''),'-','')) <> 10 )
                                                                OR bank_id = '-' 
                                                        THEN '2'
                                                        WHEN bank_id = 'BCA' AND `no` NOT LIKE '%000000%' THEN '0' 
                                                        ELSE '1' END AS urut
                                        FROM(
                                                SELECT 
                                                        m.id AS memberId, m.ewallet, m.nama, m.account_id
                                                        , IFNULL(acc.bank_id,'-')AS bank_id, IFNULL(acc.area,'-') AS cabang
                                                        , IFNULL(acc.no,'_')AS NO, UCASE(IFNULL(acc.`name`,'-')) AS namaNasabah
                                                        , acc.flag
                                                FROM member m
                                                LEFT JOIN account acc ON m.account_id = acc.id
                                                WHERE m.ewallet >= 50000 AND m.id <> '00000001'
                                                -- and acc.bank_id = 'BCA'
                                                GROUP BY m.id
                                                ORDER BY acc.bank_id DESC, m.ewallet DESC
                                        )AS dt
                                        GROUP BY dt.memberId
                                        ORDER BY dt.bank_id DESC, dt.ewallet DESC
                                )AS dtbr
                                LEFT JOIN(
                                        SELECT member_id, SUM(nominal) AS bns
                                        FROM bonus
                                        WHERE periode = LAST_DAY(CURDATE() - INTERVAL 1 MONTH)
                                        GROUP BY member_id
                                )AS bns ON dtbr.memberId = bns.member_id
                                WHERE ewallet-5000 > 50000
                                -- ".$flag."
                                ORDER BY urut, bank_id, ewallet DESC
                        )AS dt
                        -- WHERE bank_id = 'BCA'
                        GROUP BY namaNasabah, bank_id, `no`
                        ORDER BY urut, namaNasabah
                )AS dt
                WHERE urut <> 'NoValid'
                ";
                
        }elseif($type_id == 'bonus_account'){
            $query = "SELECT dt2.member_id,m.nama
                    ,dt2.Bonus_Pembelian_Pribadi
                    ,dt2.Bonus_Aktivasi_Level_1
                    ,dt2.Bonus_Aktivasi_Level_2
                    ,dt2.Bonus_Aktivasi_Level_3
                    ,dt2.Bonus_Aktivasi_Level_4
                    
                    ,dt2.Bonus_Aktivasi_Level_5
                    ,dt2.Bonus_Kesuksesan_Sponsor AS 'Bonus_Kesuksesan_Sponsor(2%)'
                    ,dt2.Bonus_Kesuksesan_L_SL_SS AS 'Bonus_Kesuksesan_L_SL_SS(4%)'
                    ,dt2.Bonus_Posisi_Leader
                    ,dt2.Bonus_Posisi_Super_Leader
                    ,dt2.Bonus_Penjualan_Tahunan
                    
                    , dt2.Bonus_RequalifikasiLeader
                    ,dt2.Bonus_Distribusi_Stockiest_Area
                    ,dt2.Penyesuaian_3_RO_Stc
                    ,dt2.Bonus_Cetak_Stc_Area
                    ,dt2.Hit_Reward_Stockiest
                    
                    ,dt2.PDPM
                    ,dt2.Anatolia
                    ,dt2.CarCashBonus
                    ,dt2.CarReward
                    ,dt2.Bonus_Koreksi
                    ,dt2.SPB
                    ,dt2.PPH_21
                    
                    ,dt2.Bonus_Pembelian_Pribadi
                    +dt2.Bonus_Aktivasi_Level_1
                    +dt2.Bonus_Aktivasi_Level_2
                    +dt2.Bonus_Aktivasi_Level_3
                    +dt2.Bonus_Aktivasi_Level_4
                    
                    +dt2.Bonus_Aktivasi_Level_5
                    +dt2.Bonus_Kesuksesan_Sponsor
                    +dt2.Bonus_Kesuksesan_L_SL_SS
                    +dt2.Bonus_Posisi_Leader
                    +dt2.Bonus_Posisi_Super_Leader
                    +dt2.Bonus_Penjualan_Tahunan
                    
                    +dt2.Bonus_RequalifikasiLeader
                    +dt2.Bonus_Distribusi_Stockiest_Area
                    +dt2.Penyesuaian_3_RO_Stc
                    +dt2.Bonus_Cetak_Stc_Area
                    +dt2.Hit_Reward_Stockiest
                    
                    +dt2.PDPM
                    +dt2.Anatolia
                    +dt2.CarCashBonus
                    +dt2.CarReward
                    +dt2.Bonus_Koreksi
                    +dt2.SPB
                    +dt2.PPH_21
                    AS total
            FROM (
                    SELECT member_id
                            ,IFNULL(SUM(Bonus_Pembelian_Pribadi),0)AS Bonus_Pembelian_Pribadi
                            ,IFNULL(SUM(Bonus_Aktivasi_Level_1),0)AS Bonus_Aktivasi_Level_1
                            ,IFNULL(SUM(Bonus_Aktivasi_Level_2),0)AS Bonus_Aktivasi_Level_2
                            ,IFNULL(SUM(Bonus_Aktivasi_Level_3),0)AS Bonus_Aktivasi_Level_3
                            ,IFNULL(SUM(Bonus_Aktivasi_Level_4),0)AS Bonus_Aktivasi_Level_4
                            
                            ,IFNULL(SUM(Bonus_Aktivasi_Level_5),0)AS Bonus_Aktivasi_Level_5
                            ,IFNULL(SUM(Bonus_Kesuksesan_Sponsor),0)AS Bonus_Kesuksesan_Sponsor
                            ,IFNULL(SUM(Bonus_Kesuksesan_L_SL_SS),0)AS Bonus_Kesuksesan_L_SL_SS
                            ,IFNULL(SUM(Bonus_Posisi_Leader),0)AS Bonus_Posisi_Leader
                            ,IFNULL(SUM(Bonus_Posisi_Super_Leader),0)AS Bonus_Posisi_Super_Leader
                            ,IFNULL(SUM(Bonus_Penjualan_Tahunan),0)AS Bonus_Penjualan_Tahunan
                            
                            ,IFNULL(SUM(Bonus_RequalifikasiLeader),0)AS Bonus_RequalifikasiLeader
                            ,IFNULL(SUM(Bonus_Distribusi_Stockiest_Area),0)AS Bonus_Distribusi_Stockiest_Area
                            ,IFNULL(SUM(Penyesuaian_3_RO_Stc),0)AS Penyesuaian_3_RO_Stc
                            ,IFNULL(SUM(Bonus_Cetak_Stc_Area),0)AS Bonus_Cetak_Stc_Area
                            ,IFNULL(SUM(Hit_Reward_Stockiest),0)AS Hit_Reward_Stockiest
                            
                            ,IFNULL(SUM(PDPM),0)AS PDPM
                            ,IFNULL(SUM(Anatolia),0)AS Anatolia
                            ,IFNULL(SUM(CarCashBonus),0)AS CarCashBonus
                            ,IFNULL(SUM(CarReward),0)AS CarReward
                            ,IFNULL(SUM(Bonus_Koreksi),0)AS Bonus_Koreksi
                            ,IFNULL(SUM(SPB),0)AS SPB
                            ,IFNULL(SUM(PPH_21),0)AS PPH_21
                    FROM(
                            SELECT member_id
                                    ,CASE WHEN dt.title=25 THEN dt.nominal END AS Bonus_Pembelian_Pribadi
                                    ,CASE WHEN dt.title=26 THEN dt.nominal END AS Bonus_Aktivasi_Level_1
                                    ,CASE WHEN dt.title=27 THEN dt.nominal END AS Bonus_Aktivasi_Level_2
                                    ,CASE WHEN dt.title=28 THEN dt.nominal END AS Bonus_Aktivasi_Level_3
                                    ,CASE WHEN dt.title=29 THEN dt.nominal END AS Bonus_Aktivasi_Level_4
                                    
                                    ,CASE WHEN dt.title=30 THEN dt.nominal END AS Bonus_Aktivasi_Level_5
                                    ,CASE WHEN dt.title=31 THEN dt.nominal END AS Bonus_Kesuksesan_Sponsor
                                    ,CASE WHEN dt.title=32 THEN dt.nominal END AS Bonus_Kesuksesan_L_SL_SS
                                    ,CASE WHEN dt.title=33 THEN dt.nominal END AS Bonus_Posisi_Leader
                                    ,CASE WHEN dt.title=34 THEN dt.nominal END AS Bonus_Posisi_Super_Leader
                                    ,CASE WHEN dt.title=36 THEN dt.nominal END AS Bonus_Penjualan_Tahunan
                                    
                                    ,CASE WHEN dt.title=37 THEN dt.nominal END AS Bonus_RequalifikasiLeader
                                    ,CASE WHEN dt.title=54 THEN dt.nominal END AS Bonus_Distribusi_Stockiest_Area
                                    ,CASE WHEN dt.title=55 THEN dt.nominal END AS Penyesuaian_3_RO_Stc
                                    ,CASE WHEN dt.title=56 THEN dt.nominal END AS Bonus_Cetak_Stc_Area
                                    ,CASE WHEN dt.title=57 THEN dt.nominal END AS Hit_Reward_Stockiest
                                    
                                    ,CASE WHEN dt.title=118 THEN dt.nominal END AS PDPM
                                    ,CASE WHEN dt.title=130 THEN dt.nominal END AS Anatolia
                                    ,CASE WHEN dt.title=131 THEN dt.nominal END AS CarCashBonus
                                    ,CASE WHEN dt.title=132 THEN dt.nominal END AS CarReward
                                    ,CASE WHEN dt.title=995 THEN dt.nominal END AS Bonus_Koreksi
                                    ,CASE WHEN dt.title=127 THEN dt.nominal END AS SPB
                                    ,CASE WHEN dt.title=999 THEN dt.nominal END AS PPH_21
                            FROM (
                                    SELECT member_id,title,nominal
                                    FROM v_bonus b
                                    WHERE periode BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                                    ORDER BY member_id,title
                            )AS dt
                    )AS dt1
                    GROUP BY member_id
            )AS dt2
            LEFT JOIN member m ON m.id=dt2.member_id
            ";
        }
        elseif($type_id == 'rekening_tidak_lengkap'){
            $petik = "\'";
            $query = "
                    SELECT r.nama AS region, concat('".$petik."',dtbr.memberId) AS memberId
                                , dtbr.nama AS nama, alamatM, concat('".$petik."',dtbr.hp1)as hp1
                                , k.name AS kota
                                -- , dtbr.hp, dtbr.namaNasabah , dtbr.bank_id AS bank_id, dtbr.cabang AS cabang , IFNULL(REPLACE(REPLACE(REPLACE(dtbr.no,'-',''),'.',''),' ',''),'-') AS NO 
                                -- , dtbr.no as no 
                                , concat('".$petik."',dtbr.stockiest_id)as stockiest_id, stc.nama, stc.alamat, ks.name AS kotaSp, concat('".$petik."',stc.hp) AS hpSp, r.nama AS regionStc
                                , dtbr.urut 
                                , dtbr.ewallet
                FROM( 
                        SELECT dt.*
                                , CASE WHEN `no` LIKE '%000000%' 
                                                OR namaNasabah LIKE '%000000%' 
                                                OR `no` IS NULL 
                                                OR (bank_id = 'BCA' AND LENGTH(REPLACE(REPLACE(REPLACE(`no`,'.',''),' ',''),'-','')) <> 10 ) 
                                                OR bank_id = '-' THEN '2' 
                                WHEN bank_id = 'BCA' AND `no` NOT LIKE '%000000%' THEN '0' 
                                ELSE '1' 
                                END AS urut 
                        FROM( 
                                SELECT m.id AS memberId, m.ewallet, m.nama, m.kota_id, m.telp, m.hp, m.account_id 
                                        , IFNULL(acc.bank_id,'-')AS bank_id, IFNULL(acc.area,'-') AS cabang 
                                        , IFNULL(acc.no,'_')AS NO, IFNULL(acc.`name`,'-') AS namaNasabah 
                                        , m.stockiest_id, m.hp AS hp1, m.alamat AS alamatM
                                FROM member m 
                                LEFT JOIN account acc ON m.account_id = acc.id 
                                WHERE m.ewallet >= 50000 AND m.id <> '00000001' 
                                GROUP BY m.id 
                                ORDER BY acc.bank_id DESC, m.ewallet DESC 
                        )AS dt 
                        GROUP BY dt.memberId 
                        ORDER BY dt.bank_id DESC, dt.ewallet DESC 
                )AS dtbr 
                LEFT JOIN kota k ON dtbr.kota_id = k.id
                LEFT JOIN member stc ON dtbr.stockiest_id = stc.id
                LEFT JOIN kota ks ON stc.kota_id=ks.id
                LEFT JOIN region r ON ks.region = r.id
                LEFT JOIN( 
                        SELECT member_id, SUM(nominal) AS bns 
                        FROM bonus 
                        WHERE periode = LAST_DAY(CURDATE() - INTERVAL 1 MONTH) 
                        GROUP BY member_id 
                )AS bns ON dtbr.memberId = bns.member_id 
                WHERE dtbr.ewallet-5000 > 50000 
                AND dtbr.urut > 1
                ORDER BY k.region, dtbr.ewallet DESC
                ";
        }elseif($type_id == 'rekening_tidak_lengkap_region'){
            $petik = "\'";
            $query = "
                SELECT r.nama AS region, concat('".$petik."',dtbr.memberId) AS memberId
                                , dtbr.nama AS nama, alamatM, concat('".$petik."',dtbr.hp1)as hp1
                                , k.name AS kota
                                -- , dtbr.hp, dtbr.namaNasabah , dtbr.bank_id AS bank_id, dtbr.cabang AS cabang , IFNULL(REPLACE(REPLACE(REPLACE(dtbr.no,'-',''),'.',''),' ',''),'-') AS NO 
                                -- , dtbr.no as no 
                                , concat('".$petik."',dtbr.stockiest_id)as stockiest_id, stc.nama, stc.alamat, ks.name AS kotaSp, concat('".$petik."',stc.hp) AS hpSp, r.nama AS regionStc
                                , dtbr.urut 
                                , dtbr.ewallet
                FROM( 
                        SELECT dt.*
                                , CASE WHEN `no` LIKE '%000000%' 
                                                OR namaNasabah LIKE '%000000%' 
                                                OR `no` IS NULL 
                                                OR (bank_id = 'BCA' AND LENGTH(REPLACE(REPLACE(REPLACE(`no`,'.',''),' ',''),'-','')) <> 10 ) 
                                                OR bank_id = '-' THEN '2' 
                                WHEN bank_id = 'BCA' AND `no` NOT LIKE '%000000%' THEN '0' 
                                ELSE '1' 
                                END AS urut 
                        FROM( 
                                SELECT m.id AS memberId, m.ewallet, m.nama, m.kota_id, m.telp, m.hp, m.account_id 
                                        , IFNULL(acc.bank_id,'-')AS bank_id, IFNULL(acc.area,'-') AS cabang 
                                        , IFNULL(acc.no,'_')AS NO, IFNULL(acc.`name`,'-') AS namaNasabah 
                                        , m.stockiest_id, m.hp AS hp1, m.alamat AS alamatM
                                FROM member m 
                                LEFT JOIN account acc ON m.account_id = acc.id 
                                WHERE m.ewallet >= 50000 AND m.id <> '00000001' 
                                GROUP BY m.id 
                                ORDER BY acc.bank_id DESC, m.ewallet DESC 
                        )AS dt 
                        GROUP BY dt.memberId 
                        ORDER BY dt.bank_id DESC, dt.ewallet DESC 
                )AS dtbr 
                LEFT JOIN kota k ON dtbr.kota_id = k.id
                LEFT JOIN member stc ON dtbr.stockiest_id = stc.id
                LEFT JOIN kota ks ON stc.kota_id=ks.id
                LEFT JOIN region r ON ks.region = r.id
                LEFT JOIN( 
                        SELECT member_id, SUM(nominal) AS bns 
                        FROM bonus 
                        WHERE periode = LAST_DAY(CURDATE() - INTERVAL 1 MONTH) 
                        GROUP BY member_id 
                )AS bns ON dtbr.memberId = bns.member_id 
                WHERE dtbr.ewallet-5000 > 50000 
                AND dtbr.urut > 1
                ORDER BY k.region, dtbr.ewallet DESC
                
            ";
        }elseif($type_id == 'update_stockist'){
                $query = "
                SELECT dt.member_id
            , s.no_stc, m.nama, s.type
            , SUM(jmlMktTools1)AS jmlMktTools1
            , SUM(jmlBlj1)AS jmlBlj1
            , SUM(jmlKit1)AS jmlKit1
            , SUM(jmlMktTools2)AS jmlMktTools2
            , SUM(jmlBlj2)AS jmlBlj2
            , SUM(jmlKit2)AS jmlKit2
            , SUM(jmlMktTools1) + SUM(jmlMktTools2) AS jmlMktTools
            , SUM(jmlBlj1) + SUM(jmlBlj2) AS jmlBlj
            , SUM(jmlKit1) + SUM(jmlKit2) AS jmlKit
            -- , case when SUM(jmlBlj1) + SUM(jmlBlj2) >= 30000000 then 'Cek' else '' end as note
            , IFNULL(rtr.jmlRtrMktTools,0)AS jmlRtrMktTools
            , IFNULL(rtr.jmlRtr,0)AS jmlRtr
            , SUM(jmlMktTools1) + SUM(jmlMktTools2) - IFNULL(rtr.jmlRtrMktTools,0) AS TotalMktTools
            , SUM(jmlBlj1) + SUM(jmlBlj2) - IFNULL(rtr.jmlRtr,0) AS TotalBlj
            , CASE WHEN SUM(jmlKit1) + SUM(jmlKit2) + SUM(jmlMktTools1) + SUM(jmlMktTools2) + SUM(jmlBlj1) + SUM(jmlBlj2) - IFNULL(rtr.jmlRtr,0) >= 30000000 THEN 'Cek' ELSE '' END AS note
            , CASE WHEN SUM(jmlKit1) + SUM(jmlKit2) + SUM(jmlMktTools1) + SUM(jmlMktTools2) + SUM(jmlBlj1) + SUM(jmlBlj2) - IFNULL(rtr.jmlRtr,0) >= 30000000 THEN ROUND(((SUM(jmlBlj1) + SUM(jmlBlj2) - IFNULL(rtr.jmlRtr,0))*0.06)*1) ELSE '' END AS penyesuaianRO
            , CASE WHEN IFNULL(us.member_id,0) = 0 THEN '' ELSE 'Cek' END AS note2
    FROM(
            SELECT ro.member_id
                    , CASE WHEN i.type_id = 3 AND MONTH(ro.date) = (MONTH(NOW()) - 2) THEN rod.jmlharga ELSE 0 END AS jmlMktTools1
                    , CASE WHEN i.type_id = 2 AND MONTH(ro.date) = (MONTH(NOW()) - 2) THEN rod.jmlharga ELSE 0 END AS jmlBlj1
                    , CASE WHEN i.type_id = 1 AND MONTH(ro.date) = (MONTH(NOW()) - 2) THEN rod.jmlharga ELSE 0 END AS jmlKit1
                    
                    , CASE WHEN i.type_id = 3 AND MONTH(ro.date) = (MONTH(NOW()) - 1) THEN rod.jmlharga ELSE 0 END AS jmlMktTools2
                    , CASE WHEN i.type_id = 2 AND MONTH(ro.date) = (MONTH(NOW()) - 1) THEN rod.jmlharga ELSE 0 END AS jmlBlj2
                    , CASE WHEN i.type_id = 1 AND MONTH(ro.date) = (MONTH(NOW()) - 1) THEN rod.jmlharga ELSE 0 END AS jmlKit2
            FROM ro
            RIGHT JOIN ro_d rod ON ro.id = rod.ro_id
            LEFT JOIN item i ON rod.item_id = i.id
            WHERE ro.`date` BETWEEN (LAST_DAY(NOW() - INTERVAL 3 MONTH)+ INTERVAL 1 DAY) AND (LAST_DAY(NOW() - INTERVAL 1 MONTH))
            AND ro.stockiest_id = 0
    )AS dt
    LEFT JOIN(
            SELECT member_id
                    , SUM(jmlRtrMktTools) AS jmlRtrMktTools
                    , SUM(jmlRtr) AS jmlRtr
            FROM(
                    SELECT rtr.id, rtr.tgl, rtr.stockiest_id AS member_id, rtrd.item_id
                            , CASE WHEN i.type_id = 3 THEN rtrd.jmlharga ELSE 0 END AS jmlRtrMktTools
                            , CASE WHEN i.type_id <> 3 THEN rtrd.jmlharga ELSE 0 END AS jmlRtr
                    FROM retur_titipan rtr
                    RIGHT JOIN retur_titipan_d rtrd ON rtr.id = rtrd.retur_titipan_id
                    LEFT JOIN item i ON rtrd.item_id = i.id
                    WHERE rtr.tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 3 MONTH)+ INTERVAL 1 DAY) AND (LAST_DAY(NOW() - INTERVAL 1 MONTH))
            )AS dt
            GROUP BY member_id
    )AS rtr ON dt.member_id = rtr.member_id
    LEFT JOIN member m ON dt.member_id = m.id
    LEFT JOIN stockiest s ON m.id = s.id
    LEFT JOIN(
            SELECT member_id, 1 AS jml
            FROM update_stc
            WHERE createddate BETWEEN LAST_DAY(NOW() - INTERVAL 1 MONTH) + INTERVAL 1 DAY AND LAST_DAY(NOW())
            AND old_type = new_type
    )AS us ON s.id = us.member_id
    GROUP BY dt.member_id
    ORDER BY note2 DESC, s.type DESC, jmlBlj DESC
    ";
    }elseif($type_id =='leader_conference'){
            $petik = "\'";
            
			$query="
			SELECT concat('".$petik."',member_id)as member_id, m.nama, s.no_stc
	, SUM(ps1)AS ps1, SUM(pgs1)+SUM(ps1)AS pgs1, SUM(ba1)AS ba1, ro1
	, SUM(ps2)AS ps2, SUM(pgs2)+SUM(ps2)AS pgs2, SUM(ba2)AS ba2, ro2
	, SUM(ps3)AS ps3, SUM(pgs3)+SUM(ps3)AS pgs3, SUM(ba3)AS ba3, ro3
	, SUM(ps4)AS ps4, SUM(pgs4)+SUM(ps4)AS pgs4, SUM(ba4)AS ba4, ro4
	, SUM(ps5)AS ps5, SUM(pgs5)+SUM(ps5)AS pgs5, SUM(ba5)AS ba5, ro5
	, SUM(ps6)AS ps6, SUM(pgs6)+SUM(ps6)AS pgs6, SUM(ba6)AS ba6, ro6
	, SUM(ps7)AS ps7, SUM(pgs7)+SUM(ps7)AS pgs7, SUM(ba7)AS ba7, ro7
	, SUM(ps8)AS ps8, SUM(pgs8)+SUM(ps8)AS pgs8, SUM(ba8)AS ba8, ro8
	, SUM(ps9)AS ps9, SUM(pgs9)+SUM(ps9)AS pgs9, SUM(ba9)AS ba9, ro9
	, SUM(ps10)AS ps10, SUM(pgs10)+SUM(ps10)AS pgs10, SUM(ba10)AS ba10, ro10
	, SUM(ps11)AS ps11, SUM(pgs11)+SUM(ps11)AS pgs11, SUM(ba11)AS ba11, ro11
	, SUM(ps12)AS ps12, SUM(pgs12)+SUM(ps12)AS pgs12, SUM(ba12)AS ba12, ro12
FROM(
	SELECT member_id
		, CASE WHEN MONTH(tgl) = 1 THEN pgs-pgs_cut ELSE 0 END AS pgs1
		, CASE WHEN MONTH(tgl) = 2 THEN pgs-pgs_cut ELSE 0 END AS pgs2
		, CASE WHEN MONTH(tgl) = 3 THEN pgs-pgs_cut ELSE 0 END AS pgs3
		, CASE WHEN MONTH(tgl) = 4 THEN pgs-pgs_cut ELSE 0 END AS pgs4
		, CASE WHEN MONTH(tgl) = 5 THEN pgs-pgs_cut ELSE 0 END AS pgs5
		, CASE WHEN MONTH(tgl) = 6 THEN pgs-pgs_cut ELSE 0 END AS pgs6
		, CASE WHEN MONTH(tgl) = 7 THEN pgs-pgs_cut ELSE 0 END AS pgs7
		, CASE WHEN MONTH(tgl) = 8 THEN pgs-pgs_cut ELSE 0 END AS pgs8
		, CASE WHEN MONTH(tgl) = 9 THEN pgs-pgs_cut ELSE 0 END AS pgs9
		, CASE WHEN MONTH(tgl) = 10 THEN pgs-pgs_cut ELSE 0 END AS pgs10
		, CASE WHEN MONTH(tgl) = 11 THEN pgs-pgs_cut ELSE 0 END AS pgs11
		, CASE WHEN MONTH(tgl) = 12 THEN pgs-pgs_cut ELSE 0 END AS pgs12
		
		, CASE WHEN MONTH(tgl) = 1 THEN ps ELSE 0 END AS ps1
		, CASE WHEN MONTH(tgl) = 2 THEN ps ELSE 0 END AS ps2
		, CASE WHEN MONTH(tgl) = 3 THEN ps ELSE 0 END AS ps3
		, CASE WHEN MONTH(tgl) = 4 THEN ps ELSE 0 END AS ps4
		, CASE WHEN MONTH(tgl) = 5 THEN ps ELSE 0 END AS ps5
		, CASE WHEN MONTH(tgl) = 6 THEN ps ELSE 0 END AS ps6
		, CASE WHEN MONTH(tgl) = 7 THEN ps ELSE 0 END AS ps7
		, CASE WHEN MONTH(tgl) = 8 THEN ps ELSE 0 END AS ps8
		, CASE WHEN MONTH(tgl) = 9 THEN ps ELSE 0 END AS ps9
		, CASE WHEN MONTH(tgl) = 10 THEN ps ELSE 0 END AS ps10
		, CASE WHEN MONTH(tgl) = 11 THEN ps ELSE 0 END AS ps11
		, CASE WHEN MONTH(tgl) = 12 THEN ps ELSE 0 END AS ps12
		
		, CASE WHEN MONTH(tgl) = 1 THEN (pgs_temp+sumbangan)/20000000 ELSE 0 END AS ba1
		, CASE WHEN MONTH(tgl) = 2 THEN (pgs_temp+sumbangan)/20000000 ELSE 0 END AS ba2
		, CASE WHEN MONTH(tgl) = 3 THEN (pgs_temp+sumbangan)/20000000 ELSE 0 END AS ba3
		, CASE WHEN MONTH(tgl) = 4 THEN (pgs_temp+sumbangan)/20000000 ELSE 0 END AS ba4
		, CASE WHEN MONTH(tgl) = 5 THEN (pgs_temp+sumbangan)/20000000 ELSE 0 END AS ba5
		, CASE WHEN MONTH(tgl) = 6 THEN (pgs_temp+sumbangan)/20000000 ELSE 0 END AS ba6
		, CASE WHEN MONTH(tgl) = 7 THEN (pgs_temp+sumbangan)/20000000 ELSE 0 END AS ba7
		, CASE WHEN MONTH(tgl) = 8 THEN (pgs_temp+sumbangan)/20000000 ELSE 0 END AS ba8
		, CASE WHEN MONTH(tgl) = 9 THEN (pgs_temp+sumbangan)/20000000 ELSE 0 END AS ba9
		, CASE WHEN MONTH(tgl) = 10 THEN (pgs_temp+sumbangan)/20000000 ELSE 0 END AS ba10
		, CASE WHEN MONTH(tgl) = 11 THEN (pgs_temp+sumbangan)/20000000 ELSE 0 END AS ba11
		, CASE WHEN MONTH(tgl) = 12 THEN (pgs_temp+sumbangan)/20000000 ELSE 0 END AS ba12
	FROM pgs_bulanan
	WHERE tgl BETWEEN '2014-01-01' AND '2014-12-31'
	-- and member_id = '00007505'
	ORDER BY member_id
)AS dt
LEFT JOIN member m ON dt.member_id = m.id
LEFT JOIN stockiest s ON m.id = s.id
LEFT JOIN(
	SELECT s.no_stc, s.id
		, IFNULL(ro.ro1,0)+IFNULL(scp.scp1,0)-IFNULL(rtr.rtr1,0) AS ro1
		, IFNULL(ro.ro2,0)+IFNULL(scp.scp2,0)-IFNULL(rtr.rtr2,0) AS ro2
		, IFNULL(ro.ro3,0)+IFNULL(scp.scp3,0)-IFNULL(rtr.rtr3,0) AS ro3
		, IFNULL(ro.ro4,0)+IFNULL(scp.scp4,0)-IFNULL(rtr.rtr4,0) AS ro4
		, IFNULL(ro.ro5,0)+IFNULL(scp.scp5,0)-IFNULL(rtr.rtr5,0) AS ro5
		, IFNULL(ro.ro6,0)+IFNULL(scp.scp6,0)-IFNULL(rtr.rtr6,0) AS ro6
		, IFNULL(ro.ro7,0)+IFNULL(scp.scp7,0)-IFNULL(rtr.rtr7,0) AS ro7
		, IFNULL(ro.ro8,0)+IFNULL(scp.scp8,0)-IFNULL(rtr.rtr8,0) AS ro8
		, IFNULL(ro.ro9,0)+IFNULL(scp.scp9,0)-IFNULL(rtr.rtr9,0) AS ro9
		, IFNULL(ro.ro10,0)+IFNULL(scp.scp10,0)-IFNULL(rtr.rtr10,0) AS ro10
		, IFNULL(ro.ro11,0)+IFNULL(scp.scp11,0)-IFNULL(rtr.rtr11,0) AS ro11
		, IFNULL(ro.ro12,0)+IFNULL(scp.scp12,0)-IFNULL(rtr.rtr12,0) AS ro12
	FROM stockiest s
	LEFT JOIN(
		SELECT member_id
			, SUM(ro1)AS ro1, SUM(ro2)AS ro2, SUM(ro3)AS ro3
			, SUM(ro4)AS ro4, SUM(ro5)AS ro5, SUM(ro6)AS ro6
			, SUM(ro7)AS ro7, SUM(ro8)AS ro8, SUM(ro9)AS ro9
			, SUM(ro10)AS ro10, SUM(ro11)AS ro11, SUM(ro12)AS ro12
		FROM(
			SELECT member_id
				, CASE WHEN MONTH(`date`) = 1 THEN totalharga ELSE 0 END AS ro1, CASE WHEN MONTH(`date`) = 2 THEN totalharga ELSE 0 END AS ro2, CASE WHEN MONTH(`date`) = 3 THEN totalharga ELSE 0 END AS ro3
				, CASE WHEN MONTH(`date`) = 4 THEN totalharga ELSE 0 END AS ro4, CASE WHEN MONTH(`date`) = 5 THEN totalharga ELSE 0 END AS ro5, CASE WHEN MONTH(`date`) = 6 THEN totalharga ELSE 0 END AS ro6
				, CASE WHEN MONTH(`date`) = 7 THEN totalharga ELSE 0 END AS ro7, CASE WHEN MONTH(`date`) = 8 THEN totalharga ELSE 0 END AS ro8, CASE WHEN MONTH(`date`) = 9 THEN totalharga ELSE 0 END AS ro9
				, CASE WHEN MONTH(`date`) = 10 THEN totalharga ELSE 0 END AS ro10, CASE WHEN MONTH(`date`) = 11 THEN totalharga ELSE 0 END AS ro11, CASE WHEN MONTH(`date`) = 12 THEN totalharga ELSE 0 END AS ro12
			FROM ro
			WHERE `date` BETWEEN '2014-01-01' AND '2014-12-31'
		)AS ro
		GROUP BY member_id
	)AS ro ON s.id = ro.member_id
	LEFT JOIN(
		SELECT member_id
			, SUM(rtr1)AS rtr1, SUM(rtr2)AS rtr2, SUM(rtr3)AS rtr3
			, SUM(rtr4)AS rtr4, SUM(rtr5)AS rtr5, SUM(rtr6)AS rtr6
			, SUM(rtr7)AS rtr7, SUM(rtr8)AS rtr8, SUM(rtr9)AS rtr9
			, SUM(rtr10)AS rtr10, SUM(rtr11)AS rtr11, SUM(rtr12)AS rtr12
		FROM(
			SELECT stockiest_id AS member_id
				, CASE WHEN MONTH(tgl) = 1 THEN totalharga ELSE 0 END AS rtr1, CASE WHEN MONTH(tgl) = 2 THEN totalharga ELSE 0 END AS rtr2, CASE WHEN MONTH(tgl) = 3 THEN totalharga ELSE 0 END AS rtr3
				, CASE WHEN MONTH(tgl) = 4 THEN totalharga ELSE 0 END AS rtr4, CASE WHEN MONTH(tgl) = 5 THEN totalharga ELSE 0 END AS rtr5, CASE WHEN MONTH(tgl) = 6 THEN totalharga ELSE 0 END AS rtr6
				, CASE WHEN MONTH(tgl) = 7 THEN totalharga ELSE 0 END AS rtr7, CASE WHEN MONTH(tgl) = 8 THEN totalharga ELSE 0 END AS rtr8, CASE WHEN MONTH(tgl) = 9 THEN totalharga ELSE 0 END AS rtr9
				, CASE WHEN MONTH(tgl) = 10 THEN totalharga ELSE 0 END AS rtr10, CASE WHEN MONTH(tgl) = 11 THEN totalharga ELSE 0 END AS rtr11, CASE WHEN MONTH(tgl) = 12 THEN totalharga ELSE 0 END AS rtr12
			FROM retur_titipan
			WHERE tgl BETWEEN '2014-01-01' AND '2014-12-31'
		)AS rtr
		GROUP BY member_id
	)AS rtr ON s.id = rtr.member_id
	LEFT JOIN(
		SELECT member_id
			, SUM(scp1)AS scp1, SUM(scp2)AS scp2, SUM(scp3)AS scp3
			, SUM(scp4)AS scp4, SUM(scp5)AS scp5, SUM(scp6)AS scp6
			, SUM(scp7)AS scp7, SUM(scp8)AS scp8, SUM(scp9)AS scp9
			, SUM(scp10)AS scp10, SUM(scp11)AS scp11, SUM(scp12)AS scp12
		FROM(
			SELECT stockiest_id AS member_id
				, CASE WHEN MONTH(tgl) = 1 THEN totalharga ELSE 0 END AS scp1, CASE WHEN MONTH(tgl) = 2 THEN totalharga ELSE 0 END AS scp2, CASE WHEN MONTH(tgl) = 3 THEN totalharga ELSE 0 END AS scp3
				, CASE WHEN MONTH(tgl) = 4 THEN totalharga ELSE 0 END AS scp4, CASE WHEN MONTH(tgl) = 5 THEN totalharga ELSE 0 END AS scp5, CASE WHEN MONTH(tgl) = 6 THEN totalharga ELSE 0 END AS scp6
				, CASE WHEN MONTH(tgl) = 7 THEN totalharga ELSE 0 END AS scp7, CASE WHEN MONTH(tgl) = 8 THEN totalharga ELSE 0 END AS scp8, CASE WHEN MONTH(tgl) = 9 THEN totalharga ELSE 0 END AS scp9
				, CASE WHEN MONTH(tgl) = 10 THEN totalharga ELSE 0 END AS scp10, CASE WHEN MONTH(tgl) = 11 THEN totalharga ELSE 0 END AS scp11, CASE WHEN MONTH(tgl) = 12 THEN totalharga ELSE 0 END AS scp12
			FROM pinjaman_titipan
			WHERE tgl BETWEEN '2014-01-01' AND '2014-12-31'
		)AS scp
		GROUP BY member_id
	)AS scp ON s.id = scp.member_id
)AS ro ON s.id = ro.id
GROUP BY member_id
			";
			/*
            $query="
SELECT concat('".$petik."',dt.member_id)as member_id, m.nama, j.name AS jenjang, IFNULL(s.no_stc,'-')AS kdStc
	, SUM(pgs1)AS pgs1, SUM(ps1)AS ps1, SUM(pgs_temp1)/20000000 AS ba1, IFNULL(ql.q1,0)AS q1
	, SUM(pgs2)AS pgs2, SUM(ps2)AS ps2, SUM(pgs_temp2)/20000000 AS ba2, IFNULL(ql.q2,0)AS q2
	, SUM(pgs3)AS pgs3, SUM(ps3)AS ps3, SUM(pgs_temp3)/20000000 AS ba3, IFNULL(ql.q3,0)AS q3
	, SUM(pgs4)AS pgs4, SUM(ps4)AS ps4, SUM(pgs_temp4)/20000000 AS ba4, IFNULL(ql.q4,0)AS q4
	, SUM(pgs5)AS pgs5, SUM(ps5)AS ps5, SUM(pgs_temp5)/20000000 AS ba5, IFNULL(ql.q5,0)AS q5
	, SUM(pgs6)AS pgs6, SUM(ps6)AS ps6, SUM(pgs_temp6)/20000000 AS ba6, IFNULL(ql.q6,0)AS q6
	, SUM(pgs7)AS pgs7, SUM(ps7)AS ps7, SUM(pgs_temp7)/20000000 AS ba7, IFNULL(ql.q7,0)AS q7
	, SUM(pgs8)AS pgs8, SUM(ps8)AS ps8, SUM(pgs_temp8)/20000000 AS ba8, IFNULL(ql.q8,0)AS q8
	, SUM(pgs9)AS pgs9, SUM(ps9)AS ps9, SUM(pgs_temp9)/20000000 AS ba9, IFNULL(ql.q9,0)AS q9
	, SUM(pgs10)AS pgs10, SUM(ps10)AS ps10, SUM(pgs_temp10)/20000000 AS ba10, IFNULL(ql.q10,0)AS q10
	, SUM(pgs11)AS pgs11, SUM(ps11)AS ps11, SUM(pgs_temp11)/20000000 AS ba11, IFNULL(ql.q11,0)AS q11
	, SUM(pgs12)AS pgs12, SUM(ps12)AS ps12, SUM(pgs_temp12)/20000000 AS ba12, IFNULL(ql.q12,0)AS q12
FROM(
	SELECT member_id, MONTH(tgl)AS bln, ps, pgs
		, CASE WHEN MONTH(tgl)=1 THEN ps ELSE 0 END AS ps1
		, CASE WHEN MONTH(tgl)=2 THEN ps ELSE 0 END AS ps2
		, CASE WHEN MONTH(tgl)=3 THEN ps ELSE 0 END AS ps3
		, CASE WHEN MONTH(tgl)=4 THEN ps ELSE 0 END AS ps4
		, CASE WHEN MONTH(tgl)=5 THEN ps ELSE 0 END AS ps5
		, CASE WHEN MONTH(tgl)=6 THEN ps ELSE 0 END AS ps6
		, CASE WHEN MONTH(tgl)=7 THEN ps ELSE 0 END AS ps7
		, CASE WHEN MONTH(tgl)=8 THEN ps ELSE 0 END AS ps8
		, CASE WHEN MONTH(tgl)=9 THEN ps ELSE 0 END AS ps9
		, CASE WHEN MONTH(tgl)=10 THEN ps ELSE 0 END AS ps10
		, CASE WHEN MONTH(tgl)=11 THEN ps ELSE 0 END AS ps11
		, CASE WHEN MONTH(tgl)=12 THEN ps ELSE 0 END AS ps12
		
		, CASE WHEN MONTH(tgl)=1 THEN pgs ELSE 0 END AS pgs1
		, CASE WHEN MONTH(tgl)=2 THEN pgs ELSE 0 END AS pgs2
		, CASE WHEN MONTH(tgl)=3 THEN pgs ELSE 0 END AS pgs3
		, CASE WHEN MONTH(tgl)=4 THEN pgs ELSE 0 END AS pgs4
		, CASE WHEN MONTH(tgl)=5 THEN pgs ELSE 0 END AS pgs5
		, CASE WHEN MONTH(tgl)=6 THEN pgs ELSE 0 END AS pgs6
		, CASE WHEN MONTH(tgl)=7 THEN pgs ELSE 0 END AS pgs7
		, CASE WHEN MONTH(tgl)=8 THEN pgs ELSE 0 END AS pgs8
		, CASE WHEN MONTH(tgl)=9 THEN pgs ELSE 0 END AS pgs9
		, CASE WHEN MONTH(tgl)=10 THEN pgs ELSE 0 END AS pgs10
		, CASE WHEN MONTH(tgl)=11 THEN pgs ELSE 0 END AS pgs11
		, CASE WHEN MONTH(tgl)=12 THEN pgs ELSE 0 END AS pgs12
		
		, CASE WHEN MONTH(tgl)=1 THEN pgs_temp ELSE 0 END AS pgs_temp1
		, CASE WHEN MONTH(tgl)=2 THEN pgs_temp ELSE 0 END AS pgs_temp2
		, CASE WHEN MONTH(tgl)=3 THEN pgs_temp ELSE 0 END AS pgs_temp3
		, CASE WHEN MONTH(tgl)=4 THEN pgs_temp ELSE 0 END AS pgs_temp4
		, CASE WHEN MONTH(tgl)=5 THEN pgs_temp ELSE 0 END AS pgs_temp5
		, CASE WHEN MONTH(tgl)=6 THEN pgs_temp ELSE 0 END AS pgs_temp6
		, CASE WHEN MONTH(tgl)=7 THEN pgs_temp ELSE 0 END AS pgs_temp7
		, CASE WHEN MONTH(tgl)=8 THEN pgs_temp ELSE 0 END AS pgs_temp8
		, CASE WHEN MONTH(tgl)=9 THEN pgs_temp ELSE 0 END AS pgs_temp9
		, CASE WHEN MONTH(tgl)=10 THEN pgs_temp ELSE 0 END AS pgs_temp10
		, CASE WHEN MONTH(tgl)=11 THEN pgs_temp ELSE 0 END AS pgs_temp11
		, CASE WHEN MONTH(tgl)=12 THEN pgs_temp ELSE 0 END AS pgs_temp12
	FROM pgs_bulanan
	WHERE YEAR(tgl) = YEAR(NOW() - INTERVAL 1 MONTH)
)AS dt
LEFT JOIN(
	SELECT member_id
		, SUM(q1)AS q1, SUM(q2)AS q2, SUM(q3)AS q3
		, SUM(q4)AS q4, SUM(q5)AS q5, SUM(q6)AS q6
		, SUM(q7)AS q7, SUM(q8)AS q8, SUM(q9)AS q9
		, SUM(q10)AS q10, SUM(q11)AS q11, SUM(q12)AS q12
	FROM(
		SELECT member_id, MONTH(tgl)AS bln, qty AS q
			, CASE WHEN MONTH(tgl) = 1 THEN qty ELSE 0 END AS q1
			, CASE WHEN MONTH(tgl) = 2 THEN qty ELSE 0 END AS q2
			, CASE WHEN MONTH(tgl) = 3 THEN qty ELSE 0 END AS q3
			, CASE WHEN MONTH(tgl) = 4 THEN qty ELSE 0 END AS q4
			, CASE WHEN MONTH(tgl) = 5 THEN qty ELSE 0 END AS q5
			, CASE WHEN MONTH(tgl) = 6 THEN qty ELSE 0 END AS q6
			, CASE WHEN MONTH(tgl) = 7 THEN qty ELSE 0 END AS q7
			, CASE WHEN MONTH(tgl) = 8 THEN qty ELSE 0 END AS q8
			, CASE WHEN MONTH(tgl) = 9 THEN qty ELSE 0 END AS q9
			, CASE WHEN MONTH(tgl) = 10 THEN qty ELSE 0 END AS q10
			, CASE WHEN MONTH(tgl) = 11 THEN qty ELSE 0 END AS q11
			, CASE WHEN MONTH(tgl) = 12 THEN qty ELSE 0 END AS q12
		FROM leader_qualified
		WHERE YEAR(tgl) = YEAR(NOW() - INTERVAL 1 MONTH)
	)AS dt
	GROUP BY member_id
)AS ql ON dt.member_id = ql.member_id
LEFT JOIN member m ON dt.member_id = m.id
LEFT JOIN jenjang j ON m.jenjang_id = j.id
LEFT JOIN stockiest s ON m.id = s.id
WHERE m.jenjang_id > 3
GROUP BY member_id
ORDER BY j.id DESC
";
		*/
        }elseif($type_id == ''){
            $query = "";
        }else{
            $this->db->select("m.id as member_id,m.nama,format(m.pgs,0)as fpgs,j.decription as jenjang",false)
                ->from('member m')
                ->join('jenjang j','m.jenjang_id=j.id','left')
                ->where('m.pgs >=',40000000)
                ->order_by('m.pgs','desc')
                ->order_by('m.nama','asc');
            //$query = $this->db->get();        
        }
        //echo $this->db->last_query();
        //echo $query." ** ".$type_id;
        
        
        $data = $this->db->query($query);
        //echo $this->db->last_query()." ".$temp;
        /*if($q->num_rows > 0){
	    foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        
        $q->free_result();
        */
        return $data;
    }
    




}
?>