<?php
class MRekap extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Laporan Omzet 
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-09-29
    |
    */
    public function omzet_ro($stc,$fromdate,$todate)
    {
        $where = "a.stockiest_id = '0' and (a.date between '$fromdate' and '$todate') ";
        if($stc == 'stc')$where = "s.type = '1' and ".$where;
        else $where = "s.type = '2' and ".$where;
        $this->db->select("a.id,a.member_id,date_format(a.date,'%d-%b-%Y')as tgl,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama as namamember,s.no_stc",false) 
            ->from('ro a')
            ->join('member m','a.member_id=m.id','left')
            ->join('stockiest s','a.member_id=s.id','left')
            ->where($where);
            
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_omzet_ro($stc,$fromdate,$todate){
        $where = "a.stockiest_id = '0' and (a.date between '$fromdate' and '$todate') ";
        if($stc == 'stc')$where = "s.type = '1' and ".$where;
        else $where = "s.type = '2' and ".$where;
        
        $data = array();
        $this->db->select("sum(a.totalharga)as totalharga,format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('ro a')
            ->join('stockiest s','a.member_id=s.id','left')
            ->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function omzet_so($fromdate,$todate)
    {
        $where = "a.stockiest_id = '0' and (a.tgl between '$fromdate' and '$todate') ";
        
        $this->db->select("a.id,a.member_id,date_format(a.tgl,'%d-%b-%Y')as tgl,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama as namamember,s.no_stc",false) 
            ->from('so a')->join('member m','a.member_id=m.id','left')->join('stockiest s','a.stockiest_id=s.id','left')
            ->where($where);
            
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_omzet_so($fromdate,$todate){
        $where = "a.stockiest_id = '0' and (a.tgl between '$fromdate' and '$todate') ";
        $data = array();
        $this->db->select("sum(a.totalharga)as totalharga,format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('so a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function omzet_retur($fromdate,$todate)
    {
        $where = "(a.tgl between '$fromdate' and '$todate') ";
        
        $this->db->select("a.id,a.stockiest_id as member_id,date_format(a.tgl,'%d-%b-%Y')as tgl,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama as namamember,s.no_stc",false) 
            ->from('retur_titipan a')->join('member m','a.stockiest_id=m.id','left')->join('stockiest s','a.stockiest_id=s.id','left')
            ->where($where);
            
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_omzet_retur($fromdate,$todate){
        $where = "(a.tgl between '$fromdate' and '$todate') ";
        $data = array();
        $this->db->select("sum(a.totalharga)as totalharga,format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('retur_titipan a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Rekap Sales Order f   or Member & Back Office
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-05-20
    |
    */
    public function list_salesorder($fromdate,$todate,$member_id)
    {
        if($this->session->userdata('group_id') <= 100){
			$select = "a.id,a.member_id,date_format(a.tgl,'%d-%b-%Y')as tgl,format(a.totalharga_,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.kit,a.remark,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
						m.nama as namamember,s.no_stc";
            if($member_id)$where = "a.member_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
            else $where = "(a.tgl between '$fromdate' and '$todate') ";
        }else{
            $select = "a.id,a.member_id,date_format(a.tgl,'%d-%b-%Y')as tgl,format(a.totalharga_,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.kit,a.remark,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
						m.nama as namamember,s.no_stc";
			$where = "a.member_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
        }
        $this->db->select($select,false) 
            ->from('so a')->join('member m','a.member_id=m.id','left')->join('stockiest s','a.stockiest_id=s.id','left')
            ->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                //$rs['date_added'] = mdate("%d/%m/%Y", mysql_to_unix($rs['date_added']));
                $rs['details'] = $this->list_salesorder_detail($rs['id']);
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function list_salesorder_detail($id=0)
    {
        if($this->session->userdata('group_id') <= 100){
			$select = "d.item_id,i.name,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.pv,0)as fpv,format(d.jmlharga,0)as fjmlharga,format(d.jmlpv,0)as fjmlpv";
		}else{
			$select = "d.item_id,i.name,format(d.qty,0)as fqty,format(d.harga_,0)as fharga,format(d.pv,0)as fpv,format(d.jmlharga_,0)as fjmlharga,format(d.jmlpv,0)as fjmlpv";
		}
		$q = $this->db->select($select,false)
        ->from('so_d d')->join('item i','d.item_id=i.id','left')->where('d.so_id',$id)->get();
        $details = array();
        
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $details[] = $rs;
            }
        }
        $q->free_result();
        return $details;
    }
    public function sum_salesorder($fromdate,$todate,$member_id)
    {
        if($this->session->userdata('group_id') <= 100){
			$select = "format(sum(a.totalharga_),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv";
            if($member_id)$where = "a.member_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
            else $where = "(a.tgl between '$fromdate' and '$todate') ";
        }else{
			$select = "format(sum(a.totalharga_),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv";
            $where = "a.member_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
        }
        
        $data = array();
        $this->db->select($select,false)
            ->from('so a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
    public function getWarehouse(){
        $data = array();
        $q=$this->db->select("id,name",false)
            ->from('warehouse')
            ->order_by('id','asc')
            ->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[$row['id']] = $row['name'];
            }
        }
        $q->free_result();
        return $data;
    }    
    
    public function countFreeSO($keywords=0){
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND a.id LIKE '$keywords%'";
        }
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
            else $where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        $this->db->select("a.id",false);
        $this->db->from('promofree a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }

    public function getFreeSO($id=0){
        $data = array();
        $this->db->select("a.id,a.approveddate,a.approvedby,a.remark,a.status,a.approvedby,s.no_stc,m.nama,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('promofree a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->session->userdata('group_id')>100)$this->db->where('a.stockiest_id',$this->session->userdata('userid'));
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$this->db->where('a.warehouse_id',$whsid); 
        }
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getFreeSODetail($id=0){
        $data = array();
        $this->db->select("d.item_id,d.tgl,format(d.qty,0)as fqty,d.member_id,m.nama,a.name",false);
        $this->db->from('promofree_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->join('member m','d.member_id=m.id','left');
        $this->db->where('d.promofree_id',$id);
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function getFreeSOApproved($id=0){
        $data = array();
        $this->db->select("a.id,a.approveddate,a.approvedby,a.warehouse_id,a.remark,a.status,a.approvedby,s.no_stc,m.nama,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('promofree a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->session->userdata('group_id')>100)$this->db->where('a.stockiest_id',$this->session->userdata('userid'));
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$this->db->where('a.warehouse_id',$whsid); 
        }
        $this->db->where('a.status','pending');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    public function freeSOApproved(){
        $id = $this->input->post('id');
        $remarkapp=$this->db->escape_str($this->input->post('remark'));
        $empid = $this->session->userdata('user');
        $whsid = $this->input->post('whsid');
        $this->db->query("call sp_free_so('$id','$whsid','$remarkapp','$empid')");
    }
    
    /*
    |--------------------------------------------------------------------------
    | Rekap Sales Order for Stockiest & Back Office
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-04
    |
    */
    public function list_salesorder_stc($fromdate,$todate,$member_id)
    {
        if($this->session->userdata('group_id') <= 100){
            if($member_id)$where = "a.stockiest_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
            else $where = "(a.tgl between '$fromdate' and '$todate') ";
        }else{
            $where = "a.stockiest_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
        }
        $this->db->select("a.id,a.member_id,date_format(a.tgl,'%d-%b-%Y')as tgl,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.kit,a.remark,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                          m.nama as namamember,s.no_stc",false)
            ->from('so a')->join('member m','a.member_id=m.id','left')->join('stockiest s','a.stockiest_id=s.id','left')
            ->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                //$rs['date_added'] = mdate("%d/%m/%Y", mysql_to_unix($rs['date_added']));
                $rs['details'] = $this->list_salesorder_detail($rs['id']);
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_salesorder_stc($fromdate,$todate,$member_id)
    {
        if($this->session->userdata('group_id') <= 100){
            if($member_id)$where = "a.stockiest_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
            else $where = "(a.tgl between '$fromdate' and '$todate') ";
        }else{
            $where = "a.stockiest_id = '$member_id' and (a.tgl between '$fromdate' and '$todate') ";
        }
        $data = array();
        $this->db->select("format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('so a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
    
    public function getDropDownWhs(){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            $data['all']='All Cabang';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    public function getDropDownWhs2(){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            $data['all']='All Cabang Total';   
            $data['all_hub']='Rincian All Cabang';
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    public function getDropDownCluster(){
        $data = array();
        $q = $this->db->get('propinsi_cluster');
        if($q->num_rows >0){
            $data['all']='All';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['alias'];    
            }
        }
        $q->free_result();
        return $data;
    }
    public function getDropDownCity(){
        $data = array();
        $q = $this->db->get('kota');
        if($q->num_rows >0){
            $data['all']='All';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    /*
    |--------------------------------------------------------------------------
    | Rekap Deposit Back Office
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-04
    |
    */
    public function list_deposit($fromdate,$todate,$whsid)
    {
        if($whsid == 'all')$where = "a.approved = 'approved' and (a.tgl_approved between '$fromdate' and '$todate 23:59:59') ";
        else $where = "a.warehouse_id = '$whsid' and a.approved = 'approved' and (a.tgl_approved between '$fromdate' and '$todate 23:59:59') ";
        $this->db->select("a.id,a.member_id,a.tgl_approved,a.approvedby,m.nama,s.no_stc,a.flag,format(a.transfer,0)as ftransfer,format(a.tunai,0)as ftunai,format(a.debit_card,0)as fdebit_card,
                          format(a.credit_card,0)as fcredit_card,format(a.total_approved,0)as ftotal_approved, a.bank_id, a.remark, a.remark_fin",false)
            ->from('deposit a')->join('member m','a.member_id=m.id','left')->join('stockiest s','a.member_id=s.id','left')
            ->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_deposit($fromdate,$todate,$whsid)
    {
        if($whsid == 'all')$where = "a.approved = 'approved' and (a.tgl_approved between '$fromdate' and '$todate 23:59:59') ";
        else $where = "a.warehouse_id = '$whsid' and a.approved = 'approved' and (a.tgl_approved between '$fromdate' and '$todate 23:59:59') ";
        $this->db->select("format(sum(a.transfer),0)as ftransfer,format(sum(a.tunai),0)as ftunai,format(sum(a.debit_card),0)as fdebit_card,
                          format(sum(a.credit_card),0)as fcredit_card,format(sum(a.total_approved),0)as ftotal_approved,sum(a.total_approved)as total_approved",false)
            ->from('deposit a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    /*
    |--------------------------------------------------------------------------
    | Rekap Withdrawal Back Office
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-05
    |
    */
    public function list_withdrawal($via,$fromdate,$todate)
    {
        $where = "a.status = 'approved' and (a.approved between '$fromdate' and '$todate') ";
        if($via)$where="a.event_id = '$via' and ".$where;
        
        $this->db->select("a.id,a.member_id,a.approved,a.approvedby,m.nama,a.flag,format(a.amount,0)as famount,format(a.biayaadm,0)as fbiayaadm,format(a.amount-a.biayaadm,0)as fbayar,c.no_stc,s.bank_id,s.name,s.no,s.area",false)
            ->from('withdrawal a')->join('member m','a.member_id=m.id','left')->join('stockiest c','a.member_id=c.id','left')->join('account s','a.account_id=s.id','left')
            ->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_withdrawal($via,$fromdate,$todate)
    {
        $where = "a.status = 'approved' and (a.approved between '$fromdate' and '$todate') ";
        if($via)$where="a.event_id = '$via' and ".$where;
        
        $this->db->select("format(sum(a.amount),0)as famount,sum(a.amount)as amount,format(sum(a.biayaadm),0)as fbiayaadm,format(sum(a.amount)-sum(a.biayaadm),0)as fbayar",false)
            ->from('withdrawal a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Neraca Daily Fund
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-06
    |
    */
    public function sum_SO($fromdate,$todate,$whsid,$so,$flag)
    {
        if($so == 'so'){
            if($flag == 'order'){
                if($whsid == 'all')$where = "a.stockiest_id = '0' and (a.tgl between '$fromdate' and '$todate') ";
                else $where = "a.warehouse_id = '$whsid' and a.stockiest_id = '0' and (a.tgl between '$fromdate' and '$todate') ";
            }else{
                if($whsid == 'all')$where = "a.stockiest_id = '0' and (a.created between '$fromdate' and '$todate') ";
                else $where = "a.warehouse_id = '$whsid' and a.stockiest_id = '0' and (a.created between '$fromdate' and '$todate') ";
            }
        }elseif($so == 'stc'){
            if($flag == 'order'){
                if($whsid == 'all')$where = "a.stockiest_id != '0' and (a.tgl between '$fromdate' and '$todate') ";
                else $where = "a.warehouse_id = '$whsid' and a.stockiest_id != '0' and (a.tgl between '$fromdate' and '$todate') ";
            }else{
                if($whsid == 'all')$where = "a.stockiest_id != '0' and (a.created between '$fromdate' and '$todate') ";
                else $where = "a.warehouse_id = '$whsid' and a.stockiest_id != '0' and (a.created between '$fromdate' and '$todate') ";
            }
        }else{
            if($flag == 'order'){
                if($whsid == 'all')$where = "(a.tgl between '$fromdate' and '$todate') ";
                else $where = "a.warehouse_id = '$whsid' and (a.tgl between '$fromdate' and '$todate') ";
            }else{
                if($whsid == 'all')$where = "(a.created between '$fromdate' and '$todate') ";
                else $where = "a.warehouse_id = '$whsid' and (a.created between '$fromdate' and '$todate') ";
            }
        }
        $data = array();
        $this->db->select("format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('so a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function sum_SOKIT($fromdate,$todate,$whsid,$so)
    {
        if($so == 'so'){
            if($whsid == 'all')$where = "a.kit = 'Y' and a.stockiest_id = '0' and (a.tgl between '$fromdate' and '$todate') ";
            else $where = "a.kit = 'Y' and a.warehouse_id = '$whsid' and a.stockiest_id = '0' and (a.tgl between '$fromdate' and '$todate') ";
        }else{
            if($whsid == 'all')$where = "a.kit = 'Y' and a.stockiest_id != '0' and (a.tgl between '$fromdate' and '$todate') ";
            else $where = "a.kit = 'Y' and a.warehouse_id = '$whsid' and a.stockiest_id != '0' and (a.tgl between '$fromdate' and '$todate') ";
        }
        $data = array();
        $this->db->select("format(count(*),0)as fjmlmember,format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('so a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function sum_RO($fromdate,$todate,$whsid,$flag){
        if($flag == 'company'){
            if($whsid == 'all')$where = "a.stockiest_id = '0' and (a.date between '$fromdate' and '$todate') ";
            else $where = "a.warehouse_id = '$whsid' and a.stockiest_id = '0' and (a.date between '$fromdate' and '$todate') ";
        }else{
            if($whsid == 'all')$where = "a.stockiest_id != '0' and (a.date between '$fromdate' and '$todate') ";
            else $where = "a.warehouse_id = '$whsid' and a.stockiest_id != '0' and (a.date between '$fromdate' and '$todate') ";
        }
        $data = array();
        $this->db->select("format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('ro a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Rekap History Pinjaman Stockiest
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-09
    |
    */
    public function list_saldopinjaman($stcid)
    {
        $this->db->select("a.item_id,a.name,format(sum(a.qty),0)as fqty,a.fharga,a.fpv,format(sum(a.jmlharga),0)as ftotalharga,format(sum(a.jmlpv),0)as ftotalpv",false)
            ->from('v_pinjaman a');
        if($stcid)$this->db->where('a.stockiest_id',$stcid);
        $this->db->group_by('a.item_id');
        $this->db->group_by('a.harga');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_saldopinjaman($stcid)
    {
         $this->db->select("format(sum(a.jmlharga),0)as ftotalharga,format(sum(a.jmlpv),0)as ftotalpv",false)
            ->from('v_pinjaman a');
        if($stcid)$this->db->where('a.stockiest_id',$stcid);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function list_pinjamanhistory($fromdate,$todate,$stcid)
    {
        $where = "a.stockiest_id = '$stcid' and (a.tgl between '$fromdate' and '$todate') ";
        
        $this->db->select("a.noref,date_format(a.tgl,'%d-%b-%Y')as tgl,createdby,format(a.awal,0)as fawal,format(a.pinjam,0)as fpinjam,format(a.lunas,0)as flunas,format(a.akhir,0)as fakhir,a.remark",false)
            ->from('pinjaman_titipan_h a');
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=1;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sum_pinjamanhistory($fromdate,$todate,$stcid)
    {
        $where = "a.stockiest_id = '$stcid' and (a.tgl between '$fromdate' and '$todate') ";
        
         $this->db->select("format(sum(a.pinjam),0)as ftotalpinjam,format(sum(a.lunas),0)as ftotallunas",false)
            ->from('pinjaman_titipan_h a');
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function list_pinjaman_stock($fromdate,$todate,$stcid)
    {
        $where = "a.member_id = '$stcid' and (a.tgl between '$fromdate' and '$todate') ";
        
        $this->db->select("a.noref,date_format(a.tgl,'%d-%b-%Y')as ftgl,a.createdby,format(a.saldoawal,0)as fawal,
                          format(a.saldoin,0)as fin,format(a.saldoout,0)as fout,format(a.saldoakhir,0)as fakhir,
                          format(a.harga,0)as fharga,a.item_id,i.name",false)
            ->from('pinjaman_history a')
            ->join('item i','a.item_id=i.id','left')
            ->where($where)
            ->order_by('a.item_id','asc')
            ->order_by('a.harga','asc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    /*
    |--------------------------------------------------------------------------
    | Rekap Pinjaman & Pembayaran Stockiest
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-10
    |
    */
    public function sum_Pinjaman($fromdate,$todate,$whsid){
        if($whsid == 'all')$where = "(a.tgl between '$fromdate' and '$todate') ";
        else $where = "a.warehouse_id = '$whsid' and (a.tgl between '$fromdate' and '$todate') ";
        
        $data = array();
        $this->db->select("format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('pinjaman a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function sum_Pelunasan($fromdate,$todate){
        $where = "(a.tgl between '$fromdate' and '$todate') ";
        
        $data = array();
        $this->db->select("format(sum(a.totalharga),0)as ftotalharga,format(sum(a.totalpv),0)as ftotalpv",false)
            ->from('pinjaman_titipan a')->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function sum_saldotitipan()
    {
         $this->db->select("format(sum(a.jmlharga),0)as ftotalharga,format(sum(a.jmlpv),0)as ftotalpv",false)
            ->from('v_titipan a');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function sum_saldoewallet($table){
        $data = array();
        $this->db->select("format(sum(ewallet),0)as ftotalewallet",false)
            ->from($table);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
	
	/* Created by Boby (2009-12-10) */
	public function omset_ro($fromdate,$todate)
    {
		$flag = 1;
		$q = "	
		SELECT 
			id, nama
			, FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			, ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama, dta.price AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
			FROM(
				SELECT id, `name`, price FROM item
			)AS dta

			LEFT JOIN(
				SELECT sod.item_id, SUM(qty)AS jml, sod.harga AS harga";
		if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM so_d sod
				LEFT JOIN so ON sod.so_id = so.id
				WHERE 
					so.tgl BETWEEN '$fromdate' AND '$todate'
					AND so.stockiest_id = '0'
				GROUP BY item_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id

			LEFT JOIN(
				SELECT rod.item_id, SUM(qty)AS jml, rod.harga AS harga";
		if($flag==0){$q.="	, SUM(rod.jmlharga)AS total";}else{$q.="	, SUM(1*rod.jmlharga)AS total";}
		$q.="	FROM ro_d rod
				LEFT JOIN ro ON rod.ro_id = ro.id
				WHERE 
					ro.`date` BETWEEN '$fromdate' AND '$todate'
					AND ro.stockiest_id = '0'
				GROUP BY item_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id

			LEFT JOIN(
				SELECT ptd.item_id, SUM(qty) AS jml, ptd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM pinjaman_titipan_d ptd
				LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
				WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY ptd.item_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id

			LEFT JOIN(
				SELECT rtd.item_id, SUM(qty) AS jml, rtd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM retur_titipan_d rtd
				LEFT JOIN retur_titipan rt ON rtd.retur_titipan_id = rt.id
				WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY rtd.item_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id
			
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				GROUP BY ncd.item_id
			)AS dtf ON dta.id = dtf.item_id
		)AS newdata
		";
        $data = array();
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
	public function omzet_scp($fromdate,$todate)
    {
        $data = array();$i=0;
		$qry = $this->db->query("		
			SELECT pin.id, pin.tgl, s.no_stc, m.nama, pin.totalharga, pin.totalpv
			FROM pinjaman_titipan pin
			LEFT JOIN member m ON pin.stockiest_id = m.id
			LEFT JOIN stockiest s ON pin.stockiest_id = s.id
			WHERE tgl BETWEEN '$fromdate' AND '$todate'
		");
		
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$i+=1;
				$row['i'] = $i;
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
	public function sum_omzet_scp($fromdate,$todate){
        $data = array();
		$qry = $this->db->query("		
			SELECT SUM(pin.totalharga)AS totalhrg, SUM(pin.totalpv)AS totalpv
			FROM pinjaman_titipan pin
			LEFT JOIN member m ON pin.stockiest_id = m.id
			LEFT JOIN stockiest s ON pin.stockiest_id = s.id
			WHERE tgl BETWEEN '$fromdate' AND '$todate'
		");
        if($qry->num_rows()>0){
            $data = $qry->row_array();
        }
        $qry->free_result();
        return $data;
    }
    /* End created by Boby (2009-12-10) */
	
	/* Created by Boby 20100720 */
	public function periodeOmset($fromdate){
		$data = array();
		$qry = $this->db->query("
			SELECT CONCAT(MONTHNAME('$fromdate'), ' ',YEAR('$fromdate')) AS bln1
			, CONCAT(MONTHNAME('$fromdate' + INTERVAL 1 MONTH), ' ',YEAR('$fromdate' + INTERVAL 1 MONTH)) AS bln2
			, CONCAT(MONTHNAME('$fromdate' + INTERVAL 2 MONTH), ' ',YEAR('$fromdate' + INTERVAL 2 MONTH)) AS bln3
			, CONCAT(MONTHNAME('$fromdate' + INTERVAL 3 MONTH), ' ',YEAR('$fromdate' + INTERVAL 3 MONTH)) AS bln4
			, CONCAT(MONTHNAME('$fromdate' + INTERVAL 4 MONTH), ' ',YEAR('$fromdate' + INTERVAL 4 MONTH)) AS bln5
			, CONCAT(MONTHNAME('$fromdate' + INTERVAL 5 MONTH), ' ',YEAR('$fromdate' + INTERVAL 5 MONTH)) AS bln6
			, MONTH('$fromdate') AS b1
			, MONTH('$fromdate' + INTERVAL 1 MONTH) AS b2
			, MONTH('$fromdate' + INTERVAL 2 MONTH) AS b3
			, MONTH('$fromdate' + INTERVAL 3 MONTH) AS b4
			, MONTH('$fromdate' + INTERVAL 4 MONTH) AS b5
			, MONTH('$fromdate' + INTERVAL 5 MONTH) AS b6
		");
        if($qry->num_rows()>0){
            $data = $qry->row_array();
        }
        $qry->free_result();
        return $data;
    }

	public function omsetPerKota($fromdate, $todate, $leader){
        $data = array();
		//$leader = 1;
		$query = "
		SELECT *, dt4.1+dt4.2+dt4.3+dt4.4+dt4.5+dt4.6+dt4.7+dt4.8+dt4.9+dt4.10+dt4.11+dt4.12 AS total
		FROM (
			SELECT ";
				if($leader == 1){$query .= " leader, nmLeader as kota";}else{$query .= " kota ";} $query.="
				, IFNULL(SUM(bln1),0) AS '1' , IFNULL(SUM(bln2),0) AS '2'
				, IFNULL(SUM(bln3),0) AS '3' , IFNULL(SUM(bln4),0) AS '4'
				, IFNULL(SUM(bln5),0) AS '5' , IFNULL(SUM(bln6),0) AS '6'
				, IFNULL(SUM(bln7),0) AS '7' , IFNULL(SUM(bln8),0) AS '8'
				, IFNULL(SUM(bln9),0) AS '9' , IFNULL(SUM(bln10),0) AS '10'
				, IFNULL(SUM(bln11),0) AS '11' , IFNULL(SUM(bln12),0) AS '12'
			FROM (
				SELECT";
					if($leader == 1){$query .= " bln, leader, nmLeader ";}else{$query .= " bln, kota ";} $query.="
					, CASE WHEN bln = 1 THEN omset END AS 'bln1' , CASE WHEN bln = 2 THEN omset END AS 'bln2'
					, CASE WHEN bln = 3 THEN omset END AS 'bln3' , CASE WHEN bln = 4 THEN omset END AS 'bln4'
					, CASE WHEN bln = 5 THEN omset END AS 'bln5' , CASE WHEN bln = 6 THEN omset END AS 'bln6'
					, CASE WHEN bln = 7 THEN omset END AS 'bln7' , CASE WHEN bln = 8 THEN omset END AS 'bln8'
					, CASE WHEN bln = 9 THEN omset END AS 'bln9' , CASE WHEN bln = 10 THEN omset END AS 'bln10'
					, CASE WHEN bln = 11 THEN omset END AS 'bln11' , CASE WHEN bln = 12 THEN omset END AS 'bln12'
				FROM (
					SELECT bln, txt, num, SUM(omset)AS omset, ";
					if($leader == 1){$query .= " leader, nmLeader ";}else{$query .= " kota_id, kota ";} $query.="
					FROM (
						SELECT 
							MONTH(so.tgl)AS bln, CONCAT(MONTHNAME(so.tgl), ' ', YEAR(so.tgl)) AS txt, CONCAT(YEAR(so.tgl), MONTH(so.tgl)) AS num
							, so.stockiest_id, IFNULL(SUM(so.totalharga),0) AS omset, ";
							if($leader == 1){$query .= " stc.leader_id AS leader, l.nama AS nmLeader ";}
							else{$query .= " stc.kota_id, IFNULL(k.`name`, '__Unknown__') AS kota ";} $query.="
						FROM so
						LEFT JOIN stockiest stc ON so.stockiest_id = stc.id ";
						if($leader != 1){$query .= " LEFT JOIN kota k ON stc.kota_id = k.id ";}
						else{$query .= " LEFT JOIN member l ON stc.leader_id = l.id ";} 
						$query.="
						WHERE so.tgl BETWEEN '$fromdate' AND ";
						if($todate==$fromdate){
							// $query .= "LAST_DAY('$fromdate' + INTERVAL 5 MONTH)";
							$query .= "'$todate' ";
						}else{
							$query .= "'$todate' ";
						}
						$query .="
						GROUP BY MONTHNAME(so.tgl), so.stockiest_id";
					if($leader == 1){$query .= " 
					)AS dt1
					GROUP BY bln, leader
					ORDER BY num, leader
				)AS dt2
				ORDER BY leader, num
			)AS dt3
			GROUP BY leader
		)AS dt4
		ORDER BY total DESC ";}else{$query .= " 
					)AS dt1
					GROUP BY bln, kota
					ORDER BY num, kota_id
				)AS dt2
				ORDER BY kota, num
			)AS dt3
			GROUP BY kota
		)AS dt4
		ORDER BY total DESC";} $query.="
		";
		
		$qry = $this->db->query($query);
		
		// echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
	public function periodeOmsetPerProduct($fromdate, $leader, $leaderId, $kota_id){
		//$leader = 1;
		$data = array();
		$query = "
		SELECT *
			, dt4.jml1+dt4.jml2+dt4.jml3+dt4.jml4+dt4.jml5+dt4.jml6+dt4.jml7+dt4.jml8+dt4.jml9+dt4.jml10+dt4.jml11+dt4.jml12 AS total
			, dt4.bln1+dt4.bln2+dt4.bln3+dt4.bln4+dt4.bln5+dt4.bln6+dt4.bln7+dt4.bln8+dt4.bln9+dt4.bln10+dt4.bln11+dt4.bln12 AS totalJml
		FROM (
			SELECT 
				";$query.=($leader == 0)?"kota_id, ifnull(k.`name`, '__Unknown__') AS nama":"stc, m.nama";$query.="
				, item_id, i.`name` as kota
				, IFNULL(SUM(jml1),0) AS jml1, IFNULL(SUM(bln1),0) AS bln1
				, IFNULL(SUM(jml2),0) AS jml2, IFNULL(SUM(bln2),0) AS bln2
				, IFNULL(SUM(jml3),0) AS jml3, IFNULL(SUM(bln3),0) AS bln3
				, IFNULL(SUM(jml4),0) AS jml4, IFNULL(SUM(bln4),0) AS bln4
				, IFNULL(SUM(jml5),0) AS jml5, IFNULL(SUM(bln5),0) AS bln5
				, IFNULL(SUM(jml6),0) AS jml6, IFNULL(SUM(bln6),0) AS bln6
				, IFNULL(SUM(jml7),0) AS jml7, IFNULL(SUM(bln7),0) AS bln7
				, IFNULL(SUM(jml8),0) AS jml8, IFNULL(SUM(bln8),0) AS bln8
				, IFNULL(SUM(jml9),0) AS jml9, IFNULL(SUM(bln9),0) AS bln9
				, IFNULL(SUM(jml10),0) AS jml10, IFNULL(SUM(bln10),0) AS bln10
				, IFNULL(SUM(jml11),0) AS jml11, IFNULL(SUM(bln11),0) AS bln11
				, IFNULL(SUM(jml12),0) AS jml12, IFNULL(SUM(bln12),0) AS bln12
			FROM (
				SELECT
					bln, item_id, 
					";$query.=($leader == 0)?"kota AS kota_id":"stc";$query.="
					, CASE WHEN bln = 1 THEN qty END AS jml1 , CASE WHEN bln = 1 THEN jmlharga END AS bln1
					, CASE WHEN bln = 2 THEN qty END AS jml2 , CASE WHEN bln = 2 THEN jmlharga END AS bln2
					, CASE WHEN bln = 3 THEN qty END AS jml3 , CASE WHEN bln = 3 THEN jmlharga END AS bln3
					, CASE WHEN bln = 4 THEN qty END AS jml4 , CASE WHEN bln = 4 THEN jmlharga END AS bln4
					, CASE WHEN bln = 5 THEN qty END AS jml5 , CASE WHEN bln = 5 THEN jmlharga END AS bln5
					, CASE WHEN bln = 6 THEN qty END AS jml6 , CASE WHEN bln = 6 THEN jmlharga END AS bln6
					, CASE WHEN bln = 7 THEN qty END AS jml7 , CASE WHEN bln = 7 THEN jmlharga END AS bln7
					, CASE WHEN bln = 8 THEN qty END AS jml8 , CASE WHEN bln = 8 THEN jmlharga END AS bln8
					, CASE WHEN bln = 9 THEN qty END AS jml9 , CASE WHEN bln = 9 THEN jmlharga END AS bln9
					, CASE WHEN bln = 10 THEN qty END AS jml10 , CASE WHEN bln = 10 THEN jmlharga END AS bln10
					, CASE WHEN bln = 11 THEN qty END AS jml11 , CASE WHEN bln = 11 THEN jmlharga END AS bln11
					, CASE WHEN bln = 12 THEN qty END AS jml12 , CASE WHEN bln = 12 THEN jmlharga END AS bln12
				FROM (
					SELECT MONTH(so.tgl)AS bln, sod.item_id, 
						";$query.=($leader == 0)?"ifnull(s.kota_id,'__Unknown_') AS kota,":"s.leader_id AS stc,";$query.="
						SUM(sod.qty) AS qty, sod.harga, SUM(sod.qty)*sod.harga AS total, SUM(sod.jmlharga) AS jmlharga
					FROM so_d sod
					LEFT JOIN so ON sod.so_id = so.id
					LEFT JOIN stockiest s ON so.stockiest_id = s.id
					WHERE
						so.tgl BETWEEN '$fromdate' AND LAST_DAY('$fromdate' + INTERVAL 5 MONTH)
					GROUP BY sod.item_id, 
					";$query.=($leader == 0)?"kota":"stc";$query.="
					,MONTH(so.tgl)
					ORDER BY ";$query.=($leader == 0)?"kota":"stc";$query.=", bln, item_id
				)AS dt
				GROUP BY bln, item_id, ";$query.=($leader == 0)?"kota":"stc";$query.="
				ORDER BY ";$query.=($leader == 0)?"kota":"stc";$query.=", bln, item_id
			)AS dt1
			";$query.=($leader == 0)?"LEFT JOIN kota k ON dt1.kota_id = k.id":"LEFT JOIN member m ON dt1.stc = m.id";$query.="
			LEFT JOIN item i ON dt1.item_id = i.id ";
			if($leaderId!='' && $leader==1){$query.=" where stc = '$leaderId' ";}
			if($kota_id!='' && $leader==0){$query.=" where kota_id = '$kota_id' ";}
			$query.= " GROUP BY item_id, ";$query.=($leader == 0)?"kota_id":"stc";$query.="
			ORDER BY ";$query.=($leader == 0)?"kota_id":"stc";$query.=", item_id
		)AS dt4";
		$qry = $this->db->query($query);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
        $qry->free_result();
        return $data;
    }
	/* End created by Boby 20100720 */
	
	public function omset_ro_($fromdate,$todate)
    {
		$flag = 1;
		$q = "	
		SELECT 
			id, nama
			-- , FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			-- , ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, ((jml + jmlsc + 0) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
			, (total + totsc + 0) + totretur AS totalnominal1_
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama -- , dta.harga AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
			FROM(
				SELECT i.id, i.`name` -- , sod.harga
				FROM item i
				-- LEFT JOIN so_d sod ON i.id = sod.item_id
				-- GROUP BY sod.item_id -- , sod.harga
			)AS dta
			LEFT JOIN(
				SELECT sod.item_id, SUM(qty)AS jml, sod.harga AS harga";
		if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM so_d sod
				LEFT JOIN so ON sod.so_id = so.id
				WHERE 
					so.tgl BETWEEN '$fromdate' AND '$todate'
					AND so.stockiest_id = '0'
				GROUP BY item_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT rod.item_id, SUM(qty)AS jml, rod.harga AS harga";
		if($flag==0){$q.="	, SUM(rod.jmlharga)AS total";}else{$q.="	, SUM(1*rod.jmlharga)AS total";}
		$q.="	FROM ro_d rod
				LEFT JOIN ro ON rod.ro_id = ro.id
				WHERE 
					ro.`date` BETWEEN '$fromdate' AND '$todate'
					AND ro.stockiest_id = '0'
				GROUP BY item_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT ptd.item_id, SUM(qty) AS jml, ptd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM pinjaman_titipan_d ptd
				LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
				WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY ptd.item_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
			LEFT JOIN(
				SELECT rtd.item_id, SUM(qty) AS jml, rtd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM retur_titipan_d rtd
				LEFT JOIN retur_titipan rt ON rtd.retur_titipan_id = rt.id
				WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY rtd.item_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				GROUP BY ncd.item_id
			)AS dtf ON dta.id = dtf.item_id
		)AS newdata
		HAVING totalnominal1_ <> 0 OR jmlnc <> 0
		-- order by nama
		order by id, nama
		";
        $data = array();
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
    
	// 20160930 ASP Start
	public function omset_ro_new_($fromdate,$todate)
    {
		$flag = 1;
		$q = "	
		SELECT 
			id, nama
			-- , FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			-- adj stock
			, jmladjstock, FORMAT(totadjstock,0)AS totadjstock, totadjstock AS totadjstock1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			-- , ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, ((jml + jmlsc + 0) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
			, (total + totsc + 0) + totretur AS totalnominal1_
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama -- , dta.harga AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtg.jml,0) AS jmladjstock, IFNULL(dtg.total,0) AS totadjstock
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
			FROM(
				SELECT i.id, i.`name` -- , sod.harga
				FROM item i
				-- LEFT JOIN so_d sod ON i.id = sod.item_id
				-- GROUP BY sod.item_id -- , sod.harga
			)AS dta
			LEFT JOIN(
				SELECT sod.item_id, SUM(qty)AS jml, sod.harga AS harga";
		if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM so_d sod
				LEFT JOIN so ON sod.so_id = so.id
				WHERE 
					so.tgl BETWEEN '$fromdate' AND '$todate'
					AND so.stockiest_id = '0'
				GROUP BY item_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT rod.item_id, SUM(qty)AS jml, rod.harga AS harga";
		if($flag==0){$q.="	, SUM(rod.jmlharga)AS total";}else{$q.="	, SUM(1*rod.jmlharga)AS total";}
		$q.="	FROM ro_d rod
				LEFT JOIN ro ON rod.ro_id = ro.id
				WHERE 
					ro.`date` BETWEEN '$fromdate' AND '$todate'
					AND ro.stockiest_id = '0'
				GROUP BY item_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT ptd.item_id, SUM(qty) AS jml, ptd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM pinjaman_titipan_d ptd
				LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
				WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY ptd.item_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
			LEFT JOIN(
				SELECT rtd.item_id, SUM(qty) AS jml, rtd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM retur_titipan_d rtd
				LEFT JOIN retur_titipan rt ON rtd.retur_titipan_id = rt.id
				WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY rtd.item_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				-- AND remark  NOT LIKE 'Free Of RO%'
				-- AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%')
				AND (
					CASE WHEN nc.`date` >= '2020-05-01' AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%' AND remark NOT LIKE 'Free Of SO%' AND remark NOT LIKE 'CFSO no.%' ) THEN 1
						 WHEN nc.`date` < '2020-05-01' AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%' ) THEN 2
					ELSE 0 END
				) IN (1,2)
				GROUP BY ncd.item_id
			)AS dtf ON dta.id = dtf.item_id
			-- adj free RO
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				-- AND remark  LIKE 'Free Of RO%'
				-- AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%')
				AND (
					CASE WHEN nc.`date` >= '2020-05-01' AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%' OR remark LIKE 'Free Of SO%' OR remark LIKE 'CFSO no.%' ) THEN 1
						 WHEN nc.`date` < '2020-05-01' AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%' ) THEN 2
					ELSE 0 END
				) IN (1,2)
				GROUP BY ncd.item_id
			)AS dtg ON dta.id = dtg.item_id

		)AS newdata
		HAVING totalnominal1_ <> 0 OR jmlnc <> 0 OR jmladjstock <> 0
		-- order by nama
		order by id, nama
		";
        $data = array();
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	// 20160930	ASP End
	
	/* Created by Boby 2012-08-06 */
	public function periodeOmset_($fromdate){
		$data = array();
		$qry = $this->db->query("
			SELECT 
			  CONCAT(MONTHNAME('$fromdate' - INTERVAL 5 MONTH), ' ',YEAR('$fromdate' - INTERVAL 5 MONTH)) AS bln1
			, CONCAT(MONTHNAME('$fromdate' - INTERVAL 4 MONTH), ' ',YEAR('$fromdate' - INTERVAL 4 MONTH)) AS bln2
			, CONCAT(MONTHNAME('$fromdate' - INTERVAL 3 MONTH), ' ',YEAR('$fromdate' - INTERVAL 3 MONTH)) AS bln3
			, CONCAT(MONTHNAME('$fromdate' - INTERVAL 2 MONTH), ' ',YEAR('$fromdate' - INTERVAL 2 MONTH)) AS bln4
			, CONCAT(MONTHNAME('$fromdate' - INTERVAL 1 MONTH), ' ',YEAR('$fromdate' - INTERVAL 1 MONTH)) AS bln5
			, CONCAT(MONTHNAME('$fromdate'), ' ',YEAR('$fromdate')) AS bln6
			, MONTH('$fromdate' - INTERVAL 5 MONTH) AS b1
			, MONTH('$fromdate' - INTERVAL 4 MONTH) AS b2
			, MONTH('$fromdate' - INTERVAL 3 MONTH) AS b3
			, MONTH('$fromdate' - INTERVAL 2 MONTH) AS b4
			, MONTH('$fromdate' - INTERVAL 1 MONTH) AS b5
			, MONTH('$fromdate') AS b6
		");
        if($qry->num_rows()>0){
            $data = $qry->row_array();
        }
        $qry->free_result();
        return $data;
    }
	
	public function omsetByRgn($fromdate){
        $data = array();
		//$leader = 1;
		$query = "
SELECT *
FROM(
	SELECT k.id
	, IFNULL(oms,'UHN')AS oms
	,k.name AS kota
	,(IFNULL(st.bln1,'0')+IFNULL(u.bln1,'0'))AS '1'
	,(IFNULL(st.bln2,'0')+IFNULL(u.bln2,'0'))AS '2'
	,(IFNULL(st.bln3,'0')+IFNULL(u.bln3,'0'))AS '3'
	,(IFNULL(st.bln4,'0')+IFNULL(u.bln4,'0'))AS '4'
	,(IFNULL(st.bln5,'0')+IFNULL(u.bln5,'0'))AS '5'
	,(IFNULL(st.bln6,'0')+IFNULL(u.bln6,'0'))AS '6'
	,(IFNULL(st.bln7,'0')+IFNULL(u.bln7,'0'))AS '7'
	,(IFNULL(st.bln8,'0')+IFNULL(u.bln8,'0'))AS '8'
	,(IFNULL(st.bln9,'0')+IFNULL(u.bln9,'0'))AS '9'
	,(IFNULL(st.bln10,'0')+IFNULL(u.bln10,'0'))AS '10'
	,(IFNULL(st.bln11,'0')+IFNULL(u.bln11,'0'))AS '11'
	,(IFNULL(st.bln12,'0')+IFNULL(u.bln12,'0'))AS '12'
	,IFNULL(st.bln1,'0')+IFNULL(u.bln1,'0')
		+IFNULL(st.bln2,'0')+IFNULL(u.bln2,'0')
		+IFNULL(st.bln3,'0')+IFNULL(u.bln3,'0')
		+IFNULL(st.bln4,'0')+IFNULL(u.bln4,'0')
		+IFNULL(st.bln5,'0')+IFNULL(u.bln5,'0')
		+IFNULL(st.bln6,'0')+IFNULL(u.bln6,'0')
		+IFNULL(st.bln7,'0')+IFNULL(u.bln7,'0')
		+IFNULL(st.bln8,'0')+IFNULL(u.bln8,'0')
		+IFNULL(st.bln9,'0')+IFNULL(u.bln9,'0')
		+IFNULL(st.bln10,'0')+IFNULL(u.bln10,'0')
		+IFNULL(st.bln11,'0')+IFNULL(u.bln11,'0')
		+IFNULL(st.bln12,'0')+IFNULL(u.bln12,'0')
	AS total
	FROM kota k
	LEFT JOIN(
		SELECT 'Stockiest' AS oms
			, dt.kota_id
			,IFNULL(SUM(dt.bln1),'0')AS bln1
			,IFNULL(SUM(dt.bln2),'0')AS bln2
			,IFNULL(SUM(dt.bln3),'0')AS bln3
			,IFNULL(SUM(dt.bln4),'0')AS bln4
			,IFNULL(SUM(dt.bln5),'0')AS bln5
			,IFNULL(SUM(dt.bln6),'0')AS bln6
			,IFNULL(SUM(dt.bln7),'0')AS bln7
			,IFNULL(SUM(dt.bln8),'0')AS bln8
			,IFNULL(SUM(dt.bln9),'0')AS bln9
			,IFNULL(SUM(dt.bln10),'0')AS bln10
			,IFNULL(SUM(dt.bln11),'0')AS bln11
			,IFNULL(SUM(dt.bln12),'0')AS bln12
		FROM (
			SELECT  MONTH(s.tgl)AS bln, st.kota_id, SUM(s.totalharga) AS omset
				,CASE WHEN MONTH(s.tgl)=1 THEN SUM(s.totalharga) END AS bln1
				,CASE WHEN MONTH(s.tgl)=2 THEN SUM(s.totalharga) END AS bln2
				,CASE WHEN MONTH(s.tgl)=3 THEN SUM(s.totalharga) END AS bln3
				,CASE WHEN MONTH(s.tgl)=4 THEN SUM(s.totalharga) END AS bln4
				,CASE WHEN MONTH(s.tgl)=5 THEN SUM(s.totalharga) END AS bln5
				,CASE WHEN MONTH(s.tgl)=6 THEN SUM(s.totalharga) END AS bln6
				,CASE WHEN MONTH(s.tgl)=7 THEN SUM(s.totalharga) END AS bln7
				,CASE WHEN MONTH(s.tgl)=8 THEN SUM(s.totalharga) END AS bln8
				,CASE WHEN MONTH(s.tgl)=9 THEN SUM(s.totalharga) END AS bln9
				,CASE WHEN MONTH(s.tgl)=10 THEN SUM(s.totalharga) END AS bln10
				,CASE WHEN MONTH(s.tgl)=11 THEN SUM(s.totalharga) END AS bln11
				,CASE WHEN MONTH(s.tgl)=12 THEN SUM(s.totalharga) END AS bln12
			FROM so s
			LEFT JOIN stockiest st ON s.stockiest_id=st.id
			WHERE s.tgl BETWEEN (LAST_DAY('$fromdate'-INTERVAL 6 MONTH) + INTERVAL 1 DAY) AND LAST_DAY('$fromdate')
			AND st.kota_id<>0
			GROUP BY bln,st.kota_id
		)AS dt
		GROUP BY dt.kota_id
	)AS st ON k.id=st.kota_id
	LEFT JOIN(
		SELECT dt.kota_id
			,IFNULL(SUM(dt.bln1),'0')AS bln1
			,IFNULL(SUM(dt.bln2),'0')AS bln2
			,IFNULL(SUM(dt.bln3),'0')AS bln3
			,IFNULL(SUM(dt.bln4),'0')AS bln4
			,IFNULL(SUM(dt.bln5),'0')AS bln5
			,IFNULL(SUM(dt.bln6),'0')AS bln6
			,IFNULL(SUM(dt.bln7),'0')AS bln7
			,IFNULL(SUM(dt.bln8),'0')AS bln8
			,IFNULL(SUM(dt.bln9),'0')AS bln9
			,IFNULL(SUM(dt.bln10),'0')AS bln10
			,IFNULL(SUM(dt.bln11),'0')AS bln11
			,IFNULL(SUM(dt.bln12),'0')AS bln12
		FROM (
			SELECT  MONTH(s.tgl)AS bln, m.kota_id, SUM(s.totalharga) AS omset
				,CASE WHEN MONTH(s.tgl)=1 THEN SUM(s.totalharga) END AS bln1
				,CASE WHEN MONTH(s.tgl)=2 THEN SUM(s.totalharga) END AS bln2
				,CASE WHEN MONTH(s.tgl)=3 THEN SUM(s.totalharga) END AS bln3
				,CASE WHEN MONTH(s.tgl)=4 THEN SUM(s.totalharga) END AS bln4
				,CASE WHEN MONTH(s.tgl)=5 THEN SUM(s.totalharga) END AS bln5
				,CASE WHEN MONTH(s.tgl)=6 THEN SUM(s.totalharga) END AS bln6
				,CASE WHEN MONTH(s.tgl)=7 THEN SUM(s.totalharga) END AS bln7
				,CASE WHEN MONTH(s.tgl)=8 THEN SUM(s.totalharga) END AS bln8
				,CASE WHEN MONTH(s.tgl)=9 THEN SUM(s.totalharga) END AS bln9
				,CASE WHEN MONTH(s.tgl)=10 THEN SUM(s.totalharga) END AS bln10
				,CASE WHEN MONTH(s.tgl)=11 THEN SUM(s.totalharga) END AS bln11
				,CASE WHEN MONTH(s.tgl)=12 THEN SUM(s.totalharga) END AS bln12
			FROM so s
			LEFT JOIN stockiest st ON s.stockiest_id=st.id
			LEFT JOIN member m ON s.member_id=m.id
			WHERE st.kota_id=0 AND s.tgl BETWEEN (LAST_DAY('$fromdate'-INTERVAL 6 MONTH) + INTERVAL 1 DAY) AND LAST_DAY('$fromdate')
			GROUP BY bln,m.kota_id
		)AS dt
		GROUP BY dt.kota_id
	)AS u ON k.id=u.kota_id
	GROUP BY k.id, st.oms
)AS dt
WHERE dt.total>0
ORDER BY oms, id
		";
		
		$qry = $this->db->query($query);
		
		// echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	/* End created by Boby 2012-08-06 */
	
	/* Created by Boby 2012-09-28 */
	public function omzet_rtr($fromdate,$todate)
    {
        $data = array();$i=0;
		$qry = $this->db->query("		
			SELECT pin.id, pin.tgl, s.no_stc, m.nama, pin.totalharga, pin.totalpv
			FROM retur_titipan pin
			LEFT JOIN member m ON pin.stockiest_id = m.id
			LEFT JOIN stockiest s ON pin.stockiest_id = s.id
			WHERE tgl BETWEEN '$fromdate' AND '$todate'
		");
		
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$i+=1;
				$row['i'] = $i;
				$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
	public function sum_omzet_rtr($fromdate,$todate){
        $data = array();
		$qry = $this->db->query("		
			SELECT SUM(pin.totalharga)AS totalhrg, SUM(pin.totalpv)AS totalpv
			FROM retur_titipan pin
			LEFT JOIN member m ON pin.stockiest_id = m.id
			LEFT JOIN stockiest s ON pin.stockiest_id = s.id
			WHERE tgl BETWEEN '$fromdate' AND '$todate'
		");
        if($qry->num_rows()>0){
            $data = $qry->row_array();
        }
        $qry->free_result();
        return $data;
    }
	/* End created by Boby 2012-09-28 */
	
	// Start ASP 20150709 
	public function omset_ro_gross_($fromdate,$todate)
    {
		$flag = 1;
		$q = "	
		SELECT 
			id, nama
			-- , FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			-- , ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, ((jml + jmlsc + 0) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
			, (total + totsc + 0) + totretur AS totalnominal1_
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama -- , dta.harga AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
			FROM(
				SELECT i.id, i.`name` -- , sod.harga
				FROM item i
				-- LEFT JOIN so_d sod ON i.id = sod.item_id
				-- GROUP BY sod.item_id -- , sod.harga
			)AS dta
			LEFT JOIN(
				SELECT sod.item_id, SUM(qty)AS jml, sod.harga AS harga";
		if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM so_d sod
				LEFT JOIN so ON sod.so_id = so.id
				WHERE 
					so.tgl BETWEEN '$fromdate' AND '$todate'
					AND so.stockiest_id = '0'
				GROUP BY item_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT rod.item_id, SUM(qty)AS jml, rod.harga AS harga";
		//if($flag==0){$q.="	, SUM(rod.jmlharga_)AS total";}else{$q.="	, SUM(1*rod.jmlharga_)AS total";}
		if($flag==0){$q.="	, SUM((rod.harga_*qty))AS total";}else{$q.="	, SUM(1*(rod.harga_*qty))AS total";}
		$q.="	FROM ro_d rod
				LEFT JOIN ro ON rod.ro_id = ro.id
				WHERE 
					ro.`date` BETWEEN '$fromdate' AND '$todate'
					AND ro.stockiest_id = '0'
				GROUP BY item_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT ptd.item_id, SUM(qty) AS jml, ptd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM pinjaman_titipan_d ptd
				LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
				WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY ptd.item_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
			LEFT JOIN(
				SELECT rtd.item_id, SUM(qty) AS jml, rtd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM retur_titipan_d rtd
				LEFT JOIN retur_titipan rt ON rtd.retur_titipan_id = rt.id
				WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY rtd.item_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				GROUP BY ncd.item_id
			)AS dtf ON dta.id = dtf.item_id
		)AS newdata
		HAVING totalnominal1_ <> 0 OR jmlnc <> 0
		order by id,nama
		";
        $data = array();
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }

	// End ASP 20150709 
	
	// Start ASP 20180810 
	public function omset_ro_gross_new_($fromdate,$todate)
    {
		$flag = 1;
		$q = "	
		SELECT 
			id, nama
			-- , FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			-- adj stock
			, jmladjstock, FORMAT(totadjstock,0)AS totadjstock, totadjstock AS totadjstock1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			-- , ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, ((jml + jmlsc + 0) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
			, (total + totsc + 0) + totretur AS totalnominal1_
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama -- , dta.harga AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtg.jml,0) AS jmladjstock, IFNULL(dtg.total,0) AS totadjstock
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
			FROM(
				SELECT i.id, i.`name` -- , sod.harga
				FROM item i
				-- LEFT JOIN so_d sod ON i.id = sod.item_id
				-- GROUP BY sod.item_id -- , sod.harga
			)AS dta
			LEFT JOIN(
				SELECT sod.item_id, SUM(qty)AS jml, sod.harga AS harga";
		if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM so_d sod
				LEFT JOIN so ON sod.so_id = so.id
				WHERE 
					so.tgl BETWEEN '$fromdate' AND '$todate'
					AND so.stockiest_id = '0'
				GROUP BY item_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT rod.item_id, SUM(qty)AS jml, rod.harga AS harga";
		//if($flag==0){$q.="	, SUM(rod.jmlharga_)AS total";}else{$q.="	, SUM(1*rod.jmlharga_)AS total";}
		if($flag==0){$q.="	, SUM((rod.harga_*qty))AS total";}else{$q.="	, SUM(1*(rod.harga_*qty))AS total";}
		$q.="	FROM ro_d rod
				LEFT JOIN ro ON rod.ro_id = ro.id
				WHERE 
					ro.`date` BETWEEN '$fromdate' AND '$todate'
					AND ro.stockiest_id = '0'
				GROUP BY item_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT ptd.item_id, SUM(qty) AS jml, ptd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM pinjaman_titipan_d ptd
				LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
				WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY ptd.item_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
			LEFT JOIN(
				SELECT rtd.item_id, SUM(qty) AS jml, rtd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM retur_titipan_d rtd
				LEFT JOIN retur_titipan rt ON rtd.retur_titipan_id = rt.id
				WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY rtd.item_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				-- AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%')
				AND (
					CASE WHEN nc.`date` >= '2020-05-01' AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%' AND remark NOT LIKE 'Free Of SO%' AND remark NOT LIKE 'CFSO no.%' ) THEN 1
						 WHEN nc.`date` < '2020-05-01' AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%' ) THEN 2
					ELSE 0 END
				) IN (1,2)
				GROUP BY ncd.item_id
			)AS dtf ON dta.id = dtf.item_id
			-- adj free RO
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				-- AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%')
				AND (
					CASE WHEN nc.`date` >= '2020-05-01' AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%' OR remark LIKE 'Free Of SO%' OR remark LIKE 'CFSO no.%' ) THEN 1
						 WHEN nc.`date` < '2020-05-01' AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%' ) THEN 2
					ELSE 0 END
				) IN (1,2)
				GROUP BY ncd.item_id
			)AS dtg ON dta.id = dtg.item_id
		)AS newdata
		HAVING totalnominal1_ <> 0 OR jmlnc <> 0 OR jmladjstock <> 0
		order by id,nama
		";
        $data = array();
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }

	// End ASP 20180810 
	
	// Start ASP 20150929 
	public function omset_ro_product_gross_($fromdate,$todate)
    {
		$flag = 1;
		$q = "	
		SELECT 
			IFNULL(ig.id,99) as group_id
			, CASE when ig.id is not null THEN ig.name ELSE 'Package' END as group_name
			, newdata.id, newdata.nama
			-- , FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			-- , ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, ((jml + jmlsc + 0) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
			, (total + totsc + 0) + totretur AS totalnominal1_
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama -- , dta.harga AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
			FROM(
				SELECT i.id, i.`name` -- , sod.harga
				FROM item i
				-- LEFT JOIN so_d sod ON i.id = sod.item_id
				-- GROUP BY sod.item_id -- , sod.harga
			)AS dta
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct (so.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.jmlharga END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
					FROM so_d sod
					LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga = rp.package_price
					LEFT JOIN so ON sod.so_id = so.id
					WHERE 
						so.tgl BETWEEN '$fromdate' AND '$todate'
						AND so.stockiest_id = '0'
				) as dt
				GROUP BY itemo_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(rod.jmlharga_)AS total";}else{$q.="	, SUM(1*rod.jmlharga_)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct(ro.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE rod.jmlharga_ END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga_*rod.qty) END AS total
					FROM ro_d rod
					LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga_ = rp.package_price
					LEFT JOIN ro ON rod.ro_id = ro.id
					WHERE 
						ro.`date` BETWEEN '$fromdate' AND '$todate'
						AND ro.stockiest_id = '0'
				) as dt
				GROUP BY itemo_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ptd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE ptd.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*ptd.qty ELSE ptd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*ptd.qty ELSE ptd.jmlharga END AS total
					FROM pinjaman_titipan_d ptd
					LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
					LEFT JOIN reff_package rp ON ptd.item_id = rp.package_id AND ptd.harga = rp.package_price
					WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
				) AS dt
				GROUP BY itemo_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- DISTINCT (rt.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
					FROM retur_titipan_d rtd
					LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
					LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
					WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
				) as dt
				GROUP BY itemo_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT itemo_id AS item_id, FLOOR(SUM(qty)) AS jml, hargao AS harga, SUM(total) AS total
				FROM (
					SELECT
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
					FROM ncm_d ncd
					LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
					LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
					WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				) AS dt	
				GROUP BY itemo_id
			)AS dtf ON dta.id = dtf.item_id
		)AS newdata
		LEFT JOIN item_to_item_group iig on newdata.id = iig.id
		LEFT JOIN item_group ig on iig.group_id = ig.id
		HAVING totalnominal1_ <> 0 OR jmlnc <> 0
		order by group_id, newdata.id,newdata.nama
		";
        $data = array();
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }

	// End ASP 20150929 
	// Start ASP 20161010 
	public function omset_ro_product_gross_new_($fromdate,$todate)
    {
		$flag = 1;
		$q = "	
		SELECT 
			IFNULL(ig.id,99) as group_id
			, CASE when ig.id is not null THEN ig.name ELSE 'Package' END as group_name
			, newdata.id, newdata.nama
			-- , FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			-- adj stock
			, jmladjstock, FORMAT(totadjstock,0)AS totadjstock, totadjstock AS totadjstock1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			-- , ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, ((jml + jmlsc + 0) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
			, (total + totsc + 0) + totretur AS totalnominal1_
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama -- , dta.harga AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtg.jml,0) AS jmladjstock, IFNULL(dtg.total,0) AS totadjstock
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
			FROM(
				SELECT i.id, i.`name` -- , sod.harga
				FROM item i
				-- LEFT JOIN so_d sod ON i.id = sod.item_id
				-- GROUP BY sod.item_id -- , sod.harga
			)AS dta
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct (so.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.jmlharga END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
					FROM so_d sod
					LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga = rp.package_price
					LEFT JOIN so ON sod.so_id = so.id
					WHERE 
						so.tgl BETWEEN '$fromdate' AND '$todate'
						AND so.stockiest_id = '0'
				) as dt
				GROUP BY itemo_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(rod.jmlharga_)AS total";}else{$q.="	, SUM(1*rod.jmlharga_)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct(ro.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE rod.jmlharga_ END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga_*rod.qty) END AS total
					FROM ro_d rod
					LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga_ = rp.package_price
					LEFT JOIN ro ON rod.ro_id = ro.id
					WHERE 
						ro.`date` BETWEEN '$fromdate' AND '$todate'
						AND ro.stockiest_id = '0'
				) as dt
				GROUP BY itemo_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ptd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE ptd.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*ptd.qty ELSE ptd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*ptd.qty ELSE ptd.jmlharga END AS total
					FROM pinjaman_titipan_d ptd
					LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
					LEFT JOIN reff_package rp ON ptd.item_id = rp.package_id AND ptd.harga = rp.package_price
					WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
				) AS dt
				GROUP BY itemo_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- DISTINCT (rt.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
					FROM retur_titipan_d rtd
					LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
					LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
					WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
				) as dt
				GROUP BY itemo_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT itemo_id AS item_id, FLOOR(SUM(qty)) AS jml, hargao AS harga, SUM(total) AS total
				FROM (
					SELECT
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
					FROM ncm_d ncd
					LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
					LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
					WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
					AND (remark NOT LIKE 'Free Of RO%'
					AND remark NOT LIKE 'CFRO no.%')
				) AS dt	
				GROUP BY itemo_id
			)AS dtf ON dta.id = dtf.item_id
			-- adj free RO
			LEFT JOIN(
				SELECT itemo_id AS item_id, FLOOR(SUM(qty)) AS jml, hargao AS harga, SUM(total) AS total
				FROM (
					SELECT
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
					FROM ncm_d ncd
					LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
					LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
					WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
					AND (remark LIKE 'Free Of RO%'
					OR remark LIKE 'CFRO no.%')
				) AS dt	
				GROUP BY itemo_id
			)AS dtg ON dta.id = dtg.item_id
		)AS newdata
		LEFT JOIN item_to_item_group iig on newdata.id = iig.id
		LEFT JOIN item_group ig on iig.group_id = ig.id
		HAVING totalnominal1_ <> 0 OR jmlnc <> 0 OR jmladjstock <> 0
		order by group_id, newdata.id,newdata.nama
		";
        $data = array();
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }

	// End ASP 20161010 

	// Start ASP 20180901 
	public function omset_ro_product_gross_new2_($fromdate,$todate)
    {
		$flag = 1;
		$q = "	
		SELECT 
			IFNULL(ig.id,99) as group_id
			, CASE when ig.id is not null THEN ig.name ELSE 'Package' END as group_name
			, newdata.id, newdata.nama
			-- , FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			-- adj stock
			, jmladjstock, FORMAT(totadjstock,0)AS totadjstock, totadjstock AS totadjstock1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			-- , ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, ((jml + jmlsc + 0) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
			, (total + totsc + 0) + totretur AS totalnominal1_
			-- topup event
			, jml_topup, FORMAT(total_topup,0)AS total_topup, total_topup as total_topup1
			, jml_event, FORMAT(total_event,0)AS total_event, total_event as total_event1
			, ((jml + jmlsc + 0 + jml_topup + jml_event) - (jmlretur)) AS totalqty_all
			, FORMAT((total + totsc + 0 + total_topup + total_event) - (totretur),0) AS totalnominal_all
			, (total + totsc + 0 + total_topup + total_event) - (totretur) AS totalnominal_all1
			, (total + totsc + 0 + total_topup + total_event) + (totretur) AS totalnominal_all1_
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama -- , dta.harga AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtg.jml,0) AS jmladjstock, IFNULL(dtg.total,0) AS totadjstock
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
				-- topup event
				, (IFNULL(dtb_topup.jml,0) + IFNULL(dtc_topup.jml,0) - IFNULL(dte_topup.jml,0)) AS jml_topup
				, (IFNULL(dtb_topup.total,0) + IFNULL(dtc_topup.total,0) - IFNULL(dte_topup.total,0)) AS total_topup
				, (IFNULL(dtb_event.jml,0) + IFNULL(dtc_event.jml,0) - IFNULL(dte_event.jml,0)) AS jml_event
				, (IFNULL(dtb_event.total,0) + IFNULL(dtc_event.total,0) - IFNULL(dte_event.total,0)) AS total_event
			FROM(
				SELECT i.id, i.`name` -- , sod.harga
				FROM item i
				-- LEFT JOIN so_d sod ON i.id = sod.item_id
				-- GROUP BY sod.item_id -- , sod.harga
			)AS dta
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct (so.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.jmlharga END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
					FROM so_d sod
					LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga = rp.package_price
					LEFT JOIN so ON sod.so_id = so.id
					WHERE 
						so.tgl BETWEEN '$fromdate' AND '$todate'
						AND so.stockiest_id = '0'
						AND sod.item_id NOT IN (SELECT id FROM item_topup)
						AND sod.item_id NOT IN (SELECT id FROM item_event)
				) as dt
				GROUP BY itemo_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct (so.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.jmlharga END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
					FROM so_d sod
					LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga = rp.package_price
					LEFT JOIN so ON sod.so_id = so.id
					WHERE 
						so.tgl BETWEEN '$fromdate' AND '$todate'
						AND so.stockiest_id = '0'
						AND sod.item_id IN (SELECT id FROM item_topup)
				) as dt
				GROUP BY itemo_id -- , sod.harga
			)AS dtb_topup ON dta.id = dtb_topup.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct (so.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.jmlharga END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
					FROM so_d sod
					LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga = rp.package_price
					LEFT JOIN so ON sod.so_id = so.id
					WHERE 
						so.tgl BETWEEN '$fromdate' AND '$todate'
						AND so.stockiest_id = '0'
						AND sod.item_id IN (SELECT id FROM item_event)
				) as dt
				GROUP BY itemo_id -- , sod.harga
			)AS dtb_event ON dta.id = dtb_event.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(rod.jmlharga_)AS total";}else{$q.="	, SUM(1*rod.jmlharga_)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct(ro.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE rod.jmlharga_ END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga_*rod.qty) END AS total
					FROM ro_d rod
					LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga_ = rp.package_price
					LEFT JOIN ro ON rod.ro_id = ro.id
					WHERE 
						ro.`date` BETWEEN '$fromdate' AND '$todate'
						AND ro.stockiest_id = '0'
						AND rod.item_id NOT IN (SELECT id FROM item_topup)
						AND rod.item_id NOT IN (SELECT id FROM item_event)
				) as dt
				GROUP BY itemo_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(rod.jmlharga_)AS total";}else{$q.="	, SUM(1*rod.jmlharga_)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct(ro.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE rod.jmlharga_ END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga_*rod.qty) END AS total
					FROM ro_d rod
					LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga_ = rp.package_price
					LEFT JOIN ro ON rod.ro_id = ro.id
					WHERE 
						ro.`date` BETWEEN '$fromdate' AND '$todate'
						AND ro.stockiest_id = '0'
						AND rod.item_id IN (SELECT id FROM item_topup)
				) as dt
				GROUP BY itemo_id -- , rod.harga
			)AS dtc_topup ON dta.id = dtc_topup.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(rod.jmlharga_)AS total";}else{$q.="	, SUM(1*rod.jmlharga_)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct(ro.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE rod.jmlharga_ END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga_*rod.qty) END AS total
					FROM ro_d rod
					LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga_ = rp.package_price
					LEFT JOIN ro ON rod.ro_id = ro.id
					WHERE 
						ro.`date` BETWEEN '$fromdate' AND '$todate'
						AND ro.stockiest_id = '0'
						AND rod.item_id IN (SELECT id FROM item_event)
				) as dt
				GROUP BY itemo_id -- , rod.harga
			)AS dtc_event ON dta.id = dtc_event.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ptd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE ptd.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*ptd.qty ELSE ptd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*ptd.qty ELSE ptd.jmlharga END AS total
					FROM pinjaman_titipan_d ptd
					LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
					LEFT JOIN reff_package rp ON ptd.item_id = rp.package_id AND ptd.harga = rp.package_price
					WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
				) AS dt
				GROUP BY itemo_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- DISTINCT (rt.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
					FROM retur_titipan_d rtd
					LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
					LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
					WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
					AND rtd.item_id NOT IN (SELECT id FROM item_topup)
					AND rtd.item_id NOT IN (SELECT id FROM item_event)
				) as dt
				GROUP BY itemo_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- DISTINCT (rt.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
					FROM retur_titipan_d rtd
					LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
					LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
					WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
					AND rtd.item_id IN (SELECT id FROM item_topup)
				) as dt
				GROUP BY itemo_id -- , rtd.harga
			)AS dte_topup ON dta.id = dte_topup.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- DISTINCT (rt.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
					FROM retur_titipan_d rtd
					LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
					LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
					WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
					AND rtd.item_id IN (SELECT id FROM item_event)
				) as dt
				GROUP BY itemo_id -- , rtd.harga
			)AS dte_event ON dta.id = dte_event.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT itemo_id AS item_id, FLOOR(SUM(qty)) AS jml, hargao AS harga, SUM(total) AS total
				FROM (
					SELECT
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
					FROM ncm_d ncd
					LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
					LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
					WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
					-- AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%')
					AND (
						CASE WHEN nc.`date` >= '2020-05-01' AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%' AND remark NOT LIKE 'Free Of SO%' AND remark NOT LIKE 'CFSO no.%' ) THEN 1
						     WHEN nc.`date` < '2020-05-01' AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%' ) THEN 2
						ELSE 0 END
					) IN (1,2)
				) AS dt	
				GROUP BY itemo_id
			)AS dtf ON dta.id = dtf.item_id
			-- adj free RO
			LEFT JOIN(
				SELECT itemo_id AS item_id, FLOOR(SUM(qty)) AS jml, hargao AS harga, SUM(total) AS total
				FROM (
					SELECT
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
					FROM ncm_d ncd
					LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
					LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
					WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
					-- AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%')
					AND (
						CASE WHEN nc.`date` >= '2020-05-01' AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%' OR remark LIKE 'Free Of SO%' OR remark LIKE 'CFSO no.%' ) THEN 1
						     WHEN nc.`date` < '2020-05-01' AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%' ) THEN 2
						ELSE 0 END
					) IN (1,2)
				) AS dt	
				GROUP BY itemo_id
			)AS dtg ON dta.id = dtg.item_id
		)AS newdata
		LEFT JOIN item_to_item_group iig on newdata.id = iig.id
		LEFT JOIN item_group ig on iig.group_id = ig.id
		HAVING totalnominal1_ <> 0 OR jmlnc <> 0 OR jmladjstock <> 0 OR totalnominal_all1_ <> 0 OR jml_topup <> 0 OR jml_event <> 0
		order by group_id, newdata.id,newdata.nama
		";
        $data = array();
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }

	// End ASP 20180901 

    // START ASP 20190927
    public function omset_ro_product_nett_new2_($fromdate,$todate)
    {
        $flag = 1;
        $q = "  
        SELECT 
            IFNULL(ig.id,99) as group_id
            , CASE when ig.id is not null THEN ig.name ELSE 'Package' END as group_name
            , newdata.id, newdata.nama
            -- , FORMAT(harga,0)AS harga, harga as harga1
            , jml, FORMAT(total,0)AS total, total as total1
            , jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
            -- adj stock
            , jmladjstock, FORMAT(totadjstock,0)AS totadjstock, totadjstock AS totadjstock1
            , jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
            , jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
            -- , ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
            , ((jml + jmlsc + 0) - jmlretur) AS totalqty
            , FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
            , (total + totsc + 0) - totretur AS totalnominal1
            , (total + totsc + 0) + totretur AS totalnominal1_
            -- topup event
            , jml_topup, FORMAT(total_topup,0)AS total_topup, total_topup as total_topup1
            , jml_event, FORMAT(total_event,0)AS total_event, total_event as total_event1
            , ((jml + jmlsc + 0 + jml_topup + jml_event) - (jmlretur)) AS totalqty_all
            , FORMAT((total + totsc + 0 + total_topup + total_event) - (totretur),0) AS totalnominal_all
            , (total + totsc + 0 + total_topup + total_event) - (totretur) AS totalnominal_all1
            , (total + totsc + 0 + total_topup + total_event) + (totretur) AS totalnominal_all1_
        FROM(
            SELECT 
                dta.id, dta.`name` AS nama -- , dta.harga AS harga
                , (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
                , (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
                , IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
                , IFNULL(dtg.jml,0) AS jmladjstock, IFNULL(dtg.total,0) AS totadjstock
                , IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
                , IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
                -- topup event
                , (IFNULL(dtb_topup.jml,0) + IFNULL(dtc_topup.jml,0) - IFNULL(dte_topup.jml,0)) AS jml_topup
                , (IFNULL(dtb_topup.total,0) + IFNULL(dtc_topup.total,0) - IFNULL(dte_topup.total,0)) AS total_topup
                , (IFNULL(dtb_event.jml,0) + IFNULL(dtc_event.jml,0) - IFNULL(dte_event.jml,0)) AS jml_event
                , (IFNULL(dtb_event.total,0) + IFNULL(dtc_event.total,0) - IFNULL(dte_event.total,0)) AS total_event
            FROM(
                SELECT i.id, i.`name` -- , sod.harga
                FROM item i
                -- LEFT JOIN so_d sod ON i.id = sod.item_id
                -- GROUP BY sod.item_id -- , sod.harga
            )AS dta
            LEFT JOIN(
                SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
        //if($flag==0){$q.="    , SUM(sod.jmlharga)AS total";}else{$q.="    , SUM(1*sod.jmlharga)AS total";}
        $q.="   FROM (
                    SELECT
                    -- distinct (so.id),
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
                    -- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.jmlharga END AS total
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
                    FROM so_d sod
                    LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga = rp.package_price
                    LEFT JOIN so ON sod.so_id = so.id
                    WHERE 
                        so.tgl BETWEEN '$fromdate' AND '$todate'
                        AND so.stockiest_id = '0'
                        AND sod.item_id NOT IN (SELECT id FROM item_topup)
                        AND sod.item_id NOT IN (SELECT id FROM item_event)
                ) as dt
                GROUP BY itemo_id -- , sod.harga
            )AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
            LEFT JOIN(
                SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
        //if($flag==0){$q.="    , SUM(sod.jmlharga)AS total";}else{$q.="    , SUM(1*sod.jmlharga)AS total";}
        $q.="   FROM (
                    SELECT
                    -- distinct (so.id),
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
                    -- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.jmlharga END AS total
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
                    FROM so_d sod
                    LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga = rp.package_price
                    LEFT JOIN so ON sod.so_id = so.id
                    WHERE 
                        so.tgl BETWEEN '$fromdate' AND '$todate'
                        AND so.stockiest_id = '0'
                        AND sod.item_id IN (SELECT id FROM item_topup)
                ) as dt
                GROUP BY itemo_id -- , sod.harga
            )AS dtb_topup ON dta.id = dtb_topup.item_id  -- AND dta.harga = dtb.harga
            LEFT JOIN(
                SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
        //if($flag==0){$q.="    , SUM(sod.jmlharga)AS total";}else{$q.="    , SUM(1*sod.jmlharga)AS total";}
        $q.="   FROM (
                    SELECT
                    -- distinct (so.id),
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
                    -- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.jmlharga END AS total
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
                    FROM so_d sod
                    LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga = rp.package_price
                    LEFT JOIN so ON sod.so_id = so.id
                    WHERE 
                        so.tgl BETWEEN '$fromdate' AND '$todate'
                        AND so.stockiest_id = '0'
                        AND sod.item_id IN (SELECT id FROM item_event)
                ) as dt
                GROUP BY itemo_id -- , sod.harga
            )AS dtb_event ON dta.id = dtb_event.item_id  -- AND dta.harga = dtb.harga
            LEFT JOIN(
                SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
        //if($flag==0){$q.="    , SUM(rod.jmlharga_)AS total";}else{$q.="   , SUM(1*rod.jmlharga_)AS total";}
        $q.="   FROM (
                    SELECT
                    -- distinct(ro.id),
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
                    -- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE rod.jmlharga_ END AS total
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga*rod.qty) END AS total
                    FROM ro_d rod
                    LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga = rp.package_price
                    LEFT JOIN ro ON rod.ro_id = ro.id
                    WHERE 
                        ro.`date` BETWEEN '$fromdate' AND '$todate'
                        AND ro.stockiest_id = '0'
                        AND rod.item_id NOT IN (SELECT id FROM item_topup)
                        AND rod.item_id NOT IN (SELECT id FROM item_event)
                ) as dt
                GROUP BY itemo_id -- , rod.harga
            )AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
            LEFT JOIN(
                SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
        //if($flag==0){$q.="    , SUM(rod.jmlharga_)AS total";}else{$q.="   , SUM(1*rod.jmlharga_)AS total";}
        $q.="   FROM (
                    SELECT
                    -- distinct(ro.id),
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
                    -- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE rod.jmlharga_ END AS total
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga*rod.qty) END AS total
                    FROM ro_d rod
                    LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga = rp.package_price
                    LEFT JOIN ro ON rod.ro_id = ro.id
                    WHERE 
                        ro.`date` BETWEEN '$fromdate' AND '$todate'
                        AND ro.stockiest_id = '0'
                        AND rod.item_id IN (SELECT id FROM item_topup)
                ) as dt
                GROUP BY itemo_id -- , rod.harga
            )AS dtc_topup ON dta.id = dtc_topup.item_id  -- AND dta.harga = dtc.harga
            LEFT JOIN(
                SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
        //if($flag==0){$q.="    , SUM(rod.jmlharga_)AS total";}else{$q.="   , SUM(1*rod.jmlharga_)AS total";}
        $q.="   FROM (
                    SELECT
                    -- distinct(ro.id),
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
                    -- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE rod.jmlharga_ END AS total
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga*rod.qty) END AS total
                    FROM ro_d rod
                    LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga = rp.package_price
                    LEFT JOIN ro ON rod.ro_id = ro.id
                    WHERE 
                        ro.`date` BETWEEN '$fromdate' AND '$todate'
                        AND ro.stockiest_id = '0'
                        AND rod.item_id IN (SELECT id FROM item_event)
                ) as dt
                GROUP BY itemo_id -- , rod.harga
            )AS dtc_event ON dta.id = dtc_event.item_id  -- AND dta.harga = dtc.harga
            LEFT JOIN(
                SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
        //if($flag==0){$q.="    , SUM(jmlharga)AS total";}else{$q.="    , SUM(1*jmlharga)AS total";}
        $q.="   FROM (
                    SELECT
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ptd.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE ptd.harga END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*ptd.qty ELSE ptd.qty END AS qty
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*ptd.qty ELSE ptd.jmlharga END AS total
                    FROM pinjaman_titipan_d ptd
                    LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
                    LEFT JOIN reff_package rp ON ptd.item_id = rp.package_id AND ptd.harga = rp.package_price
                    WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
                ) AS dt
                GROUP BY itemo_id -- , ptd.harga
            )AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
            LEFT JOIN(
                SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
        //if($flag==0){$q.="    , SUM(jmlharga)AS total";}else{$q.="    , SUM(1*jmlharga)AS total";}
        $q.="   FROM (
                    SELECT
                    -- DISTINCT (rt.id),
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
                    FROM retur_titipan_d rtd
                    LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
                    LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
                    WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
                    AND rtd.item_id NOT IN (SELECT id FROM item_topup)
                    AND rtd.item_id NOT IN (SELECT id FROM item_event)
                ) as dt
                GROUP BY itemo_id -- , rtd.harga
            )AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
            LEFT JOIN(
                SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
        //if($flag==0){$q.="    , SUM(jmlharga)AS total";}else{$q.="    , SUM(1*jmlharga)AS total";}
        $q.="   FROM (
                    SELECT
                    -- DISTINCT (rt.id),
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
                    FROM retur_titipan_d rtd
                    LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
                    LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
                    WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
                    AND rtd.item_id IN (SELECT id FROM item_topup)
                ) as dt
                GROUP BY itemo_id -- , rtd.harga
            )AS dte_topup ON dta.id = dte_topup.item_id  -- AND dta.harga = dte.harga
            LEFT JOIN(
                SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
        //if($flag==0){$q.="    , SUM(jmlharga)AS total";}else{$q.="    , SUM(1*jmlharga)AS total";}
        $q.="   FROM (
                    SELECT
                    -- DISTINCT (rt.id),
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
                    FROM retur_titipan_d rtd
                    LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
                    LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
                    WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
                    AND rtd.item_id IN (SELECT id FROM item_event)
                ) as dt
                GROUP BY itemo_id -- , rtd.harga
            )AS dte_event ON dta.id = dte_event.item_id  -- AND dta.harga = dte.harga
            LEFT JOIN(
                SELECT itemo_id AS item_id, FLOOR(SUM(qty)) AS jml, hargao AS harga, SUM(total) AS total
                FROM (
                    SELECT
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
                    ,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
                    FROM ncm_d ncd
                    LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
                    LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
                    WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
                    AND (remark NOT LIKE 'Free Of RO%'
                    AND remark NOT LIKE 'CFRO no.%')
                ) AS dt 
                GROUP BY itemo_id
            )AS dtf ON dta.id = dtf.item_id
            -- adj free RO
            LEFT JOIN(
                SELECT itemo_id AS item_id, FLOOR(SUM(qty)) AS jml, hargao AS harga, SUM(total) AS total
                FROM (
                    SELECT
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
                    ,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
                    FROM ncm_d ncd
                    LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
                    LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
                    WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
                    AND (remark LIKE 'Free Of RO%'
                    OR remark LIKE 'CFRO no.%')
                ) AS dt 
                GROUP BY itemo_id
            )AS dtg ON dta.id = dtg.item_id
        )AS newdata
        LEFT JOIN item_to_item_group iig on newdata.id = iig.id
        LEFT JOIN item_group ig on iig.group_id = ig.id
        HAVING totalnominal1_ <> 0 OR jmlnc <> 0 OR jmladjstock <> 0 OR totalnominal_all1_ <> 0 OR jml_topup <> 0 OR jml_event <> 0
        order by group_id, newdata.id,newdata.nama
        ";
        $data = array();
        $qry = $this->db->query($q);
        //echo $this->db->last_query();
        if($qry->num_rows()>0){
            foreach($qry->result_array() as $row){
                    $data[]=$row;
            }
        }
        $qry->free_result();
        return $data;
    }

    //EOF ASP 20190927  

    public function getWhsName($id){
        $data = array();
        $q = $this->db->get_where('warehouse',array('id' => $id));
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[]=$row;    
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function omset_ro_new_wh($fromdate,$todate,$whsid)
    {
		$flag = 1;
		$q = "	
		SELECT 
			id, nama
			-- , FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			-- adj stock
			, jmladjstock, FORMAT(totadjstock,0)AS totadjstock, totadjstock AS totadjstock1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			-- , ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, ((jml + jmlsc + 0) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
			, (total + totsc + 0) + totretur AS totalnominal1_
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama -- , dta.harga AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtg.jml,0) AS jmladjstock, IFNULL(dtg.total,0) AS totadjstock
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
			FROM(
				SELECT i.id, i.`name` -- , sod.harga
				FROM item i
				-- LEFT JOIN so_d sod ON i.id = sod.item_id
				-- GROUP BY sod.item_id -- , sod.harga
			)AS dta
			LEFT JOIN(
				SELECT sod.item_id, SUM(qty)AS jml, sod.harga AS harga";
		if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM so_d sod
				LEFT JOIN so ON sod.so_id = so.id
				WHERE 
					so.tgl BETWEEN '$fromdate' AND '$todate'
					AND so.stockiest_id = '0'
					AND so.warehouse_id = ".$whsid."
				GROUP BY item_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT rod.item_id, SUM(qty)AS jml, rod.harga AS harga";
		if($flag==0){$q.="	, SUM(rod.jmlharga)AS total";}else{$q.="	, SUM(1*rod.jmlharga)AS total";}
		$q.="	FROM ro_d rod
				LEFT JOIN ro ON rod.ro_id = ro.id
				WHERE 
					ro.`date` BETWEEN '$fromdate' AND '$todate'
					AND ro.stockiest_id = '0'
					AND ro.warehouse_id = ".$whsid."
				GROUP BY item_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT ptd.item_id, SUM(qty) AS jml, ptd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM pinjaman_titipan_d ptd
				LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
				WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY ptd.item_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
			LEFT JOIN(
				SELECT rtd.item_id, SUM(qty) AS jml, rtd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM retur_titipan_d rtd
				LEFT JOIN retur_titipan rt ON rtd.retur_titipan_id = rt.id
				WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
				AND rt.warehouse_id = ".$whsid."
				GROUP BY rtd.item_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				AND remark  NOT LIKE 'Free Of RO%'
				AND nc.warehouse_id = ".$whsid."
				GROUP BY ncd.item_id
			)AS dtf ON dta.id = dtf.item_id
			-- adj free RO
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				AND remark  LIKE 'Free Of RO%'
				AND nc.warehouse_id = ".$whsid."
				GROUP BY ncd.item_id
			)AS dtg ON dta.id = dtg.item_id

		)AS newdata
		HAVING totalnominal1_ <> 0 OR jmlnc <> 0
		-- order by nama
		order by id, nama
		";
        $data = array();
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
	public function omset_ro_new_cluster($fromdate,$todate,$clusterid)
    {
		$flag = 1;
		$q = "	
		SELECT 
			id, nama
			-- , FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			-- adj stock
			, jmladjstock, FORMAT(totadjstock,0)AS totadjstock, totadjstock AS totadjstock1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			-- , ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, ((jml + jmlsc + 0) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
			, (total + totsc + 0) + totretur AS totalnominal1_
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama -- , dta.harga AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtg.jml,0) AS jmladjstock, IFNULL(dtg.total,0) AS totadjstock
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
			FROM(
				SELECT i.id, i.`name` -- , sod.harga
				FROM item i
				-- LEFT JOIN so_d sod ON i.id = sod.item_id
				-- GROUP BY sod.item_id -- , sod.harga
			)AS dta
			LEFT JOIN(
				SELECT sod.item_id, SUM(qty)AS jml, sod.harga AS harga";
		if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM so_d sod
				LEFT JOIN so ON sod.so_id = so.id
				LEFT JOIN member m ON so.member_id = m.id
				LEFT JOIN member_delivery md ON so.delivery_addr = md.id
				LEFT JOIN kota kmd ON md.kota = kmd.id
				LEFT JOIN propinsi_to_propinsi_cluster ppc ON kmd.propinsi_id = ppc.id
				LEFT JOIN kota km ON m.kota_id = km.id
				LEFT JOIN propinsi_to_propinsi_cluster ppcm ON km.propinsi_id = ppcm.id
				WHERE 
					so.tgl BETWEEN '$fromdate' AND '$todate'
					AND so.stockiest_id = '0'
					AND IF(so.delivery_addr > 0,ppc.cluster_id,ppcm.cluster_id) = ".$clusterid."
				GROUP BY item_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT rod.item_id, SUM(qty)AS jml, rod.harga AS harga";
		if($flag==0){$q.="	, SUM(rod.jmlharga)AS total";}else{$q.="	, SUM(1*rod.jmlharga)AS total";}
		$q.="	FROM ro_d rod
				LEFT JOIN ro ON rod.ro_id = ro.id
				LEFT JOIN stockiest s ON ro.member_id = s.id
				LEFT JOIN member m ON ro.member_id = m.id
				LEFT JOIN member_delivery md ON ro.deliv_addr = md.id
				LEFT JOIN kota kmd ON md.kota = kmd.id
				LEFT JOIN propinsi_to_propinsi_cluster ppc ON kmd.propinsi_id = ppc.id
				LEFT JOIN kota ks ON s.kota_id = ks.id
				LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ks.propinsi_id = ppcs.id
				LEFT JOIN kota km ON m.kota_id = km.id
				LEFT JOIN propinsi_to_propinsi_cluster ppcm ON km.propinsi_id = ppcm.id
				WHERE 
					ro.`date` BETWEEN '$fromdate' AND '$todate'
					AND ro.stockiest_id = '0'
					AND IF(ro.deliv_addr > 0,ppc.cluster_id,ppcm.cluster_id) = ".$clusterid."
				GROUP BY item_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT ptd.item_id, SUM(qty) AS jml, ptd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM pinjaman_titipan_d ptd
				LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
				WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY ptd.item_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
			LEFT JOIN(
				SELECT rtd.item_id, SUM(qty) AS jml, rtd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM retur_titipan_d rtd
				LEFT JOIN retur_titipan rt ON rtd.retur_titipan_id = rt.id
				LEFT JOIN stockiest s ON rt.stockiest_id = s.id
				LEFT JOIN member m ON rt.stockiest_id = m.id
				LEFT JOIN kota ks ON s.kota_id = ks.id
				LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ks.propinsi_id = ppcs.id
				LEFT JOIN kota km ON m.kota_id = km.id
				LEFT JOIN propinsi_to_propinsi_cluster ppcm ON km.propinsi_id = ppcm.id
				WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
				AND ppcm.cluster_id = ".$clusterid."
				GROUP BY rtd.item_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				AND remark  NOT LIKE 'Free Of RO%'
				AND 2 = ".$clusterid."
				GROUP BY ncd.item_id
			)AS dtf ON dta.id = dtf.item_id
			-- adj free RO
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				LEFT JOIN ro ON CONCAT('Free of RO no.',ro.id) = nc.remark
				LEFT JOIN stockiest s ON ro.member_id = s.id
				LEFT JOIN member m ON ro.member_id = m.id
				LEFT JOIN member_delivery md ON ro.deliv_addr = md.id
				LEFT JOIN kota kmd ON md.kota = kmd.id
				LEFT JOIN propinsi_to_propinsi_cluster ppc ON kmd.propinsi_id = ppc.id
				LEFT JOIN kota ks ON s.kota_id = ks.id
				LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ks.propinsi_id = ppcs.id
				LEFT JOIN kota km ON m.kota_id = km.id
				LEFT JOIN propinsi_to_propinsi_cluster ppcm ON km.propinsi_id = ppcm.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				AND nc.remark  LIKE 'Free Of RO%'
				AND IF(ro.deliv_addr > 0,ppc.cluster_id,ppcm.cluster_id) = ".$clusterid."
				GROUP BY ncd.item_id			
			)AS dtg ON dta.id = dtg.item_id

		)AS newdata
		HAVING totalnominal1_ <> 0 OR jmlnc <> 0
		-- order by nama
		order by id, nama
		";
        $data = array();
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
	public function omset_ro_new_city($fromdate,$todate,$cityid)
    {
		$flag = 1;
		$q = "	
		SELECT 
			id, nama
			-- , FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			-- adj stock
			, jmladjstock, FORMAT(totadjstock,0)AS totadjstock, totadjstock AS totadjstock1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			-- , ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, ((jml + jmlsc + 0) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
			, (total + totsc + 0) + totretur AS totalnominal1_
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama -- , dta.harga AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtg.jml,0) AS jmladjstock, IFNULL(dtg.total,0) AS totadjstock
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
			FROM(
				SELECT i.id, i.`name` -- , sod.harga
				FROM item i
				-- LEFT JOIN so_d sod ON i.id = sod.item_id
				-- GROUP BY sod.item_id -- , sod.harga
			)AS dta
			LEFT JOIN(
				SELECT sod.item_id, SUM(qty)AS jml, sod.harga AS harga";
		if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM so_d sod
				LEFT JOIN so ON sod.so_id = so.id
				LEFT JOIN member m ON so.member_id = m.id
				LEFT JOIN member_delivery md ON so.delivery_addr = md.id
				LEFT JOIN kota kmd ON md.kota = kmd.id
				LEFT JOIN propinsi_to_propinsi_cluster ppc ON kmd.propinsi_id = ppc.id
				LEFT JOIN kota km ON m.kota_id = km.id
				LEFT JOIN propinsi_to_propinsi_cluster ppcm ON km.propinsi_id = ppcm.id
				WHERE 
					so.tgl BETWEEN '$fromdate' AND '$todate'
					AND so.stockiest_id = '0'
					AND IF(so.delivery_addr > 0,md.kota,m.kota_id) = ".$cityid."
				GROUP BY item_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT rod.item_id, SUM(qty)AS jml, rod.harga AS harga";
		if($flag==0){$q.="	, SUM(rod.jmlharga)AS total";}else{$q.="	, SUM(1*rod.jmlharga)AS total";}
		$q.="	FROM ro_d rod
				LEFT JOIN ro ON rod.ro_id = ro.id
				LEFT JOIN stockiest s ON ro.member_id = s.id
				LEFT JOIN member m ON ro.member_id = m.id
				LEFT JOIN member_delivery md ON ro.deliv_addr = md.id
				LEFT JOIN kota kmd ON md.kota = kmd.id
				LEFT JOIN propinsi_to_propinsi_cluster ppc ON kmd.propinsi_id = ppc.id
				LEFT JOIN kota ks ON s.kota_id = ks.id
				LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ks.propinsi_id = ppcs.id
				LEFT JOIN kota km ON m.kota_id = km.id
				LEFT JOIN propinsi_to_propinsi_cluster ppcm ON km.propinsi_id = ppcm.id
				WHERE 
					ro.`date` BETWEEN '$fromdate' AND '$todate'
					AND ro.stockiest_id = '0'
					AND IF(ro.deliv_addr > 0,md.kota,m.kota_id) = ".$cityid."
				GROUP BY item_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT ptd.item_id, SUM(qty) AS jml, ptd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM pinjaman_titipan_d ptd
				LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
				WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
				GROUP BY ptd.item_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
			LEFT JOIN(
				SELECT rtd.item_id, SUM(qty) AS jml, rtd.harga";
		if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM retur_titipan_d rtd
				LEFT JOIN retur_titipan rt ON rtd.retur_titipan_id = rt.id
				LEFT JOIN stockiest s ON rt.stockiest_id = s.id
				LEFT JOIN member m ON rt.stockiest_id = m.id
				LEFT JOIN kota ks ON s.kota_id = ks.id
				LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ks.propinsi_id = ppcs.id
				LEFT JOIN kota km ON m.kota_id = km.id
				LEFT JOIN propinsi_to_propinsi_cluster ppcm ON km.propinsi_id = ppcm.id
				WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
				AND m.kota_id = ".$cityid."
				GROUP BY rtd.item_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				AND remark  NOT LIKE 'Free Of RO%'
				AND 233 = ".$cityid."
				GROUP BY ncd.item_id
			)AS dtf ON dta.id = dtf.item_id
			-- adj free RO
			LEFT JOIN(
				SELECT ncd.item_id, SUM(ncd.qty) AS jml, SUM(ncd.qty*ncd.hpp) AS total
				FROM ncm_d ncd
				LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
				LEFT JOIN ro ON CONCAT('Free of RO no.',ro.id) = nc.remark
				LEFT JOIN stockiest s ON ro.member_id = s.id
				LEFT JOIN member m ON ro.member_id = m.id
				LEFT JOIN member_delivery md ON ro.deliv_addr = md.id
				LEFT JOIN kota kmd ON md.kota = kmd.id
				LEFT JOIN propinsi_to_propinsi_cluster ppc ON kmd.propinsi_id = ppc.id
				LEFT JOIN kota ks ON s.kota_id = ks.id
				LEFT JOIN propinsi_to_propinsi_cluster ppcs ON ks.propinsi_id = ppcs.id
				LEFT JOIN kota km ON m.kota_id = km.id
				LEFT JOIN propinsi_to_propinsi_cluster ppcm ON km.propinsi_id = ppcm.id
				WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
				AND nc.remark  LIKE 'Free Of RO%'
				AND IF(ro.deliv_addr > 0,md.kota,m.kota_id) = ".$cityid."
				GROUP BY ncd.item_id			
			)AS dtg ON dta.id = dtg.item_id

		)AS newdata
		HAVING totalnominal1_ <> 0 OR jmlnc <> 0
		-- order by nama
		order by id, nama
		";
        $data = array();
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }
	
	// Start ASP 20180528 
	public function omset_ro_product_gross_new_wh_($fromdate,$todate,$whsid)
    {
		$flag = 1;
		$q = "	
		SELECT 
			IFNULL(ig.id,99) as group_id
			, CASE when ig.id is not null THEN ig.name ELSE 'Package' END as group_name
			, newdata.id, newdata.nama
			-- , FORMAT(harga,0)AS harga, harga as harga1
			, jml, FORMAT(total,0)AS total, total as total1
			, jmlnc, FORMAT(totnc,0)AS totnc, totnc as totnc1
			-- adj stock
			, jmladjstock, FORMAT(totadjstock,0)AS totadjstock, totadjstock AS totadjstock1
			, jmlsc, FORMAT(totsc,0)AS totsc, totsc as totsc1
			, jmlretur, FORMAT(totretur,0)AS totretur, totretur as totretur1
			-- , ((jml + jmlsc + jmlnc) - jmlretur) AS totalqty
			, ((jml + jmlsc + 0) - jmlretur) AS totalqty
			, FORMAT((total + totsc + 0) - totretur,0) AS totalnominal
			, (total + totsc + 0) - totretur AS totalnominal1
			, (total + totsc + 0) + totretur AS totalnominal1_
		FROM(
			SELECT 
				dta.id, dta.`name` AS nama -- , dta.harga AS harga
				, (IFNULL(dtb.jml,0) + IFNULL(dtc.jml,0)) AS jml
				, (IFNULL(dtb.total,0) + IFNULL(dtc.total,0)) AS total
				, IFNULL(dtf.jml,0) AS jmlnc, IFNULL(dtf.total,0) AS totnc
				, IFNULL(dtg.jml,0) AS jmladjstock, IFNULL(dtg.total,0) AS totadjstock
				, IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
				, IFNULL(dte.jml,0) AS jmlretur, IFNULL(dte.total,0) AS totretur
			FROM(
				SELECT i.id, i.`name` -- , sod.harga
				FROM item i
				-- LEFT JOIN so_d sod ON i.id = sod.item_id
				-- GROUP BY sod.item_id -- , sod.harga
			)AS dta
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(sod.jmlharga)AS total";}else{$q.="	, SUM(1*sod.jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct (so.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.jmlharga END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
					FROM so_d sod
					LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga = rp.package_price
					LEFT JOIN so ON sod.so_id = so.id
					WHERE 
						so.tgl BETWEEN '$fromdate' AND '$todate'
						AND so.stockiest_id = '0'
						AND so.warehouse_id = ".$whsid."
				) as dt
				GROUP BY itemo_id -- , sod.harga
			)AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(rod.jmlharga_)AS total";}else{$q.="	, SUM(1*rod.jmlharga_)AS total";}
		$q.="	FROM (
					SELECT
					-- distinct(ro.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
					-- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE rod.jmlharga_ END AS total
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga_*rod.qty) END AS total
					FROM ro_d rod
					LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga_ = rp.package_price
					LEFT JOIN ro ON rod.ro_id = ro.id
					WHERE 
						ro.`date` BETWEEN '$fromdate' AND '$todate'
						AND ro.stockiest_id = '0'
						AND ro.warehouse_id = ".$whsid."
				) as dt
				GROUP BY itemo_id -- , rod.harga
			)AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ptd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE ptd.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*ptd.qty ELSE ptd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*ptd.qty ELSE ptd.jmlharga END AS total
					FROM pinjaman_titipan_d ptd
					LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
					LEFT JOIN reff_package rp ON ptd.item_id = rp.package_id AND ptd.harga = rp.package_price
					WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
				) AS dt
				GROUP BY itemo_id -- , ptd.harga
			)AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
			LEFT JOIN(
				SELECT itemo_id as item_id, SUM(qty)AS jml, hargao AS harga, SUM(total) AS total";
		//if($flag==0){$q.="	, SUM(jmlharga)AS total";}else{$q.="	, SUM(1*jmlharga)AS total";}
		$q.="	FROM (
					SELECT
					-- DISTINCT (rt.id),
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
					FROM retur_titipan_d rtd
					LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
					LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
					WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
					AND rt.warehouse_id = ".$whsid."
				) as dt
				GROUP BY itemo_id -- , rtd.harga
			)AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
			LEFT JOIN(
				SELECT itemo_id AS item_id, FLOOR(SUM(qty)) AS jml, hargao AS harga, SUM(total) AS total
				FROM (
					SELECT
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
					FROM ncm_d ncd
					LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
					LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
					WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
					-- AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%')
					AND (
						CASE WHEN nc.`date` >= '2020-05-01' AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%' AND remark NOT LIKE 'Free Of SO%' AND remark NOT LIKE 'CFSO no.%' ) THEN 1
						     WHEN nc.`date` < '2020-05-01' AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%' ) THEN 2
						ELSE 0 END
					) IN (1,2)
					AND nc.warehouse_id = ".$whsid."
				) AS dt	
				GROUP BY itemo_id
			)AS dtf ON dta.id = dtf.item_id
			-- adj free RO
			LEFT JOIN(
				SELECT itemo_id AS item_id, FLOOR(SUM(qty)) AS jml, hargao AS harga, SUM(total) AS total
				FROM (
					SELECT
					CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
					,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
					,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
					FROM ncm_d ncd
					LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
					LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
					WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
					-- AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%')
					AND (
						CASE WHEN nc.`date` >= '2020-05-01' AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%' OR remark LIKE 'Free Of SO%' OR remark LIKE 'CFSO no.%' ) THEN 1
						     WHEN nc.`date` < '2020-05-01' AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%' ) THEN 2
						ELSE 0 END
					) IN (1,2)
					AND nc.warehouse_id = ".$whsid."
				) AS dt	
				GROUP BY itemo_id
			)AS dtg ON dta.id = dtg.item_id
		)AS newdata
		LEFT JOIN item_to_item_group iig on newdata.id = iig.id
		LEFT JOIN item_group ig on iig.group_id = ig.id
		HAVING totalnominal1_ <> 0 OR jmlnc <> 0
		order by group_id, newdata.id,newdata.nama
		";
        $data = array();
		$qry = $this->db->query($q);
		//echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
            		$data[]=$row;
			}
        }
		$qry->free_result();
		return $data;
    }

	// End ASP 20180528 

    // Start Annisa 20191001
    public function omset_ro_product_gross_new_wh_all($fromdate,$todate)
    {
        $flag = 1;
        $q = "  
        SELECT 
            IFNULL(ig.id,99) AS group_id
            , CASE WHEN ig.id IS NOT NULL THEN ig.name ELSE 'Package' END AS group_name
            , newdata.id, newdata.nama
         ,((jml1 + jmlsc + 0) - jmlretur1) AS totalqty1 
        ,((jml2 + jmlsc + 0) - jmlretur2) AS totalqty2 
        ,((jml3 + jmlsc + 0) - jmlretur3) AS totalqty3 
        ,((jml4 + jmlsc + 0) - jmlretur4) AS totalqty4 
        ,((jml5 + jmlsc + 0) - jmlretur5) AS totalqty5 
        ,((jml6 + jmlsc + 0) - jmlretur6) AS totalqty6 
        ,((jml7 + jmlsc + 0) - jmlretur7) AS totalqty7 
        ,((jml8 + jmlsc + 0) - jmlretur8) AS totalqty8 
        ,((jml9 + jmlsc + 0) - jmlretur9) AS totalqty9
        ,((jml10 + jmlsc + 0) - jmlretur10) AS totalqty10
        ,((jml11 + jmlsc + 0) - jmlretur11) AS totalqty11
        ,((jml12 + jmlsc + 0) - jmlretur12) AS totalqty12 
        ,FORMAT((total1 + totsc + 0) - totretur1,0) AS totalnominal1
        ,FORMAT((total2 + totsc + 0) - totretur2,0) AS totalnominal2
        ,FORMAT((total3 + totsc + 0) - totretur3,0) AS totalnominal3
        ,FORMAT((total4 + totsc + 0) - totretur4,0) AS totalnominal4
        ,FORMAT((total5 + totsc + 0) - totretur5,0) AS totalnominal5
        ,FORMAT((total6 + totsc + 0) - totretur6,0) AS totalnominal6
        ,FORMAT((total7 + totsc + 0) - totretur7,0) AS totalnominal7
        ,FORMAT((total8 + totsc + 0) - totretur8,0) AS totalnominal8
        ,FORMAT((total9 + totsc + 0) - totretur9,0) AS totalnominal9
        ,FORMAT((total10 + totsc + 0) - totretur10,0) AS totalnominal10
        ,FORMAT((total11 + totsc + 0) - totretur11,0) AS totalnominal11
        ,FORMAT((total12 + totsc + 0) - totretur12,0) AS totalnominal12
        
        FROM(
            SELECT 
                dta.id, dta.`name` AS nama -- , dta.harga AS harga
               , (IFNULL(dtb.qwh1,0) + IFNULL(dtc.qwh1,0)) AS jml1
                , (IFNULL(dtb.totwh1,0) + IFNULL(dtc.totwh1,0)) AS total1
                , IFNULL(dtf.qwh1,0) AS jmlnc1, IFNULL(dtf.totwh1,0) AS totnc1
                , IFNULL(dtg.qwh1,0) AS jmladjstock1, IFNULL(dtg.totwh1,0) AS totadjstock1
                , IFNULL(dte.qwh1,0) AS jmlretur1, IFNULL(dte.totwh1,0) AS totretur1
                
                , (IFNULL(dtb.qwh2,0) + IFNULL(dtc.qwh2,0)) AS jml2
                , (IFNULL(dtb.totwh2,0) + IFNULL(dtc.totwh2,0)) AS total2
                , IFNULL(dtf.qwh2,0) AS jmlnc2, IFNULL(dtf.totwh2,0) AS totnc2
                , IFNULL(dtg.qwh2,0) AS jmladjstock2, IFNULL(dtg.totwh2,0) AS totadjstock2
                , IFNULL(dte.qwh2,0) AS jmlretur2, IFNULL(dte.totwh2,0) AS totretur2
                
                , (IFNULL(dtb.qwh3,0) + IFNULL(dtc.qwh3,0)) AS jml3
                , (IFNULL(dtb.totwh3,0) + IFNULL(dtc.totwh3,0)) AS total3
                , IFNULL(dtf.qwh3,0) AS jmlnc3, IFNULL(dtf.totwh3,0) AS totnc3
                , IFNULL(dtg.qwh3,0) AS jmladjstock3, IFNULL(dtg.totwh3,0) AS totadjstock3
                , IFNULL(dte.qwh3,0) AS jmlretur3, IFNULL(dte.totwh3,0) AS totretur3
                
                , (IFNULL(dtb.qwh4,0) + IFNULL(dtc.qwh4,0)) AS jml4
                , (IFNULL(dtb.totwh4,0) + IFNULL(dtc.totwh4,0)) AS total4
                , IFNULL(dtf.qwh4,0) AS jmlnc4, IFNULL(dtf.totwh4,0) AS totnc4
                , IFNULL(dtg.qwh4,0) AS jmladjstock4, IFNULL(dtg.totwh4,0) AS totadjstock4
                , IFNULL(dte.qwh4,0) AS jmlretur4, IFNULL(dte.totwh4,0) AS totretur4
                
                , (IFNULL(dtb.qwh5,0) + IFNULL(dtc.qwh5,0)) AS jml5
                , (IFNULL(dtb.totwh5,0) + IFNULL(dtc.totwh5,0)) AS total5
                , IFNULL(dtf.qwh5,0) AS jmlnc5, IFNULL(dtf.totwh5,0) AS totnc5
                , IFNULL(dtg.qwh5,0) AS jmladjstock5, IFNULL(dtg.totwh5,0) AS totadjstock5
                , IFNULL(dte.qwh5,0) AS jmlretur5, IFNULL(dte.totwh5,0) AS totretur5
                
                , (IFNULL(dtb.qwh6,0) + IFNULL(dtc.qwh6,0)) AS jml6
                , (IFNULL(dtb.totwh6,0) + IFNULL(dtc.totwh6,0)) AS total6
                , IFNULL(dtf.qwh6,0) AS jmlnc6, IFNULL(dtf.totwh6,0) AS totnc6
                , IFNULL(dtg.qwh6,0) AS jmladjstock6, IFNULL(dtg.totwh6,0) AS totadjstock6
                , IFNULL(dte.qwh6,0) AS jmlretur6, IFNULL(dte.totwh6,0) AS totretur6
                
                , (IFNULL(dtb.qwh7,0) + IFNULL(dtc.qwh7,0)) AS jml7
                , (IFNULL(dtb.totwh7,0) + IFNULL(dtc.totwh7,0)) AS total7
                , IFNULL(dtf.qwh7,0) AS jmlnc7, IFNULL(dtf.totwh7,0) AS totnc7
                , IFNULL(dtg.qwh7,0) AS jmladjstock7, IFNULL(dtg.totwh7,0) AS totadjstock7
                , IFNULL(dte.qwh7,0) AS jmlretur7, IFNULL(dte.totwh7,0) AS totretur7
                
                , (IFNULL(dtb.qwh8,0) + IFNULL(dtc.qwh8,0)) AS jml8
                , (IFNULL(dtb.totwh8,0) + IFNULL(dtc.totwh8,0)) AS total8
                , IFNULL(dtf.qwh8,0) AS jmlnc8, IFNULL(dtf.totwh8,0) AS totnc8
                , IFNULL(dtg.qwh8,0) AS jmladjstock8, IFNULL(dtg.totwh8,0) AS totadjstock8
                , IFNULL(dte.qwh8,0) AS jmlretur8, IFNULL(dte.totwh1,0) AS totretur8
                
                , (IFNULL(dtb.qwh9,0) + IFNULL(dtc.qwh9,0)) AS jml9
                , (IFNULL(dtb.totwh9,0) + IFNULL(dtc.totwh9,0)) AS total9
                , IFNULL(dtf.qwh9,0) AS jmlnc9, IFNULL(dtf.totwh9,0) AS totnc9
                , IFNULL(dtg.qwh9,0) AS jmladjstock9, IFNULL(dtg.totwh9,0) AS totadjstock9
                , IFNULL(dte.qwh9,0) AS jmlretur9, IFNULL(dte.totwh9,0) AS totretur9
                
                , (IFNULL(dtb.qwh10,0) + IFNULL(dtc.qwh10,0)) AS jml10
                , (IFNULL(dtb.totwh10,0) + IFNULL(dtc.totwh10,0)) AS total10
                , IFNULL(dtf.qwh10,0) AS jmlnc10, IFNULL(dtf.totwh10,0) AS totnc10
                , IFNULL(dtg.qwh10,0) AS jmladjstock10, IFNULL(dtg.totwh10,0) AS totadjstock10
                , IFNULL(dte.qwh10,0) AS jmlretur10, IFNULL(dte.totwh10,0) AS totretur10
                
                , (IFNULL(dtb.qwh11,0) + IFNULL(dtc.qwh11,0)) AS jml11
                , (IFNULL(dtb.totwh11,0) + IFNULL(dtc.totwh11,0)) AS total11
                , IFNULL(dtf.qwh11,0) AS jmlnc11, IFNULL(dtf.totwh11,0) AS totnc11
                , IFNULL(dtg.qwh11,0) AS jmladjstock11, IFNULL(dtg.totwh11,0) AS totadjstock11
                , IFNULL(dte.qwh11,0) AS jmlretur11, IFNULL(dte.totwh11,0) AS totretur11
                
                , (IFNULL(dtb.qwh12,0) + IFNULL(dtc.qwh12,0)) AS jml12
                , (IFNULL(dtb.totwh12,0) + IFNULL(dtc.totwh12,0)) AS total12
                , IFNULL(dtf.qwh12,0) AS jmlnc12, IFNULL(dtf.totwh12,0) AS totnc12
                , IFNULL(dtg.qwh12,0) AS jmladjstock12, IFNULL(dtg.totwh12,0) AS totadjstock12
                , IFNULL(dte.qwh12,0) AS jmlretur12, IFNULL(dte.totwh12,0) AS totretur12
                
                , IFNULL(dtd.jml,0) AS jmlsc, IFNULL(dtd.total,0) AS totsc
               -- ,dtb.warehouse_id
            FROM(
                SELECT i.id, i.`name` -- , sod.harga
                FROM item i
                -- LEFT JOIN so_d sod ON i.id = sod.item_id
                -- GROUP BY sod.item_id -- , sod.harga
            )AS dta
            LEFT JOIN(
                SELECT 
            item_id
            , harga
            , warehouse_id
            , SUM(qwh1) AS qwh1, SUM(totwh1) AS totwh1
            , SUM(qwh2) AS qwh2, SUM(totwh2) AS totwh2
            , SUM(qwh3) AS qwh3, SUM(totwh3) AS totwh3
            , SUM(qwh4) AS qwh4, SUM(totwh4) AS totwh4
            , SUM(qwh5) AS qwh5, SUM(totwh5) AS totwh5
            , SUM(qwh6) AS qwh6, SUM(totwh6) AS totwh6
            , SUM(qwh7) AS qwh7, SUM(totwh7) AS totwh7
            , SUM(qwh8) AS qwh8, SUM(totwh8) AS totwh8
            , SUM(qwh9) AS qwh9, SUM(totwh9) AS totwh9
            , SUM(qwh10) AS qwh10, SUM(totwh10) AS totwh10
            , SUM(qwh11) AS qwh11, SUM(totwh11) AS totwh11
            , SUM(qwh12) AS qwh12, SUM(totwh12) AS totwh12
            FROM (
                SELECT itemo_id AS item_id, hargao AS harga-- , SUM(qty)AS jml, SUM(total) AS total
                , warehouse_id
                ,CASE WHEN warehouse_id = 1 THEN qty ELSE 0 END AS qwh1,CASE WHEN warehouse_id = 1 THEN total ELSE 0 END AS totwh1
                ,CASE WHEN warehouse_id = 2 THEN qty ELSE 0 END AS qwh2,CASE WHEN warehouse_id = 2 THEN total ELSE 0 END AS totwh2
                ,CASE WHEN warehouse_id = 3 THEN qty ELSE 0 END AS qwh3,CASE WHEN warehouse_id = 3 THEN total ELSE 0 END AS totwh3
                ,CASE WHEN warehouse_id = 4 THEN qty ELSE 0 END AS qwh4,CASE WHEN warehouse_id = 4 THEN total ELSE 0 END AS totwh4
                ,CASE WHEN warehouse_id = 5 THEN qty ELSE 0 END AS qwh5,CASE WHEN warehouse_id = 5 THEN total ELSE 0 END AS totwh5
                ,CASE WHEN warehouse_id = 6 THEN qty ELSE 0 END AS qwh6,CASE WHEN warehouse_id = 6 THEN total ELSE 0 END AS totwh6
                ,CASE WHEN warehouse_id = 7 THEN qty ELSE 0 END AS qwh7,CASE WHEN warehouse_id = 7 THEN total ELSE 0 END AS totwh7
            ,CASE WHEN warehouse_id = 8 THEN qty ELSE 0 END AS qwh8,CASE WHEN warehouse_id = 8 THEN total ELSE 0 END AS totwh8
        ,CASE WHEN warehouse_id = 9 THEN qty ELSE 0 END AS qwh9,CASE WHEN warehouse_id = 9 THEN total ELSE 0 END AS totwh9
        ,CASE WHEN warehouse_id = 10 THEN qty ELSE 0 END AS qwh10,CASE WHEN warehouse_id = 10 THEN total ELSE 0 END AS totwh10
        ,CASE WHEN warehouse_id = 11 THEN qty ELSE 0 END AS qwh11,CASE WHEN warehouse_id = 11 THEN total ELSE 0 END AS totwh11
        ,CASE WHEN warehouse_id = 12 THEN qty ELSE 0 END AS qwh12,CASE WHEN warehouse_id = 12 THEN total ELSE 0 END AS totwh12
           FROM (
                    SELECT
                    -- distinct (so.id),
                    IFNULL(sod.warehouse_id,so.warehouse_id) AS warehouse_id,
                    -- sod.`warehouse_id`,
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE sod.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE sod.harga END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*sod.qty ELSE sod.qty END AS qty
                    -- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.jmlharga END AS total
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*sod.qty ELSE sod.harga*sod.qty END AS total
                    FROM so_d sod
                    LEFT JOIN reff_package rp ON sod.item_id = rp.package_id AND sod.harga = rp.package_price
                    LEFT JOIN so ON sod.so_id = so.id
                    WHERE 
                        so.tgl BETWEEN '$fromdate' AND '$todate'
                        AND so.stockiest_id = '0'
                    -- group by warehouse_id
                ) AS dt
                -- GROUP BY itemo_id -- , rod.harga
              )AS dt
              GROUP BY item_id -- , rod.harga
            )AS dtb ON dta.id = dtb.item_id  -- AND dta.harga = dtb.harga
            LEFT JOIN(
            SELECT 
            item_id
            , harga
            , SUM(qwh1) AS qwh1, SUM(totwh1) AS totwh1
            , SUM(qwh2) AS qwh2, SUM(totwh2) AS totwh2
            , SUM(qwh3) AS qwh3, SUM(totwh3) AS totwh3
            , SUM(qwh4) AS qwh4, SUM(totwh4) AS totwh4
            , SUM(qwh5) AS qwh5, SUM(totwh5) AS totwh5
            , SUM(qwh6) AS qwh6, SUM(totwh6) AS totwh6
            , SUM(qwh7) AS qwh7, SUM(totwh7) AS totwh7
            , SUM(qwh8) AS qwh8, SUM(totwh8) AS totwh8
            , SUM(qwh9) AS qwh9, SUM(totwh9) AS totwh9
            , SUM(qwh10) AS qwh10, SUM(totwh10) AS totwh10
            , SUM(qwh11) AS qwh11, SUM(totwh11) AS totwh11
            , SUM(qwh12) AS qwh12, SUM(totwh12) AS totwh12
            FROM (
                SELECT itemo_id AS item_id, hargao AS harga-- , SUM(qty)AS jml, SUM(total) AS total
                , warehouse_id
                ,CASE WHEN warehouse_id = 1 THEN qty ELSE 0 END AS qwh1,CASE WHEN warehouse_id = 1 THEN total ELSE 0 END AS totwh1
                ,CASE WHEN warehouse_id = 2 THEN qty ELSE 0 END AS qwh2,CASE WHEN warehouse_id = 2 THEN total ELSE 0 END AS totwh2
                ,CASE WHEN warehouse_id = 3 THEN qty ELSE 0 END AS qwh3,CASE WHEN warehouse_id = 3 THEN total ELSE 0 END AS totwh3
                ,CASE WHEN warehouse_id = 4 THEN qty ELSE 0 END AS qwh4,CASE WHEN warehouse_id = 4 THEN total ELSE 0 END AS totwh4
                ,CASE WHEN warehouse_id = 5 THEN qty ELSE 0 END AS qwh5,CASE WHEN warehouse_id = 5 THEN total ELSE 0 END AS totwh5
                ,CASE WHEN warehouse_id = 6 THEN qty ELSE 0 END AS qwh6,CASE WHEN warehouse_id = 6 THEN total ELSE 0 END AS totwh6
                ,CASE WHEN warehouse_id = 7 THEN qty ELSE 0 END AS qwh7,CASE WHEN warehouse_id = 7 THEN total ELSE 0 END AS totwh7
            ,CASE WHEN warehouse_id = 8 THEN qty ELSE 0 END AS qwh8,CASE WHEN warehouse_id = 8 THEN total ELSE 0 END AS totwh8
        ,CASE WHEN warehouse_id = 9 THEN qty ELSE 0 END AS qwh9,CASE WHEN warehouse_id = 9 THEN total ELSE 0 END AS totwh9
        ,CASE WHEN warehouse_id = 10 THEN qty ELSE 0 END AS qwh10,CASE WHEN warehouse_id = 10 THEN total ELSE 0 END AS totwh10
        ,CASE WHEN warehouse_id = 11 THEN qty ELSE 0 END AS qwh11,CASE WHEN warehouse_id = 11 THEN total ELSE 0 END AS totwh11
        ,CASE WHEN warehouse_id = 12 THEN qty ELSE 0 END AS qwh12,CASE WHEN warehouse_id = 12 THEN total ELSE 0 END AS totwh12
        FROM (
                    SELECT 
                    -- distinct(ro.id),
                    rod.warehouse_id,
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rod.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rod.harga END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rod.qty ELSE rod.qty END AS qty
                    -- ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE rod.jmlharga_ END AS total
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rod.qty ELSE (rod.harga_*rod.qty) END AS total
                    FROM ro_d rod
                    LEFT JOIN reff_package rp ON rod.item_id = rp.package_id AND rod.harga_ = rp.package_price
                    LEFT JOIN ro ON rod.ro_id = ro.id
                    WHERE 
                        ro.`date` BETWEEN '$fromdate' AND '$todate'
                        AND ro.stockiest_id = '0'
                    -- group by rod.warehouse_id
                ) AS dt
                -- GROUP BY itemo_id -- , rod.harga
              )AS dt
              GROUP BY item_id -- , rod.harga
              
            )AS dtc ON dta.id = dtc.item_id  -- AND dta.harga = dtc.harga
            LEFT JOIN(
                SELECT itemo_id AS item_id, qty AS jml, hargao AS harga, total
           FROM (
                    SELECT
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ptd.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE ptd.harga END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*ptd.qty ELSE ptd.qty END AS qty
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*ptd.qty ELSE ptd.jmlharga END AS total
                    FROM pinjaman_titipan_d ptd
                    LEFT JOIN pinjaman_titipan pj ON ptd.pinjaman_titipan_id = pj.id
                    LEFT JOIN reff_package rp ON ptd.item_id = rp.package_id AND ptd.harga = rp.package_price
                    WHERE pj.tgl BETWEEN '$fromdate' AND '$todate'
                ) AS dt
                GROUP BY itemo_id -- , ptd.harga
            )AS dtd ON dta.id = dtd.item_id  -- AND dta.harga = dtd.harga
            LEFT JOIN(
                SELECT 
            item_id
            , harga
            , SUM(qwh1) AS qwh1, SUM(totwh1) AS totwh1
            , SUM(qwh2) AS qwh2, SUM(totwh2) AS totwh2
            , SUM(qwh3) AS qwh3, SUM(totwh3) AS totwh3
            , SUM(qwh4) AS qwh4, SUM(totwh4) AS totwh4
            , SUM(qwh5) AS qwh5, SUM(totwh5) AS totwh5
            , SUM(qwh6) AS qwh6, SUM(totwh6) AS totwh6
            , SUM(qwh7) AS qwh7, SUM(totwh7) AS totwh7
            , SUM(qwh8) AS qwh8, SUM(totwh8) AS totwh8
            , SUM(qwh9) AS qwh9, SUM(totwh9) AS totwh9
            , SUM(qwh10) AS qwh10, SUM(totwh10) AS totwh10
            , SUM(qwh11) AS qwh11, SUM(totwh11) AS totwh11
            , SUM(qwh12) AS qwh12, SUM(totwh12) AS totwh12
            FROM (
                SELECT itemo_id AS item_id, hargao AS harga-- , SUM(qty)AS jml, SUM(total) AS total
                , warehouse_id
                ,CASE WHEN warehouse_id = 1 THEN qty ELSE 0 END AS qwh1,CASE WHEN warehouse_id = 1 THEN total ELSE 0 END AS totwh1
                ,CASE WHEN warehouse_id = 2 THEN qty ELSE 0 END AS qwh2,CASE WHEN warehouse_id = 2 THEN total ELSE 0 END AS totwh2
                ,CASE WHEN warehouse_id = 3 THEN qty ELSE 0 END AS qwh3,CASE WHEN warehouse_id = 3 THEN total ELSE 0 END AS totwh3
                ,CASE WHEN warehouse_id = 4 THEN qty ELSE 0 END AS qwh4,CASE WHEN warehouse_id = 4 THEN total ELSE 0 END AS totwh4
                ,CASE WHEN warehouse_id = 5 THEN qty ELSE 0 END AS qwh5,CASE WHEN warehouse_id = 5 THEN total ELSE 0 END AS totwh5
                ,CASE WHEN warehouse_id = 6 THEN qty ELSE 0 END AS qwh6,CASE WHEN warehouse_id = 6 THEN total ELSE 0 END AS totwh6
                ,CASE WHEN warehouse_id = 7 THEN qty ELSE 0 END AS qwh7,CASE WHEN warehouse_id = 7 THEN total ELSE 0 END AS totwh7
            ,CASE WHEN warehouse_id = 8 THEN qty ELSE 0 END AS qwh8,CASE WHEN warehouse_id = 8 THEN total ELSE 0 END AS totwh8
        ,CASE WHEN warehouse_id = 9 THEN qty ELSE 0 END AS qwh9,CASE WHEN warehouse_id = 9 THEN total ELSE 0 END AS totwh9
        ,CASE WHEN warehouse_id = 10 THEN qty ELSE 0 END AS qwh10,CASE WHEN warehouse_id = 10 THEN total ELSE 0 END AS totwh10
        ,CASE WHEN warehouse_id = 11 THEN qty ELSE 0 END AS qwh11,CASE WHEN warehouse_id = 11 THEN total ELSE 0 END AS totwh11
        ,CASE WHEN warehouse_id = 12 THEN qty ELSE 0 END AS qwh12,CASE WHEN warehouse_id = 12 THEN total ELSE 0 END AS totwh12
           FROM (
                    SELECT
                    -- DISTINCT (rt.id),
                    rt.warehouse_id,
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE rtd.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price ELSE rtd.harga END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_qty*rtd.qty ELSE rtd.qty END AS qty
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.item_price*rp.item_qty*rtd.qty ELSE rtd.jmlharga END AS total
                    FROM retur_titipan_d rtd
                    LEFT JOIN reff_package rp ON rtd.item_id = rp.package_id AND rtd.harga = rp.package_price
                    LEFT JOIN retur_titipan rt ON  rt.id = rtd.retur_titipan_id
                    WHERE rt.tgl BETWEEN '$fromdate' AND '$todate'
                ) AS dt
                -- GROUP BY itemo_id -- , rod.harga
              )AS dt
              GROUP BY item_id -- , rod.harga
            )AS dte ON dta.id = dte.item_id  -- AND dta.harga = dte.harga
            LEFT JOIN(
        SELECT
            item_id
            , harga
            , SUM(qwh1) AS qwh1, SUM(totwh1) AS totwh1
            , SUM(qwh2) AS qwh2, SUM(totwh2) AS totwh2
            , SUM(qwh3) AS qwh3, SUM(totwh3) AS totwh3
            , SUM(qwh4) AS qwh4, SUM(totwh4) AS totwh4
            , SUM(qwh5) AS qwh5, SUM(totwh5) AS totwh5
            , SUM(qwh6) AS qwh6, SUM(totwh6) AS totwh6
            , SUM(qwh7) AS qwh7, SUM(totwh7) AS totwh7
            , SUM(qwh8) AS qwh8, SUM(totwh8) AS totwh8
            , SUM(qwh9) AS qwh9, SUM(totwh9) AS totwh9
            , SUM(qwh10) AS qwh10, SUM(totwh10) AS totwh10
            , SUM(qwh11) AS qwh11, SUM(totwh11) AS totwh11
            , SUM(qwh12) AS qwh12, SUM(totwh12) AS totwh12
            FROM (
                SELECT itemo_id AS item_id, hargao AS harga-- , SUM(qty)AS jml, SUM(total) AS total
                , warehouse_id
                ,CASE WHEN warehouse_id = 1 THEN qty ELSE 0 END AS qwh1,CASE WHEN warehouse_id = 1 THEN total ELSE 0 END AS totwh1
                ,CASE WHEN warehouse_id = 2 THEN qty ELSE 0 END AS qwh2,CASE WHEN warehouse_id = 2 THEN total ELSE 0 END AS totwh2
                ,CASE WHEN warehouse_id = 3 THEN qty ELSE 0 END AS qwh3,CASE WHEN warehouse_id = 3 THEN total ELSE 0 END AS totwh3
                ,CASE WHEN warehouse_id = 4 THEN qty ELSE 0 END AS qwh4,CASE WHEN warehouse_id = 4 THEN total ELSE 0 END AS totwh4
                ,CASE WHEN warehouse_id = 5 THEN qty ELSE 0 END AS qwh5,CASE WHEN warehouse_id = 5 THEN total ELSE 0 END AS totwh5
                ,CASE WHEN warehouse_id = 6 THEN qty ELSE 0 END AS qwh6,CASE WHEN warehouse_id = 6 THEN total ELSE 0 END AS totwh6
                ,CASE WHEN warehouse_id = 7 THEN qty ELSE 0 END AS qwh7,CASE WHEN warehouse_id = 7 THEN total ELSE 0 END AS totwh7
            ,CASE WHEN warehouse_id = 8 THEN qty ELSE 0 END AS qwh8,CASE WHEN warehouse_id = 8 THEN total ELSE 0 END AS totwh8
        ,CASE WHEN warehouse_id = 9 THEN qty ELSE 0 END AS qwh9,CASE WHEN warehouse_id = 9 THEN total ELSE 0 END AS totwh9
        ,CASE WHEN warehouse_id = 10 THEN qty ELSE 0 END AS qwh10,CASE WHEN warehouse_id = 10 THEN total ELSE 0 END AS totwh10
        ,CASE WHEN warehouse_id = 11 THEN qty ELSE 0 END AS qwh11,CASE WHEN warehouse_id = 11 THEN total ELSE 0 END AS totwh11
        ,CASE WHEN warehouse_id = 12 THEN qty ELSE 0 END AS qwh12,CASE WHEN warehouse_id = 12 THEN total ELSE 0 END AS totwh12
                FROM (
                    SELECT
                    nc.warehouse_id,
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
                    ,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
                    FROM ncm_d ncd
                    LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
                    LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
                    WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
					-- AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%')
					AND (
						CASE WHEN nc.`date` >= '2020-05-01' AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%' AND remark NOT LIKE 'Free Of SO%' AND remark NOT LIKE 'CFSO no.%' ) THEN 1
						     WHEN nc.`date` < '2020-05-01' AND (remark NOT LIKE 'Free Of RO%' AND remark NOT LIKE 'CFRO no.%' ) THEN 2
						ELSE 0 END
					) in (1,2)
                ) AS dt
                -- GROUP BY itemo_id -- , rod.harga
              )AS dt
              GROUP BY item_id -- , rod.harga
            )AS dtf ON dta.id = dtf.item_id
            -- adj free RO
            LEFT JOIN(
                SELECT 
            item_id
            , harga
            , SUM(qwh1) AS qwh1, SUM(totwh1) AS totwh1
            , SUM(qwh2) AS qwh2, SUM(totwh1) AS totwh2
            , SUM(qwh3) AS qwh3, SUM(totwh1) AS totwh3
            , SUM(qwh4) AS qwh4, SUM(totwh1) AS totwh4
            , SUM(qwh5) AS qwh5, SUM(totwh1) AS totwh5
            , SUM(qwh6) AS qwh6, SUM(totwh1) AS totwh6
            , SUM(qwh7) AS qwh7, SUM(totwh1) AS totwh7
            , SUM(qwh8) AS qwh8, SUM(totwh1) AS totwh8
            , SUM(qwh9) AS qwh9, SUM(totwh1) AS totwh9
            , SUM(qwh10) AS qwh10, SUM(totwh1) AS totwh10
            , SUM(qwh11) AS qwh11, SUM(totwh1) AS totwh11
            , SUM(qwh12) AS qwh12, SUM(totwh1) AS totwh12
            FROM (
                SELECT itemo_id AS item_id, hargao AS harga-- , SUM(qty)AS jml, SUM(total) AS total
                , warehouse_id
                ,CASE WHEN warehouse_id = 1 THEN qty ELSE 0 END AS qwh1,CASE WHEN warehouse_id = 1 THEN total ELSE 0 END AS totwh1
                ,CASE WHEN warehouse_id = 2 THEN qty ELSE 0 END AS qwh2,CASE WHEN warehouse_id = 2 THEN total ELSE 0 END AS totwh2
                ,CASE WHEN warehouse_id = 3 THEN qty ELSE 0 END AS qwh3,CASE WHEN warehouse_id = 3 THEN total ELSE 0 END AS totwh3
                ,CASE WHEN warehouse_id = 4 THEN qty ELSE 0 END AS qwh4,CASE WHEN warehouse_id = 4 THEN total ELSE 0 END AS totwh4
                ,CASE WHEN warehouse_id = 5 THEN qty ELSE 0 END AS qwh5,CASE WHEN warehouse_id = 5 THEN total ELSE 0 END AS totwh5
                ,CASE WHEN warehouse_id = 6 THEN qty ELSE 0 END AS qwh6,CASE WHEN warehouse_id = 6 THEN total ELSE 0 END AS totwh6
                ,CASE WHEN warehouse_id = 7 THEN qty ELSE 0 END AS qwh7,CASE WHEN warehouse_id = 7 THEN total ELSE 0 END AS totwh7
            ,CASE WHEN warehouse_id = 8 THEN qty ELSE 0 END AS qwh8,CASE WHEN warehouse_id = 8 THEN total ELSE 0 END AS totwh8
        ,CASE WHEN warehouse_id = 9 THEN qty ELSE 0 END AS qwh9,CASE WHEN warehouse_id = 9 THEN total ELSE 0 END AS totwh9
        ,CASE WHEN warehouse_id = 10 THEN qty ELSE 0 END AS qwh10,CASE WHEN warehouse_id = 10 THEN total ELSE 0 END AS totwh10
        ,CASE WHEN warehouse_id = 11 THEN qty ELSE 0 END AS qwh11,CASE WHEN warehouse_id = 11 THEN total ELSE 0 END AS totwh11
        ,CASE WHEN warehouse_id = 12 THEN qty ELSE 0 END AS qwh12,CASE WHEN warehouse_id = 12 THEN total ELSE 0 END AS totwh12
                FROM (
                    SELECT
                    nc.warehouse_id,
                    CASE WHEN rp.item_id IS NOT NULL THEN rp.item_id ELSE ncd.item_id END AS itemo_id
                    ,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp ELSE ncd.hpp END AS hargao
                    ,CASE WHEN rp.item_id IS NOT NULL THEN rp.qty*ncd.qty ELSE ncd.qty END AS qty
                    ,CASE WHEN rp.item_id IS NOT NULL THEN ncd.hpp*rp.qty*ncd.qty ELSE ncd.qty*ncd.hpp END AS total
                    FROM ncm_d ncd
                    LEFT JOIN ncm nc ON ncd.ncm_id = nc.id
                    LEFT JOIN manufaktur rp ON ncd.item_id = rp.manufaktur_id
                    WHERE nc.`date` BETWEEN '$fromdate' AND '$todate'
					-- AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%')
					AND (
						CASE WHEN nc.`date` >= '2020-05-01' AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%' OR remark LIKE 'Free Of SO%' OR remark LIKE 'CFSO no.%' ) THEN 1
						     WHEN nc.`date` < '2020-05-01' AND (remark LIKE 'Free Of RO%' OR remark LIKE 'CFRO no.%' ) THEN 2
						ELSE 0 END
					) IN (1,2)
                ) AS dt
                -- GROUP BY itemo_id -- , rod.harga
              )AS dt
              GROUP BY item_id -- , rod.harga
            )AS dtg ON dta.id = dtg.item_id
        )AS newdata
        LEFT JOIN item_to_item_group iig ON newdata.id = iig.id
        LEFT JOIN item_group ig ON iig.group_id = ig.id
       -- HAVING totalnominall_ <> 0 OR jmlnc <> 0 OR jmladjstock <> 0
        ORDER BY group_id, newdata.id,newdata.nama
        ";
        $data = array();
        $qry = $this->db->query($q);
        //echo $this->db->last_query();
        if($qry->num_rows()>0){
            foreach($qry->result_array() as $row){
                    $data[]=$row;
            }
        }
        $qry->free_result();
        return $data;
    }

    // End ARR 20191001	
	/*
    |--------------------------------------------------------------------------
    | Rekap History Stockiest Consignment 2019
    |--------------------------------------------------------------------------
    |
    | @author budi@nawadata.com
    | @poweredby www.smartindo-technology.com
    | @created 2019-01-29
    |
    */
	
	public function list_sc($keywords=0,$num,$offset){
		
		$data = array();
		/*
		if($this->session->userdata('group_id') <= 100){
            if($keywords){
				$where = "a.stockiest_id = '$keywords' and (a.tgl between '$num' and '$offset') ";
            }else{
				$where = "(a.tgl between '$num' and '$offset') ";
			}
        }else{
            $where = "a.stockiest_id = '$keywords' and (a.tgl between '$num' and '$offset') ";
        }
		*/
		if($this->session->userdata('group_id') <= 100){
            if($keywords){
				$where = "a.stockiest_id = '$keywords' AND dt.qty_total > 0";
            }else{
				$where = "dt.qty_total > 0";
			}
        }else{
            $where = "a.stockiest_id = '$keywords' AND dt.qty_total > 0";
        }

		$this->db->select("a.id, date_format(a.tgl,'%d-%b-%Y')as tgl, a.tgl as normal_date,
		m.id as member_id,
		m.nama,
		FORMAT(a.totalharga,0)AS totalharga,
		FORMAT(a.totalpv,0)AS totalpv,
		IFNULL((SELECT format(SUM(pinjaman_d.qty_total),0) as qty_total FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id),0) as qty_total,
		IFNULL((SELECT format(SUM(pinjaman_d.qty_retur),0) as qty_retur FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id),0) as qty_retur,
		IFNULL((SELECT format(SUM(pinjaman_d.qty_lunas),0) as qty_lunas FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id),0) as qty_lunas,
		IFNULL((SELECT format(SUM(pinjaman_d.qty),0) as qty FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id),0) as qty,
		a.remark,
		w.name as warehouse_name, w.id as warehouse_id",false);
        $this->db->from('pinjaman a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
		$this->db->join('(
						SELECT pinjaman_d.pinjaman_id, FORMAT(SUM(pinjaman_d.qty_total),0) AS qty_total FROM pinjaman_d
						GROUP BY pinjaman_id
						) AS dt	'
						, 'dt.pinjaman_id = a.id'
						,'left');
		// START ASP 20180525
		$this->db->join('warehouse w', 'a.warehouse_id=w.id','left');
		// EOF ASP 20180525
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
		
        
        $q = $this->db->get();
        $xSql= $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
				if ($row['qty_total']>0){
					$data[] = $row;
				}
            }			
        }
		//echo $xSql;
        return $data;
		
	}
	
	public function list_sc_lunas($keywords=0,$num,$offset){
		$data = array();
		if($this->session->userdata('group_id') <= 100){
            if($keywords)$where = "a.stockiest_id = '$keywords' and (a.tgl between '$num' and '$offset') ";
            else $where = "(a.tgl between '$num' and '$offset') ";
        }else{
            $where = "a.stockiest_id = '$keywords' and (a.tgl between '$num' and '$offset') ";
        }
		/*
		$where = "a.stockiest_id = '$keywords'";
		if($num!='' && $offset!=''){
			//$this->db->limit($num,$offset);
			$where = "a.stockiest_id = '$keywords'  AND ( a.tgl BETWEEN '$num' AND '$offset') ";
		}
		*/
		
		$this->db->select("a.id, date_format(a.tgl,'%d-%b-%Y')as tgl, a.tgl as normal_date,
		m.id as member_id,
		m.nama,
		FORMAT(a.totalharga,0)AS totalharga,
		FORMAT(a.totalpv,0)AS totalpv,
		IFNULL((SELECT format(SUM(pinjaman_d.qty_total),0) as qty_total FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id),0) as qty_total,
		IFNULL((SELECT format(SUM(pinjaman_d.qty_retur),0) as qty_retur FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id),0) as qty_retur,
		IFNULL((SELECT format(SUM(pinjaman_d.qty_lunas),0) as qty_lunas FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id),0) as qty_lunas,
		IFNULL((SELECT format(SUM(pinjaman_d.qty),0) as qty FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id),0) as qty,
		a.remark,
		w.name as warehouse_name, w.id as warehouse_id",false);
        $this->db->from('pinjaman a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
		// START ASP 20180525
		$this->db->join('warehouse w', 'a.warehouse_id=w.id','left');
		// EOF ASP 20180525
       	$this->db->where($where);
        $this->db->order_by('a.id','desc');
		
        
        $q = $this->db->get();
        $xSql= $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
				if ($row['qty_total']<1){ $data[] = $row; }
            }			
        }
		//echo $xSql;
        return $data;
		
	}
	
	
		
    public function getPayment($keywords){
		$where = "a.pinjaman_id LIKE '%".$keywords."%'";
		$this->db->select(" 
		a.id as id,
		IFNULL(FORMAT(a.qty,0),0) as qty,
		IFNULL(FORMAT(a.qty_lunas,0),0) as qty_lunas,
		IFNULL(FORMAT(a.qty_retur,0),0) as qty_retur,
		FORMAT(a.harga,0) as harga,
		FORMAT(a.pv,0) as pv,
		FORMAT(a.jmlharga,0) as jmlharga, FORMAT(a.jmlpv,0) as jmlpv, 
		IFNULL(FORMAT((a.harga * a.qty_lunas),0), 0 ) AS total_lunas_ori,
		IFNULL(FORMAT((SELECT SUM(pinjaman_payment.pinjaman_payment_ttl) FROM pinjaman_payment WHERE pinjaman_d_id=a.id),0), 0 ) as total_pay_lunas,
		IFNULL(FORMAT((SELECT SUM(pinjaman_payment.pinjaman_payment_ttl_pv) FROM pinjaman_payment WHERE pinjaman_d_id=a.id),0), 0 ) as total_pay_lunaspv,
		IFNULL(FORMAT((a.pv * a.qty_lunas),0), 0) AS total_lunaspv,
		IFNULL(FORMAT((a.harga * a.qty_retur),0),0) AS total_retur,
		IFNULL(FORMAT((a.pv * a.qty_retur),0), 0) AS total_returpv,
		IFNULL(FORMAT((a.harga * (a.qty - a.qty_retur - a.qty_lunas)),0),0) AS total_sisa,
		IFNULL(FORMAT((a.pv * (a.qty - a.qty_retur - a.qty_lunas)),0),0) AS total_sisapv,
		pd.stockiest_id as member_id,
		m.nama as member_nama_to, 
		i.`name` as nama_item 
		",false);
        $this->db->from('pinjaman_d a');
        $this->db->join('pinjaman pd','pd.id = a.pinjaman_id','inner');
        //$this->db->join('pinjaman_payment py','py.pinjaman_id = a.pinjaman_id','LEFT');
        $this->db->join('member m','m.id = pd.stockiest_id','inner');
        $this->db->join('item i','i.id = a.item_id','inner');
		// START ASP 20180410
        //$this->db->join('member d','d.stockiest_id = a.stockiest_id_payment','LEFT');
		// EOF ASP 20180410
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        //$this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
	}
	
	public function get_FreeItem($keywords){
		$data = array();
		/*
		$this->db->select("
		a.*, 
		b.item_id,
		c.`name` AS item_name,
		( SELECT c. NAME AS free_item_name FROM item c WHERE c.id = d.free_item_id ) AS Free_Item_Name, 
		b.warehouse_id,
		d.item_qty,
		d.free_item_id,
		d.free_item_qty,
		a.qty as GetFree 
		",false);
		$this->db->from('pinjaman_nc a');
		$this->db->join('pinjaman_d b','b.id = a.pinjaman_d_id','inner');
		$this->db->join('item c','c.id = b.item_id','inner');
		$this->db->join('promo d','d.id = a.promo_id','inner');
		$this->db->like('a.pinjaman_id', $keywords, 'after');
		$this->db->order_by('c.`name`','asc');
		$this->db->limit($num,$offset);
		$q = $this->db->get();
		//echo $this->db->last_query();
		if($q->num_rows > 0){
		  foreach($q->result_array() as $row){
			$data[] = $row;
		  }
		}
		$q->free_result();
		return $data;
		*/
		
		$this->db->select("
		a.*, 
		b.item_id,
		c.`name` AS item_name,
		( SELECT c. NAME AS free_item_name FROM item c WHERE c.id = d.free_item_id ) AS Free_Item_Name, 
		b.warehouse_id,
		d.item_qty,
		d.free_item_id,
		d.free_item_qty,
		a.qty as GetFree 
		",false);
		$this->db->from('pinjaman_nc a');
		$this->db->join('pinjaman_d b','b.id = a.pinjaman_d_id','inner');
		$this->db->join('item c','c.id = b.item_id','inner');
		$this->db->join('promo d','d.id = a.promo_id','inner');
		$this->db->like('a.pinjaman_id', $keywords, 'after');
		$this->db->order_by('c.`name`','asc');
		$q = $this->db->get();
		//echo $this->db->last_query();
		if($q->num_rows > 0){
		  foreach($q->result_array() as $row){
			$data[] = $row;
		  }
		}
		$q->free_result();
		return $data;
	}
	
	

	
	
	
	
	
}?>