<?php
class BOnus_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    /*
    |--------------------------------------------------------------------------
    | Report Bonus Statment
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @development by www.smartindo-technology.com
    | @created 2009-05-29
    |
    */
    
    public function getBonus($member_id,$periode){
        $data = array();
        $this->db->select("`b`.`id`          AS `id`,
			  b.flag,
	`b`.`display`     AS `display`,
	`b`.`member_id`   AS `member_id`,
	`m`.`jenjang_id`  AS `jenjang_id`,
	`m`.`nama`        AS `nama`,
	`m`.`hp`        AS `hp`,
	`j`.`decription` AS `jenjang`,
	`m`.`enroller_id` AS `enroller_id`,
	`b`.`periode`     AS `periode`,
	DATE_FORMAT(`b`.`periode`,'%M %Y') AS `tgl`,
	FORMAT(`b`.`pv`,0) AS `fbv`,
	`b`.`pv`          AS `bv`,
	`m`.`npwp`        AS `npwp`,
	`m`.`tglberlaku`  AS `tglberlaku`,
	CONCAT(FORMAT(`b`.`persen`,2),'%') AS `persen`,
	FORMAT(`b`.`nominal`,0) AS `fnominal`,
	`b`.`nominal`     AS `nominal`,
	`b`.`title`       AS `title`,
	`t`.`description` AS `titlebonus`,b.approvedby,DATE_FORMAT(`b`.`approved`,'%d-%m-%Y') AS `ftglapproved`,b.remark,",false);
      
      $this->db->from('bonus_pending b');
      $this->db->join('member m','b.member_id=m.id','left');
      $this->db->join('title_bonus t','b.title=t.id','left');
      $this->db->join('jenjang j','b.jenjang_id=j.id','left');
      
      if($member_id)$this->db->where('b.member_id',$member_id);
      $this->db->where('b.periode',$periode);

	$q = $this->db->get();
	//echo $this->db->last_query();
    
	$i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function getTotalBonus($member_id,$periode){
        $data = array();
        $this->db->select("format(sum(b.nominal),0)as fnominal",false);
	$this->db->from('bonus_pending b');
	if($member_id)$this->db->where('b.member_id',$member_id);
	$this->db->where('b.periode',$periode);
	$q = $this->db->get();
	
	//echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getSubBonus($id){
        $data = array();
	$this->db->select("`b`.`id`          AS `id`,
			  b.flag,
	`b`.`display`     AS `display`,
	`b`.`member_id`   AS `member_id`,
	`m`.`jenjang_id`  AS `jenjang_id`,
	`m`.`nama`        AS `nama`,
	`m`.`hp`        AS `hp`,
	`j`.`decription` AS `jenjang`,
	`m`.`enroller_id` AS `enroller_id`,
	`b`.`periode`     AS `periode`,
	DATE_FORMAT(`b`.`periode`,'%M %Y') AS `tgl`,
	FORMAT(`b`.`pv`,0) AS `fbv`,
	`b`.`pv`          AS `bv`,
	`m`.`npwp`        AS `npwp`,
	`m`.`tglberlaku`  AS `tglberlaku`,
	CONCAT(FORMAT(`b`.`persen`,2),'%') AS `persen`,
	FORMAT(`b`.`nominal`,0) AS `fnominal`,
	`b`.`nominal`     AS `nominal`,
	`b`.`title`       AS `title`,
	`t`.`description` AS `titlebonus`",false);
      
        $this->db->from('bonus_pending b');
      $this->db->join('member m','b.member_id=m.id','left');
      $this->db->join('title_bonus t','b.title=t.id','left');
      $this->db->join('jenjang j','b.jenjang_id=j.id','left');
	$this->db->where('b.id',$id);
        if($this->session->userdata('group_id')>100)$this->db->where('member_id',$this->session->userdata('userid'));
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
                $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getBonusDetail($id){
        $data = array();
        $this->db->select("d.downline_id,m.nama,format(pv,0)as fpv,d.persen,format(d.nominal,0)as fnominal",false);
	$this->db->from('bonus_pending_d d');
	$this->db->join('member m','d.downline_id=m.id','left');
	$this->db->where('d.bonus_id',$id);
	$this->db->order_by('m.nama','asc');
        $q = $this->db->get();
	
	//echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getDropDrownPeriode(){
        $data = array();
        $this->db->select("periode,date_format(periode,'%M %Y')as tgl",false);
        $this->db->from('bonus_pending');
        $this->db->group_by('tgl');
        $this->db->order_by('periode','desc');
        $q=$this->db->get();
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $data[$row['periode']] = $row['tgl'];
            }
        }
        $q->free_result();
        return $data;
    }
    public function approval_bonus_peringkat(){
	if($this->input->post('counter')){
	    $empid = $this->session->userdata('username');		    
	    $key=0;
	    while($key < count($_POST['counter'])){
		$p_id = $this->input->post('p_id'.$key);
		if($this->input->post('p_id'.$key) > 0){
		    $row = $this->_get_approval_peringkat($p_id);
		    if($row->id > 0){
			$remark = $this->db->escape_str($this->input->post('remark'));
			$data=array(
			    'flag' => '1',
			    'remark' => $remark,
			    'approved' => date('Y-m-d H:i:s',now()),
			    'approvedby' => $empid
			);
			$this->db->update('bonus_pending',$data,array('id'=>$p_id));
		    }
		}
		$key++;
	    }
	    $this->session->set_flashdata('message','Refund withdrawal successfully');
	    
        }else{
            $this->session->set_flashdata('message','Nothing to refund withdrawal!');
        }
    }
    protected function _get_approval_peringkat($id = 0){
        $data=array();
        $this->db->select("id",false);
        $this->db->where('id',$id);
	$this->db->where('flag','0');
	$this->db->limit(1);
        $q = $this->db->get('bonus_pending');
        if($q->num_rows() > 0){
	    $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
	
    /* Created by Boby 20141030 */
	public function getBonusNew($member_id,$periode){
        $data = array();
		$br = "<br>";
		$br = "";
		$qry = "
			SELECT ".$br."
				b.id AS id ".$br."
				, b.flag ".$br."
				, b.display AS display ".$br."
				, b.member_id AS member_id ".$br."
				, m.jenjang_id AS jenjang_id ".$br."
				, m.nama AS nama ".$br."
				, m.hp AS hp ".$br."
				, j.decription AS jenjang ".$br."
				, m.enroller_id AS enroller_id ".$br."
				, b.periode AS periode ".$br."
				, DATE_FORMAT(b.periode,'%M %Y') AS tgl ".$br."
				, FORMAT(b.pv,0) AS fbv ".$br."
				, b.pv AS bv ".$br."
				, m.npwp AS npwp ".$br."
				, m.tglberlaku AS tglberlaku ".$br."
				, CONCAT(FORMAT(b.persen,2),'%') AS persen ".$br."
				, FORMAT(b.nominal,0) AS fnominal ".$br."
				, b.nominal AS nominal ".$br."
				, b.title AS title ".$br."
				, t.description AS titlebonus ".$br."
				, b.approvedby ".$br."
				, DATE_FORMAT(b.approved,'%d-%m-%Y') AS ftglapproved ".$br."
				, b.remark as remark_ ".$br."
				, CASE WHEN IFNULL(mq.jml,0) > 0 THEN 'Manual qualified' ELSE b.remark END AS remark ".$br."
				, CASE WHEN IFNULL(mq.jml,0) > 0 THEN 1 ELSE 0 END AS flag_ ".$br."
			FROM bonus_pending b ".$br."
			LEFT JOIN member m ON b.member_id = m.id ".$br."
			LEFT JOIN title_bonus t ON b.title = t.id ".$br."
			LEFT JOIN jenjang j ON b.jenjang_id = j.id ".$br."
			LEFT JOIN( ".$br."
				SELECT member_id, title_bonus, 1 AS jml ".$br."
				FROM manual_qualified ".$br."
				WHERE periode BETWEEN LAST_DAY('$periode'-INTERVAL 12 MONTH) + INTERVAL 1 DAY AND LAST_DAY('$periode') ".$br."
			)AS mq ON m.id = mq.member_id and b.title = mq.title_bonus".$br."
		";
		// echo $periode;
		if($member_id){
			$qry .= " where b.member_id = '$member_id' ";
		}else{
			$qry .= "where b.periode = '$periode'";
		}
		
		$q = $this->db->query($qry);
		// echo $this->db->last_query();
		
		$i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    /* End created by Boby 20141030 */
}
?>