<?php
class MTesti extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function searchTesti($keywords=0,$num,$offset){
        $data = array();
        $this->db->select('id,title,shortdesc,status,created,createdby',false)
            ->from('testi')
            ->like('title', $keywords, 'match')
            ->order_by('id','desc')
            ->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countTesti($keywords=0){
        $this->db->like('title', $keywords, 'match');
        $this->db->from('testi');
        return $this->db->count_all_results();
    }
    public function addTestimonial($name=''){
        $data = array(
            'thumbnail' => $name,
            'title' => $this->db->escape_str($this->input->post('title')),
            'shortdesc' => $this->db->escape_str($this->input->post('shortdesc')),
            'longdesc' => $_POST['longdesc'],
            'status' => $this->input->post('status'),
            'created' => date('Y-m-d H:m:s',now()),
            'createdby' => $this->session->userdata('user')
        );
        $this->db->insert('testi',$data);
    }
    
    public function updateTestimonial($filename = ''){
        if($filename){
            $data = array(
                'title' => $this->db->escape_str($this->input->post('title')),
                'shortdesc' => $this->db->escape_str($this->input->post('shortdesc')),
                'thumbnail' => $filename,
                'longdesc' => $_POST['longdesc'],
                'status' => $this->input->post('status'),
                'updated' => date('Y-m-d H:m:s',now()),
                'updatedby' => $this->session->userdata('user')
            );
        }else{
            $data = array(
                'title' => $this->db->escape_str($this->input->post('title')),
                'shortdesc' => $this->db->escape_str($this->input->post('shortdesc')),
                'longdesc' => $_POST['longdesc'],
                'status' => $this->input->post('status'),
                'updated' => date('Y-m-d H:m:s',now()),
                'updatedby' => $this->session->userdata('user')
            );
        }
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('testi',$data);
    }
    
    public function getTesti($id=0){
        $data = array();
        $option = array('id' => $id);
        $q = $this->db->get_where('testi',$option,1);
        if($q->num_rows() > 0){
                $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getTestiList($num,$offset){
        $data = array();
        $this->db->select('id,shortdesc,thumbnail',false)
            ->from('testi')
            ->where('status','active')
            ->order_by('id','desc')
            ->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countTestiList(){
        $this->db->where('status','active');
        $this->db->from('testi');
        return $this->db->count_all_results();
    }

}
?>