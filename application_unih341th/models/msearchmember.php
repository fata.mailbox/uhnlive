<?php
/**
* 
*/
class MsearchMember extends CI_Model
{
    
    function __construct()
    {
        parent::__construct();
    }

    public function search_member($keywords=0,$num,$offset){
        
        $data = array();
        $this->db->select('id,nama');
        $this->db->from('member');
        $this->db->like('id', $keywords);
        $this->db->like('nama', $keywords);     
               
        $this->db->order_by('id','asc');
        $this->db->limit($num,$offset);

        $q = $this->db->get();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
        
    }
    public function count_search_member($keywords=0){
        $this->db->select('id,nama');
        $this->db->from('member');
        $this->db->like('id', $keywords);
        $this->db->like('nama', $keywords);  

        return $this->db->count_all_results();
    }

    public function search_stockiest($keywords=0,$num,$offset){
        
        $data = array();
        $this->db->select('s.id,m.nama');
        $this->db->from('stockiest s');
        $this->db->join('member m','m.id=s.id','left');
        $this->db->like('s.id', $keywords);
        $this->db->like('m.nama', $keywords);       
               
        $this->db->order_by('s.id','asc');
        $this->db->limit($num,$offset);

        $q = $this->db->get();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
        
    }
    public function count_search_stockiest($keywords=0){
        $this->db->select('s.id,m.nama');
        $this->db->from('stockiest s');
        $this->db->join('member m','m.id=s.id','left');
        $this->db->like('s.id', $keywords);
        $this->db->like('m.nama', $keywords); 

        return $this->db->count_all_results();
    }
}
?>