<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Demandmodel extends CI_Model {

    
    public function __construct()
    {
        parent::__construct();
        
    }

    function demand_get()
    {
        return $this->db->query("SELECT * from topupbydemand ")->result();
    }

    
    function loyalti_get($usern)
    {
        return $this->db->query("SELECT DISTINCT b.* FROM topupbydemand_detail a
        JOIN topupbydemand b ON a.`parent_upload` = b.`topupno`
        WHERE a.`stockiest_id` = '" . $usern . "'")->result();
    }

    function loyalti_view($no,$usern)
    {
        return $this->db->query("SELECT * FROM topupbydemand JOIN topupbydemand_detail on topupbydemand.topupno = topupbydemand_detail.parent_upload WHERE topupno = '" . $no . "'  and stockiest_id  = '$usern' GROUP BY stockiest_id  ")->row();
    }

    function history_demand($no,$usern)
    {
        return $this->db->query("SELECT * from history_demand where stc_id = '$usern' and  topupno = '" . $no . "'   ORDER BY id ASC ")->result_array();
    }

    function history_row($no,$usern)
    {
        return $this->db->query("SELECT * from history_demand where stc_id = '$usern' and  topupno = '" . $no . "'   ORDER BY id ASC ")->row_array();
    }
    
    function titipan_demand($stc_id,$item_id)
    {
        return $this->db->query("SELECT fharga, fpv FROM (v_titipan v) WHERE item_id = '" . $item_id . "' and member_id = '" . $stc_id . "' ORDER BY id desc")->row_array();
    }

    function temp_demand($ac)
    {
        $ata = $this->db->query("SELECT * from temp_demand $ac ")->result_array();
    }


    function detail_history($no,$usern)
    {
        return $this->db->query("SELECT * FROM topupbydemand_detail  WHERE parent_upload = '" . $no . "'  and stockiest_id  = '$usern' ")->result();
    }
    
    function sum_demand($no,$usern)
    {
        $this->db->query("select sum(jml_awal) as awal, sum(jml_pakai) as pakai, sum(jml_akhir) as akhir  from history_demand where stc_id = '$usern' and topupno = '" . $no . "' ")->row_array();

    }

    function result_demand($id)
    {
        return $this->db->query("SELECT * FROM topupbydemand where topupno = '" . $id . "' ")->row();
    }

    function edit_demand($id)
    {
        return $this->db->query("SELECT m1.nama as nama, s1.no_stc, tD.* FROM topupbydemand_detail tD
											JOIN member m1 ON tD.stockiest_id = m1.id
											JOIN stockiest s1 ON tD.stockiest_id = s1.id
											WHERE parent_upload = '" . $id . "'")->result();
    }

    function demand_stockiest()
    {
        return $this->db->query("SELECT a.`id`,b.`nama` FROM stockiest a
        JOIN member b ON a.`id` = b.`id`
        WHERE a.`id` != 0")->result();
    }
    

    function demand_delete($id)
    {
        $r = $this->db->query("SELECT filename from topupbydemand where topupno = '" . $id . "' ")->row_array();
		$file = './excel/'.$r['filename'];
		unlink($file);	
		$y = $this->db->query("DELETE FROM topupbydemand where topupno = '" . $id . "'");
		$this->db->query("DELETE FROM topupbydemand_detail where parent_upload = '" . $id . "'");
		$this->db->query("DELETE FROM history_demand where topupno = '" . $id . "'");
		echo '<script language="javascript">alert("Sukses Hapus Data")</script>';
			redirect('smartindo/topupdemand', 'refresh');	
    }

    function import_demand()
    {
        $this->db->empty_table('temp_demand');
		$getjml = $this->db->query("SELECT COUNT(*) AS jml FROM topupbydemand")->row()->jml + 1;
		if ($getjml < 10) {
			$codec = '0000' . $getjml;
		} else if ($getjml >= 10 and $getjml < 100) {
			$codec = '000' . $getjml;
		} else if ($getjml >= 100 and $getjml < 1000) {
			$codec = '00' . $getjml;
		} else if ($getjml >= 1000 and $getjml < 10000) {
			$codec = '0' . $getjml;
		} else if ($getjml >= 10000 and $getjml < 100000) {
			$codec = $getjml;
		}
		//var_dump($codec);exit();
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';
		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx|xls';
		$config['max_size']    = '10240'; //10 MB
		$config['max_width']  = '1600';
		$config['max_height']  = '1200';

		$this->load->library('upload');
		$this->upload->initialize($config);

		if (!empty($_FILES['file'])) {
			$rest = $_FILES['file']['name'];
			$cek = $this->db->query("SELECT filename from topupbydemand where filename = '$rest' ")->num_rows();
			if ($cek > 0) {
				echo "<script>alert('File sudah ada di database, silahkan rename file !')</script>";
				redirect('smartindo/topupdemand/add', 'refresh');
			}else {
				if (!$this->upload->do_upload('file')) {
					echo "<script>alert('Gagal Import Data , Ubah Permission !')</script>";
					redirect('smartindo/topupdemand/add', 'refresh');
				} else {
					$upload_data = $this->upload->data();
					$datefrom = $this->input->post('datefrom');
					$dateto = $this->input->post('dateto');
					$multiple = $this->input->post('multiple');
					$status = $this->input->post('status');
					$topupno = $codec;
					$data = array(
						'valid_from' => $datefrom,
						'valid_to' => $dateto,
						'multiple' => $multiple,
						'filename' => $upload_data['file_name'],
						'topupno' => $topupno,
						'status' => $status,
						'last_update' => date('Y-m-d H:i:s'),
						'update_by' => $this->session->userdata('username'),
					);
					$excelreader = new PHPExcel_Reader_Excel2007();
					$loadexcel = $excelreader->load('excel/' . $upload_data['file_name']);
	
					$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
					$res = [];
					$ser = [];
	
					$no = 1;
					$numrow = 1;
					foreach ($sheet as $row) {
						if ($numrow > 1) {
							if (empty(get_data_by_tabel_id($row['C'], 'item'))) {
								array_push($res, $no);
							} else {
								if (empty(get_data_by_tabel_id($row['A'], 'member'))) {
									array_push($res, $no);
								} else {
									if (empty($row['D']) || $row['D'] == 0) {
										array_push($res, $no);
									} else {
										array_push($ser, array(
											'member_id' => $row['A'], // Insert data nis dari kolom A di excel
											'stockiest_id' => $row['B'], // Insert data nama dari kolom B di excel
											'item_code' => $row['C'], // Insert data jenis kelamin dari kolom C di excel
											'qty' => $row['D'],
											'parent_upload' => $topupno // Insert data alamat dari kolom D di excel
										));
									}
								}
							}
						}
	
						if (!empty($res)) {
							$dz =
								[
									'no'	=> $no,
									'member_id' => $row['A'], // Insert data nis dari kolom A di excel
									'stockiest_id' => $row['B'], // Insert data nama dari kolom B di excel
									'item_code' => $row['C'], // Insert data jenis kelamin dari kolom C di excel
									'qty' => $row['D']
								];
							$this->db->insert('temp_demand', $dz);
							$no++;
						}
						$numrow++;
					}
	
					if (!empty($res)) {
						$this->session->set_flashdata('message', $res);
	
						echo '<script language="javascript">alert("Gagal Import Data")</script>';
						redirect('smartindo/topupdemand/add', 'refresh');
					} else if (!empty($ser) && empty($res)) {
	
						$this->db->insert('topupbydemand', $data);
						foreach ($ser as $key) {
							$rowdata = array(
								'member_id'      => $key['member_id'],
								'item_code'        => $key['item_code'],
								'qty'        => $key['qty'],
								'stockiest_id'    => $key['stockiest_id'],
								'parent_upload'  => $key['parent_upload']
							);
							$this->db->insert('topupbydemand_detail', $rowdata);
						}
	
						echo '<script language="javascript">alert("Sukses Import Data")</script>';
						redirect("smartindo/topupdemand");
					}
					// $this->Topupbydemand_model->insert_multiple($data);
	
				}
			}
		}
    }

    function update_demand()
    {

		$datefrom = $this->input->post('datefrom');
		$dateto = $this->input->post('dateto');
		$multiple = $this->input->post('multiple');
		$status = $this->input->post('status');
		$data = array(
			'valid_from' => $datefrom,
			'valid_to' => $dateto,
			'multiple' => $multiple,
			'status' => $status,
			'last_update' => date('Y-m-d H:i:s'),
			'update_by' => $this->session->userdata('username'),
		);

		$this->db->where('topupno', $this->input->post('id'));
		$this->db->update('topupbydemand', $data);

		$itemcode = $this->input->post('ids');
		//$stockiest = $this->input->post('stockiest');
		$stockiest = $this->input->post('id_stc');
		$memberid = $this->input->post('memberid');
		//var_dump($itemcode); die();
		//for ($i = 0; $i < count($itemcode); $i++) {
		for ($i = 0; $i < count($stockiest); $i++) {
			$datas = array(
				'stockiest_id' => $stockiest[$i]
			);
			$this->db->where('id', $itemcode[$i]);
			$this->db->update('topupbydemand_detail', $datas);
			
			$this->db->query("UPDATE member SET stockiest_id = '" . $stockiest[$i] . "' WHERE id = '" . $memberid[$i] . "'");
		}
		//echo '<script language="javascript">alert("Sukses Update Data")</script>';
		$this->session->set_flashdata('message', 'Updata Data successfully');
		redirect("smartindo/topupdemand");
    }

    function getno_demand($member)
    {
        return $this->db->query("SELECT DISTINCT a.parent_upload FROM topupbydemand_detail a
									JOIN topupbydemand b ON a.`parent_upload` = b.`topupno`
									WHERE stockiest_id = '" . $member . "' AND b.`status` = 1 and qty > 0 GROUP BY a.parent_upload")->result_array();
    }


    function stcid_demand($member,$stc_id)
    {
        return $this->db->query("SELECT DISTINCT a.parent_upload,qty FROM topupbydemand_detail a
        JOIN topupbydemand b ON a.`parent_upload` = b.`topupno`
        WHERE stockiest_id = '" . $stc_id . "' and member_id = '" . $member . "' and a.is_active = '1' and qty = 0 GROUP BY a.parent_upload")->result_array();
    }

    function source($member,$getno)
    {
        $k = [];
        for ($i = 0; $i < count($getno); $i++) {
            $k[$i] =  "'" . $getno[$i]['parent_upload'] . "'" . ',';
        }
        $b =  implode(',', $k);
        $c =  str_replace(',,', ',', $b);
        $d =  rtrim($c, ',');
        $topup = "parent_upload IN ($d)";
        $qry = "SELECT SUM(qty) as qty_sum,item_code,parent_upload from topupbydemand_detail where $topup AND stockiest_id = '" . $member . "'  and qty > 0  GROUP BY parent_upload";
        return $this->db->query($qry)->result();
    }



    function source_so($member,$getno,$stc_id)
    {
        $k = [];
        for ($i = 0; $i < count($getno); $i++) {
            $k[$i] =  "'" . $getno[$i]['parent_upload'] . "'" . ',';
        }
        $b =  implode(',', $k);
        $c =  str_replace(',,', ',', $b);
        $d =  rtrim($c, ',');
        $topup = "parent_upload IN ($d)";
        $qry = "SELECT SUM(qty) as qty_sum,item_code,parent_upload from topupbydemand_detail where $topup AND stockiest_id = '" . $stc_id . "' and  member_id = '" . $member . "' and is_active = '1'  GROUP BY parent_upload";
        return $this->db->query($qry)->result();
    }
    
    	function get_member($id)
	{
		return $this->db->query("SELECT * FROM member WHERE id = '" . $id . "'")->row()->nama; 
	}

}

/* End of file ModelName.php */
