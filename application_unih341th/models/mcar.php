<?php
class MCar extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function getCarRewardList($keywords=0,$num,$offset, $flag){
        $data = array();
		if($flag==1){
			$q1 = " AND cl.created_date < LAST_DAY(NOW()-INTERVAL 1 MONTH) ";
		}else{
			$q1 = " AND cl.created_date >= LAST_DAY(NOW()-INTERVAL 1 MONTH) ";
		}
        $q = "
			SELECT cl.id, cl.member_id, m.nama, j.name AS jenjang
				, CASE WHEN cl.ccb_cop = 1 THEN 'CCB' ELSE 'COP' END AS tipe
				, CASE WHEN cl.expired = 1 THEN 'Inactive' ELSE 'Active' END AS status_
				, cl.dp
				, cl.flag_ccb AS ccbCount
				, cl.ccb
				, cl.flag_cop AS copCount
				, cl.cop
				, cop_start
			FROM car_reward_list cl
			LEFT JOIN member m ON cl.member_id = m.id
			LEFT JOIN jenjang j ON cl.jenjang = j.id
			WHERE (cl.member_id LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')
			".$q1."
			LIMIT $num $offset
		";
        $q = $this->db->query($q);
        // echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function countCarRewardList($keywords=0,$flag){
        $data = array();
		if($flag==1){
			$q1 = " AND cl.created_date < LAST_DAY(NOW()-INTERVAL 1 MONTH) ";
		}else{
			$q1 = " AND cl.created_date >= LAST_DAY(NOW()-INTERVAL 1 MONTH) ";
		}
        $q = "
			SELECT cl.id, cl.member_id, m.nama, j.name AS jenjang
				, CASE WHEN cl.ccb_cop = 1 THEN 'CCB' ELSE 'COP' END AS tipe
				, CASE WHEN cl.expired = 1 THEN 'Inactive' ELSE 'Active' END AS status_
				, cl.flag_ccb AS ccbCount
				, cl.ccb
				, cl.flag_cop AS copCount
				, cl.cop
				, cop_start
			FROM car_reward_list cl
			LEFT JOIN member m ON cl.member_id = m.id
			LEFT JOIN jenjang j ON cl.jenjang = j.id
			WHERE (cl.member_id LIKE '%$keywords%' OR m.nama LIKE '%$keywords%')
			".$q1."
		";
        $q = $this->db->query($q);
        return $this->db->count_all_results();
    }
	
	public function getDetail($id){
        $data=array();
        $q = "
			SELECT cl.id, cl.member_id, m.nama, j.name AS jenjang
				, CASE WHEN cl.ccb_cop = 1 THEN 'CCB' ELSE 'COP' END AS tipe
				, CASE WHEN cl.expired = 1 THEN 'Inactive' ELSE 'Active' END AS status_
				, cl.expired
				, cl.dp
				, cl.flag_ccb AS ccbCount
				, cl.ccb
				, cl.ccb_start
				, cl.flag_cop AS copCount
				, cl.cop
				, cl.cop_start
			FROM car_reward_list cl
			LEFT JOIN member m ON cl.member_id = m.id
			LEFT JOIN jenjang j ON cl.jenjang = j.id
			WHERE cl.id = '$id'
		";
        $q = $this->db->query($q);
        // echo $this->db->last_query();
		if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
	
	public function edit(){
		$data=array(
			'ccb_cop' =>  $this->input->post('tipe')
			,'cop' =>  $this->input->post('cop_val')
			,'dp' =>  $this->input->post('cop_dp')
			,'expired' =>  $this->input->post('status')
        );
		$this->db->update('car_reward_list',$data,array('id'=>$this->input->post('id_')));
    }    
}
?>