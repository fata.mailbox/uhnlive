<?php
class MSc_retur extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Retur Stockiest Consignment
    |--------------------------------------------------------------------------
    |
    | @author budi@nawadata.com
    | @poweredby www.nawadata.com
    | @created 2019-01-21
    |
    */
	
	public function searchPinjaman_Retur($keywords=0,$num,$offset){
        $data = array();
        $whsid = $this->session->userdata('whsid');
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%') AND dt.qty_total > 0";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) AND dt.qty_total > 0";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) AND dt.qty_total > 0";
					else
						$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) and a.warehouse_id = '".$this->session->userdata('keywords_whsid')."' AND dt.qty_total > 0";
						
			}
		}
        //echo $where;
        $this->db->select("a.id,a.status_ho,a.status_hub,
		a.tglapproved_ho,a.tglapproved_hub ,
		a.approvedby_ho,a.approvedby_hub ,date_format(a.tgl,'%d-%b-%Y')as tgl,
		a.createdby,a.stockiest_id,s.no_stc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,
		m.id as member_id,
		m.nama,
		m.kecamatan,
		m.kelurahan,
		m.alamat,
		m.kodepos,
		m.kota_id,
		m.ewallet,
		m.hp,
		s.kota_delivery,
		s.type AS tipe,
			CASE
		WHEN s.type = 1 THEN
			6
		WHEN s.type = 2 THEN
			2
		ELSE
			0
		END AS persen ,
		(SELECT format(SUM(pinjaman_d.qty_total),0) as qty_total FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) as qty_total,
		(SELECT format(SUM(pinjaman_d.qty_retur),0) as qty_retur FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) as qty_retur,
		(SELECT format(SUM(pinjaman_d.qty_lunas),0) as qty_lunas FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) as qty_lunas,
		(SELECT format(SUM(pinjaman_d.qty),0) as qty FROM pinjaman_d WHERE pinjaman_d.pinjaman_id=a.id) as qty,
		a.remark,
		w.name as warehouse_name, w.id as warehouse_id,  
		ifnull(date_format(a.tglapproved,'%d-%b-%Y %T'),'-')as appdate,
		ifnull(date_format(a.tglapproved_ho,'%d-%b-%Y %T'),'-')as appdate_ho,
		ifnull(date_format(a.tglapproved_hub,'%d-%b-%Y %T'),'-')as appdate_hub,
		a.status,
		IF(a.approvedby = '' OR a.approvedby IS NULL , '-',a.approvedby ) as approvedby",false);
        $this->db->from('pinjaman a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
		$this->db->join('(
						SELECT pinjaman_d.pinjaman_id, FORMAT(SUM(pinjaman_d.qty_total),0) AS qty_total FROM pinjaman_d
						GROUP BY pinjaman_id
						) AS dt	'
						, 'dt.pinjaman_id = a.id'
						,'left');
		// START ASP 20180525
		$this->db->join('warehouse w', 'a.warehouse_id=w.id','left');
		// EOF ASP 20180525
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        $xSql= $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
				if ($row['status_ho']=='1' AND $row['status_hub']=='1' ){
					$cSql="UPDATE pinjaman SET 
					remarkapp=CONCAT(remarkapp,' Change to Delivery '), 
					status='delivery', 
					approvedby='Pusat,HUB', 
					tglapproved='".date('Y-m-d H:i:s', now())."' 
					WHERE id='".$row['id']."'";
					$query = $this->db->query($cSql);
				}
				
                //$data[] = $row;
				//echo $row['approvedby'].'<br>';
            }
        }
		//echo $xSql;
		$query = $this->db->query($xSql);
        return $query->result_array();
		
    }
	
	public function countPinjaman_Retur($keywords=0){
        $whsid = $this->session->userdata('whsid');
		
        if($whsid > 1){
			$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%') AND dt.qty_total > 0";
		}else{
			if(!$this->session->userdata('keywords_whsid')){
				$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) AND dt.qty_total > 0";
			}else{
					if($this->session->userdata('keywords_whsid')=='all')
						$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) AND dt.qty_total > 0";
					else
						$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' ) and a.warehouse_id = '".$this->session->userdata('keywords_whsid')."' AND dt.qty_total > 0";
						//$where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			}
		}
        $this->db->select("a.id");
        $this->db->from('pinjaman a');
		$this->db->join('(
						SELECT pinjaman_d.pinjaman_id, FORMAT(SUM(pinjaman_d.qty_total),0) AS qty_total FROM pinjaman_d
						GROUP BY pinjaman_id
						) AS dt	'
						, 'dt.pinjaman_id = a.id'
						,'left');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->where($where);
		$query = $this->db->get();
		//echo $this->db->last_query();
        //return $this->db->count_all_results();
		return $query->num_rows();;
    }
   
    
    public function searchReturPinjaman($keywords=0,$num,$offset){
        $data = array();
        $whsid = $this->session->userdata('whsid');
        if($whsid > 1)$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
        else $where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        
        $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.createdby,a.stockiest_id,s.no_stc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama,a.remark",false);
        $this->db->from('retur_pinjaman a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countReturPinjaman($keywords=0){
        $whsid = $this->session->userdata('whsid');
        if($whsid > 1)$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
        else $where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        
        $this->db->select("a.id");
        $this->db->from('retur_pinjaman a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
	public function getDropDownWhs($all=''){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            if($all == 'all')$data['all']='All Cabang';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
	
    
    public function addReturTitipanTemp(){
        $member_id = $this->input->post('member_id');
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
        
        $empid = $this->session->userdata('user');
        $whsid = $this->input->post('whsid');
        $sc_id = $this->input->post('sc_id');
       
		
		if (empty($this->db->escape_str($this->input->post('remark')))){
			$var_remark="Created By System in ".date('Y-m-d H:i:s');
		}else{
			$var_remark=$this->db->escape_str($this->input->post('remark'));
		}
		
        $data_ibu_asu = array(
            'pinjaman_id' => $sc_id,
            'stockiest_id' => $member_id,
            'tgl' => date('Y-m-d',now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'warehouse_id' => $whsid,
            'remark'=>$var_remark,
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('retur_titipan_temp',$data_ibu_asu);
		//echo '<br>'.$this->db->last_query();
        $id = $this->db->insert_id();
		
        return $id;
    }
	
	public function addReturTitipanTempDet($id){
		$empid = $this->session->userdata('user');
        $sc_id = $this->input->post('sc_id');
		
		$myData=$this->input->post('iRow');
		for ($i = 0; $i <= ($myData-1); $i++) {
			
			if (!empty($this->input->post('qty_lunas'.$i))){
				$data_anak_asu=array(
					'retur_titipan_id' => $id,
					'pinjaman_id' => $sc_id,
					'pinjaman_d_id' => $this->input->post('pin_d'.$i),
					'item_id' => $this->input->post('itemcode'.$i),
					'qty' => $this->input->post('qty_lunas'.$i),
					'harga' => str_replace(",","",$this->input->post('price'.$i)),
					'pv' => str_replace(",","",$this->input->post('pv'.$i)),
					'jmlharga' => str_replace(".","",$this->input->post('subtotal'.$i)),
					'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$i))
				);
				$this->db->insert('retur_titipan_temp_d',$data_anak_asu);
				//echo '<br>'.$this->db->last_query();
			}
		}
	}
	
    public function check_retur_pinjaman($id,$stcid)
    {
        $data=array();
        $q = $this->db->query("SELECT f_check_retur_pinjaman('$id','$stcid') as l_result");
        //$q = $this->db->query("SELECT f_check_sc_retur_pinjaman('$id','$stcid') as l_result");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
		//echo $this->db->last_query();
        $q->free_result();
        return $data['l_result'];
    }
	
    public function addReturPinjaman($soid)
    {
        $empid = $this->session->userdata('user');
        $this->db->query("call sp_sc_retur_pinjaman('".$soid."','".$empid."')");
		
		$this->db->query("UPDATE pinjaman_d SET qty_total=(IFNULL(qty,0)-IFNULL(qty_retur,0)-IFNULL(qty_lunas,0))");
    }
	
    public function getReturPinjaman($id=0){
        $data = array();
        $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.stockiest_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    s.no_stc,m.nama,m.alamat,m.kodepos,k.name as kota,p.name as propinsi,w.name as warehouse",false);
        $this->db->from('retur_pinjaman a');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->join('warehouse w','a.warehouse_id=w.id','left');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getReturPinjamanDetail($id=0){
        $data = array();
        $this->db->select("d.item_id,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.pv,0)as fpv,format(sum(d.qty*d.harga),0)as fsubtotal,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name",false);
        $this->db->from('retur_pinjaman_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.retur_pinjaman_id',$id);
        $this->db->group_by('d.id');
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	
	public function getPinjamanDetail($id){
        $data = array();
        $this->db->select("d.id,
		d.item_id,
		d.warehouse_id,
		format(d.qty, 0) AS fqty,
		IFNULL(d.qty_retur, 0) AS fqty_retur,
		IFNULL(d.qty_lunas, 0) AS fqty_lunas,
		format(d.harga,0)as fharga,
		format(d.pv,0)as fpv,
		format((d.qty - IFNULL(d.qty_retur, 0) )* d.harga, 0) AS fsubtotal,
		format((d.qty - IFNULL(d.qty_retur, 0) )* d.pv, 0) AS fsubtotalpv,
		a.name",false);
        $this->db->from('pinjaman_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.pinjaman_id',$id);
        $this->db->group_by('d.id');
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function search_FreeItem($keywords=0,$num,$offset){
		$data = array();
		/*
		$this->db->select("s.item_id,i.name,s.qty,format(s.qty,0)as fqty,i.price,format(i.price,0)as fprice,i.price2,format(i.price2,0)as fprice2,i.pv,format(i.pv,0)as fpv,i.bv",false);
		$this->db->from('stock s');
		$this->db->join('item i','s.item_id=i.id','left');
		*/
		$cSql="
		SELECT
		a.*, 
		b.item_id,
		c.`name` AS item_name,
		( SELECT c. NAME AS free_item_name FROM item c WHERE c.id = d.free_item_id ) AS Free_Item_Name, 
		b.warehouse_id,
		d.item_qty,
		d.free_item_id,
		d.free_item_qty
		FROM
		pinjaman_nc a 
		INNER JOIN pinjaman_d b ON b.id = a.pinjaman_d_id
		INNER JOIN item c ON c.id = b.item_id
		INNER JOIN promo d ON d.id = a.promo_id
		";
		
		$this->db->select("
		a.*, 
		b.item_id,
		c.`name` AS item_name,
		( SELECT c. NAME AS free_item_name FROM item c WHERE c.id = d.free_item_id ) AS Free_Item_Name, 
		b.warehouse_id,
		d.item_qty,
		d.free_item_id,
		d.free_item_qty,
		a.qty as GetFree, 
		r.retur_pinjaman_id 
		",false);
		$this->db->from('pinjaman_nc a');
		$this->db->join('pinjaman_d b','b.id = a.pinjaman_d_id','LEFT');
		$this->db->join('retur_pinjaman_d r','r.pinjaman_d_id = a.pinjaman_d_id','LEFT');
		$this->db->join('item c','c.id = b.item_id','LEFT');
		$this->db->join('promo d','d.id = a.promo_id','LEFT');
		$this->db->like('r.retur_pinjaman_id', $keywords, 'after');
		$this->db->order_by('c.`name`','asc');
		$this->db->limit($num,$offset);
		$q = $this->db->get();
		//echo $this->db->last_query();
		if($q->num_rows > 0){
		  foreach($q->result_array() as $row){
			$data[] = $row;
		  }
		}
		$q->free_result();
		return $data;
	}
	
	public function getPriceItem($whs,$item){
		$sql = "SELECT i.id AS item_id
					, i.name
					, i.price,FORMAT(i.price,0)AS fprice
					, i.price2,FORMAT(i.price2,0)AS fprice2
					, i.pv,FORMAT(i.pv,0)AS fpv
					, i.bv
					, i.hpp
					, CASE
						WHEN i.type_id = 1 THEN 'SO4'
						WHEN i.type_id = 2 THEN 'SO1'
						WHEN i.type_id = 3 THEN 'SO5'
						ELSE 'SO4'
					  END AS event_id
					, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
					, CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
		  FROM item i
		  LEFT JOIN item_rule_order_promo ir ON i.id =ir.item_id AND ir.expireddate >= DATE(NOW())
		  LEFT JOIN item_non_stc_fee inon ON i.id =inon.id
		  LEFT JOIN(	SELECT i.id
								, IFNULL(ind.price_not_disc,0) AS price_not_disc
								, MIN(ind.end_period) AS end_period
								FROM
								item i
								LEFT JOIN item_not_discount ind ON ind.item_id = i.id
									AND ind.expired = 0
									AND ind.end_period >= DATE(NOW())
								GROUP BY i.id
							) AS dt ON i.id = dt.id 
		  WHERE 
		  i.id = '".$item."'
		  ORDER BY NAME ASC
		  ";
		  
		  /*
		  $sql="
		  SELECT
			s.item_id
			,i.name
			,s.qty,FORMAT(s.qty,0)AS fqty,i.price,FORMAT(i.price,0)AS fprice,i.price2,FORMAT(i.price2,0)AS fprice2
			,i.pv,FORMAT(i.pv,0)AS fpv
			,i.bv, CASE WHEN ir.min_order IS NULL THEN 1 ELSE ir.min_order END AS min_order
			, CASE WHEN inon.id IS NOT NULL THEN 1 ELSE 0 END AS nonstcfee, price_not_disc
			, w.name AS st0ck_wh, w.id as whsid,  i.warehouse_id
			FROM item i
			LEFT JOIN stock s ON s.item_id=i.id AND s.warehouse_id = i.warehouse_id
			LEFT JOIN item_rule_order_promo ir ON s.item_id=ir.item_id AND ir.expireddate >= DATE(NOW())
			LEFT JOIN item_non_stc_fee inon ON s.item_id=inon.id
			LEFT JOIN warehouse w ON s.warehouse_id = w.id
			LEFT JOIN(
								SELECT
								i.id
								, IFNULL(ind.price_not_disc,0) AS price_not_disc
								, MIN(ind.end_period) AS end_period
								FROM
								item i
								LEFT JOIN item_not_discount ind ON ind.item_id = i.id AND ind.expired = 0 AND ind.end_period >= DATE(NOW())
								GROUP BY i.id
							) AS dt ON s.item_id = dt.id
			WHERE  s.warehouse_id = 1  
			AND i.sales = 'Yes' AND (i.name LIKE '%".$item."%' OR  i.id LIKE '%".$item."%') 
			ORDER BY NAME ASC
		  ";
		  */
		$q = $this->db->query($sql);
		if($q->num_rows > 0){
		  foreach($q->result_array() as $row){
			$data[] = $row;
		  }
		}
		//$q->free_result();
		//echo $this->db->last_query();
		return $data;
	}
    
 
 
}?>