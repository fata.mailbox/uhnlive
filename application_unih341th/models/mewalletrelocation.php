<?php
class Mewalletrelocation extends CI_Model{
    function __construct(){
        parent::__construct();
    }
	
	public function getGrantEwallet($keyword, $offset, $num){
        $data = array();
		if($num){$num=$num.', ';}
		$query = "
			SELECT CONCAT(mg.source_id, ' / ', sr.nama)AS src, CONCAT(mg.destination_id, ' / ', des.nama)AS dsc
				, CASE WHEN mg.active = 1 THEN 'Transfer to other member'
					WHEN mg.active = 2 THEN 'To Ewallet Stockiest'
					ELSE 'Inactive'
				END AS note
				, CASE WHEN mg.created > mg.updated THEN mg.created ELSE mg.updated END AS created
				, CASE WHEN mg.created > mg.updated THEN mg.createdby ELSE mg.updatedby END AS createdby
			FROM member_grant mg
			LEFT JOIN member sr ON mg.source_id = sr.id
			LEFT JOIN member des ON mg.destination_id = des.id
			WHERE sr.id LIKE '%$keyword%' OR sr.nama LIKE '%$keyword%'
			ORDER BY mg.updated DESC, mg.id DESC
			LIMIT $num $offset
		";
		$qry = $this->db->query($query);
		// echo $this->db->last_query();
        if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data[]=$row;
			}
        }
		// $data['jml'] = $qry->num_rows();
		$qry->free_result();
		return $data;
    }
	public function countGrantEwallet($keyword){
        $data = array();
		$query = "
			SELECT CONCAT(mg.source_id, '/', sr.nama)AS src, CONCAT(mg.destination_id, '/', des.nama)AS dsc
				, CASE WHEN mg.active = 2 THEN 'To Other Ewallet'
					WHEN mg.active = 1 THEN 'Transfer to other member'
					ELSE 'Inactive'
				END AS note
			FROM member_grant mg
			LEFT JOIN member sr ON mg.source_id = sr.id
			LEFT JOIN member des ON mg.destination_id = des.id
			WHERE sr.id LIKE '%$keyword%' OR sr.nama LIKE '%$keyword%'
			ORDER BY mg.id DESC
		";
		$qry = $this->db->query($query);
		// echo $this->db->last_query();
		$a = $qry->num_rows();
		$qry->free_result();
		return $a;
    }
	
	public function getGrantType(){
        $data = array();
		$data[0] = 'Inactive';
		$data[1] = 'Transfer to another member';
		$data[2] = 'Give to ewallet Stc';
        return $data;
    }
	
	public function createGrant(){
		$member_id = $this->input->post('member_id0');
		$dest_id = $this->input->post('member_id1');
		$type_id = $this->input->post('type');
		$empId = $this->session->userdata('username');
		
		if($type_id == 1){
			$this->db->query("CALL sp_grants_wdw_1('$member_id','$dest_id','$empId'); ");
		}elseif($type_id == 2){
			$this->db->query("CALL sp_grants_1('$member_id','$dest_id','$empId'); ");
		}else{
			$this->db->query("UPDATE member_grant SET active = '0', updated = NOW(), updatedby = '$empId' WHERE source_id = '$member_id'; ");
		}
    }
}
?>