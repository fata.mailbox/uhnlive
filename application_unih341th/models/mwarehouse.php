<?php
class MWarehouse extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Master Warehouse
    |--------------------------------------------------------------------------
    |
    | to sign warehouse
    |
    | @created 2009-03-29
    | @author qtakwa@yahoo.com@yahoo.com
    |
    */
    public function searchWarehouse($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.name,a.address,a.telp,a.fax",false);
        $this->db->from('warehouse a');
        $this->db->like('a.name',$keywords,'after');
        $this->db->order_by('a.name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countWarehouse($keywords=0){
        $this->db->like('a.name',$keywords,'after');
        $this->db->from('warehouse a');
        return $this->db->count_all_results();
    }
    public function addWarehouse(){
        $data=array(
            'name' => $this->input->post('name'),
            'address' => $this->input->post('address'),
            'telp' => $this->input->post('telp'),
            'fax' => $this->input->post('fax'),
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $this->session->userdata('user')
        );
            
        $this->db->insert('warehouse',$data);
        
        $id = $this->db->insert_id();
        $this->db->query("call sp_warehouse('$id')");
    }
       
    public function getWarehouse($id){
        $data=array();
        $q = $this->db->get_where('warehouse',array('id'=>$id));
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function editWarehouse(){
        $data=array(
            'name' => $this->input->post('name'),
            'address' => $this->input->post('address'),
            'telp' => $this->input->post('telp'),
            'fax' => $this->input->post('fax'),
            'updated' => date('Y-m-d H:i:s',now()),
            'updatedby' => $this->session->userdata('user')
        );
            
        $this->db->update('warehouse',$data,array('id'=>$this->input->post('id')));
    }
    
}?>