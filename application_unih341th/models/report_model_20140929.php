<?php
class Report_model extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function report_convert_to_excel($type_id){
        $data = array();
        $date1 = date('Y-m-d',now());
            
        if($type_id == 'royalty_magozai'){
            $query = "SELECT so.tgl, sod.so_id, sod.item_id, i.name AS product, i.manufaktur, IFNULL(m.item_id,'-')AS item, sod.harga
                    , CASE WHEN m.qty IS NULL THEN sod.qty ELSE sod.qty * m.qty END AS qty
                    FROM so_d sod
                    LEFT JOIN so ON sod.so_id = so.id
                    LEFT JOIN item i ON sod.item_id = i.id
                    LEFT JOIN manufaktur m ON i.id = m.manufaktur_id
                    WHERE 
                    so.tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                    AND (sod.item_id IN ('NT020003')OR m.item_id = 'NT020003')
                    ORDER BY tgl DESC";
        }elseif($type_id == 'so_ke_uhn'){
            $query = "SELECT s.id,s.tgl,member_id,sod.item_id,i.name,qty, sod.jmlharga
                    FROM so_d sod
                    LEFT JOIN so s ON sod.so_id=s.id 
                    LEFT JOIN item i ON sod.item_id=i.id
                    LEFT JOIN member m ON s.member_id=m.id
                    WHERE s.tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH) AND s.stockiest_id=0";
        }elseif($type_id == 'ro_stc'){
            $query = "SELECT s.id,s.date,s.member_id,sod.item_id,i.name,qty,(sod.harga_*qty)AS totalsales,(sod.harga_*qty)-sod.jmlharga AS diskon,sod.jmlharga AS netsales
                    FROM ro_d sod
                    LEFT JOIN ro s ON sod.ro_id=s.id 
                    LEFT JOIN item i ON sod.item_id=i.id
                    LEFT JOIN stockiest stc ON s.member_id=stc.id
                    WHERE s.date BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH) AND s.stockiest_id=0 AND stc.type='1'
                    ";
        }elseif($type_id == 'ro_mstc'){
            $query = "SELECT s.id,s.date,s.member_id,sod.item_id,i.name,qty,(sod.harga_*qty)AS totalsales,(sod.harga_*qty)-sod.jmlharga AS diskon,sod.jmlharga AS netsales
                FROM ro_d sod
                LEFT JOIN ro s ON sod.ro_id=s.id 
                LEFT JOIN item i ON sod.item_id=i.id
                LEFT JOIN stockiest stc ON s.member_id=stc.id
                WHERE s.date BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH) AND s.stockiest_id=0 AND stc.type='2'
            ";
        }elseif($type_id == 'scp'){
            $query = "SELECT s.id,s.stockiest_id,m.nama,s.tgl,sod.item_id,i.name,qty,(sod.harga*qty)AS total
                FROM pinjaman_titipan_d sod
                LEFT JOIN pinjaman_titipan s ON sod.pinjaman_titipan_id=s.id 
                LEFT JOIN item i ON sod.item_id=i.id
                LEFT JOIN member m ON s.stockiest_id=m.id
                LEFT JOIN stockiest stc ON s.stockiest_id=stc.id
                WHERE s.tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH) 
                ";
        }elseif($type_id == 'retur'){
            $query = "SELECT s.id,s.stockiest_id,m.nama,s.tgl,sod.item_id,i.name,qty,(sod.harga*qty)AS total
                FROM retur_titipan_d sod
                LEFT JOIN retur_titipan s ON sod.retur_titipan_id=s.id
                LEFT JOIN item i ON sod.item_id=i.id
                LEFT JOIN member m ON s.stockiest_id=m.id
                LEFT JOIN stockiest stc ON s.stockiest_id=stc.id
                WHERE s.tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)";
        }else if($type_id == 'member_transaction_history'){
            $petik = "\'";
			//20140828 ASP Start
			$bln_query = '';
			$curr_month = (integer)date('m');
			$year = date('Y') -1;
			for($i=12;$i>0;$i--){
				if($curr_month==13){
					$curr_month=1;
					$year++;
				}
				$bln_query.=", IFNULL(dt3.bln_".$curr_month.",0)AS 'bln_".$i." (".date('M',mktime(0,0,0,$curr_month,1,$year))." ".$year.")'";
				$curr_month++;
			}
			//echo $bln_query;
			//20140828 ASP End
            $query = "SELECT concat('".$petik."',dt.member_id)as member_id, dt.nama, m.alamat, IFNULL(k.name,'-')AS kota, concat('".$petik."',m.telp)as telp
                    , concat('".$petik."',m.hp)as hp, dt.joindate, me.id, me.nama AS nama_sponsor, j.decription
					".$bln_query."
                    -- , IFNULL(dt3.bln_11,0)AS bln_12
                    -- , IFNULL(dt3.bln_12,0)AS bln_11
                    -- , IFNULL(dt3.bln_1,0)AS bln_10
                    -- , IFNULL(dt3.bln_2,0)AS bln_9
                    -- , IFNULL(dt3.bln_3,0)AS bln_8
                    -- , IFNULL(dt3.bln_4,0)AS bln_7
                    -- , IFNULL(dt3.bln_5,0)AS bln_6
                    -- , IFNULL(dt3.bln_6,0)AS bln_5
                    -- , IFNULL(dt3.bln_7,0)AS bln_4
                    -- , IFNULL(dt3.bln_8,0)AS bln_3
                    -- , IFNULL(dt3.bln_9,0)AS bln_2
                    -- , IFNULL(dt3.bln_10,0)AS bln_1
                    , IFNULL(dt3.bln_1,0) + IFNULL(dt3.bln_2,0) + IFNULL(dt3.bln_3,0) + IFNULL(dt3.bln_4,0) + IFNULL(dt3.bln_5,0) + IFNULL(dt3.bln_6,0)
                    + IFNULL(dt3.bln_7,0) + IFNULL(dt3.bln_8,0) + IFNULL(dt3.bln_9,0) + IFNULL(dt3.bln_10,0) + IFNULL(dt3.bln_11,0) + IFNULL(dt3.bln_12,0)
                            AS total
            FROM( 
                    SELECT member_id,nama,joindate
                    FROM v_memberjoin
            )AS dt
            LEFT JOIN (
                    SELECT member_id
                            , IFNULL(SUM(bln1),0)AS bln_1	, IFNULL(SUM(bln2),0)AS bln_2	, IFNULL(SUM(bln3),0)AS bln_3
                            , IFNULL(SUM(bln4),0)AS bln_4	, IFNULL(SUM(bln5),0)AS bln_5	, IFNULL(SUM(bln6),0)AS bln_6
                            , IFNULL(SUM(bln7),0)AS bln_7	, IFNULL(SUM(bln8),0)AS bln_8	, IFNULL(SUM(bln9),0)AS bln_9
                            , IFNULL(SUM(bln10),0)AS bln_10	, IFNULL(SUM(bln11),0)AS bln_11	, IFNULL(SUM(bln12),0)AS bln_12		
                    FROM (
                            SELECT member_id
                                    ,CASE WHEN bln=1 THEN totalharga END AS bln1	,CASE WHEN bln=2 THEN totalharga END AS bln2	,CASE WHEN bln=3 THEN totalharga END AS bln3
                                    ,CASE WHEN bln=4 THEN totalharga END AS bln4	,CASE WHEN bln=5 THEN totalharga END AS bln5	,CASE WHEN bln=6 THEN totalharga END AS bln6
                                    ,CASE WHEN bln=7 THEN totalharga END AS bln7	,CASE WHEN bln=8 THEN totalharga END AS bln8	,CASE WHEN bln=9 THEN totalharga END AS bln9
                                    ,CASE WHEN bln=10 THEN totalharga END AS bln10	,CASE WHEN bln=11 THEN totalharga END AS bln11	,CASE WHEN bln=12 THEN totalharga END AS bln12
                            FROM(
                                    SELECT s.member_id,IFNULL(SUM(sod.jmlharga_),0)AS totalharga,MONTH(s.tgl)AS bln	
                                    FROM so_d sod
                                    LEFT JOIN so s ON sod.so_id=s.id
                                    WHERE s.tgl BETWEEN (LAST_DAY((NOW() - INTERVAL 1 YEAR)- INTERVAL 1 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                                    AND s.totalpv<>0
                                    GROUP BY s.member_id,MONTH(s.tgl)
                            )AS dt1
                    )AS dt2 
                    GROUP BY member_id
            )AS dt3 ON dt.member_id=dt3.member_id
            LEFT JOIN member m ON dt.member_id=m.id
            LEFT JOIN kota k ON m.kota_id=k.id
            LEFT JOIN member me ON m.sponsor_id=me.id
            LEFT JOIN jenjang j ON me.jenjang_id=j.id
            WHERE IFNULL(dt3.bln_2,0)+IFNULL(dt3.bln_3,0)+IFNULL(dt3.bln_4,0)
                    +IFNULL(dt3.bln_5,0)+IFNULL(dt3.bln_6,0)+IFNULL(dt3.bln_7,0)
                    +IFNULL(dt3.bln_8,0)+IFNULL(dt3.bln_9,0)+IFNULL(dt3.bln_10,0)
                    +IFNULL(dt3.bln_11,0)+IFNULL(dt3.bln_12,0)+IFNULL(dt3.bln_1,0)<>0
            ORDER BY total DESC";
			//echo $query;
        }elseif($type_id == 'member_active_old_1_tahun'){
            $query = "SELECT dt.member_id, dt.nama, m.alamat, IFNULL(k.name,'-')AS kota
                    , m.telp, m.hp, dt.joindate, me.id, me.nama, j.decription
                    
                    , IFNULL(dt3.bln_11,0)AS bln_11_2012
                    , IFNULL(dt3.bln_12,0)AS bln_12_2012
                    , IFNULL(dt3.bln_1,0)AS bln_1_2013
                    , IFNULL(dt3.bln_2,0)AS bln_2_2013
                    , IFNULL(dt3.bln_3,0)AS bln_3_2013
                    , IFNULL(dt3.bln_4,0)AS bln_4_2013
                    , IFNULL(dt3.bln_5,0)AS bln_5_2013
                    , IFNULL(dt3.bln_6,0)AS bln_6_2013
                    , IFNULL(dt3.bln_7,0)AS bln_7_2013
                    , IFNULL(dt3.bln_8,0)AS bln_8_2013
                    , IFNULL(dt3.bln_9,0)AS bln_9_2013
                    , IFNULL(dt3.bln_10,0)AS bln_10_2013
                    , IFNULL(dt3.bln_1,0)	+ IFNULL(dt3.bln_2,0)	+ IFNULL(dt3.bln_3,0)	+ IFNULL(dt3.bln_4,0)	+ IFNULL(dt3.bln_5,0)	+ IFNULL(dt3.bln_6,0)
                    + IFNULL(dt3.bln_7,0)	+ IFNULL(dt3.bln_8,0)	+ IFNULL(dt3.bln_9,0)	+ IFNULL(dt3.bln_10,0)	+ IFNULL(dt3.bln_11,0)	+ IFNULL(dt3.bln_12,0)
                    AS total
            FROM( 
                    SELECT member_id,nama,joindate
                    FROM v_memberjoin
                    WHERE joindate BETWEEN '2009-01-01' AND (LAST_DAY((NOW() - INTERVAL 1 YEAR)- INTERVAL 1 MONTH))
            )AS dt
            LEFT JOIN (
                    SELECT member_id
                            ,IFNULL(SUM(bln1),0)AS bln_1	,IFNULL(SUM(bln2),0)AS bln_2	,IFNULL(SUM(bln3),0)AS bln_3
                            ,IFNULL(SUM(bln4),0)AS bln_4	,IFNULL(SUM(bln5),0)AS bln_5	,IFNULL(SUM(bln6),0)AS bln_6
                            ,IFNULL(SUM(bln7),0)AS bln_7	,IFNULL(SUM(bln8),0)AS bln_8	,IFNULL(SUM(bln9),0)AS bln_9
                            ,IFNULL(SUM(bln10),0)AS bln_10	,IFNULL(SUM(bln11),0)AS bln_11	,IFNULL(SUM(bln12),0)AS bln_12
                    FROM (
                            SELECT member_id
                                    ,CASE WHEN bln=1 THEN totalharga END AS bln1
                                    ,CASE WHEN bln=2 THEN totalharga END AS bln2
                                    ,CASE WHEN bln=3 THEN totalharga END AS bln3
                                    ,CASE WHEN bln=4 THEN totalharga END AS bln4
                                    ,CASE WHEN bln=5 THEN totalharga END AS bln5
                                    ,CASE WHEN bln=6 THEN totalharga END AS bln6
                                    ,CASE WHEN bln=7 THEN totalharga END AS bln7
                                    ,CASE WHEN bln=8 THEN totalharga END AS bln8
                                    ,CASE WHEN bln=9 THEN totalharga END AS bln9
                                    ,CASE WHEN bln=10 THEN totalharga END AS bln10
                                    ,CASE WHEN bln=11 THEN totalharga END AS bln11
                                    ,CASE WHEN bln=12 THEN totalharga END AS bln12
                            FROM(
                                    SELECT s.member_id,IFNULL(SUM(sod.jmlharga_),0)AS totalharga,MONTH(s.tgl)AS bln	
                                    FROM so_d sod
                                    LEFT JOIN so s ON sod.so_id=s.id
                                    WHERE s.tgl BETWEEN -- '2012-05-01' AND '2013-04-31' 
                                    (LAST_DAY((NOW() - INTERVAL 1 YEAR)- INTERVAL 1 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                                    AND s.totalpv<>0
                                    GROUP BY s.member_id,MONTH(s.tgl)
                            )AS dt1
                    )AS dt2 
                    GROUP BY member_id
            )AS dt3 ON dt.member_id=dt3.member_id
            LEFT JOIN member m ON dt.member_id=m.id
            LEFT JOIN kota k ON m.kota_id=k.id
            LEFT JOIN member me ON m.sponsor_id=me.id
            LEFT JOIN jenjang j ON me.jenjang_id=j.id
            WHERE IFNULL(dt3.bln_1,0)+IFNULL(dt3.bln_2,0)+IFNULL(dt3.bln_3,0)+IFNULL(dt3.bln_4,0)+IFNULL(dt3.bln_5,0)+IFNULL(dt3.bln_6,0)
                    +IFNULL(dt3.bln_7,0)+IFNULL(dt3.bln_8,0)+IFNULL(dt3.bln_9,0)+IFNULL(dt3.bln_10,0)+IFNULL(dt3.bln_11,0)+IFNULL(dt3.bln_12,0)<>0
            ORDER BY total DESC 
        ";
		
        }elseif($type_id == 'recruiting_history'){
            $petik = "\'";
			//20140828 ASP Start
			$bln_query = '';
			$curr_month = (integer)date('m');
			$year = date('Y') -1;
			for($i=12;$i>0;$i--){
				if($curr_month==13){
					$curr_month=1;
					$year++;
				}
				$bln_query.=", IFNULL(SUM(dt.bln".$curr_month."),'0')AS 'bln_".$i." (".date('M',mktime(0,0,0,$curr_month,1,$year))." ".$year.")'";//bln_".$i;
				$curr_month++;
			}
			//echo $bln_query;
			//20140828 ASP End
            $query = "SELECT concat('".$petik."',dt.id_sponsor)as id_sponsor
                    ,dt.nama_sponsor
					".$bln_query."
                    -- ,IFNULL(SUM(dt.bln2),'0')AS bln_12
                    -- ,IFNULL(SUM(dt.bln3),'0')AS bln_11
                    -- ,IFNULL(SUM(dt.bln4),'0')AS bln_10
                    -- ,IFNULL(SUM(dt.bln5),'0')AS bln_9
                    -- ,IFNULL(SUM(dt.bln6),'0')AS bln_8
                    -- ,IFNULL(SUM(dt.bln7),'0')AS bln_7
                    -- ,IFNULL(SUM(dt.bln8),'0')AS bln_6
                    -- ,IFNULL(SUM(dt.bln9),'0')AS bln_5
                    -- ,IFNULL(SUM(dt.bln10),'0')AS bln_4
                    -- ,IFNULL(SUM(dt.bln11),'0')AS bln_3
                    -- ,IFNULL(SUM(dt.bln12),'0')AS bln_2
                    -- ,IFNULL(SUM(dt.bln1),'0')AS bln_1
                    ,IFNULL(SUM(dt.bln1),'0')+IFNULL(SUM(dt.bln2),'0')+IFNULL(SUM(dt.bln3),'0')+IFNULL(SUM(dt.bln4),'0')+IFNULL(SUM(dt.bln5),'0')+IFNULL(SUM(dt.bln6),'0')
                            +IFNULL(SUM(dt.bln7),'0')+IFNULL(SUM(dt.bln8),'0')+IFNULL(SUM(dt.bln9),'0')+IFNULL(SUM(dt.bln10),'0')+IFNULL(SUM(dt.bln11),'0')+IFNULL(SUM(dt.bln12),'0')
                            AS total
            FROM(
                    SELECT dt.id_sponsor
                            ,dt.nama_sponsor
                            ,CASE WHEN dt.bln=1 THEN jml END AS bln1
                            ,CASE WHEN dt.bln=2 THEN jml END AS bln2
                            ,CASE WHEN dt.bln=3 THEN jml END AS bln3
                            ,CASE WHEN dt.bln=4 THEN jml END AS bln4
                            ,CASE WHEN dt.bln=5 THEN jml END AS bln5
                            ,CASE WHEN dt.bln=6 THEN jml END AS bln6
                            ,CASE WHEN dt.bln=7 THEN jml END AS bln7
                            ,CASE WHEN dt.bln=8 THEN jml END AS bln8
                            ,CASE WHEN dt.bln=9 THEN jml END AS bln9
                            ,CASE WHEN dt.bln=10 THEN jml END AS bln10
                            ,CASE WHEN dt.bln=11 THEN jml END AS bln11
                            ,CASE WHEN dt.bln=12 THEN jml END AS bln12
                    FROM (
                            SELECT dt.id_sponsor,dt.nama_sponsor,COUNT(dt.id)AS jml, MONTH(dt.joindate)AS bln
                            FROM(
                                    SELECT mr.id AS id_sponsor,mr.nama AS nama_sponsor,m.id,m.nama,vm.joindate
                                    FROM v_memberjoin vm 
                                    LEFT JOIN member m ON vm.member_id=m.id
                                    LEFT JOIN member mr ON m.enroller_id=mr.id
                                    WHERE vm.joindate BETWEEN--  '2012-05-01' AND '2013-04-31' 
                                    (LAST_DAY((NOW() - INTERVAL 1 YEAR)- INTERVAL 1 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                                    AND mr.id NOT IN ('0',00000001,10000001)
                                    ORDER BY vm.joindate
                            )AS dt
                            GROUP BY dt.id_sponsor,bln
                    )AS dt
            )AS dt
            GROUP BY dt.id_sponsor
            ORDER BY total DESC ";
        }elseif($type_id == 'cetak_stc'){
            $query = "SELECT m.id AS idOmsetStc, m.nama
                        , CASE WHEN s.type = 1 THEN 'Stc' ELSE 'M-Stc' END AS tipe
                        , IFNULL(ro.ro,0)AS ro, IFNULL(scp.scp,0)AS scp, IFNULL(rtr.rtr,0)AS rtr
                        , IFNULL(ro.ro,0)+IFNULL(scp.scp,0)-IFNULL(rtr.rtr,0) AS oms
                        ,ROUND((IFNULL(ro.ro,0)+IFNULL(scp.scp,0)-IFNULL(rtr.rtr,0))/1.1) nettSales
                FROM member m
                LEFT JOIN stockiest s ON m.id = s.id
                LEFT JOIN(
                        SELECT member_id AS id, SUM(totalharga)AS ro
                        FROM ro
                        WHERE `date` BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                        GROUP BY member_id
                )AS ro ON m.id = ro.id
                LEFT JOIN(
                        SELECT stockiest_id AS id, SUM(totalharga)AS scp
                        FROM pinjaman_titipan
                        WHERE tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                        GROUP BY stockiest_id
                )AS scp ON m.id = scp.id
                LEFT JOIN(
                        SELECT stockiest_id AS id, SUM(totalharga)AS rtr
                        FROM retur_titipan
                        WHERE tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                        GROUP BY stockiest_id
                )AS rtr ON m.id = rtr.id
                -- WHERE s.`type` = 1
                HAVING oms > 0
                ORDER BY oms DESC, s.type DESC
                ";
        }elseif($type_id == 'one_hit'){
            $query = "SELECT stc.created, ro.member_id, m.nama, SUM(ro.totalharga)AS omset, IFNULL(bns.jml,0)AS hitOnSemester
                        , CASE WHEN IFNULL(bns.jml,0) = 0 AND SUM(ro.totalharga2-(ro.totalharga2*0.06)) >= 60000000 THEN 'Get' ELSE '' END AS note
                FROM ro
                LEFT JOIN stockiest stc ON ro.member_id = stc.id
                LEFT JOIN member m ON stc.id = m.id
                LEFT JOIN(
                        SELECT member_id, COUNT(id)AS jml
                        FROM bonus
                        WHERE periode BETWEEN '2014-01-01' AND '2014-06-30'
                        AND title = 57
                        GROUP BY member_id
                )AS bns ON ro.member_id = bns.member_id
                WHERE ro.`date` BETWEEN LAST_DAY(NOW() - INTERVAL 2 MONTH) + INTERVAL 1 DAY AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                AND stc.status = 'active'
                GROUP BY ro.member_id
                HAVING omset >= 0 -- AND jml_ >= 1
                ";
        }elseif($type_id == 'pph21_new'){
            $query = "SELECT -- 'F113301' AS Kode_Form
                    MONTH(pph.periode) AS 'Masa Pajak'
                    , YEAR(pph.periode) AS 'Tahun Pajak'
                    , 0 AS Pembetulan
                    , '' AS 'Nomor Bukti Potong'
                    , CASE WHEN LENGTH(REPLACE(REPLACE(pph.npwp,'-',''), '.', '')) >= 15 THEN REPLACE(REPLACE(pph.npwp,'/',''), '.', '') ELSE '000000000000000' END AS NPWP
                    , CASE WHEN LENGTH(REPLACE(REPLACE(m.noktp,'-',''), '.', '')) >= 15 THEN REPLACE(REPLACE(m.noktp,'/',''), '.', '') ELSE '0' END AS NIK
                    , pph.nama AS 'Nama'
                    , pph.alamat AS 'Alamat'
                    , 'N' AS 'WP Luar Negeri'
                    , '' AS 'Kode Negara' -- Created Table Kode Negara
                    , '21-100-04' AS 'Kode Pajak'
                    , pph.bonus AS 'Jumlah Bruto'
                    -- , (FLOOR(pph.pkp/1000)*1000) AS 'Jumlah DPP'
                    , pph.pkp AS 'Jumlah DPP'
                    , CASE WHEN pph.tarif_npwp > 100 THEN 'Y' ELSE 'N' END AS 'Tanpa NPWP'
                    , pph.tarif AS Tarif
                    , pph.pph21 AS 'Jumlah PPH'
                    , REPLACE(REPLACE('02.313.286.3-044.001','-',''), '.', '') AS 'NPWP Pemotong'
                    , 'PT. UNIVERSAL HEALTH NETWORK' AS 'Nama Pemotong'
                    , DATE_FORMAT(pph.periode, '%d/%m/%Y') AS 'Tanggal Bukti Potong'
                    , akumulasi_bruto
            FROM pph
            LEFT JOIN member m ON pph.member_id = m.id
            WHERE YEAR(pph.periode) = YEAR(LAST_DAY(NOW() - INTERVAL 1 MONTH))
            AND MONTH(pph.periode) = MONTH(LAST_DAY(NOW() - INTERVAL 1 MONTH))
            ";
        }elseif($type_id == 'list_transfer'){
            $query = "SELECT *
                FROM(
                        SELECT namaNasabah, bank_id, MAX(cabang)AS cabang, `no`, SUM(ewallet)AS payoutTransfer
                                , MAX(biayaTransfer)AS biayaTransfer
                                , SUM(ewallet) -  MAX(biayaTransfer) AS tkHomePayed
                                -- , urut2, flag
                                , CASE 
                                        WHEN MAX(urut) = 0 THEN 'BCA'
                                        WHEN MAX(urut) = 1 THEN 'Non BCA'
                                        ELSE 'NoValid'
                                END AS urut
                        FROM
                        (
                                SELECT dtbr.memberId AS memberId, dtbr.account_id
                                        , CASE WHEN dtbr.nama <> dtbr.namaNasabah THEN 'Cek' ELSE '' END AS note
                                        , dtbr.ewallet - IFNULL(bns.bns,0) AS b4, IFNULL(bns.bns,0)AS bns
                                        , dtbr.ewallet - 5000 AS ewallet, dtbr.nama AS nama, dtbr.namaNasabah
                                        , dtbr.bank_id AS bank_id, dtbr.cabang AS cabang
                                        , dtbr.urut AS urut2, flag
                                        , CASE 
                                                WHEN dtbr.urut THEN 5000
                                                WHEN flag = 2 THEN 2500
                                                WHEN flag = 1 THEN 0
                                                ELSE 'n/a'
                                        END AS biayaTransfer
                                        , IFNULL(REPLACE(REPLACE(REPLACE(dtbr.no,'-',''),'.',''),' ',''),'-') AS NO
                                        -- , dtbr.no as no
                                        , dtbr.urut
                                FROM(
                                        SELECT dt.*, CASE 
                                                        WHEN 
                                                                `no` LIKE '%000000%' 
                                                                OR namaNasabah LIKE '%000000%' 
                                                                OR `no` IS NULL 
                                                                OR (bank_id = 'BCA' AND LENGTH(REPLACE(REPLACE(REPLACE(`no`,'.',''),' ',''),'-','')) <> 10 )
                                                                OR bank_id = '-' 
                                                        THEN '2'
                                                        WHEN bank_id = 'BCA' AND `no` NOT LIKE '%000000%' THEN '0' 
                                                        ELSE '1' END AS urut
                                        FROM(
                                                SELECT 
                                                        m.id AS memberId, m.ewallet, m.nama, m.account_id
                                                        , IFNULL(acc.bank_id,'-')AS bank_id, IFNULL(acc.area,'-') AS cabang
                                                        , IFNULL(acc.no,'_')AS NO, UCASE(IFNULL(acc.`name`,'-')) AS namaNasabah
                                                        , acc.flag
                                                FROM member m
                                                LEFT JOIN account acc ON m.account_id = acc.id
                                                WHERE m.ewallet >= 50000 AND m.id <> '00000001'
                                                -- and acc.bank_id = 'BCA'
                                                GROUP BY m.id
                                                ORDER BY acc.bank_id DESC, m.ewallet DESC
                                        )AS dt
                                        GROUP BY dt.memberId
                                        ORDER BY dt.bank_id DESC, dt.ewallet DESC
                                )AS dtbr
                                LEFT JOIN(
                                        SELECT member_id, SUM(nominal) AS bns
                                        FROM bonus
                                        WHERE periode = LAST_DAY(CURDATE() - INTERVAL 1 MONTH)
                                        GROUP BY member_id
                                )AS bns ON dtbr.memberId = bns.member_id
                                WHERE ewallet-5000 > 50000
                                -- ".$flag."
                                ORDER BY urut, bank_id, ewallet DESC
                        )AS dt
                        -- WHERE bank_id = 'BCA'
                        GROUP BY namaNasabah, bank_id, `no`
                        ORDER BY urut, namaNasabah
                )AS dt
                WHERE urut <> 'NoValid'
                ";
                
        }elseif($type_id == 'bonus_account'){
            $query = "SELECT dt2.member_id,m.nama
                    ,dt2.Bonus_Pembelian_Pribadi
                    ,dt2.Bonus_Aktivasi_Level_1
                    ,dt2.Bonus_Aktivasi_Level_2
                    ,dt2.Bonus_Aktivasi_Level_3
                    ,dt2.Bonus_Aktivasi_Level_4
                    
                    ,dt2.Bonus_Aktivasi_Level_5
                    ,dt2.Bonus_Kesuksesan_Sponsor AS 'Bonus_Kesuksesan_Sponsor(2%)'
                    ,dt2.Bonus_Kesuksesan_L_SL_SS AS 'Bonus_Kesuksesan_L_SL_SS(4%)'
                    ,dt2.Bonus_Posisi_Leader
                    ,dt2.Bonus_Posisi_Super_Leader
                    ,dt2.Bonus_Penjualan_Tahunan
                    
                    , dt2.Bonus_RequalifikasiLeader
                    ,dt2.Bonus_Distribusi_Stockiest_Area
                    ,dt2.Penyesuaian_3_RO_Stc
                    ,dt2.Bonus_Cetak_Stc_Area
                    ,dt2.Hit_Reward_Stockiest
                    
                    ,dt2.PDPM
                    ,dt2.Anatolia
                    ,dt2.CarCashBonus
                    ,dt2.CarReward
                    ,dt2.Bonus_Koreksi
                    ,dt2.SPB
                    ,dt2.PPH_21
                    
                    ,dt2.Bonus_Pembelian_Pribadi
                    +dt2.Bonus_Aktivasi_Level_1
                    +dt2.Bonus_Aktivasi_Level_2
                    +dt2.Bonus_Aktivasi_Level_3
                    +dt2.Bonus_Aktivasi_Level_4
                    
                    +dt2.Bonus_Aktivasi_Level_5
                    +dt2.Bonus_Kesuksesan_Sponsor
                    +dt2.Bonus_Kesuksesan_L_SL_SS
                    +dt2.Bonus_Posisi_Leader
                    +dt2.Bonus_Posisi_Super_Leader
                    +dt2.Bonus_Penjualan_Tahunan
                    
                    +dt2.Bonus_RequalifikasiLeader
                    +dt2.Bonus_Distribusi_Stockiest_Area
                    +dt2.Penyesuaian_3_RO_Stc
                    +dt2.Bonus_Cetak_Stc_Area
                    +dt2.Hit_Reward_Stockiest
                    
                    +dt2.PDPM
                    +dt2.Anatolia
                    +dt2.CarCashBonus
                    +dt2.CarReward
                    +dt2.Bonus_Koreksi
                    +dt2.SPB
                    +dt2.PPH_21
                    AS total
            FROM (
                    SELECT member_id
                            ,IFNULL(SUM(Bonus_Pembelian_Pribadi),0)AS Bonus_Pembelian_Pribadi
                            ,IFNULL(SUM(Bonus_Aktivasi_Level_1),0)AS Bonus_Aktivasi_Level_1
                            ,IFNULL(SUM(Bonus_Aktivasi_Level_2),0)AS Bonus_Aktivasi_Level_2
                            ,IFNULL(SUM(Bonus_Aktivasi_Level_3),0)AS Bonus_Aktivasi_Level_3
                            ,IFNULL(SUM(Bonus_Aktivasi_Level_4),0)AS Bonus_Aktivasi_Level_4
                            
                            ,IFNULL(SUM(Bonus_Aktivasi_Level_5),0)AS Bonus_Aktivasi_Level_5
                            ,IFNULL(SUM(Bonus_Kesuksesan_Sponsor),0)AS Bonus_Kesuksesan_Sponsor
                            ,IFNULL(SUM(Bonus_Kesuksesan_L_SL_SS),0)AS Bonus_Kesuksesan_L_SL_SS
                            ,IFNULL(SUM(Bonus_Posisi_Leader),0)AS Bonus_Posisi_Leader
                            ,IFNULL(SUM(Bonus_Posisi_Super_Leader),0)AS Bonus_Posisi_Super_Leader
                            ,IFNULL(SUM(Bonus_Penjualan_Tahunan),0)AS Bonus_Penjualan_Tahunan
                            
                            ,IFNULL(SUM(Bonus_RequalifikasiLeader),0)AS Bonus_RequalifikasiLeader
                            ,IFNULL(SUM(Bonus_Distribusi_Stockiest_Area),0)AS Bonus_Distribusi_Stockiest_Area
                            ,IFNULL(SUM(Penyesuaian_3_RO_Stc),0)AS Penyesuaian_3_RO_Stc
                            ,IFNULL(SUM(Bonus_Cetak_Stc_Area),0)AS Bonus_Cetak_Stc_Area
                            ,IFNULL(SUM(Hit_Reward_Stockiest),0)AS Hit_Reward_Stockiest
                            
                            ,IFNULL(SUM(PDPM),0)AS PDPM
                            ,IFNULL(SUM(Anatolia),0)AS Anatolia
                            ,IFNULL(SUM(CarCashBonus),0)AS CarCashBonus
                            ,IFNULL(SUM(CarReward),0)AS CarReward
                            ,IFNULL(SUM(Bonus_Koreksi),0)AS Bonus_Koreksi
                            ,IFNULL(SUM(SPB),0)AS SPB
                            ,IFNULL(SUM(PPH_21),0)AS PPH_21
                    FROM(
                            SELECT member_id
                                    ,CASE WHEN dt.title=25 THEN dt.nominal END AS Bonus_Pembelian_Pribadi
                                    ,CASE WHEN dt.title=26 THEN dt.nominal END AS Bonus_Aktivasi_Level_1
                                    ,CASE WHEN dt.title=27 THEN dt.nominal END AS Bonus_Aktivasi_Level_2
                                    ,CASE WHEN dt.title=28 THEN dt.nominal END AS Bonus_Aktivasi_Level_3
                                    ,CASE WHEN dt.title=29 THEN dt.nominal END AS Bonus_Aktivasi_Level_4
                                    
                                    ,CASE WHEN dt.title=30 THEN dt.nominal END AS Bonus_Aktivasi_Level_5
                                    ,CASE WHEN dt.title=31 THEN dt.nominal END AS Bonus_Kesuksesan_Sponsor
                                    ,CASE WHEN dt.title=32 THEN dt.nominal END AS Bonus_Kesuksesan_L_SL_SS
                                    ,CASE WHEN dt.title=33 THEN dt.nominal END AS Bonus_Posisi_Leader
                                    ,CASE WHEN dt.title=34 THEN dt.nominal END AS Bonus_Posisi_Super_Leader
                                    ,CASE WHEN dt.title=36 THEN dt.nominal END AS Bonus_Penjualan_Tahunan
                                    
                                    ,CASE WHEN dt.title=37 THEN dt.nominal END AS Bonus_RequalifikasiLeader
                                    ,CASE WHEN dt.title=54 THEN dt.nominal END AS Bonus_Distribusi_Stockiest_Area
                                    ,CASE WHEN dt.title=55 THEN dt.nominal END AS Penyesuaian_3_RO_Stc
                                    ,CASE WHEN dt.title=56 THEN dt.nominal END AS Bonus_Cetak_Stc_Area
                                    ,CASE WHEN dt.title=57 THEN dt.nominal END AS Hit_Reward_Stockiest
                                    
                                    ,CASE WHEN dt.title=118 THEN dt.nominal END AS PDPM
                                    ,CASE WHEN dt.title=130 THEN dt.nominal END AS Anatolia
                                    ,CASE WHEN dt.title=131 THEN dt.nominal END AS CarCashBonus
                                    ,CASE WHEN dt.title=132 THEN dt.nominal END AS CarReward
                                    ,CASE WHEN dt.title=995 THEN dt.nominal END AS Bonus_Koreksi
                                    ,CASE WHEN dt.title=127 THEN dt.nominal END AS SPB
                                    ,CASE WHEN dt.title=999 THEN dt.nominal END AS PPH_21
                            FROM (
                                    SELECT member_id,title,nominal
                                    FROM v_bonus b
                                    WHERE periode BETWEEN (LAST_DAY(NOW() - INTERVAL 2 MONTH)+ INTERVAL 1 DAY) AND LAST_DAY(NOW() - INTERVAL 1 MONTH)
                                    ORDER BY member_id,title
                            )AS dt
                    )AS dt1
                    GROUP BY member_id
            )AS dt2
            LEFT JOIN member m ON m.id=dt2.member_id
            ";
        }
        elseif($type_id == 'rekening_tidak_lengkap'){
            $petik = "\'";
            $query = "
                    SELECT r.nama AS region, concat('".$petik."',dtbr.memberId) AS memberId
                                , dtbr.nama AS nama, alamatM, concat('".$petik."',dtbr.hp1)as hp1
                                , k.name AS kota
                                -- , dtbr.hp, dtbr.namaNasabah , dtbr.bank_id AS bank_id, dtbr.cabang AS cabang , IFNULL(REPLACE(REPLACE(REPLACE(dtbr.no,'-',''),'.',''),' ',''),'-') AS NO 
                                -- , dtbr.no as no 
                                , concat('".$petik."',dtbr.stockiest_id)as stockiest_id, stc.nama, stc.alamat, ks.name AS kotaSp, concat('".$petik."',stc.hp) AS hpSp, r.nama AS regionStc
                                , dtbr.urut 
                                , dtbr.ewallet
                FROM( 
                        SELECT dt.*
                                , CASE WHEN `no` LIKE '%000000%' 
                                                OR namaNasabah LIKE '%000000%' 
                                                OR `no` IS NULL 
                                                OR (bank_id = 'BCA' AND LENGTH(REPLACE(REPLACE(REPLACE(`no`,'.',''),' ',''),'-','')) <> 10 ) 
                                                OR bank_id = '-' THEN '2' 
                                WHEN bank_id = 'BCA' AND `no` NOT LIKE '%000000%' THEN '0' 
                                ELSE '1' 
                                END AS urut 
                        FROM( 
                                SELECT m.id AS memberId, m.ewallet, m.nama, m.kota_id, m.telp, m.hp, m.account_id 
                                        , IFNULL(acc.bank_id,'-')AS bank_id, IFNULL(acc.area,'-') AS cabang 
                                        , IFNULL(acc.no,'_')AS NO, IFNULL(acc.`name`,'-') AS namaNasabah 
                                        , m.stockiest_id, m.hp AS hp1, m.alamat AS alamatM
                                FROM member m 
                                LEFT JOIN account acc ON m.account_id = acc.id 
                                WHERE m.ewallet >= 50000 AND m.id <> '00000001' 
                                GROUP BY m.id 
                                ORDER BY acc.bank_id DESC, m.ewallet DESC 
                        )AS dt 
                        GROUP BY dt.memberId 
                        ORDER BY dt.bank_id DESC, dt.ewallet DESC 
                )AS dtbr 
                LEFT JOIN kota k ON dtbr.kota_id = k.id
                LEFT JOIN member stc ON dtbr.stockiest_id = stc.id
                LEFT JOIN kota ks ON stc.kota_id=ks.id
                LEFT JOIN region r ON ks.region = r.id
                LEFT JOIN( 
                        SELECT member_id, SUM(nominal) AS bns 
                        FROM bonus 
                        WHERE periode = LAST_DAY(CURDATE() - INTERVAL 1 MONTH) 
                        GROUP BY member_id 
                )AS bns ON dtbr.memberId = bns.member_id 
                WHERE dtbr.ewallet-5000 > 50000 
                AND dtbr.urut > 1
                ORDER BY k.region, dtbr.ewallet DESC
                ";
        }elseif($type_id == 'rekening_tidak_lengkap_region'){
            $petik = "\'";
            $query = "
                SELECT r.nama AS region, concat('".$petik."',dtbr.memberId) AS memberId
                                , dtbr.nama AS nama, alamatM, concat('".$petik."',dtbr.hp1)as hp1
                                , k.name AS kota
                                -- , dtbr.hp, dtbr.namaNasabah , dtbr.bank_id AS bank_id, dtbr.cabang AS cabang , IFNULL(REPLACE(REPLACE(REPLACE(dtbr.no,'-',''),'.',''),' ',''),'-') AS NO 
                                -- , dtbr.no as no 
                                , concat('".$petik."',dtbr.stockiest_id)as stockiest_id, stc.nama, stc.alamat, ks.name AS kotaSp, concat('".$petik."',stc.hp) AS hpSp, r.nama AS regionStc
                                , dtbr.urut 
                                , dtbr.ewallet
                FROM( 
                        SELECT dt.*
                                , CASE WHEN `no` LIKE '%000000%' 
                                                OR namaNasabah LIKE '%000000%' 
                                                OR `no` IS NULL 
                                                OR (bank_id = 'BCA' AND LENGTH(REPLACE(REPLACE(REPLACE(`no`,'.',''),' ',''),'-','')) <> 10 ) 
                                                OR bank_id = '-' THEN '2' 
                                WHEN bank_id = 'BCA' AND `no` NOT LIKE '%000000%' THEN '0' 
                                ELSE '1' 
                                END AS urut 
                        FROM( 
                                SELECT m.id AS memberId, m.ewallet, m.nama, m.kota_id, m.telp, m.hp, m.account_id 
                                        , IFNULL(acc.bank_id,'-')AS bank_id, IFNULL(acc.area,'-') AS cabang 
                                        , IFNULL(acc.no,'_')AS NO, IFNULL(acc.`name`,'-') AS namaNasabah 
                                        , m.stockiest_id, m.hp AS hp1, m.alamat AS alamatM
                                FROM member m 
                                LEFT JOIN account acc ON m.account_id = acc.id 
                                WHERE m.ewallet >= 50000 AND m.id <> '00000001' 
                                GROUP BY m.id 
                                ORDER BY acc.bank_id DESC, m.ewallet DESC 
                        )AS dt 
                        GROUP BY dt.memberId 
                        ORDER BY dt.bank_id DESC, dt.ewallet DESC 
                )AS dtbr 
                LEFT JOIN kota k ON dtbr.kota_id = k.id
                LEFT JOIN member stc ON dtbr.stockiest_id = stc.id
                LEFT JOIN kota ks ON stc.kota_id=ks.id
                LEFT JOIN region r ON ks.region = r.id
                LEFT JOIN( 
                        SELECT member_id, SUM(nominal) AS bns 
                        FROM bonus 
                        WHERE periode = LAST_DAY(CURDATE() - INTERVAL 1 MONTH) 
                        GROUP BY member_id 
                )AS bns ON dtbr.memberId = bns.member_id 
                WHERE dtbr.ewallet-5000 > 50000 
                AND dtbr.urut > 1
                ORDER BY k.region, dtbr.ewallet DESC
                
            ";
        }elseif($type_id == 'update_stockist'){
                $query = "
                SELECT dt.member_id
            , s.no_stc, m.nama, s.type
            , SUM(jmlMktTools1)AS jmlMktTools1
            , SUM(jmlBlj1)AS jmlBlj1
            , SUM(jmlKit1)AS jmlKit1
            , SUM(jmlMktTools2)AS jmlMktTools2
            , SUM(jmlBlj2)AS jmlBlj2
            , SUM(jmlKit2)AS jmlKit2
            , SUM(jmlMktTools1) + SUM(jmlMktTools2) AS jmlMktTools
            , SUM(jmlBlj1) + SUM(jmlBlj2) AS jmlBlj
            , SUM(jmlKit1) + SUM(jmlKit2) AS jmlKit
            -- , case when SUM(jmlBlj1) + SUM(jmlBlj2) >= 30000000 then 'Cek' else '' end as note
            , IFNULL(rtr.jmlRtrMktTools,0)AS jmlRtrMktTools
            , IFNULL(rtr.jmlRtr,0)AS jmlRtr
            , SUM(jmlMktTools1) + SUM(jmlMktTools2) - IFNULL(rtr.jmlRtrMktTools,0) AS TotalMktTools
            , SUM(jmlBlj1) + SUM(jmlBlj2) - IFNULL(rtr.jmlRtr,0) AS TotalBlj
            , CASE WHEN SUM(jmlKit1) + SUM(jmlKit2) + SUM(jmlMktTools1) + SUM(jmlMktTools2) + SUM(jmlBlj1) + SUM(jmlBlj2) - IFNULL(rtr.jmlRtr,0) >= 30000000 THEN 'Cek' ELSE '' END AS note
            , CASE WHEN SUM(jmlKit1) + SUM(jmlKit2) + SUM(jmlMktTools1) + SUM(jmlMktTools2) + SUM(jmlBlj1) + SUM(jmlBlj2) - IFNULL(rtr.jmlRtr,0) >= 30000000 THEN ROUND(((SUM(jmlBlj1) + SUM(jmlBlj2) - IFNULL(rtr.jmlRtr,0))*0.06)*1) ELSE '' END AS penyesuaianRO
            , CASE WHEN IFNULL(us.member_id,0) = 0 THEN '' ELSE 'Cek' END AS note2
    FROM(
            SELECT ro.member_id
                    , CASE WHEN i.type_id = 3 AND MONTH(ro.date) = (MONTH(NOW()) - 2) THEN rod.jmlharga ELSE 0 END AS jmlMktTools1
                    , CASE WHEN i.type_id = 2 AND MONTH(ro.date) = (MONTH(NOW()) - 2) THEN rod.jmlharga ELSE 0 END AS jmlBlj1
                    , CASE WHEN i.type_id = 1 AND MONTH(ro.date) = (MONTH(NOW()) - 2) THEN rod.jmlharga ELSE 0 END AS jmlKit1
                    
                    , CASE WHEN i.type_id = 3 AND MONTH(ro.date) = (MONTH(NOW()) - 1) THEN rod.jmlharga ELSE 0 END AS jmlMktTools2
                    , CASE WHEN i.type_id = 2 AND MONTH(ro.date) = (MONTH(NOW()) - 1) THEN rod.jmlharga ELSE 0 END AS jmlBlj2
                    , CASE WHEN i.type_id = 1 AND MONTH(ro.date) = (MONTH(NOW()) - 1) THEN rod.jmlharga ELSE 0 END AS jmlKit2
            FROM ro
            RIGHT JOIN ro_d rod ON ro.id = rod.ro_id
            LEFT JOIN item i ON rod.item_id = i.id
            WHERE ro.`date` BETWEEN (LAST_DAY(NOW() - INTERVAL 3 MONTH)+ INTERVAL 1 DAY) AND (LAST_DAY(NOW() - INTERVAL 1 MONTH))
            AND ro.stockiest_id = 0
    )AS dt
    LEFT JOIN(
            SELECT member_id
                    , SUM(jmlRtrMktTools) AS jmlRtrMktTools
                    , SUM(jmlRtr) AS jmlRtr
            FROM(
                    SELECT rtr.id, rtr.tgl, rtr.stockiest_id AS member_id, rtrd.item_id
                            , CASE WHEN i.type_id = 3 THEN rtrd.jmlharga ELSE 0 END AS jmlRtrMktTools
                            , CASE WHEN i.type_id <> 3 THEN rtrd.jmlharga ELSE 0 END AS jmlRtr
                    FROM retur_titipan rtr
                    RIGHT JOIN retur_titipan_d rtrd ON rtr.id = rtrd.retur_titipan_id
                    LEFT JOIN item i ON rtrd.item_id = i.id
                    WHERE rtr.tgl BETWEEN (LAST_DAY(NOW() - INTERVAL 3 MONTH)+ INTERVAL 1 DAY) AND (LAST_DAY(NOW() - INTERVAL 1 MONTH))
            )AS dt
            GROUP BY member_id
    )AS rtr ON dt.member_id = rtr.member_id
    LEFT JOIN member m ON dt.member_id = m.id
    LEFT JOIN stockiest s ON m.id = s.id
    LEFT JOIN(
            SELECT member_id, 1 AS jml
            FROM update_stc
            WHERE createddate BETWEEN LAST_DAY(NOW() - INTERVAL 1 MONTH) + INTERVAL 1 DAY AND LAST_DAY(NOW())
            AND old_type = new_type
    )AS us ON s.id = us.member_id
    GROUP BY dt.member_id
    ORDER BY note2 DESC, s.type DESC, jmlBlj DESC
    ";
        }elseif($type_id == ''){
            $query = "";
        }else{
            $this->db->select("m.id as member_id,m.nama,format(m.pgs,0)as fpgs,j.decription as jenjang",false)
                ->from('member m')
                ->join('jenjang j','m.jenjang_id=j.id','left')
                ->where('m.pgs >=',40000000)
                ->order_by('m.pgs','desc')
                ->order_by('m.nama','asc');
            //$query = $this->db->get();        
        }
        //echo $this->db->last_query();
        //echo $query." ** ".$type_id;
        
        
        $data = $this->db->query($query);
        //echo $this->db->last_query()." ".$temp;
        /*if($q->num_rows > 0){
	    foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        
        $q->free_result();
        */
        return $data;
    }
    




}
?>