<?php
class MTransfer extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Transfer ewallet for Stockiest from member
    |--------------------------------------------------------------------------
    |
    | @created 2009-05-03
    | @author qtakwa@yahoo.com@yahoo.com
    | @copyright www.smartindo-techmology.com
    */
    public function searchTransfer($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.remark,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,m.nama as namamember,s.no_stc,x.nama as namastc,format(a.amount,0)as famount",false);
        $this->db->from('transfer a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('member x','a.stockiest_id=x.id','left');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        if($this->session->userdata('group_id')>100){
            if($this->session->userdata('group_id') == 101) $where = "`a`.`member_id` = '".$this->session->userdata('userid')."' AND (s.no_stc LIKE '$keywords%' OR a.id LIKE '$keywords%')";
            else $where = "`a`.`stockiest_id` = '".$this->session->userdata('userid')."' AND (s.no_stc LIKE '$keywords%' OR a.id LIKE '$keywords%')";
        }else{
            $where = "(a.member_id LIKE '$keywords%' OR s.no_stc LIKE '$keywords%' OR a.id LIKE '$keywords%')";
        }
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countTransfer($keywords=0){
        $this->db->select("a.id");
        $this->db->from('transfer a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('member x','a.stockiest_id=x.id','left');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        if($this->session->userdata('group_id')>100){
            if($this->session->userdata('group_id') == 101) $where = "`a`.`member_id` = '".$this->session->userdata('userid')."' AND (s.no_stc LIKE '$keywords%' OR a.id LIKE '$keywords%')";
            else $where = "`a`.`stockiest_id` = '".$this->session->userdata('userid')."' AND (s.no_stc LIKE '$keywords%' OR a.id LIKE '$keywords%')";
        }else{
            $where = "(a.member_id LIKE '$keywords%' OR s.no_stc LIKE '$keywords%' OR a.id LIKE '$keywords%')";
        }
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    public function addTransfer(){
        $member_id = $this->session->userdata('userid');
        $stockiest_id = $this->input->post('member_id');
        $amount = str_replace(".","",$this->input->post('amount'));
        $data=array(
            'member_id' => $member_id,
            'stockiest_id' => $stockiest_id,
            'tgl' => date('Y-m-d',now()),
            'amount' => $amount,
            'event_id' => 'TW1',
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $member_id
        );
        $this->db->insert('transfer',$data);
        
        $id = $this->db->insert_id();
        $this->db->query("call sp_ewallet_transfer('$member_id','$stockiest_id','Transfer Ewallet','$id','$amount','$member_id')");
    }
    public function getTransfer($id){
        $data=array();  
        $this->db->select("a.id,a.createdby,a.remark,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,m.nama as namamember,s.no_stc,x.nama as namastc,format(a.amount,0)as famount",false);
        $this->db->from('transfer a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('member x','a.stockiest_id=x.id','left');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        if($this->session->userdata('group_id')>100){
            if($this->session->userdata('group_id') == 101) $where = "`a`.`member_id` = '".$this->session->userdata('userid')."' and a.id = '$id'";
            else $where = "`a`.`stockiest_id` = '".$this->session->userdata('userid')."' and a.id = '$id'";
        }else{
            $where = "a.id = '$id'";
        }
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function getEwallet($memberid){
        $data=array();
        if($this->session->userdata('group_id') == 101){
            $q=$this->db->select("m.account_id,m.ewallet,format(m.ewallet,0)as fewallet",false)
                ->from('member m')
                ->where('m.id',$memberid)
                ->get();
        }else{
            $q=$this->db->select("m.account_id,s.ewallet,format(s.ewallet,0)as fewallet",false)
                ->from('stockiest s')
                ->join('member m','s.id=m.id','left')
                ->where('s.id',$memberid)
                ->get();
        }
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
        
    
}?>