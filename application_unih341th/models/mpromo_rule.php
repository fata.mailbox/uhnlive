<?php
class Mpromo_rule extends CI_Model{
  function __construct()
  {
    parent::__construct();
  }

  /*
  |--------------------------------------------------------------------------
  | Promo Product
  |--------------------------------------------------------------------------
  |
  | @created 2019
  | Annisa Rahmawaty
  |
  */
  public function searchPromo($keywords=0,$num,$offset){
    $data = array();
    $this->db->select("p.*,a.name",false);
    $this->db->from('item_rule_order_promo p');
    $this->db->join('item a','a.id=p.item_id','left');
    $this->db->like('p.item_id',$keywords,'between');
    $this->db->or_like('a.name',$keywords,'between');
    $this->db->order_by('p.id','DESC');
    $this->db->limit($num,$offset);
    $q = $this->db->get();
    
    if($q->num_rows > 0){
      foreach($q->result_array() as $row){
        $data[] = $row;
      }
    }
    $q->free_result();
    return $data;
  }
  public function countPromo($keywords=0){
    $this->db->like('a.item_id',$keywords,'between');
    $this->db->or_like('i.name',$keywords,'between');
    $this->db->from('item_rule_order_promo a');
    $this->db->join('item i','i.id=a.item_id','left');
    return $this->db->count_all_results();
  }

  public function addPromo_rule(){    
    $data=array(
        'item_id' => $this->input->post('item_id'),
        'min_order' => $this->input->post('min_order'),
        'max_order' => $this->input->post('max_order'),
        'multiples' => $this->input->post('multiples'),
        'expireddate' => $this->input->post('exp_date')
    );
    $this->db->insert('item_rule_order_promo',$data);
  }

  public function getPromo($id=0){
    $data = array();
    $this->db->select("p.*,a.name",false);
    $this->db->from('item_rule_order_promo p');
    $this->db->join('item a','a.id=p.item_id','left');
    $this->db->where('p.id',$id);
    
    $q = $this->db->get();

    if($q->num_rows()>0){
      $data=$q->row();
    }
    $q->free_result();
    return $data;
  }

  public function editPromo_rule($id){
    $data=array(
        'item_id' => $this->input->post('item_id'),
        'min_order' => $this->input->post('min_order'),
        'max_order' => $this->input->post('max_order'),
        'multiples' => $this->input->post('multiples'),
        'expireddate' => $this->input->post('exp_date')
    );

    $this->db->where('id',$id);
    $this->db->update('item_rule_order_promo',$data);
    $this->session->set_flashdata('message','Edit Promo Product Successfully');
  }
  
}?>
