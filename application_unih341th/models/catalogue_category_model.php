<?php
class Catalogue_category_model extends CI_Model
{
	    function __construct()
	    {
	        parent::__construct();
	    }

    	public function getDataList($limit = '', $offset = '', $id = '', $name = '', $sort = '', $keyword = '')
		{
			$wQuery = 'WHERE';

			if($id != ''){
				$wQuery.= " catalogue_category_id=".$id;
			}

			if($name != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " name='".$name."'";
				}else{
					$wQuery.= " AND name='".$name."'";
				}
			}

			if($sort != ''){
				if($wQuery == 'WHERE'){
					$wQuery.= " catalogue_category_sort='".$sort."'";
				}else{
					$wQuery.= " AND catalogue_category_sort='".$sort."'";
				}
			}

			if($keyword != '')
			{
				if($wQuery=='WHERE'){
					$wQuery.= " category_id LIKE '%".$keyword."%' OR name LIKE '%".$keyword."%'";
				}else{
					$wQuery.= " AND category_id LIKE '%".$keyword."%' OR name LIKE '%".$keyword."%'";
				}
			}

			if($limit!=''&&$offset!='')$lQuery = ' LIMIT '.$limit.','.$offset; else $lQuery = '';

			if($wQuery=='WHERE')$wQuery = '';

			$rs = $this->db->query("SELECT * 
	    				    FROM catalogue_category
	                        ".$wQuery.$lQuery);
	        
	        $result = array();

	        if ($rs->num_rows() > 0) 
	        {
	            foreach($rs->result_array() as $row ) {
	                $result['data'][] = $row;
	            }

	            foreach($rs->result_object() as $row2 ) {
	                $result['dataObject'] = $row2;
	            }
	        }
	        else
	        {
	        	return FALSE;
	        }

	        $result['countResult']=$rs->num_rows();
	        $rs->free_result();
			return $result;
		}

		public function create($name = '', $sort = '')
		{
			$query1 = "SELECT * FROM catalogue_category WHERE name = '$name'";
			$query2 = "INSERT INTO catalogue_category(name,catalogue_category_sort) VALUES('$name','$sort')";
			$rs1 = $this->db->query($query1);

			if($rs1->num_rows() > 0) {
				return false;
			} else {
				$rs2 = $this->db->query($query2);

				if($rs2) {
					return true;
				} else {
					return false;
				}
			}

		}

		public function edit($id = '', $name = '', $sort = '')
		{
			$query1 = "SELECT * FROM catalogue_category WHERE name = '$name' AND catalogue_category_id != '$id'";
			$query2 = "UPDATE catalogue_category SET name='$name', catalogue_category_sort='$sort' WHERE catalogue_category_id='$id'";
			$rs1 = $this->db->query($query1);

			if($rs1->num_rows() > 0) {
				return false;
			} else {
				$rs2 = $this->db->query($query2);

				if($rs2) {
					return true;
				} else {
					return false;
				}
			}
		}

		public function delete($id = '')
		{
			$query = "DELETE FROM catalogue_category WHERE catalogue_category_id='$id'";
			$rs =  $this->db->query($query);

			if($rs){
				return true;
			} else {
				return false;
			}
		}


}