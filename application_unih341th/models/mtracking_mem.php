<?php
class MTracking_mem extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    public function get_mem_trips(){
        $data = array();
		$q = "
			select 
			id, trip_name, start_date, end_date 
			from member_trip
			order by id asc
		";
        $q=$this->db->query($q);
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function get_curr_month(){
        $data = array();
		$q = "
			SELECT MONTH(MAX(tgl))+1 as currmonth, MAX(tgl) as lastdate
			FROM pgs_bulanan
		";
        $q=$this->db->query($q);
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }


    public function getTracking($member_id,$startdate,$enddate){
        $data = array();
		$q = "
		SELECT m.id AS member_id
		, m.nama, k.region
		, ps1, tgpv1, ba1
		, ps2, tgpv2, ba2
		, ps3, tgpv3, ba3
		, ps4, tgpv4, ba4
		, ps5, tgpv5, ba5
		, ps6, tgpv6, ba6
		, ps7, tgpv7, ba7
		, ps8, tgpv8, ba8
		, ps9, tgpv9, ba9
		, ps10, tgpv10, ba10
		, ps11, tgpv11, ba11
		, ps12, tgpv12, ba12
		, CASE WHEN tgpv1 >= 60000000 AND ps1 >= 1000000 THEN 1 WHEN ba1 >=1 AND tgpv1 >= 40000000 AND ps1 >= 1000000 THEN 1 WHEN ba1 >=2 AND tgpv1 >= 20000000 AND ps1 >= 1000000 THEN 1 WHEN ba1 >=3 AND tgpv1 >= 1000000 AND ps1 >= 1000000 THEN 1 ELSE 0 END AS Q1		
		, CASE WHEN tgpv2 >= 60000000 AND ps2 >= 1000000 THEN 1 WHEN ba2 >=1 AND tgpv2 >= 40000000 AND ps2 >= 1000000 THEN 1 WHEN ba2 >=2 AND tgpv2 >= 20000000 AND ps2 >= 1000000 THEN 1 WHEN ba2 >=3 AND tgpv2 >= 1000000 AND ps2 >= 1000000 THEN 1 ELSE 0 END AS Q2		
		, CASE WHEN tgpv3 >= 60000000 AND ps3 >= 1000000 THEN 1 WHEN ba3 >=1 AND tgpv3 >= 40000000 AND ps3 >= 1000000 THEN 1 WHEN ba3 >=2 AND tgpv3 >= 20000000 AND ps3 >= 1000000 THEN 1 WHEN ba3 >=3 AND tgpv3 >= 1000000 AND ps3 >= 1000000 THEN 1 ELSE 0 END AS Q3		
		, CASE WHEN tgpv4 >= 60000000 AND ps4 >= 1000000 THEN 1 WHEN ba4 >=1 AND tgpv4 >= 40000000 AND ps4 >= 1000000 THEN 1 WHEN ba4 >=2 AND tgpv4 >= 20000000 AND ps4 >= 1000000 THEN 1 WHEN ba4 >=3 AND tgpv4 >= 1000000 AND ps4 >= 1000000 THEN 1 ELSE 0 END AS Q4		
		, CASE WHEN tgpv5 >= 60000000 AND ps5 >= 1000000 THEN 1 WHEN ba5 >=1 AND tgpv5 >= 40000000 AND ps5 >= 1000000 THEN 1 WHEN ba5 >=2 AND tgpv5 >= 20000000 AND ps5 >= 1000000 THEN 1 WHEN ba5 >=3 AND tgpv5 >= 1000000 AND ps5 >= 1000000 THEN 1 ELSE 0 END AS Q5		
		, CASE WHEN tgpv6 >= 60000000 AND ps6 >= 1000000 THEN 1 WHEN ba6 >=1 AND tgpv6 >= 40000000 AND ps6 >= 1000000 THEN 1 WHEN ba6 >=2 AND tgpv6 >= 20000000 AND ps6 >= 1000000 THEN 1 WHEN ba6 >=3 AND tgpv6 >= 1000000 AND ps6 >= 1000000 THEN 1 ELSE 0 END AS Q6		
		, CASE WHEN tgpv7 >= 60000000 AND ps7 >= 1000000 THEN 1 WHEN ba7 >=1 AND tgpv7 >= 40000000 AND ps7 >= 1000000 THEN 1 WHEN ba7 >=2 AND tgpv7 >= 20000000 AND ps7 >= 1000000 THEN 1 WHEN ba7 >=3 AND tgpv7 >= 1000000 AND ps7 >= 1000000 THEN 1 ELSE 0 END AS Q7		
		, CASE WHEN tgpv8 >= 60000000 AND ps8 >= 1000000 THEN 1 WHEN ba8 >=1 AND tgpv8 >= 40000000 AND ps8 >= 1000000 THEN 1 WHEN ba8 >=2 AND tgpv8 >= 20000000 AND ps8 >= 1000000 THEN 1 WHEN ba8 >=3 AND tgpv8 >= 1000000 AND ps8 >= 1000000 THEN 1 ELSE 0 END AS Q8		
		, CASE WHEN tgpv9 >= 60000000 AND ps9 >= 1000000 THEN 1 WHEN ba9 >=1 AND tgpv9 >= 40000000 AND ps9 >= 1000000 THEN 1 WHEN ba9 >=2 AND tgpv9 >= 20000000 AND ps9 >= 1000000 THEN 1 WHEN ba9 >=3 AND tgpv9 >= 1000000 AND ps9 >= 1000000 THEN 1 ELSE 0 END AS Q9		
		, CASE WHEN tgpv10 >= 60000000 AND ps10 >= 1000000 THEN 1 WHEN ba10 >=1 AND tgpv10 >= 40000000 AND ps10 >= 1000000 THEN 1 WHEN ba10 >=2 AND tgpv10 >= 20000000 AND ps10 >= 1000000 THEN 1 WHEN ba10 >=3 AND tgpv10 >= 1000000 AND ps10 >= 1000000 THEN 1 ELSE 0 END AS Q10		
		, CASE WHEN tgpv11 >= 60000000 AND ps11 >= 1000000 THEN 1 WHEN ba11 >=1 AND tgpv11 >= 40000000 AND ps11 >= 1000000 THEN 1 WHEN ba11 >=2 AND tgpv11 >= 20000000 AND ps11 >= 1000000 THEN 1 WHEN ba11 >=3 AND tgpv11 >= 1000000 AND ps11 >= 1000000 THEN 1 ELSE 0 END AS Q11		
		, CASE WHEN tgpv12 >= 60000000 AND ps12 >= 1000000 THEN 1 WHEN ba12 >=1 AND tgpv12 >= 40000000 AND ps12 >= 1000000 THEN 1 WHEN ba12 >=2 AND tgpv12 >= 20000000 AND ps12 >= 1000000 THEN 1 WHEN ba12 >=3 AND tgpv12 >= 1000000 AND ps12 >= 1000000 THEN 1 ELSE 0 END AS Q12		
		FROM member m
		LEFT JOIN kota k ON m.kota_id = k.id
		LEFT JOIN (
			SELECT 
			member_id
			, SUM(ps1)AS ps1, SUM(ps2)AS ps2, SUM(ps3)AS ps3
			, SUM(ps4)AS ps4, SUM(ps5)AS ps5, SUM(ps6)AS ps6
			, SUM(ps7)AS ps7, SUM(ps8)AS ps8, SUM(ps9)AS ps9
			, SUM(ps10)AS ps10, SUM(ps11)AS ps11, SUM(ps12)AS ps12
			, SUM(ps1)+SUM(tgpv1)AS tgpv1, SUM(ps2)+SUM(tgpv2)AS tgpv2, SUM(ps3)+SUM(tgpv3)AS tgpv3
			, SUM(ps4)+SUM(tgpv4)AS tgpv4, SUM(ps5)+SUM(tgpv5)AS tgpv5, SUM(ps6)+SUM(tgpv6)AS tgpv6
			, SUM(ps7)+SUM(tgpv7)AS tgpv7, SUM(ps8)+SUM(tgpv8)AS tgpv8, SUM(ps9)+SUM(tgpv9)AS tgpv9
			, SUM(ps10)+SUM(tgpv10)AS tgpv10, SUM(ps11)+SUM(tgpv11)AS tgpv11, SUM(ps12)+SUM(tgpv12)AS tgpv12
			, SUM(ba1)AS ba1, SUM(ba2)AS ba2, SUM(ba3)AS ba3
			, SUM(ba4)AS ba4, SUM(ba5)AS ba5, SUM(ba6)AS ba6
			, SUM(ba7)AS ba7, SUM(ba8)AS ba8, SUM(ba9)AS ba9
			, SUM(ba10)AS ba10, SUM(ba11)AS ba11, SUM(ba12)AS ba12
			FROM(
				SELECT member_id
				, CASE WHEN MONTH(pb.tgl)=MONTH('".$startdate."') THEN pb.ps ELSE 0 END AS ps1, CASE WHEN MONTH(pb.tgl)=MONTH('".$startdate."') THEN pb.pgs-pb.pgs_cut ELSE 0 END AS tgpv1, CASE WHEN MONTH(pb.tgl)=MONTH('".$startdate."') THEN ROUND(IFNULL(pb.sumbangan,m.sumbangan)/20000000,0) ELSE 0 END AS ba1
				, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') THEN pb.ps ELSE 0 END AS ps2, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') THEN pb.pgs-pb.pgs_cut ELSE 0 END AS tgpv2, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+1 Month"))."') THEN ROUND(IFNULL(pb.sumbangan,m.sumbangan)/20000000,0) ELSE 0 END AS ba2
				, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') THEN pb.ps ELSE 0 END AS ps3, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') THEN pb.pgs-pb.pgs_cut ELSE 0 END AS tgpv3, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+2 Month"))."') THEN ROUND(IFNULL(pb.sumbangan,m.sumbangan)/20000000,0) ELSE 0 END AS ba3
				, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') THEN pb.ps ELSE 0 END AS ps4, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') THEN pb.pgs-pb.pgs_cut ELSE 0 END AS tgpv4, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+3 Month"))."') THEN ROUND(IFNULL(pb.sumbangan,m.sumbangan)/20000000,0) ELSE 0 END AS ba4
				, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') THEN pb.ps ELSE 0 END AS ps5, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') THEN pb.pgs-pb.pgs_cut ELSE 0 END AS tgpv5, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+4 Month"))."') THEN ROUND(IFNULL(pb.sumbangan,m.sumbangan)/20000000,0) ELSE 0 END AS ba5
				, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') THEN pb.ps ELSE 0 END AS ps6, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') THEN pb.pgs-pb.pgs_cut ELSE 0 END AS tgpv6, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+5 Month"))."') THEN ROUND(IFNULL(pb.sumbangan,m.sumbangan)/20000000,0) ELSE 0 END AS ba6
				, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') THEN pb.ps ELSE 0 END AS ps7, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') THEN pb.pgs-pb.pgs_cut ELSE 0 END AS tgpv7, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+6 Month"))."') THEN ROUND(IFNULL(pb.sumbangan,m.sumbangan)/20000000,0) ELSE 0 END AS ba7
				, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') THEN pb.ps ELSE 0 END AS ps8, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') THEN pb.pgs-pb.pgs_cut ELSE 0 END AS tgpv8, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+7 Month"))."') THEN ROUND(IFNULL(pb.sumbangan,m.sumbangan)/20000000,0) ELSE 0 END AS ba8
				, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') THEN pb.ps ELSE 0 END AS ps9, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') THEN pb.pgs-pb.pgs_cut ELSE 0 END AS tgpv9, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+8 Month"))."') THEN ROUND(IFNULL(pb.sumbangan,m.sumbangan)/20000000,0) ELSE 0 END AS ba9
				, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') THEN pb.ps ELSE 0 END AS ps10, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') THEN pb.pgs-pb.pgs_cut ELSE 0 END AS tgpv10, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+9 Month"))."') THEN ROUND(IFNULL(pb.sumbangan,m.sumbangan)/20000000,0) ELSE 0 END AS ba10
				, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') THEN pb.ps ELSE 0 END AS ps11, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') THEN pb.pgs-pb.pgs_cut ELSE 0 END AS tgpv11, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+10 Month"))."') THEN ROUND(IFNULL(pb.sumbangan,m.sumbangan)/20000000,0) ELSE 0 END AS ba11
				, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') THEN pb.ps ELSE 0 END AS ps12, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') THEN pb.pgs-pb.pgs_cut ELSE 0 END AS tgpv12, CASE WHEN MONTH(pb.tgl)=MONTH('".date('Y-m-d',strtotime($startdate."+11 Month"))."') THEN ROUND(IFNULL(pb.sumbangan,m.sumbangan)/20000000,0) ELSE 0 END AS ba12
				FROM pgs_bulanan pb
				LEFT JOIN member m ON  pb.member_id = m.id
				WHERE pb.tgl BETWEEN '".$startdate."' and '".$enddate."'
			)AS dt
			GROUP BY member_id
		) AS dt ON dt.member_id = m.id
		WHERE m.id = '".$member_id."'		
		";
        $q=$this->db->query($q);
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getTrackingCurr($member_id){
        $data = array();
		$q = "
		SELECT *
		, CASE WHEN tgpv >= 60000000 AND ps >= 1000000 THEN 1 WHEN ba >=1 AND tgpv >= 40000000 AND ps >= 1000000 THEN 1 WHEN ba >=2 AND tgpv >= 20000000 AND ps >= 1000000 THEN 1 WHEN ba >=3 AND tgpv >= 1000000 AND ps >= 1000000 THEN 1 ELSE 0 END AS Q		
		FROM(
			SELECT m.id AS member_id
			, m.nama, k.region
			,m.ps, m.ps+m.pgs-m.pgs_cut AS tgpv,ROUND(m.sumbangan/20000000,0) AS ba
			FROM member m
			LEFT JOIN kota k ON m.kota_id = k.id
			WHERE m.id = '".$member_id."'
		) AS dt
		";
        $q=$this->db->query($q);
        //echo $this->db->last_query();
        $i=0;
        if($q->num_rows()>0){
            foreach($q->result_array()as $row){
                $i++;
                $row['i'] = $i;
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }

}
?>