<?php
class MRo extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Request Order Stockiest
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-04-15
    |
    */
    
    public function searchRO($keywords=0,$num,$offset){
        $data = array();
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.member_id = '$memberid' AND a.id LIKE '$keywords%'";
        }
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.stockiest_id = '0' and a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
			/*Modified by Boby 2009-11-23*/
			//else $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			/*end Modified by Boby 2009-11-23*/
			//$where = "a.stockiest_id = '0' and s.type <> 2 and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
            else $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.member_id,s.no_stc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama,a.remarkapp,a.status",false);
        $this->db->from('ro a');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->where($where);
        $this->db->order_by('a.status','asc');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countRO($keywords=0){
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.member_id = '$memberid' AND a.id LIKE '$keywords%'";
        }
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.stockiest_id = '0' and a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
			/*Modified by Boby 2009-11-23*/
			//else $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			/*end Modified by Boby 2009-11-23*/
            else $where = "a.stockiest_id = '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        
        $this->db->from('ro a');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }

	public function addRequestOrder(){
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
		$totalbv = str_replace(".","",$this->input->post('totalbv'));
        $empid = $this->session->userdata('userid');
        $whsid = $this->session->userdata('whsid');
        $diskon = 6;	// modified by Boby 20130301
		$diskon1 = $diskon/100;
		$rpdiskon = round(($totalharga * ($diskon1)),0);
		$nettotalharga = $totalharga - $rpdiskon;
		
        $data = array(
            'member_id' => $empid,
            'stockiest_id'=>$this->input->post('member_id'),
            'date' => date('Y-m-d',now()),
			'diskon' => $diskon,
			'rpdiskon' => $rpdiskon,
            'totalharga' => $nettotalharga,
			'totalharga2' => $totalharga,
            'totalpv' => $totalpv,
			'totalbv' => $totalbv,
            'warehouse_id' => $whsid,
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('ro',$data);
        
        $id = $this->db->insert_id();
        
		// Created by Boby 20130301
		for($i=0;$i<10;$i++){
			$qty[$i] = str_replace(".","",$this->input->post('qty'.$i));
			if($this->input->post('itemcode'.$i) and $qty[$i] > 0){
				$hrg[$i] = str_replace(".","",$this->input->post('price'.$i)) * (1-$diskon1);
				$jml[$i] = $qty[$i] * $hrg[$i];
				$data=array(
					'ro_id' => $id,
					'item_id' => $this->input->post('itemcode'.$i),
					'qty' => $qty[$i],
					'harga' => $hrg[$i],
					'pv' => str_replace(".","",$this->input->post('pv'.$i)),
					'jmlharga' => $jml[$i],
					'jmlpv' => str_replace(".","",$this->input->post('subtotalpv'.$i)),
					'bv' => str_replace(".","",$this->input->post('bv'.$i)),
					'jmlbv' => str_replace(".","",$this->input->post('subtotalbv'.$i))
				);
				$this->db->insert('ro_d',$data);
			}
		}
		$this->db->query("call sp_request_order('$id','$whsid','$empid','$nettotalharga','$empid')");
		// End created by Boby 20130301
		
		/* 
		//modified by Boby 20130301
		$qty0 = str_replace(".","",$this->input->post('qty0'));
        if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                'harga' => str_replace(".","",$this->input->post('price0')),
                'pv' => str_replace(".","",$this->input->post('pv0')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal0')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv0')),
				'bv' => str_replace(".","",$this->input->post('bv0')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv0'))
            );
			$this->db->insert('ro_d',$data);
		}
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => str_replace(".","",$this->input->post('price1')),
                'pv' => str_replace(".","",$this->input->post('pv1')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal1')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv1')),
				'bv' => str_replace(".","",$this->input->post('bv1')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv1'))                
            );
            $this->db->insert('ro_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => str_replace(".","",$this->input->post('price2')),
                'pv' => str_replace(".","",$this->input->post('pv2')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal2')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv2')),
				'bv' => str_replace(".","",$this->input->post('bv2')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv2'))
            );
            $this->db->insert('ro_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => str_replace(".","",$this->input->post('price3')),
                'pv' => str_replace(".","",$this->input->post('pv3')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal3')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv3')),
				'bv' => str_replace(".","",$this->input->post('bv3')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv3'))
            );
            $this->db->insert('ro_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => str_replace(".","",$this->input->post('price4')),
                'pv' => str_replace(".","",$this->input->post('pv4')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal4')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv4')),
				'bv' => str_replace(".","",$this->input->post('bv4')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv4'))
            );
            $this->db->insert('ro_d',$data);
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => str_replace(".","",$this->input->post('price5')),
                'pv' => str_replace(".","",$this->input->post('pv5')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal5')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv5')),
				'bv' => str_replace(".","",$this->input->post('bv5')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv5'))
            );
            $this->db->insert('ro_d',$data);
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => str_replace(".","",$this->input->post('price6')),
                'pv' => str_replace(".","",$this->input->post('pv6')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal6')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv6')),
				'bv' => str_replace(".","",$this->input->post('bv6')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv6'))
            );
            $this->db->insert('ro_d',$data);
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => str_replace(".","",$this->input->post('price7')),
                'pv' => str_replace(".","",$this->input->post('pv7')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal7')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv7')),
				'bv' => str_replace(".","",$this->input->post('bv7')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv7'))
            );
            $this->db->insert('ro_d',$data);
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => str_replace(".","",$this->input->post('price8')),
                'pv' => str_replace(".","",$this->input->post('pv8')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal8')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv8')),
				'bv' => str_replace(".","",$this->input->post('bv8')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv8'))
            );
            $this->db->insert('ro_d',$data);
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
       if($this->input->post('itemcode9') and $qty9 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                'harga' => str_replace(".","",$this->input->post('price9')),
                'pv' => str_replace(".","",$this->input->post('pv9')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal9')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv9')),
				'bv' => str_replace(".","",$this->input->post('bv9')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv9'))
            );
            $this->db->insert('ro_d',$data);
        }
        $this->db->query("call sp_request_order('$id','$whsid','$empid','$nettotalharga','$empid')");
		//end modified by Boby 20130301 
		*/
    }
	
	/*
    public function addRequestOrder(){
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
		$totalbv = str_replace(".","",$this->input->post('totalbv'));
        $empid = $this->session->userdata('userid');
        $whsid = $this->session->userdata('whsid');
        $diskon = 3;
		$rpdiskon = round(($totalharga * 0.03),0);
		$nettotalharga = $totalharga - $rpdiskon;
		
        $data = array(
            'member_id' => $empid,
            'stockiest_id'=>$this->input->post('member_id'),
            'date' => date('Y-m-d',now()),
			'diskon' => $diskon,
			'rpdiskon' => $rpdiskon,
            'totalharga' => $nettotalharga,
			'totalharga2' => $totalharga,
            'totalpv' => $totalpv,
			'totalbv' => $totalbv,
            'warehouse_id' => $whsid,
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('ro',$data);
        
        $id = $this->db->insert_id();
        
		$qty0 = str_replace(".","",$this->input->post('qty0'));
        if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                'harga' => str_replace(".","",$this->input->post('price0')),
                'pv' => str_replace(".","",$this->input->post('pv0')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal0')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv0')),
				'bv' => str_replace(".","",$this->input->post('bv0')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv0'))
            );
			$this->db->insert('ro_d',$data);
		}
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => str_replace(".","",$this->input->post('price1')),
                'pv' => str_replace(".","",$this->input->post('pv1')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal1')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv1')),
				'bv' => str_replace(".","",$this->input->post('bv1')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv1'))                
            );
            $this->db->insert('ro_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => str_replace(".","",$this->input->post('price2')),
                'pv' => str_replace(".","",$this->input->post('pv2')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal2')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv2')),
				'bv' => str_replace(".","",$this->input->post('bv2')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv2'))
            );
            $this->db->insert('ro_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => str_replace(".","",$this->input->post('price3')),
                'pv' => str_replace(".","",$this->input->post('pv3')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal3')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv3')),
				'bv' => str_replace(".","",$this->input->post('bv3')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv3'))
            );
            $this->db->insert('ro_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => str_replace(".","",$this->input->post('price4')),
                'pv' => str_replace(".","",$this->input->post('pv4')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal4')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv4')),
				'bv' => str_replace(".","",$this->input->post('bv4')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv4'))
            );
            $this->db->insert('ro_d',$data);
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => str_replace(".","",$this->input->post('price5')),
                'pv' => str_replace(".","",$this->input->post('pv5')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal5')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv5')),
				'bv' => str_replace(".","",$this->input->post('bv5')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv5'))
            );
            $this->db->insert('ro_d',$data);
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => str_replace(".","",$this->input->post('price6')),
                'pv' => str_replace(".","",$this->input->post('pv6')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal6')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv6')),
				'bv' => str_replace(".","",$this->input->post('bv6')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv6'))
            );
            $this->db->insert('ro_d',$data);
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => str_replace(".","",$this->input->post('price7')),
                'pv' => str_replace(".","",$this->input->post('pv7')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal7')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv7')),
				'bv' => str_replace(".","",$this->input->post('bv7')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv7'))
            );
            $this->db->insert('ro_d',$data);
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => str_replace(".","",$this->input->post('price8')),
                'pv' => str_replace(".","",$this->input->post('pv8')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal8')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv8')),
				'bv' => str_replace(".","",$this->input->post('bv8')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv8'))
            );
            $this->db->insert('ro_d',$data);
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
       if($this->input->post('itemcode9') and $qty9 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                'harga' => str_replace(".","",$this->input->post('price9')),
                'pv' => str_replace(".","",$this->input->post('pv9')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal9')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv9')),
				'bv' => str_replace(".","",$this->input->post('bv9')),
				'jmlbv' => str_replace(".","",$this->input->post('subtotalbv9'))
            );
            $this->db->insert('ro_d',$data);
        }
		
        $this->db->query("call sp_request_order('$id','$whsid','$empid','$nettotalharga','$empid')");       
    }
	*/
    public function addRequestOrderTemp(){
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
        $empid = $this->session->userdata('userid');
        $whsid = $this->session->userdata('whsid');
        $diskon = 3;
		$rpdiskon = round(($totalharga * 0.03),0);
		$nettotalharga = $totalharga - $rpdiskon;
		
        $data = array(
            'member_id' => $empid,
            'stockiest_id'=>$this->input->post('member_id'),
            'date' => date('Y-m-d',now()),
			'diskon' => $diskon,
			'rpdiskon' => $rpdiskon,
            'totalharga' => $nettotalharga,
			'totalharga2' => $totalharga,
            'totalpv' => $totalpv,
            'warehouse_id' => $whsid,
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('ro_temp',$data);
        
        $id = $this->db->insert_id();
        
        $qty0 = str_replace(".","",$this->input->post('qty0'));
        if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                'harga' => str_replace(".","",$this->input->post('price0')),
                'pv' => str_replace(".","",$this->input->post('pv0')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal0')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv0'))
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => str_replace(".","",$this->input->post('price1')),
                'pv' => str_replace(".","",$this->input->post('pv1')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal1')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv1'))                
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => str_replace(".","",$this->input->post('price2')),
                'pv' => str_replace(".","",$this->input->post('pv2')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal2')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv2'))
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => str_replace(".","",$this->input->post('price3')),
                'pv' => str_replace(".","",$this->input->post('pv3')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal3')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv3'))
            );
            
            $this->db->insert('ro_temp_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => str_replace(".","",$this->input->post('price4')),
                'pv' => str_replace(".","",$this->input->post('pv4')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal4')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv4'))
            );
            
            $this->db->insert('ro_temp_d',$data);
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => str_replace(".","",$this->input->post('price5')),
                'pv' => str_replace(".","",$this->input->post('pv5')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal5')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv5'))
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => str_replace(".","",$this->input->post('price6')),
                'pv' => str_replace(".","",$this->input->post('pv6')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal6')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv6'))
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => str_replace(".","",$this->input->post('price7')),
                'pv' => str_replace(".","",$this->input->post('pv7')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal7')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv7'))
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => str_replace(".","",$this->input->post('price8')),
                'pv' => str_replace(".","",$this->input->post('pv8')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal8')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv8'))
            );
            
            $this->db->insert('ro_temp_d',$data);
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
       if($this->input->post('itemcode9') and $qty9 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                'harga' => str_replace(".","",$this->input->post('price9')),
                'pv' => str_replace(".","",$this->input->post('pv9')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal9')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv9'))
            );
            
            $this->db->insert('ro_temp_d',$data);
        }
    }
    public function getRequestOrder($id=0){
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.diskon
		,format(a.rpdiskon,0)as frpdiskon
		,format(a.totalharga2,0)as ftotalharga2
		,a.totalharga2
		,a.member_id
		,format(a.totalharga,0)as ftotalharga
		,a.totalharga
		,format(a.totalpv,0)as ftotalpv
		,a.remark,a.remarkapp,a.status,a.tglapproved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    s.no_stc,x.no_stc as no_stc2,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('ro a');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->join('stockiest x','a.stockiest_id=x.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('member z','a.stockiest_id=z.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->session->userdata('group_id')>100)$this->db->where('a.member_id',$this->session->userdata('userid'));
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$this->db->where('a.warehouse_id',$whsid); 
        }
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getRequestOrderDetail($id=0){
        $data = array();
        $this->db->select("d.item_id,format(d.qty,0)as fqty
			
			,format(d.harga_,0)as fharga
			,format(d.pv,0)as fpv
			
			,format(sum(d.qty*d.harga_),0)as fsubtotal
			,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name",false);
        $this->db->from('ro_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.ro_id',$id);
        $this->db->group_by('d.id');
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function requestOrderApproved(){
        if($this->input->post('p_id')){
            $row = array();
            
            $empid = $this->session->userdata('user');
            $idlist = implode(",",array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";
            
            $option = $where. " and status = 'pending'";
            $row = $this->_countApproved($option);
            $remarkapp=$this->db->escape_str($this->input->post('remark'));
            
            if($row){
                $data = array(
                    'status'=> 'delivery',
                    'remarkapp'=>$remarkapp,
                    'approvedby'=>$this->session->userdata('user'),
                    'tglapproved' => date('Y-m-d', now())
                );
                $this->db->update('ro',$data,$option);
                
                $this->session->set_flashdata('message','Delivery approved successfully');
            }else{
                $this->session->set_flashdata('message','Nothing to delevery approved!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing to delevery approved!');
        }
    }
    protected function _countApproved($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('ro');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getEwallet($member_id){
        $q = $this->db->select("ewallet,format(ewallet,0)as fewallet",false)
            ->from('stockiest')
            ->where('id',$member_id)
            ->get();
        return $var = ($q->num_rows()>0)? $q->row() : false;
    }
	public function getECB($member_id){
        $q = $this->db->select("saldo_ecb as saldoecb,format(saldo_ecb,0)as fsaldoecb",false)
            ->from('deposit_exclusive')
            ->where('member_id',$member_id)
            ->get();
        return $var = ($q->num_rows()>0)? $q->row() : false;
    }
    
    public function searchROTemp(){
        $data = array();
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            if($this->session->userdata('group_id')==102){
                $where = "a.stockiest_id = '$memberid'";
            }else{
                $where = "a.member_id = '$memberid'";
            }
        }
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.member_id,s.no_stc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama,a.remarkapp,a.status",false);
        $this->db->from('ro_temp a');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        if($this->session->userdata('group_id')>100)$this->db->where($where);
        $this->db->order_by('a.id','desc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getRequestOrderTemp($id=0){
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.remarkapp,a.status,a.tglapproved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    s.no_stc,x.no_stc as no_stc2,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('ro_temp a');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->join('stockiest x','a.stockiest_id=x.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('member z','a.stockiest_id=z.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->where('a.member_id',$this->session->userdata('userid'));
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
	
    public function getRequestOrderDetailTemp($id=0){
        $data = array();
        $this->db->select("d.item_id,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.pv,0)as fpv,format(sum(d.qty*d.harga),0)as fsubtotal,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name",false);
        $this->db->from('ro_temp_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.ro_id',$id);
        $this->db->group_by('d.id');
        $q=$this->db->get();
		//echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
    public function requestOrderTempDel(){
        if($this->input->post('p_id')){
            $row = array();
            
            $idlist = implode(",",array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";
            $where_d = "ro_id in ($idlist)";
            
            $row = $this->_countDelete($where);
            if($row){
                $this->db->delete('ro_temp',$where);
                $this->db->delete('ro_temp_d',$where_d);
                $this->session->set_flashdata('message','Delete Request Order M-STC to STC successfully');
            }else{
                $this->session->set_flashdata('message','Nothing to Delete Request Order M-STC to STC!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing to Delete Request Order M-STC to STC!');
        }
    }
	
    private function _countDelete($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('ro_temp');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Request Order Stockiest
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-04-15
    |
    */
    public function searchROMSTC($keywords=0,$num,$offset){
        $data = array();
        if($this->session->userdata('group_id')==102){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        else{
			/*Modified by Boby 2009-11-23*/
			//$where = "a.stockiest_id != '0' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
			/*end Modified by Boby 2009-11-23*/
            $where = "a.stockiest_id = '0' and s.type = '2' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.member_id,s.no_stc,x.no_stc as no_stc2,z.nama as namastc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama,a.remarkapp,a.status",false);
        $this->db->from('ro a');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('stockiest x','a.stockiest_id=x.id','left');
        $this->db->join('member z','a.stockiest_id=z.id','left');
        $this->db->where($where);
        $this->db->order_by('a.status','asc');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countROMSTC($keywords=0){
        if($this->session->userdata('group_id')==102){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        else{
            $where = "a.stockiest_id = '0' and s.type = '2' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        $this->db->from('ro a');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    public function getRequestOrderMSTC($id=0){
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.diskon,format(a.rpdiskon,0)as frpdiskon,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.remarkapp,a.status,a.tglapproved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    s.no_stc,x.no_stc as no_stc2,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('ro a');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->join('stockiest x','a.stockiest_id=x.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('member z','a.stockiest_id=z.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->session->userdata('group_id')==102)$this->db->where('a.stockiest_id',$this->session->userdata('userid'));
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getRequestOrderTempMSTC($id=0){
        $data = array();
        $this->db->select("a.id,date_format(a.date,'%d-%b-%Y')as date,a.stockiest_id,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.remarkapp,a.status,a.tglapproved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    s.no_stc,x.no_stc as no_stc2,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('ro_temp a');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        $this->db->join('stockiest x','a.stockiest_id=x.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('member z','a.stockiest_id=z.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->session->userdata('group_id')>100)$this->db->where('a.stockiest_id',$this->session->userdata('userid'));
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function check_titipan_ro($rqtid,$stcid)
    {
        $data=array();
        $q = $this->db->query("SELECT f_check_titipan_ro('$rqtid','$stcid') as l_result");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['l_result'];
    }
    public function addROApproved(){
        $empid = $this->session->userdata('userid');
        $rqtid = $this->input->post('id');
        
        $data=array(
            'status'=>'delivery',
            'tglapproved'=>date('Y-m-d',now()),
            'approvedby'=>$empid,
            'remarkapp'=>$this->db->escape_str($this->input->post('remark'))
        );
        $this->db->update('ro_temp',$data,array('id'=>$rqtid));
        
        $this->db->query("call sp_ro_mstc('$rqtid','$empid')");
    }
    
    
    
   public function addRequestOrderMSTC(){
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
        $empid = $this->session->userdata('userid');
        //echo $empid;
        $whsid = $this->session->userdata('whsid');
        
        $data = array(
            'member_id' => $this->input->post('member_id'),
            'stockiest_id'=>$empid,
            'date' => date('Y-m-d',now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'status' => 'delivery',
            'warehouse_id' => $whsid,
            'remarkapp'=>$this->db->escape_str($this->input->post('remark')),
            'tglapproved'=>date('Y-m-d',now()),
            'approvedby'=>$empid,
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('ro_temp',$data);
        
        $id = $this->db->insert_id();
        
        $qty0 = str_replace(".","",$this->input->post('qty0'));
        if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                'harga' => str_replace(".","",$this->input->post('price0')),
                'pv' => str_replace(".","",$this->input->post('pv0')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal0')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv0')),
                'titipan_id' => $this->input->post('titipan_id0')
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => str_replace(".","",$this->input->post('price1')),
                'pv' => str_replace(".","",$this->input->post('pv1')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal1')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv1')),
                'titipan_id' => $this->input->post('titipan_id1')
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => str_replace(".","",$this->input->post('price2')),
                'pv' => str_replace(".","",$this->input->post('pv2')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal2')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv2')),
                'titipan_id' => $this->input->post('titipan_id2')
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => str_replace(".","",$this->input->post('price3')),
                'pv' => str_replace(".","",$this->input->post('pv3')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal3')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv3')),
                'titipan_id' => $this->input->post('titipan_id3')
            );
            
            $this->db->insert('ro_temp_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => str_replace(".","",$this->input->post('price4')),
                'pv' => str_replace(".","",$this->input->post('pv4')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal4')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv4')),
                'titipan_id' => $this->input->post('titipan_id4')
            );
            
            $this->db->insert('ro_temp_d',$data);
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => str_replace(".","",$this->input->post('price5')),
                'pv' => str_replace(".","",$this->input->post('pv5')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal5')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv5')),
                'titipan_id' => $this->input->post('titipan_id5')
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => str_replace(".","",$this->input->post('price6')),
                'pv' => str_replace(".","",$this->input->post('pv6')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal6')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv6')),
                'titipan_id' => $this->input->post('titipan_id6')
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => str_replace(".","",$this->input->post('price7')),
                'pv' => str_replace(".","",$this->input->post('pv7')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal7')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv7')),
                'titipan_id' => $this->input->post('titipan_id7')
            );
            
            $this->db->insert('ro_temp_d',$data);
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => str_replace(".","",$this->input->post('price8')),
                'pv' => str_replace(".","",$this->input->post('pv8')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal8')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv8')),
                'titipan_id' => $this->input->post('titipan_id8')
            );
            
            $this->db->insert('ro_temp_d',$data);
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
       if($this->input->post('itemcode9') and $qty9 > 0){
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                'harga' => str_replace(".","",$this->input->post('price9')),
                'pv' => str_replace(".","",$this->input->post('pv9')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal9')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv9')),
                'titipan_id' => $this->input->post('titipan_id9')
            );
            
            $this->db->insert('ro_temp_d',$data);
        }
        return $id;
    }
    public function check_titipan($rqtid)
    {
        $data=array();
        $q = $this->db->query("SELECT f_check_titipan('$rqtid') as l_result");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['l_result'];
    }
    public function addROMSTC($rqtid)
    {
        $empid = $this->session->userdata('userid');
        $this->db->query("call sp_ro_mstc('$rqtid','$empid')");
    }
    
   /*
    |--------------------------------------------------------------------------
    | Request Order New Stockiest Special Diskon
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-06
    |
    */ 
    public function addRequestOrderDiskon(){
        $amount = str_replace(".","",$this->input->post('total'));
		$totalharga2 = $amount;
        if($this->input->post('persen') >0){            
            $diskon = $this->input->post('persen');
            $rpdiskon = $amount * $this->input->post('persen') / 100;
        }else{
            $diskon=0;
            $rpdiskon=0;
        }
		if($this->input->post('persen1') >0){
            $diskonecb = $this->input->post('persen1');
            $rpdiskonecb = $amount * $this->input->post('persen1') / 100;
        }else{
            $diskonecb=0;
            $rpdiskonecb=0;
        }
		
        $totalharga = $amount - $rpdiskon - $rpdiskonecb;
			
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
        $empid = $this->session->userdata('user');
        $whsid = $this->session->userdata('whsid');
        $member_id = $this->input->post('member_id');
        
        $data = array(
            'member_id' => $member_id,
            'stockiest_id'=>'0',
            'date' => date('Y-m-d',now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'diskon' => $diskon+$diskonecb,					// updated by Boby 2012-05-14
            'rpdiskon' => $rpdiskon+$rpdiskonecb,
            'totalharga2' => $totalharga2,
            'warehouse_id' => $whsid,
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('ro',$data);
        
        $id = $this->db->insert_id();
        
        $qty0 = str_replace(".","",$this->input->post('qty0'));
        if($this->input->post('itemcode0') and $qty0 > 0){
			// updated by Boby 2012-05-14
			$hrg = str_replace(".","",$this->input->post('price0'));
			$hrg = $hrg * (1-(($diskon+$diskonecb)/100));
			// end updated by Boby 2012-05-14
            $data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
				// updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price0')),
				'harga_' => str_replace(".","",$this->input->post('price0')),
				'harga' => $hrg,
				// end updated by Boby 2012-05-14
                'pv' => str_replace(".","",$this->input->post('pv0')),
                'jmlharga' => $hrg*$qty0,// 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv0'))
            );
            
            $this->db->insert('ro_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            // updated by Boby 2012-05-14
			$hrg = str_replace(".","",$this->input->post('price1'));
			$hrg = $hrg * (1-(($diskon+$diskonecb)/100));
			// end updated by Boby 2012-05-14
			$data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price1')),
				'harga_' => str_replace(".","",$this->input->post('price1')),
				'harga' => $hrg,
				// end updated by Boby 2012-05-14
                'pv' => str_replace(".","",$this->input->post('pv1')),
                'jmlharga' => $hrg*$qty1,// 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv1'))                
            );
            
            $this->db->insert('ro_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
             // updated by Boby 2012-05-14
			$hrg = str_replace(".","",$this->input->post('price2'));
			$hrg = $hrg * (1-(($diskon+$diskonecb)/100));
			// end updated by Boby 2012-05-14
			$data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price2')),
				'harga_' => str_replace(".","",$this->input->post('price2')),
				'harga' => $hrg,
				// end updated by Boby 2012-05-14
                'pv' => str_replace(".","",$this->input->post('pv2')),
                'jmlharga' => $hrg*$qty2,// 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv2'))
            );
            
            $this->db->insert('ro_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
             // updated by Boby 2012-05-14
			$hrg = str_replace(".","",$this->input->post('price3'));
			$hrg = $hrg * (1-(($diskon+$diskonecb)/100));
			// end updated by Boby 2012-05-14
			$data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price3')),
				'harga_' => str_replace(".","",$this->input->post('price3')),
				'harga' => $hrg,
				// end updated by Boby 2012-05-14
                'pv' => str_replace(".","",$this->input->post('pv3')),
                'jmlharga' => $hrg*$qty3,// 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv3'))
            );
            
            $this->db->insert('ro_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
             // updated by Boby 2012-05-14
			$hrg = str_replace(".","",$this->input->post('price4'));
			$hrg = $hrg * (1-(($diskon+$diskonecb)/100));
			// end updated by Boby 2012-05-14
			$data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price4')),
				'harga_' => str_replace(".","",$this->input->post('price4')),
				'harga' => $hrg,
				// end updated by Boby 2012-05-14
                'pv' => str_replace(".","",$this->input->post('pv4')),
                'jmlharga' => $hrg*$qty4,// 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv4'))
            );
            
            $this->db->insert('ro_d',$data);
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
             // updated by Boby 2012-05-14
			$hrg = str_replace(".","",$this->input->post('price5'));
			$hrg = $hrg * (1-(($diskon+$diskonecb)/100));
			// end updated by Boby 2012-05-14
			$data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price5')),
				'harga_' => str_replace(".","",$this->input->post('price5')),
				'harga' => $hrg,
				// end updated by Boby 2012-05-14
                'pv' => str_replace(".","",$this->input->post('pv5')),
                'jmlharga' => $hrg*$qty5,// 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv5'))
            );
            
            $this->db->insert('ro_d',$data);
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
             // updated by Boby 2012-05-14
			$hrg = str_replace(".","",$this->input->post('price6'));
			$hrg = $hrg * (1-(($diskon+$diskonecb)/100));
			// end updated by Boby 2012-05-14
			$data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price6')),
				'harga_' => str_replace(".","",$this->input->post('price6')),
				'harga' => $hrg,
				// end updated by Boby 2012-05-14
                'pv' => str_replace(".","",$this->input->post('pv6')),
                'jmlharga' => $hrg*$qty6,// 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv6'))
            );
            
            $this->db->insert('ro_d',$data);
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
             // updated by Boby 2012-05-14
			$hrg = str_replace(".","",$this->input->post('price7'));
			$hrg = $hrg * (1-(($diskon+$diskonecb)/100));
			// end updated by Boby 2012-05-14
			$data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price7')),
				'harga_' => str_replace(".","",$this->input->post('price7')),
				'harga' => $hrg,
				// end updated by Boby 2012-05-14
                'pv' => str_replace(".","",$this->input->post('pv7')),
                'jmlharga' => $hrg*$qty7,// 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv7'))
            );
            
            $this->db->insert('ro_d',$data);
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
             // updated by Boby 2012-05-14
			$hrg = str_replace(".","",$this->input->post('price8'));
			$hrg = $hrg * (1-(($diskon+$diskonecb)/100));
			// end updated by Boby 2012-05-14
			$data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price8')),
				'harga_' => str_replace(".","",$this->input->post('price8')),
				'harga' => $hrg,
				// end updated by Boby 2012-05-14
                'pv' => str_replace(".","",$this->input->post('pv8')),
                'jmlharga' => $hrg*$qty8,// 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv8'))
            );
            
            $this->db->insert('ro_d',$data);
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
       if($this->input->post('itemcode9') and $qty9 > 0){
             // updated by Boby 2012-05-14
			$hrg = str_replace(".","",$this->input->post('price9'));
			$hrg = $hrg * (1-(($diskon+$diskonecb)/100));
			// end updated by Boby 2012-05-14
			$data=array(
                'ro_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                // updated by Boby 2012-05-14
                // 'harga' => str_replace(".","",$this->input->post('price9')),
				'harga_' => str_replace(".","",$this->input->post('price9')),
				'harga' => $hrg,
				// end updated by Boby 2012-05-14
                'pv' => str_replace(".","",$this->input->post('pv9')),
                'jmlharga' => $hrg*$qty9,// 'jmlharga' => str_replace(".","",$this->input->post('subtotal0')), // updated by Boby 2012-06-11
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv9'))
            );
            
            $this->db->insert('ro_d',$data);
        }
		
		$ecb = str_replace(".","",$this->input->post('rpdiskon1'));
		if($ecb > 0){
			$this->db->query("call sp_cut_deposit_ecb('$member_id','$ecb', '$id','$empid')");
		}
        $this->db->query("call sp_request_order('$id','$whsid','$member_id','$totalharga','$empid')");
    }

	/*Updated by Boby (2009-11-18)*/
	/*Updated by Boby (2011-01-04)*/
	public function cekStok($whsid, $brg, $hrg, $jml, $id){
		$stok = 0;
		if($this->session->userdata('group_id') < 100 or $id == 0){
		    $q = $this->db->select("s.qty as qty",false)
		    ->from('stock s')
		    ->where("item_id = '$brg'")
		    ->where("warehouse_id = '$whsid'")
            	->get();
		}else{
			$where = array('item_id'=>$brg,'harga'=>$hrg,'member_id'=>$id); //Updated by Boby (2011-01-04)
			$q = $this->db->select("s.qty as qty",false)
            	->from('v_titipan s')
            	->where($where)
            	->get();
		}
		/*
		$q = $this->db->select("s.qty as qty",false)
            ->from('stock s')
            ->where("item_id = '$brg'")
            ->get();
		*/
		//echo $this->db->last_query()." ".$jml." ".$this->input->post('qty0');
		if($q->num_rows > 0){
			$row = $q->row_array();
			if(($row['qty']) >= $jml){
				return 'Ready';    
			}else{ 
	      		return $row['qty'];
			}
		}else{
			return 'Limited';
		}
	}   
	public function prnt($jml){
		echo $jml." ";
	}
}
?>