<?php
class MEwallet extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | report view balance ewallet stockiest dan admin
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-08
    |
    */
    
    public function getBalanceEwallet($memberid,$fromdate,$todate){
        $data=array();
        if($memberid)$where = array('s.member_id'=>$memberid,'e.date >='=>$fromdate,'e.date <='=>$todate);
        else $where = array('s.member_id'=>'company','e.date >='=>$fromdate,'e.date <='=>$todate);
        
        $q=$this->db->select("concat(s.noref,' - ',s.description)as description,s.created,createdby,format(s.saldoawal,0)as fsaldoawal,format(s.kredit,0)as fkredit,format(s.debet,0)as fdebet,format(s.saldoakhir,0)as fsaldoakhir", false)
            ->from('ewallet_stc_d s')
            ->join('ewallet_stc e','s.ewallet_stc_id=e.id','left')
            ->where($where)
            ->order_by('s.id','asc')
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sumBalanceEwallet($memberid,$fromdate,$todate){
        $data=array();
        if($memberid)$where = array('s.member_id'=>$memberid,'e.date >='=>$fromdate,'e.date <='=>$todate);
        else $where = array('s.member_id'=>'company','e.date >='=>$fromdate,'e.date <='=>$todate);
        
        $q=$this->db->select("format(sum(s.kredit),0)as fkredit,format(sum(s.debet),0)as fdebet", false)
            ->from('ewallet_stc_d s')
            ->join('ewallet_stc e','s.ewallet_stc_id=e.id','left')
            ->where($where)
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    public function getEwallet($memberid){
        $data=array();
        if(!$memberid){
            $q=$this->db->select("format(saldoakhir,0)as fewallet",false)
                ->from('ewallet_stc_d')
                ->where('member_id','company')
                ->order_by('id','desc')
                ->limit(1)
                ->get();
        }else{
            if($this->uri->segment(2) == 'bemem'){
                $q=$this->db->select("ewallet,format(ewallet,0)as fewallet",false)
                    ->from('member')
                    ->where('id',$memberid)
                    ->get();
            }else{
                $q=$this->db->select("ewallet,format(ewallet,0)as fewallet",false)
                    ->from('stockiest')
                    ->where('id',$memberid)
                    ->get();
            }
        }
        
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | report view balance ewallet member dan admin
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-05-02
    |
    */
    
    public function getBalanceEwalletMember($memberid,$fromdate,$todate){
        $data=array();
        if($memberid)$where = array('s.member_id'=>$memberid,'e.date >='=>$fromdate,'e.date <='=>$todate);
        else $where = array('s.member_id'=>'company','e.date >='=>$fromdate,'e.date <='=>$todate);
        
        $q=$this->db->select("concat(s.noref,' - ',s.description)as description,s.created,createdby,format(s.saldoawal,0)as fsaldoawal,format(s.kredit,0)as fkredit,format(s.debet,0)as fdebet,format(s.saldoakhir,0)as fsaldoakhir", false)
            ->from('ewallet_mem_d s')
            ->join('ewallet_mem e','s.ewallet_mem_id=e.id','left')
            ->where($where)
            ->order_by('s.id','asc')
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function sumBalanceEwalletMember($memberid,$fromdate,$todate){
        $data=array();
        if($memberid)$where = array('s.member_id'=>$memberid,'e.date >='=>$fromdate,'e.date <='=>$todate);
        else $where = array('s.member_id'=>'company','e.date >='=>$fromdate,'e.date <='=>$todate);
        
        $q=$this->db->select("format(sum(s.kredit),0)as fkredit,format(sum(s.debet),0)as fdebet", false)
            ->from('ewallet_mem_d s')
            ->join('ewallet_mem e','s.ewallet_mem_id=e.id','left')
            ->where($where)
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
}
?>