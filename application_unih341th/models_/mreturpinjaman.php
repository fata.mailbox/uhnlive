<?php
class MReturpinjaman extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Retur Stock Pinjaman Stockiest
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-29
    |
    */
    
    public function searchReturPinjaman($keywords=0,$num,$offset){
        $data = array();
        $whsid = $this->session->userdata('whsid');
        if($whsid > 1)$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
        else $where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        
        $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.createdby,a.stockiest_id,s.no_stc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama,a.remark",false);
        $this->db->from('retur_pinjaman a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countReturPinjaman($keywords=0){
        $whsid = $this->session->userdata('whsid');
        if($whsid > 1)$where = "a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
        else $where = "( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        
        $this->db->select("a.id");
        $this->db->from('retur_pinjaman a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    public function getDropDownWhs($all=''){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            if($all == 'all')$data['all']='All Cabang';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    public function addReturTitipanTemp(){
        $member_id = $this->input->post('member_id');
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
        
        $empid = $this->session->userdata('user');
        $whsid = $this->input->post('warehouse_id');
        
        $data = array(
            'stockiest_id' => $member_id,
            'tgl' => date('Y-m-d',now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'warehouse_id' => $whsid,
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('retur_titipan_temp',$data);
        
        $id = $this->db->insert_id();
        
        $qty0 = str_replace(".","",$this->input->post('qty0'));
        if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'retur_titipan_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                'harga' => str_replace(".","",$this->input->post('price0')),
                'pv' => str_replace(".","",$this->input->post('pv0')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal0')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv0'))
            );
            
            $this->db->insert('retur_titipan_temp_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'retur_titipan_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => str_replace(".","",$this->input->post('price1')),
                'pv' => str_replace(".","",$this->input->post('pv1')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal1')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv1'))                
            );
            
            $this->db->insert('retur_titipan_temp_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'retur_titipan_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => str_replace(".","",$this->input->post('price2')),
                'pv' => str_replace(".","",$this->input->post('pv2')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal2')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv2'))
            );
            
            $this->db->insert('retur_titipan_temp_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'retur_titipan_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => str_replace(".","",$this->input->post('price3')),
                'pv' => str_replace(".","",$this->input->post('pv3')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal3')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv3'))
            );
            
            $this->db->insert('retur_titipan_temp_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'retur_titipan_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => str_replace(".","",$this->input->post('price4')),
                'pv' => str_replace(".","",$this->input->post('pv4')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal4')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv4'))
            );
            
            $this->db->insert('retur_titipan_temp_d',$data);
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
            $data=array(
                'retur_titipan_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => str_replace(".","",$this->input->post('price5')),
                'pv' => str_replace(".","",$this->input->post('pv5')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal5')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv5'))
            );
            
            $this->db->insert('retur_titipan_temp_d',$data);
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
            $data=array(
                'retur_titipan_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => str_replace(".","",$this->input->post('price6')),
                'pv' => str_replace(".","",$this->input->post('pv6')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal6')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv6'))
            );
            
            $this->db->insert('retur_titipan_temp_d',$data);
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
            $data=array(
                'retur_titipan_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => str_replace(".","",$this->input->post('price7')),
                'pv' => str_replace(".","",$this->input->post('pv7')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal7')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv7'))
            );
            
            $this->db->insert('retur_titipan_temp_d',$data);
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
            $data=array(
                'retur_titipan_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => str_replace(".","",$this->input->post('price8')),
                'pv' => str_replace(".","",$this->input->post('pv8')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal8')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv8'))
            );
            
            $this->db->insert('retur_titipan_temp_d',$data);
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
       if($this->input->post('itemcode9') and $qty9 > 0){
            $data=array(
                'retur_titipan_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                'harga' => str_replace(".","",$this->input->post('price9')),
                'pv' => str_replace(".","",$this->input->post('pv9')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal9')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv9'))
            );
            
            $this->db->insert('retur_titipan_temp_d',$data);
        }
        return $id;
    }
    public function check_retur_pinjaman($id,$stcid)
    {
        $data=array();
        $q = $this->db->query("SELECT f_check_retur_pinjaman('$id','$stcid') as l_result");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['l_result'];
    }
    public function addReturPinjaman($soid)
    {
        $empid = $this->session->userdata('user');
        $this->db->query("call sp_retur_pinjaman('$soid','$empid')");
    }
    public function getReturPinjaman($id=0){
        $data = array();
        $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.stockiest_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    s.no_stc,m.nama,m.alamat,m.kodepos,k.name as kota,p.name as propinsi,w.name as warehouse",false);
        $this->db->from('retur_pinjaman a');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->join('warehouse w','a.warehouse_id=w.id','left');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getReturPinjamanDetail($id=0){
        $data = array();
        $this->db->select("d.item_id,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.pv,0)as fpv,format(sum(d.qty*d.harga),0)as fsubtotal,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name",false);
        $this->db->from('retur_pinjaman_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.retur_pinjaman_id',$id);
        $this->db->group_by('d.id');
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
}?>