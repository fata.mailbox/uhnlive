<?php
/*
    |--------------------------------------------------------------------------
    | Pinjaman Admin
    |--------------------------------------------------------------------------
    |
    | @author gold_phoenix99@yahoo.com
    | @poweredby Boby UHN02
    | @created 2012-03-16
    |
*/
class MPinjamanadmin extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function addPinjaman(){
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
        $empid = $this->session->userdata('user');
        $whsid = $this->input->post('whsid');
        $admin_id = $this->input->post('member_id');
        
        $data = array(
            'admin_id'=>$admin_id,
            'tgl' => date('Y-m-d',now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'warehouse_id' => $whsid,
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('pinjaman_admin',$data);
        
        $id = $this->db->insert_id();
        
        $qty0 = str_replace(".","",$this->input->post('qty0'));
        if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'pinjaman_admin_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                'harga' => str_replace(".","",$this->input->post('price0')),
                'pv' => str_replace(".","",$this->input->post('pv0')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal0')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv0'))
            );
            
            $this->db->insert('pinjaman_admin_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'pinjaman_admin_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => str_replace(".","",$this->input->post('price1')),
                'pv' => str_replace(".","",$this->input->post('pv1')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal1')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv1'))                
            );
            
            $this->db->insert('pinjaman_admin_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'pinjaman_admin_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => str_replace(".","",$this->input->post('price2')),
                'pv' => str_replace(".","",$this->input->post('pv2')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal2')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv2'))
            );
            
            $this->db->insert('pinjaman_admin_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'pinjaman_admin_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => str_replace(".","",$this->input->post('price3')),
                'pv' => str_replace(".","",$this->input->post('pv3')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal3')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv3'))
            );
            
            $this->db->insert('pinjaman_admin_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'pinjaman_admin_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => str_replace(".","",$this->input->post('price4')),
                'pv' => str_replace(".","",$this->input->post('pv4')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal4')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv4'))
            );
            
            $this->db->insert('pinjaman_admin_d',$data);
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
            $data=array(
                'pinjaman_admin_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => str_replace(".","",$this->input->post('price5')),
                'pv' => str_replace(".","",$this->input->post('pv5')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal5')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv5'))
            );
            
            $this->db->insert('pinjaman_admin_d',$data);
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
            $data=array(
                'pinjaman_admin_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => str_replace(".","",$this->input->post('price6')),
                'pv' => str_replace(".","",$this->input->post('pv6')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal6')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv6'))
            );
            
            $this->db->insert('pinjaman_admin_d',$data);
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
            $data=array(
                'pinjaman_admin_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => str_replace(".","",$this->input->post('price7')),
                'pv' => str_replace(".","",$this->input->post('pv7')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal7')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv7'))
            );
            
            $this->db->insert('pinjaman_admin_d',$data);
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
            $data=array(
                'pinjaman_admin_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => str_replace(".","",$this->input->post('price8')),
                'pv' => str_replace(".","",$this->input->post('pv8')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal8')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv8'))
            );
            
            $this->db->insert('pinjaman_admin_d',$data);
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
       if($this->input->post('itemcode9') and $qty9 > 0){
            $data=array(
                'pinjaman_admin_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                'harga' => str_replace(".","",$this->input->post('price9')),
                'pv' => str_replace(".","",$this->input->post('pv9')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal9')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv9'))
            );
            
            $this->db->insert('pinjaman_admin_d',$data);
        }
        $this->db->query("call sp_pinjaman_admin('$id','$admin_id','$totalharga','$whsid','$empid')");
    }
    
	public function countPinjaman($keywords=0){
        $whsid = $this->session->userdata('whsid');
        if($whsid > 1)$where = "pjm.warehouse_id = '$whsid' and (pjm.id LIKE '$keywords%' or adm.username LIKE '$keywords%' OR adm.name LIKE '$keywords%')";
        else $where = "( pjm.id LIKE '$keywords%' or adm.username LIKE '$keywords%' OR adm.name LIKE '$keywords%')";
        
        $this->db->select("pjm.id");
        $this->db->from('pinjaman_admin pjm');
        $this->db->join('admins adm','pjm.admin_id=adm.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
	public function searchPinjaman($keywords=0,$num,$offset){
        $data = array();
        $whsid = $this->session->userdata('whsid');
        if($whsid > 1)$where = "pjm.warehouse_id = '$whsid' and (pjm.id LIKE '$keywords%' or adm.username LIKE '$keywords%' OR adm.name LIKE '$keywords%')";
        else $where = "( pjm.id LIKE '$keywords%' or adm.username LIKE '$keywords%' OR adm.name LIKE '$keywords%' )";
        
        $this->db->select("pjm.id,date_format(pjm.tgl,'%d-%b-%Y')as tgl,pjm.createdby,pjm.admin_id,adm.username,format(pjm.totalharga,0)as ftotalharga,format(pjm.totalpv,0)as ftotalpv,adm.name,pjm.remark",false);
        $this->db->from('pinjaman_admin pjm');
        $this->db->join('admins adm','pjm.admin_id=adm.id','left');
        $this->db->where($where);
        $this->db->order_by('pjm.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function getPinjaman($id=0){
        $data = array();
        $this->db->select("
			pjm.id, date_format(pjm.tgl,'%d-%b-%Y')as tgl, format(pjm.totalharga,0)as ftotalharga, format(pjm.totalpv,0)as ftotalpv, pjm.remark, date_format(pjm.created,'%d-%b-%Y')as created,pjm.createdby
            , adm.username as no_stc
			, adm.name as nama, whs.address as alamat, '123' as kodepos
			, 'UHN Kota' as kota
			, 'UHN Propinsi' as propinsi
			",false);
        $this->db->from('pinjaman_admin pjm');
        $this->db->join('admins adm','pjm.admin_id=adm.id','left');
		$this->db->join('warehouse whs','pjm.warehouse_id=whs.id','left');
        //$this->db->join('member m','a.stockiest_id=m.id','left');
        //$this->db->join('kota k','m.kota_id=k.id','left');
        //$this->db->join('propinsi p','k.propinsi_id=p.id','left');
        
        $whsid = $this->session->userdata('whsid');
        if($whsid > 1)$this->db->where('pjm.warehouse_id',$whsid); 
        $this->db->where('pjm.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
	
	public function getPinjamanDetail($id=0){
        $data = array();
        $this->db->select("
			d.item_id,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.pv,0)as fpv,format(sum(d.qty*d.harga),0)as fsubtotal,format(sum(d.qty*d.pv),0)as fsubtotalpv
			,a.name
			",false);
        $this->db->from('pinjaman_admin_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.pinjaman_admin_id',$id);
        $this->db->group_by('d.id');
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
    
	/* Retur Pinjaman Staff */
	public function countReturPinjaman($keywords=0){
        $whsid = $this->session->userdata('whsid');
        if($whsid > 1)$where = "pjm.warehouse_id = '$whsid' and (pjm.id LIKE '$keywords%' OR adm.name LIKE '$keywords%')";
        else $where = "( pjm.id LIKE '$keywords%' OR adm.name LIKE '$keywords%' )";
        
        $this->db->select("pjm.id");
        $this->db->from('pinjaman_admin_retur pjm');
        $this->db->join('admins adm','pjm.admin_id=adm.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
	
	public function searchReturPinjaman($keywords=0,$num,$offset){
        $data = array();
        $whsid = $this->session->userdata('whsid');
        if($whsid > 1)$where = "pjm.warehouse_id = '$whsid' and (pjm.id LIKE '$keywords%' OR adm.name LIKE '$keywords%')";
        else $where = "( pjm.id LIKE '$keywords%' OR adm.name LIKE '$keywords%' )";
        
        $this->db->select("
			pjm.id,date_format(pjm.tgl,'%d-%b-%Y')as tgl
			,pjm.createdby
			,pjm.admin_id,adm.username as no_stc
			,format(pjm.totalharga,0)as ftotalharga,format(pjm.totalpv,0)as ftotalpv
			,adm.name as nama
			,pjm.remark",false);
        $this->db->from('pinjaman_admin_retur pjm');
        $this->db->join('admins adm','pjm.admin_id=adm.id','left');
        $this->db->where($where);
        $this->db->order_by('pjm.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
	
    public function getDropDownWhs($all=''){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            if($all == 'all')$data['all']='All Cabang';    
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function addReturTitipanTemp(){
        $member_id = $this->input->post('member_id');
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
        
        $empid = $this->session->userdata('user');
        $whsid = $this->input->post('warehouse_id');
        
        $data = array(
            'admin_id' => $member_id,
            'tgl' => date('Y-m-d',now()),
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'warehouse_id' => $whsid,
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('pinjaman_admin_retur_temp',$data);
        
        $id = $this->db->insert_id();
        
        $qty0 = str_replace(".","",$this->input->post('qty0'));
        if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'pinjaman_admin_retur_temp_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                'harga' => str_replace(".","",$this->input->post('price0')),
                'pv' => str_replace(".","",$this->input->post('pv0')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal0')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv0'))
            );
            
            $this->db->insert('pinjaman_admin_retur_temp_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'pinjaman_admin_retur_temp_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => str_replace(".","",$this->input->post('price1')),
                'pv' => str_replace(".","",$this->input->post('pv1')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal1')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv1'))                
            );
            
            $this->db->insert('pinjaman_admin_retur_temp_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'pinjaman_admin_retur_temp_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => str_replace(".","",$this->input->post('price2')),
                'pv' => str_replace(".","",$this->input->post('pv2')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal2')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv2'))
            );
            
            $this->db->insert('pinjaman_admin_retur_temp_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'pinjaman_admin_retur_temp_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => str_replace(".","",$this->input->post('price3')),
                'pv' => str_replace(".","",$this->input->post('pv3')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal3')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv3'))
            );
            
            $this->db->insert('pinjaman_admin_retur_temp_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'pinjaman_admin_retur_temp_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => str_replace(".","",$this->input->post('price4')),
                'pv' => str_replace(".","",$this->input->post('pv4')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal4')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv4'))
            );
            
            $this->db->insert('pinjaman_admin_retur_temp_d',$data);
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
            $data=array(
                'pinjaman_admin_retur_temp_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => str_replace(".","",$this->input->post('price5')),
                'pv' => str_replace(".","",$this->input->post('pv5')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal5')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv5'))
            );
            
            $this->db->insert('pinjaman_admin_retur_temp_d',$data);
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
            $data=array(
                'pinjaman_admin_retur_temp_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => str_replace(".","",$this->input->post('price6')),
                'pv' => str_replace(".","",$this->input->post('pv6')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal6')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv6'))
            );
            
            $this->db->insert('pinjaman_admin_retur_temp_d',$data);
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
            $data=array(
                'pinjaman_admin_retur_temp_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => str_replace(".","",$this->input->post('price7')),
                'pv' => str_replace(".","",$this->input->post('pv7')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal7')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv7'))
            );
            
            $this->db->insert('pinjaman_admin_retur_temp_d',$data);
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
            $data=array(
                'pinjaman_admin_retur_temp_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => str_replace(".","",$this->input->post('price8')),
                'pv' => str_replace(".","",$this->input->post('pv8')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal8')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv8'))
            );
            
            $this->db->insert('pinjaman_admin_retur_temp_d',$data);
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
       if($this->input->post('itemcode9') and $qty9 > 0){
            $data=array(
                'pinjaman_admin_retur_temp_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                'harga' => str_replace(".","",$this->input->post('price9')),
                'pv' => str_replace(".","",$this->input->post('pv9')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal9')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv9'))
            );
            
            $this->db->insert('pinjaman_admin_retur_temp_d',$data);
        }
        return $id;
    }
	
	public function check_retur_pinjaman($id,$stcid)
    {
        $data=array();
        $q = $this->db->query("SELECT f_check_retur_pinjaman_admin('$id','$stcid') as l_result");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['l_result'];
    }
	
	public function addReturPinjaman($soid)
    {
        $empid = $this->session->userdata('user');
        $this->db->query("call sp_retur_pinjaman_adm('$soid','$empid')");
    }
	
	public function getReturPinjaman($id=0){
        $data = array();
        $this->db->select("
			pjm.id,date_format(pjm.tgl,'%d-%b-%Y')as tgl,pjm.admin_id,format(pjm.totalharga,0)as ftotalharga
			,format(pjm.totalpv,0)as ftotalpv,pjm.remark,date_format(pjm.created,'%d-%b-%Y')as created
			,pjm.createdby as createdby
			,adm.username as no_stc
			,adm.name as nama
			,w.address as alamat,'Kodepos_' as kodepos
			,w.telp as kota,w.fax as propinsi
			,w.name as warehouse",false);
        $this->db->from('pinjaman_admin_retur pjm');
        $this->db->join('admins adm','pjm.admin_id=adm.id','left');
        // $this->db->join('kota k','m.kota_id=k.id','left');
        // $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->join('warehouse w','pjm.warehouse_id=w.id','left');
        $this->db->where('pjm.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getReturPinjamanDetail($id=0){
        $data = array();
        $this->db->select("d.item_id,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.pv,0)as fpv,format(sum(d.qty*d.harga),0)as fsubtotal,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name",false);
        $this->db->from('pinjaman_admin_retur_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.pinjaman_admin_retur_id',$id);
        $this->db->group_by('d.id');
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	/* End Retur Pinjaman Staff */
	
	/* History Pinjaman Staff */
	public function list_saldopinjaman($stcid)
    {
        $this->db->select("
			a.item_id,i.name,format(sum(a.qty),0)as fqty,format(a.harga,0)as fharga,format(a.pv,0)as fpv,format(sum(a.harga*a.qty),0)as ftotalharga,format(sum(a.pv*a.qty),0)as ftotalpv",false)
            ->from('pinjaman_admin_saldo a');
		$this->db->join('item i','a.item_id=i.id','left');
		if($stcid)$this->db->where('a.admin_id',$stcid);
        $this->db->group_by('a.item_id');
        $this->db->group_by('a.harga');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function sum_saldopinjaman($stcid)
    {
         $this->db->select("format(sum(a.harga*a.qty),0)as ftotalharga,format(sum(a.pv*a.qty),0)as ftotalpv",false)
            ->from('pinjaman_admin_saldo a');
        if($stcid)$this->db->where('a.admin_id',$stcid);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
	
	public function list_pinjamanhistory($fromdate,$todate,$stcid)
    {
        $where = "a.admin_id = '$stcid' and (a.tgl between '$fromdate' and '$todate') ";
        
        $this->db->select("a.noref,date_format(a.tgl,'%d-%b-%Y')as tgl,createdby,format(a.awal,0)as fawal,format(a.take,0)as fpinjam,format(a.return,0)as flunas,format(a.akhir,0)as fakhir,a.remark",false)
            ->from('pinjaman_admin_his a');
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=1;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function sum_pinjamanhistory($fromdate,$todate,$stcid)
    {
        $where = "a.admin_id = '$stcid' and (a.tgl between '$fromdate' and '$todate') ";
        
         $this->db->select("format(sum(a.take),0)as ftotalpinjam,format(sum(a.return),0)as ftotallunas",false)
            ->from('pinjaman_admin_his a');
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
	
	public function list_pinjaman_stock($fromdate,$todate,$stcid)
    {
        $where = "a.admin_id = '$stcid' and (a.tgl between '$fromdate' and '$todate') ";
        
        $this->db->select("a.noref,date_format(a.tgl,'%d-%b-%Y')as ftgl,a.createdby,format(a.saldoawal,0)as fawal,
                          format(a.saldoin,0)as fin,format(a.saldoout,0)as fout,format(a.saldoakhir,0)as fakhir,
                          format(a.harga,0)as fharga,a.item_id,i.name",false)
            ->from('pinjaman_admin_stockcard a')
            ->join('item i','a.item_id=i.id','left')
            ->where($where)
            ->order_by('a.item_id','asc')
            ->order_by('a.harga','asc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs)
            {
                $i++;
                $rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
	
	/* End History Pinjaman Staff */
}?>