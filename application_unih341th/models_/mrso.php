<?php
class MRso extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Request Sales Order Member ke Company diteruskan ke STC
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @developedby www.smartindo-technology.com
    | @created 2009-06-01
    |
    */
    
    public function searchRequestSOMember($keywords=0,$num,$offset){
        $data = array();
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.member_id = '$memberid' and a.romember = 'Y' AND a.id LIKE '$keywords%'";
        }
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.romember = 'Y' and a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
            else $where = "a.romember = 'Y' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        
        $this->db->select("a.id,a.status,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,s.no_stc,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,m.nama,a.remarkapp",false);
        $this->db->from('so a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->where($where);
        $this->db->order_by('a.status','asc');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countRequestSOMember($keywords=0){
        $data = array();
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.member_id = '$memberid' and a.romember = 'Y' AND a.id LIKE '$keywords%'";
        }
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$where = "a.romember = 'Y' and a.warehouse_id = '$whsid' and (a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
            else $where = "a.romember = 'Y' and ( a.id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' OR m.nama LIKE '$keywords%' )";
        }
        $this->db->select("a.id");
        $this->db->from('so a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.stockiest_id=m.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    public function addRequestSalesOrder(){
        $member_id = $this->input->post('member_id');
        $totalharga = str_replace(".","",$this->input->post('total'));
        $totalpv = str_replace(".","",$this->input->post('totalpv'));
        
        $empid = $this->session->userdata('userid');
        $whsid = $this->session->userdata('whsid');
        $groupid = $this->session->userdata('group_id');
        $tgl = date('Y-m-d',now());
        //$ppn=ROUND($totalharga * 10 / 100);
        $data = array(
            'member_id' => $member_id,
            'stockiest_id'=>'0',
            'tgl' => $tgl,
            'totalharga' => $totalharga,
            'totalpv' => $totalpv,
            'kit'=>'N',
            'warehouse_id' => $whsid,
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid,
            'romember'=>'Y'
        );
        $this->db->insert('so',$data);
        
        $id = $this->db->insert_id();
        
        $qty0 = str_replace(".","",$this->input->post('qty0'));
        if($this->input->post('itemcode0') and $qty0 > 0){
            $data=array(
                'so_id' => $id,
                'item_id' => $this->input->post('itemcode0'),
                'qty' => $qty0,
                'harga' => str_replace(".","",$this->input->post('price0')),
                'pv' => str_replace(".","",$this->input->post('pv0')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal0')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv0'))
            );
            
            $this->db->insert('so_d',$data);
       }
       
       $qty1 = str_replace(".","",$this->input->post('qty1'));
       if($this->input->post('itemcode1') and $qty1 > 0){
            $data=array(
                'so_id' => $id,
                'item_id' => $this->input->post('itemcode1'),
                'qty' => $qty1,
                'harga' => str_replace(".","",$this->input->post('price1')),
                'pv' => str_replace(".","",$this->input->post('pv1')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal1')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv1'))                
            );
            
            $this->db->insert('so_d',$data);
       }
       
       $qty2 = str_replace(".","",$this->input->post('qty2'));
       if($this->input->post('itemcode2') and $qty2 > 0){
            $data=array(
                'so_id' => $id,
                'item_id' => $this->input->post('itemcode2'),
                'qty' => $qty2,
                'harga' => str_replace(".","",$this->input->post('price2')),
                'pv' => str_replace(".","",$this->input->post('pv2')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal2')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv2'))
            );
            
            $this->db->insert('so_d',$data);
       }
       
       $qty3 = str_replace(".","",$this->input->post('qty3'));
       if($this->input->post('itemcode3') and $qty3 > 0){
            $data=array(
                'so_id' => $id,
                'item_id' => $this->input->post('itemcode3'),
                'qty' => $qty3,
                'harga' => str_replace(".","",$this->input->post('price3')),
                'pv' => str_replace(".","",$this->input->post('pv3')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal3')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv3'))
            );
            
            $this->db->insert('so_d',$data);
        }
       
       $qty4 = str_replace(".","",$this->input->post('qty4'));
        if($this->input->post('itemcode4') and $qty4 > 0){
            $data=array(
                'so_id' => $id,
                'item_id' => $this->input->post('itemcode4'),
                'qty' => $qty4,
                'harga' => str_replace(".","",$this->input->post('price4')),
                'pv' => str_replace(".","",$this->input->post('pv4')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal4')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv4'))
            );
            
            $this->db->insert('so_d',$data);
        }
       
       $qty5 = str_replace(".","",$this->input->post('qty5'));
       if($this->input->post('itemcode5') and $qty5 > 0){
            $data=array(
                'so_id' => $id,
                'item_id' => $this->input->post('itemcode5'),
                'qty' => $qty5,
                'harga' => str_replace(".","",$this->input->post('price5')),
                'pv' => str_replace(".","",$this->input->post('pv5')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal5')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv5'))
            );
            
            $this->db->insert('so_d',$data);
       }
       
       $qty6 = str_replace(".","",$this->input->post('qty6'));
       if($this->input->post('itemcode6') and $qty6 > 0){
            $data=array(
                'so_id' => $id,
                'item_id' => $this->input->post('itemcode6'),
                'qty' => $qty6,
                'harga' => str_replace(".","",$this->input->post('price6')),
                'pv' => str_replace(".","",$this->input->post('pv6')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal6')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv6'))
            );
            
            $this->db->insert('so_d',$data);
       }
       
       $qty7 = str_replace(".","",$this->input->post('qty7'));
       if($this->input->post('itemcode7') and $qty7 > 0){
            $data=array(
                'so_id' => $id,
                'item_id' => $this->input->post('itemcode7'),
                'qty' => $qty7,
                'harga' => str_replace(".","",$this->input->post('price7')),
                'pv' => str_replace(".","",$this->input->post('pv7')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal7')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv7'))
            );
            
            $this->db->insert('so_d',$data);
       }
       
       $qty8 = str_replace(".","",$this->input->post('qty8'));
       if($this->input->post('itemcode8') and $qty8 > 0){
            $data=array(
                'so_id' => $id,
                'item_id' => $this->input->post('itemcode8'),
                'qty' => $qty8,
                'harga' => str_replace(".","",$this->input->post('price8')),
                'pv' => str_replace(".","",$this->input->post('pv8')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal8')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv8'))
            );
            
            $this->db->insert('so_d',$data);
        }
       
       $qty9 = str_replace(".","",$this->input->post('qty9'));
       if($this->input->post('itemcode9') and $qty9 > 0){
            $data=array(
                'so_id' => $id,
                'item_id' => $this->input->post('itemcode9'),
                'qty' => $qty9,
                'harga' => str_replace(".","",$this->input->post('price9')),
                'pv' => str_replace(".","",$this->input->post('pv9')),
                'jmlharga' => str_replace(".","",$this->input->post('subtotal9')),
                'jmlpv' => str_replace(".","",$this->input->post('subtotalpv9'))
            );
            
            $this->db->insert('so_d',$data);
        }
        
        $this->db->query("call sp_so_member('$id','$tgl','$member_id','0','$totalharga','$totalpv','$whsid','$groupid','$empid')");       
    }
    
    public function getRequestSOMember($id=0){
        $data = array();
        $this->db->select("a.id,a.status,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.remarkapp,a.tglapproved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    s.no_stc,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('so a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('member z','a.stockiest_id=z.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        if($this->session->userdata('group_id')>100)$this->db->where('a.member_id',$this->session->userdata('userid'));
        else{
            $whsid = $this->session->userdata('whsid');
            if($whsid > 1)$this->db->where('a.warehouse_id',$whsid); 
        }
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getRequestSOMemberDetail($id=0){
        $data = array();
        $this->db->select("d.item_id,format(d.qty,0)as fqty,format(d.harga,0)as fharga,format(d.pv,0)as fpv,format(sum(d.qty*d.harga),0)as fsubtotal,format(sum(d.qty*d.pv),0)as fsubtotalpv,a.name",false);
        $this->db->from('so_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.so_id',$id);
        $this->db->group_by('d.id');
        $q=$this->db->get();
            //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function getRSOApproved($id=0){
        $data = array();
        $this->db->select("a.id,a.status,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,a.totalharga,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.remarkapp,a.tglapproved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    s.no_stc,m.nama,z.nama as namastc,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('so a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('member z','a.stockiest_id=z.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $whsid = $this->session->userdata('whsid');
        if($whsid > 1)$this->db->where('a.warehouse_id',$whsid); 
        $this->db->where('a.id',$id);
        $this->db->where('a.status','0');
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function requestOrderApproved(){
        $empid = $this->session->userdata('user');
        $remarkapp=$this->db->escape_str($this->input->post('remark'));
        $soid = $this->input->post('soid');
        $stcid = $this->input->post('member_id');
        $totalharga = $this->input->post('total');
            
        $data = array(
            'stockiest_id'=> $stcid,
             'status'=> '1',
             'remarkapp'=>$remarkapp,
             'approvedby'=>$this->session->userdata('user'),
             'tglapproved' => date('Y-m-d', now())
         );
         $this->db->update('so',$data,array('id'=>$soid));
                
        $this->db->query("call sp_soapp_member('$soid','$stcid','$totalharga','1','$empid')");
        $this->session->set_flashdata('message','approved request SO successfully');
    }
    
    public function check_titipan_soapp($soid,$stcid)
    {
        $data=array();
        $q = $this->db->query("SELECT f_check_titipan_soapp('$soid','$stcid') as l_result");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['l_result'];
    }
    
    public function getEwallet($member_id){
        $q = $this->db->select("ewallet,format(ewallet,0)as fewallet",false)
            ->from('member')
            ->where('id',$member_id)
            ->get();
        return $var = ($q->num_rows()>0)? $q->row() : false;
    }

   	/*Updated by Boby (2009-11-18)*/
	public function cekStok($whsid, $brg, $jml){
		$stok = 0;
		$q = $this->db->select("s.qty as qty",false)
            ->from('stock s')
            ->where("item_id = '$brg'")
            ->where("warehouse_id = '$whsid'")
            ->get();
            //echo $this->db->last_query();
        	if($q->num_rows > 0){
	            $row = $q->row_array();
			if(($row['qty']) >= $jml){
				return 'Ready';    
	          	}else{
	      		return $row['qty'];
			}
		}else{
			return 'Limited';
		}
	}

}?>