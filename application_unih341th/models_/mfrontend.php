<?php
class MFrontend extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Show Product, category and detail
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-10
    |
    */
	
	// updated by Boby 20100615
    public function list_product($cat,$num,$offset)
    {
        //if($cat)$where = "a.type_id = '$cat' and a.sales = 'Yes' and a.display = 'Yes'";
		if($cat)$where = "cat.category_id = '$cat' and i.sales = 'Yes' and i.display = 'Yes'";
        else $where = "i.sales = 'Yes' and i.display = 'Yes'";
        
        $this->db->select("i.id,i.name,i.pricecust,i.headline,i.image",false)
            ->from('item i')
			->join('category_products cat', 'i.id = cat.item_id', 'left')
            ->where($where)
            ->order_by('i.type_id','asc')
            ->order_by('i.name','asc')
            ->limit($num,$offset);    
        $q = $this->db->get();
        // echo $this->db->last_query();
        $data = array();
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs){
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function count_list_product($cat)
    {
        if($cat)$where = "cat.category_id = '$cat' and i.sales = 'Yes' and i.display = 'Yes'";
        else $where = "i.sales = 'Yes' and i.display = 'Yes'";
        
        $this->db->select("i.id,i.name,i.pricecust,i.headline,i.image",false)
            ->from('item i')
			->join('category_products cat', 'i.id = cat.item_id', 'left')
            ->where($where)
            ->order_by('i.type_id','asc')
            ->order_by('i.name','asc');    
        $q = $this->db->get();
        // echo $this->db->last_query();
		
        $data = array();
        if($q->num_rows()>0){
            $data[] = $q->row_array();
        }
		$jml = $q->num_rows();
        $q->free_result();
        return $jml;
    }
   
    public function list_category()
    {
        $q = $this->db->query("
			SELECT id, category_name
			FROM category_group
		");
        $data = array();
		
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs){
                $data[] = $rs;
            }
        }
		
        $q->free_result();
        return $data;
    }
	
	// end updated by Boby 20100615
	
	public function get_product($id=0){
        $data = array();
        $this->db->select("a.id,a.name,a.pricecust,a.description",false);
        $this->db->from('item a');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Show News Event dan Promo dengan detailnya
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-17
    |
    */
    public function list_news($promo,$box,$num,$offset)
    {
        $where = "a.promo = '$promo' and a.status = 'active' ";
        if($box) $where .= "and a.newsbox = '$box'";
        $this->db->select("a.id,a.file,a.title,a.createdby,date_format(a.created,'%M %d, %Y')as ftgl,
					concat(substr(a.shortdesc,1,75),'...')as shortdesc
					,format(a.countview,0)as fcountview",false)
            ->from('news a')
            ->where($where)
            ->order_by('a.id','desc')
            ->limit($num,$offset);    
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
	$i = 0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs){
		$i++;
		$rs['i'] = $i;
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function count_list_news($promo){
        $where = "a.promo = '$promo' and a.status = 'active' ";
        $this->db->select("a.id",false)->from('news a')->where($where);
        return $this->db->count_all_results();
    }
    public function list_get_news($promo,$tgl)
    {
        //$where = "a.promo = '$promo' and a.status = 'active' and date_format(a.created,'%Y-%m') = '$tgl'";
        $where = "a.status = 'active' and date_format(a.created,'%Y-%m') = '$tgl'";
        
        $this->db->select("a.id,a.title,a.createdby,date_format(a.created,'%W, %d %M %Y')as ftgl,a.shortdesc,format(a.countview,0)as fcountview",false)
            ->from('news a')
            ->where($where)
            ->order_by('a.id','desc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs){
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function get_news($id)
    {
        $where = "a.status = 'active' and a.id = '$id'";
        
        $this->db->select("a.id,a.title,a.promo,a.createdby,date_format(a.created, '%M %d, %Y')as ftgl,a.longdesc,format(a.countview,0)as fcountview",false)
            ->from('news a')
            ->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function list_archive($promo)
    {
        $where = "a.status = 'active'";
        
        $this->db->select("date_format(a.created,'%Y-%m')as tgl,date_format(a.created,'%M %Y')as ftgl",false)
            ->from('news a')
            ->where($where)
            ->group_by("date_format(a.created,'%M %Y')")
            ->order_by('a.created','asc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs){
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function update_countview($id){
        $this->db->query("UPDATE news SET countview = countview + 1 WHERE `id` = '$id' ");
        //echo $this->db->last_query();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Show Testimonial dengan detailnya
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-18
    |
    */
    public function list_testimonial($num,$offset)
    {
        $this->db->select("a.id,a.title,a.thumbnail,a.shortdesc",false)
            ->from('testi a')
            ->where('a.status','active')
            ->order_by('a.id','desc')
            ->limit($num,$offset);    
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs){
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function count_list_testimonial(){
        $this->db->select("a.id",false);
        $this->db->from('testi a');
        $this->db->where('a.status','active');
        return $this->db->count_all_results();
    }
    public function get_testimonial($id)
    {
        $where = "a.status = 'active' and a.id = '$id'";
        
        $this->db->select("a.id,a.title,a.longdesc,a.thumbnail",false)
            ->from('testi a')
            ->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    /*
    |--------------------------------------------------------------------------
    | Show Download File
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-18
    |
    */
    public function list_download($type='',$num,$offset)
    {
        $this->db->select("a.id,a.description,a.file_alias,a.type",false)
            ->from('download a')
            ->where('a.status','active');
        if($type)$this->db->where('a.type',$type);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);    
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs){
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function count_list_download($type=''){
        $this->db->select("a.id",false);
        $this->db->from('download a');
        $this->db->where('a.status','active');
        if($type)$this->db->where('a.type',$type);
        return $this->db->count_all_results();
    }
    public function get_download($id)
    {
        $this->db->select("a.id,a.description,a.file,a.file_alias,a.file_ext,a.type",false)
            ->from('download a')
            ->where('a.status','active')
            ->where('a.id',$id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function archive_download(){
        $this->db->select("a.type",false)
            ->from('download a')
            ->where('a.status','active')
            ->group_by('a.type');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs){
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
   /*
    |--------------------------------------------------------------------------
    | Show Marketing Plan
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-18
    |
    */
    public function list_plan($num,$offset)
    {
        $this->db->select("a.id,a.longdesc",false)
            ->from('marketing_plan a')
            ->where('a.status','active');
        $this->db->order_by('a.sort','asc');
        $this->db->limit($num,$offset);    
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs){
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function count_list_plan(){
        $this->db->select("a.id",false);
        $this->db->from('marketing_plan a');
        $this->db->where('a.status','active');
        return $this->db->count_all_results();
    }
    public function archive_plan(){
        $this->db->select("a.id - 1 as id,a.category",false)
            ->from('marketing_plan a')
            ->where('a.status','active')
            ->order_by('sort','asc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs){
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function get_plan($num)
    {
        $this->db->select("a.id,a.longdesc",false)
            ->from('marketing_plan a')
            ->where('a.status','active')
            ->where('a.id',$num)
            ->order_by('sort','asc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs){
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function list_gallery($num,$offset)
    {
        $this->db->select("a.id,a.title,a.album,a.description,a.file",false)
            ->from('gallery a')
            ->where('a.status','active')
            ->where('a.coveralbum','1')
            ->order_by('a.nourut','asc')
            ->limit($num,$offset);    
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        //$i=0;
        if($q->num_rows()>0){
            
            foreach ($q->result_array() as $rs){
                //$i++;
                //$rs['i'] = $i; 
                //$rs['details'] = $this->list_gallery_album($rs['album']);
                $rs['total'] = $q->num_rows();
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
   public function count_list_gallery(){
        
        $this->db->select("a.id",false)->from('gallery a')->where('a.status','active')->where('a.coveralbum','1');
        return $this->db->count_all_results();
    }
    public function count_gallery_album($album){
        
        $this->db->select("a.id",false)->from('gallery a')->where('a.status','active')->where('a.album',$album);
        return $this->db->count_all_results();
    }
   public function list_gallery_album($album,$num,$offset){
    $this->db->select("a.id,a.title,a.album,a.description,a.file",false)
            ->from('gallery a')
            ->where('a.status','active')
            ->where('a.album',$album)
            ->order_by('a.nourut','asc')
            ->limit($num,$offset);    
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        $i=0;
        if($q->num_rows()>0){
            foreach ($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
   } 
   public function list_stockiest($num,$offset){
        $this->db->select("a.id,m.nama,a.no_stc,a.alamat,a.telp,a.hp",false)
            ->from('stockiest a')
            ->join('member m','a.id=m.id','left')
            ->where('a.status','active')
			->where("m.id <> '00015704'")
			->where("m.id <> '00001335'")
            ->limit($num,$offset)
            ->order_by('a.no_stc','asc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        $data = array();
        if($q->num_rows()>0){
            foreach ($q->result_array() as $rs){
                $data[] = $rs;
            }
        }
        $q->free_result();
        return $data;
    }
    public function count_list_stockiest(){
        $this->db->select("a.id",false);
        $this->db->from('stockiest a');
        $this->db->where('a.status','active');
        return $this->db->count_all_results();
    } 
    
}?>