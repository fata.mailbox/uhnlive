<?php
class MKota extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Master Propinsi
    |--------------------------------------------------------------------------
    |
    | @created 2009-04-05
    | @author qtakwa@yahoo.com@yahoo.com
    |
    */
    public function searchPropinsi($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.name,date_format(a.created,'%d-%b-%Y')as created,a.createdby",false);
        $this->db->from('propinsi a');
        $this->db->like('a.name',$keywords,'after');
        $this->db->order_by('a.name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countPropinsi($keywords=0){
        $this->db->like('a.name',$keywords,'after');
        $this->db->from('propinsi a');
        return $this->db->count_all_results();
    }
    public function addPropinsi(){
        $data=array(
            'name' => $this->input->post('name'),
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $this->session->userdata('user')
        );
            
        $this->db->insert('propinsi',$data);
    }
    public function check_propinsi($id){
        $q=$this->db->select("id",false)
            ->from('propinsi')
            ->where('name',$id)
            ->get();
        return ($q->num_rows() > 0)? true : false;    
    }
    public function getPropinsi($id){
        $data=array();
        $q = $this->db->get_where('propinsi',array('id'=>$id));
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function editPropinsi(){
        $data=array(
            'name' => $this->input->post('name'),
            'updated' => date('Y-m-d H:i:s',now()),
            'updatedby' => $this->session->userdata('user')
        );
        $this->db->update('propinsi',$data,array('id'=>$this->input->post('id')));
    }
    
    /*
    |--------------------------------------------------------------------------
    | Master Kota / Kabupaten
    |--------------------------------------------------------------------------
    |
    | @created 2009-04-05
    | @author qtakwa@yahoo.com@yahoo.com
    |
    */
    public function searchKota($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.name,b.name as warehouse,p.name as propinsi,format(a.mqo,0)as fmqo, format(a.ongkoskirim,0)as fongkoskirim,date_format(a.created,'%d-%b-%Y')as created,a.createdby",false);
        $this->db->from('kota a');
        $this->db->join('propinsi p','a.propinsi_id=p.id','left');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
        $this->db->like('a.name',$keywords,'after');
        $this->db->or_like('p.name',$keywords,'after');
        $this->db->order_by('p.name','asc');
        $this->db->order_by('a.name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countKota($keywords=0){
        $this->db->from('kota a');
        $this->db->join('propinsi p','a.propinsi_id=p.id','left');
        $this->db->like('a.name',$keywords,'after');
        $this->db->or_like('p.name',$keywords,'after');
        
        return $this->db->count_all_results();
    }
    public function addKota(){
        $mqo = str_replace(".","",$this->input->post('mqo'));
        $ongkoskirim = str_replace(".","",$this->input->post('ongkoskirim'));
        $data=array(
            'name' => $this->input->post('name'),
            'propinsi_id' => $this->input->post('propinsi_id'),
            'warehouse_id' => $this->input->post('warehouse_id'),
            'mqo' => $mqo,
            'ongkoskirim' => $ongkoskirim,
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $this->session->userdata('user')
        );
            
        $this->db->insert('kota',$data);
    }
    public function check_kota($id){
        $q=$this->db->select("id",false)
            ->from('kota')
            ->where('name',$id)
            ->get();
        return ($q->num_rows() > 0)? true : false;    
    }
    public function getKota($id){
        $data=array();
        $this->db->select("a.id,a.name,b.name as warehouse,p.name as propinsi,a.mqo,format(a.mqo,0)as fmqo,a.ongkoskirim,format(a.ongkoskirim,0)as fongkoskirim,a.propinsi_id,a.warehouse_id,date_format(a.created,'%d-%b-%Y')as created,a.createdby,a.updated,a.updatedby",false);
        $this->db->from('kota a');
        $this->db->join('propinsi p','a.propinsi_id=p.id','left');
        $this->db->join('warehouse b','a.warehouse_id=b.id','left');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function editKota(){
        $mqo = str_replace(".","",$this->input->post('mqo'));
        $ongkoskirim = str_replace(".","",$this->input->post('ongkoskirim'));
        $data=array(
            'name' => $this->input->post('name'),
            'propinsi_id' => $this->input->post('propinsi_id'),
            'warehouse_id' => $this->input->post('warehouse_id'),
            'mqo' => $mqo,
            'ongkoskirim' => $ongkoskirim,
            'updated' => date('Y-m-d H:i:s',now()),
            'updatedby' => $this->session->userdata('user')
        );
        $this->db->update('kota',$data,array('id'=>$this->input->post('id')));
    }
    public function getDropDownWhs(){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    public function getDropDownPropinsi(){
        $data = array();
        $this->db->order_by('name','asc');
        $q = $this->db->get('propinsi');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    } 
    
    
}?>