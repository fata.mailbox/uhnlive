<?php
class MStockiest extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Master Stockiest
    |--------------------------------------------------------------------------
    |
    | @created 2009-03-29
    | @author qtakwa@yahoo.com@yahoo.com
    | @developmby www.smartindo-technology.com
    |
    */
    public function searchStockiest($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,m.nama,a.no_stc,t.name as typename,a.status",false);
        $this->db->from('stockiest a');
        $this->db->join('member m','a.id=m.id','left');
        $this->db->join('type_stockiest t','a.type=t.id','left');
        $this->db->like('a.no_stc',$keywords,'between');
        $this->db->or_like('m.nama',$keywords,'between');
        $this->db->order_by('a.no_stc','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countStockiest($keywords=0){
        $this->db->from('stockiest a');
        $this->db->join('member m','a.id=m.id','left');
        $this->db->like('a.no_stc',$keywords,'between');
        $this->db->or_like('m.nama',$keywords,'between');
        return $this->db->count_all_results();
    }
    public function addStockiest(){
        $hash = sha1(microtime());
        if($this->input->post('type') == '1')$group='102';
        else $group='103';
		// updated by Boby 20100614
		// updated by Boby 20100616
        $data=array(
            'id' => $this->input->post('member_id'),
            'group_id' => $group,
            'warehouse_id' => $this->input->post('warehouse_id'),
            'no_stc' => strtoupper($this->input->post('no_stc')),
			'kota_id' => $this->input->post('kota_id'),
			'leader_id' => $this->input->post('member_id2'),
			
			'alamat' => $this->input->post('alamat'),
			'telp' => $this->input->post('telp'),
			'hp' => $this->input->post('hp'),
			'email' => $this->input->post('email'),
			
            'password' => substr(sha1(sha1(md5($hash.$this->input->post('passwd')))),5,15),
            'pin' => substr(sha1(sha1(md5($hash.$this->input->post('pin')))),3,7),
            'hash' => $hash,
            'type' => $this->input->post('type'),
            'status' => $this->input->post('status'),
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $this->session->userdata('user')
        );
            
        $this->db->insert('stockiest',$data);
    }
    
    public function check_memberid($id){
        $q=$this->db->select("id",false)
            ->from('stockiest')
            ->where('id',$id)
            ->get();
        return ($q->num_rows() > 0)? true : false;    
    }   
    public function getStockiest($id){
        $data=array();
		// updated by Boby 20100614
        //$this->db->select("a.id,m.nama,a.no_stc,a.warehouse_id,w.name as warehouse,a.type,t.name as typename,a.status,date_format(a.created,'%d-%b-%Y')as created,a.createdby,a.updated,a.updatedby",false);
		
		// updated by Boby 20100614
		// $this->db->select("a.leader_id, lm.nama as nama_leader, l.no_stc as no_leader, a.kota_id, k.`name` as nama_kota, a.id,m.nama,a.no_stc,a.warehouse_id,w.name as warehouse,a.type,t.name as typename,a.status,date_format(a.created,'%d-%b-%Y')as created,a.createdby,a.updated,a.updatedby",false);
		$this->db->select("a.leader_id
			, a.alamat, a.telp, a.hp, a.email
			, lm.nama as nama_leader, l.no_stc as no_leader, a.kota_id, k.`name` as nama_kota, a.id,m.nama,a.no_stc,a.warehouse_id,w.name as warehouse,a.type,t.name as typename,a.status,date_format(a.created,'%d-%b-%Y')as created,a.createdby,a.updated,a.updatedby",false);
		// updated by Boby 20100614
		
        // end updated by Boby 20100614
		
		$this->db->from('stockiest a');
        $this->db->join('member m','a.id=m.id','left');
		
		// updated by Boby 20100614
		$this->db->join('member lm','a.leader_id=lm.id','left');
		$this->db->join('stockiest l','a.leader_id=l.id','left');
		$this->db->join('kota k','a.kota_id=k.id','left');
		// end updated by Boby 20100614
		
        $this->db->join('type_stockiest t','a.type=t.id','left');
        $this->db->join('warehouse w','a.warehouse_id=w.id','left');
        $this->db->where('a.id',$id);
        $q = $this->db->get();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function editStockiest(){
        $hash = $this->_getHash($this->input->post('id'));
        if($this->input->post('type') == '1')$group='102';
        else $group='103';
        if($this->input->post('passwd') && $this->input->post('pin')){
            $data=array(
                'no_stc' => strtoupper($this->input->post('no_stc')),
                'warehouse_id' => $this->input->post('warehouse_id'),
                'group_id' => $group,
                'kota_id' => $this->input->post('kota_id'),
				'leader_id' => $this->input->post('member_id2'),
				
				'alamat' => $this->input->post('alamat'),
				'telp' => $this->input->post('telp'),
				'hp' => $this->input->post('hp'),
				'email' => $this->input->post('email'),
				
				'password' => substr(sha1(sha1(md5($hash->hash.$this->input->post('passwd')))),5,15),
                'pin' => substr(sha1(sha1(md5($hash->hash.$this->input->post('pin')))),3,7),
                'type' => $this->input->post('type'),
                'status' => $this->input->post('status'),
                'updated' => date('Y-m-d H:i:s',now()),
                'updatedby' => $this->session->userdata('user')
            );
        }else{
            if($this->input->post('passwd')){
                $data=array(
                    'no_stc' => strtoupper($this->input->post('no_stc')),
                    'warehouse_id' => $this->input->post('warehouse_id'),
                    'group_id' => $group,
                    'kota_id' => $this->input->post('kota_id'),
					'leader_id' => $this->input->post('member_id2'),
					
					'alamat' => $this->input->post('alamat'),
					'telp' => $this->input->post('telp'),
					'hp' => $this->input->post('hp'),
					'email' => $this->input->post('email'),
					
					'password' => substr(sha1(sha1(md5($hash->hash.$this->input->post('passwd')))),5,15),
                    'type' => $this->input->post('type'),
                    'status' => $this->input->post('status'),
                    'updated' => date('Y-m-d H:i:s',now()),
                    'updatedby' => $this->session->userdata('user')
                );
            }elseif($this->input->post('pin')){
                $data=array(
                    'no_stc' => strtoupper($this->input->post('no_stc')),
                    'warehouse_id' => $this->input->post('warehouse_id'),
                    'group_id' => $group,
                    'kota_id' => $this->input->post('kota_id'),
					'leader_id' => $this->input->post('member_id2'),
					
					'alamat' => $this->input->post('alamat'),
					'telp' => $this->input->post('telp'),
					'hp' => $this->input->post('hp'),
					'email' => $this->input->post('email'),
				
					'pin' => substr(sha1(sha1(md5($hash->hash.$this->input->post('pin')))),3,7),
                    'type' => $this->input->post('type'),
                    'status' => $this->input->post('status'),
                    'updated' => date('Y-m-d H:i:s',now()),
                    'updatedby' => $this->session->userdata('user')
                );
            }else{
                $data=array(
                    'no_stc' => strtoupper($this->input->post('no_stc')),
                    'warehouse_id' => $this->input->post('warehouse_id'),
                    'kota_id' => $this->input->post('kota_id'),
					'leader_id' => $this->input->post('member_id2'),
					
					'alamat' => $this->input->post('alamat'),
					'telp' => $this->input->post('telp'),
					'hp' => $this->input->post('hp'),
					'email' => $this->input->post('email'),
				
					'group_id' => $group,
                    'type' => $this->input->post('type'),
                    'status' => $this->input->post('status'),
                    'updated' => date('Y-m-d H:i:s',now()),
                    'updatedby' => $this->session->userdata('user')
                );
            }
        }
        $this->db->update('stockiest',$data,array('id'=>$this->input->post('id')));
    }
    private function _getHash($id){
        $q=$this->db->select("hash",false)
            ->from('stockiest')
            ->where('id',$id)
            ->get();
        return ($q->num_rows() > 0)? $q->row() : false;    
    }
    public function getDropDownType(){
        $data = array();
        $this->db->order_by('id','asc');
        $q = $this->db->get('type_stockiest');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    public function getDropDownWhs(){
        $data = array();
        $q = $this->db->get('warehouse');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
    public function getCheckStockiest($nostc){
        $q=$this->db->select("id",false)->from('stockiest')->where('no_stc',$nostc)->get();
        //echo $this->db->last_query();
        return $var = ($q->num_rows()>0)? $q->row() : false;
    }
	
	// created by Boby 20100630
	public function saveLogStc(){
		if($this->input->post('no_stc') != $this->input->post('no_stc1') || $this->input->post('type1') != $this->input->post('type')){
			$data=array(
				'member_id' => $this->input->post('id'),
				'stc_old' => strtoupper($this->input->post('no_stc1')),
				'stc_new' => strtoupper($this->input->post('no_stc')),
				'old_type' => $this->input->post('type1'),
				'new_type' => $this->input->post('type'),
				'createddate' => date('Y-m-d H:i:s',now()),
				'createdby' => $this->session->userdata('user')
			);				
			$this->db->insert('update_stc',$data);
		}
    }
	// end created by Boby 20100630
    
}?>