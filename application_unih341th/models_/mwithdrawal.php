<?php
class MWithdrawal extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Withdrawal ewallet for Stockiest & member
    |--------------------------------------------------------------------------
    |
    | @created 2009-05-02
    | @author qtakwa@yahoo.com@yahoo.com
    | @copyright www.smartindo-techmology.com
    */
    public function searchWithdrawal($keywords=0,$num,$offset){
		$data = array();
        $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.refund,a.flag,a.member_id,m.nama,s.no_stc,format(a.amount,0)as famount,a.status,a.remark,a.remarkapp,a.approved",false);
        $this->db->from('withdrawal a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        if($this->session->userdata('group_id')>100){
            if($this->session->userdata('group_id') == 101)$flag = 'mem';
            else $flag = 'stc'; 
            $where = "a.flag = '$flag' and `a`.`member_id` = '".$this->session->userdata('userid')."' AND (s.no_stc LIKE '$keywords%' OR a.id LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
        }else{
            $where = "(s.no_stc LIKE '$keywords%' OR a.id LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
        }
        $this->db->where($where);
        $this->db->order_by('a.status','asc');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countWithdrawal($keywords=0){
        $this->db->from('withdrawal a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        if($this->session->userdata('group_id')>100){
            if($this->session->userdata('group_id') == 101)$flag = 'mem';
            else $flag = 'stc'; 
            $where = "a.flag = '$flag' and `a`.`member_id` = '".$this->session->userdata('userid')."' AND (s.no_stc LIKE '$keywords%' OR a.id LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
        }else{
            $where = "(s.no_stc LIKE '$keywords%' OR a.id LIKE '$keywords%' OR m.nama LIKE '$keywords%')";
        }
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    public function addWithdrawal(){
        $amount = str_replace(".","",$this->input->post('amount'));
        if($this->session->userdata('group_id') == 101)$flag = 'mem';
        else $flag = 'stc';
        $empid = $this->session->userdata('userid');
        $data=array(
            'member_id' => $this->session->userdata('userid'),
            'tgl' => date('Y-m-d',now()),
            'amount' => $amount,
            'biayaadm' => 5000,
            'type_wdr_id' => '1',
            'account_id' => $this->input->post('account_id'),
            'remark' => $this->db->escape_str($this->input->post('remark')),
            'flag' => $flag,
            'event_id' => 'WD1',
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $empid
        );
        $this->db->insert('withdrawal',$data);
        
        $id = $this->db->insert_id();
        $this->db->query("call sp_withdrawal('$id','$empid','company','$amount','$flag','Withdrawal','$empid')");
    }
    public function getWithdrawal($id){
        $data=array();
        $this->db->select("a.id,a.account_id,a.flag,date_format(a.tgl,'%d-%b-%Y')as tgl,a.status,a.member_id,m.nama,s.no_stc,a.amount,format(a.amount,0)as famount,a.remark,a.remarkapp,
                          a.approved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby,c.bank_id,c.name,c.no,c.area",false);
        $this->db->from('withdrawal a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('account c','a.account_id=c.id','left');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        if($this->session->userdata('group_id')>100){
            if($this->session->userdata('group_id') == 101)$flag = 'mem';
            else $flag = 'stc';
            $where = "`a`.`id` = '$id' and `a`.`flag` = '$flag' and `a`.`member_id` = '".$this->session->userdata('userid')."'";
        }else{
            $where = "`a`.`id` = '$id'";
        }
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function refund(){
        if($this->input->post('counter')){
	    $empid = $this->session->userdata('userid');		    
	    $key=0;
	    while($key < count($_POST['counter'])){
		$p_id = $this->input->post('p_id'.$key);
		if($this->input->post('p_id'.$key) > 0){
		    $row = $this->_getRefund($p_id);
		    if($row->id > 0){
			$amount = $row->amount - $row->biayaadm;
			$remark = 'wdr refund: '.$p_id.'. '.$this->db->escape_str($this->input->post('remark'));
			$data=array(
			    'member_id' => $row->member_id,
			    'tgl' => date('Y-m-d',now()),
			    'amount' => -1*$amount,
			    'biayaadm' => 0,
			    'type_wdr_id' => $row->type_wdr_id,
			    'account_id' => $row->account_id,
			    'remarkapp' => $remark,
			    'flag' => $row->flag,
			    'status'=>'approved',
			    'approved' => date('Y-m-d H:i:s',now()),
			    'approvedby' => $empid,
			    'refund' => 'Y',
			    'event_id' => 'WD8',
			    'created' => date('Y-m-d H:i:s',now()),
			    'createdby' => $empid
			);
			$this->db->insert('withdrawal',$data);
			$id = $this->db->insert_id();
			$this->db->update('withdrawal',array('refund'=>'Y'),array('id'=>$p_id));
			
			$member_id = $row->member_id;
			$flag = $row->flag;
			$this->db->query("call sp_refund_wdr('$id','$member_id','$amount','$flag','$remark','$empid')");
		    }
		}
		$key++;
	    }
	    $this->session->set_flashdata('message','Refund withdrawal successfully');
	    
        }else{
            $this->session->set_flashdata('message','Nothing to refund withdrawal!');
        }
    }
    protected function _getRefund($id = 0){
        $data=array();
        $this->db->select("id,member_id,amount,type_wdr_id,biayaadm,account_id,flag,warehouse_id",false);
        $this->db->where('id',$id);
	$this->db->limit(1);
        $q = $this->db->get('withdrawal');
        if($q->num_rows() > 0){
	    $data = $q->row();
        }
        $q->free_result();
        return $data;
    }
    public function getEwallet($memberid){
        $data=array();
        if($this->session->userdata('group_id') == 101){
            $q=$this->db->select("m.account_id,m.ewallet,format(m.ewallet,0)as fewallet",false)
                ->from('member m')
                ->where('m.id',$memberid)
                ->get();
        }else{
            $q=$this->db->select("m.account_id,s.ewallet,format(s.ewallet,0)as fewallet",false)
                ->from('stockiest s')
                ->join('member m','s.id=m.id','left')
                ->where('s.id',$memberid)
                ->get();
        }
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getAccountBank($id){
        $data = array();
        $q = $this->db->select("b.id,b.bank_id,b.no,b.name,b.area",false)
            ->from('account b')
            ->where('b.id',$id)
            ->get();
        if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    public function getWithdrawalApp($id){
        $data=array();
        $this->db->select("a.id,a.account_id,a.flag,date_format(a.tgl,'%d-%b-%Y')as tgl,a.status,a.member_id,m.nama,s.no_stc,a.amount,format(a.amount,0)as famount,a.remark,a.remarkapp,
                          a.approved,a.approvedby,date_format(a.created,'%d-%b-%Y')as created,a.createdby,c.bank_id,c.name,c.no,c.area",false);
        $this->db->from('withdrawal a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('account c','a.account_id=c.id','left');
        $this->db->join('stockiest s','a.member_id=s.id','left');
        if($this->session->userdata('group_id')>100){
            if($this->session->userdata('group_id') == 101)$flag = 'mem';
            else $flag = 'stc';
            $where = "a.status = 'pending' and `a`.`id` = '$id' and `a`.`flag` = '$flag' and `a`.`member_id` = '".$this->session->userdata('userid')."'";
        }else{
            $where = "a.status = 'pending' and `a`.`id` = '$id'";
        }
        $this->db->where($where);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function approvedWithdrawal(){
        $id = $this->input->post('id');
        $empid =$this->session->userdata('user');
        $member_id =$this->input->post('member_id');
        $flag =$this->input->post('flag');
        $status =$this->input->post('status');
        $amount = str_replace(".","",$this->input->post('amount'));
        if($status == 'approvedbca'){
            $event_id='wd1';
            $biayaadm = 5000;
			$status = 'approved';
        }elseif($status == 'approvedman'){
            $event_id='wd2';
            $biayaadm = 5000;
			$status = 'approved';
        }else{
            $event_id='';
            $biayaadm=0;
        }
        
        $data=array(
            'status'=>$status,
            'remarkapp' => $this->db->escape_str($this->input->post('remark')),
            'event_id' => $event_id,
            'biayaadm'=>$biayaadm,
            'approved' => date('Y-m-d H:i:s',now()),
            'approvedby' => $empid
        );
            
        $this->db->update('withdrawal',$data,array('id'=>$id));
        
        if($status == 'reject')$this->db->query("call sp_withdrawal('$id','company','$member_id','$amount','$flag','Withdrawal Reject','$empid')");
    }
    public function addWithdrawalApproved(){
        $member_id=$this->input->post('member_id');
        $amount = str_replace(".","",$this->input->post('amount'));
        
        if($this->input->post('type_withdrawal_id') == 'wd1' or $this->input->post('type_withdrawal_id') == 'wd2')$biayaadm=5000;
        else $biayaadm=0;
        $flag = $this->input->post('flag');
        $empid=$this->session->userdata('user');
        $row = $this->getAccountID($member_id);
        $data=array(
            'member_id' => $member_id,
            'tgl' => date('Y-m-d',now()),
            'amount' => $amount,
            'biayaadm' => $biayaadm,
            'event_id' => $this->input->post('type_withdrawal_id'),
            'flag' => $flag,
            '`status`' => 'approved',
            'account_id' => $row->account_id,
            'remarkapp' => $this->db->escape_str($this->input->post('remark')),
            'approved' => date('Y-m-d',now()),
            'approvedby' => $empid,
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $empid
        );
        $this->db->insert('withdrawal',$data);
        $id = $this->db->insert_id();
        $this->db->query("call sp_withdrawal('$id','$member_id','company','$amount','$flag','Withdrawal','$empid')");
    }
    public function getStockiest(){
        $q=$this->db->select("id",false)
            ->from('stockiest')
            ->where('id',$this->input->post('member_id'))
            ->where('status','active')
            ->get();
        return $var = ($q->num_rows()>0) ? false : true;
    }
    public function getEwalletApp($memberid,$status){
        $data=array();
        if($status == 'mem'){
            $q=$this->db->select("m.account_id,m.ewallet,format(m.ewallet,0)as fewallet",false)
                ->from('member m')
                ->where('m.id',$memberid)
                ->get();
        }else{
            $q=$this->db->select("m.account_id,s.ewallet,format(s.ewallet,0)as fewallet",false)
                ->from('stockiest s')
                ->join('member m','s.id=m.id','left')
                ->where('s.id',$memberid)
                ->get();
        }
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function getAccountID($memberid){
        $q=$this->db->select("id,account_id",false)
            ->from('member')
            ->where('id',$memberid)
            ->where('account_id >',0)
            ->get();
        return $var = ($q->num_rows()>0) ? $q->row() : false;
    }
	
    public function autoWithdrawal(){
        // $empid=$this->session->userdata('user');
        // $this->db->query("call sp_auto_withdrawal('50000','mem','Auto Withdrawal','$empid')");
    }
	
	/* Created by Boby 20100708*/
	public function listAutoWithdrawal(){
		$data = array();
		$show = "
			, CASE WHEN dtbr.nama <> dtbr.namaNasabah THEN 'Cek' ELSE '' END AS note
			, dtbr.ewallet - IFNULL(bns.bns,0) AS b4, IFNULL(bns.bns,0)AS bns
		";
		$join = "
			LEFT JOIN(
				SELECT member_id, SUM(nominal) AS bns
				FROM bonus
				WHERE periode = LAST_DAY(CURDATE() - INTERVAL 1 MONTH)
				GROUP BY member_id
			)AS bns ON dtbr.memberId = bns.member_id
		";
		$qry = $this->db->query("
		SELECT dtbr.memberId as memberId, dtbr.account_id
			".$show.", dtbr.ewallet AS ewallet, dtbr.nama as nama, dtbr.namaNasabah
			, dtbr.bank_id as bank_id, dtbr.cabang as cabang
			, IFNULL(REPLACE(REPLACE(REPLACE(dtbr.no,'-',''),'.',''),' ',''),'-') as no
			-- , dtbr.no as no
			, dtbr.urut
		FROM(
			SELECT dt.*, CASE 
					WHEN 
						`no` LIKE '%000000%' 
						OR namaNasabah LIKE '%000000%' 
						OR `no` IS NULL 
						OR (bank_id = 'BCA' AND LENGTH(REPLACE(REPLACE(REPLACE(`no`,'.',''),' ',''),'-','')) <> 10 )
						OR bank_id = '-' 
					THEN '2'
					WHEN bank_id = 'BCA' AND `no` NOT LIKE '%000000%' THEN '0' 
					ELSE '1' END AS urut
			FROM(
				SELECT 
					m.id AS memberId, m.ewallet, m.nama, m.account_id
					, ifnull(acc.bank_id,'-')as bank_id, ifnull(acc.area,'-') AS cabang
					, ifnull(acc.no,'_')as no, ifnull(acc.`name`,'-') AS namaNasabah
				FROM member m
				LEFT JOIN account acc ON m.account_id = acc.id
				WHERE m.ewallet >= 50000 AND m.id <> '00000001'
				GROUP BY m.id
				ORDER BY acc.bank_id DESC, m.ewallet DESC
			)AS dt
			GROUP BY dt.memberId
			ORDER BY dt.bank_id DESC, dt.ewallet DESC
		)AS dtbr
		".$join."
		WHERE ewallet-5000 > 50000
		AND urut < 3
		ORDER BY urut, bank_id, ewallet DESC
		");
		//echo $this->db->last_query();
		if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
               $data[]=$row;	
			}
		}
		$qry->free_result();
		return $data;
	}
	
	public function addAutoWdw(){
		if(count($this->input->post('p_id'))>1){
			$member_id = array_values($this->input->post('p_id'));
			for($i=0;$i<count($this->input->post('p_id'));$i++){
				$ewallet = $this->_getEwallet($member_id[$i]);
				$acc = $this->_getAccount($member_id[$i]);
				//echo $member_id[$i]." ".$ewallet." ".$acc." $i<br>";
				
				$empid=$this->session->userdata('user');
				$data=array(
					'member_id' => $member_id[$i],
					'tgl' => date('Y-m-d',now()),
					'amount' => $ewallet,
					'biayaadm' => 5000,
					'event_id' => 'WD1',
					'flag' => 'mem',
					'`status`' => 'approved',
					'account_id' => $acc,
					'remarkapp' => $this->db->escape_str($this->input->post('remark')),
					'approved' => date('Y-m-d',now()),
					'approvedby' => $empid,
					'created' => date('Y-m-d H:i:s',now()),
					'createdby' => $empid
				);
				$this->db->insert('withdrawal',$data);
				$id = $this->db->insert_id();
				$this->db->query("call sp_withdrawal('$id','$member_id[$i]','company','$ewallet','mem','Withdrawal','$empid')");
				
			}
		}
    }
	
	private function _getEwallet($member_id){
		$this->db->select("ewallet",false);
        $this->db->where("id = $member_id");
        $q = $this->db->get('member');
		
		/*
		$this->db2 = $this->load->database('db2', TRUE);
		$this->db2->select("ewallet",false);
        $this->db2->where("id = $member_id");
        $q = $this->db2->get('member');
		*/
		
        if($q->num_rows() > 0){
			$row = $q->row_array();
        }
        $q->free_result();
        return $row["ewallet"];
    }
	
	private function _getAccount($member_id){
        $this->db->select("account_id",false);
        $this->db->where("id = $member_id");
        $q = $this->db->get('member');
        if($q->num_rows() > 0){
			$row = $q->row_array();
        }
        $q->free_result();
        return $row["account_id"];
    }
    /* End created by Boby 20100708*/
	
	/* Created by Boby 20110131 */
	public function listAutoWithdrawalExbon($id){
		$data = array();
		$id += 113;
		if($id==113){$str = "";}else{$str = " AND b.title = ('$id') ";}
		$skrg = date('Y-m-d',now());
		$query1 = "
			SELECT b.id, b.periode, b.member_id, m.nama
				, acc.bank_id, acc.`name` AS namaNasabah, acc.area AS cabang, acc.`no` AS noRek
				, bs.nama as namaBns, b.nominal, (b.nominal*0.03) AS pph21, (b.nominal - (b.nominal*0.03)) AS transfer
				, (b.nominal - (b.nominal*0.03)) as ewallet, b.member_id as memberId, acc.`no` AS no, m.account_id, (b.nominal*0.03) as bns, (b.nominal) as b4
				, CASE WHEN 
					acc.`no` LIKE '%000000%' 
					OR acc.`name` LIKE '%000000%' 
					OR acc.`no` IS NULL 
					OR (acc.bank_id = 'BCA' AND LENGTH(REPLACE(REPLACE(acc.`no`,'.',''),' ','')) <> 10 )
					OR bank_id = '-' 
				THEN '2'
				WHEN bank_id = 'BCA' AND `no` NOT LIKE '%000000%' THEN '0' 
				ELSE '1' END AS urut
			FROM bonus b
			LEFT JOIN member m ON b.member_id = m.id
			LEFT JOIN account acc ON m.account_id = acc.id
			LEFT JOIN bonus_st bs ON b.title = bs.id
			WHERE
				bs.uniq > 0
				AND b.periode = LAST_DAY('$skrg' - INTERVAL 1 MONTH)
				-- AND b.periode = LAST_DAY('$skrg')
				".$str."
			ORDER BY urut
		";
		
		$qry = $this->db->query($query1);
		//echo $this->db->last_query();
		if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
               $data[]=$row;	
			}
		}
		$qry->free_result();
		return $data;
	}
	
	/* Created by Boby 2011-02-10 */
	public function addAutoWdwExBon(){
		if(count($this->input->post('p_id'))>1){
			$member_id = array_values($this->input->post('p_id'));
			$wdw = array_values($this->input->post('ewallet'));
			for($i=0;$i<count($this->input->post('p_id'));$i++){
				$acc = $this->_getAccount($member_id[$i]);
				
				$empid=$this->session->userdata('user');
				$data=array(
					'member_id' => $member_id[$i],
					'tgl' => date('Y-m-d',now()),
					'amount' => $wdw[$i],
					'biayaadm' => 0,
					'event_id' => 'WD1',
					'flag' => 'mem',
					'`status`' => 'approved',
					'account_id' => $acc,
					'remarkapp' => $this->db->escape_str($this->input->post('remark')),
					'approved' => date('Y-m-d',now()),
					'approvedby' => $empid,
					'created' => date('Y-m-d H:i:s',now()),
					'createdby' => $empid
				);
				
				$ewallet = $this->_getEwallet($member_id[$i]);
				if($ewallet >= $wdw[$i]){
					$this->db->insert('withdrawal',$data);
					$id = $this->db->insert_id();
					$this->db->query("call sp_withdrawal('$id','$member_id[$i]','company','$wdw[$i]','mem','Withdrawal','$empid')");
				}
			}
		}
    }
	/* End created by Boby 2011-02-10 */
}?>