<?php
class MSupplier extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Master Supplier
    |--------------------------------------------------------------------------
    |
    | @created 2009-06-30
    | @author qtakwa@yahoo.com@yahoo.com
    |
    */
    public function searchSupplier($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.name,a.address,a.telp,a.fax,a.cp",false);
        $this->db->from('supplier a');
        $this->db->like('a.name',$keywords,'after');
        $this->db->order_by('a.name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countSupplier($keywords=0){
        $this->db->like('a.name',$keywords,'after');
        $this->db->from('supplier a');
        return $this->db->count_all_results();
    }
    public function addSupplier(){
        $data=array(
            'name' => $this->input->post('name'),
            'address' => $this->input->post('address'),
            'telp' => $this->input->post('telp'),
            'fax' => $this->input->post('fax'),
            'cp' => $this->input->post('cp'),
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $this->session->userdata('user')
        );
            
        $this->db->insert('supplier',$data);
    }
       
    public function getSupplier($id){
        $data=array();
        $q = $this->db->get_where('supplier',array('id'=>$id));
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function editSupplier(){
        $data=array(
            'name' => $this->input->post('name'),
            'address' => $this->input->post('address'),
            'telp' => $this->input->post('telp'),
            'fax' => $this->input->post('fax'),
            'cp' => $this->input->post('cp'),
            'updated' => date('Y-m-d H:i:s',now()),
            'updatedby' => $this->session->userdata('user')
        );
            
        $this->db->update('supplier',$data,array('id'=>$this->input->post('id')));
    }
    
}?>