<?php
class MItem extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Master Item
    |--------------------------------------------------------------------------
    |
    | @created 2009-03-29
    | @author qtakwa@yahoo.com@yahoo.com
    |
    */
    public function searchItem($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.name,t.name as type,a.sales,a.display,a.manufaktur",false);
        $this->db->from('item a');
        $this->db->join('type t','a.type_id=t.id','left');
        $this->db->like('a.id',$keywords,'between');
        $this->db->or_like('a.name',$keywords,'between');
        $this->db->order_by('a.id','DESC');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countItem($keywords=0){
        $this->db->like('a.id',$keywords,'between');
        $this->db->or_like('a.name',$keywords,'between');
        $this->db->from('item a');
        return $this->db->count_all_results();
    }
    public function addItem($image){
        $id=$this->input->post('id');
        $price = str_replace(".","",$this->input->post('price'));
        $pricecust = str_replace(".","",$this->input->post('pricecust'));
        $pv = str_replace(".","",$this->input->post('pv'));
        $bv = str_replace(".","",$this->input->post('bv'));
        if($image){
            $data=array(
                'id' => $id,
                'name' => $this->input->post('name'),
                'headline' => $this->db->escape_str($this->input->post('headline')),
                'description' => $_POST['longdesc'],
                'image' => $image,
                'sales' => $this->input->post('sales'),
                'display' => $this->input->post('display'),
                'type_id' => $this->input->post('type'),
                'manufaktur' => $this->input->post('manufaktur'),
                'price' => $price,
                'pv' => $pv,
                'bv' => $bv,
                'pricecust' => $pricecust,
                'created' => date('Y-m-d H:i:s',now()),
                'createdby' => $this->session->userdata('user')
            );
        }else{
            $data=array(
                'id' => $id,
                'name' => $this->input->post('name'),
                'headline' => $this->db->escape_str($this->input->post('headline')),
                'description' => $_POST['longdesc'],
                'sales' => $this->input->post('sales'),
                'display' => $this->input->post('display'),
                'type_id' => $this->input->post('type'),
                'manufaktur' => $this->input->post('manufaktur'),
                'price' => $price,
                'pv' => $pv,
                'bv' => $bv,
                'pricecust' => $pricecust,
                'created' => date('Y-m-d H:i:s',now()),
                'createdby' => $this->session->userdata('user')
            );
        }
        $this->db->insert('item',$data);
        
        $this->db->query("call sp_item('$id')");
    }
       
    public function getItem($id){
        $data=array();
        $this->db->select("a.id,a.name,a.type_id,t.name as type,a.price,a.pv,a.bv,a.pricecust,format(a.price,0)as fprice,
                          format(a.pv,0)as fpv,format(a.bv,0)as fbv,format(a.pricecust,0)as fpricecust,a.sales,a.display,
                          a.manufaktur,a.image,a.headline,a.description,date_format(a.created,'%d-%b-%Y')as created,a.createdby,a.updated,a.updatedby",false);
        $this->db->from('item a');
        $this->db->join('type t','a.type_id=t.id','left');
        $this->db->where('a.id',$id);
        $q = $this->db->get();
        
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function editItem($image){
        $id=$this->input->post('id');
        $price = str_replace(".","",$this->input->post('price'));
        $pricecust = str_replace(".","",$this->input->post('pricecust'));
        $pv = str_replace(".","",$this->input->post('pv'));
        $bv = str_replace(".","",$this->input->post('bv'));
        if($image){
            $data=array(
                'name' => $this->input->post('name'),
                'headline' => $this->db->escape_str($this->input->post('headline')),
                'description' => $_POST['longdesc'],
                'image' => $image,
                'sales' => $this->input->post('sales'),
                'display' => $this->input->post('display'),
                'type_id' => $this->input->post('type'),
                'manufaktur' => $this->input->post('manufaktur'),
                'price' => $price,
                'pv' => $pv,
                'bv' => $bv,
                'pricecust' => $pricecust,
                /* Update by Boby 2011-02-22 */
				//'created' => date('Y-m-d H:i:s',now()),
                //'createdby' => $this->session->userdata('user')
				'updated' => date('Y-m-d H:i:s',now()),
                'updatedby' => $this->session->userdata('user')
				/* End update by Boby 2011-02-22 */
            );
        }else{
            $data=array(
                'name' => $this->input->post('name'),
                'headline' => $this->db->escape_str($this->input->post('headline')),
                'description' => $_POST['longdesc'],
                'sales' => $this->input->post('sales'),
                'display' => $this->input->post('display'),
                'type_id' => $this->input->post('type'),
                'manufaktur' => $this->input->post('manufaktur'),
                'price' => $price,
                'pv' => $pv,
                'bv' => $bv,
                'pricecust' => $pricecust,
                /* Update by Boby 2011-02-22 */
				//'created' => date('Y-m-d H:i:s',now()),
                //'createdby' => $this->session->userdata('user')
				'updated' => date('Y-m-d H:i:s',now()),
                'updatedby' => $this->session->userdata('user')
				/* End update by Boby 2011-02-22 */
            );
        }
            
        $this->db->update('item',$data,array('id'=>$this->input->post('id')));
    }
    public function check_productid($id){
        $q=$this->db->select("id",false)
            ->from('item')
            ->where('id',$id)
            ->get();
        return ($q->num_rows() > 0)? true : false;    
    }
    public function getDropDownType(){
        $data = array();
        $this->db->order_by('id','asc');
        $q = $this->db->get('type');
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[$row['id']]=$row['name'];    
            }
        }
        $q->free_result();
        return $data;
    }
	
}?>