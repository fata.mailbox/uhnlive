<?php
class MTitipan extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | report view titipan stock stc dan admin
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-21
    |
    */
    public function getStockTitipan($member_id){
        $data=array();
        if($member_id != ''){
            $this->db->select("v.id,v.item_id,v.name,fqty,fharga,fpv,fjmlharga,fjmlpv",false)
                ->from('v_titipan v')
                ->where('v.member_id',$member_id)
				// update by Boby 2011-01-04
				->group_by('v.item_id')
                ->group_by('v.harga')
				// end update by Boby 2011-01-04
				;
        }else{
            $this->db->select("v.item_id,v.name,format(sum(v.qty),0)as fqty,fharga,fpv,format(sum(v.qty) * v.harga,0)as fjmlharga,format(sum(v.qty) * v.pv,0)as fjmlpv",false)
                ->from('v_titipan v')
                ->group_by('v.item_id')
                ->group_by('v.harga');
        }
        $this->db->order_by('v.name','asc');
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function sumStockTitipan($member_id){
        $data=array();
        $this->db->select("format(sum(v.jmlharga),0)as ftotalharga,format(sum(v.jmlpv),0)as ftotalpv",false)
                ->from('v_titipan v');
            
        if($member_id)$this->db->where('v.member_id',$member_id);
        
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
                $data=$q->row_array();
        }
        $q->free_result();
        return $data;
    }
    
    public function getStockCardTitipan($member_id,$itemid,$fromdate,$todate){
        $data=array();
        $where = array('s.member_id'=>$member_id,'s.item_id'=>$itemid,'s.tgl >=' => $fromdate,'s.tgl <=' => $todate);
        $q=$this->db->select("concat(s.noref,' - ',s.description)as description,s.tgl,createdby,format(s.saldoawal,0)as fsaldoawal,format(s.saldoin,0)as fsaldoin,format(s.saldoout,0)as fsaldoout,format(s.saldoakhir,0)as fsaldoakhir", false)
            ->from('titipan_history s')
            ->where($where)
            ->order_by('s.id','asc')
            ->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    
}
?>