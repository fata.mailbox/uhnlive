<?php
class MProfile extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | profile member
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @copyright www.smartindo-technology.com
    | @created 2009-04-08
    |
    */
    
    public function getProfile($id=0){
        $q = $this->db->select("m.id,m.nama,m.tgllahir,m.sponsor_id,x.nama as namastc,z.no_stc,m.enroller_id,m.stockiest_id,m.account_id,date_format(m.tglaplikasi,'%d-%m-%Y')as ftglaplikasi,m.jk,m.tempatlahir,
                date_format(m.tgllahir,'%d-%m-%Y')as ftgllahir,m.alamat,m.kota_id,m.email,m.hp,m.telp,m.fax,m.kodepos,m.noktp,m.created,
                m.npwp,m.ahliwaris,b.bank_id,r.name as namabank,b.flag,b.name as namanasabah,b.no,b.area,k.name as kota,p.name as propinsi,e.nama as namaenroller,s.nama as namasponsor",false)
            ->from('member m')
            ->join('account b','m.account_id = b.id','left')
            ->join('kota k','m.kota_id = k.id','left')
            ->join('propinsi p','k.propinsi_id = p.id','left')
            ->join('member e','m.enroller_id = e.id','left')
            ->join('member s','m.sponsor_id = s.id','left')
            ->join('member x','m.stockiest_id = x.id','left')
            ->join('stockiest z','m.stockiest_id = z.id','left')
            ->join('bank r','b.bank_id = r.id','left')
            ->where('m.id',$id)
            ->limit(1)
            ->get();
            //echo $this->db->last_query();
			// b.flag Modified by Boby (2009-11-19)
        return $row = ($q->num_rows() > 0) ? $q->row_array() : false;
    }
    public function getDropDownBank(){
        $data = array();
        
        $q = $this->db->select("id,name",false)
            ->from('bank')
            ->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[$row['id']] = $row['name'];
            }
        }
        $q->free_result();
        return $data;
    }
       
    public function updateProfile(){
	
	/*Modified by Boby (2009-11-19)*/	
	/*
	echo "<br>1 ".$this->input->post('bank_id')." ".$this->input->post('currbank_id');
	echo "<br>2 ".$this->input->post('namanasabah')." ".$this->input->post('currnamanasabah');
	echo "<br>3 ".$this->input->post('norek')." ".$this->input->post('currnorek');
	*/
	if($this->input->post('bank_id') != $this->input->post('currbank_id')){$flag = 0; }
	else if($this->input->post('namanasabah') != $this->input->post('currnamanasabah')){$flag = 0;}
	else if($this->input->post('norek') != $this->input->post('currnorek')){$flag = 0;}
	else{$flag = $this->input->post('flag');}
	//echo $flag;

	/*end Modified by Boby (2009-11-19)*/

        $data = array(
            'alamat'  => $this->input->post('alamat'),
            'kota_id'  => $this->input->post('kota_id'),
            'kodepos'  => $this->input->post('kodepos'),
            'noktp'  => $this->input->post('noktp'),
            'tempatlahir'  => $this->input->post('tempatlahir'),
            'tgllahir'  => $this->input->post('date'),
            'jk'    => $this->input->post('jk'),
            'telp'  => $this->input->post('telp'),
            'fax'    => $this->input->post('fax'),
            'hp'    => $this->input->post('hp'),
            'email'    => $this->input->post('email'),
            'ahliwaris'    => $this->input->post('ahliwaris'),
            'npwp'    => $this->input->post('npwp'),
            'updated'   => date('Y-m-d', now()),
            'updatedby'    => $this->session->userdata('username')
        );
        $this->db->update('member',$data,array('id' => $this->input->post('id')));
        
        if($this->input->post('area') && $this->input->post('norek')){
            $data = array(
                'bank_id' => $this->input->post('bank_id'),
                'member_id' => $this->input->post('id'),
                'name' => $this->input->post('namanasabah'),
                'no' => $this->input->post('norek'),
                'area' => $this->input->post('area'),
				'flag' => $flag,	// Modified by Boby (2009-11-19)
                'createdby' => $this->session->userdata('username'),
                'created' => date('Y-m-d H:i:s', now())
            );
            
            if($this->input->post('account_id') > 0){
                $row = $this->_getAccountBank($this->input->post('account_id'));
                if($row){
                    if($row->bank_id != $this->input->post('bank_id')){
                        $this->db->insert('account',$data);
                        
                        $id = $this->db->insert_id();
                        $this->db->update('member',array('account_id'=>$id),array('id' => $this->input->post('id')));
                    }else{
                        if($row->name != $this->input->post('namanasabah')){
                            $this->db->insert('account',$data);
                            
                            $id = $this->db->insert_id();
                            $this->db->update('member',array('account_id'=>$id),array('id' => $this->input->post('id')));
                        }else{
                            if($row->no != $this->input->post('norek')){
                                $this->db->insert('account',$data);
                                
                                $id = $this->db->insert_id();
                                $this->db->update('member',array('account_id'=>$id),array('id' => $this->input->post('id')));
                            }else{
                                if($row->area != $this->input->post('area')){
                                    $this->db->insert('account',$data);
                                    
                                    $id = $this->db->insert_id();
                                    $this->db->update('member',array('account_id'=>$id),array('id' => $this->input->post('id')));
                                }
                            }
                        }
                    }
                }
            }else{
                $this->db->insert('account',$data);
                
                $id = $this->db->insert_id();
                $this->db->update('member',array('account_id'=>$id),array('id' => $this->input->post('id')));
            }
        }
    }
    protected function _getAccountBank($id){
        $q=$this->db->get_where('account',array('id'=>$id));
        //echo $this->db->last_query();
        return $var = ($q->num_rows>0) ? $q->row() : false;
    }
    public function searchMember($option,$keywords=0,$num,$offset){
        $data = array();
        $this->db->select("m.id,m.nama,k.name as kota,format(m.ewallet,0)as fewallet,format(m.ps,0)as fps,j.name as jenjang,b.reason",false);
        $this->db->from('member m');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('jenjang j','m.jenjang_id=j.id','left');
        $this->db->join('users u','m.id=u.id','left');
        $this->db->join('banned b','u.banned_id=b.id','left');
        if($option ==='member_id'){
            $this->db->like('m.id', $keywords, 'between');
        }
        else{
            $this->db->like('m.nama', $keywords, 'between');
        }
        $this->db->order_by('u.banned_id','desc');
        $this->db->order_by('m.nama','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        
        if($q->num_rows >0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countSearchMember($option=0,$keywords=0){
        $this->db->select("m.id",false);
        $this->db->from('member m');
        if($option === 'member_id')$this->db->like('m.id', $keywords, 'between');
        else $this->db->like('m.nama', $keywords, 'between');
        
        return $this->db->count_all_results();
    }
    public function getBannedMember($id){
        $q=$this->db->select("m.id,m.nama,u.banned_id,b.reason",false)
            ->from('member m')
            ->join('users u','m.id=u.id','left')
            ->join('banned b','u.banned_id=b.id','left')
            ->where('m.id',$id)->get();
        return $var = ($q->num_rows()>0)? $q->row() : false;
    }
    
    public function updateBanned(){
        if($this->input->post('banned') == '1'){
            $data = array(
                'reason' => $this->db->escape_str($this->input->post('remark'))
            );
            if($this->input->post('banned_id') >0){
                $this->db->update('banned',$data,array('id'=>$this->input->post('banned_id')));
                $this->session->set_flashdata('message','Updated banned member successfully');
            }else{
                $this->db->insert('banned',$data);
                $id = $this->db->insert_id();
                $this->db->update('users',array('banned_id'=>$id),array('id'=>$this->input->post('member_id')));
                $this->session->set_flashdata('message','Banned member successfully');
            }
        }else{
            if($this->input->post('banned') == '2' && $this->input->post('banned_id') >0){
                $this->db->delete('banned',array('id'=>$this->input->post('banned_id')));
                $this->db->update('users',array('banned_id'=>0),array('id'=>$this->input->post('member_id')));
                $this->session->set_flashdata('message','Active banned member successfully');
            }else{
                $this->session->set_flashdata('message','Cancel banned member successfully');
            }
            
        }
    }
	
	// Created by Boby 20100715
    public function cekBonusPeriode(){
		$this->db->select("month(max(periode)) as bln",false);
        $q = $this->db->get('bonus');
        if($q->num_rows() > 0){
			$row = $q->row_array();
        }
        $q->free_result();
        return $row["bln"];
	}
	// End created by Boby 20100715
	
	// Created by Boby 20110323
    public function getNama($id){
		$qry = "
			SELECT nama
			FROM member
			WHERE id = '$id'
		";
        $q = $this->db->query($qry);
        if($q->num_rows() > 0){
			$row = $q->row_array();
        }
        $q->free_result();
        return $row['nama'];
	}
	
	public function cekOdp($id){
		$q = $this->db->query("
			SELECT m.nama
			FROM z_201102_odp z 
			LEFT JOIN member m ON z.reff_id = m.id 
			WHERE z.member_id = '$id'
		");
		//echo $this->db->last_query();
        if($q->num_rows() > 0){
			$row = $q->row_array();
        }else{
			$row['nama']='None';
		}
        $q->free_result();
        return $row['nama'];
	}
	
	public function saveOdp($id){
		$data = array(
            'member_id' => $this->input->post('member_id'),
            'reff_id'=> $id,
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$this->session->userdata('username')
        );
        $this->db->insert('z_201102_odp',$data);
	}
	
	// End created by Boby 20110323
	
}
?>