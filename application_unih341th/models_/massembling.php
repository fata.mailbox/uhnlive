<?php
class MAssembling extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | untuk menu manufaktur item
    |--------------------------------------------------------------------------
    |
    | untuk menaufaktur/mengabaungkan beberatpa item jadi 1 code tersendiri
    |
    | @author takwa
    | @created 2009-03-24
    |
    */
    public function searchManufaktur($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.manufaktur_id,i.name,t.name as type",false);
        $this->db->from('manufaktur a'); 
        $this->db->join('item i','a.manufaktur_id=i.id','left');
        $this->db->join('type t','i.type_id=t.id','left');
        $this->db->like('a.id',$keywords,'after');
        $this->db->or_like('i.name',$keywords,'after');
        $this->db->group_by('a.manufaktur_id');
        $this->db->order_by('i.name','asc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countManufaktur($keywords=0){
        
        $this->db->like('a.id',$keywords,'after');
        $this->db->or_like('i.name',$keywords,'after');
        $this->db->from('manufaktur a');
        $this->db->join('item i','a.manufaktur_id=i.id','left');
        return $this->db->count_all_results();
    }
    /*
    |--------------------------------------------------------------------------
    | add inventory in
    |--------------------------------------------------------------------------
    |
    | to save Form penerimaan barang
    |
    | @param void
    | @author takwa
    | @created 2008-12-27
    |
    */
    public function addManufaktur(){
        $id = $this->input->post('itemcode');
        $this->db->update('item',array('manufaktur'=>'Yes'),array('id'=>$id));
        
        $key=0;
        while($key < count($_POST['counter'])){
            //echo $_POST['qty'.$key]."aa <br>";
            $qty = str_replace(".","",$_POST['qty'.$key]);
            
            if($_POST['itemcode'.$key] and $qty > 0){
                $data=array(
                    'manufaktur_id' => $id,
                    'item_id' => $this->input->post('itemcode'.$key),
                    'qty' => $qty
                 );
                 $this->db->insert('manufaktur',$data);
            }
            $key++;
        }
        
    }
    public function editManufaktur(){
        $id = $this->input->post('itemcodex');
        $this->db->delete('manufaktur',array('manufaktur_id'=>$id));
        
        $key=0;
        while($key < count($_POST['counter'])){
            //echo $_POST['qty'.$key]."aa <br>";
            $qty = str_replace(".","",$_POST['qty'.$key]);
            
            if($_POST['itemcode'.$key] and $qty > 0){
                $data=array(
                    'manufaktur_id' => $id,
                    'item_id' => $_POST['itemcode'.$key],
                    'qty' => $qty
                 );
                 $this->db->insert('manufaktur',$data);
            }
            $key++;
        }
    }
    
    public function check_manufaktur($id){
        $q=$this->db->select("id",false)
            ->from('manufaktur')
            ->where('manufaktur_id',$id)
            ->get();
        return ($q->num_rows() > 0)? true : false;    
    }
    public function getManufaktur($id){
        $data=array();
        $this->db->select("a.manufaktur_id,i.type_id,a.item_id,i.name as namemanufaktur,x.name as nameitem,a.qty,format(a.qty,0)as fqty,t.name as type",false);
        $this->db->from('manufaktur a');
        $this->db->join('item i','a.manufaktur_id=i.id','left');
        $this->db->join('type t','i.type_id=t.id','left');
        $this->db->join('item x','a.item_id=x.id','left');
        $this->db->where('a.manufaktur_id',$id);
        $this->db->order_by('a.id','asc');
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Assembling 
    |--------------------------------------------------------------------------
    |
    | to assembling or dessembling product
    |
    | @created 2009-03-29
    | @author qtakwa@yahoo.com@yahoo.com
    |
    */
    public function searchAssembling($keywords=0,$num,$offset){
        $data = array();
        $this->db->select("a.id,a.item_id,format(a.qty,0)as fqty,a.desembling,a.remark,date_format(a.date,'%d-%b-%Y')as date,a.createdby,i.name,t.name as type",false);
        $this->db->from('assembling a');
        $this->db->join('item i','a.item_id=i.id','left');
        $this->db->join('type t','i.type_id=t.id','left');
        $this->db->like('a.id',$keywords,'after');
        $this->db->or_like('a.item_id',$keywords,'after');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function countAssembling($keywords=0){
        $this->db->like('a.id',$keywords,'after');
        $this->db->or_like('a.item_id',$keywords,'after');
        $this->db->from('assembling a');
        $this->db->join('item i','a.item_id=i.id','left');
        return $this->db->count_all_results();
    }
    
    public function addAssembling(){
        $item_id = $this->input->post('itemcode');
        $desembling = $this->input->post('condition');
        if($desembling == 'Yes')$event_id = 'DS1';
        else $event_id = 'AS1';
        $empid = $this->session->userdata('user');
        
        $qty = str_replace(".","",$this->input->post('qty'));
        $whsid = $this->input->post('whsid');
        
        $data=array(
            'date' => date('Y-m-d',now()),
            'warehouse_id'=>$whsid,
            'item_id'=>$item_id,
            'qty'=>$qty,
            'remark'=>$this->db->escape_str($this->input->post('remark')),
            'desembling' => $desembling,
            'event_id' => $event_id,
            'created'=>date('Y-m-d H:i:s',now()),
            'createdby'=>$empid
        );
        $this->db->insert('assembling',$data);
        
        $id=$this->db->insert_id();
        $this->db->query("call sp_assembling('$desembling','$whsid','$id','$item_id','$qty','$empid')");

    }
    public function getAssembling($id){
        $data=array();
        $this->db->select("a.id,a.item_id,format(a.qty,0)as fqty,a.remark,date_format(a.date,'%d-%b-%Y')as date,a.desembling,a.createdby,i.name",false);
        $this->db->from('assembling a');
        $this->db->join('item i','a.item_id=i.id','left');
        $this->db->where('a.id',$id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            $data=$q->row();
        }
        $q->free_result();
        return $data;
    }
    public function getAssemblingDetail($id){
        $data=array();
        $this->db->select("a.item_id,format(a.qty,0)as fqty,i.name",false);
        $this->db->from('assembling_d a');
        $this->db->join('item i','a.item_id=i.id','left');
        $this->db->where('a.assembling_id',$id);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }

    /* Update by Boby (2010-05-14)*/
    public function cekStokAssembling(){
		$item = $this->input->post('itemcode');
                $whsid = $this->input->post('whsid');
		$qty = str_replace(".","",$this->input->post('qty'));
		
		$qry = "SELECT f_check_asm('$whsid','$item', '$qty')AS krg";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
		if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['krg'];
	}
	/* End update by Boby (2010-05-14)*/
	
	/* Update by Boby (2010-07-05)*/
    public function cekStok(){
		$item = $this->input->post('itemcode');
		$qty = str_replace(".","",$this->input->post('qty'));
		$whsid = $this->input->post('whsid');
				
		$qry = "SELECT qty FROM stock WHERE item_id = '$item' and warehouse_id = '$whsid'";
		$q = $this->db->query($qry);
		//echo $this->db->last_query();
		if($q->num_rows()>0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data['qty'];
	}
	/* End update by Boby (2010-07-05)*/
    
}?>