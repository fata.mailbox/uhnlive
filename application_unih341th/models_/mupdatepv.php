<?php
class MUpdatepv extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Sales Order
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-05-12
    |
    */
    
    public function search_update_pv($keywords=0,$num,$offset){
        $data = array();
        
        $where = "( a.so_id LIKE '$keywords%' or m.nama LIKE '$keywords%' )";
        
        $this->db->select("s.id,date_format(a.tanggal,'%d-%b-%Y')as tgl,a.item_id,s.member_id,format(sum(a.pv_lama),0)as fpv_lama,format(sum(a.pv_baru),0)as fpv_baru,m.nama,a.createdby",false);
        $this->db->from('update_pv a');
        $this->db->join('so s','a.so_id=s.id','left');
        $this->db->join('member m','s.member_id=m.id','left');
        $this->db->where($where);
        $this->db->group_by('a.so_id');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function count_update_pv($keywords=0){
        $where = "( a.so_id LIKE '$keywords%' or m.nama LIKE '$keywords%' )";
        
        $this->db->from('update_pv a');
        $this->db->join('so s','a.so_id=s.id','left');
        $this->db->join('member m','s.member_id=m.id','left');
        $this->db->where($where);
        //$q=$this->db->get();
        //echo $this->db->last_query();
        return $this->db->count_all_results();
    }
    public function editUpdatePV(){
        $so_id = $this->input->post('so_id');
        $empid = $this->session->userdata('username');
        
        $key=0;
        while($key < count($_POST['id'])){
            
            $item_id = $_POST['itemcode'][$key];
            $qty = str_replace(".","",$_POST['qty'][$key]);
            $pv_lama = str_replace(".","",$_POST['pv_lama'][$key]);
            $pv_baru = str_replace(".","",$_POST['pv_baru'][$key]);
            $id = $_POST['id'][$key];
            //echo $id." / ".$this->input->post('so_id');
            
            if($_POST['id'][$key] and $pv_baru >= 0){
                 $data=array(
                     'pv' => $pv_baru,
                     'jmlpv' => $qty * $pv_baru
                 );
                 
                 $this->db->update('so_d',$data,array('id'=>$id));
                 $data = array(
                    'so_id' => $so_id,
                    'so_d_id' => $id,
                    'tanggal' => date('Y-m-d',now()),
                    'item_id' => $item_id,
                    'qty' => $qty,
                    'pv_lama' => $pv_lama,
                    'pv_baru' => $pv_baru,
                    'created'=>date('Y-m-d H:i:s',now()),
                    'createdby'=>$empid
                    );
                $this->db->insert('update_pv',$data);
            }
            $key++;
        }
        $this->db->query("call sp_so_update_pv('$so_id','$empid')");
    }
    
    public function get_update_pv($id=0){
        $data = array();
        $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.kit,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    m.nama,m.alamat,m.kodepos,k.name as kota,p.name as propinsi",false);
        $this->db->from('so a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('kota k','m.kota_id=k.id','left');
        $this->db->join('propinsi p','k.propinsi_id=p.id','left');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function get_update_pv_detail($id=0){
        $data = array();
        $this->db->select("d.item_id,d.qty,format(d.qty,0)as fqty,d.pv_lama,d.pv_baru,format(d.pv_lama,0)as fpv_lama,format(d.pv_baru,0)as fpv_baru,a.name",false);
        $this->db->from('update_pv d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.so_id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    public function get_so($id=0){
        $data = array();
        $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.member_id,format(a.totalharga,0)as ftotalharga,format(a.totalpv,0)as ftotalpv,a.remark,a.kit,date_format(a.created,'%d-%b-%Y')as created,a.createdby,
                    m.nama",false);
        $this->db->from('so a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->where('a.id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function get_so_detail($id=0){
        $data = array();
        $this->db->select("d.id,d.item_id,d.qty,format(d.qty,0)as fqty,d.pv,format(d.pv,0)as fpv,a.name",false);
        $this->db->from('so_d d');
        $this->db->join('item a','d.item_id=a.id','left');
        $this->db->where('d.so_id',$id);
        $q=$this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
     
}?>