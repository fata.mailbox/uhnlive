<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Invinsearchrsk extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MSearch'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','pagination'));
        $this->form_validation->set_rules('search','','');

        $idwhr =$this->uri->segment(4);
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(5))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $config['base_url'] = site_url().'invinsearchrsk/index/'.$this->uri->segment(3).'/'.$this->uri->segment(4);
        $data['from_rows'] = $this->uri->segment(5); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countSearchRusak($keywords,$idwhr);
        $config['per_page'] = 10;
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchRusak($keywords,$idwhr,$config['per_page'], $this->uri->segment(5));
        $data['page_title'] = 'Inverntory Search';
        
        $this->load->view('search/inv_rusak_search',$data);
    }

    public function stock_rusak(){
        $this->load->library(array('form_validation','pagination'));
        $this->form_validation->set_rules('search','','');

        $idwhr =$this->uri->segment(4);
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(5))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $config['base_url'] = site_url().'invinsearchrsk/stock_rusak/'.$this->uri->segment(3).'/'.$this->uri->segment(4);
        $data['from_rows'] = $this->uri->segment(5); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countSearchStockRusak($keywords,$idwhr);
        $config['per_page'] = 10;
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchStockRusak($keywords,$idwhr,$config['per_page'], $this->uri->segment(5));
        $data['page_title'] = 'Inverntory Rusak Search';
        
        $this->load->view('search/stock_rusak_search',$data);
    }
    
}
?>