<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Monthly_report extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu', 'Report_model','MTrck'));
	$this->load->library('export');
	$this->load->library('exportdaily');
	        
    }
    public function index(){
	$this->create();
    }
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library('form_validation');
	
        
	$this->form_validation->set_rules('bulan','','');
	$this->form_validation->set_rules('tahun','','');
	$this->form_validation->set_rules('flag','','');
        $this->form_validation->set_rules('type_id','type','required');
	
        if($this->form_validation->run()){
	    $fname=$this->input->post('type_id');
	    $file = $fname."_".date('Ymd_His',now());
        
		//20151104 ASP START
		//$sql = $this->Report_model->report_convert_to_excel($fname);
		//@$this->export->to_excel($sql, $file);
		
		if($fname=='salesbyproduct' || $fname =='salesbyregion' || $fname == 'salesbyregioncluster' || $fname == 'salesbyregionstc'){
	    	$sql = $this->Report_model->daily_report_convert_to_excel($this->input->post('bulandaily'),$this->input->post('tahundaily'),$fname);
			if($fname=='salesbyproduct'){
				@$this->exportdaily->salesbyprod_to_excel($this->input->post('bulandaily'),$this->input->post('tahundaily'),$sql, $file); 
			}else if($fname=='salesbyregion'){
				@$this->exportdaily->salesbyregion_to_excel($this->input->post('bulandaily'),$this->input->post('tahundaily'),$sql, $file); 
			}else if($fname=='salesbyregioncluster'){
				@$this->exportdaily->salesbyregioncluster_to_excel($this->input->post('bulandaily'),$this->input->post('tahundaily'),$sql, $file); 
			}else if($fname=='salesbyregionstc'){
				@$this->exportdaily->salesbyregionstc_to_excel($this->input->post('bulandaily'),$this->input->post('tahundaily'),$sql, $file); 
			}
		}else{
	    	$sql = $this->Report_model->report_convert_to_excel($fname);
			@$this->export->to_excel($sql, $file); 
		}
		//20151104 ASP END
		
		$this->MTrck->addTrck('Export Report',$fname);
	    
	    //$sql = $this->Report_model->report_convert_to_excel();
	    //$sql = $this->Report_model->report_convert_to_excel($fname);
	    //$this->export->to_excel($sql, $fname);

	    //$qry=$this->Report_model->report_convert_to_excel($fname);

		redirect('','refresh');
		
	}else{
            $data['results']=false;
        }
        
        $data['page_title'] = 'Export Report';
		
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
		
        $this->load->view('report/monthly_report_xls',$data);
    }
	
	/* Created by Boby 20131112 */
    public function cetak($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
		$this->load->library(array('form_validation'));
        $data['results'] = $this->Report_model->emaildata($id);
		if($id==1){$data['fname'] = 'Stockiest';}
		else{$data['fname'] = 'MStockiest';}
        $this->load->view('member/emailStc',$data);
    }
	/* End created by Boby 20131112 */
	
}
?>