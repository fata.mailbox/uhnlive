<?php // UPDATE `menu` SET `url`='sls_inc' WHERE `id`='92';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sls_util_stc extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('MSales','MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('tahun','','');
		$this->form_validation->set_rules('quart','','');
        //$this->form_validation->set_rules('sort','','');
        
        if($this->form_validation->run()){
			$data['period_']="Quart";
			if($this->input->post('quart')=="00-00"){
				$data['results']=$this->MSales->sales_util_member_rpt($this->input->post('tahun'),0);
				$data['results_total']=$this->MSales->sales_util_member_rpt($this->input->post('tahun'),0);
			}else{
				$period = $this->input->post('tahun');
				$period.= '-';
				$period.= $this->input->post('quart');
				
				$data['results']=$this->MSales->sales_util_stc_rpt($this->input->post('tahun'),$period);
				$data['results_total']=$this->MSales->sales_util_stc_rpt($this->input->post('tahun'),$period);
				$data['period_']="Month";
			}
            $data['thn']=$this->input->post('tahun');
			$data['total']=0;
        }else{
            $data['results']=false;
			$data['thn']=date("Y");
            $data['total']=false;
        }
        $data['dropdownyear'] = $this->MSales->get_year_report();
		$data['dropdownq'] = $this->MSales->get_q(1);
        $data['page_title'] = 'RSM/ASM Utilities - Stockiest (For Tracking RSM/ASM Performance)';
        // $this->load->view('report/sales_incentive',$data); // updated by Boby 20140911
		//$this->load->view('report/sales_incentive_report',$data); // created by Boby 20140911
		if($this->input->post('submit')=="export")
		$this->load->view('report/sales_util_stc_report_export',$data);
		else
		$this->load->view('report/sales_util_stc_report',$data); // created by ASP 20160927
		
		
    }
}
?>