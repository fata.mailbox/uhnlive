<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class National_report_daily extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MBonus','MMenu', 'MSales', 'MTopping'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        //$this->form_validation->set_rules('member_id','Member ID','required');
        //$this->form_validation->set_rules('name','','');
        //$this->form_validation->set_rules('periode','','');
        $this->form_validation->set_rules('tahun','','');

		if($this->form_validation->run()){
			//echo "run";
            //$data['results']=$this->MBonus->getBonus($this->input->post('member_id'),$this->input->post('periode'));
            //$data['total']=$this->MBonus->getTotalBonus($this->input->post('member_id'),$this->input->post('periode'));
			$data['results'] = true;
            $data['thn']=$this->input->post('tahun');
			$data['rowdata']=$this->MSales->getNationalMonthly($this->input->post('tahun'));
        }else{
			//echo "not run";
            $data['results']=false;
            $data['total']=false;
        }
        
        $data['dropdownyear'] = $this->MSales->get_year_report();
        $data['page_title'] = 'Monthly National Report';
        $this->load->view('report/national_report_monthly',$data);
    } 
}
?>