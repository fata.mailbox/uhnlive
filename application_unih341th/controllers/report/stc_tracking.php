<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stc_tracking extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MBonus','MMenu', 'MTracking'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        //$this->form_validation->set_rules('periode','','');
        $this->form_validation->set_rules('member_id','','');
        $this->form_validation->set_rules('name','','');
        if($this->form_validation->run()){
            $memberid = $this->input->post('member_id');
            //$data['results']=$this->MBonus->getJenjang($memberid,$this->input->post('periode'));
            //$data['rs']=$this->MBonus->kualifikasiLeader($this->input->post('periode'),$memberid);
			$data['trip'] = $this->MTracking->get_stc_trips();
			$data['trackTrip'] = TRUE;
			
			/*
            if(!$memberid){
                $data['results2']=$this->MBonus->countLeader($this->input->post('periode'));
            }else{
                $data['hightjenjang']=$this->MBonus->get_hightjenjang($memberid);
				//20150901 ASP Start
				if($data['hightjenjang']->jenjangbaru > 5){
					$getTgl = $this->MBonus->getTglSL($memberid);
           			$data['rs']=$this->MBonus->kualifikasiLeaderNew($this->input->post('periode'),$memberid,$getTgl->tgl,$getTgl->tgl_akhir);
					$data['rs_sl']=$this->MBonus->kualifikasiSuperLeader($this->input->post('periode'),$memberid);	
				}
				//20150901 ASP End
            }
			*/
        }else{
            $memberid = $this->session->userdata('userid');
			/*
            $data['row']=$this->MBonus->getPS($memberid,'');
            $data['rs']=$this->MBonus->kualifikasiLeader('',$memberid);
            $data['results']=$this->MBonus->getJenjang($memberid,'');
            $data['hightjenjang']=$this->MBonus->get_hightjenjang($memberid);<br>
			*/
            $data['row']=$this->MBonus->getPS($memberid,'');
			$data['trip'] = $this->MTracking->get_stc_trips();
			$data['trackTrip'] = FALSE;
        }
        
        //$data['dropdown'] = $this->MBonus->get_dropdown();
        $data['page_title'] = 'Stockiest Tracking';
        //$this->load->view('member/jenjang_history',$data);
		//20150901 ASP Start
        $this->load->view('report/stc_tracking',$data);
		//20150901 ASP End
    }
    
      
}
?>