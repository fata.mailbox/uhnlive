<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stc_acv_old extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('MReport', 'MMenu'));
		$this->load->library('export');
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('tahun','','');
        $this->form_validation->set_rules('bulan','','');
        
        if($this->form_validation->run()){
            $data['results']=$this->MReport->viewStcAcv($this->input->post('tahun'), $this->input->post('bulan')); // updated by Boby 20140922
			$data['thn']=$this->input->post('tahun');
			$data['bulan']=$this->input->post('bulan');
			$data['total']=0;
			$data['bln']=$this->input->post('bulan');
			$data['thn']=$this->input->post('tahun');
        }else{
            $data['results']=false;
			$data['thn']=date("Y");
			$data['bulan']=date("mm");
            $data['total']=false;
			$data['bln']=0;
			$data['thn']=0;
        }
        $data['dropdownyear'] = $this->MReport->get_year_report();
		$data['dropdownq'] = $this->MReport->get_q(0);
        $data['page_title'] = 'Stockiest Achievement';
        $this->load->view('report/vstc_acv_old',$data);
    }
	public function export($thn,$bln){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
		$fname=$this->input->post('type_id');
		$file = "stcAcv_".date('Ymd_His',now());
		
		$sql = $this->MReport->stockiest_acv_export($thn, $bln);
		@$this->export->to_excel($sql, $file); 
		redirect('','refresh');
	}
}
?>