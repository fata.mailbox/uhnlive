<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Base_model extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MBonus','MMenu', 'MSales', 'MTopping'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        //$this->form_validation->set_rules('member_id','Member ID','required');
        //$this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('periode','','');
        
        if($this->form_validation->run()){
			//echo "run";
            //$data['results']=$this->MBonus->getBonus($this->input->post('member_id'),$this->input->post('periode'));
            //$data['total']=$this->MBonus->getTotalBonus($this->input->post('member_id'),$this->input->post('periode'));
			$data['results'] = true;
			
			$data['field']=$this->MTopping->get_month_name($this->input->post('periode'));
			$data['curr']=$this->MSales->getBaseModel($this->input->post('periode'),0);
			$data['lmtd']=$this->MSales->getBaseModel($this->input->post('periode'),1);
			$data['lytd']=$this->MSales->getBaseModel($this->input->post('periode'),2);
			$data['skrg']=$this->MSales->getBaseModel($this->input->post('periode'),3);
			
			$data['sp']=$this->MSales->getSponsoring($this->input->post('periode'));
			$data['tcurr']=$this->MSales->getTarget($this->input->post('periode'),0);
			$data['tlmtd']=$this->MSales->getTarget($this->input->post('periode'),1);
			$data['tlytd']=$this->MSales->getTarget($this->input->post('periode'),2);
			$data['tskrg']=$this->MSales->getTarget($this->input->post('periode'),3);
			
			/*
			$data['nmr']=$this->MSales->getNewMember_($this->input->post('periode'));
			$data['nm']=$this->MSales->getNewMember($this->input->post('periode'));
			$data['nr']=$this->MSales->getNewRecruit($this->input->post('periode'));
			$data['onm']=$this->MSales->getOmsetNewMember($this->input->post('periode'));
			$data['sp']=$this->MSales->getSponsoring($this->input->post('periode'));
			$data['oom']=$this->MSales->getOmsetOldMember($this->input->post('periode'));
			$data['oom2']=$this->MSales->getOmsetOldMember_($this->input->post('periode'));
			$data['os']=$this->MSales->getOmsetStaff($this->input->post('periode'));
			*/
        }else{
			//echo "not run";
            $data['results']=false;
            $data['total']=false;
        }
        
        $data['dropdown'] = $this->MBonus->getDropDrownPeriode();
        $data['page_title'] = 'Base Model Report';
        $this->load->view('report/vbase_model',$data);
    } 
}
?>