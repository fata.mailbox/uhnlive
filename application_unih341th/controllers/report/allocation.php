<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Allocation extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('MSales','MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('tahun','','');
		$this->form_validation->set_rules('quart','','');
        
        if($this->form_validation->run()){
			if($this->input->post('quart')=="00-00"){
				$data['results']=$this->MSales->viewAllocMember($this->input->post('tahun'),0);
			}else{
				$period = $this->input->post('tahun');
				$period.= '-';
				$period.= $this->input->post('quart');
				
				$data['results']=$this->MSales->viewAllocMember($this->input->post('tahun'),$period);
				$data['period_']="Month";
			}
            $data['thn']=$this->input->post('tahun');
			$data['total']=0;
        }else{
			$thn=date("Y");
            $data['results']=false;
			$data['thn']=date("Y");
            $data['total']=false;
        }
        $data['dropdownyear'] = $this->MSales->get_year_report();
		$data['dropdownq'] = $this->MSales->get_q(1);
        $data['page_title'] = 'Allocation Member';
        $this->load->view('report/allocation_list',$data);
    }
	
    public function create(){
		$z=0;
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
		$this->form_validation->set_rules('tahun','','');
		$this->form_validation->set_rules('quart','','');
		//$this->form_validation->set_rules('region','','');
		$this->form_validation->set_rules('member_id','Member ID','required|callback__check_exist_data');
		$this->form_validation->set_rules('name','','');
		$this->form_validation->set_rules('kota_id','Allocation City','required');
		$this->form_validation->set_rules('propinsi','','');
		$this->form_validation->set_rules('city','','');
        
        $groupid = $this->session->userdata('group_id');
        $data['z']=$z;
		
        if($this->form_validation->run()){
			
            if(!$this->MMenu->blocked()){
				$period = $this->input->post('tahun');
				$period.= '-';
				$period.= $this->input->post('quart');
				
				$results = $this->MSales->cek_alloc_member($period, $this->input->post('member_id'));
				if($results == 'ok'){
					$this->MSales->insert_data_alloc_member($period, $this->input->post('member_id'), $this->input->post('kota_id'));
					$this->session->set_flashdata('message','Create Allocation Successfully');
					redirect('report/allocation/create/','refresh');
				}else{
					$this->session->set_flashdata('message','Data Already Exists');
					redirect('report/allocation/create/','refresh');
				}
            }
			//$this->session->set_flashdata('message','Flag = '.$flag);
            redirect('report/allocation','refresh');
        }
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-green.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['groupid'] = $groupid;
		$data['dropdownyear'] = $this->MSales->get_y();
		$data['dropdownq'] = $this->MSales->get_q(0);
		$data['dropdownreg'] = $this->MSales->get_region();
        $data['page_title'] = 'Create Allocation Member '.$flag;
        $this->load->view('report/Allocation_form',$data);
    }
	
	public function _check_exist_data(){
        $results = $this->MSales->cek_alloc_member($period, $this->input->post('member_id'));
		if($results != 'ok'){
			$this->session->set_flashdata('message','Data Already Exists');
			return false;
		}
        return true;
    }
}
?>