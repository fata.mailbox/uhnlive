<?php // INSERT INTO `menu`(`id`,`submenu_id`,`title`,`folder`,`url`,`comment`,`sort`)VALUES(NULL,'16','Sales By City','report','salesbycity','','1');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Salesbycity extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('MSales','MMenu'));
		$this->load->library('export');
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('tahun','','');
		$this->form_validation->set_rules('quart','','');
        //$this->form_validation->set_rules('sort','','');
        
        if($this->form_validation->run()){
			$data['results']=$this->MSales->sales_by_city($this->input->post('tahun'));
			/*
			$data['period_']="Quart";
			if($this->input->post('quart')=="00-00"){
				$data['results']=$this->MSales->sales_by_city($this->input->post('tahun'),0);
			}else{
				$period = $this->input->post('tahun');
				$period.= '-';
				$period.= $this->input->post('quart');
				
				$data['results']=$this->MSales->sales_by_city($this->input->post('tahun'),$period);
				$data['period_']="Month";
			}
			$data['total']=0;
			*/
            $data['thn']=$this->input->post('tahun');
        }else{
            $data['results']=false;
			$data['thn']=date("Y");
            $data['total']=false;
        }
        $data['dropdownyear'] = $this->MSales->get_year_report();
		//$data['dropdownq'] = $this->MSales->get_q(1);
        $data['page_title'] = 'Sales By City';
        // $this->load->view('report/sales_incentive',$data); // updated by Boby 20140911
		$this->load->view('report/salesbycity',$data); // created by Boby 20140911
    }
	public function exportxls($thn){
	    $fname='salesbycity';
	    $file = $fname."_".date('Ymd_His',now());
        
	    $sql = $this->MSales->sales_by_city($thn);
	    @$this->export->to_excel($sql, $file); 
	}
}
?>