<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Schistory extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        $this->load->model(array('MMenu','MSchistory'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        $this->form_validation->set_rules('reptype','','');
        
        if($this->form_validation->run()){
			//$data['topten'] = $this->MMember->getTop10recuit($this->input->post('fromdate'),$this->input->post('todate'));
			//$data['topbuy'] = $this->MMember->getTop10buyers($this->input->post('fromdate'),$this->input->post('todate'));
			//$data['rekap'] = $this->MMember->browseRC($this->input->post('fromdate'),$this->input->post('todate'));
			if($this->input->post('reptype') == 0) {
				$data['sc'] = $this->MSchistory->getSCHistory2($this->input->post('fromdate'),$this->input->post('todate'));
			}else if($this->input->post('reptype') == 1){
				$data['sc'] = $this->MSchistory->getSCHistoryPerProducts($this->input->post('fromdate'),$this->input->post('todate'));
			}else if($this->input->post('reptype') == 2){
				$data['sc'] = $this->MSchistory->getSCHistory($this->input->post('fromdate'),$this->input->post('todate'));
			}
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        if($this->session->userdata('username')=='UHN22')
			$data['reptype'] = ['0'=> 'Per SKU','1'=>'Per Product','2'=>'Per SKU 2'];
		else
			$data['reptype'] = ['0'=> 'Per SKU','1'=>'Per Product'];
		
        $data['page_title'] = 'Stockiest Consignment History';
        $this->load->view('inv/sc_index',$data);
    }    
}
?>