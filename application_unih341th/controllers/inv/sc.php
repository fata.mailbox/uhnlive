<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sc extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
      $this->load->model(array('MMenu','GLobal_model','MInvreport'));
	  $this->load->model(array('MRo','MSc','Mruleorder'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        $this->form_validation->set_rules('whsid','','');
		$data['pray_msg']='';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $this->session->set_userdata('keywords_whsid',$this->db->escape_str($this->input->post('whsid')));
			$data['pray_msg']=$this->input->post('whsid');
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords_whsid');
        }
        $config['base_url'] = site_url().'inv/sc/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $whsid = $this->session->userdata('keywords_whsid');
		
        $config['total_rows'] = $this->MSc->countPinjaman($keywords);
        $this->pagination->initialize($config);
		
        if($this->session->userdata('whsid')==1)$data['warehouse'] = $this->MInvreport->getWarehouse();
		
        $data['results'] = $this->MSc->searchPinjaman($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
		$data['warehouse']=$this->MSc->getDropDownWhs('all');
        $data['page_title'] = 'Stockiest Consignment '.$keywords;
        $this->load->view('stockiest/pinjamanstockiest_index',$data);
    }

    public function approved(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        if(!$this->MMenu->blocked()){
			 if($this->input->post('p_id_ho')){
				 $this->MSc->pinjamanApproved('p_id_ho');
			 }else if($this->input->post('p_id_hub')){
				$this->MSc->pinjamanApproved('p_id_hub');
			 }
			
        }
        redirect('inv/sc/','refresh');
    }


    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
		$data['pray_msg']='Error Awal';
		
        $this->form_validation->set_rules('member_id','Stockiest','required');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('remark','','');
        $this->form_validation->set_rules('whsid','','');
        $this->form_validation->set_rules('total','Total pinjaman','');
        $this->form_validation->set_rules('totalpv','','');
        $this->form_validation->set_rules('kecamatan','','');
        $this->form_validation->set_rules('kelurahan','','');
        $this->form_validation->set_rules('addr','','');
        
		$data['promo_aktif'] = $this->MSc->checkPromo('','aktif','0,','5');
		
		$data['kecamatan']=$this->input->post('kecamatan');
		$data['kelurahan']=$this->input->post('kelurahan');
		$data['kodepos']=$this->input->post('kodepos');
		$data['pray_remark']=$this->input->post('remark');
		$data['kota_id']=$this->input->post('kota_id');
		$data['city']=$this->input->post('city');
		$data['counti']=$this->input->post('counti');
		$data['city']=$this->input->post('city');
		$a=$this->MSc->getWhs($this->input->post('kota_id'));
		foreach ($a as $cit){
			$data['whsid']=$cit['WhId'];
		}
		$counti=10;
		if($this->input->post('action')){
			$data['counti']=$this->input->post('counti')+$this->input->post('rowx');
			//echo $this->input->post('rowx').' Total '.$this->input->post('counti');
			
			for ($i = 0; $i <= ($data['counti']-1); $i++) {
				//$this->form_validation->set_rules('itemcode'.$i,'','');
				//$this->form_validation->set_rules('itemname'.$i,'','');
				//$this->form_validation->set_rules('qty'.$i,'','');
				
				$this->form_validation->set_rules('qty'.$i,'Total repeat order','required|callback__check_stock');
				
				//$this->form_validation->set_rules('price'.$i,'','');
				//$this->form_validation->set_rules('subtotal'.$i,'','');
				//$this->form_validation->set_rules('pv'.$i,'','');
				//$this->form_validation->set_rules('subtotalpv'.$i,'','');
				//$this->form_validation->set_rules('bv'.$i,'','');
				//$this->form_validation->set_rules('subtotalbv'.$i,'','');
				
				//$this->form_validation->set_rules('rpdiskon'.$i,'','');
				//$this->form_validation->set_rules('subrpdiskon'.$i,'','');
				
				//$readystok=$this->MRo->cekStok($this->input->post('det_whsid'.$i), $this->input->post('itemname'.$i), $price[$i], $this->input->post('qty'.$i), 0);
				
				$xTotal=$xTotal+str_replace(".","",$this->input->post('subtotal'.$i));
				$xTotalPv=$xTotalPv+str_replace(".","",$this->input->post('subtotalpv'.$i));
				
			}
			
		}
		$data['total']=number_format($xTotal,0);
		$data['totalpv']=number_format($xTotalPv,0);
		$this->form_validation->set_rules('total','Total repeat order','required|callback__check_stock');		
		
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
				if ($this->input->post('cmdSimpan')){
					/* Validasi Lapis Akhir, Kalau Validasi Yang di atas Masih Lolos dan Error Juga,,, */
					if (empty($this->input->post('kecamatan'))){
						$data['pray_msg']='* Kecamatan Harus Di Isi';
					}else if (empty($this->input->post('kelurahan'))){
						$data['pray_msg']='* Kelurahan Harus Di Isi';
					}else if (empty($this->input->post('pic_name'))){
						$data['pray_msg']='* Nama PIC Harus Di Isi';
					}else if (empty($this->input->post('pic_hp'))){
						$data['pray_msg']='* No HP PIC Harus Di Isi';
					}else if (empty($this->input->post('addr'))){
						$data['pray_msg']='* Alamat Harus Di Isi';
					}else if (empty($this->input->post('itemcode0'))){
						$data['pray_msg']='* Paling tidak 1 item harus Di Isi';
					}else{
					
						$this->MSc->addPinjaman();
						redirect('inv/sc','refresh');
						$this->session->set_flashdata('message','Pembuatan Stockiest Consignment Berhasil');
						$data['pray_msg'] = 'Stockiest Consignment Telah Tersimpan';
					}
				}else{
					$data['pray_msg'] = '* Mohon Lengkapi data';
				}
			}else{
				$data['pray_msg'] = '* Blocked';
			}
		}else{
			$data['pray_msg'] = '* Mohon lengkapi data';
		}
        $data['page_title'] = 'Create Stockiest Consignment';
        $this->load->view('stockiest/pinjamanstockiest_form',$data);
    }
    
    public function _check_min(){
        $amount = str_replace(".","",$this->input->post('total'));
		$qtya = str_replace(".","",$this->input->post('qty0'));
        if($amount <= 0 AND $qtya <= 0){
            $this->form_validation->set_message('_check_min','browse product...');
            return false;
        }
	  	//Updated by Boby (2009-11-18)
				else{
					$flag = 0;
					if($this->input->post('itemcode0')){$item[$flag]=$this->input->post('itemcode0'); $qty[$flag]=str_replace(".","",$this->input->post('qty0')); $itemname[$flag]=$this->input->post('itemname0');	$prc[$flag]=str_replace(".","",$this->input->post('price0'));	$flag+=1;}
					if($this->input->post('itemcode1')){$item[$flag]=$this->input->post('itemcode1'); $qty[$flag]=str_replace(".","",$this->input->post('qty1')); $itemname[$flag]=$this->input->post('itemname1');	$prc[$flag]=str_replace(".","",$this->input->post('price1'));	$flag+=1;}
					if($this->input->post('itemcode2')){$item[$flag]=$this->input->post('itemcode2'); $qty[$flag]=str_replace(".","",$this->input->post('qty2')); $itemname[$flag]=$this->input->post('itemname2');	$prc[$flag]=str_replace(".","",$this->input->post('price2'));	$flag+=1;}
					if($this->input->post('itemcode3')){$item[$flag]=$this->input->post('itemcode3'); $qty[$flag]=str_replace(".","",$this->input->post('qty3')); $itemname[$flag]=$this->input->post('itemname3');	$prc[$flag]=str_replace(".","",$this->input->post('price3'));	$flag+=1;}
					if($this->input->post('itemcode4')){$item[$flag]=$this->input->post('itemcode4'); $qty[$flag]=str_replace(".","",$this->input->post('qty4')); $itemname[$flag]=$this->input->post('itemname4');	$prc[$flag]=str_replace(".","",$this->input->post('price4'));	$flag+=1;}

					if($this->input->post('itemcode5')){$item[$flag]=$this->input->post('itemcode5'); $qty[$flag]=str_replace(".","",$this->input->post('qty5')); $itemname[$flag]=$this->input->post('itemname5');	$prc[$flag]=str_replace(".","",$this->input->post('price5'));	$flag+=1;}
					if($this->input->post('itemcode6')){$item[$flag]=$this->input->post('itemcode6'); $qty[$flag]=str_replace(".","",$this->input->post('qty6')); $itemname[$flag]=$this->input->post('itemname6');	$prc[$flag]=str_replace(".","",$this->input->post('price6'));	$flag+=1;}
					if($this->input->post('itemcode7')){$item[$flag]=$this->input->post('itemcode7'); $qty[$flag]=str_replace(".","",$this->input->post('qty7')); $itemname[$flag]=$this->input->post('itemname7');	$prc[$flag]=str_replace(".","",$this->input->post('price7'));	$flag+=1;}
					if($this->input->post('itemcode8')){$item[$flag]=$this->input->post('itemcode8'); $qty[$flag]=str_replace(".","",$this->input->post('qty8')); $itemname[$flag]=$this->input->post('itemname8');	$prc[$flag]=str_replace(".","",$this->input->post('price8'));	$flag+=1;}
					if($this->input->post('itemcode9')){$item[$flag]=$this->input->post('itemcode9'); $qty[$flag]=str_replace(".","",$this->input->post('qty9')); $itemname[$flag]=$this->input->post('itemname9');	$prc[$flag]=str_replace(".","",$this->input->post('price9'));	$flag+=1;}

					$temp = 0;$twin=0;
					//$whsid = $this->session->userdata('whsid');
					$whsid = $this->input->post('whsid');
					
					for($i=0;$i<=$flag;$i++){
						if($temp>0){
							for($j=0;$j<$temp;$j++){
								if($brg[$j]==$item[$i]){//jika sama
									$twin = 1;
									$jml[$j]+=$qty[$i];
									//echo $qty[$i]."<br>";
								}
							}
						}			
						if($twin==0){//jika tidak ada yang sama
							$jml[$temp]=$qty[$i];
							$brg[$temp]=$item[$i];
							$price[$temp]=$prc[$i];
							$nama[$temp]=$itemname[$i];
							$temp+=1;
						}else{$twin=0;}
					}
					for($i=0;$i<count($jml)-1;$i++){
						//echo "<br><br>".$brg[$i]." = ".$jml[$i]." ".$nama[$i]." ";
						$readystok=$this->MRo->cekStok($whsid, $brg[$i], $price[$i], $jml[$i], 0);
						if($readystok!='Ready'){$twin=1;$stok="We only have ".$readystok." ".$nama[$i];}
						//echo "Stok = ".$readystok."<br>";
					}
					if($twin > 0){
						$this->form_validation->set_message('_check_min','Out of stockiest ( '.$stok.' )');
						return false;
					}
				}
				//end Updated by Boby (2009-11-18)
        return true;
    }
    
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MSc->getPinjaman($id);
        
		
		
        if(!count($row)){
            redirect('inv/sc','refresh');
        }
		$data['scid'] = $id;
        $data['row'] = $row;
        $data['items'] = $this->MSc->getPinjamanDetail($id);
        $data['items_get_wh'] = $this->MSc->getlist_wh($id);
        $data['items_wh'] = $this->MSc->getPinjamanDetail_Warehouse($id);
 		$data['items_free'] = $this->MSc->search_FreeItem($id,"","");
		//print_r($data['items_free']);
        $data['page_title'] = 'View Stockiest Consignment';
        $this->load->view('stockiest/pinjamanstockiest_view',$data);
    }
	
    public function _check_delivery(){
        if($this->input->post('pu')==1){
			if(strlen($this->input->post('pic_name'))<1 OR strlen($this->input->post('pic_hp'))<1 OR strlen($this->input->post('city'))<1 OR strlen($this->input->post('addr'))<1 OR strlen($this->input->post('kelurahan'))<1 OR strlen($this->input->post('kecamatan'))<1 OR strlen($this->input->post('kodepos'))<5){
				$this->form_validation->set_message('_check_delivery','Please correctly complete the delivery address.');
				return false;
			}
        }
        return true;
    }
	
	public function payment(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'inv/sc/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']);
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MSc->countSc($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MSc->searchSc($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MSc->countSc($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MSc->searchSc($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Stockiest Consignment Payment';
        $this->load->view('inv/sc_new_index_payment',$data);
    }
	
	public function payment_view(){
		$this->load->library(array('form_validation','messages'));
		
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
		
		$memId='';
        $row = $this->MSc->searchSc($this->uri->segment(4),$config['per_page'],$this->uri->segment(4));
        foreach ($row as $val){
			$memId=$val['member_id'];
		}
		//echo $memId;
		$rowMember = $this->MSc->searchMember($memId);
        foreach ($rowMember as $valMember){
			$data['nama']=$valMember['nama'];
			$data['kota']=$valMember['kota'];
			$data['propinsi']=$valMember['propinsi'];
		}
		
		$data['row'] = $row;
		$data['pay_id'] = $this->uri->segment(4);
        $data['items'] = $this->MSc->searchSc_item($this->uri->segment(4));
        $data['deliver'] = $this->MSc->searchMemberDeliver($memId);
		$data['pray_msg']='* Required Field';
		$data['pray_validation']='';
		/** Form Validation **/
		
		$data['kota_id']=$this->input->post('kota_id');
		$data['timur']=$this->input->post('timur');
		$data['pic_kecamatan']=$this->input->post('pic_kecamatan');
		$data['pic_lurah']=$this->input->post('pic_lurah');
		$data['propinsi']=$this->input->post('propinsi');
		$data['pic_kecamatan']=$this->input->post('pic_kecamatan');
		$data['pic_portal']=$this->input->post('pic_portal');
		$data['addr']=$this->input->post('addr');
		
		if($this->form_validation->run() == true){
			$this->form_validation->set_rules('kota_id','','');
			$this->form_validation->set_rules('timur','','');
			$this->form_validation->set_rules('deli_ad','','');
			$this->form_validation->set_rules('addr','','');
			$this->form_validation->set_rules('addr1','','');
			
			/*
			$data['pray_validation'] = array(
				'kota_id' => $this->input->post('kota_id'),
				'timur' => $this->input->post('timur'),
				'deli_ad' => $this->input->post('deli_ad'),
				'addr' => $this->input->post('addr'),
				'created' => date("Y-m-d H:i:s"),
				'createdby'        =>  $this->session->userdata('user')
			);
			*/

			$data['pray_msg']='Insert Success';
		}else{
			$data['pray_msg']='* Required Field';
		}
        
        $data['page_title'] = 'Stockiest Consignment';
        $this->load->view('stockiest/sc_new_index_payment_view',$data);
	}
	
	
	public function _check_min_order($counti){
		$key=0;
		while($key < count($counti)){
			$qty = str_replace(".","",$this->input->post('qty'.$key));
		    $itemid = $this->input->post('itemcode'.$key);
			$getMinOrder = $this->Mruleorder->getRuleOrder($itemid);
			$getItemName = $this->Mruleorder->getItemName($itemid);
			if($itemid and $qty > 0){
				if($qty%$getMinOrder <> 0){
					$this->form_validation->set_message('_check_min_order',"Pembelian ".$itemid."-".$getItemName." harus kelipatan ".$getMinOrder." .");
					return false;
				}
			}
		    $key++;
		}
	}
	
	public function chstock(){
		/** Test Untuk Ajax **/
		$whsid = $this->input->post('whsid');//1;
		$memberid = $this->input->post('member_id');
		$amount = str_replace(".","",$this->input->post('total'));
		
		if (empty($whsid)){
			echo 'Warehouse Masih Kosong';
		}else if(empty($memberid)){
			echo 'Member Tidak Tersedia';
		}else if(empty($amount)){
			echo 'Jumlah Masih Kosong';
		}else{
			if($amount <= 0){
				echo 'Jumlah Masih Kosong';
			}else{
				if(!validation_errors()){
					$data = array(
						'stockiest_id'=> $whsid,
						'member_id'=> $memberid
					);
					$this->db->insert('so_check',$data);
					
					$id = $this->db->insert_id();
					
					$key=0;
					$whsid = $this->input->post('whsid');//1;
					while($key < count($_POST['counti'])){
						$qty = str_replace(".","",$this->input->post('qty'.$key));
						$price = str_replace(".","",$this->input->post('price'.$key));
						$itemid = $this->input->post('itemcode'.$key);
						$whsiddet = $this->input->post('det_whsid'.$key);
						if($itemid and $qty > 0){
							$data=array(
								'so_id' => $id,
								'item_id' => $itemid,
								'qty' => $qty,
								'price' => $price,
								'warehouse_id' => $whsiddet
							);
							$this->db->insert('so_check_d',$data);
							
							//echo $id.' * '.$itemid.' * '.$price.' * '.$titipan_id.' * '.$stcid;
							
							//$rs=$this->GLobal_model->check_stock_whs($id,$itemid,$price,$whsid);
							$rs=$this->GLobal_model->check_stock_whs($id,$itemid,$price,$whsiddet);
							if(!$rs->stock){
								$this->form_validation->set_message('_check_stock',"You don't have ".$itemid);
								
								$this->db->delete('so_check',array('id'=>$id));
								$this->db->delete('so_check_d',array('so_id'=>$id));
								return false;
							}elseif($rs->stock < $rs->qty){
								$this->form_validation->set_message('_check_stock',"We only have ".$rs->stock." ".$rs->name);
								
								$this->db->delete('so_check',array('id'=>$id));
								$this->db->delete('so_check_d',array('so_id'=>$id));
								return false;
							}
						}
						$key++;
					}
					
					// START ASP 20190108
					//echo $id;
					$getCheckFreeStock = $this->GLobal_model->getFreeStock($id);
					foreach($getCheckFreeStock as $checkFreeStockKey => $rowFreeStock):
						$rsFree=$this->GLobal_model->checkFreeStock($rowFreeStock['free_item_id'],$rowFreeStock['whs_id']);
						if(!$rsFree->stock){
							$this->form_validation->set_message('_check_stock',"You don't have free item ".$rowFreeStock['free_item_id']." - ".$rowFreeStock['free_item_name']);
							
							$this->db->delete('so_check',array('id'=>$id));
							$this->db->delete('so_check_d',array('so_id'=>$id));
							return false;
						}elseif($rsFree->stock < $rowFreeStock['free_qty']){
							$this->form_validation->set_message('_check_stock',"We only have ".$rs->stock." of free item ".$rowFreeStock['free_item_id']." - ".$rowFreeStock['free_item_name']);
							
							$this->db->delete('so_check',array('id'=>$id));
							$this->db->delete('so_check_d',array('so_id'=>$id));
							return false;
						}
					endforeach;
					// END ASP 20190108
					
					$this->db->delete('so_check',array('id'=>$id));
					$this->db->delete('so_check_d',array('so_id'=>$id));
				}else{
					echo 'Gagal Melakukan Validasi Data';
				}
			}
			
				
		}
		/** End Test Ajax Function**/
	}
	
	public function _check_stock(){
		$whsid = $this->input->post('whsid');//1;
		$memberid = $this->input->post('member_id');
		//echo 'aaaaaa';
		$amount = str_replace(".","",$this->input->post('total'));
		if($amount <= 0){
			$this->form_validation->set_message('_check_stock','browse product...');
			return false;
		}else{
			if(!validation_errors()){
				$data = array(
					'stockiest_id'=> $whsid,
					'member_id'=> $memberid
				);
				$this->db->insert('so_check',$data);
				
				$id = $this->db->insert_id();
				
				$key=0;
				$whsid = $this->input->post('whsid');//1;
				while($key < count($_POST['counti'])){
					$qty = str_replace(".","",$this->input->post('qty'.$key));
					$price = str_replace(".","",$this->input->post('price'.$key));
					$itemid = $this->input->post('itemcode'.$key);
					$whsiddet = $this->input->post('det_whsid'.$key);
					if($itemid and $qty > 0){
						$data=array(
							'so_id' => $id,
							'item_id' => $itemid,
							'qty' => $qty,
							'price' => $price,
							'warehouse_id' => $whsiddet
						);
						$this->db->insert('so_check_d',$data);
						
						//echo $id.' * '.$itemid.' * '.$price.' * '.$titipan_id.' * '.$stcid;
						
						//$rs=$this->GLobal_model->check_stock_whs($id,$itemid,$price,$whsid);
						$rs=$this->GLobal_model->check_stock_whs($id,$itemid,$price,$whsiddet);
						if(!$rs->stock){
							$this->form_validation->set_message('_check_stock',"You don't have ".$itemid);
							
							$this->db->delete('so_check',array('id'=>$id));
							$this->db->delete('so_check_d',array('so_id'=>$id));
							return false;
						}elseif($rs->stock < $rs->qty){
							$this->form_validation->set_message('_check_stock',"We only have ".$rs->stock." ".$rs->name);
							
							$this->db->delete('so_check',array('id'=>$id));
							$this->db->delete('so_check_d',array('so_id'=>$id));
							return false;
						}
					}
					$key++;
				}
				
				// START ASP 20190108
				//echo $id;
				$getCheckFreeStock = $this->GLobal_model->getFreeStock($id);
				foreach($getCheckFreeStock as $checkFreeStockKey => $rowFreeStock):
					$rsFree=$this->GLobal_model->checkFreeStock($rowFreeStock['free_item_id'],$rowFreeStock['whs_id']);
					if(!$rsFree->stock){
						$this->form_validation->set_message('_check_stock',"You don't have free item ".$rowFreeStock['free_item_id']." - ".$rowFreeStock['free_item_name']);
						
						$this->db->delete('so_check',array('id'=>$id));
						$this->db->delete('so_check_d',array('so_id'=>$id));
						return false;
					}elseif($rsFree->stock < $rowFreeStock['free_qty']){
						$this->form_validation->set_message('_check_stock',"We only have ".$rsFree->stock." of free item ".$rowFreeStock['free_item_id']." - ".$rowFreeStock['free_item_name']);
						
						$this->db->delete('so_check',array('id'=>$id));
						$this->db->delete('so_check_d',array('so_id'=>$id));
						return false;
					}
				endforeach;
				// END ASP 20190108
				
				$this->db->delete('so_check',array('id'=>$id));
				$this->db->delete('so_check_d',array('so_id'=>$id));
			}
        }
        return true;
    }
	
}
?>