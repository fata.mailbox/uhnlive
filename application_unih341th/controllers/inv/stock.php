<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stock extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MInvreport','MWhs_rusak'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('status','Status','callback__check_stockcard');
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        $this->form_validation->set_rules('itemcode','','');
        $this->form_validation->set_rules('itemname','','');
        $this->form_validation->set_rules('pilihan','','');
        
        if($this->form_validation->run()){
            if($this->input->post('status') == 'qoh' && $this->input->post('pilihan') == 'baik'){
                 
				if($this->input->post('reptype') == '1')
					$data['results'] = $this->MInvreport->getQOHperProd($this->input->post('whsid'));
                else
					$data['results'] = $this->MInvreport->getQOH($this->input->post('whsid'));  
            }elseif($this->input->post('status') == 'qoh' && $this->input->post('pilihan') == 'rusak'){
                if($this->input->post('reptype') == '1')
                    $data['results'] = $this->MInvreport->getQOHperProdRusak($this->input->post('whrsk'));    
                else 
                    $data['results'] = $this->MInvreport->getQOHrusak($this->input->post('whrsk'));    
            }elseif($this->input->post('status') == 'ss' && $this->input->post('pilihan') == 'baik'){
                if ($this->input->post('wshid') == 'all') 
                $data['results'] = $this->MInvreport->getStockSummaryAll($this->input->post('fromdate'),$this->input->post('todate'));
                else    
                $data['results'] = $this->MInvreport->getStockSummary($this->input->post('whsid'),$this->input->post('fromdate'),$this->input->post('todate'));    
            }elseif($this->input->post('status') == 'ss' && $this->input->post('pilihan') == 'rusak'){
                $data['results'] = $this->MInvreport->getStockSummaryRusak($this->input->post('whrsk'),$this->input->post('fromdate'),$this->input->post('todate'));    
            }elseif($this->input->post('status') == 'sc' && $this->input->post('pilihan') == 'baik'){
                $data['results'] = $this->MInvreport->getStockCard($this->input->post('whsid'),$this->input->post('itemcode'),$this->input->post('fromdate'),$this->input->post('todate')); 
            }elseif($this->input->post('status') == 'sc' && $this->input->post('pilihan') == 'rusak'){
                $data['results'] = $this->MInvreport->getStockCard_rusak($this->input->post('whrsk'),$this->input->post('itemcode'),$this->input->post('fromdate'),$this->input->post('todate'));    
            }            
        }else{
            $data['results'] = false;
        }
        
        $data['reportDate'] = date("Y-m-d");
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        if($this->session->userdata('whsid')==1)

        $data['warehouse'] = $this->MInvreport->getWarehouseAll('all');
        $data['rusak'] = $this->MWhs_rusak->getDropDownWhsAll('all');
		$data['reptype'] = ['0'=> 'Per SKU','1'=>'Per Product'];
        $data['page_title'] = 'Stock Report';
        $data['wh_all'] = $this->MInvreport->getWh();
        $data['whr_all'] = $this->MWhs_rusak->getWhrusak();
        $this->load->view('inv/stockreport_index',$data);
    }

    
    public function _check_stockcard(){
        if($this->input->post('status') == 'sc' && !$this->input->post('itemcode')){
            $this->form_validation->set_message('_check_stockcard','browse product...');
            return false;
        }
        return true;
    }
      
}
?>