<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Item_inactive extends CI_Controller {
    function __construct()
    {
    parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','Minactive'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'inv/item_inactive/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->Minactive->countInactive($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->Minactive->searchInactive($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->Minactive->countInactive($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->Minactive->searchInactive($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Product Inactive';
        $this->load->view('inv/item_inactive_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('itemcode','','');
        //$this->form_validation->set_rules('itemname','','');
        $this->form_validation->set_rules('exp_date','','');
        
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->Minactive->addInactive();
                $this->session->set_flashdata('message','Create Product Inactive Successfully');
            }
            redirect('inv/item_inactive','refresh');
        }
        $data['reportDate'] = date("Y-m-d");
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'Create Product Inactive';
        $this->load->view('inv/item_inactive_create',$data);
    }
        
    public function edit($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        $row = $this->Minactive->getInactive($id);

        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('status','Status','required');
        $this->form_validation->set_rules('exp_date','Exp Date','required');
                     
       
        if($this->form_validation->run() == TRUE){
                $this->Minactive->editInactive($id);
                redirect('inv/item_inactive','refresh');           
        }


        /* if(!count($row)){
            redirect('inv/item_inactive','refresh');
        }*/
        $data['reportDate'] = date("Y-m-d");
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['row'] = $row;
        $data['page_title'] = 'Edit Status Inactive';
        $this->load->view('inv/item_inactive_edit',$data);
    }
    
}
?>