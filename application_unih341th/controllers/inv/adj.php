<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Adj extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MInv', 'MInvreport'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        $this->form_validation->set_rules('whsid','','');
        
        $config['base_url'] = site_url().'inv/adj/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $this->session->set_userdata('keywords_whsid',$this->db->escape_str($this->input->post('whsid')));
        	$whsid = $this->session->userdata('keywords_whsid');
			
            $config['total_rows'] = $this->MInv->countAdjustment($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MInv->searchAdjustment($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            $keywords = $this->session->userdata('keywords');      
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords_whsid');
        	$whsid = $this->session->userdata('keywords_whsid');
			
            $config['total_rows'] = $this->MInv->countAdjustment($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MInv->searchAdjustment($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
        if($this->session->userdata('whsid')==1)$data['warehouse'] = $this->MInvreport->getWarehouse();
		$data['warehouse']=$this->MInv->getDropDownWhsAll('all');
        $data['page_title'] = 'Adjustment Stock';
        $this->load->view('inv/adjustment_index',$data);
    }
	
    public function approved(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        if(!$this->MMenu->blocked()){
            $this->MInv->adjustmentApproved();
        }
        redirect('inv/adj/','refresh');
    }
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('warehouse_id','','');
        $this->form_validation->set_rules('flag','','');
        $this->form_validation->set_rules('password1','Password','required|callback__check_password');
        $this->form_validation->set_rules('remark','','');
        
        $this->form_validation->set_rules('itemcode0','','');
        $this->form_validation->set_rules('itemname0','','');
        $this->form_validation->set_rules('qty0','','');
        
        $this->form_validation->set_rules('itemcode1','','');
        $this->form_validation->set_rules('itemname1','','');
        $this->form_validation->set_rules('qty1','','');
        
        $this->form_validation->set_rules('itemcode2','','');
        $this->form_validation->set_rules('itemname2','','');
        $this->form_validation->set_rules('qty2','','');
        
        $this->form_validation->set_rules('itemcode3','','');
        $this->form_validation->set_rules('itemname3','','');
        $this->form_validation->set_rules('qty3','','');
        
        $this->form_validation->set_rules('itemcode4','','');
        $this->form_validation->set_rules('itemname4','','');
        $this->form_validation->set_rules('qty4','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MInv->addAdjustment();
                $this->session->set_flashdata('message','Adjustment Stock successfully');
            }
            redirect('inv/adj','refresh');
        }
        $data['warehouse'] = $this->MInv->getDropDownWhs();
        $data['page_title'] = 'Create Adjustment Stock';
        $this->load->view('inv/adjustment_form',$data);
    }
    public function _check_password(){
        $this->load->model('MAuth');
        if(!$this->MAuth->check_password($this->session->userdata('user'),$this->input->post('password1'))){
            $this->form_validation->set_message('_check_password','Sorry, your password invalid');
            return false;
        }
        return true;
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MInv->getAdjustment($id);
        
        if(!count($row)){
            redirect('inv/adj','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MInv->getAdjustmentDetail($id);
        $data['page_title'] = 'View Adjustment Stock';
        $this->load->view('inv/adjustment_view',$data);
    }
    
}
?>