<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
    Annisa Rahmawaty 2018

*/
    
class Wh_rusak_mov extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MWhs_rusak'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        $this->form_validation->set_rules('wrsk','','');
        
        $config['base_url'] = site_url().'inv/Wh_rusak_mov/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $this->session->set_userdata('keywords_wrsk',$this->db->escape_str($this->input->post('wrsk')));
            $wrsk = $this->session->userdata('keywords_wrsk');

            $config['total_rows'] = $this->MWhs_rusak->countRusak($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MWhs_rusak->searchRusak($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');           
            $keywords = $this->session->userdata('keywords');      
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords_wrsk');
            $wrsk = $this->session->userdata('keywords_wrsk');

            $config['total_rows'] = $this->MWhs_rusak->countRusak($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MWhs_rusak->searchRusak($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        }
        if($this->session->userdata('wrsk')==1)$data['warehouse'] = $this->MWhs_rusak->getMrusak();
        $data['warehouse'] = $this->MWhs_rusak->getDropDownWhsAll('all');
        $data['page_title'] = 'Warehouse Rusak Movement';
        $this->load->view('inv/Wh_rusak_transaksi_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('id','','');
        $this->form_validation->set_rules('password1','Password','required|callback__check_password');
        $this->form_validation->set_rules('id_whr','','');
        $this->form_validation->set_rules('remark','','');
        
        for($i=0;$i<=9;$i++){
        $this->form_validation->set_rules('itemcode'.$i,'','');
        $this->form_validation->set_rules('itemname'.$i,'','');
        $this->form_validation->set_rules('maks'.$i,'','');
        $this->form_validation->set_rules('qty'.$i,'','callback__check_qty');
        $this->form_validation->set_rules('price'.$i,'','');
        $this->form_validation->set_rules('tprice'.$i,'','');
        }
        
		$this->form_validation->set_rules('totalprice','','');

        $data['warehouse'] = $this->MWhs_rusak->getMrusak();
		
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MWhs_rusak->addRusak();
                $this->session->set_flashdata('message','Warehouse Stock Rusak Movement Successfully');
            }
            redirect('inv/Wh_rusak_mov','refresh');
        }
        
        $data['warehouse'] = $this->MWhs_rusak->getMrusak();
        $data['page_title'] = 'Input Barang Rusak';
        $this->load->view('inv/Wh_rusak_transaksi_form',$data);
    }
    
    public function _check_password(){
        $this->load->model('MAuth');
        if(!$this->MAuth->check_password($this->session->userdata('user'),$this->input->post('password1'))){
            $this->form_validation->set_message('_check_password','Sorry, your password invalid');
            return false;
        }
        return true;
    }

    public function _check_qty(){
        if($this->input->post('cek')=="Gagal"){
            $this->form_validation->set_message('_check_qty','Qty tidak cukup');
            return false;
        }
        return true;
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MWhs_rusak->getRusakId($id);
        
        if(!count($row)){
            redirect('inv/Wh_rusak_mov','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MWhs_rusak->getRusakDetail($id);
        $data['page_title'] = 'View Warehouse Stock Movement';
        $this->load->view('inv/t_rusak_v',$data);
    }
    
    public function approved(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        if(!$this->MMenu->blocked()){
            $this->MWhs_rusak->WRApproved();
        }
        redirect('inv/Wh_rusak_mov/','refresh');
    }

}
?>