<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mtsapp extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MMutasi'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'inv/mtsapp/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MMutasi->countMutasiApp($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MMutasi->searchMutasiApp($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MMutasi->countMutasiApp($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MMutasi->searchMutasiApp($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        }
        
        //$data['page_title'] = 'Stock Movement Approval';
        $data['page_title'] = 'Receive Warehouse Movement Stock';
        $this->load->view('inv/mutasistock_app_index',$data);
    }
    
    public function app($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('remark','remark','callback__check_app');
        $this->form_validation->set_rules('counter','','');
        $this->form_validation->set_rules('totalprice_act','','');
        $this->form_validation->set_rules('totalprice_hilrus','','');
        
		for($i=0;$i<=$this->input->post('counter');$i++){
			$this->form_validation->set_rules('detail_id'.$i,'','');
			$this->form_validation->set_rules('itemcode'.$i,'','');
			$this->form_validation->set_rules('qtyOri'.$i,'','');
			
			$this->form_validation->set_rules('qty'.$i,'','');
			$this->form_validation->set_rules('price'.$i,'','');
			$this->form_validation->set_rules('tprice'.$i,'','');
			
			$this->form_validation->set_rules('rqty'.$i,'','');
			$this->form_validation->set_rules('rprice'.$i,'','');
			$this->form_validation->set_rules('rtprice'.$i,'','');
		}
		
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MMutasi->appMutasi();
                $this->session->set_flashdata('message','Stock had been Received successfully');
            }
            redirect('inv/mtsapp','refresh');
        }
        
        $row =array();
        $row = $this->MMutasi->getMutasiApp($id);
        
        if(!count($row)){
            redirect('inv/mtsapp','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MMutasi->getMutasiDetail($id);
        $data['page_title'] = 'Stock Movement Approval';
        //$this->load->view('inv/mutasistock_app_form',$data);
        $this->load->view('inv/mutasistock_app_form_new',$data);
    }
    
    public function _check_app(){
        if($this->MMutasi->check_approved($this->input->post('id'))){
            $this->form_validation->set_message('_check_app','Sorry, your receive had been cancelled');
            return false;
        }
        return true;
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MMutasi->getMutasiApp($id);
        
        if(!count($row)){
            redirect('inv/mtsapp','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MMutasi->getMutasiDetail($id);
        $data['page_title'] = 'View Stock Movement';
        $this->load->view('inv/mutasistock_app_view',$data);
    }
    
}
?>