<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rowhtrf extends CI_Controller {
    function __construct()
    {
		parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MInv', 'MInvreport', 'MRowhtrf'));
    }

    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        $this->form_validation->set_rules('whsid','','');
        
        $config['base_url'] = site_url().'inv/rowhtrf/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
		
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $this->session->set_userdata('keywords_whsid',$this->db->escape_str($this->input->post('whsid')));
        	$whsid = $this->session->userdata('keywords_whsid');
			
            $config['total_rows'] = $this->MRowhtrf->countRowhtrf($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MRowhtrf->searchRowhtrf($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            $keywords = $this->session->userdata('keywords');      
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords_whsid');
        	$whsid = $this->session->userdata('keywords_whsid');
			
            $config['total_rows'] = $this->MRowhtrf->countRowhtrf($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MRowhtrf->searchRowhtrf($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
		
        if($this->session->userdata('whsid')==1)$data['warehouse'] = $this->MInvreport->getWarehouse();
		$data['warehouse']=$this->MInv->getDropDownWhsAll('all');
        $data['page_title'] = 'RO Warehouse Transfer';
        $this->load->view('inv/rowhtrf_table',$data);
	}
	
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('ro_id','','');
        $this->form_validation->set_rules('whs1_name','','');
        $this->form_validation->set_rules('whs1','','');
        $this->form_validation->set_rules('whs2','','');
        $this->form_validation->set_rules('remark','','');
        $this->form_validation->set_rules('bns','','');
        
        $data['cekdata'] = FALSE;
        if($this->form_validation->run()){
			if($this->input->post('submit') == 'Cek Data'){
				$cekData = $this->MRowhtrf->cekDataRO($this->input->post('ro_id'));
				if($cekData > 0){
					$cekMultiWh = $this->MRowhtrf->cekWhRO($this->input->post('ro_id'));
					if($cekMultiWh == 1) {
						$data['getData'] = $this->MRowhtrf->getDataRO($this->input->post('ro_id'));
						$data['getDataDetail'] = $this->MRowhtrf->getDataRODetail($this->input->post('ro_id'), $this->input->post('whs2'));
						$data['getDataBns'] = $this->MRowhtrf->getFree($this->input->post('ro_id'), $this->input->post('whs2'));
	
						$data['cekdata'] = TRUE;
					}else{
						$this->session->set_flashdata('message','RO Multi-Warehouse Ordering Cannot be Transferred');
					}
				}else{
					$this->session->set_flashdata('message','RO Cannot be found');
				}
				
			}else if($this->input->post('submit') == 'Submit'){ 
				if(!$this->MMenu->blocked()){
					$this->MRowhtrf->addRowhtrf();
					$this->session->set_flashdata('message','RO Warehouse Transfer Created successfully');
				}
				redirect('inv/rowhtrf','refresh');
			}
        }
        $data['warehouse'] = $this->MInv->getDropDownWhs();
        $data['page_title'] = 'Create RO Warehouse Transfer';
        $this->load->view('inv/rowhtrf_form',$data);
    }
}
?>