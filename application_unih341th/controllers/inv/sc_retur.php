<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sc_Retur  extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MSc_retur','MSc'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'inv/sc_retur/index/';
        $config['per_page'] = 40;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MSc_retur->countReturPinjaman($keywords);
        //echo $config['total_rows'];
        $this->pagination->initialize($config);
        $data['results'] = $this->MSc_retur->searchReturPinjaman($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Retur Stockiest Consignment';
        $this->load->view('stockiest/returpinjaman_index',$data);
    }
    
    public function create_old(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('member_id','Member','required');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('ewallet','','');
        $this->form_validation->set_rules('remark','','');
        $this->form_validation->set_rules('warehouse_id','','');
        $this->form_validation->set_rules('total','Total sales order','required|callback__check_min');
        $this->form_validation->set_rules('totalpv','','');
        
        $this->form_validation->set_rules('itemcode0','','');
        $this->form_validation->set_rules('itemname0','','');
        $this->form_validation->set_rules('qty0','','');
        $this->form_validation->set_rules('price0','','');
        $this->form_validation->set_rules('subtotal0','','');
        $this->form_validation->set_rules('pv0','','');
        $this->form_validation->set_rules('subtotalpv0','','');
        
        $this->form_validation->set_rules('itemcode1','','');
        $this->form_validation->set_rules('itemname1','','');
        $this->form_validation->set_rules('qty1','','');
        $this->form_validation->set_rules('price1','','');
        $this->form_validation->set_rules('subtotal1','','');
        $this->form_validation->set_rules('pv1','','');
        $this->form_validation->set_rules('subtotalpv1','','');
        
        $this->form_validation->set_rules('itemcode2','','');
        $this->form_validation->set_rules('itemname2','','');
        $this->form_validation->set_rules('qty2','','');
        $this->form_validation->set_rules('price2','','');
        $this->form_validation->set_rules('subtotal2','','');
        $this->form_validation->set_rules('pv2','','');
        $this->form_validation->set_rules('subtotalpv2','','');
        
        $this->form_validation->set_rules('itemcode3','','');
        $this->form_validation->set_rules('itemname3','','');
        $this->form_validation->set_rules('qty3','','');
        $this->form_validation->set_rules('price3','','');
        $this->form_validation->set_rules('subtotal3','','');
        $this->form_validation->set_rules('pv3','','');
        $this->form_validation->set_rules('subtotalpv3','','');
        
        $this->form_validation->set_rules('itemcode4','','');
        $this->form_validation->set_rules('itemname4','','');
        $this->form_validation->set_rules('qty4','','');
        $this->form_validation->set_rules('price4','','');
        $this->form_validation->set_rules('subtotal4','','');
        $this->form_validation->set_rules('pv4','','');
        $this->form_validation->set_rules('subtotalpv4','','');
        
        $this->form_validation->set_rules('itemcode5','','');
        $this->form_validation->set_rules('itemname5','','');
        $this->form_validation->set_rules('qty5','','');
        $this->form_validation->set_rules('price5','','');
        $this->form_validation->set_rules('subtotal5','','');
        $this->form_validation->set_rules('pv5','','');
        $this->form_validation->set_rules('subtotalpv5','','');
        
        $this->form_validation->set_rules('itemcode6','','');
        $this->form_validation->set_rules('itemname6','','');
        $this->form_validation->set_rules('qty6','','');
        $this->form_validation->set_rules('price6','','');
        $this->form_validation->set_rules('subtotal6','','');
        $this->form_validation->set_rules('pv6','','');
        $this->form_validation->set_rules('subtotalpv6','','');
        
        $this->form_validation->set_rules('itemcode7','','');
        $this->form_validation->set_rules('itemname7','','');
        $this->form_validation->set_rules('qty7','','');
        $this->form_validation->set_rules('price7','','');
        $this->form_validation->set_rules('subtotal7','','');
        $this->form_validation->set_rules('pv7','','');
        $this->form_validation->set_rules('subtotalpv7','','');
        
        $this->form_validation->set_rules('itemcode8','','');
        $this->form_validation->set_rules('itemname8','','');
        $this->form_validation->set_rules('qty8','','');
        $this->form_validation->set_rules('price8','','');
        $this->form_validation->set_rules('subtotal8','','');
        $this->form_validation->set_rules('pv8','','');
        $this->form_validation->set_rules('subtotalpv8','','');
        
        $this->form_validation->set_rules('itemcode9','','');
        $this->form_validation->set_rules('itemname9','','');
        $this->form_validation->set_rules('qty9','','');
        $this->form_validation->set_rules('price9','','');
        $this->form_validation->set_rules('subtotal9','','');
        $this->form_validation->set_rules('pv9','','');
        $this->form_validation->set_rules('subtotalpv9','','');
        
        $buffer = strlen('inv/sc_retur/create/');
        $data['redirect_to'] = substr($this->uri->uri_string(), $buffer);	
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                
                    $soid = $this->MSc_retur->addReturTitipanTemp();
                    $results = $this->MSc_retur->check_retur_pinjaman($soid,$this->input->post('member_id'));
                    if($results == 'ok'){
                        $this->MSc_retur->addReturPinjaman($soid);
                        $this->session->set_flashdata('message','Created Retur Stock Stockiest Successfully');
                        redirect('inv/sc_retur','refresh');        
                    }else{
                        $this->session->set_flashdata('message','Procces Cancel, Stock tidak mencukupi');
                        redirect('inv/sc_retur/create/','refresh');
                    }
            }
            redirect('inv/sc_retur','refresh');
        }
        
        $data['warehouse'] = $this->MSc_retur->getDropDownWhs();
        $data['page_title'] = 'Create Retur Stockiest Consignment';
        $this->load->view('stockiest/returpinjaman_form',$data);
    }
    
    public function _check_min(){
        $amount = str_replace(".","",$this->input->post('total'));
        if($amount < 0){
            $this->form_validation->set_message('_check_min','browse product...');
            return false;
        }
        return true;
    }
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MSc_retur->getReturPinjaman($id);
        
        if(!count($row)){
            redirect('inv/sc_retur','refresh');
        }
        $data['row'] = $row;
		
		$data['items_wh'] = $this->MSc->getPinjamanDetail_Warehouse($id);
 		$data['items_free'] = $this->MSc_retur->search_FreeItem($id,"","");
		
        $data['items'] = $this->MSc_retur->getReturPinjamanDetail($id);
        $data['page_title'] = 'View Retur Stock Stockiest';
        $this->load->view('stockiest/returpinjaman_view',$data);
    }
	
	public function create(){
		$this->del_session();
		
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
		$data['pray_msg']='* Mohon Isikan Data pada isian yang telah di sediakan';
		
		$this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        $this->form_validation->set_rules('whsid','','');
		
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $this->session->set_userdata('keywords_whsid',$this->db->escape_str($this->input->post('whsid')));
        }else{
            if(!$this->uri->segment(4)) $this->session->unset_userdata('keywords');
            if(!$this->uri->segment(4)) $this->session->unset_userdata('keywords_whsid');
        }
		
        $config['base_url'] = site_url().'inv/sc_retur/create';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $whsid = $this->session->userdata('keywords_whsid');
		
        $config['total_rows'] = $this->MSc_retur->countPinjaman_Retur($keywords);
        $this->pagination->initialize($config);
		$data['page_title'] = 'Stockiest Consignment Retur List';
		$data['warehouse']=$this->MSc_retur->getDropDownWhs('all');
        $data['results'] = $this->MSc_retur->searchPinjaman_Retur($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        $this->form_validation->set_rules('member_id_from','Stockiest','required');
        $this->form_validation->set_rules('name_from','','');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('nominal','','');
		$this->form_validation->set_rules('persen','','');
		
		$this->form_validation->set_rules('whsid','','');
		$this->form_validation->set_rules('pic_name1','','');
		$this->form_validation->set_rules('pic_hp1','','');
		$this->form_validation->set_rules('kecamatan1','','');
		$this->form_validation->set_rules('kelurahan1','','');
		$this->form_validation->set_rules('kodepos1','','');
		
		/* Kalau Pakai Metode Post..
		if (!empty($this->input->post('sc_id'))){
			$data['item_results'] = $this->MSc_retur->getPinjamanDetail($this->input->post('sc_id'));
			$data['items_free'] = $this->MSc_retur->search_FreeItem($this->input->post('sc_id'),"","");
		}
		*/
		
		if($this->form_validation->run() == true){
			$this->del_session();
            if(!$this->MMenu->blocked()){
				
				if (empty($this->input->post('sc_id'))){
					$data['pray_msg']='Mohon Pilih Data Stockiest Consignment';
				}else{
					$data['pray_msg']='Redirect ke Retur Stockiest Consignment';

					$this->session->set_userdata(
						array(
							'r_sc_id'=>$this->input->post('sc_id'),
							'r_sc_date'=>$this->input->post('sc_date'),
							'r_stc_id_from'=>$this->input->post('no_stc_from'),
							'r_stc_id_to'=>$this->input->post('no_stc'),
							'r_ewal_to'=>$this->input->post('ewal_to'),
							'r_whs_from'=>$this->input->post('whs_from'),
							'r_nominal'=>$this->input->post('nominal'),
							'r_member_id'=>$this->input->post('member_id'),
							'r_member_nama'=>$this->input->post('member_nama'),
							'r_member_id_from'=>$this->input->post('member_id_from'),
							'r_name_from'=>$this->input->post('name_from'),
							'r_deli_ad'=>$this->input->post('deli_ad'),
							'r_timur'=>$this->input->post('timur'),
							'r_kota_id'=>$this->input->post('kota_id'),
							'r_kota_id1'=>$this->input->post('kota_id1'),
							'r_propinsi'=>$this->input->post('propinsi'),
							'r_city'=>$this->input->post('city'),
							'r_addr1'=>$this->input->post('addr1'),
							'r_whsid'=>$this->input->post('whsid'),
							'r_pic_name'=>$this->input->post('name_from'),
							'r_pic_hp'=>$this->input->post('pic_hp'),
							'r_pic_name1'=>$this->input->post('pic_name1'),
							'r_pic_hp1'=>$this->input->post('pic_hp1'),
							'r_kecamatan1'=>$this->input->post('kecamatan1'),
							'r_kelurahan1'=>$this->input->post('kelurahan1'),
							'r_kodepos1'=>$this->input->post('kodepos1')
						)
					);
					redirect('inv/sc_retur/add','refresh');
					
					
				}
				$this->load->view('stockiest/returpinjaman_form_create',$data);
			}
		}else{
			$this->load->view('stockiest/returpinjaman_form_create',$data);
		}
	
        //$this->load->view('stockiest/returpinjaman_form_create',$data);
	}
	
    public function add(){
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
        if (empty($this->session->userdata('r_sc_id'))){
			redirect('inv/sc_retur','refresh');
		}
		$data['pray_msg']='';
		$this->form_validation->set_rules('member_id','Stockist','required');
		$this->form_validation->set_rules('member_id_to','Required','');
		
		$data['item_results'] = $this->MSc_retur->getPinjamanDetail($this->session->userdata('r_sc_id'));
		$data['items_free'] = $this->MSc_retur->search_FreeItem($this->session->userdata('r_sc_id'),"","");

		$this->form_validation->set_rules('ewallet_to','eWallet','');		
		$this->form_validation->set_rules('sisaTotal','Total Pembayaran SC','');		
		
		if($this->form_validation->run() == true){
			//$this->del_session();
            if(!$this->MMenu->blocked()){
				if($this->input->post('cmSubmit')){
					/* Validasi Lapis Akhir, Kalau Validasi Yang di atas Masih Lolos dan Error Juga,,, */
					
					if (empty($this->input->post('member_id'))){
						$data['pray_msg']='Member Id Masih Kosong';
					
					}else{
											
						$member_id = $this->input->post('member_id');
						$totalharga = str_replace(".","",$this->input->post('total'));
						$totalpv = str_replace(".","",$this->input->post('totalpv'));
						
						$empid = $this->session->userdata('user');
						$whsid = $this->input->post('whsid');
						$sc_id = $this->input->post('sc_id');
					   
						if (empty($this->db->escape_str($this->input->post('remark')))){
							$var_remark="Created By System in ".date('Y-m-d H:i:s');
						}else{
							$var_remark=$this->db->escape_str($this->input->post('remark'));
						}
						
						$data_ibu_asu = array(
							'pinjaman_id' => $sc_id,
							'stockiest_id' => $member_id,
							'tgl' => date('Y-m-d',now()),
							'totalharga' => $totalharga,
							'totalpv' => $totalpv,
							'warehouse_id' => $whsid,
							'remark'=>$var_remark,
							'created'=>date('Y-m-d H:i:s',now()),
							'createdby'=>$empid
						);
						$this->db->insert('retur_titipan_temp',$data_ibu_asu);
						$tempid = $this->db->insert_id();
						
						$this->MSc_retur->addReturTitipanTempDet($tempid);
						$results = $this->MSc_retur->check_retur_pinjaman($tempid,$this->input->post('member_id')); 
						if($results == 'ok'){
							$this->MSc_retur->addReturPinjaman($tempid);
							$this->del_session();
							$this->session->set_flashdata('message','Created Retur Stock Stockiest Successfully');
							redirect('inv/sc_retur','refresh');
							$data['pray_msg']=$results.' Add Retur Stockiest Consignment Successfully';								
						}else{
							$this->session->set_flashdata('message','Procces Cancel, Stock tidak mencukupi');
							redirect('inv/sc_retur/create/','refresh');
							$data['pray_msg']=$results.' Procces Cancel, Stock tidak mencukupi';		
						}
						
					}
					/* End Validasi Lapis Akhir */
				}else{
					$data['pray_msg']='Click Submit Button Please';
				}
				
				
			}
		}
		
		$data['page_title'] = 'Add Retur Stockist Consignment ';
        $this->load->view('stockiest/returpinjaman_form_add',$data);
	
	}
	
	public function no_sc(){
		//echo 'Test POST : '.$this->input->post('search');
        $data['results'] = $this->MSc_retur->getPinjamanDetail($this->uri->segment(4));
        //$data['results'] = $this->MSc_retur->getPinjamanDetail($this->input->post('search'));
        $data['page_title'] = 'Search SC No';
        $this->load->view('stockiest/returpinjaman_scdetail_data',$data);
	}
	
	
    public function del_session(){
	
	$this->session->unset_userdata('r_sc_id');
	$this->session->unset_userdata('r_sc_date');
	$this->session->unset_userdata('r_stc_id_from');
	$this->session->unset_userdata('r_stc_id_to');
	$this->session->unset_userdata('r_whsid_to');
	$this->session->unset_userdata('r_ewal_to');
	$this->session->unset_userdata('r_whs_nm_to');
	$this->session->unset_userdata('r_whs_from');
	$this->session->unset_userdata('r_nominal');
	$this->session->unset_userdata('r_member_nama');
	$this->session->unset_userdata('r_persen_to');
	$this->session->unset_userdata('r_member_id_from');
	$this->session->unset_userdata('r_name_from');
	
	$this->session->unset_userdata('r_member_id');
	$this->session->unset_userdata('r_persen');
	$this->session->unset_userdata('r_pu');
	$this->session->unset_userdata('r_timur');
	$this->session->unset_userdata('r_kota_id');
	$this->session->unset_userdata('r_kota_id1');
	$this->session->unset_userdata('r_addr');
	$this->session->unset_userdata('r_addr1');
	$this->session->unset_userdata('r_propinsi');
	$this->session->unset_userdata('r_city');
	$this->session->unset_userdata('r_deli_ad');
	
	//START ASP 20180409
	$this->session->unset_userdata('r_whsid');
	$this->session->unset_userdata('r_pic_name');
	$this->session->unset_userdata('r_pic_hp');
	$this->session->unset_userdata('r_kecamatan');
	$this->session->unset_userdata('r_kelurahan');
	$this->session->unset_userdata('r_kodepos');
	//EOF ASP 20180409
	
	//START ASP 20180917
	$this->session->unset_userdata('r_pic_name1');
	$this->session->unset_userdata('r_pic_hp1');
	$this->session->unset_userdata('r_kecamatan1');
	$this->session->unset_userdata('r_kelurahan1');
	$this->session->unset_userdata('r_kodepos1');
	//EOF ASP 20180917
    }
    
}
?>