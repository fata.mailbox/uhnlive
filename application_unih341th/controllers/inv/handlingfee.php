<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Handlingfee extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MInvreport'));
    }
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        $this->form_validation->set_rules('trxtype','','');

        if($this->form_validation->run()){
			$data['results'] = $this->MInvreport->getHandlingFeeRep($this->input->post('whsid'),$this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('trxtype'));    
        }else{
            $data['results'] = false;
        }
        $data['reportDate'] = date("Y-m-d");
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        if($this->session->userdata('whsid')==1)$data['warehouse'] = $this->MInvreport->getWarehouse();
		
		$data['trxtype'] = ['ro'=> 'RO','retur'=>'Return RO','sc'=>'Stockiest Consignment','scretur'=>'Return Stockiest Consignment'];
        $data['page_title'] = 'Handling Fee Report';
        $this->load->view('inv/handlingfee',$data);
	}
      
}
?>