<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sc_payment extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
		//20160404 - ASP Start
        //$this->load->model(array('MMenu','MSc_payment','GLobal_model'));
        $this->load->model(array('MMenu','MSc','MSc_payment','GLobal_model','Mruleorder'));
		//20160404 - ASP End
    }
    
    public function index(){
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        $this->form_validation->set_rules('whsid','','');
        
        $config['base_url'] = site_url().'inv/sc_payment/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']);
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $this->session->set_userdata('keywords_whsid',$this->db->escape_str($this->input->post('whsid')));
            $keywords = $this->session->userdata('keywords');
            $whsid = $this->session->userdata('keywords_whsid');
			
            $config['total_rows'] = $this->MSc_payment->countPinjaman($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MSc_payment->searchPinjaman($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
            //$data['results'] = $this->MSc_payment->searchSc_Pay($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) {
				$this->session->unset_userdata('keywords');
				$this->session->unset_userdata('keywords_whsid');
			}
            
            $keywords = $this->session->userdata('keywords'); 
			$whsid = $this->session->userdata('keywords_whsid');
			
            $config['total_rows'] = $this->MSc_payment->countPinjaman($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MSc_payment->searchPinjaman($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
			//$data['results'] = $this->MSc_payment->searchSc_Pay($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Stockist Consignment Payment';
	    $data['warehouse']=$this->MSc_payment->getDropDownWhs('all');
        $this->load->view('stockiest/sc_pay_admin_index',$data);
    }
    
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MSc_payment->getRequestOrder_v($id);
        
        if(!count($row)){
            redirect('inv/sc_payment/roadmin_v','refresh');
        }
		$bns = $this->MSc_payment->getFree($id);
		$data['bns'] = $bns;
		$data['row'] = $row;
        $data['items'] = $this->MSc_payment->getRequestOrderDetail_v($id);
		
		$checkVoucher = $this->MSc_payment->count_search_voucher_ro($id);
		$data['checkVoucher'] = $checkVoucher;
		if($checkVoucher>0) {
		 $rowv = $this->MSc_payment->search_voucher_ro($id);
		 $data['rowv'] = $rowv;
		}
		
        $data['items_pl'] = $this->MSc_payment->getRequestOrderDetail_p_list_v($id);
		
        $data['page_title'] = 'View Request Order Approval';
        $this->load->view('stockiest/ro_approved_view_v',$data);
    }
    
  
    
    public function create(){
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
		$data['pray_msg']='* Mohon Isikan Data pada isian yang telah di sediakan';
		
		$this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        $this->form_validation->set_rules('whsid','','');
		//echo $this->uri->segment(4);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $this->session->set_userdata('keywords_whsid',$this->db->escape_str($this->input->post('whsid')));
        }else{
            if(!$this->uri->segment(4)) $this->session->unset_userdata('keywords');
            if(!$this->uri->segment(4)) $this->session->unset_userdata('keywords_whsid');
        }
        $config['base_url'] = site_url().'inv/sc_payment/create';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $whsid = $this->session->userdata('keywords_whsid');
		
        $config['total_rows'] = $this->MSc->countPinjaman($keywords);
        $this->pagination->initialize($config);
		$data['page_title'] = 'Stockiest Consignment List';
		$data['warehouse']=$this->MSc->getDropDownWhs('all');
        $data['results'] = $this->MSc->searchPinjaman($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        $this->form_validation->set_rules('member_id_from','Stockiest','required');
        $this->form_validation->set_rules('name_from','','');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('nominal','','');
		$this->form_validation->set_rules('persen','','');
		
		$this->form_validation->set_rules('whsid','','');
		$this->form_validation->set_rules('pic_name1','','');
		$this->form_validation->set_rules('pic_hp1','','');
		$this->form_validation->set_rules('kecamatan1','','');
		$this->form_validation->set_rules('kelurahan1','','');
		$this->form_validation->set_rules('kodepos1','','');
		
		$this->form_validation->set_rules('member_id','','');
		$this->form_validation->set_rules('member_nama','','');
		$this->form_validation->set_rules('persen_to','','');
		
		if($this->form_validation->run() == true){
			$this->del_session();
            if(!$this->MMenu->blocked()){
				
				if (empty($this->input->post('sc_id'))){
					$data['pray_msg']='Mohon Pilih Data Stockiest Consignment';
				}else if (empty($this->input->post('no_stc'))){
					$data['pray_msg']='Mohon Pilih Transaksi Yang di Tujukan';
				}else if (empty($this->input->post('member_id_from'))){
					$data['pray_msg']='Mohon Pilih Stockiest';
				}else{
					$data['pray_msg']='Redirect ke Pembayaran';
					
					$this->session->set_userdata(
						array(
							'r_sc_id'=>$this->input->post('sc_id'),
							'r_sc_date'=>$this->input->post('sc_date'),
							'r_stc_id_from'=>$this->input->post('no_stc_from'),
							'r_stc_id_to'=>$this->input->post('no_stc'),
							'r_whsid_from'=>$this->input->post('whsid_from'),
							'r_whsid_to'=>$this->input->post('whsid_to'),
							'r_ewal_to'=>$this->input->post('ewal_to'),
							'r_whs_nm_to'=>$this->input->post('whs_nm_to'),
							'r_whs_from'=>$this->input->post('whs_from'),
							'r_nominal'=>$this->input->post('nominal'),
							'r_member_id'=>$this->input->post('member_id'),
							'r_member_nama'=>$this->input->post('member_nama'),
							'r_persen_to'=>$this->input->post('persen_to'),
							'r_member_id_from'=>$this->input->post('member_id_from'),
							'r_name_from'=>$this->input->post('name_from'),
							'r_persen'=>$this->input->post('persen_to'),
							'r_deli_ad'=>$this->input->post('deli_ad'),
							'r_timur'=>$this->input->post('timur'),
							'r_kota_id'=>$this->input->post('kota_id'),
							'r_kota_id1'=>$this->input->post('kota_id1'),
							'r_propinsi'=>$this->input->post('propinsi'),
							'r_city'=>$this->input->post('city'),
							'r_addr1'=>$this->input->post('addr1'),
							'r_whsid'=>$this->input->post('whsid'),
							'r_pic_name'=>$this->input->post('name_from'),
							'r_pic_hp'=>$this->input->post('pic_hp'),
							
							// END ASP 20180409
							// START ASP 20180917
							'r_pic_name1'=>$this->input->post('pic_name1'),
							'r_pic_hp1'=>$this->input->post('pic_hp1'),
							'r_kecamatan1'=>$this->input->post('kecamatan1'),
							'r_kelurahan1'=>$this->input->post('kelurahan1'),
							'r_kodepos1'=>$this->input->post('kodepos1')
						)
					);
					redirect('inv/sc_payment/add','refresh');
					
					
				}
				$this->load->view('stockiest/sc_pay_admin_form_list',$data);
			}
		}else{
			$this->load->view('stockiest/sc_pay_admin_form_list',$data);
		}
	
        //$this->load->view('stockiest/sc_pay_admin_form_list',$data);
	}
	
    public function add(){
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
        if (empty($this->session->userdata('r_sc_id'))){
			redirect('inv/sc_payment','refresh');
		}
		$data['pray_msg']='';
		$this->form_validation->set_rules('member_id','Stockist','required');
		$this->form_validation->set_rules('member_id_to','Required','');
		
		/*
        $this->form_validation->set_rules('remark','','');
		
		$this->form_validation->set_rules('total','Total repeat order','required|callback__check_min');
        $this->form_validation->set_rules('totalpv','','');
		$this->form_validation->set_rules('totalbv','','');
	
        $this->form_validation->set_rules('persen','','');
		$this->form_validation->set_rules('totalrpdiskon','','');
        $this->form_validation->set_rules('totalbayar','Total Bayar','callback__check_bayar');
		$this->form_validation->set_rules('vselectedvoucher','','');
		$this->form_validation->set_rules('total','Total repeat order','required|callback__check_stock|callback__check_min_order');
		$this->form_validation->set_rules('totalpv','','');
		$this->form_validation->set_rules('totalbv','','');
		*/
		
		$data['counti']=$this->input->post('counti');
		if($this->input->post('action')){
			$data['counti']=$this->input->post('counti')+$this->input->post('rowx');
			for ($i = 0; $i <= ($data['counti']-1); $i++) {
				
				//$this->form_validation->set_rules('qty_lunas'.$i,'','');
				
				/*
				$data['vouchercode'.$i]=$this->input->post('vouchercode'.$i);
				$data['vprice'.$i]=$this->input->post('vprice'.$i);
				$data['vpv'.$i]=$this->input->post('vpv'.$i);
				$data['vsubtotal'.$i]=$this->input->post('vsubtotal'.$i);
				$data['vsubtotalpv'.$i]=$this->input->post('vsubtotalpv'.$i);
				*/
				
			}
		}
		
		$data['row'] = $this->GLobal_model->get_ewallet_stc($this->session->userdata('r_member_id_from'));
		$data['item_results'] = $this->MSc_payment->getPinjamanDetail($this->session->userdata('r_sc_id'));
		//$data['item_results'] = $this->MSc->getPinjamanDetail_full($this->session->userdata('r_sc_id'));
		$data['items_free'] = $this->MSc_payment->search_FreeItem($this->session->userdata('r_sc_id'),"","");
		//echo '<h1>'.$this->session->userdata('r_member_id_from').'</h1>';
		
		$this->form_validation->set_rules('ewallet_to','eWallet','');		
		$this->form_validation->set_rules('sisaTotal','Total Pembayaran SC','');		
		
		if($this->form_validation->run() == true){
			//$this->del_session();
            if(!$this->MMenu->blocked()){
				if($this->input->post('cmSubmit')){
					/* Validasi Lapis Akhir, Kalau Validasi Yang di atas Masih Lolos dan Error Juga,,, */
					//$ewall_valid=$this->input->post('ewallet_to')-str_replace(".","",$this->input->post('sisaTotal'));
					$ewall_valid=$this->input->post('ewallet_to')-str_replace(".","",$this->input->post('sisabayar'));
					if (empty($this->input->post('ewallet_to'))){
						$data['pray_msg']='eWallet Kosong';
					}else if (($this->input->post('ewallet_to')<>str_replace(",","",$this->session->userdata('r_ewal_to')))){
						$data['pray_msg']='Jumlah eWallet tidak sesuai dengan Database';
					}else if ($ewall_valid<0){
						$data['pray_msg']='Jumlah eWallet tidak Mencukupi';
					}else{
						//$data['pray_msg']='Data Successfull Submit';
						$this->MSc_payment->addPaymenySc();
						$this->del_session();
						$data['pray_msg']='Data Successfull Submit';
						redirect('inv/sc_payment/','refresh');
						
					}
					/* End Validasi Lapis Akhir */
				}
				
				/*
				$this->session->userdata('sc_id'),
				$this->session->userdata('sc_date'),
				$this->session->userdata('no_stc_from'),
				$this->session->userdata('no_stc'),
				$this->session->userdata('whsid_to'),
				$this->session->userdata('ewal_to'),
				$this->session->userdata('whs_nm_to'),
				$this->session->userdata('whs_from'),
				$this->session->userdata('nominal'),
				$this->session->userdata('member_id'),
				$this->session->userdata('member_nama'),
				$this->session->userdata('persen_to'),
				$this->session->userdata('member_id_from'),
				$this->session->userdata('name_from'),
				$this->session->userdata('persen'),
				$this->session->userdata('deli_ad'),
				$this->session->userdata('timur'),
				$this->session->userdata('kota_id'),
				$this->session->userdata('kota_id1'),
				$this->session->userdata('propinsi'),
				$this->session->userdata('city'),
				$this->session->userdata('addr1')
				// START ASP 20180409
				$this->session->userdata('whsid'),
				$this->session->userdata('name_from'),
				$this->session->userdata('pic_hp'),
				
				// END ASP 20180409
				// START ASP 20180917
				$this->session->userdata('pic_name1'),
				$this->session->userdata('pic_hp1'),
				$this->session->userdata('kecamatan1'),
				$this->session->userdata('kelurahan1'),
				$this->session->userdata('kodepos1')
				*/
				
				//redirect('inv/sc_payment/','refresh');
			}
		}
		
		$data['page_title'] = 'Create Stockist Consignment Payment';
        $this->load->view('stockiest/sc_pay_admin_form2_v',$data);
	
	}
	
	public function voucher_sc(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/voucher/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->MSc->count_search_voucher($this->uri->segment(4),$keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
       
        $data['results'] = $this->MSc->search_voucher($this->uri->segment(4),$keywords,'sc',$config['per_page'],$this->uri->segment($config['uri_segment']));
        
		//$data['stc'] = $this->GLobal_model->get_ewallet_stc($this->session->userdata('r_member_id'));
		$data['prosestambahan'] = "
		window.opener.document.form.totalrpdiskon.value=totaldiskon_curr(".$this->session->userdata('counti').",'window.opener.document.form.subrpdiskon');
                    totalbayardiskon(window.opener.document.form.total, window.opener.document.form.totalrpdiskon, window.opener.document.form.totalbayar);
		";
        $data['page_title'] = 'E-Wallet Voucher';
        $data['typevouchersearch'] = '1';
        $this->load->view('stockiest/sc_voucher_search',$data);
    }
	
	
    
    public function _check_stock(){
	$whsid = $this->input->post('whsid');//1;
	$memberid = $this->input->post('member_id');
	
	$amount = str_replace(".","",$this->input->post('total'));
	if($amount <= 0){
	    $this->form_validation->set_message('_check_stock','browse product...');
	    return false;
	}else{
	    if(!validation_errors()){
		
		$data = array(
		    'stockiest_id'=> $whsid,
		    'member_id'=> $memberid
		);
		$this->db->insert('so_check',$data);
	    
		$id = $this->db->insert_id();
	    
		$key=0;
		$whsid = $this->input->post('whsid');//1;
		while($key < count($_POST['counter'])){
		    $qty = str_replace(".","",$this->input->post('qty'.$key));
		    $price = str_replace(".","",$this->input->post('price'.$key));
		    $itemid = $this->input->post('itemcode'.$key);
		    if($itemid and $qty > 0){
			$data=array(
			    'so_id' => $id,
			    'item_id' => $itemid,
			    'qty' => $qty,
			    'price' => $price
			);
			$this->db->insert('so_check_d',$data);
			
			//echo $id.' * '.$itemid.' * '.$price.' * '.$titipan_id.' * '.$stcid;
			
			$rs=$this->GLobal_model->check_stock_whs($id,$itemid,$price,$whsid);
			if(!$rs->stock){
			    $this->form_validation->set_message('_check_stock',"You don't have ".$itemid);
				
			    $this->db->delete('so_check',array('id'=>$id));
			    $this->db->delete('so_check_d',array('so_id'=>$id));
			    return false;
			}elseif($rs->stock < $rs->qty){
			    $this->form_validation->set_message('_check_stock',"We only have ".$rs->stock." ".$rs->name);
			    
			    $this->db->delete('so_check',array('id'=>$id));
			    $this->db->delete('so_check_d',array('so_id'=>$id));
			    return false;
			}
		    }
		    $key++;
		}
		
		$this->db->delete('so_check',array('id'=>$id));
		$this->db->delete('so_check_d',array('so_id'=>$id));
	    }
        }
        return true;
    }
    
    public function _check_bayar(){
        $amount = str_replace(".","",$this->input->post('total'));
		$diskon = str_replace(".","",$this->input->post('totalrpdiskon'));
		$totalbayar = $amount-$diskon;
			
		$row = $this->GLobal_model->get_ewallet_stc($this->input->post('member_id'));
		if($row){
			if($totalbayar > $row->ewallet){
				$this->form_validation->set_message('_check_bayar','Pembayaran tidak mencukupi, ewallet anda kurang Rp. '.number_format($totalbayar-$row->ewallet,0,'','.'));
				return false;
			}
		}else{
			$this->form_validation->set_message('_check_bayar','Invalid member, silahkan hubungi IT UHN');
			return false;
		}
	
        return true;
    }
    
    
    public function del_session(){
	
	$this->session->unset_userdata('r_sc_id');
	$this->session->unset_userdata('r_sc_date');
	$this->session->unset_userdata('r_stc_id_from');
	$this->session->unset_userdata('r_stc_id_to');
	$this->session->unset_userdata('r_whsid_to');
	$this->session->unset_userdata('r_ewal_to');
	$this->session->unset_userdata('r_whs_nm_to');
	$this->session->unset_userdata('r_whs_from');
	$this->session->unset_userdata('r_nominal');
	$this->session->unset_userdata('r_member_nama');
	$this->session->unset_userdata('r_persen_to');
	$this->session->unset_userdata('r_member_id_from');
	$this->session->unset_userdata('r_name_from');
	
	$this->session->unset_userdata('r_member_id');
	$this->session->unset_userdata('r_persen');
	$this->session->unset_userdata('r_pu');
	$this->session->unset_userdata('r_timur');
	$this->session->unset_userdata('r_kota_id');
	$this->session->unset_userdata('r_kota_id1');
	$this->session->unset_userdata('r_addr');
	$this->session->unset_userdata('r_addr1');
	$this->session->unset_userdata('r_propinsi');
	$this->session->unset_userdata('r_city');
	$this->session->unset_userdata('r_deli_ad');
	
	//START ASP 20180409
	$this->session->unset_userdata('r_whsid');
	$this->session->unset_userdata('r_pic_name');
	$this->session->unset_userdata('r_pic_hp');
	$this->session->unset_userdata('r_kecamatan');
	$this->session->unset_userdata('r_kelurahan');
	$this->session->unset_userdata('r_kodepos');
	//EOF ASP 20180409
	
	//START ASP 20180917
	$this->session->unset_userdata('r_pic_name1');
	$this->session->unset_userdata('r_pic_hp1');
	$this->session->unset_userdata('r_kecamatan1');
	$this->session->unset_userdata('r_kelurahan1');
	$this->session->unset_userdata('r_kodepos1');
	//EOF ASP 20180917
    }
    public function _check_delivery(){
        if($this->input->post('pu')==1){
	    //if(strlen($this->input->post('city'))<1 OR strlen($this->input->post('addr'))<1){
		//START ASP 20180407
	    if(strlen($this->input->post('pic_name'))<1 OR strlen($this->input->post('pic_hp'))<1 OR strlen($this->input->post('city'))<1 OR strlen($this->input->post('addr'))<1 OR strlen($this->input->post('kelurahan'))<1 OR strlen($this->input->post('kecamatan'))<1 OR strlen($this->input->post('kodepos'))<5){
		//EOF ASP 20180407
		$this->form_validation->set_message('_check_delivery','Please fill the address.');
		return false;
	    }
        }
        return true;
    }
	//20160404 - ASP Start
    public function _check_min_order(){
		$key=0;
		while($key < count($_POST['counter'])){
			$qty = str_replace(".","",$this->input->post('qty'.$key));
		    $itemid = $this->input->post('itemcode'.$key);
			$getMinOrder = $this->Mruleorder->getRuleOrder($itemid);
			$getItemName = $this->Mruleorder->getItemName($itemid);
			if($itemid and $qty > 0){
				if($qty%$getMinOrder <> 0){
					$this->form_validation->set_message('_check_min_order',"Pembelian ".$itemid."-".$getItemName." harus kelipatan ".$getMinOrder." .");
					return false;
				}
			}
		    $key++;
		}
	}
	//20160404 - ASP End
}
?>