<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mnf extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        $this->load->model(array('MMenu','MAssembling'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'inv/mnf/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MAssembling->countManufaktur($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MAssembling->searchManufaktur($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
            
        $data['page_title'] = 'Bill of Material';
        $this->load->view('inv/manufaktur_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('itemcode','id BoM','required|callback__check_id');
        $this->form_validation->set_rules('itemname','','');
            
        if($_POST['action'] == 'Go'){
            $rowx = $this->input->post('rowx');
            $counti = count($_POST['counter'])+$rowx;
        }elseif($_POST['action'] == 'Submit')$counti = count($_POST['counter']);
        else $counti=5;
            
        $key=0;
        while($counti > $key){
            $this->form_validation->set_rules("itemcode".$key,'','');
            $this->form_validation->set_rules('itemname'.$key,'','');
            $this->form_validation->set_rules('qty'.$key,'','');
            $key++;
        }
        $data['counti']=$counti;
        
        if($this->form_validation->run() && $_POST['action'] != 'Go'){
            if(!$this->MMenu->blocked()){
                $this->MAssembling->addManufaktur();
                $this->session->set_flashdata('message','Bill of Material Sukses');
            }
            redirect('inv/mnf','refresh');
        }
        
        $data['page_title'] = 'Form Bill of Material';
        $this->load->view('inv/manufaktur_form',$data);
    }
    public function _check_id(){
        if($this->MAssembling->check_manufaktur($this->input->post('itemcode'))){
            $this->form_validation->set_message('_check_id','Bill of material id sudah terdaftar!');
            return false;
        }
        return true;
    }
    
    public function view(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MAssembling->getManufaktur($this->uri->segment(4));
        
        if(!count($row)){
            redirect('inv/mnf','refresh');
        }
        $data['row'] = $row;
        //$data['items'] = $this->MAssembling->getFPBDetail($this->uri->segment(3));
        $data['page_title'] = 'View Bill of Material';
        $this->load->view('inv/manufaktur_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('itemcodex','','required');
        $this->form_validation->set_rules('rowx','','');
        
        //echo count($_POST['counter']);
        if($_POST['action'] == 'Go'){
            $rowx = $this->input->post('rowx');
            $counti = count($_POST['counter'])+$rowx;
            
            $key=0;
            while($counti > $key){
                //echo $key."<br>";
                $this->form_validation->set_rules("itemcode".$key,'','');
                $this->form_validation->set_rules('itemname'.$key,'','');
                $this->form_validation->set_rules('qty'.$key,'','');
                $key++;
            }
            $data['key']=$key;
            $data['counti']=$counti;
        }
        
        $row=array();
        $row = $this->MAssembling->getManufaktur($id);
        if(!count($row)){
            redirect('inv/mnf','refresh');
        }
        $data['results'] = $row;
        
        
        if($this->form_validation->run() && $_POST['action'] != 'Go'){
            if(!$this->MMenu->blocked()){
                $this->MAssembling->editManufaktur();
                $this->session->set_flashdata('message','Update bill of material sukses');
            }
            redirect('inv/mnf','refresh');  
        }
        $data['page_title'] = 'Edit Bill of Material';
        $this->load->view('inv/manufaktur_edit',$data);
    }
    
}
?>