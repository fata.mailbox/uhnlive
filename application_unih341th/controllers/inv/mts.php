<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mts extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MMutasi'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'inv/mts/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MMutasi->countMutasi($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MMutasi->searchMutasi($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MMutasi->countMutasi($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MMutasi->searchMutasi($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Warehouse Stock Movement';
        $this->load->view('inv/mutasistock_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('warehouse_id1','From Warehouse','');
        $this->form_validation->set_rules('warehouse_id2','Warehouse','required|callback__check_warehouse');
        $this->form_validation->set_rules('password1','Password','required|callback__check_password');
        $this->form_validation->set_rules('remark','','');
        
        $this->form_validation->set_rules('itemcode0','','');
        $this->form_validation->set_rules('itemname0','','');
        $this->form_validation->set_rules('qty0','','callback__check_stock');
        $this->form_validation->set_rules('price0','','');
        $this->form_validation->set_rules('tprice0','','');
        
        $this->form_validation->set_rules('itemcode1','','');
        $this->form_validation->set_rules('itemname1','','');
        $this->form_validation->set_rules('qty1','','');
        $this->form_validation->set_rules('price1','','');
        $this->form_validation->set_rules('tprice1','','');
        
        $this->form_validation->set_rules('itemcode2','','');
        $this->form_validation->set_rules('itemname2','','');
        $this->form_validation->set_rules('qty2','','');
        $this->form_validation->set_rules('price2','','');
        $this->form_validation->set_rules('tprice2','','');
        
        $this->form_validation->set_rules('itemcode3','','');
        $this->form_validation->set_rules('itemname3','','');
        $this->form_validation->set_rules('qty3','','');
        $this->form_validation->set_rules('price3','','');
        $this->form_validation->set_rules('tprice3','','');
        
        $this->form_validation->set_rules('itemcode4','','');
        $this->form_validation->set_rules('itemname4','','');
        $this->form_validation->set_rules('qty4','','');
        $this->form_validation->set_rules('price4','','');
        $this->form_validation->set_rules('tprice4','','');
        
        $this->form_validation->set_rules('itemcode5','','');
        $this->form_validation->set_rules('itemname5','','');
        $this->form_validation->set_rules('qty5','','');
        $this->form_validation->set_rules('price5','','');
        $this->form_validation->set_rules('tprice5','','');
        
        $this->form_validation->set_rules('itemcode6','','');
        $this->form_validation->set_rules('itemname6','','');
        $this->form_validation->set_rules('qty6','','');
        $this->form_validation->set_rules('price6','','');
        $this->form_validation->set_rules('tprice6','','');
        
        $this->form_validation->set_rules('itemcode7','','');
        $this->form_validation->set_rules('itemname7','','');
        $this->form_validation->set_rules('qty7','','');
        $this->form_validation->set_rules('price7','','');
        $this->form_validation->set_rules('tprice7','','');
        
        $this->form_validation->set_rules('itemcode8','','');
        $this->form_validation->set_rules('itemname8','','');
        $this->form_validation->set_rules('qty8','','');
        $this->form_validation->set_rules('price8','','');
        $this->form_validation->set_rules('tprice8','','');
        
        $this->form_validation->set_rules('itemcode9','','');
        $this->form_validation->set_rules('itemname9','','');
        $this->form_validation->set_rules('qty9','','');
        $this->form_validation->set_rules('price9','','');
        $this->form_validation->set_rules('tprice9','','');
        
		$this->form_validation->set_rules('totalprice','','callback__check_stock');
		
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MMutasi->addMutasi();
                $this->session->set_flashdata('message','Warehouse Stock Movement Successfully');
            }
            redirect('inv/mts','refresh');
        }
        $data['warehouse'] = $this->MMutasi->getDropDownWhs();
        $data['page_title'] = 'Create Mutasi Stock';
        $this->load->view('inv/mutasistock_form',$data);
    }
    
    public function _check_password(){
        $this->load->model('MAuth');
        if(!$this->MAuth->check_password($this->session->userdata('user'),$this->input->post('password1'))){
            $this->form_validation->set_message('_check_password','Sorry, your password invalid');
            return false;
        }
        return true;
    }
    public function _check_warehouse(){
        if($this->input->post('warehouse_id1') == $this->input->post('warehouse_id2')){
            $this->form_validation->set_message('_check_warehouse','Sorry, Please choose To Warehouse');
            return false;
        }
        return true;
    }
	
	
    public function _check_stock(){
        $qty0 = str_replace(".","",$this->input->post('qty0'));
        $qty1 = str_replace(".","",$this->input->post('qty1'));
        $qty2 = str_replace(".","",$this->input->post('qty2'));
        $qty3 = str_replace(".","",$this->input->post('qty3'));
        $qty4 = str_replace(".","",$this->input->post('qty4'));
        $qty5 = str_replace(".","",$this->input->post('qty5'));
        $qty6 = str_replace(".","",$this->input->post('qty6'));
        $qty7 = str_replace(".","",$this->input->post('qty7'));
        $qty8 = str_replace(".","",$this->input->post('qty8'));
        $qty9 = str_replace(".","",$this->input->post('qty9'));
		
        if($this->input->post('itemcode0')!='') {
            if($qty0 <=0){
                $this->form_validation->set_message('_check_stock','Quantity '.$this->input->post('itemcode0').' cannot be empty !');
                return false;
            }else{
                $jml = $this->MMutasi->cekStok($this->input->post('itemcode0'),$this->input->post('warehouse_id1'));
                if($jml<$qty0){
                    $this->form_validation->set_message('_check_stock',"We only have $jml item for ".$this->input->post('itemcode0')." !");
                    return false;
                }
            }
        }
        if($this->input->post('itemcode1')!='') {
            if($qty1 <=0){
                $this->form_validation->set_message('_check_stock','Quantity '.$this->input->post('itemcode1').' cannot be empty !');
                return false;
            }else{
                $jml = $this->MMutasi->cekStok($this->input->post('itemcode1'),$this->input->post('warehouse_id1'));
                if($jml<$qty1){
                    $this->form_validation->set_message('_check_stock',"We only have $jml item for ".$this->input->post('itemcode1')." !");
                    return false;
                }
            }
        }
        if($this->input->post('itemcode2')!='') {
            if($qty2 <=0){
                $this->form_validation->set_message('_check_stock','Quantity '.$this->input->post('itemcode2').' cannot be empty !');
                return false;
            }else{
                $jml = $this->MMutasi->cekStok($this->input->post('itemcode2'),$this->input->post('warehouse_id1'));
                if($jml<$qty2){
                    $this->form_validation->set_message('_check_stock',"We only have $jml item for ".$this->input->post('itemcode2')." !");
                    return false;
                }
            }
        }
        if($this->input->post('itemcode3')!='') {
            if($qty3 <=0){
                $this->form_validation->set_message('_check_stock','Quantity '.$this->input->post('itemcode3').' cannot be empty !');
                return false;
            }else{
                $jml = $this->MMutasi->cekStok($this->input->post('itemcode3'),$this->input->post('warehouse_id1'));
                if($jml<$qty3){
                    $this->form_validation->set_message('_check_stock',"We only have $jml item for ".$this->input->post('itemcode3')." !");
                    return false;
                }
            }
        }
        if($this->input->post('itemcode4')!='') {
            if($qty4 <=0){
                $this->form_validation->set_message('_check_stock','Quantity '.$this->input->post('itemcode4').' cannot be empty !');
                return false;
            }else{
                $jml = $this->MMutasi->cekStok($this->input->post('itemcode4'),$this->input->post('warehouse_id1'));
                if($jml<$qty1){
                    $this->form_validation->set_message('_check_stock',"We only have $jml item for ".$this->input->post('itemcode4')." !");
                    return false;
                }
            }
        }
        if($this->input->post('itemcode5')!='') {
            if($qty5 <=0){
                $this->form_validation->set_message('_check_stock','Quantity '.$this->input->post('itemcode5').' cannot be empty !');
                return false;
            }else{
                $jml = $this->MMutasi->cekStok($this->input->post('itemcode5'),$this->input->post('warehouse_id1'));
                if($jml<$qty5){
                    $this->form_validation->set_message('_check_stock',"We only have $jml item for ".$this->input->post('itemcode5')." !");
                    return false;
                }
            }
        }

        if($this->input->post('itemcode6')!='') {
            if($qty6 <=0){
                $this->form_validation->set_message('_check_stock','Quantity '.$this->input->post('itemcode6').' cannot be empty !');
                return false;
            }else{
                $jml = $this->MMutasi->cekStok($this->input->post('itemcode6'),$this->input->post('warehouse_id1'));
                if($jml<$qty6){
                    $this->form_validation->set_message('_check_stock',"We only have $jml item for ".$this->input->post('itemcode6')." !");
                    return false;
                }
            }
        }
        if($this->input->post('itemcode7')!='') {
            if($qty7 <=0){
                $this->form_validation->set_message('_check_stock','Quantity '.$this->input->post('itemcode7').' cannot be empty !');
                return false;
            }else{
                $jml = $this->MMutasi->cekStok($this->input->post('itemcode7'),$this->input->post('warehouse_id1'));
                if($jml<$qty7){
                    $this->form_validation->set_message('_check_stock',"We only have $jml item for ".$this->input->post('itemcode7')." !");
                    return false;
                }
            }
        }
        if($this->input->post('itemcode8')!='') {
            if($qty8 <=0){
                $this->form_validation->set_message('_check_stock','Quantity '.$this->input->post('itemcode8').' cannot be empty !');
                return false;
            }else{
                $jml = $this->MMutasi->cekStok($this->input->post('itemcode8'),$this->input->post('warehouse_id1'));
                if($jml<$qty8){
                    $this->form_validation->set_message('_check_stock',"We only have $jml item for ".$this->input->post('itemcode8')." !");
                    return false;
                }
            }
        }
        if($this->input->post('itemcode9')!='') {
            if($qty9 <=0){
                $this->form_validation->set_message('_check_stock','Quantity '.$this->input->post('itemcode9').' cannot be empty !');
                return false;
            }else{
                $jml = $this->MMutasi->cekStok($this->input->post('itemcode9'),$this->input->post('warehouse_id1'));
                if($jml<$qty9){
                    $this->form_validation->set_message('_check_stock',"We only have $jml item for ".$this->input->post('itemcode9')." !");
                    return false;
                }
            }
        }
        return true;
    }
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MMutasi->getMutasi($id);
        
        if(!count($row)){
            redirect('inv/mts','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MMutasi->getMutasiDetail($id);
        $data['page_title'] = 'View Warehouse Stock Movement';
        $this->load->view('inv/mutasistock_view',$data);
    }
    
}
?>