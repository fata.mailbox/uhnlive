<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ncm extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MInv','MSo', 'MInvreport'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        $this->form_validation->set_rules('whsid','','');
        
        $config['base_url'] = site_url().'inv/ncm/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $this->session->set_userdata('keywords_whsid',$this->db->escape_str($this->input->post('whsid')));
        	$whsid = $this->session->userdata('keywords_whsid');

			$config['total_rows'] = $this->MInv->countNCM($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MInv->searchNCM($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            $keywords = $this->session->userdata('keywords');   
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords_whsid');
        	$whsid = $this->session->userdata('keywords_whsid');
			
            $config['total_rows'] = $this->MInv->countNCM($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MInv->searchNCM($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
		
        if($this->session->userdata('whsid')==1)$data['warehouse'] = $this->MInvreport->getWarehouse();
		$data['warehouse']=$this->MInv->getDropDownWhsAll('all');
        $data['page_title'] = 'No Comercial Product';
        $this->load->view('inv/ncm_index',$data);
    }
    public function approved(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        if(!$this->MMenu->blocked()){
            $this->MInv->ncmApproved();
        }
        redirect('inv/adj/','refresh');
    }
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('warehouse_id','','required|callback__check_min');
        $this->form_validation->set_rules('password1','Password','required|callback__check_password');
        $this->form_validation->set_rules('remark','','');
        
        $this->form_validation->set_rules('itemcode0','','');
        $this->form_validation->set_rules('itemname0','','');
        $this->form_validation->set_rules('qty0','','');
        
        $this->form_validation->set_rules('itemcode1','','');
        $this->form_validation->set_rules('itemname1','','');
        $this->form_validation->set_rules('qty1','','');
        
        $this->form_validation->set_rules('itemcode2','','');
        $this->form_validation->set_rules('itemname2','','');
        $this->form_validation->set_rules('qty2','','');
        
        $this->form_validation->set_rules('itemcode3','','');
        $this->form_validation->set_rules('itemname3','','');
        $this->form_validation->set_rules('qty3','','');
        
        $this->form_validation->set_rules('itemcode4','','');
        $this->form_validation->set_rules('itemname4','','');
        $this->form_validation->set_rules('qty4','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MInv->addNCM();
                $this->session->set_flashdata('message','No Comercial Product Successfully');
            }
            redirect('inv/ncm','refresh');
        }
        $data['warehouse'] = $this->MInv->getDropDownWhs();
        $data['page_title'] = 'Create No Comercial Product';
        $this->load->view('inv/ncm_form',$data);
    }
    public function _check_password(){
        $this->load->model('MAuth');
        if(!$this->MAuth->check_password($this->session->userdata('user'),$this->input->post('password1'))){
            $this->form_validation->set_message('_check_password','Sorry, your password invalid');
            return false;
        }
        return true;
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MInv->getNCM($id);
        
        if(!count($row)){
            redirect('inv/ncm','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MInv->getNCMDetail($id);
        $data['page_title'] = 'View No Comercial Product';
        $this->load->view('inv/ncm_view',$data);
    }
	
	public function _check_min(){
        
		if($this->session->userdata('group_id') <= 100){
		
			$flag = 0;
			if($this->input->post('itemcode0')){$item[$flag]=$this->input->post('itemcode0'); $qty[$flag]=str_replace(".","",$this->input->post('qty0')); $itemname[$flag]=$this->input->post('itemname0');$flag+=1;}
			if($this->input->post('itemcode1')){$item[$flag]=$this->input->post('itemcode1'); $qty[$flag]=str_replace(".","",$this->input->post('qty1')); $itemname[$flag]=$this->input->post('itemname1');$flag+=1;}
			if($this->input->post('itemcode2')){$item[$flag]=$this->input->post('itemcode2'); $qty[$flag]=str_replace(".","",$this->input->post('qty2')); $itemname[$flag]=$this->input->post('itemname2');$flag+=1;}
			if($this->input->post('itemcode3')){$item[$flag]=$this->input->post('itemcode3'); $qty[$flag]=str_replace(".","",$this->input->post('qty3')); $itemname[$flag]=$this->input->post('itemname3');$flag+=1;}
			if($this->input->post('itemcode4')){$item[$flag]=$this->input->post('itemcode4'); $qty[$flag]=str_replace(".","",$this->input->post('qty4')); $itemname[$flag]=$this->input->post('itemname4');$flag+=1;}

			$temp = 0;$twin=0;
			//$whsid = $this->session->userdata('whsid');
			$whsid = $this->input->post('warehouse_id');	
			
			for($i=0;$i<=$flag;$i++){
				if($temp>0){
					for($j=0;$j<$temp;$j++){
						if($brg[$j]==$item[$i]){//jika sama
							$twin = 1;
							$jml[$j]+=$qty[$i];
						}
					}
				}			
				if($twin==0){//jika tidak ada yang sama
					$jml[$temp]=$qty[$i];
					$brg[$temp]=$item[$i];
					$nama[$temp]=$itemname[$i];
					$temp+=1;
				}else{$twin=0;}
			}
			for($i=0;$i<count($jml)-1;$i++){
				//echo "<br><br>".$brg[$i]." = ".$jml[$i]." ".$nama[$i]." ";
				$readystok=$this->MSo->cekStok($whsid, $brg[$i], $jml[$i]);
				if($readystok!='Ready'){$twin=1;$stok="We only have ".$readystok." ".$nama[$i]." on hand";}
				//echo "Stok = ".$readystok."<br>";
			}
			if($twin > 0){
				$this->form_validation->set_message('_check_min','Out of order ( '.$stok.' )');
				return false;
			}
			//end Updated by Boby (2012-01-13)
		}
        return true;
    }
    
}
?>