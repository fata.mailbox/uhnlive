<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stc_report extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MEwallet','MStc'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('member_id','','');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('fromdate','','');
        //$this->form_validation->set_rules('todate','','');
		
		$data['reportDate'] = date("Y-m-d");
        
        if($this->form_validation->run()){
			//if($periode)
			$skrg=$this->input->post('fromdate'); // date("Y-m-d", now());
            $data['result'] = 1;
            $data['stc'] = $this->MStc->dataStc($this->input->post('member_id'));
			$data['rpt1'] = $this->MStc->omset1($this->input->post('member_id'), $skrg, 1);
			$data['rpt2'] = $this->MStc->omset1($this->input->post('member_id'), $skrg, 2);
			$data['newmember'] = $this->MStc->newMember($this->input->post('member_id'), $skrg);
			$data['periodeO'] = $this->MStc->periodeOmset($skrg,1);
			$data['periode'] = $this->MStc->periodeOmset($skrg,0);
			$data['reportDate']=$this->input->post('fromdate');
        }else{
            $data['result'] = false;
            $data['total'] = false;
            if($this->session->userdata('group_id')>100)$data['ewallet'] = $this->MEwallet->getEwallet($this->session->userdata('userid'));
            else $data['ewallet']=false;
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'Stockiest Report';
        
        $this->load->view('stc/vstc_report',$data);
    }
}
?>
