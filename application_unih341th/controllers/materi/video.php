<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Video extends CI_Controller 
{
    function __construct()
    {
	    parent::__construct();
        if(!$this->session->userdata('logged_in'))
        {
            redirect('');
        }
        
        $this->load->model(array('MMenu','RO_model','GLobal_model','video_model','category_model'));
    }
    
    public function index()
    {
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(3),'view'))
        {
            redirect('error','refresh');
        }
        
    }

    public function detail_video($id='')
    {
        $data['page_title'] = 'Video Page';
        $process = $this->video_model->getDataList('','',$id);
        $category_id = $process['dataObject']->cat_id;

        $data['video_banner'] = $process;
        $data['video_cat_1'] = $this->video_model->getDataList('','','','','','',$category_id,'','','1');
        
        $this->load->view('materi/video/video_play',$data);
    }

    public function video_category($category_id='')
    {
        $category_type = $this->category_model->getDataList('','',$category_id);
        $data['page_title'] = $category_type['dataObject']->name;

        $data['video_banner'] = $this->video_model->getDataList('0','10','','','','',$category_id,'','1','1');
        $data['alt_video_banner'] = $this->video_model->getDataList('0','10','','','','',$category_id,'','','1');
        $data['video_cat_1'] = $this->video_model->getDataList('','','','','','',$category_id,'','','1');
        
        $this->load->view('materi/video/video_play',$data);
    }

}