<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengaturan_video extends CI_Controller 
{
    function __construct()
    {
	    parent::__construct();
        if(!$this->session->userdata('logged_in'))
        {
            redirect('');
        }
        
        $this->load->model(array('MMenu','Video_model','category_model','GLobal_model'));
    }
    
    public function index($start = '0',$limit='10',$keyword='',$success_msg ='', $error_msg='')
    {
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view'))
        {
            redirect('error','refresh');
        }


        $data['page_title'] = 'List Video';
        
        if(!empty($this->input->post('search')))
        {
            $video_keyword = $this->input->post('video-keyword');

            $process = $this->Video_model->getDataList('','','','','','','','','','','',$video_keyword);

            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');

            $config['base_url'] = base_url().'materi/pengaturan_video/index';
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;
            $config['total_rows'] = $jumlah_data;
            
            $this->pagination->initialize($config);     
           

            $data['success_msg'] = '';
            $data['error_msg'] = '';
            $data['keyword'] = $category_keyword;
            $data['video'] = $this->Video_model->getDataList($start,$limit,'','','','','','','','','',$video_keyword);
            $this->load->view('materi/video/video_list',$data);

        }
        else
        {
            $process = $this->Video_model->getDataList();
            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');

            $config['base_url'] = base_url().'materi/pengaturan_video/index';
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;
            $config['total_rows'] = $jumlah_data;
            
            $this->pagination->initialize($config);     
           

            $data['success_msg'] = $success_msg;
            $data['error_msg'] = $error_msg;
            $data['keyword'] = '';
            $data['video'] = $this->Video_model->getDataList($start,$limit);
            $this->load->view('materi/video/video_list',$data);
        }
    }

    private function set_upload_options()
    {   
        //upload an image options
        $config = array();
        $config['upload_path']          = './images/video_thumbnail/'; //local path
        // $config['upload_path']          = '/home/deploy/source/sohomlm/images/video_thumbnail'; //server path
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']      = '10000';
        $config['overwrite']     = FALSE;

        return $config;
     }


    public function add_video()
    {
       
        $data['page_title'] = 'Add Video';
        $data['category'] = $this->category_model->getDataList();
        $this->load->view('materi/video/add_video', $data);        
    }

    public function process_add_video()
    {
        $start = '0';
        $limit = '10';
        $config['base_url'] = base_url().'materi/pengaturan_video/index';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        
        if (!empty($this->input->post('add-video'))) 
        {
            $data['page_title'] = 'Video List';
                    
            $this->load->library('upload');

            $title = $this->input->post('video-title');
            $description = $this->input->post('video-description');
            $link = $this->input->post('video-link');
            $category = $this->input->post('video-category');
            $sort = $this->input->post('sort');
            $thumbnail = $_FILES['thumbnail']['name'];
            $feature_ = $this->input->post('feature');
            $publish = $this->input->post('publish');
            $download_link = $this->input->post('download-link');

            if ($feature_ == true) {
                $feature = 1;
            }
            else
            {
                $feature = 0;
            }

            $this->upload->initialize($this->set_upload_options());
            $up = $this->upload->do_upload('thumbnail');

            if (!$up) {
                $data['error_msg']= $this->upload->display_errors();
            } else {
                $process1 = $this->Video_model->create($title,$description,$link,$category,$sort,$thumbnail,$feature,$publish,$download_link);

                if ($process1 == true) 
                { 
                    $data['success_msg'] = 'Success Add New Video'; 
                }
                else
                {
                    $data['error_msg']= 'Failed Add New Video - Video Name Already Exists';
                } 
            }

            $process = $this->Video_model->getDataList();
            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');

            $config['total_rows'] = $jumlah_data;
            
            $this->pagination->initialize($config);     
            $data['video'] = $this->Video_model->getDataList($start,$limit);
            $this->load->view('materi/video/video_list',$data);

        } else {
            $process = $this->Video_model->getDataList();
            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');
            
            $config['total_rows'] = $jumlah_data;
            
            $this->pagination->initialize($config);     
            $data['video'] = $this->Video_model->getDataList($start,$limit);
            $this->load->view('materi/video/video_list',$data);
        }
    }

        public function video_detail($id='')
        {
            $data['page_title'] = 'Video Detail';
            $data['video_detail'] = $this->Video_model->getDataList('','',$id);
            $this->load->view('materi/video/video_detail', $data);
        }

        public function edit_video($id='')
        {
            if ($id!= '') 
            {

                $data['category'] = $this->category_model->getDataList();
                $data['video_detail'] = $this->Video_model->getDataList('','',$id);
                $this->load->view('materi/video/edit_video', $data);
            }
            else
            {
                $data['video'] = $this->Video_model->getDataList();
                $this->load->view('materi/video/video_list', $data);
            }
        }

        public function process_edit_video()
        {
            $start = '0';
            $limit = '10';

            $config['base_url'] = base_url().'materi/pengaturan_video/index';
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;

            if (!empty($this->input->post('edit-video')))
            {
                $data['page_title'] = 'Video List';
         
                $this->load->library('upload');
                    
                $id = $this->input->post('id');
                $title = $this->input->post('new-video-title');
                $description = $this->input->post('new-video-description');
                $link = $this->input->post('new-video-link');
                $category = $this->input->post('new-video-category');
                $sort = $this->input->post('new-sort');
                $feature_ = $this->input->post('new-feature');
                $publish = $this->input->post('new-publish');
                $download_link = $this->input->post('new-download-link');

                $thumbnail = $_FILES['thumbnail']['name'];

                $video_detail = $this->Video_model->getDataList('','',$id);

                if ($feature_ == true) {
                    $feature = 1;
                }
                else
                {
                    $feature = 0;
                }

                if (empty($thumbnail)) {
                    $thumbnail = $video_detail['dataObject']->thumbnail;

                    $process = $this->Video_model->edit($id,$title,$description,$link,$category,$sort,$thumbnail,$feature,$publish,$download_link);

                    if ($process == true) 
                    {
                        $data['success_msg'] = 'Success Edit Video';
                    }
                    else
                    {
                        $data['error_msg'] = 'Failed Edit Video - Video Title Already Exists';
                    }

                } else {
                    $this->upload->initialize($this->set_upload_options());
                    $up = $this->upload->do_upload('thumbnail');
                     if (!$up) {
                        $data['error_msg'] = $this->upload->display_errors();
                    } else {
                        $process = $this->Video_model->edit($id,$title,$description,$link,$category,$sort,$thumbnail,$feature,$publish,$download_link);

                        if ($process == true) 
                        {
                            $data['success_msg'] = 'Success Edit Video';
                        }
                        else
                        {
                            $data['error_msg'] = 'Failed Edit Video - Video Title Already Exists';
                        }
                    }
                }
                
                

                $process = $this->Video_model->getDataList();
                $jumlah_data = $process['countResult'];
                $this->load->library('pagination');

                $config['total_rows'] = $jumlah_data;
                
                $this->pagination->initialize($config);     
                $data['video'] = $this->Video_model->getDataList($start,$limit);
                $this->load->view('materi/video/video_list',$data);

            }
            else
            {
                $process = $this->Video_model->getDataList();
                $jumlah_data = $process['countResult'];
                $this->load->library('pagination');

                $config['total_rows'] = $jumlah_data;
                
                $this->pagination->initialize($config);     
                $data['video'] = $this->Video_model->getDataList($start,$limit);
                $this->load->view('materi/video/video_list',$data);

            }
        }


        public function delete_video($id='')
        {
            $process = $this->Video_model->delete($id);
            $start = '0';
            $limit = '10';
            $config['base_url'] = base_url().'materi/pengaturan_video/index';
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;

            if ($process == true) 
            {
                $data['success_msg'] = 'Success Delete Video';
            }
            else
            {
                $data['error_msg'] = 'Failed Delete Video';
            }

            $process = $this->Video_model->getDataList();
            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');

            $config['total_rows'] = $jumlah_data;
                
            $this->pagination->initialize($config);     
            $data['video'] = $this->Video_model->getDataList($start,$limit);
            $this->load->view('materi/video/video_list',$data);
        }
}