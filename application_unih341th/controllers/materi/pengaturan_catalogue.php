<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengaturan_catalogue extends CI_Controller 
{
    function __construct()
    {
	    parent::__construct();
        if(!$this->session->userdata('logged_in'))
        {
            redirect('');
        }
        
        $this->load->model(array('MMenu','catalogue_model', 'catalogue_category_model', 'GLobal_model'));
    }
    
    public function index($start = '0',$limit='10',$keyword='',$success_msg='',$error_msg='')
    {
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view'))
        {
            redirect('error','refresh');
        }

        $data['page_title'] = 'List Catalogue';
        
        if(!empty($this->input->post('search')))
        {
            $catalogue_keyword = $this->input->post('catalogue-keyword');

            $process = $this->catalogue_model->getDataList('','','','','','','','','','','','',$catalogue_keyword);
            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');

            $config['base_url'] = base_url().'materi/catalogue/index';
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;
            $config['total_rows'] = $jumlah_data;
            
            $this->pagination->initialize($config);     
           
            $data['success_msg'] = $success_msg;
            $data['error_msg'] = $error_msg;
            $data['keyword'] = $catalogue_keyword;
            $data['catalogue'] = $this->catalogue_model->getDataList($start,$limit,'','','','','','','','','','',$catalogue_keyword);
            $this->load->view('materi/catalogue/catalogue_list',$data);

        }
        else
        {
            $process = $this->catalogue_model->getDataList();
            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');

            $config['base_url'] = base_url().'materi/catalogue/index';
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;
            $config['total_rows'] = $jumlah_data;
            
            $this->pagination->initialize($config);        

            $data['success_msg'] = $success_msg;
            $data['error_msg'] = $error_msg;
            $data['keyword'] = $keyword;
            $data['catalogue'] = $this->catalogue_model->getDataList($start,$limit);
            $this->load->view('materi/catalogue/catalogue_list',$data);
        }
    }

    private function set_upload_options1()
    {   
        //upload an image options
        $config = array();
        // $config['upload_path'] = './images/catalogue_pages'; //local Path
        $config['upload_path'] = '/home/deploy/source/sohomlm/images/catalogue_pages';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']      = '10000';
        $config['overwrite']     = FALSE;

        return $config;
    }

    private function set_upload_options2()
    {   
        //upload an image options
        $config = array();
        // $config['upload_path'] = './images/catalogue_thumbnail'; //local Path
        $config['upload_path'] = '/home/deploy/source/sohomlm/images/catalogue_thumbnail';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']      = '10000';
        $config['overwrite']     = FALSE;

        return $config;
    }

    private function set_upload_options3()
    {   
        //upload an image options
        $config = array();
        // $config['upload_path'] = './catalogue_file'; //local Path
        $config['upload_path'] = '/home/deploy/source/sohomlm/catalogue_file';
        $config['allowed_types'] = 'pdf';
        $config['max_size']      = '1000000';
        $config['overwrite']     = FALSE;

        return $config;
    }

    public function add_catalogue()
    {
        $data['page_title'] = 'Add Catalogue';
        $data['category'] = $this->catalogue_category_model->getDataList();
        
        $this->load->view('materi/catalogue/add_catalogue',$data);
        
    }

    public function process_add_catalogue()
    {
        $data['page_title'] = 'Catalogue List';

        $start = '0';
        $limit = '10';

        if (!empty($this->input->post('add-catalogue'))) 
        {
            $this->load->library('upload');

            $title = $this->input->post('catalogue-title');
            $start_period = $this->input->post('start-date');
            $end_period = $this->input->post('end-date');
            $sort = $this->input->post('sort');
            $category_id = $this->input->post('catalogue-category');

            $thumbnail = $_FILES['thumbnail']['name'];
            $pdf_file = $_FILES['catalogue-pdf']['name'];

            $this->upload->initialize($this->set_upload_options2());
            $up1 = $this->upload->do_upload('thumbnail');

            if (!$up1) {
                $data['error_msg'] = $this->upload->display_errors();
                //$this->index($start,$limit,'','',$error_msg);
            }
            else
            {
                $this->upload->initialize($this->set_upload_options3());
                $up2 = $this->upload->do_upload('catalogue-pdf');

                if (!$up2) {
                    $data['error_msg'] = $this->upload->display_errors();
                    //$this->index($start,$limit,'','',$error_msg);
                }
                else
                {
                    // echo $title ."<br>" .$start_period ."<br>" .$end_period ."<br>" .$pdf_file ."<br>" .$thumbnail ."<br>" .$sort;
                    
                        $files = $_FILES;
                        $cpt = count($_FILES['img']['name']);
                        if ($cpt > 0) 
                        {
                            $process1 = $this->catalogue_model->create_main($title,$start_period,$end_period,$pdf_file,$thumbnail,$sort,$category_id);
                            if($process1 != false)
                            {
                                $pages = $this->input->post('page');
                                for($i=0; $i<$cpt; $i++)
                                {           
                                    $_FILES['img']['name']= $files['img']['name'][$i];
                                    $_FILES['img']['type']= $files['img']['type'][$i];
                                    $_FILES['img']['tmp_name']= $files['img']['tmp_name'][$i];
                                    $_FILES['img']['error']= $files['img']['error'][$i];
                                    $_FILES['img']['size']= $files['img']['size'][$i];    

                                    $this->upload->initialize($this->set_upload_options1());
                                    $up3 = $this->upload->do_upload('img');

                                    if (!$up3) {
                                        $data['error_msg'] = $this->upload->display_errors();
                                        //$this->index($start,$limit,'','',$error_msg);
                                    } else {
                                        $process2 = $this->catalogue_model->create_page($process1,$pages[$i],$_FILES['img']['name']);

                                        if($process2 == true)
                                        {
                                            $data['success_msg'] = "Success Insert New Catalogue";
                                            //$this->index($start,$limit,'',$success_msg);
                                        } else {
                                            $data['error_msg'] = $this->upload->display_errors();
                                            //$this->index($start,$limit,'','',$error_msg);
                                        }

                                    }
                                }
                            } else {
                                $data['error_msg'] = "Failed Insert New Catalogue - Title Already Exist";
                                //$this->index($start,$limits,'','',$error_msg);
                            }

                        } else {
                            $data['error_msg'] = "Please Insert Catalogue Pages";
                            //$this->index($start,$limit,'','',$error_msg);
                        }
    

                }
            }
            
            $process = $this->catalogue_model->getDataList();
            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');

            $data['catalogue'] = $this->catalogue_model->getDataList($start,$limit);
            $data['category'] = $this->catalogue_category_model->getDataList();
            $config['base_url'] = base_url().'materi/catalogue/index';
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;
            $config['total_rows'] = $jumlah_data;
                
            $this->pagination->initialize($config);
            $this->load->view('materi/catalogue/catalogue_list',$data);
            
        } 
        else
        {
            // $data['page_title'] = 'Catalogue List';
            $this->load->view('materi/catalogue/catalogue_list',$data);        
        }
    }

    public function catalogue_detail($id)
    {
        $data['page_title'] = 'Catalogue Detail';
        $data['catalogue_detail'] = $this->catalogue_model->getDataList('','',$id);
        $data['catalogue_pages'] = $this->catalogue_model->getDetail($id);
        $this->load->view('materi/catalogue/catalogue_detail', $data);
    }

    public function edit_catalogue($id)
    {
        $data['page_title'] = 'Edit Catalogue';
        $data['catalogue_detail'] = $this->catalogue_model->getDataList('','',$id);
        $data['catalogue_pages'] = $this->catalogue_model->getDetail($id);
        $data['category'] = $this->catalogue_category_model->getDataList();
        $this->load->view('materi/catalogue/edit_catalogue', $data);
    }

    public function process_edit_catalogue()
    {
        $start = '0';
        $limit = '10';

        if (!empty($this->input->post('edit-catalogue'))) 
        {
            $this->load->library('upload');

            $id = $this->input->post('id');
            $title = $this->input->post('new-catalogue-title');
            $start_period = $this->input->post('new-start-date');
            $end_period = $this->input->post('new-end-date');
            $sort = $this->input->post('new-sort');
            $category_id = $this->input->post('new-catalogue-category');

            $data_ext = $this->catalogue_model->getDataList('','',$id);

            $thumbnail = $_FILES['thumbnail']['name'];
            $pdf_file = $_FILES['catalogue-pdf']['name'];

        
            // echo $id .'<br>' .$title .'<br>' .$start_period .'<br>' .$end_period .'<br>' .$sort .'<br>' .$category_id .'<br>' .$thumbnail .'<br>' .$pdf_file;

            if (empty($thumbnail)) {
                $thumbnail = $data_ext['dataObject']->thumbnail;
            } else {
                $this->upload->initialize($this->set_upload_options2());
                $up1 = $this->upload->do_upload('thumbnail');

                if (!$up1) {
                    $data['error_msg'] = $this->upload->display_errors();
                    
                }
            }

            if (empty($pdf_file)) {
                $pdf_file = $data_ext['dataObject']->pdf_file;
            } else {
                $this->upload->initialize($this->set_upload_options3());
                $up2 = $this->upload->do_upload('catalogue-pdf');

                if (!$up2) {
                    $data['error_msg'] = $this->upload->display_errors();
                }
            }

            if (empty($data['error_msg'])) {

                //echo "berhasil";
    
                $files = $_FILES;
                $cpt = count($_FILES['img']['name']);
                if ($cpt > 0) 
                {
                    $process1 = $this->catalogue_model->edit_main($id,$title,$start_period,$end_period,$pdf_file,$thumbnail,$sort,$category_id);
                    if($process1 != false)
                    {
                        $delete_process = $this->catalogue_model->clear_field($id);

                        if ($delete_process != false) {
                            $pages = $this->input->post('page');
                            $old_pic = $this->input->post('pic');
                            $error_upload = array();
                            // var_dump($pages);
                            // var_dump($_FILES['img']['name']);
                            for($i=0; $i<$cpt; $i++)
                            {           
                                $_FILES['img']['name']= $files['img']['name'][$i];
                                $_FILES['img']['type']= $files['img']['type'][$i];
                                $_FILES['img']['tmp_name']= $files['img']['tmp_name'][$i];
                                $_FILES['img']['error']= $files['img']['error'][$i];
                                $_FILES['img']['size']= $files['img']['size'][$i]; 

                                // var_dump($pages);

                                if (empty($_FILES['img']['name'])) {

                                    // echo $id ."<br>" .$pages[$i] ."<br>" .$old_pic[$i] ;

                                    $process2 = $this->catalogue_model->create_page($id,$pages[$i],$old_pic[$i]);

                                    if($process2 == false)
                                    {
                                       $data['error_msg'] = $this->upload->display_errors();
                                    } else {
                                        $data['success_msg'] = "Success Edit Catalogue " .$title;
                                    } 

                                } else {
                                    $this->upload->initialize($this->set_upload_options1());
                                    $up3 = $this->upload->do_upload('img');

                                    if (!$up3) {
                                        $data['error_msg'] = $this->upload->display_errors();
                                    } else {
                                        
                                        $process2 = $this->catalogue_model->create_page($id,$pages[$i],$_FILES['img']['name']);

                                        if($process2 == true)
                                        {
                                            $data['success_msg'] = "Success Edit Catalogue " .$title;
                                        } else {
                                            $data['error_msg'] = $this->upload->display_errors();
                                        }
                                        

                                    }
                                }
                                        
                            }


                        } else {
                            $data['error_msg'] = "Failed Clear Catalogue Field";
                        }

                    } else {
                        $data['error_msg'] = "Failed Edit New Catalogue - Title Already Exist";       
                    }

                } else {
                    $data['error_msg'] = "Please Insert Catalogue Pages";
                    
                }

            $process = $this->catalogue_model->getDataList();
            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');

            $config['base_url'] = base_url().'materi/catalogue/index';
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;
            $config['total_rows'] = $jumlah_data;
                
            $this->pagination->initialize($config);

            $data['catalogue'] = $this->catalogue_model->getDataList($start,$limit);

           $this->load->view('materi/catalogue/catalogue_list',$data);
        }
        else
        {
            $process = $this->catalogue_model->getDataList();
            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');

            $config['base_url'] = base_url().'materi/catalogue/index';
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;
            $config['total_rows'] = $jumlah_data;
                
            $this->pagination->initialize($config);

            $data['catalogue'] = $this->catalogue_model->getDataList($start,$limit);

          $this->load->view('materi/catalogue/catalogue_list',$data);
        }
        }

    }

    public function delete_catalogue($id)
    {
        $process = $this->catalogue_model->delete($id);
        $start = '0';
        $limit = '10';

        $process = $this->catalogue_model->getDataList();
        $jumlah_data = $process['countResult'];
        $this->load->library('pagination');

        $data['catalogue'] = $this->catalogue_model->getDataList($start,$limit);
        $config['base_url'] = base_url().'materi/catalogue/index';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $jumlah_data;
            
        $this->pagination->initialize($config);

        if ($process == true) {
            $data['success_msg'] = 'Success Delete Catalogue';
        } else {
            $data['error_msg'] = 'Failed Delete Catalogue';
        }

        $this->load->view('materi/catalogue/catalogue_list',$data);
    }
}
