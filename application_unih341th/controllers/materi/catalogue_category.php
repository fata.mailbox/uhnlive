<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Catalogue_category extends CI_Controller 
{
    function __construct()
    {
	    parent::__construct();
        if(!$this->session->userdata('logged_in'))
        {
            redirect('');
        }
        
        $this->load->model(array('MMenu','catalogue_category_model', 'GLobal_model'));
    }
    
    public function index($start = '0',$limit='10',$keyword='',$success_msg ='', $error_msg='')
    {
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view'))
        {
            redirect('error','refresh');
        }

        $data['page_title'] = 'List Category';
        
        if(!empty($this->input->post('search')))
        {
            $category_keyword = $this->input->post('category-keyword');

            $process = $this->catalogue_category_model->getDataList('','','','','',$category_keyword);
            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');

            $config['base_url'] = base_url().'materi/catalogue_category/index';
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;
            $config['total_rows'] = $jumlah_data;
            
            $this->pagination->initialize($config);     
           
            $data['success_msg'] = '';
            $data['error_msg'] = '';
            $data['keyword'] = $category_keyword;
            $data['category'] = $this->catalogue_category_model->getDataList($start,$limit,'','','',$category_keyword);
            $this->load->view('materi/catalogue/category/category_list',$data);

        }
        else
        {
            $process = $this->catalogue_category_model->getDataList();
            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');

            $config['base_url'] = base_url().'materi/catalogue_category/index';
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;
            $config['total_rows'] = $jumlah_data;
            
            $this->pagination->initialize($config);        

            $data['success_msg'] = $success_msg;
            $data['error_msg'] = $error_msg;
            $data['keyword'] = '';
            $data['category'] = $this->catalogue_category_model->getDataList($start,$limit);
            $this->load->view('materi/catalogue/category/category_list',$data);
        }
    }

    public function add_category()
    {
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view'))
        {
            redirect('error','refresh');
        }

        $start = '0';
        $limit = '10';
        
        $process = $this->catalogue_category_model->getDataList();
        $jumlah_data = $process['countResult'];
        $this->load->library('pagination');

        $data['category'] = $this->catalogue_category_model->getDataList($start,$limit);
        $config['base_url'] = base_url().'materi/catalogue_category/index';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $jumlah_data;
            
        $this->pagination->initialize($config);

        if (!empty($this->input->post('add-category'))) 
        {
            $name = $this->input->post('category-name');
            $sort = $this->input->post('sort');

            $data['page_title'] = 'Add Category';

            $process = $this->catalogue_category_model->create($name,$sort);

            if ($process == true) 
            {
                $data['success_msg'] = 'Success Add New Category'; 
                //$this->index($start,$limit,'',$success_msg);
            }
            else
            {
                $data['error_msg'] = 'Failed Add New Category - Category Name Already Exists';
                //$this->index($start,$limit,'','',$error_msg);
            }

            $this->load->view('materi/catalogue/category/add_category',$data);
        }
        else
        {
            $data['page_title'] = 'Add Category';
            $this->load->view('materi/catalogue/category/add_category',$data);
        }
            
    }

    public function category_detail($id='')
    {
        $data['page_title'] = 'Detail Category';
        $data['category_detail'] = $this->catalogue_category_model->getDataList('','',$id);
        $this->load->view('materi/catalogue/category/category_detail', $data);
    }

    public function edit_category($id='')
        {
            if ($id != '') 
            {
                $data['page_title'] = 'Edit Category';
                $data['category_detail'] = $this->catalogue_category_model->getDataList('','',$id);
                $this->load->view('materi/catalogue/category/edit_category', $data);
            }
            else
            {
                $data['page_title'] = 'Category List';
                $data['category'] = $this->catalogue_category_model->getDataList();
                $this->load->view('materi/catalogue/category/category_list', $data);
            }
             $this->load->view('materi/catalogue/category/add_category',$data);
        }

    public function process_edit_category()
    {
        $start = '0';
        $limit = '10';

        $process = $this->catalogue_category_model->getDataList();
        $jumlah_data = $process['countResult'];
        $this->load->library('pagination');

        $data['category'] = $this->catalogue_category_model->getDataList($start,$limit);
        $config['base_url'] = base_url().'materi/catalogue_category/index';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $jumlah_data;
            
        $this->pagination->initialize($config);

        $data['page_title'] = 'Category List';
        if (!empty($this->input->post('edit-category'))) 
        {
            $id = $this->input->post('id');
            $name = $this->input->post('new-category-name');
            $sort = $this->input->post('new-sort');

            $process = $this->catalogue_category_model->edit($id,$name,$sort);

            if ($process == true) 
            {
                $data['success_msg'] = 'Success Edit Category';
            }
            else
            {
                $data['error_msg'] = 'Failed Edit Category - Category Name Already Exists';
            }
             $this->load->view('materi/catalogue/category/add_category',$data);
        }
        else
        {
             $this->load->view('materi/catalogue/category/add_category',$data);
        }
    }

        public function delete_category($id='')
        {
            $process = $this->catalogue_category_model->delete($id);
            $start = '0';
            $limit = '10';

            $process = $this->catalogue_category_model->getDataList();
            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');

            $data['category'] = $this->catalogue_category_model->getDataList($start,$limit);
            $config['base_url'] = base_url().'materi/catalogue_category/index';
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;
            $config['total_rows'] = $jumlah_data;
                
            $this->pagination->initialize($config);

            if ($process == true) 
            {
                $data['success_msg'] = 'Success Delete Category';
            }
            else
            {
                $data['error_msg'] = 'Failed Delete Category';
            }

             $this->load->view('materi/catalogue/category/add_category',$data);
        }

}