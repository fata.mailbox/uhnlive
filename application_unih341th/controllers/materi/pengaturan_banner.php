<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengaturan_banner extends CI_Controller 
{
    function __construct()
    {
	    parent::__construct();
        if(!$this->session->userdata('logged_in'))
        {
            redirect('');
        }
        
        $this->load->model(array('MMenu','banner_model', 'banner_category_model', 'GLobal_model'));
    }
    
    public function index($start = '0',$limit='10',$keyword='',$success_msg='',$error_msg='')
    {
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view'))
        {
            redirect('error','refresh');
        }

        $data['page_title'] = 'List Banner';

		if(!empty($this->input->post('search')))
		{
			$banner_keyword = $this->input->post('banner-keyword');

            $process = $this->banner_model->getDataList('','','','','','','','',$banner_keyword);
            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');

            $config['base_url'] = base_url().'materi/banner/index';
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;
            $config['total_rows'] = $jumlah_data;
            
            $this->pagination->initialize($config);     
           
            $data['success_msg'] = $success_msg;
            $data['error_msg'] = $error_msg;
            $data['keyword'] = $banner_keyword;
            $data['banner'] = $this->banner_model->getDataList($start,$limit,'','','','','','',$banner_keyword);
            $this->load->view('materi/banner/banner_list',$data);
		}
		else
		{
			$process = $this->banner_model->getDataList();
            $jumlah_data = $process['countResult'];
            $this->load->library('pagination');

            $config['base_url'] = base_url().'materi/banner/index';
            $config['per_page'] = 10;
            $config['uri_segment'] = 4;
            $config['total_rows'] = $jumlah_data;
            
            $this->pagination->initialize($config);        

            $data['success_msg'] = $success_msg;
            $data['error_msg'] = $error_msg;
            $data['keyword'] = $keyword;
            $data['banner'] = $this->banner_model->getDataList($start,$limit);
            $this->load->view('materi/banner/banner_list',$data);
		}
	}

	public function add_banner()
	{
		$data['page_title'] = 'Add Banner';
		$data['category'] = $this->banner_category_model->getDataList();
		$this->load->view('materi/banner/add_banner',$data);
	}

	 private function set_upload_options1()
     {   
        //upload an image options
        $config = array();
        // $config['upload_path'] = './images/banner'; // local path
        $config['upload_path'] = '/home/deploy/source/sohomlm/images/banner'; //server path

        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']      = '10000';
        $config['overwrite']     = FALSE;

        return $config;
     }

     private function set_upload_options2()
     {   
        //upload an image options
        $config = array();
        // $config['upload_path'] = './banner_file'; // local path
        $config['upload_path'] = '/home/deploy/source/sohomlm/banner_file'; //server path

        $config['allowed_types'] = 'pdf';
        $config['max_size']      = '10000';
        $config['overwrite']     = FALSE;

        return $config;
     }

	public function process_add_banner()
	{
		$start = '0';
        $limit = '10';
        $this->load->library('pagination');

        $config['base_url'] = base_url().'materi/banner/index';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;

		if (!empty($this->input->post('add-banner'))) 
		{
			$data['page_title'] = 'Banner List';
            
            $this->load->library('upload');
			$title = $this->input->post('title');
			$category = $this->input->post('category');	
			$file = $_FILES['banner']['name'];
            $pdf = $_FILES['pdf']['name'];
			$publish = $this->input->post('publish');	
			$sort = $this->input->post('sort');

			// var_dump($title .'<br>' .$category .'<br>' .$file .'<br>' .$publish .'<br>' .$sort);

			$this->upload->initialize($this->set_upload_options1());
            $up1 = $this->upload->do_upload('banner');

            if (!$up1) {
            	$data['error_msg'] = $this->upload->display_errors();
            } else {
                $this->upload->initialize($this->set_upload_options2());
                $up2 = $this->upload->do_upload('pdf');

                if (!$up2) {
                    $data['error_msg'] = $this->upload->display_errors();
                } else {
                    $process = $this->banner_model->create($title,$file,$category,$publish,$sort,$pdf);

                    if ($process == TRUE) {
                        $data['success_msg'] = 'Success Add New Banner';
                    } else {
                        $data['error_msg']= 'Failed Add New Banner - Banner Name Already Exists';
                    }
                }
            	
            }

            $process = $this->banner_model->getDataList();
            $jumlah_data = $process['countResult'];
            $config['total_rows'] = $jumlah_data;
            
            $this->pagination->initialize($config);        
            $data['banner'] = $this->banner_model->getDataList($start,$limit);
            $this->load->view('materi/banner/banner_list',$data);

		}
		else
		{
			$process = $this->banner_model->getDataList();
            $jumlah_data = $process['countResult'];
            $config['total_rows'] = $jumlah_data;
            
            $this->pagination->initialize($config);        
            $data['banner'] = $this->banner_model->getDataList($start,$limit);
            $this->load->view('materi/banner/banner_list',$data);
		}
	}

	public function banner_detail($id='')
	{
		$data['page_title'] = 'Banner Detail';
		$data['category'] = $this->banner_category_model->getDataList();
        $data['banner_detail'] = $this->banner_model->getDataList('','',$id);
        $this->load->view('materi/banner/banner_detail', $data);
	}

	public function edit_banner($id='')
	{
		$data['page_title'] = 'Edit Banner';
		$data['category'] = $this->banner_category_model->getDataList();
        $data['banner_detail'] = $this->banner_model->getDataList('','',$id);
        // var_dump($data['banner_detail']);
        $this->load->view('materi/banner/edit_banner', $data);
	}

	public function process_edit_banner()
	{
        $start = '0';
        $limit = '10';
        $this->load->library('pagination');

        $config['base_url'] = base_url().'materi/banner/index';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;

		 if (!empty($this->input->post('edit-banner')))
         {
         	$data['page_title'] = 'Banner List';
            
            $this->load->library('upload');
            $id = $this->input->post('id');
			$title = $this->input->post('new-title');
			$category = $this->input->post('new-category');	
			$publish = $this->input->post('new-publish');	
			$sort = $this->input->post('new-sort');

            $file = $_FILES['banner']['name'];
            $pdf = $_FILES['pdf']['name'];

            $banner_detail = $this->banner_model->getDataList('','',$id);

			// var_dump($title .'<br>' .$category .'<br>' .$file .'<br>' .$publish .'<br>' .$sort);

            if (empty($file)){
            	$file = $banner_detail['dataObject']->file_name;
            	
            } else {
                $this->upload->initialize($this->set_upload_options1());
                $up1 = $this->upload->do_upload('banner');

                if (!$up1) {
                    $data['error_msg'] = $this->upload->display_errors();
                }


            }

            if (empty($pdf)){
                $pdf = $banner_detail['dataObject']->pdf_file;

                $process = $this->banner_model->edit($id,$title,$file,$category,$publish,$sort,$pdf);

                if ($process == TRUE) {
                    $data['success_msg'] = 'Success Edit Banner'; 
                } else {
                    $data['error_msg'] = 'Failed Edit Banner - Banner Name Already Exists';
                }
            } else {
                $this->upload->initialize($this->set_upload_options2());
                $up2 = $this->upload->do_upload('pdf');

                if (!$up2) {
                    $data['error_msg'] = $this->upload->display_errors();
                } else {
                    $process = $this->banner_model->edit($id,$title,$file,$category,$publish,$sort,$pdf);

                    if ($process == TRUE) {
                        $data['success_msg'] = 'Success Edit Banner'; 
                    } else {
                        $data['error_msg'] = 'Failed Edit Banner - Banner Name Already Exists';
                    }
                }
            }

            $process = $this->banner_model->getDataList();
            $jumlah_data = $process['countResult'];
            $config['total_rows'] = $jumlah_data;
            
            $this->pagination->initialize($config);        
            $data['banner'] = $this->banner_model->getDataList($start,$limit);
            $this->load->view('materi/banner/banner_list',$data);
         }
         else
         {
            $process = $this->banner_model->getDataList();
            $jumlah_data = $process['countResult'];
            $config['total_rows'] = $jumlah_data;
            
            $this->pagination->initialize($config);        
            $data['banner'] = $this->banner_model->getDataList($start,$limit);
            $this->load->view('materi/banner/banner_list',$data);
         }
	}

	public function delete_banner($id)
	{
		$process = $this->banner_model->delete($id);
        $start = '0';
        $limit = '10';

        if ($process == true) 
        {
           $data['success_msg'] = 'Success Delete Banner';
        }
        else
        {
           $data['error_msg'] = 'Failed Delete Banner';
        }

        $process = $this->banner_model->getDataList();
        $jumlah_data = $process['countResult'];
        $config['total_rows'] = $jumlah_data;
            
        $this->pagination->initialize($config);        
        $data['banner'] = $this->banner_model->getDataList($start,$limit);
        $this->load->view('materi/banner/banner_list',$data);
	}
}
