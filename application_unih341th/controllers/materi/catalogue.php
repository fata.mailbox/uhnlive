<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Catalogue extends CI_Controller 
{
    function __construct()
    {
	    parent::__construct();
        if(!$this->session->userdata('logged_in'))
        {
            redirect('');
        }
        
        $this->load->model(array('MMenu','catalogue_model', 'GLobal_model'));
    }
    
    public function index($start = '0',$limit='10',$keyword='')
    {
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view'))
        {
            redirect('error','refresh');
        }
    }

    public function e_catalogue()
    {
        $data['page_title'] = "E-Catalogue List";
        $process1 = $this->catalogue_model->getDataList('','','','','','','','','','1');
        $catalogue_id = $process1['data'][0]['catalogue_id'];

        $process2 = $this->catalogue_model->select_page($catalogue_id);
        $ext_data = $this->catalogue_model->getDataList('','',$catalogue_id);
        $data['download_link'] = $ext_data['dataObject']->pdf_file;
        $data['catalogue'] = $process1;
        $data['page'] = $process2;
        $this->load->view('materi/catalogue/master_catalogue',$data);
    }

    public function e_news_letter()
    {
        $data['catalogue'] = $this->catalogue_model->getDataList('','','','','','','','','','2');
    }

    public function e_health_and_beauty_guide()
    {
        $data['catalogue'] = $this->catalogue_model->getDataList('','','','','','','','','','3');
    }

    public function e_member_guide_book()
    {
        $data['catalogue'] = $this->catalogue_model->getDataList('','','','','','','','','','4');
    }

    public function detail_catalogue($id='')
    {   
        $data['page_title'] = "E-Catalogue List";
        $process1 = $this->catalogue_model->getDataList('','','','','','','','','','1','1','1');

        $process2 = $this->catalogue_model->select_page($id);
        $data['catalogue'] = $process1;
        $data['page'] = $process2;
        
        $this->load->view('materi/catalogue/master_catalogue',$data);
    }
}
