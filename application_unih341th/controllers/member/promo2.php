<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Promo2 extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){ 
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MPromo'));
    }
    
    public function index(){
		
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
			redirect('error','refresh');
		}
		
		$this->load->library(array('form_validation'));

		$this->form_validation->set_rules('member_id','','');
		$this->form_validation->set_rules('name','','');

		if($this->form_validation->run()){
			if($this->session->userdata('userid')>100){
				$id = $this->session->userdata('userid');
			}else{
				$id = $this->input->post('member_id');
			}
			
			/*
			$row = array();
			$row["datapribadi"] = $this->MPromo->getDataPribadi($id);
			$data["results"] = $this->MPromo->getTopThree($id);
			$row2 = array(); 
			$row2["getPoin"]= $this->MPromo->getPlusPoin($id);
			*/
			
			$row = array();
			$row["uig"] = $this->MPromo->uigResult2($id);
			//$row2["ps"] = $this->MPromo->currShop($id);
			$data["results"] = $this->MPromo->uigDet2($id);
        }else{
			$row["uig"] = false;
			//$row2["ps"] = false;
			$data["results"] = false;
		}
		$data['row'] = $row;
		//$data['row2'] = $row2;

		$data['page_title'] = 'Promo UIG2 2010';
        $this->load->view('member/vpromo2',$data);
	}
}
?>