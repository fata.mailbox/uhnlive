<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Hbonus extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('MBonus','MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('periode','','');
        $this->form_validation->set_rules('periode2','','');
        $this->form_validation->set_rules('sort','','');
        
        if($this->form_validation->run()){
            $data['results']=$this->MBonus->sumBonus($this->input->post('periode'),$this->input->post('periode2'),$this->input->post('sort'));
            $data['total']=$this->MBonus->getTotalCA($this->input->post('periode'),$this->input->post('periode2'));
        }else{
            $data['results']=false;
            $data['total']=false;
        }
        
        $data['dropdown'] = $this->MBonus->getDropDrownPeriode();
        $data['page_title'] = 'Summary Bonus';
        $this->load->view('member/summary_bonus',$data);
    }
        
}
?>