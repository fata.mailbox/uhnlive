<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Level extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MBonus','MMenu', 'GLobal_model2'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        		
        $this->form_validation->set_rules('periode','','');
        $this->form_validation->set_rules('member_id','','');
        $this->form_validation->set_rules('name','','');
        if($this->form_validation->run()){
            $memberid = $this->input->post('member_id');
            $data['results']=$this->MBonus->getJenjang($memberid,$this->input->post('periode'));
			//var_dump($data['results'][0]['id']); die();
			$this->session->set_userdata(array('jenjangMemberId' => $memberid, 'rowIdResults' => $data['results'][0]['id']));
			//$this->session->set_userdata('rowIdResults', $data['results'][0]['id']);
            $data['rs']=$this->MBonus->kualifikasiLeader($this->input->post('periode'),$memberid);
            if(!$memberid){
                $data['results2']=$this->MBonus->countLeader($this->input->post('periode'));
            }else{
                $data['hightjenjang']=$this->MBonus->get_hightjenjang($memberid);
				//20150901 ASP Start
				if($data['hightjenjang']->jenjangbaru > 5){
					$getTgl = $this->MBonus->getTglSL($memberid);
           			$data['rs']=$this->MBonus->kualifikasiLeaderNew($this->input->post('periode'),$memberid,$getTgl->tgl,$getTgl->tgl_akhir);
					$data['rs_sl']=$this->MBonus->kualifikasiSuperLeader($this->input->post('periode'),$memberid);	
				}
				//20150901 ASP End
            }
        }else{
            $memberid = $this->session->userdata('userid');
            $data['row']=$this->MBonus->getPS($memberid,'');
            $data['rs']=$this->MBonus->kualifikasiLeader('',$memberid);
            $data['results']=$this->MBonus->getJenjang($memberid,'');
            $data['hightjenjang']=$this->MBonus->get_hightjenjang($memberid);
        }
        
        $data['dropdown'] = $this->MBonus->get_dropdown();
        $data['page_title'] = 'Member Position';
		//$this->load->view('member/jenjang_history',$data);
		//20150901 ASP Start
        $this->load->view('member/jenjang_history_new',$data);
		//20150901 ASP End
    }
    
	
	// 20200110 Damsul
	public function edit(){
	    //ASP Start 20200221
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        // EOF ASP 20200221
		$this->session->unset_userdata('jenjangMemberId');
		$this->session->unset_userdata('rowIdResults');
		$data = array();
        $data['memberID'] = $this->uri->segment(4);
		$data['results']=$this->MBonus->getJenjang($this->uri->segment(4),'')[0];
        $data['dropdown'] = $this->MBonus->get_dropdown();
		$data['memberDetail'] = $this->MBonus->getPS($this->uri->segment(4),'');
		$data['jenjangList'] = $this->GLobal_model2->get_listJenjangAll();
		$data['page_title'] = 'Member Position Edit';
		$this->load->view('member/jenjang_history_edit',$data);
	}
    
	public function insert(){
        //ASP Start 20200221
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
		}
		// EOF ASP 20200221
		$jenjangBaru = $this->input->post('jBaru');
		$idJenjangBaru = $this->input->post('idJenjangBaru');
		$idJenjangLama = $this->input->post('idJenjangLama');
		$period = $this->input->post('periode');
		$periodTerakhir = date("Y-m-d", strtotime('-1 day +11 month', strtotime($period)));
		$sponsorId = $this->input->post('sponsorId');
		$memberId= $this->input->post('memberId');
		$date = date("Y-m-d");
		$datetime = date("Y-m-d h:i:s");
		
		$selectPGS = "SELECT * FROM pgs_bulanan WHERE member_id = '". $memberId ."' AND tgl = '". $period ."'";
		$qPGS = $this->db->query($selectPGS)->result();
		
		$dataSetMember = array(
			'jenjang_id' => $jenjangBaru,
			'tgljenjang' => $periodTerakhir,
			'updated' => $date,
			'updatedby' => $this->session->userdata('user')
		);
		
		if($jenjangBaru > $idJenjangLama){
			$dataSetMember['jenjang_id2'] = $jenjangBaru;
			$dataSetMember['highjenjang'] = $jenjangBaru;
		}
		
		$q = $this->db->where('id', $memberId)->update('member', $dataSetMember);
		if($q){
			$dataSetJenjang = array(
				'member_id' => $memberId,
				'sponsor_id' => $sponsorId,
				'jenjangbaru' => $jenjangBaru,
				'jenjanglama' => $idJenjangBaru,
				'tgl' => $period,
				'tglakhir' => $periodTerakhir
				
			);
			$qInsert = $this->db->insert('jenjang_history', $dataSetJenjang);
			
			if(count($qPGS) > 0){
				$dataSetPGS = array(
					'jenjang_id' => $jenjangBaru,
				);
				if($jenjangBaru > $idJenjangLama){
					$dataSetPGS['jenjang_id2'] = $jenjangBaru;
				}
				
				if($jenjangBaru > $idJenjangLama){
					$dataSetMember['jenjang_id2'] = $jenjangBaru;
					$dataSetMember['highjenjang'] = $jenjangBaru;
				}
				$q = $this->db->where(array('member_id' => $memberId, 'tgl' => $period))->update('pgs_bulanan', $dataSetPGS);
			}
			$this->session->set_flashdata('message', 'Update ' .$memberId. ' Member jenjang successfully');
			redirect('member/level');
		}
	}
}
?>