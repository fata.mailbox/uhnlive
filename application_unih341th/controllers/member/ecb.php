<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ecb extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('MBonus','MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('periode','','');
        
        if($this->form_validation->run()){
            $data['results']=$this->MBonus->getECB($this->input->post('periode'));
        }else{
            $data['results']=false;
        }
        
        $data['dropdown'] = $this->MBonus->getDropDrownPeriode();
        $data['page_title'] = 'Eternal Cyclone Bonus';
        $this->load->view('member/eternal_cyclone_bonus',$data);
    }
        
}
?>