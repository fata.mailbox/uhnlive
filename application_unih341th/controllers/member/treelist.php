<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Treelist extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){ 
            redirect('','refresh');
        }
	
        $this->load->model(array('MMenu','MTree'));
    }
    
    public function index(){
        $this->view();
    }
    public function view(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library('form_validation');
        
	$this->form_validation->set_rules('member_id','member id','required');
        $this->form_validation->set_rules('name','','');
	$this->form_validation->set_rules('username','','');
        
        $this->form_validation->set_rules('g1','','');
        $this->form_validation->set_rules('g2','','');
        $this->form_validation->set_rules('g3','','');
        $this->form_validation->set_rules('g4','','');
        $this->form_validation->set_rules('g5','','');
	
	$this->form_validation->set_rules('g6','','');
	$this->form_validation->set_rules('g7','','');
	$this->form_validation->set_rules('g8','','');
	$this->form_validation->set_rules('g9','','');
	$this->form_validation->set_rules('g10','','');
        
        if($this->session->userdata('group_id') > 100)$memberid = $this->session->userdata('userid');
        else{
	    /*
	    $rs = $this->MGlobal['lft']et_memberid($this->input->post('member_id'));
	    $memberid = $rs->id;
	    */
	    $memberid = $this->input->post('member_id');
	}
        
        $row=$this->MTree->get_member($memberid);
        if($this->form_validation->run()){
            if($row){
                $generasi_1 = $this->input->post('g1');
                $generasi_2 = $this->input->post('g2');
                $generasi_3 = $this->input->post('g3');
                $generasi_4 = $this->input->post('g4');
                $generasi_5 = $this->input->post('g5');
		$generasi_6 = $this->input->post('g6');
		$generasi_7 = $this->input->post('g7');
		$generasi_8 = $this->input->post('g8');
		$generasi_9 = $this->input->post('g9');
		$generasi_10 = $this->input->post('g10');
		
		$data['g1'] = $this->MTree->get_network_generasi($generasi_1,$row['level'],$row['lft'],$row['rgt']);
		$data['g2'] = $this->MTree->get_network_generasi($generasi_2,$row['level'],$row['lft'],$row['rgt']);
		$data['g3'] = $this->MTree->get_network_generasi($generasi_3,$row['level'],$row['lft'],$row['rgt']);
		$data['g4'] = $this->MTree->get_network_generasi($generasi_4,$row['level'],$row['lft'],$row['rgt']);
		$data['g5'] = $this->MTree->get_network_generasi($generasi_5,$row['level'],$row['lft'],$row['rgt']);
		
		$data['g6'] = $this->MTree->get_network_generasi($generasi_6,$row['level'],$row['lft'],$row['rgt']);
		$data['g7'] = $this->MTree->get_network_generasi($generasi_7,$row['level'],$row['lft'],$row['rgt']);
		$data['g8'] = $this->MTree->get_network_generasi($generasi_8,$row['level'],$row['lft'],$row['rgt']);
		$data['g9'] = $this->MTree->get_network_generasi($generasi_9,$row['level'],$row['lft'],$row['rgt']);
		$data['g10'] = $this->MTree->get_network_generasi($generasi_10,$row['level'],$row['lft'],$row['rgt']);
		
            }
       }else {
            $generasi_1 = 1;
            $generasi_2 = 2;
            $generasi_3 = 3;
            $generasi_4 = false;
            $generasi_5 = false;
	    $generasi_6 = false;
	    $generasi_7 = false;
            $generasi_8 = false;
	    $generasi_9 = false;
	    $generasi_10 = false;
       }
       
        $data['page_title'] = 'Network Generation List';
        $this->load->view('member/network_generation',$data);
    }
    
}
?>