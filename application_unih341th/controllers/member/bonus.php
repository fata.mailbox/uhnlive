<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bonus extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MBonus','MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('member_id','Member ID','required');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('periode','','');
        
        if($this->form_validation->run()){
            //$data['results']=$this->MBonus->getBonus($this->input->post('member_id'),$this->input->post('periode'));
            $data['results']=$this->MBonus->getBonus_new($this->input->post('member_id'),$this->input->post('periode'));
            $data['total']=$this->MBonus->getTotalBonus($this->input->post('member_id'),$this->input->post('periode'));
            $data['totalaccumulation']=$this->MBonus->getAccumulationBonus($this->input->post('member_id'),$this->input->post('periode'));
            $data['totalbonusfromjoin']=$this->MBonus->getTotalBonusFromJoin($this->input->post('member_id'));
			$data['memid'] = $this->input->post('member_id');
			$data['periodecurr'] = $this->input->post('periode');
        }else{
            $data['results']=false;
            $data['total']=false;
            $data['totalaccumulation']=false;
            $data['totalbonusfromjoin']=false;
			$data['memid'] = '';
			$data['periodecurr'] = '';
        }
        
        $data['dropdown'] = $this->MBonus->getDropDrownPeriode();
        $data['page_title'] = 'Statement Bonus';
		if($this->input->post('submit')=="export")
        $this->load->view('member/bonus_statment_export',$data);
		else
        $this->load->view('member/bonus_statment',$data);
    }

    public function statement(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('member_id','Member ID','required');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('periode','','');
        
        if($this->form_validation->run()){
            $data['results']=$this->MBonus->getBonus($this->input->post('member_id'),$this->input->post('periode'));
            //$data['results']=$this->MBonus->getBonus_new($this->input->post('member_id'),$this->input->post('periode'));
            $data['total']=$this->MBonus->getTotalBonus($this->input->post('member_id'),$this->input->post('periode'));
            $data['totalaccumulation']=$this->MBonus->getAccumulationBonus($this->input->post('member_id'),$this->input->post('periode'));
            $data['totalbonusfromjoin']=$this->MBonus->getTotalBonusFromJoin($this->input->post('member_id'));
            $data['memid'] = $this->input->post('member_id');
            $data['periodecurr'] = $this->input->post('periode');
        }else{
            $data['results']=false;
            $data['total']=false;
            $data['totalaccumulation']=false;
            $data['totalbonusfromjoin']=false;
            $data['memid'] = '';
            $data['periodecurr'] = '';
        }
        
        $data['dropdown'] = $this->MBonus->getDropDrownPeriode();
        $data['page_title'] = 'Statement Bonus';
        if($this->input->post('submit')=="export")
        $this->load->view('member/bonus_statment_export_',$data);
        else
        $this->load->view('member/bonus_statment_',$data);
    }

    public function detail($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $header = $this->MBonus->getSubBonus($id);
        
        if(!count($header)){
            redirect('member/bonus','refresh');
        }
        $data['header'] = $header;
        
        $data['results']=$this->MBonus->getBonusDetail($id);
        
        $data['page_title'] = 'Statement Bonus Detail';
        $this->load->view('member/bonusdetail_statment',$data);
    }
    public function detailexport($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $header = $this->MBonus->getSubBonus($id);
        
        if(!count($header)){
            redirect('member/bonus','refresh');
        }
        $data['header'] = $header;
        
        $data['results']=$this->MBonus->getBonusDetail($id);
        
        $data['page_title'] = 'Statement Bonus Detail';
        $this->load->view('member/bonusdetail_statment_export',$data);
    }
}
?>