<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tgpv extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MBonus','MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('member_id','id mitra','required');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        $this->form_validation->set_rules('level','','');
        
        if($this->form_validation->run()){
            if($this->session->userdata('group_id') > 100)$memberid=$this->session->userdata('userid');
            else $memberid = $this->input->post('member_id');
            
            $page=$this->input->post('level');
            $data['page'] = $page;
            //echo $page;
            $data['downline'] = $this->MBonus->get_downline($page,$memberid,$this->input->post('fromdate'),$this->input->post('todate'));
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-win2k-cold-1.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'Total Account perLevel';
        $this->load->view('member/total_account_perlevel',$data);
    }    
}
?>