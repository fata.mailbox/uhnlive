<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Networks extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('welcome/index/','refresh');
        }
        $this->load->model('Nested_sets_model');
        $this->load->model('Networks_model','cats');
        $this->load->model('MMenu');
        
    }
    
    public function index(){
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('member_id','Member ID','required');
        $this->form_validation->set_rules('name','','');
        
        if($this->form_validation->run()){
            if($this->session->userdata('group_id')>100){
                if ($this->session->userdata('userid') != $this->input->post('member_id')){
                    $param = $this->cats->check_genealogy($this->input->post('member_id'),$this->session->userdata('userid'));
                    if($param == 'error')$data['treehtml']=false;
                    else $data['treehtml'] = $this->cats->getTreeAsHTML();
                }else{
                    $data['treehtml'] = $this->cats->getTreeAsHTML();
                }
                
            }else $data['treehtml'] = $this->cats->getTreeAsHTML();
        }else{
            if($this->session->userdata('group_id') > 100 && !$_POST)$data['treehtml'] = $this->cats->getTreeAsHTML();
            else $data['treehtml']=false;
        }
        
        $data['page_title'] = 'Network Genealogy';
        $this->load->view('member/network_genealogy',$data);
    }
      
}
?>