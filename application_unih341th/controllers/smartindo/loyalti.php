<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Loyalti extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101) {
			redirect('');
		}
		$this->load->model(array('MMenu','Demandmodel'));
	}

	public function index()
	{
		$usern = $this->session->userdata('userid');
		$data["result"] = $this->Demandmodel->loyalti_get($usern);
		$data['page_title'] = 'Loyalti Monitoring';
		$this->load->view('smartindo/loyalti_view', $data);
	}

	function view($no)
	{
		$usern = $this->session->userdata('userid');
		$data["result"] = $this->Demandmodel->loyalti_get($no,$usern);
		$qrt = $this->Demandmodel->history_demand($no,$usern);
		$k = [];
		for ($i = 0; $i < count($qrt); $i++) {
			$k[$i] =  "" . $qrt[$i]['noref'] . "" . ',';
		} 
		$b =  implode(',', $k);
		$c =  str_replace(',,', ',', $b);
		$data['no_ro'] =  rtrim($c, ',');
		$awal = $this->Demandmodel->history_row($no,$usern);
		$data["rows"] = $this->Demandmodel->detail_history($no,$usern);
		
	    if(!empty($awal)){
	         $hasil_awal = intval($awal['jml_awal']);
	    }else{
	        $hasil_awal = intval(count($data["rows"]));
	    }
	    
	    $jml = $this->Demandmodel->sum_demand($no,$usern);
	    if(!empty($jml) and !is_null($jml['pakai'])){
	    $data['jml_awal'] = $hasil_awal;
	    $data['jml_pakai'] = $jml['pakai'];
	    $data['jml_akhir'] = intval($hasil_awal) - intval($jml['pakai']);
	    }else if(is_null($jml['pakai'])) 
	    {
	        $data['jml_awal'] = $hasil_awal;
	 
	        $data['jml_akhir'] = intval($hasil_awal) - intval($jml['pakai']);
	         $data['jml_pakai'] = 0;
	    }
	    else{
	     $data['jml_awal'] = 0;
	     $data['jml_pakai'] = 0;
	     $data['jml_akhir'] = 0;
	    }
		$data['page_title'] = 'Loyalti Monitoring - Detail / Topup : ' . $no;
		$this->load->view('smartindo/loyalti_add', $data);
	}
}

/* End of file loyalti.php */
/* Location: ./application_unih341th/controllers/smartindo/loyalti.php */
