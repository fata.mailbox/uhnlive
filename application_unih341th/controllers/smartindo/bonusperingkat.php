<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bonusperingkat extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('BOnus_model','MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('member_id','Member ID','');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('periode','','');
        
        if($this->form_validation->run()){
            // $data['results']=$this->BOnus_model->getBonus($this->input->post('member_id'),$this->input->post('periode'));
            $data['results']=$this->BOnus_model->getBonusNew($this->input->post('member_id'),$this->input->post('periode'));
            $data['total']=$this->BOnus_model->getTotalBonus($this->input->post('member_id'),$this->input->post('periode'));
        }else{
            $data['results']=false;
            $data['total']=false;
        }
        
        $data['dropdown'] = $this->BOnus_model->getDropDrownPeriode();
        $data['page_title'] = 'Statement Bonus Peringkat';
        $this->load->view('smartindo/bonus_peringkat_statment',$data);
    }
    
    public function detail($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $header = $this->BOnus_model->getSubBonus($id);
        
        if(!count($header)){
            redirect('smartindo/bonusperingkat','refresh');
        }
        $data['header'] = $header;
        
        $data['results']=$this->BOnus_model->getBonusDetail($id);
        
        $data['page_title'] = 'Statement Bonus Peringkat Detail';
        $this->load->view('smartindo/bonus_peringkat_detail_statment',$data);
    }
    public function approved(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        //print_r($_POST);
        if(!$this->MMenu->blocked()){
            $this->BOnus_model->approval_bonus_peringkat();
        }
        redirect('smartindo/bonusperingkat/','refresh');
    }
    
      
}
?>