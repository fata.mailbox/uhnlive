<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Topupvalue extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
		$this->load->model(array('MMenu','ValueTopup_model'));
	}

	public function index()
	{
	    
		$data["result"] = $this->ValueTopup_model->GetTopup_Value();
		$data['page_title'] = 'Top Up By Value';
		$this->load->view('smartindo/topupvalue_view', $data);
	}

	function add()
	{
		$data['page_title'] = 'Add Top Up By Value';
		$this->load->view('smartindo/topupvalue_add', $data);
	}

	function save()
	{
	    
	
      $this->ValueTopup_model->SaveValue();

		echo '<script language="javascript">alert("Sukses Simpan Data")</script>';
		redirect('smartindo/topupvalue', 'refresh');
	}

	function edit($id)
	{
		$data["result"] = $this->ValueTopup_model->result_edit($id);
		$data["detail"] = $this->ValueTopup_model->detail_edit($id);
		$data['page_title'] = 'Edit Top Up By Value';
		$this->load->view('smartindo/topupvalue_edit', $data);
	}

	function update()
	{
		$this->ValueTopup_model->update_detail();
		echo '<script language="javascript">alert("Sukses Update Data")</script>';
		redirect('smartindo/topupvalue', 'refresh');
	}

	function delete($id)
	{
		$this->ValueTopup_model->delete_topup_value($id);
	}

	function viewresult($value, $codeuser, $topupno, $price, $discount)
	{
		$newcode = [];
		$endpromo = [];
		$codevalue = [];
		if ($value == 2) {
			if (count($this->session->userdata('codetpvalue_' . $codeuser)) > 0) {
				foreach ($this->session->userdata('codetpvalue_' . $codeuser) as $value) {
					array_push($codevalue, "'" . $value["topupno"] . "'");
				}
				$data["multiple"] = $this->ValueTopup_model->viewresult_datavalue($codevalue);
				$data["result"] = $this->ValueTopup_model->viewresult_parent($codevalue);
			}
		} else {
			if (count($this->session->userdata('codetpvalue_' . $codeuser)) > 0) {
				foreach ($this->session->userdata('codetpvalue_' . $codeuser) as $values) {
					array_push($codevalue, "'" . $values["topupno"] . "'");
				}
				$data["multiple"] = $this->ValueTopup_model->viewresult_datavalue($codevalue);
				$data["result"] = $this->ValueTopup_model->viewresult_parent($codevalue);
			}
		}
		$data['codex'] = $codeuser;
		$data['persen'] = intval($discount);
		$data['price'] = intval($price);
		$this->load->view('smartindo/topupvalue_result', $data);
	}


function resultview($value, $codeuser, $topupno, $price)
	{
		$newcode = [];
		$endpromo = [];
		$codevalue = [];
		if ($value == 2) {
			if (count($this->session->userdata('codetpvalue_' . $codeuser)) > 0) {
				foreach ($this->session->userdata('codetpvalue_' . $codeuser) as $value) {
					array_push($codevalue, "'" . $value["topupno"] . "'");
				}
				$data["multiple"] =  $this->ValueTopup_model->viewresult_datavalue($codevalue);
				$data["result"] = $this->ValueTopup_model->viewresult_parent($codevalue);
			}
		} else {
			if (count($this->session->userdata('codetpvalue_' . $codeuser)) > 0) {
				foreach ($this->session->userdata('codetpvalue_' . $codeuser) as $values) {
					array_push($codevalue, "'" . $values["topupno"] . "'");
				}
				$data["multiple"] =  $this->ValueTopup_model->viewresult_datavalue($codevalue);
				$data["result"] = $this->ValueTopup_model->viewresult_parent($codevalue);
			}
		}

		$data['codex'] = $codeuser;
		$data['price'] = intval($price);
		$this->load->view('smartindo/topupvalue_view_result', $data);
	}
}

/* End of file topupvalue.php */
/* Location: ./application/controllers/topupvalue.php */
