<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Point_ord extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MPinjamanadmin','MPinjaman', 'MPointemp'));
		$this->load->model(array('MMenu','MRo'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'smartindo/point_ord/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MPointemp->countRedeem($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MPointemp->searchRedeem($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Staff Point Order';
        $this->load->view('smartindo/stf_point',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('member_id','Admin ID','required');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('point','','callback__check_saldo');
        $this->form_validation->set_rules('remark','','');
        $this->form_validation->set_rules('total','Total pinjaman','required|callback__check_min');
        $this->form_validation->set_rules('whsid','','');
        
		for($i=0;$i<10;$i++){
			$this->form_validation->set_rules('itemcode'.$i,'','');
			$this->form_validation->set_rules('itemname'.$i,'','');
			$this->form_validation->set_rules('qty'.$i,'','');
			$this->form_validation->set_rules('price'.$i,'','');
			$this->form_validation->set_rules('subtotal'.$i,'','');
		}
        
        if($this->form_validation->run()){
			if(!$this->MMenu->blocked()){
                $this->MPointemp->addRedeem();
                $this->session->set_flashdata('message','Point Order Successfully');
            }
            redirect('smartindo/point_ord','refresh');
        }
        $data['warehouse']=$this->MPinjaman->getDropDownWhs('');
        $data['page_title'] = 'Create Point Order';
        $this->load->view('smartindo/stf_point_create',$data);
    }
    
	public function _check_saldo(){
		$this->form_validation->set_message('_check_saldo','Cek');
		$amount = str_replace(",","",$this->input->post('point'));
		$blj = str_replace(".","",$this->input->post('total'));
		$member_id = $this->input->post('member_id');
		$avail= $this->MPointemp->cekSaldo($member_id, $blj);
		if($avail>0){
			return true;
		}else{
			$this->form_validation->set_message('_check_saldo','Point is not enough..');
            return false;
        }
		return true;
	}
	
    public function _check_min(){
		$amount = str_replace(".","",$this->input->post('qty0'));
		if($amount <= 0){
            $this->form_validation->set_message('_check_min','browse product...');
            return false;
        }
	  	//Updated by Boby (2012-01-13)
		else{
			$flag = 0;
			if($this->input->post('itemcode0')){$item[$flag]=$this->input->post('itemcode0'); $qty[$flag]=str_replace(".","",$this->input->post('qty0')); $itemname[$flag]=$this->input->post('itemname0');	$prc[$flag]=str_replace(".","",$this->input->post('price0'));	$flag+=1;}
			if($this->input->post('itemcode1')){$item[$flag]=$this->input->post('itemcode1'); $qty[$flag]=str_replace(".","",$this->input->post('qty1')); $itemname[$flag]=$this->input->post('itemname1');	$prc[$flag]=str_replace(".","",$this->input->post('price1'));	$flag+=1;}
			if($this->input->post('itemcode2')){$item[$flag]=$this->input->post('itemcode2'); $qty[$flag]=str_replace(".","",$this->input->post('qty2')); $itemname[$flag]=$this->input->post('itemname2');	$prc[$flag]=str_replace(".","",$this->input->post('price2'));	$flag+=1;}
			if($this->input->post('itemcode3')){$item[$flag]=$this->input->post('itemcode3'); $qty[$flag]=str_replace(".","",$this->input->post('qty3')); $itemname[$flag]=$this->input->post('itemname3');	$prc[$flag]=str_replace(".","",$this->input->post('price3'));	$flag+=1;}
			if($this->input->post('itemcode4')){$item[$flag]=$this->input->post('itemcode4'); $qty[$flag]=str_replace(".","",$this->input->post('qty4')); $itemname[$flag]=$this->input->post('itemname4');	$prc[$flag]=str_replace(".","",$this->input->post('price4'));	$flag+=1;}

			if($this->input->post('itemcode5')){$item[$flag]=$this->input->post('itemcode5'); $qty[$flag]=str_replace(".","",$this->input->post('qty5')); $itemname[$flag]=$this->input->post('itemname5');	$prc[$flag]=str_replace(".","",$this->input->post('price5'));	$flag+=1;}
			if($this->input->post('itemcode6')){$item[$flag]=$this->input->post('itemcode6'); $qty[$flag]=str_replace(".","",$this->input->post('qty6')); $itemname[$flag]=$this->input->post('itemname6');	$prc[$flag]=str_replace(".","",$this->input->post('price6'));	$flag+=1;}
			if($this->input->post('itemcode7')){$item[$flag]=$this->input->post('itemcode7'); $qty[$flag]=str_replace(".","",$this->input->post('qty7')); $itemname[$flag]=$this->input->post('itemname7');	$prc[$flag]=str_replace(".","",$this->input->post('price7'));	$flag+=1;}
			if($this->input->post('itemcode8')){$item[$flag]=$this->input->post('itemcode8'); $qty[$flag]=str_replace(".","",$this->input->post('qty8')); $itemname[$flag]=$this->input->post('itemname8');	$prc[$flag]=str_replace(".","",$this->input->post('price8'));	$flag+=1;}
			if($this->input->post('itemcode9')){$item[$flag]=$this->input->post('itemcode9'); $qty[$flag]=str_replace(".","",$this->input->post('qty9')); $itemname[$flag]=$this->input->post('itemname9');	$prc[$flag]=str_replace(".","",$this->input->post('price9'));	$flag+=1;}

			$temp = 0;$twin=0;
			$whsid = $this->session->userdata('whsid');
			
			for($i=0;$i<=$flag;$i++){
				if($temp>0){
					for($j=0;$j<$temp;$j++){
						if($brg[$j]==$item[$i]){//jika sama
							$twin = 1;
							$jml[$j]+=$qty[$i];
							//echo $qty[$i]."<br>";
						}
					}
				}			
				if($twin==0){//jika tidak ada yang sama
					$jml[$temp]=$qty[$i];
					$brg[$temp]=$item[$i];
					$price[$temp]=$prc[$i];
					$nama[$temp]=$itemname[$i];
					$temp+=1;
				}else{$twin=0;}
			}
			for($i=0;$i<count($jml)-1;$i++){
				//echo "<br><br>".$brg[$i]." = ".$jml[$i]." ".$nama[$i]." ";
				$readystok=$this->MRo->cekStok($whsid, $brg[$i], $price[$i], $jml[$i], 0);
				if($readystok!='Ready'){$twin=1;$stok="We only have ".$readystok." ".$nama[$i];}
				//echo "Stok = ".$readystok."<br>";
			}
			if($twin > 0){
				$this->form_validation->set_message('_check_min','Out of order ( '.$stok.' )');
				return false;
			}
		}
		//end Updated by Boby (2012-01-13)
		
        return true;
    }
    
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MPointemp->getRedeem($id);
        
        if(!count($row)){
            redirect('order/pjm_adm','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->MPointemp->getRedeemD($id);
        $data['page_title'] = 'View Admin Consignment';
        $this->load->view('smartindo/point_redeem_view',$data);
    }
    
}
?>