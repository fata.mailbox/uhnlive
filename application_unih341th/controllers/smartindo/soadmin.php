<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Soadmin extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
            redirect('');
        }
        
        $this->load->model(array('MMenu','SO_model','GLobal_model'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'smartindo/soadmin/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->SO_model->countSO($keywords);
        //echo $config['total_rows'];
        $this->pagination->initialize($config);
        $data['results'] = $this->SO_model->searchSO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Sales Order admin';
        $this->load->view('smartindo/so_admin_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        	
        //$this->form_validation->set_rules('date','Tanggal SO','required|callback__check_tglso');
        $this->form_validation->set_rules('member_id','Member','required');
        $this->form_validation->set_rules('name','','');
	$this->form_validation->set_rules('ewallet','','');
	
	$this->form_validation->set_rules('pu','','');
	if($this->input->post('pu') == '1'){
	    $this->form_validation->set_rules('city','','callback__check_delivery');
	    $this->form_validation->set_rules('kota_id','','');
	    $this->form_validation->set_rules('kota_id1','','');
	    
	    $this->form_validation->set_rules('propinsi','','');
	    $this->form_validation->set_rules('addr','','');
	    $this->form_validation->set_rules('addr1','','');
	    $this->form_validation->set_rules('timur','','');
	}
	if($this->form_validation->run() == true){
	    $this->del_session();
            if(!$this->MMenu->blocked()){
		if($this->input->post('pu') == '1'){
		    $this->session->set_userdata(
		    array(
			's_member_id'=>$this->input->post('member_id'),
			's_pu'=>$this->input->post('pu'),
			's_deli_ad'=>$this->input->post('deli_ad'),
			's_timur'=>$this->input->post('timur'),
			's_kota_id'=>$this->input->post('kota_id'),
			's_kota_id1'=>$this->input->post('kota_id1'),
			's_propinsi'=>$this->input->post('propinsi'),
			's_city'=>$this->input->post('city'),
			's_addr'=>$this->input->post('addr'),
			's_addr1'=>$this->input->post('addr1')
			)
		    );
		}else{
		    $this->session->set_userdata(
		    array(
			's_member_id'=>$this->input->post('member_id'),
			's_pu'=>$this->input->post('pu')
			)
		    );
		}
	    }
	    redirect('smartindo/soadmin/add','refresh');
	}
	
	$data['page_title'] = 'Create Sales Order Admin';
        $this->load->view('smartindo/so_admin_form',$data);
    }
    public function add(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        	
        $this->form_validation->set_rules('member_id','Member','required');
        $this->form_validation->set_rules('remark','','');
	
	$this->form_validation->set_rules('pu','','');
	if($this->input->post('pu') == '1'){
	    $this->form_validation->set_rules('city','','callback__check_delivery');
	    $this->form_validation->set_rules('kota_id','','');
	    $this->form_validation->set_rules('propinsi','','');
	    $this->form_validation->set_rules('addr','','');
	    $this->form_validation->set_rules('addr1','','');
	    $this->form_validation->set_rules('timur','','');
	}
	$this->form_validation->set_rules('tunai','','');
	$this->form_validation->set_rules('debit','','');
	$this->form_validation->set_rules('credit','','');
	$this->form_validation->set_rules('totalbayar','Total Bayar','callback__check_bayar');
	    
	$groupid = $this->session->userdata('group_id');
	
	if($_POST['action'] == 'Go'){
	    $rowx = $this->input->post('rowx');
	    $counti = count($_POST['counter'])+$rowx;
	}elseif($_POST['action'] == 'Submit')$counti = count($_POST['counter']);
	else $counti=5;
	$this->session->set_userdata(array('counti'=>$counti));
	  
	$i=0;
	while($counti > $i){
	    $this->form_validation->set_rules('itemcode'.$i,'','');
	    $this->form_validation->set_rules('itemname'.$i,'','');
	    $this->form_validation->set_rules('qty'.$i,'','');
	    $this->form_validation->set_rules('price'.$i,'','');
	    $this->form_validation->set_rules('subtotal'.$i,'','');
	    $this->form_validation->set_rules('pv'.$i,'','');
	    $this->form_validation->set_rules('subtotalpv'.$i,'','');
	    $this->form_validation->set_rules('bv'.$i,'','');
	    $this->form_validation->set_rules('subtotalbv'.$i,'','');
	    
	    $i++;
	}
	
	$data['counti']=$counti;
		
	$this->form_validation->set_rules('total','Total repeat order','required|callback__check_stock');
	$this->form_validation->set_rules('totalpv','','');
	$this->form_validation->set_rules('totalbv','','');
	if($this->form_validation->run() && $_POST['action'] != 'Go'){
	    if(!$this->MMenu->blocked()){
		$this->SO_model->so_add_admin();
		$this->session->set_flashdata('message','Create sales order admin successfully');
	    }
	    redirect('smartindo/soadmin','refresh');
	}
	$data['row'] = $this->GLobal_model->get_ewallet($this->session->userdata('s_member_id'));
        $data['page_title'] = 'Create Sales Order Admin';
        $this->load->view('smartindo/so_admin_form2',$data);
    }
    
    public function _check_stock(){
	$whsid = 1;
	$memberid = $this->input->post('member_id');
	
	$amount = str_replace(".","",$this->input->post('total'));
	if($amount <= 0){
	    $this->form_validation->set_message('_check_stock','browse product...');
	    return false;
	}else{
	    if(!validation_errors()){
		
		$data = array(
		    'stockiest_id'=> $whsid,
		    'member_id'=> $memberid
		);
		$this->db->insert('so_check',$data);
	    
		$id = $this->db->insert_id();
	    
		$key=0;
		$whsid = 1;
		while($key < count($_POST['counter'])){
		    $qty = str_replace(".","",$this->input->post('qty'.$key));
		    $price = str_replace(".","",$this->input->post('price'.$key));
		    $itemid = $this->input->post('itemcode'.$key);
		    if($itemid and $qty > 0){
			$data=array(
			    'so_id' => $id,
			    'item_id' => $itemid,
			    'qty' => $qty,
			    'price' => $price
			);
			$this->db->insert('so_check_d',$data);
			
			//echo $id.' * '.$itemid.' * '.$price.' * '.$titipan_id.' * '.$stcid;
			
			$rs=$this->GLobal_model->check_stock_whs($id,$itemid,$price,$whsid);
			if(!$rs->stock){
			    $this->form_validation->set_message('_check_stock',"You don't have ".$itemid);
				
			    $this->db->delete('so_check',array('id'=>$id));
			    $this->db->delete('so_check_d',array('so_id'=>$id));
			    return false;
			}elseif($rs->stock < $rs->qty){
			    $this->form_validation->set_message('_check_stock',"We only have ".$rs->stock." ".$rs->name);
			    
			    $this->db->delete('so_check',array('id'=>$id));
			    $this->db->delete('so_check_d',array('so_id'=>$id));
			    return false;
			}
		    }
		    $key++;
		}
		
		$this->db->delete('so_check',array('id'=>$id));
		$this->db->delete('so_check_d',array('so_id'=>$id));
	    }
        }
        return true;
    }
    
    public function _check_bayar(){
        $amount = str_replace(".","",$this->input->post('total'));
        $bayar = str_replace(".","",$this->input->post('totalbayar'));
        
        $row = $this->GLobal_model->get_ewallet($this->input->post('member_id'));
        if($row){
            $saldo = $row->ewallet + $bayar;
            if($amount > $saldo){
                $this->form_validation->set_message('_check_bayar','Pembayaran tidak mencukupi, ewallet anda kurang Rp. '.number_format($amount-$saldo,0,'','.'));
                return false;
            }
        }else{
	    $this->form_validation->set_message('_check_bayar','Invalid member, silahkan hubungi IT UHN');
            return false;
	}
        return true;
    }
    
    public function _check_tglso(){
        if(date('Y-m-d',now()) > $this->input->post('date')){
	    $allow = 0;
		
	    if($this->session->userdata('group_id')>100){
	    $this->form_validation->set_message('_check_tglso','Tanggal sales order tidak boleh mundur');
		return false;
	    }else{
		if($this->session->userdata('group_id')<100){
		    $row=$this->SO_model->get_blocked();
		    if($this->input->post('date') <= $row->tglrunning){
			$this->form_validation->set_message('_check_tglso','Tanggal '.$row->tglrunning.' bonus sudah dijalankan');
			return false;
		    }
		}
	    }
        }
        return true;
    }
    
    public function _check_pin(){
        if(!$this->MAuth->check_pin($this->session->userdata('username'),$this->input->post('pin'))){
            $this->form_validation->set_message('_check_pin','Sorry, your pin invalid');
            return false;
        }
        return true;
    }
	
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->SO_model->getSalesOrder($id);
        
        if(!count($row)){
            redirect('smartindo/so','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->SO_model->getSalesOrderDetail($id);
	
	$data['page_title'] = 'View Sales Order';
        $this->load->view('smartindo/salesorder_view',$data);
    }
    public function del_session(){
	$this->session->unset_userdata('s_member_id');
	$this->session->unset_userdata('s_pu');
	$this->session->unset_userdata('s_timur');
	$this->session->unset_userdata('s_kota_id');
	$this->session->unset_userdata('s_kota_id1');
	$this->session->unset_userdata('s_addr');
	$this->session->unset_userdata('s_addr1');
	$this->session->unset_userdata('s_propinsi');
	$this->session->unset_userdata('s_city');
	$this->session->unset_userdata('s_deli_ad');
    }
    public function _check_delivery(){
        if($this->input->post('pu')==1){
	    if(strlen($this->input->post('city'))<1 OR strlen($this->input->post('addr'))<1){
		$this->form_validation->set_message('_check_delivery','Please fill the address.');
		return false;
	    }
        }
        return true;
    }
}
?>
