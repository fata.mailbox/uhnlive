<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Topupdemand extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
		    redirect('');
		}

		$this->load->model(array('MMenu', 'Demandmodel','ValueTopup_model'));
	}

	public function index()
	{
		$data["result"] = $this->Demandmodel->demand_get();
		$data['page_title'] = 'Top Up On Demand';

		$this->load->view('smartindo/topupdemand_view', $data);
	}

	function add()
	{
		$data['page_title'] = 'Add Top Up On Demand';
		$this->load->view('smartindo/topupdemand_add', $data);
	}

	function edit($id)
	{
		$data["result"] = $this->Demandmodel->result_demand($id);
		$data["detail"] = $this->Demandmodel->edit_demand($id);
		$data["stockiest"] =  $this->Demandmodel->demand_stockiest();					
		$data['page_title'] = 'Edit Top Up On Demand / Topup : ' . $id . ' '.$data['result']->filename;
		$this->load->view('smartindo/topupdemand_edit', $data);
	}

	function delete($id)
	{
		$this->Demandmodel->demand_delete($id);
	}

	function import()
	{
		$this->Demandmodel->import_demand();
	}

	function update()
	{
		$this->Demandmodel->update_demand();
	}

	function viewresult($member,$persen)
	{
		$getno = $this->Demandmodel->getno_demand($member);
		if (!empty($getno)) {
		
			$data['result'] =  $this->Demandmodel->source($member,$getno);
			$data['multiple'] = '1';
			$data['stc_id'] = $member;
		} else {
			$data['result'] = '';
			$data['multiple'] = '';
			$data['stc_id'] = '';
		}
		$data['persen'] = $persen;
		$this->load->view('smartindo/topupbydemand_result', $data);
	}
	function resultview($member,$persen)
	{
		$getno = $this->Demandmodel->getno_demand($member);
		if (!empty($getno)) {
		
			$data['result'] =  $this->Demandmodel->source($member,$getno);
			$data['multiple'] = '1';
			$data['stc_id'] = $member;
		} else {
			$data['result'] = '';
			$data['multiple'] = '';
			$data['stc_id'] = '';
		}
		$data['persen'] = $persen;
		$this->load->view('smartindo/resultview', $data);
	}
	function sodemand($member, $stc_id)
	{
		$getno = $this->Demandmodel->stcid_demand($member,$stc_id);
		if (!empty($getno)) {
		
			$data['result'] =  $this->Demandmodel->source_so($member,$getno,$stc_id);
			$data['multiple'] = '1';
			$data['stc_id'] = $stc_id;
		} else {
			$data['result'] = '';
			$data['multiple'] = '';
			$data['stc_id'] = '';
		}
	$this->load->view('smartindo/sodemand', $data);
	}
}

/* End of file topupdemand.php */
/* Location: ./application/controllers/topupdemand.php */
