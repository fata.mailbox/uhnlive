<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Roadmin extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','RO_model','GLobal_model'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'smartindo/roadmin/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']);
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->RO_model->countRO($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->RO_model->searchRO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->RO_model->countRO($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->RO_model->searchRO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Request Order Approval';
        $this->load->view('smartindo/ro_admin_index',$data);
    }
    
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->RO_model->getRequestOrder($id);
        
        if(!count($row)){
            redirect('smartindo/roadmin','refresh');
        }
		$bns = $this->RO_model->getFree($id);
		$data['bns'] = $bns;
		$data['row'] = $row;
        $data['items'] = $this->RO_model->getRequestOrderDetail($id);
        $data['page_title'] = 'View Request Order Approval';
        $this->load->view('smartindo/ro_approved_view',$data);
    }
    
    public function approved(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        if(!$this->MMenu->blocked()){
            $this->RO_model->requestOrderApproved();
        }
        redirect('smartindo/roadmin/','refresh');
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        	
        //$this->form_validation->set_rules('date','Tanggal SO','required|callback__check_tglso');
        $this->form_validation->set_rules('member_id','Stockiest','required');
        $this->form_validation->set_rules('name','','');
	$this->form_validation->set_rules('no_stc','','');
	$this->form_validation->set_rules('persen','','');
	
	$this->form_validation->set_rules('pu','','');
	if($this->input->post('pu') == '1'){
	    $this->form_validation->set_rules('city','','callback__check_delivery');
	    $this->form_validation->set_rules('kota_id','','');
	    $this->form_validation->set_rules('kota_id1','','');
	    
	    $this->form_validation->set_rules('propinsi','','');
	    $this->form_validation->set_rules('addr','','');
	    $this->form_validation->set_rules('addr1','','');
	    $this->form_validation->set_rules('timur','','');
	}
	if($this->form_validation->run() == true){
	    $this->del_session();
            if(!$this->MMenu->blocked()){
		if($this->input->post('pu') == '1'){
		    $this->session->set_userdata(
		    array(
			'r_member_id'=>$this->input->post('member_id'),
			'r_persen'=>$this->input->post('persen'),
			'r_pu'=>$this->input->post('pu'),
			'r_deli_ad'=>$this->input->post('deli_ad'),
			'r_timur'=>$this->input->post('timur'),
			'r_kota_id'=>$this->input->post('kota_id'),
			'r_kota_id1'=>$this->input->post('kota_id1'),
			'r_propinsi'=>$this->input->post('propinsi'),
			'r_city'=>$this->input->post('city'),
			'r_addr'=>$this->input->post('addr'),
			'r_addr1'=>$this->input->post('addr1')
			)
		    );
		}else{
		    $this->session->set_userdata(
		    array(
			'r_member_id'=>$this->input->post('member_id'),
			'r_persen'=>$this->input->post('persen'),
			'r_pu'=>$this->input->post('pu')
			)
		    );
		}
	    }
	    redirect('smartindo/roadmin/add','refresh');
	}
	
	$data['page_title'] = 'Create Request Order Admin';
        $this->load->view('smartindo/ro_admin_form',$data);
    }
    
    public function add(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
	$this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        	
        $this->form_validation->set_rules('member_id','Stockist','required');
        $this->form_validation->set_rules('remark','','');
	
	$this->form_validation->set_rules('pu','','');
	if($this->input->post('pu') == '1'){
	    $this->form_validation->set_rules('city','','callback__check_delivery');
	    $this->form_validation->set_rules('kota_id','','');
	    $this->form_validation->set_rules('propinsi','','');
	    $this->form_validation->set_rules('addr','','');
	    $this->form_validation->set_rules('addr1','','');
	    $this->form_validation->set_rules('timur','','');
	}
	
        $this->form_validation->set_rules('total','Total repeat order','required|callback__check_min');
        $this->form_validation->set_rules('totalpv','','');
	$this->form_validation->set_rules('totalbv','','');
	
        $this->form_validation->set_rules('persen','','');
	$this->form_validation->set_rules('totalrpdiskon','','');
        $this->form_validation->set_rules('totalbayar','Total Bayar','callback__check_bayar');
	    
	$groupid = $this->session->userdata('group_id');
	
	if($_POST['action'] == 'Go'){
	    $rowx = $this->input->post('rowx');
	    $counti = count($_POST['counter'])+$rowx;
	}elseif($_POST['action'] == 'Submit')$counti = count($_POST['counter']);
	else $counti=5;
	$this->session->set_userdata(array('counti'=>$counti));
	  
	$i=0;
	while($counti > $i){
	    $this->form_validation->set_rules('itemcode'.$i,'','');
	    $this->form_validation->set_rules('itemname'.$i,'','');
	    $this->form_validation->set_rules('qty'.$i,'','');
	    $this->form_validation->set_rules('price'.$i,'','');
	    $this->form_validation->set_rules('subtotal'.$i,'','');
	    $this->form_validation->set_rules('pv'.$i,'','');
	    $this->form_validation->set_rules('subtotalpv'.$i,'','');
	    $this->form_validation->set_rules('bv'.$i,'','');
	    $this->form_validation->set_rules('subtotalbv'.$i,'','');
	    
	    $this->form_validation->set_rules('rpdiskon'.$i,'','');
	    $this->form_validation->set_rules('subrpdiskon'.$i,'','');
	    $i++;
	}
	
	$data['counti']=$counti;
		
	$this->form_validation->set_rules('total','Total repeat order','required|callback__check_stock');
	$this->form_validation->set_rules('totalpv','','');
	$this->form_validation->set_rules('totalbv','','');
	if($this->form_validation->run() && $_POST['action'] != 'Go'){
	    if(!$this->MMenu->blocked()){
		$this->RO_model->ro_add_admin();
		$this->session->set_flashdata('message','Create sales order admin successfully');
	    }
	    redirect('smartindo/roadmin','refresh');
	}
	$data['row'] = $this->GLobal_model->get_ewallet_stc($this->session->userdata('r_member_id'));
        $data['page_title'] = 'Create Request Order Admin';
        $this->load->view('smartindo/ro_admin_form2',$data);
    }
    
    public function _check_stock(){
	$whsid = 1;
	$memberid = $this->input->post('member_id');
	
	$amount = str_replace(".","",$this->input->post('total'));
	if($amount <= 0){
	    $this->form_validation->set_message('_check_stock','browse product...');
	    return false;
	}else{
	    if(!validation_errors()){
		
		$data = array(
		    'stockiest_id'=> $whsid,
		    'member_id'=> $memberid
		);
		$this->db->insert('so_check',$data);
	    
		$id = $this->db->insert_id();
	    
		$key=0;
		$whsid = 1;
		while($key < count($_POST['counter'])){
		    $qty = str_replace(".","",$this->input->post('qty'.$key));
		    $price = str_replace(".","",$this->input->post('price'.$key));
		    $itemid = $this->input->post('itemcode'.$key);
		    if($itemid and $qty > 0){
			$data=array(
			    'so_id' => $id,
			    'item_id' => $itemid,
			    'qty' => $qty,
			    'price' => $price
			);
			$this->db->insert('so_check_d',$data);
			
			//echo $id.' * '.$itemid.' * '.$price.' * '.$titipan_id.' * '.$stcid;
			
			$rs=$this->GLobal_model->check_stock_whs($id,$itemid,$price,$whsid);
			if(!$rs->stock){
			    $this->form_validation->set_message('_check_stock',"You don't have ".$itemid);
				
			    $this->db->delete('so_check',array('id'=>$id));
			    $this->db->delete('so_check_d',array('so_id'=>$id));
			    return false;
			}elseif($rs->stock < $rs->qty){
			    $this->form_validation->set_message('_check_stock',"We only have ".$rs->stock." ".$rs->name);
			    
			    $this->db->delete('so_check',array('id'=>$id));
			    $this->db->delete('so_check_d',array('so_id'=>$id));
			    return false;
			}
		    }
		    $key++;
		}
		
		$this->db->delete('so_check',array('id'=>$id));
		$this->db->delete('so_check_d',array('so_id'=>$id));
	    }
        }
        return true;
    }
    
    public function _check_bayar(){
        $amount = str_replace(".","",$this->input->post('total'));
	$diskon = str_replace(".","",$this->input->post('totalrpdiskon'));
        $totalbayar = $amount-$diskon;
        
        $row = $this->GLobal_model->get_ewallet_stc($this->input->post('member_id'));
        if($row){
            if($totalbayar > $row->ewallet){
                $this->form_validation->set_message('_check_bayar','Pembayaran tidak mencukupi, ewallet anda kurang Rp. '.number_format($totalbayar-$row->ewallet,0,'','.'));
                return false;
            }
        }else{
	    $this->form_validation->set_message('_check_bayar','Invalid member, silahkan hubungi IT UHN');
            return false;
	}
	
        return true;
    }
    
    
    public function del_session(){
	$this->session->unset_userdata('r_member_id');
	$this->session->unset_userdata('r_persen');
	$this->session->unset_userdata('r_pu');
	$this->session->unset_userdata('r_timur');
	$this->session->unset_userdata('r_kota_id');
	$this->session->unset_userdata('r_kota_id1');
	$this->session->unset_userdata('r_addr');
	$this->session->unset_userdata('r_addr1');
	$this->session->unset_userdata('r_propinsi');
	$this->session->unset_userdata('r_city');
	$this->session->unset_userdata('r_deli_ad');
    }
    public function _check_delivery(){
        if($this->input->post('pu')==1){
	    if(strlen($this->input->post('city'))<1 OR strlen($this->input->post('addr'))<1){
		$this->form_validation->set_message('_check_delivery','Please fill the address.');
		return false;
	    }
        }
        return true;
    }
    
}
?>