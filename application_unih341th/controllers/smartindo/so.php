<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class So extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
            redirect('');
        }
        
        $this->load->model(array('MMenu','SO_model','GLobal_model'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'smartindo/so/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->SO_model->countSO($keywords);
        //echo $config['total_rows'];
        $this->pagination->initialize($config);
        $data['results'] = $this->SO_model->searchSO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Sales Order';
        $this->load->view('smartindo/salesorder_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        	
        $this->form_validation->set_rules('date','Tanggal SO','required|callback__check_tglso');
        $this->form_validation->set_rules('member_id','Member','required');
        $this->form_validation->set_rules('remark','','');
	$this->form_validation->set_rules('name','','');
	    
        $groupid = $this->session->userdata('group_id');
        //print_r($_POST);
	if($_POST['action'] == 'Go'){
	    $rowx = $this->input->post('rowx');
	    $counti = count($_POST['counter'])+$rowx;
	}elseif($_POST['action'] == 'Submit')$counti = count($_POST['counter']);
	else $counti=5;
	$this->session->set_userdata(array('counti'=>$counti));
	
	$i=0;
	while($counti > $i){
	    $this->form_validation->set_rules('itemcode'.$i,'','');
	    $this->form_validation->set_rules('itemname'.$i,'','');
	    $this->form_validation->set_rules('qty'.$i,'','');
	    $this->form_validation->set_rules('price'.$i,'','');
	    $this->form_validation->set_rules('subtotal'.$i,'','');
	    $this->form_validation->set_rules('pv'.$i,'','');
	    $this->form_validation->set_rules('subtotalpv'.$i,'','');
	    $this->form_validation->set_rules('bv'.$i,'','');
	    $this->form_validation->set_rules('subtotalbv'.$i,'','');
	    $this->form_validation->set_rules('titipan_id'.$i,'','');
	    
	    $i++;
	}
	
	$data['counti']=$counti;
		
	$this->form_validation->set_rules('pin','PIN','required|callback__check_pin');
        $this->form_validation->set_rules('total','Total repeat order','required|callback__check_stock');
        $this->form_validation->set_rules('totalpv','','');
        $this->form_validation->set_rules('totalbv','','');
	if($this->form_validation->run() && $_POST['action'] != 'Go'){
            if(!$this->MMenu->blocked()){
		$this->SO_model->so_add_stc();
		$this->session->set_flashdata('message','Create sales order successfully');
	    }
            redirect('smartindo/so','refresh');
        }
	
	$data['groupid'] = $groupid;
        $data['page_title'] = 'Create Sales Order';
        $this->load->view('smartindo/salesorder_stc_form',$data);
    }
    
    public function _check_stock(){
	$stcid = $this->session->userdata('userid');
	$memberid = $this->input->post('member_id');
		
	$amount = str_replace(".","",$this->input->post('total'));
	$row = $this->SO_model->check_so_invalid($stcid);
	if($amount <= 0){
	    $this->form_validation->set_message('_check_stock','browse product...');
	    return false;
	}elseif($row->qty > 0){
	    $this->form_validation->set_message('_check_stock','silahkan hubungi UHN');
	    return false;
	}else{
	    if(!validation_errors()){
		
		$data = array(
		    'stockiest_id'=> $stcid,
		    'member_id'=> $memberid
		);
		$this->db->insert('so_check',$data);
	    
		$id = $this->db->insert_id();
	    
		$key=0;
		$whsid = 1;
		while($key < count($_POST['counter'])){
		    $qty = str_replace(".","",$this->input->post('qty'.$key));
		    $titipan_id = str_replace(".","",$this->input->post('titipan_id'.$key));
		    $price = str_replace(".","",$this->input->post('price'.$key));
		    $itemid = $this->input->post('itemcode'.$key);
		    if($itemid and $qty > 0){
			if($titipan_id <= 0){
			    $this->form_validation->set_message('_check_stock',"You don't have ".$itemid);
				    
			    $this->db->delete('so_check',array('id'=>$id));
			    $this->db->delete('so_check_d',array('so_id'=>$id));
			    return false;
			}else{
			    $data=array(
				'so_id' => $id,
				'item_id' => $itemid,
				'qty' => $qty,
				'price' => $price,
				'titipan_id' => $titipan_id
			    );
			    $this->db->insert('so_check_d',$data);
			    
			    //echo $id.' * '.$itemid.' * '.$price.' * '.$titipan_id.' * '.$stcid;
			    
			    $rs=$this->GLobal_model->check_stock_stc($id,$itemid,$price,$titipan_id,$stcid);
			    if(!$rs->stock){
				$this->form_validation->set_message('_check_stock',"You don't have ".$itemid);
				    
				$this->db->delete('so_check',array('id'=>$id));
				$this->db->delete('so_check_d',array('so_id'=>$id));
				return false;
			    }elseif($rs->stock < $rs->qty){
				$this->form_validation->set_message('_check_stock',"We only have ".$rs->stock." ".$rs->name." price ".$rs->fprice);
				
				$this->db->delete('so_check',array('id'=>$id));
				$this->db->delete('so_check_d',array('so_id'=>$id));
				return false;
			    }
			}
		    }
		    $key++;
		}
		
		$this->db->delete('so_check',array('id'=>$id));
		$this->db->delete('so_check_d',array('so_id'=>$id));
	    }
        }
        return true;
    }
    
    public function _check_tglso(){
        if(date('Y-m-d',now()) > $this->input->post('date')){
	    $allow = 0;
		
	    if($this->session->userdata('group_id')>100){
	    $this->form_validation->set_message('_check_tglso','Tanggal sales order tidak boleh mundur');
		return false;
	    }else{
		if($this->session->userdata('group_id')<100){
		    $row=$this->SO_model->get_blocked();
		    if($this->input->post('date') <= $row->tglrunning){
			$this->form_validation->set_message('_check_tglso','Tanggal '.$row->tglrunning.' bonus sudah dijalankan');
			return false;
		    }
		}
	    }
        }
        return true;
    }
    
    public function _check_pin(){
        if(!$this->MAuth->check_pin($this->session->userdata('username'),$this->input->post('pin'))){
            $this->form_validation->set_message('_check_pin','Sorry, your pin invalid');
            return false;
        }
        return true;
    }
	
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->SO_model->getSalesOrder($id);
        
        if(!count($row)){
            redirect('smartindo/so','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->SO_model->getSalesOrderDetail($id);
	
	$data['page_title'] = 'View Sales Order';
        $this->load->view('smartindo/salesorder_view',$data);
    }
    
}
?>
