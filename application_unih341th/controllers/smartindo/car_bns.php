<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Car_bns extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MAuth','MCar'));
    }
    
	public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','pagination'));
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        
		$keywords = $this->session->userdata('keywords');
		
		$config['base_url'] = site_url().'smartindo/car_bns/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->MCar->countCarRewardList($keywords,1);
		$this->pagination->initialize($config);
		
		$data['base_url'] = $config['base_url'];
		$data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $data['results'] = $this->MCar->getCarRewardList($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']),1);
		
        $data['page_title'] = 'Car Reward';
        $this->load->view('smartindo/v_car',$data);
		
    }
    
	public function newcarbns($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','pagination'));
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        
		$keywords = $this->session->userdata('keywords');
		
		$config['base_url'] = site_url().'smartindo/car_bns/newcarbns/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->MCar->countCarRewardList($keywords,1);
		$this->pagination->initialize($config);
		
		$data['base_url'] = $config['base_url'];
		$data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $data['results'] = $this->MCar->getCarRewardList($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']),2);
		
        $data['page_title'] = 'Car Reward';
        $this->load->view('smartindo/v_car_new',$data);
    }
	
	public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MCar->getDetail($id);
        
        if(!count($row)){
            redirect('smartindo/car_bns','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Car Bonus Detail';
        $this->load->view('smartindo/car_view',$data);
    }
	
	public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        $this->form_validation->set_rules('tipe','Type','required');
        $this->form_validation->set_rules('cop_val','','required');
        $this->form_validation->set_rules('cop_dp','','required');
        $this->form_validation->set_rules('status','','required');
        
		$row =array();
        $row = $this->MCar->getDetail($id);
        
        if(!count($row)){
            redirect('smartindo/car_bns','refresh');
        }
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MCar->edit();
                $this->session->set_flashdata('message','Update Car Bonus Successfully');
            }
            redirect('smartindo/car_bns','refresh');
        }
        $data['row'] = $row;
		$data['id_'] = $id;
        $data['page_title'] = 'Edit Car Bonus Term';
        $this->load->view('smartindo/car_edit',$data);
    }
}
?>