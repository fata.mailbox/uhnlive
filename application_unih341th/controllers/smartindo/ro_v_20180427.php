<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ro_v extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') <= 100){
            redirect('');
        }
		//20160404 - ASP Start
        //$this->load->model(array('MMenu','RO_model','GLobal_model'));
        $this->load->model(array('MMenu','RO_model','GLobal_model','Mruleorder'));
		//20160404 - ASP End
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'smartindo/ro_v/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']);
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->RO_model->countRO($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->RO_model->searchRO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->RO_model->countRO($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->RO_model->searchRO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
        $data['page_title'] = 'Request Order Stockist';
        $this->load->view('smartindo/ro_index_v',$data);
    }
    
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->RO_model->getRequestOrder_v($id);
        
        if(!count($row)){
            redirect('smartindo/ro_v','refresh');
        }
		$bns = $this->RO_model->getFree($id);
		$data['bns'] = $bns;
		$data['row'] = $row;
        $data['items'] = $this->RO_model->getRequestOrderDetail_v($id);
		
		$checkVoucher = $this->RO_model->count_search_voucher_ro($id);
		$data['checkVoucher'] = $checkVoucher;
		if($checkVoucher>0) {
		 $rowv = $this->RO_model->search_voucher_ro($id);
		 $data['rowv'] = $rowv;
		}
		
        $data['page_title'] = 'View Request Order';
        $this->load->view('smartindo/ro_view_v',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        	
        //$this->form_validation->set_rules('date','Tanggal SO','required|callback__check_tglso');
        $this->form_validation->set_rules('member_id','Stockiest','required');
	$this->form_validation->set_rules('persen','','');
	
	$this->form_validation->set_rules('pu','','');
	$this->form_validation->set_rules('oaddr','','');
	$this->form_validation->set_rules('idaddr','','');
	
	$this->form_validation->set_rules('timur','','');
	
	if($this->input->post('pu') == '1' and $this->input->post('oaddr') == 'N' and $this->input->post('idaddr') == ''){
	    $this->form_validation->set_rules('city','','callback__check_delivery');
	}
	$this->form_validation->set_rules('kota_id','','');
	    
	$this->form_validation->set_rules('propinsi','','');
	$this->form_validation->set_rules('addr','','');
		
	if($this->form_validation->run() == true){
	    $this->del_session();
        if(!$this->MMenu->blocked()){
		if($this->input->post('pu') == '1'){
			if ($this->input->post('oaddr') == 'N'){
				$idaddr=$this->input->post('idaddr');
				if(!$idaddr)$idaddr=0;
				
				if($idaddr > 0){
					$row = $this->GLobal_model->get_ro_delivery_id($idaddr);
					$this->session->set_userdata(
						array('ro_member_id'=>$this->input->post('member_id'),
						'ro_persen'=>$this->input->post('persen'),
						'ro_pu'=>$this->input->post('pu'),
						'ro_deli_ad'=>$idaddr,
						
						'ro_timur'=>$row->timur,
						'ro_kota_id'=>$row->kota_id,
						'ro_propinsi'=>$row->propinsi,
						'ro_city'=>$row->namakota,
						'ro_addr'=>$row->alamat,
						'ro_oaddr'=>$this->input->post('oaddr')
					));
				}else{
					$this->session->set_userdata(
						array('ro_member_id'=>$this->input->post('member_id'),
						'ro_persen'=>$this->input->post('persen'),
						'ro_pu'=>$this->input->post('pu'),
						'ro_deli_ad'=> 0,
						'ro_timur'=>$this->input->post('timur'),
						'ro_kota_id'=>$this->input->post('kota_id'),
						'ro_propinsi'=>$this->input->post('propinsi'),
						'ro_city'=>$this->input->post('city'),
						'ro_addr'=>$this->input->post('addr'),
						'ro_oaddr'=>$this->input->post('oaddr')
					));
				}
			}else{
				$idaddr = $this->input->post('deli_ad');
				if($idaddr > 0){
					$row = $this->GLobal_model->get_ro_delivery_id($idaddr);
					$this->session->set_userdata(
						array('ro_member_id'=>$this->input->post('member_id'),
						'ro_persen'=>$this->input->post('persen'),
						'ro_pu'=>$this->input->post('pu'),
						'ro_deli_ad'=>$idaddr,
						
						'ro_timur'=>$row->timur,
						'ro_kota_id'=>$row->kota_id,
						'ro_propinsi'=>$row->propinsi,
						'ro_city'=>$row->namakota,
						'ro_addr'=>$row->alamat,
						'ro_oaddr'=>$this->input->post('oaddr')
					));
				}else{
					$this->session->set_userdata(
						array('ro_member_id'=>$this->input->post('member_id'),
						'ro_persen'=>$this->input->post('persen'),
						'ro_pu'=>$this->input->post('pu'),
						'ro_deli_ad'=>0,
						
						'ro_timur'=>$this->input->post('timur'),
						'ro_kota_id'=>$this->input->post('kota_id'),
						'ro_propinsi'=>$row->propinsi,
						'ro_city'=>'',
						'ro_addr'=>'',
						'ro_oaddr'=>$this->input->post('oaddr')
					));
				}
			}
		}else{
		    $this->session->set_userdata(
		    array('ro_member_id'=>$this->input->post('member_id'),
			'ro_persen'=>$this->input->post('persen'),
			'ro_timur'=>$this->input->post('timur'),
			'ro_deli_ad'=>0,
			'ro_pu'=>$this->input->post('pu')
			)
		    );
		}
	    }
	    redirect('smartindo/ro_v/add','refresh');
	}
		
		$data['address'] = $this->GLobal_model->get_ro_diskon($this->session->userdata('userid'));
		$data['alamathistory'] = $this->GLobal_model->get_ro_delivery($this->session->userdata('userid'));
        $data['page_title'] = 'Create Request Order';
        $this->load->view('smartindo/ro_form_v',$data);
    }
    
    public function add(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
		$this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        	
        $this->form_validation->set_rules('member_id','Stockist','required');
        $this->form_validation->set_rules('remark','','');
		
		$this->form_validation->set_rules('idaddr','','');
		$this->form_validation->set_rules('pu','','');
		$this->form_validation->set_rules('oaddr','','');
		
		if($this->input->post('pu') == '1' and $this->input->post('oaddr') == 'N'){
			$this->form_validation->set_rules('city','','callback__check_delivery');
		}
		$this->form_validation->set_rules('kota_id','','');
	    $this->form_validation->set_rules('propinsi','','');
	    $this->form_validation->set_rules('addr','','');
	    $this->form_validation->set_rules('timur','','');
		
        $this->form_validation->set_rules('total','Total repeat order','required');
        $this->form_validation->set_rules('totalpv','','');
		$this->form_validation->set_rules('totalbv','','');
	
        $this->form_validation->set_rules('persen','','');
		$this->form_validation->set_rules('totalrpdiskon','','');
        $this->form_validation->set_rules('totalbayar','Total Bayar','callback__check_bayar');
	    
	$groupid = $this->session->userdata('group_id');
	
	/*
	if($_POST['action'] == 'Go'){
	    $rowx = $this->input->post('rowx');
	    $counti = count($_POST['counter'])+$rowx;
	}elseif($_POST['action'] == 'Submit')$counti = count($_POST['counter']);
	else $counti=5;
	*/
	$cekcounti = $this->session->userdata('counti');
	if(isset($cekcounti))
	{
		$counti = 5;
		if($_POST['action'] == 'Go'){
			$rowx = $this->input->post('rowx');
			$counti = count($_POST['counter'])+$rowx;
		}elseif($_POST['action'] == 'Submit')$counti = count($_POST['counter']);
		else {
			if(isset($_POST['counter']))
			$counti=count($_POST['counter']);
		}
	}else{
		$counti=5;
	}
	$this->session->set_userdata(array('counti'=>$counti));
	  
	$i=0;
	while($counti > $i){
	    $this->form_validation->set_rules('itemcode'.$i,'','');
	    $this->form_validation->set_rules('itemname'.$i,'','');
	    $this->form_validation->set_rules('qty'.$i,'','');
	    $this->form_validation->set_rules('price'.$i,'','');
	    $this->form_validation->set_rules('subtotal'.$i,'','');
	    $this->form_validation->set_rules('pv'.$i,'','');
	    $this->form_validation->set_rules('subtotalpv'.$i,'','');
	    $this->form_validation->set_rules('bv'.$i,'','');
	    $this->form_validation->set_rules('subtotalbv'.$i,'','');
	    
	    $this->form_validation->set_rules('rpdiskon'.$i,'','');
	    $this->form_validation->set_rules('subrpdiskon'.$i,'','');
	    $i++;
	}
	
	$data['counti']=$counti;
		
	//voucher
	/*
	if($_POST['actionv'] == 'Go'){
	    $rowxv = $this->input->post('rowxv');
	    $countv = count($_POST['vcounter'])+$rowxv;
	}elseif($_POST['actionv'] == 'Submit')$countv = count($_POST['vcounter']);
	else $countv = 1;
	*/
	$cekcountv = $this->session->userdata('countv');
	if(isset($cekcountv))
	{
		$countv = 1;
		if($_POST['actionv'] == 'Go'){
			$rowxv = $this->input->post('rowxv');
			$countv = count($_POST['vcounter'])+$rowxv;
		}elseif($_POST['actionv'] == 'Submit')$countv = count($_POST['vcounter']);
		else {
			if(isset($_POST['vcounter']))
			$countv = count($_POST['vcounter']);
		}
	}else{
		$countv = 1;
	}
	$this->session->set_userdata(array('countv'=>$countv)); 
	$v=0;
	while($countv > $v){
		$this->form_validation->set_rules('vouchercode'.$v,'','');
	    $this->form_validation->set_rules('vprice'.$v,'','');
	    $this->form_validation->set_rules('vsubtotal'.$v,'','');
	    $this->form_validation->set_rules('vpv'.$v,'','');
	    $this->form_validation->set_rules('vsubtotalpv'.$v,'','');
	    $this->form_validation->set_rules('vbv'.$v,'','');
	    $this->form_validation->set_rules('vsubtotalbv'.$v,'','');
	
		$v++;
	}
	$data['countv']=$countv;
	$this->form_validation->set_rules('vselectedvoucher','','');
	//eof voucher
		
	$this->form_validation->set_rules('total','Total repeat order','required|callback__check_stock|callback__check_min_order');
	$this->form_validation->set_rules('totalpv','','');
	$this->form_validation->set_rules('totalbv','','');
	if($this->form_validation->run() && $_POST['action'] != 'Go' && $_POST['actionv'] != 'Go'){
	    if(!$this->MMenu->blocked()){
		$this->RO_model->ro_add_v();
		$this->session->set_flashdata('message','Create request order successfully');
	    }
	    redirect('smartindo/ro_v','refresh');
	}
		$data['row'] = $this->GLobal_model->get_ro_diskon($this->session->userdata('ro_member_id'));
        $data['page_title'] = 'Create Request Order';
        $this->load->view('smartindo/ro_form2_v',$data);
    }
    
    public function _check_stock(){
	$whsid = 1;
	$memberid = $this->input->post('member_id');
	
	$amount = str_replace(".","",$this->input->post('total'));
	if($amount <= 0){
	    $this->form_validation->set_message('_check_stock','browse product...');
	    return false;
	}else{
	    if(!validation_errors()){
		
		$data = array(
		    'stockiest_id'=> $whsid,
		    'member_id'=> $memberid
		);
		$this->db->insert('so_check',$data);
	    
		$id = $this->db->insert_id();
	    
		$key=0;
		$whsid = 1;
		while($key < count($_POST['counter'])){
		    $qty = str_replace(".","",$this->input->post('qty'.$key));
		    $price = str_replace(".","",$this->input->post('price'.$key));
		    $itemid = $this->input->post('itemcode'.$key);
		    if($itemid and $qty > 0){
			$data=array(
			    'so_id' => $id,
			    'item_id' => $itemid,
			    'qty' => $qty,
			    'price' => $price
			);
			$this->db->insert('so_check_d',$data);
			
			//echo $id.' * '.$itemid.' * '.$price.' * '.$titipan_id.' * '.$stcid;
			
			$rs=$this->GLobal_model->check_stock_whs($id,$itemid,$price,$whsid);
			if(!$rs->stock){
			    $this->form_validation->set_message('_check_stock',"You don't have ".$itemid);
				
			    $this->db->delete('so_check',array('id'=>$id));
			    $this->db->delete('so_check_d',array('so_id'=>$id));
			    return false;
			}elseif($rs->stock < $rs->qty){
			    $this->form_validation->set_message('_check_stock',"We only have ".$rs->stock." ".$rs->name);
			    
			    $this->db->delete('so_check',array('id'=>$id));
			    $this->db->delete('so_check_d',array('so_id'=>$id));
			    return false;
			}
		    }
		    $key++;
		}
		
		$this->db->delete('so_check',array('id'=>$id));
		$this->db->delete('so_check_d',array('so_id'=>$id));
	    }
        }
        return true;
    }
    
    public function _check_bayar(){
        $amount = str_replace(".","",$this->input->post('total'));
	$diskon = str_replace(".","",$this->input->post('totalrpdiskon'));
        $totalbayar = $amount-$diskon;
        
        $row = $this->GLobal_model->get_ewallet_stc($this->input->post('member_id'));
        if($row){
            if($totalbayar > $row->ewallet){
                $this->form_validation->set_message('_check_bayar','Pembayaran tidak mencukupi, ewallet anda kurang Rp. '.number_format($totalbayar-$row->ewallet,0,'','.'));
                return false;
            }
        }else{
	    $this->form_validation->set_message('_check_bayar','Invalid member, silahkan hubungi IT UHN');
            return false;
	}
	
        return true;
    }
    
    
    public function del_session(){
	$this->session->unset_userdata('ro_member_id');
	$this->session->unset_userdata('ro_persen');
	$this->session->unset_userdata('ro_pu');
	$this->session->unset_userdata('ro_timur');
	$this->session->unset_userdata('ro_kota_id');
	$this->session->unset_userdata('ro_addr');
	$this->session->unset_userdata('ro_oaddr');
	$this->session->unset_userdata('ro_propinsi');
	$this->session->unset_userdata('ro_city');
	$this->session->unset_userdata('ro_deli_ad');
    }
    public function _check_delivery(){
        if($this->input->post('pu')==1 and $this->input->post('oaddr') == 'N'){
		    if(strlen($this->input->post('city'))<1 OR strlen($this->input->post('addr'))<1){
				$this->form_validation->set_message('_check_delivery','Please fill the address.');
			return false;
		    }
        }
        return true;
    }
	//20160404 - ASP Start
    public function _check_min_order(){
		$key=0;
		while($key < count($_POST['counter'])){
			$qty = str_replace(".","",$this->input->post('qty'.$key));
		    $itemid = $this->input->post('itemcode'.$key);
			$getMinOrder = $this->Mruleorder->getRuleOrder($itemid);
			$getItemName = $this->Mruleorder->getItemName($itemid);
			if($itemid and $qty > 0){
				if($qty%$getMinOrder <> 0){
					$this->form_validation->set_message('_check_min_order',"Pembelian ".$itemid."-".$getItemName." harus kelipatan ".$getMinOrder." .");
					return false;
				}
			}
		    $key++;
		}
	}
	//20160404 - ASP End
}
?>