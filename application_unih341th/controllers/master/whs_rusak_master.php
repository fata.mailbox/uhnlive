<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Whs_rusak_master extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','Mrusak'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
	$config['base_url'] = site_url().'master/whs_rusak_master/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->Mrusak->countWarehouse_rusak($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->Mrusak->searchWarehouse_rusak($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->Mrusak->countWarehouse_rusak($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->Mrusak->searchWarehouse_rusak($keywords,$config['per_page'],  $this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Warehouse Rusak';
        $this->load->view('master/whs_rusak_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('nama','Warehouse Name','required');
        $this->form_validation->set_rules('id_wh','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->Mrusak->addWarehouse_rusak();
                $this->session->set_flashdata('message','Create warehouse successfully');
            }
            redirect('master/whs_rusak_master','refresh');
        }
        //else if(!$this->form_validation->run()) {echo "gagal";}
        
        $data['warehouse'] = $this->Mrusak->getWarehouse();
        $data['page_title'] = 'Create Warehouse';
        $this->load->view('master/whs_rusak_form',$data);
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->Mrusak->getWarehouse($id);
        
        if(!count($row)){
            redirect('master/whs_rusak_master','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Warehouse';
        $this->load->view('master/warehouse_view',$data);
    }
    
    
}

// created by Annisa Rahmawaty

?>