<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Prop extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MKota'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'master/prop/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MKota->countPropinsi($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MKota->searchPropinsi($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MKota->countPropinsi($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MKota->searchPropinsi($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Provinci';
        $this->load->view('master/propinsi_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('name','Propinsi','required|callback__check_id');
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MKota->addPropinsi();
                $this->session->set_flashdata('message','Create propinci successfully');
            }
            redirect('master/prop','refresh');
        }
        
        $data['page_title'] = 'Create Provinci';
        $this->load->view('master/propinsi_form',$data);
    }
    public function _check_id(){
        if($this->MKota->check_propinsi($this->input->post('name'))){
            $this->form_validation->set_message('_check_id','Provinci available !');
            return false;
        }
        return true;
    } 
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MKota->getPropinsi($id);
        
        if(!count($row)){
            redirect('master/prop','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Provinci';
        $this->load->view('master/propinsi_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('name','Provinci','required');
        $row =array();
        $row = $this->MKota->getPropinsi($id);
        
        if(!count($row)){
            redirect('master/prop','refresh');
        }
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MKota->editPropinsi();
                $this->session->set_flashdata('message','Update provinci successfully');
            }
            redirect('master/prop','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'Edit Provinci';
        $this->load->view('master/propinsi_edit',$data);
    }
    
}
?>