<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mp extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if($this->session->userdata('group_id') < 1 or $this->session->userdata('group_id') >= 100){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MPlan'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'master/mp/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MPlan->count_plan($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MPlan->search_plan($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Marketing Plan';
        $this->load->view('master/marketingplan_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('category','category','required');
        $this->form_validation->set_rules('sort','sort','required');
        $this->form_validation->set_rules('longdesc','longdesc','required');
        $this->form_validation->set_rules('status','status','');
        
        if($this->form_validation->run()){
            $this->MPlan->add_plan();
            $this->session->set_flashdata('message','Marketing plan created');
            redirect('master/mp/','refresh');
        }
        $data['page_title'] = 'Create Marketing Plan';
        $this->load->view('master/marketingplan_form',$data);
    }
        
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $data['row'] = $this->MPlan->get_plan($id);
            
        if(!count($data['row'])){
            redirect('master/mp/','refresh');
        }
        
        $data['page_title'] = 'Marketing Plan';
        $this->load->view('master/marketingplan_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('category','category','required');
        $this->form_validation->set_rules('sort','sort','required');
        $this->form_validation->set_rules('longdesc','longdesc','required');
        $this->form_validation->set_rules('status','status','');
        
        if($this->form_validation->run()){
            $this->MPlan->update_plan();
            $this->session->set_flashdata('message','Marketing plan updated');
            redirect('master/mp/','refresh');
        }else{
            if($_POST)$id=$this->input->post('id');
            $data['row'] = $this->MPlan->get_plan($id);
            
            if(!count($data['row'])){
                redirect('master/mp/','refresh');
            }
            
            $data['page_title'] = 'Edit Marketing Plan';
            $this->load->view('master/marketingplan_edit',$data);
        }
    }
}
?>