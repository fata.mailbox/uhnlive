<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kota extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MKota'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        $this->form_validation->set_rules('whsid','','');
		
        $config['base_url'] = site_url().'master/kota/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $this->session->set_userdata('keywords_whsid',$this->db->escape_str($this->input->post('whsid')));
			
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MKota->countKota($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MKota->searchKota($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords_whsid');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MKota->countKota($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MKota->searchKota($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
		$data['warehouse']=$this->MKota->getDropDownWhs_('all');
        $data['page_title'] = 'City';
        $this->load->view('master/kota_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('name','Kota','required|callback__check_id');
        $this->form_validation->set_rules('propinsi_id','','');
        $this->form_validation->set_rules('warehouse_id','','');
        $this->form_validation->set_rules('region_id','','');
        $this->form_validation->set_rules('mqo','','');
        $this->form_validation->set_rules('ongkoskirim','','');
        $this->form_validation->set_rules('timur','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MKota->addKota();
                $this->session->set_flashdata('message','Create city successfully');
            }
            redirect('master/kota','refresh');
        }
        $data['region'] = $this->MKota->getRegion_();
        $data['harga'] = $this->MKota->getHarga();
        $data['warehouse'] = $this->MKota->getDropDownWhs();
        $data['propinsi'] = $this->MKota->getDropDownPropinsi();
        $data['page_title'] = 'Create City';
        $this->load->view('master/kota_form',$data);
    }
    public function _check_id(){
        if($this->MKota->check_propinsi($this->input->post('name'))){
            $this->form_validation->set_message('_check_id','Propinci available !');
            return false;
        }
        return true;
    } 
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MKota->getKota($id);
        
        if(!count($row)){
            redirect('master/kota','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View City';
        $this->load->view('master/kota_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        $this->form_validation->set_rules('name','Kota','');
        $this->form_validation->set_rules('propinsi_id','','');
        $this->form_validation->set_rules('warehouse_id','','');
        $this->form_validation->set_rules('mqo','','');
        $this->form_validation->set_rules('ongkoskirim','','');
        $this->form_validation->set_rules('region_id','','');
        $this->form_validation->set_rules('timur','','');
		
        $row =array();
        $row = $this->MKota->getKota($id);
        
        if(!count($row)){
            redirect('master/kota','refresh');
        }
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MKota->editKota();
                $this->session->set_flashdata('message','Update city successfully');
            }
            redirect('master/kota','refresh');
        }
        $data['row'] = $row;
        $data['region'] = $this->MKota->getRegion_();
        $data['harga'] = $this->MKota->getHarga();
        $data['warehouse'] = $this->MKota->getDropDownWhs();
        $data['propinsi'] = $this->MKota->getDropDownPropinsi();
        $data['page_title'] = 'Edit City';
        $this->load->view('master/kota_edit',$data);
    }
    
}
?>