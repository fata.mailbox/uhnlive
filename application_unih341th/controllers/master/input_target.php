<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Input_target extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('MSales','MMenu'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('tahun','','');
        //$this->form_validation->set_rules('sort','','');
        
        if($this->form_validation->run()){
            $data['results']=$this->MSales->viewTarget($this->input->post('tahun'));
            $data['thn']=$this->input->post('tahun');
			$data['total']=0;
        }else{
			$thn=date("Y");
            $data['results']=false;
			$data['thn']=date("Y");
            $data['total']=false;
        }
        $data['dropdownyear'] = $this->MSales->get_year_report();
        $data['page_title'] = 'Sales Target '.$a;
        $this->load->view('master/sales_target_list',$data);
    }
	
    public function create(){
		$z=1;
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
		$this->form_validation->set_rules('tahun','','');
		$this->form_validation->set_rules('quart','','');
		$this->form_validation->set_rules('region','','');
        
        $groupid = $this->session->userdata('group_id');
        $data['z']=$z;
		$flag = 0;
        if($this->form_validation->run()){
			$flag = 1;
            if(!$this->MMenu->blocked()){
				$flag = 2;
				if($z<1){	
					$flag = 31;
					$this->form_validation->set_rules('target','Omset Target','required');
					$this->form_validation->set_rules('nr','Recruitment Target','required');
		
					$period = $this->input->post('tahun');
					$period.= '-';
					$period.= $this->input->post('quart');
					
					$results = $this->MSales->cek_data_target($period, $this->input->post('region'));
					if($results == 'ok'){
						$target = str_replace('.','',$this->input->post('target'));
						$nr = str_replace('.','',$this->input->post('nr'));
						$this->MSales->insert_data_target($period, $this->input->post('region'), $target, $nr);
						$this->session->set_flashdata('message','Create Target Successfully'.$flag);
						redirect('master/input_target/create/','refresh');
					}else{
						$this->session->set_flashdata('message','Data Already Exists'.$flag);
						redirect('master/input_target/create/','refresh');
					}
				}else{
					$flag = 32;
					for($i=1;$i<=12;$i++){
						$this->form_validation->set_rules('target'.$i,'','');
						$this->form_validation->set_rules('nr'.$i,'','');
						
						if($i<10){$month='0'.$i;}else{$month=$i;}
						$period = $this->input->post('tahun').'-';
						$period.= $month.'-01';
						
						$target = str_replace('.','',$this->input->post('target'.$i));
						$nr = str_replace('.','',$this->input->post('nr'.$i));
						
						$results = $this->MSales->cek_data_target($period, $this->input->post('region'));
						if($results == 'ok'){
							$this->MSales->insert_data_target($period, $this->input->post('region'), $target, $nr);
						}else{
							$this->MSales->update_data_target($period, $this->input->post('region'), $target, $nr);
						}
					}
					$this->session->set_flashdata('message','Create Target Successfully'.$flag);
					redirect('master/input_target/create/','refresh');
				}
            }
			$this->session->set_flashdata('message','Flag = '.$flag);
            redirect('master/input_target','refresh');
        }else{
			$flag = 999;
			$this->session->set_flashdata('message','Flag = '.$flag);
		}
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-green.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['groupid'] = $groupid;
		$data['dropdownyear'] = $this->MSales->get_y();
		$data['dropdownq'] = $this->MSales->get_q(0);
		$data['dropdownreg'] = $this->MSales->get_region();
        $data['page_title'] = 'Create Sales Target '.$flag;
        $this->load->view('master/sales_target_form',$data);
    }   
}
?>