<?php
defined('BASEPATH') or exit('No direct script access allowed');

class monitoring_voucher extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100) {
            redirect('');
        }
        $this->load->model(array('MMenu', 'MItem', 'MVoucher'));
    }

    public function index()
    {
        $data['page_title'] = 'Voucher Monitoring';
        $data['list'] = $this->MVoucher->monit_voucher();

        $this->load->view('master/monitoring_voucher', $data);
    }

    public function add()
    {
        $vcode =  $this->uri->segment(6);
        $stc_id =  $this->uri->segment(5);
        $id_voucher =  $this->uri->segment(4);
        $data['stc'] = $stc_id;
        $data['list'] = $this->MVoucher->list_voucher($id_voucher,$stc_id,$vcode);
        $data['page_title'] = 'Voucher Monitoring Detail';
        $data['date'] = $this->MVoucher->voucher_monitoring($id_voucher);
        $this->load->view('master/monitoring_add', $data);
    
    
    }

    
}

/* End of file monitoring_voucher.php */
