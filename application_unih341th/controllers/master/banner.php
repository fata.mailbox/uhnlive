<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if($this->session->userdata('group_id') < 1 or $this->session->userdata('group_id') >= 100){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MBanner'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'master/banner/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MBanner->countBanner($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MBanner->searchBanner($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
            
        $data['totalrow'] = $config['total_rows'];
        $data['page_title'] = 'Banner';
        $this->load->view('master/banner_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        #ini_set("upload_max_filesize",30);
	#set_time_limit(0);

        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('title','title','required');
        $this->form_validation->set_rules('shortdesc','short description','required');
        $this->form_validation->set_rules('status','','');
		$this->form_validation->set_rules('url','','');
		$this->form_validation->set_rules('fromdate','Exp date','required');
        $this->form_validation->set_rules('nourut','order by (a-z)','required|numeric');
		$this->form_validation->set_rules('userfile','','');
	        
        if($this->form_validation->run()){
            $config['upload_path'] = './userfiles/banner/';
            $config['allowed_types'] = 'gif|jpeg|jpg|png';
            $config['max_size']    = '5000'; //1 MB
            //$config['max_width']  = '1600';
            //$config['max_height']  = '1200';
            $config['remove_spaces']    = 'true';
            $config['overwrite']        = 'true'; 
                
            $this->load->library('upload', $config);
            
            if($this->upload->do_upload()){
                $tp=$this->upload->data(); //ambil atribut data yang baru saja diupload
                $this->load->library('myresizer'); //load library
                
		$image= "userfiles/banner/".$tp['file_name']; //jangan lupa diset permissionnya
                
		$newimage= "userfiles/banner/t_".$tp['file_name']; //jangan lupa diset permissionnya
                $newwidth=65; //width yang diinginkan
                $newheight=62; //height yang diinginkan
                
                $params = array(
			'file' => $image,
			'newfile' => $newimage,
			'width' => $tp['image_width'],
			'height' => $tp['image_height'],
			'newwidth' => $newwidth,
			'newheight' => $newheight
		);
		$this->myresizer->resizecrop($params);
		    	
		$newimage= "userfiles/banner/".$tp['file_name']; //jangan lupa diset permissionnya
                $newwidth=1000; //width yang diinginkan
                $newheight=300; //height yang diinginkan
                
		$params = array(
			'file' => $image,
			'newfile' => $newimage,
			'width' => $tp['image_width'],
			'height' => $tp['image_height'],
			'newwidth' => $newwidth,
			'newheight' => $newheight
		);
                $this->myresizer->resizecrop($params);
                
                $image = $tp['file_name'];
                $this->MBanner->addBanner($image);
                $this->session->set_flashdata('message','Banner created');
                redirect('master/banner/','refresh');
            }else{
                $data['error'] = array('error' => $this->upload->display_errors());
                
                $error = array('error' => $this->upload->display_errors());
                $x = $error['error'];
                $y = "<p>You did not select a file to upload.</p>";
                if($x == $y){
                    $this->MBanner->addBanner('');
                    $this->session->set_flashdata('message','Banner created');
                    redirect('master/banner/','refresh');
                }
                $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
		$data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
		$data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
		$data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
		$data['page_title'] = 'Create Banner';
                $this->load->view('master/banner_form',$data);
            }                
        }
        else{
	    $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
	    $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
	    $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
	    $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
	    
	
            $data['page_title'] = 'Create Banner';
            $this->load->view('master/banner_form',$data);
        }
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $data['row'] = $this->MBanner->getBanner($id);
            
        if(!count($data['row'])){
            redirect('master/banner/','refresh');
        }
        
        $data['page_title'] = 'View Banner';
        $this->load->view('master/banner_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('title','title','required');
        $this->form_validation->set_rules('shortdesc','short description','required');
        $this->form_validation->set_rules('status','','');
        $this->form_validation->set_rules('url','','');
		$this->form_validation->set_rules('fromdate','','required');
        $this->form_validation->set_rules('nourut','order by (a-z)','required|numeric');
            
        if($this->form_validation->run()){
            $config['upload_path'] = './userfiles/banner/';
            $config['allowed_types'] = 'gif|jpeg|jpg|png';
            $config['max_size']    = '1024'; //5 MB
            //$config['max_width']  = '1600';
            //$config['max_height']  = '1200';
            $config['remove_spaces']    = 'true';
            $config['overwrite']        = 'true'; 
                
            $this->load->library('upload', $config);
            $this->load->library('myresizer'); //load library
	    
            if($this->upload->do_upload()){                
                $tp=$this->upload->data(); //ambil atribut data yang baru saja diupload
                
                if($this->input->post('namafile') != $tp['file_name'] and $this->input->post('namafile')){
                    $place = $_SERVER['DOCUMENT_ROOT'].'userfiles/banner/'.$this->input->post('namafile');
                    if(file_exists($place)){
                        unlink($place);
                    }
					$place = $_SERVER['DOCUMENT_ROOT'].'userfiles/banner/t_'.$this->input->post('namafile');
                    if(file_exists($place)){
                        unlink($place);
                    }
                }
                
                $image= "userfiles/banner/".$tp['file_name']; //jangan lupa diset permissionnya
				$newimage= "userfiles/banner/t_".$tp['file_name']; //jangan lupa diset permissionnya
                $newwidth=65; //width yang diinginkan
                $newheight=62; //height yang diinginkan
                
                $params = array(
			'file' => $image,
			'newfile' => $newimage,
			'width' => $tp['image_width'],
			'height' => $tp['image_height'],
			'newwidth' => $newwidth,
			'newheight' => $newheight
		);
		$this->myresizer->resizecrop($params);
		    	
		$newimage= "userfiles/banner/".$tp['file_name']; //jangan lupa diset permissionnya
                $newwidth=1000; //width yang diinginkan
                $newheight=300; //height yang diinginkan
                
		$params = array(
			'file' => $image,
			'newfile' => $newimage,
			'width' => $tp['image_width'],
			'height' => $tp['image_height'],
			'newwidth' => $newwidth,
			'newheight' => $newheight
		);
                $this->myresizer->resizecrop($params);
                
                $filename = $tp['file_name'];
                $this->MBanner->updateBanner($filename);
                $this->session->set_flashdata('message','Banner updated');
                redirect('master/banner/','refresh');
            }else{
                if($_POST)$id=$this->input->post('id');
                $data['row'] = $this->MBanner->getBanner($id);
                
                if(!count($data['row'])){
                    redirect('master/banner/','refresh');
                }
            
                $error = array('error' => $this->upload->display_errors());
                $x = $error['error'];
                $y = "<p>You did not select a file to upload.</p>";
                if($x == $y){
                    $this->MBanner->updateBanner('');
                    $this->session->set_flashdata('message','Banner updated');
                    redirect('master/banner/','refresh');
                }
				$data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
				$data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
				$data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
				$data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
				
				$data['jscal'] = '1';
                $data['error'] = $error;
                $data['page_title'] = 'Edit Banner';
                $this->load->view('master/banner_edit',$data);
            }
        }else{
            if($_POST)$id=$this->input->post('id');
            $data['row'] = $this->MBanner->getBanner($id);
            
            if(!count($data['row'])){
                redirect('master/banner/','refresh');
            }
			$data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
			$data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
			$data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
			$data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
			
			$data['jscal'] = '1';
            $data['page_title'] = 'Edit Banner';
            $this->load->view('master/banner_edit',$data);
        }
    }
	
	public function edit_img($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('id','id news','required');
		$this->form_validation->set_rules('status','status','required');
            
        if($this->form_validation->run()){
            $config['upload_path'] = './userfiles/banner/';
            $config['allowed_types'] = 'gif|jpeg|jpg|png';
            $config['max_size']    = '25600'; //25 MB
            //$config['max_width']  = '1600';
            //$config['max_height']  = '1200';
            $config['remove_spaces']    = 'true';
            $config['overwrite']        = 'false'; 
                
            $this->load->library('upload', $config);
	    
            if($this->upload->do_upload()){                
                $tp=$this->upload->data(); //ambil atribut data yang baru saja diupload
                
                if($this->input->post('namafile') != $tp['file_name'] and $this->input->post('namafile')){
                   $place = $_SERVER['DOCUMENT_ROOT'].'userfiles/banner/big_'.$this->input->post('namafile');
                    if(file_exists($place)){
                        unlink($place);
                    }
                }
                
                $image= "userfiles/banner/".$tp['file_name']; //jangan lupa diset permissionnya
				//$newimage= "userfiles/banner/big_".$tp['file_name']; //jangan lupa diset permissionnya
                //$newwidth=65; //width yang diinginkan
                //$newheight=62; //height yang diinginkan
                
                $filename = $tp['file_name'];
                $this->MBanner->update_big_banner($filename);
                $this->session->set_flashdata('message','Big Banner updated');
                redirect('master/banner/','refresh');
            }else{
				if($_POST)$id=$this->input->post('id');
                $data['row'] = $this->MBanner->getBanner($id);
                
                if(!count($data['row'])){
                    redirect('master/banner/','refresh');
                }
            
                $error = array('error' => $this->upload->display_errors());
                $x = $error['error'];
                $y = "<p>You did not select a file to upload.</p>";
                if($x == $y){
                    $this->MBanner->update_big_banner('');
                    $this->session->set_flashdata('message','Big Banner updated');
                    redirect('master/banner/','refresh');
                }
                $data['error'] = $error;
                $data['page_title'] = 'Edit Big Banner';
                $this->load->view('master/banner_big_edit',$data);
			}
        }else{
            if($_POST)$id=$this->input->post('id');
            $data['row'] = $this->MBanner->getBanner($id);
            
            if(!count($data['row'])){
                redirect('master/banner/','refresh');
            }
			
            $data['page_title'] = 'Edit Big Banner';
            $this->load->view('master/banner_big_edit',$data);
        }
    }
	
	
}
?>