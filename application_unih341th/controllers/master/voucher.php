<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Voucher extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->file('asset/phpexcel/PHPExcel.php');
    $this->load->file('asset/phpexcel/PHPExcel/IOFactory.php');

    if (!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100) {
      redirect('');
    }

    $this->load->model(array('MMenu', 'MItem', 'MVoucher'));
  }

  public function index()
  {
    if ($this->MMenu->access($this->session->userdata('group_id'), $this->uri->segment(2), 'view')) {
      redirect('error', 'refresh');
    }

    $this->load->library(array('form_validation', 'pagination'));

    $this->form_validation->set_rules('search', '', '');

    $config['base_url'] = site_url() . 'master/item/index/';
    $config['per_page'] = 20;
    $config['uri_segment'] = 4;
    $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut pagingg

    if ($this->form_validation->run()) {
      $this->session->set_userdata('keywords', $this->db->escape_str($this->input->post('search')));
      $keywords = $this->session->userdata('keywords');
      $config['total_rows'] = $this->MVoucher->countItem($keywords);
      $this->pagination->initialize($config);
      $data['results'] = $this->MVoucher->searchItem($keywords, $config['per_page'], $this->uri->segment($config['uri_segment']));
    } else {
      if (!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
      $keywords = $this->session->userdata('keywords');
      $config['total_rows'] = $this->MVoucher->countItem($keywords);
      $this->pagination->initialize($config);
      $data['results'] = $this->MVoucher->searchItem($keywords, $config['per_page'], $this->uri->segment($config['uri_segment']));
    }

    $data['page_title'] = 'Voucher';
    $this->load->view('master/voucher_index', $data);
  }

  public function create()
  {

    $this->load->library(array('form_validation', 'messages'));
    $this->form_validation->set_rules('valid-from', 'Valid From', 'required');
    $this->form_validation->set_rules('valid-to', 'Valid To', 'required');
    $this->form_validation->set_rules('remarks', 'Remarks', 'required');
    if (empty($_FILES['upload']['name'])) {
      $this->form_validation->set_rules('upload', 'List Penerima', 'required');
    }
    $data_post = $_POST;
    if ($this->form_validation->run()) {
      if (!$this->MMenu->blocked()) {
        // Load PHPExcel
        $v = $_FILES['upload']['name'];
        $cek = $this->db->query("SELECT path_upload from voucher_user where path_upload =  '$v' ")->num_rows();
        
        if ($cek > 0) {
          
          echo "<script>alert('File sudah ada di database, silahkan rename file !')</script>";
				redirect('master/voucher/create', 'refresh');
        }else {
          
        $target = './asset/data/voucher/' . basename($_FILES['upload']['name']);
        move_uploaded_file($_FILES['upload']['tmp_name'], $target);
        $path = './asset/data/voucher/' . $_FILES['upload']['name'];
        $this->db->empty_table('temp_voucher');
        $res = array();
        $ser = array();
        $objPHPExcel = PHPExcel_IOFactory::load($path);
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
          $worksheetTitle = $worksheet->getTitle();
          $highestRow = $worksheet->getHighestRow();
          $highestColumn = $worksheet->getHighestColumn();
          $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
          $nrColumns = ord($highestColumn) - 64;
          $no = 2;
          for ($row = 2; $row <= $highestRow; ++$row) {
            $val = array();
            for ($col = 0; $col < $highestColumnIndex; ++$col) {
              $cell = $worksheet->getCellByColumnAndRow($col, $row);
              $val[] = $cell->getValue();
            }
            if (!empty($val[0])) {
              if (call($val[0], 'member') == 'tidak ada') {
                array_push($res, $no);
              } else {
                if (call($val[1], 'member') == 'tidak ada') {
                  array_push($res, $no);
                } else {
                  $gj =
                    [
                      "stc_id" => $val[0],
                      "member_id" => $val[1],
                      "nominal" => $val[2],
                      "remarks" => $val[3],
                      "pv"  => $val[4],
                      "bv" => $val[5],
                      "status" => $val[6],
                      "posisi" => $val[7]
                    ];
                  array_push($ser, $gj);
                }
              }

              if (!empty($res)) {
                $dz =
                  [
                    "no" => $no,
                    "stc_id" => $val[0],
                    "member_id" => $val[1],
                    "nominal" => $val[2],
                    "remarks" => $val[3],
                    "pv"  => $val[4],
                    "bv" => $val[5],
                    "status" => $val[6],
                    "posisi" => $val[7]
                  ];

                $this->db->insert('temp_voucher', $dz);
                $no++;
              }
            }
          }
        }
      }

      if (!empty($res)) {
        $this->session->set_flashdata('message', $res);
        redirect('master/voucher/create', 'refresh');
      } else if (!empty($ser) && empty($res)) {
        $id_voucher_path = $this->MVoucher->addVoucher($data_post,$_FILES['upload']['name']);
        $valid_to = $this->input->post('valid-to');
        foreach ($ser as $key) {
          if (empty($key['status'])) {
            $status = 0;
          } else {
            $status = $key['status'];
          }
          if (empty($key['posisition'])) {
            $posisi = 0;
          } else {
            $posisi = $key['posisition'];
          }
          $rowdata = array(
            'stc_id'    => $key['stc_id'],
            'member_id'      => $key['member_id'],
            'price'        => $key['nominal'],
            'pv'        => $key['pv'],
            'bv'        => $key['bv'],
            'remark'  => $key['remarks'],
            'expired_date' => $valid_to,
            'status' => $status,
            'posisi' => $posisi,
            'minorder' => 0
          );

          $iu =  $this->MVoucher->voucheradd($rowdata);
          if (!empty($iu)) {
            $tra = [
              "vouchercode" => $iu,
              "id_voucher"  => $id_voucher_path
            ];
            $this->db->insert('voucher_user_detail', $tra);
          }
        }

        $this->session->set_flashdata('message', 'Create voucher data successfully');
        redirect('master/voucher/', 'refresh');
      }
        }



    } else {
      $data['page_title'] = 'Create Voucher Data';
      $this->load->view('master/voucher_form', $data);
    }
  }


  public function _check_id()
  {
    if ($this->MItem->check_productid($this->input->post('id'))) {
      $this->form_validation->set_message('_check_id', 'product id available !');
      return false;
    }
    return true;
  }


  public function search()
  {
    if (isset($_POST['searchTerm'])) {
      $search = $_POST['searchTerm'];
      $hasil = $this->MVoucher->search_voucher($search);
      $response = [];
      foreach ($hasil as $key) {
        $h['id'] = $key['id'];
        $h['text'] =codestc($key['id']).'  '.$key['nama'];
        array_push($response, $h);
      }
      echo json_encode($response);
    }
  }

  public function view($id = 0)
  {
    $row = array();
    $row = $this->MVoucher->getItem($id);
    if (!count($row)) {
      redirect('master/voucher', 'refresh');
    }

    $data['results'] = $row;
    $data['page_title'] = 'View Voucher Data';
    $this->load->view('master/voucher_view', $data);
  }

  public function ud()
  {
    $stc_id = $this->input->post('x');
    $vouchercode = $this->input->post('vouchercode');
    $this->MVoucher->ud_search($stc_id,$vouchercode);
  }
  public function update($id = 0)
  {

    $row = array();
    $row = $this->MVoucher->getItem($id);
    $valid = detail($id);
    if (!count($row)) {
      redirect('master/voucher', 'refresh');
    }
    $data['results'] = $row;
    $data['valid_from'] = $valid['valid_from'];
    $data['valid_to'] = $valid['valid_to'];
    $data['page_title'] = 'Edit Voucher Data';
    $data['id'] = $id;
    $this->load->view('master/edit_voucher', $data);
  }
  public function delete($id)
  {
	$canDel = $this->MVoucher->checkCanDelVou($id);
	if($canDel->nRow == $canDel->cSRO){
    $this->db->select('vouchercode');
    $this->db->from('voucher_user_detail');
    $this->db->where('id_voucher', $id);
    $r = $this->db->get()->result_array();
    if (!count($r)) {
      redirect('master/voucher', 'refresh');
    }
    foreach ($r as $key) {
      $data[] = "'" . $key['vouchercode'] . "'" . ',';
    }
    $t = implode(",", $data);
    $iu = str_replace(',,', ',', $t);
    $df = '(' . rtrim($iu, ',') . ')';
    $this->MVoucher->ud_search($df,$id);
    $this->session->set_flashdata('message', 'Delete voucher data successfully');
    redirect('master/voucher', 'refresh');
	}else{
		$this->session->set_flashdata('message', 'You Cant Delete Voucher '. $canDel->remarks .'!');
		redirect('master/voucher', 'refresh');
	}
  }

  public function edit($id = 0)
  {
    if ($this->MMenu->access($this->session->userdata('group_id'), $this->uri->segment(2), 'edit')) {
      redirect('error', 'refresh');
    }

    $this->load->library(array('form_validation', 'messages'));

    $this->form_validation->set_rules('id', 'Product ID', 'required');
    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('headline', '', '');
    $this->form_validation->set_rules('type', '', '');
    $this->form_validation->set_rules('display', '', '');
    $this->form_validation->set_rules('manufaktur', '', '');
    $this->form_validation->set_rules('sales', '', '');
    $this->form_validation->set_rules('longdesc', '', '');
    $this->form_validation->set_rules('price', '', '');
    $this->form_validation->set_rules('price2', '', ''); // created by Boby 2014-04-04
    $this->form_validation->set_rules('pv', '', '');
    $this->form_validation->set_rules('minbuy', '', ''); //created by Andri pratama 2018
    $this->form_validation->set_rules('categoryid', '', '');
    $this->form_validation->set_rules('warehouse_id', '', ''); //created by Andri pratama 2018
    $this->form_validation->set_rules('subcategoryid', '', '');
    $this->form_validation->set_rules('new', '', '');
    $this->form_validation->set_rules('promo', '', '');

    /* Update by Boby 2011-02-22 */
    $this->form_validation->set_rules('price_', '', '');
    $this->form_validation->set_rules('pv_', '', '');
    /* End update by Boby 2011-02-22 */

    $this->form_validation->set_rules('pricecust', '', '');

    $row = array();
    $row = $this->MItem->getItem($id);

    if (!count($row)) {
      redirect('master/item', 'refresh');
    }

    if ($this->form_validation->run()) {
      if (!$this->MMenu->blocked()) {
        $config['upload_path'] = './userfiles/product/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']    = '10240'; //10 MB
        $config['max_width']  = '1600';
        $config['max_height']  = '1200';

        $this->load->library('upload', $config);

        if ($this->upload->do_upload()) {
          $tp = $this->upload->data(); //ambil atribut data yang baru saja diupload

          if ($this->input->post('namafile') != $tp['file_name'] and $this->input->post('namafile')) {
            $place = $_SERVER['DOCUMENT_ROOT'] . "userfiles/product/" . $this->input->post('namafile');
            if (file_exists($place)) {
              unlink($place);
            }

            $place = $_SERVER['DOCUMENT_ROOT'] . "userfiles/product/t_" . $this->input->post('namafile');
            if (file_exists($place)) {
              unlink($place);
            }
          }
          $this->load->library('myresizer'); //load library
          $image = "userfiles/product/" . $tp['file_name']; //jangan lupa diset permissionnya

          //if($this->input->post('cover') == '1'){
          $newimage2 = "userfiles/product/t_" . $tp['file_name']; //jangan lupa diset permissionnya
          $newwidth = 214; //width yang diinginkan
          $newheight = 141; //height yang diinginkan
          $params = array(
            'file' => $image,
            'newfile' => $newimage2,
            'width' => $tp['image_width'],
            'height' => $tp['image_height'],
            'newwidth' => $newwidth,
            'newheight' => $newheight
          );
          $this->myresizer->resizecrop($params);
          //}

          $newimage4 = "userfiles/product/" . $tp['file_name']; //jangan lupa diset permissionnya
          $newwidth = 410; //width yang diinginkan
          $newheight = 410; //height yang diinginkan

          $params = array(
            'file' => $image,
            'newfile' => $newimage4,
            'width' => $tp['image_width'],
            'height' => $tp['image_height'],
            'newwidth' => $newwidth,
            'newheight' => $newheight
          );
          $this->myresizer->resizecrop($params);

          $filename = $tp['file_name'];
          $this->MItem->editItem($filename);
          $this->session->set_flashdata('message', 'Update item product successfully');
          redirect('master/item', 'refresh');
        } else {
          $error = array('error' => $this->upload->display_errors());
          $x = $error['error'];
          $y = "<p>You did not select a file to upload.</p>";
          if ($x == $y) {
            $this->MItem->editItem('');

            /* Update by Boby 2011-02-22 */
            $id_ = $this->input->post('id');
            $price = $this->input->post('price');
            $price_ = $this->input->post('price_');
            $pv = $this->input->post('pv');
            $pv_ = $this->input->post('pv_');
            if ($pv != $pv_ || $price != $price_) {
              // $this->MItem->addTracking($id_, $price, $price_, $pv, $pv_);
            }
            /* End update by Boby 2011-02-22 */

            $this->session->set_flashdata('message', 'Update item product successfully');
            redirect('master/item', 'refresh');
          }
          $data['error'] = $error;
          $data['row'] = $row;
          $data['type'] = $this->MItem->getDropDownType();
          $data['category'] = $this->MItem->getDropDownCategory();
          $data['subcategory'] = $this->MItem->getDropDownSubCategory();
          $data['warehouse'] = $this->MItem->getDropDownWarehouse(); // created by Andri PRatama 2018
          $data['page_title'] = 'Edit Item Product';
          $this->load->view('master/item_edit', $data);
        }
      }
    } else {
      $data['row'] = $row;
      $data['type'] = $this->MItem->getDropDownType();
      $data['category'] = $this->MItem->getDropDownCategory();
      $data['subcategory'] = $this->MItem->getDropDownSubCategory();
      $data['warehouse'] = $this->MItem->getDropDownWarehouse(); // created by Andri Pratama 2018
      $data['page_title'] = 'Edit Item Product';
      $this->load->view('master/item_edit', $data);
    }
  }
  public function Uv()
  {
    $valid = $this->input->post('valid_from');
    $to = $this->input->post('valid_to');
    $id = $this->input->post('id');
    $this->db->set('valid_from', $valid);
    $this->db->set('valid_to', $to);
    $this->db->where('id', $id);
    $this->db->update('voucher_user');
    echo "Oke";
  }
}
