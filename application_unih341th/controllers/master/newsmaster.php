<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Newsmaster extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if($this->session->userdata('group_id') < 1 or $this->session->userdata('group_id') >= 100){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MNews'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        $config['base_url'] = site_url().'master/newsmaster/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MNews->countNews($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MNews->searchNews($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'News Master';
        $this->load->view('master/newsmaster_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('title','title','required|min_length[3]');
        $this->form_validation->set_rules('shortdesc','short description','required');
        $this->form_validation->set_rules('longdesc','long description','required');
        $this->form_validation->set_rules('status','','');
        $this->form_validation->set_rules('promo','','');
        $this->form_validation->set_rules('newsbox','','');
            
        if($this->form_validation->run()){
            $config['upload_path'] = './userfiles/news/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']    = '1024'; //1 MB
            //$config['max_width']  = '1600';
            //$config['max_height']  = '1200';
            $config['remove_spaces']    = 'true';
            $config['overwrite']        = 'true'; 
            
            $this->load->library('upload', $config);
            
            if($this->upload->do_upload()){
                $tp=$this->upload->data(); //ambil atribut data yang baru saja diupload
                
                $this->load->library('myresizer'); //load library
                $image= "userfiles/news/".$tp['file_name']; //jangan lupa diset permissionnya
                
                $newimage= "userfiles/news/".$tp['file_name']; //jangan lupa diset permissionnya
                $newwidth=185; //width yang diinginkan
                $newheight=140; //height yang diinginkan
                $params = array(
                        'file' => $image,
                        'newfile'=> $newimage,
                        'width' => $tp['image_width'],
                        'height' => $tp['image_height'],
                        'newwidth' => $newwidth,
                        'newheight' => $newheight
                );
                $this->myresizer->resizecrop($params);
                
                $filename = $tp['file_name'];
                
                $this->MNews->addNews($filename);
                $this->session->set_flashdata('message','News created');
                redirect('master/newsmaster/','refresh');
            }else{
                $error = array('error' => $this->upload->display_errors());
                $x = $error['error'];
                $y = "<p>You did not select a file to upload.</p>";
                if($x == $y){
                    $this->MNews->addNews('');
                    $this->session->set_flashdata('message','News created');
                    redirect('master/newsmaster/','refresh');
                }
                
                $data['page_title'] = 'Create News Master';
                $this->load->view('master/newsmaster_form',$data);
            } 
        }else{
            $data['page_title'] = 'Create News Master';
            $this->load->view('master/newsmaster_form',$data);
        }
            
    }
        
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $data['row'] = $this->MNews->getNews($id);
            
        if(!count($data['row'])){
            redirect('master/newsmaster/','refresh');
        }
        
        $data['page_title'] = 'News Master';
        $this->load->view('master/newsmaster_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('title','title','required|min_length[3]');
        $this->form_validation->set_rules('shortdesc','short description','required');
        $this->form_validation->set_rules('longdesc','long description','required');
        $this->form_validation->set_rules('status','','');
        $this->form_validation->set_rules('promo','','');
        $this->form_validation->set_rules('newsbox','','');
        
        if($this->form_validation->run()){
            $config['upload_path'] = './userfiles/news/';
            $config['allowed_types'] = 'gif|jpeg|jpg|png';
            $config['max_size']    = '1024'; //1 MB
            //$config['max_width']  = '1600';
            //$config['max_height']  = '1200';
            $config['remove_spaces']    = 'true';
            $config['overwrite']        = 'true'; 
                
            $this->load->library('upload', $config);
            
            if($this->upload->do_upload()){                                
                $tp=$this->upload->data(); //ambil atribut data yang baru saja diupload
                
                if($this->input->post('namafile') != $tp['file_name'] and $this->input->post('namafile')){
                    $place = $_SERVER['DOCUMENT_ROOT'].'userfiles/news/'.$this->input->post('namafile');
                    if(file_exists($place)){
                        unlink($place);
                    }
                }
                
                $this->load->library('myresizer'); //load library
                $image= "userfiles/news/".$tp['file_name']; //jangan lupa diset permissionnya
                
                $newimage= "userfiles/news/".$tp['file_name']; //jangan lupa diset permissionnya
                $newwidth=185; //width yang diinginkan
                $newheight=140; //height yang diinginkan
                $params = array(
                        'file' => $image,
                        'newfile'=> $newimage,
                        'width' => $tp['image_width'],
                        'height' => $tp['image_height'],
                        'newwidth' => $newwidth,
                        'newheight' => $newheight
                );
                $this->myresizer->resizecrop($params);
                
                $filename = $tp['file_name'];
                
                $this->MNews->updateNews($filename);
                $this->session->set_flashdata('message','News updated');
                redirect('master/newsmaster/','refresh');
            }else{
                if($_POST)$id=$this->input->post('id');
                $data['row'] = $this->MNews->getNews($id);
                
                if(!count($data['row'])){
                    redirect('master/newsmaster/','refresh');
                }
            
                $error = array('error' => $this->upload->display_errors());
                $x = $error['error'];
                $y = "<p>You did not select a file to upload.</p>";
                if($x == $y){
                    $this->MNews->updateNews('');
                    $this->session->set_flashdata('message','News updated');
                    redirect('master/newsmaster/','refresh');
                }
                $data['error'] = $error;
                $data['page_title'] = 'Edit News';
                $this->load->view('master/newsmaster_edit',$data);
            }
        }else{
            if($_POST)$id=$this->input->post('id');
            $data['row'] = $this->MNews->getNews($id);
            
            if(!count($data['row'])){
                redirect('master/newsmaster/','refresh');
            }
            
            $data['page_title'] = 'Edit News';
            $this->load->view('master/newsmaster_edit',$data);
        }
            
    }
}
?>