<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Item extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    if (!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100) {
      redirect('');
    }

    $this->load->model(array('MMenu', 'MItem'));
  }

  public function index()
  {
    if ($this->MMenu->access($this->session->userdata('group_id'), $this->uri->segment(2), 'view')) {
      redirect('error', 'refresh');
    }

    $this->load->library(array('form_validation', 'pagination'));

    $this->form_validation->set_rules('search', '', '');

    $config['base_url'] = site_url() . 'master/item/index/';
    $config['per_page'] = 20;
    $config['uri_segment'] = 4;
    $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging

    if ($this->form_validation->run()) {
      $this->session->set_userdata('keywords', $this->db->escape_str($this->input->post('search')));
      $keywords = $this->session->userdata('keywords');
      $config['total_rows'] = $this->MItem->countItem($keywords);
      $this->pagination->initialize($config);
      $data['results'] = $this->MItem->searchItem($keywords, $config['per_page'], $this->uri->segment($config['uri_segment']));
    } else {
      if (!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');

      $keywords = $this->session->userdata('keywords');
      $config['total_rows'] = $this->MItem->countItem($keywords);
      $this->pagination->initialize($config);

      $data['results'] = $this->MItem->searchItem($keywords, $config['per_page'], $this->uri->segment($config['uri_segment']));
    }

    $data['page_title'] = 'Item Product';
    $this->load->view('master/item_index', $data);
  }

  public function create()
  {
    if ($this->MMenu->access($this->session->userdata('group_id'), $this->uri->segment(2), 'save')) {
      redirect('error', 'refresh');
    }

    $this->load->library(array('form_validation', 'messages'));

    $this->form_validation->set_rules('id', 'Product ID', 'required|callback__check_id');
    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('headline', '', '');
    $this->form_validation->set_rules('longdesc', '', '');
    $this->form_validation->set_rules('type', '', '');
    $this->form_validation->set_rules('price', '', '');
    $this->form_validation->set_rules('price2', '', ''); // updated by Boby 2014-04-04
    $this->form_validation->set_rules('topup', '', ''); // updated by Boby 2014-04-04
    $this->form_validation->set_rules('pv', '', '');
    $this->form_validation->set_rules('bv', '', '');
    $this->form_validation->set_rules('pricecust', '', '');
    $this->form_validation->set_rules('minbuy', '', ''); //updated by Andri Pratama 2018
    $this->form_validation->set_rules('categoryid', '', '');
    $this->form_validation->set_rules('warehouse_id', '' . ''); //updated by Andri Pratama 2018
    $this->form_validation->set_rules('subcategoryid', '', '');
    $this->form_validation->set_rules('new', '', '');
    $this->form_validation->set_rules('promo', '', '');

    if ($this->form_validation->run()) {
      if (!$this->MMenu->blocked()) {
        $config['upload_path'] = './userfiles/product/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']    = '10240'; //10 MB
        $config['max_width']  = '1600';
        $config['max_height']  = '1200';

        $this->load->library('upload', $config);
        if ($this->upload->do_upload()) {
          $tp = $this->upload->data(); //ambil atribut data yang baru saja diupload

          $this->load->library('myresizer'); //load library
          $image = "userfiles/product/" . $tp['file_name']; //jangan lupa diset permissionnya

          $newimage = "userfiles/product/t_" . $tp['file_name']; //jangan lupa diset permissionnya
          $newwidth = 214; //width yang diinginkan
          $newheight = 151; //height yang diinginkan
          $params = array(
            'file' => $image,
            'newfile' => $newimage,
            'width' => $tp['image_width'],
            'height' => $tp['image_height'],
            'newwidth' => $newwidth,
            'newheight' => $newheight
          );
          $this->myresizer->resizecrop($params);

          $newimage = "userfiles/product/" . $tp['file_name']; //jangan lupa diset permissionnya
          $newwidth = 410; //width yang diinginkan
          $newheight = 410; //height yang diinginkan

          $params = array(
            'file' => $image,
            'newfile' => $newimage,
            'width' => $tp['image_width'],
            'height' => $tp['image_height'],
            'newwidth' => $newwidth,
            'newheight' => $newheight
          );
          $this->myresizer->resizecrop($params);

          $filename = $tp['file_name'];
          $this->MItem->addItem($filename);
          $this->session->set_flashdata('message', 'Create item product successfully');
          redirect('master/item', 'refresh');
        } else {
          $this->MItem->addItem("");
          /*
          $data['error'] = array('error' => $this->upload->display_errors());
          $data['type'] = $this->MItem->getDropDownType();
          $data['page_title'] = 'Create Item Product';
          $this->load->view('master/item_form',$data);
          */
        }
      }
      redirect('master/item', 'refresh');
    } else {
      $data['type'] = $this->MItem->getDropDownType();
      $data['category'] = $this->MItem->getDropDownCategory();
      $data['subcategory'] = $this->MItem->getDropDownSubCategory();
      $data['warehouse'] = $this->MItem->getDropdownWarehouse(); // created by Andri Pratama 2018
      $data['page_title'] = 'Create Item Product';
      $this->load->view('master/item_form', $data);
    }
  }

  public function _check_id()
  {
    if ($this->MItem->check_productid($this->input->post('id'))) {
      $this->form_validation->set_message('_check_id', 'product id available !');
      return false;
    }
    return true;
  }

  public function view($id = 0)
  {
    if ($this->MMenu->access($this->session->userdata('group_id'), $this->uri->segment(2), 'view')) {
      redirect('error', 'refresh');
    }
    $row = array();
    $row = $this->MItem->getItem($id);

    if (!count($row)) {
      redirect('master/item', 'refresh');
    }
    $data['row'] = $row;
    $data['page_title'] = 'View Item Product';
    $this->load->view('master/item_view', $data);
  }

  public function edit($id = 0)
  {
    if ($this->MMenu->access($this->session->userdata('group_id'), $this->uri->segment(2), 'edit')) {
      redirect('error', 'refresh');
    }

    $this->load->library(array('form_validation', 'messages'));

    $this->form_validation->set_rules('id', 'Product ID', 'required');
    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('headline', '', '');
    $this->form_validation->set_rules('type', '', '');
    $this->form_validation->set_rules('display', '', '');
    $this->form_validation->set_rules('manufaktur', '', '');
    $this->form_validation->set_rules('sales', '', '');
    $this->form_validation->set_rules('longdesc', '', '');
    $this->form_validation->set_rules('price', '', '');
    $this->form_validation->set_rules('price2', '', ''); // created by Boby 2014-04-04
    $this->form_validation->set_rules('pv', '', '');
    $this->form_validation->set_rules('minbuy', '', ''); //created by Andri pratama 2018
    $this->form_validation->set_rules('categoryid', '', '');
    $this->form_validation->set_rules('warehouse_id', '', ''); //created by Andri pratama 2018
    $this->form_validation->set_rules('subcategoryid', '', '');
    $this->form_validation->set_rules('new', '', '');
    $this->form_validation->set_rules('promo', '', '');

    /* Update by Boby 2011-02-22 */
    $this->form_validation->set_rules('price_', '', '');
    $this->form_validation->set_rules('pv_', '', '');
    /* End update by Boby 2011-02-22 */

    $this->form_validation->set_rules('pricecust', '', '');

    $row = array();
    $row = $this->MItem->getItem($id);

    if (!count($row)) {
      redirect('master/item', 'refresh');
    }

    if ($this->form_validation->run()) {
      if (!$this->MMenu->blocked()) {
        $config['upload_path'] = './userfiles/product/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']    = '10240'; //10 MB
        $config['max_width']  = '1600';
        $config['max_height']  = '1200';

        $this->load->library('upload', $config);

        if ($this->upload->do_upload()) {
          $tp = $this->upload->data(); //ambil atribut data yang baru saja diupload

          if ($this->input->post('namafile') != $tp['file_name'] and $this->input->post('namafile')) {
            $place = $_SERVER['DOCUMENT_ROOT'] . "userfiles/product/" . $this->input->post('namafile');
            if (file_exists($place)) {
              unlink($place);
            }

            $place = $_SERVER['DOCUMENT_ROOT'] . "userfiles/product/t_" . $this->input->post('namafile');
            if (file_exists($place)) {
              unlink($place);
            }
          }
          $this->load->library('myresizer'); //load library
          $image = "userfiles/product/" . $tp['file_name']; //jangan lupa diset permissionnya

          //if($this->input->post('cover') == '1'){
          $newimage2 = "userfiles/product/t_" . $tp['file_name']; //jangan lupa diset permissionnya
          $newwidth = 214; //width yang diinginkan
          $newheight = 141; //height yang diinginkan
          $params = array(
            'file' => $image,
            'newfile' => $newimage2,
            'width' => $tp['image_width'],
            'height' => $tp['image_height'],
            'newwidth' => $newwidth,
            'newheight' => $newheight
          );
          $this->myresizer->resizecrop($params);
          //}

          $newimage4 = "userfiles/product/" . $tp['file_name']; //jangan lupa diset permissionnya
          $newwidth = 410; //width yang diinginkan
          $newheight = 410; //height yang diinginkan

          $params = array(
            'file' => $image,
            'newfile' => $newimage4,
            'width' => $tp['image_width'],
            'height' => $tp['image_height'],
            'newwidth' => $newwidth,
            'newheight' => $newheight
          );
          $this->myresizer->resizecrop($params);

          $filename = $tp['file_name'];
          $this->MItem->editItem($filename);
          $this->session->set_flashdata('message', 'Update item product successfully');
          redirect('master/item', 'refresh');
        } else {
          $error = array('error' => $this->upload->display_errors());
          $x = $error['error'];
          $y = "<p>You did not select a file to upload.</p>";
          if ($x == $y) {
            $this->MItem->editItem('');

            /* Update by Boby 2011-02-22 */
            $id_ = $this->input->post('id');
            $price = $this->input->post('price');
            $price_ = $this->input->post('price_');
            $pv = $this->input->post('pv');
            $pv_ = $this->input->post('pv_');
            if ($pv != $pv_ || $price != $price_) {
              // $this->MItem->addTracking($id_, $price, $price_, $pv, $pv_);
            }
            /* End update by Boby 2011-02-22 */

            $this->session->set_flashdata('message', 'Update item product successfully');
            redirect('master/item', 'refresh');
          }
          $data['error'] = $error;
          $data['row'] = $row;
          $data['type'] = $this->MItem->getDropDownType();
          $data['category'] = $this->MItem->getDropDownCategory();
          $data['subcategory'] = $this->MItem->getDropDownSubCategory();
          $data['warehouse'] = $this->MItem->getDropDownWarehouse(); // created by Andri PRatama 2018
          $data['page_title'] = 'Edit Item Product';
          $this->load->view('master/item_edit', $data);
        }
      }
    } else {
      $data['row'] = $row;
      $data['type'] = $this->MItem->getDropDownType();
      $data['category'] = $this->MItem->getDropDownCategory();
      $data['subcategory'] = $this->MItem->getDropDownSubCategory();
      $data['warehouse'] = $this->MItem->getDropDownWarehouse(); // created by Andri Pratama 2018
      $data['page_title'] = 'Edit Item Product';
      $this->load->view('master/item_edit', $data);
    }
  }
}
