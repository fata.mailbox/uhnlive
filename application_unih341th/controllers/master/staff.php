<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Staff extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','Staff_model'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'master/staff/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->Staff_model->count($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->Staff_model->search($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->Staff_model->count($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->Staff_model->search($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Staff';
        $this->load->view('master/staff_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('nik','nik','required|callback__check_id');
	$this->form_validation->set_rules('name','name','required');
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->Staff_model->add();
                $this->session->set_flashdata('message','Create staff successfully');
            }
            redirect('master/staff','refresh');
        }
        
        $data['page_title'] = 'Create Staff';
        $this->load->view('master/staff_form',$data);
    }
    public function _check_id(){
        if($this->Staff_model->check_nik($this->input->post('nik'))){
            $this->form_validation->set_message('_check_id','NIK available !');
            return false;
        }
        return true;
    } 
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->Staff_model->get($id);
        
        if(!count($row)){
            redirect('master/staff','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Staff';
        $this->load->view('master/staff_view',$data);
    }
    
    public function edit($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'edit')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('name','Provinci','required');
        $row =array();
        $row = $this->Staff_model->get($id);
        
        if(!count($row)){
            redirect('master/staff','refresh');
        }
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->Staff_model->edit();
                $this->session->set_flashdata('message','Update staff successfully');
            }
            redirect('master/staff','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'Edit Staff';
        $this->load->view('master/staff_edit',$data);
    }
    
}
?>