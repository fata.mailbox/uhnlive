<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Staffsearch extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MSearch'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','Keywords','min_length[3]');
        
        $config['base_url'] = site_url().'staffsearch/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(2))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(3); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->count_search_staff($keywords);
        $config['per_page'] = 20;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->search_staff($keywords,$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Staff Search';
        
        $this->load->view('search/staff_search',$data);
    }
    
}
?>