<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Memsearch extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MSearch'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','Keywords','min_length[3]');
        
        $config['base_url'] = site_url().'memsearch/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(2))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(3); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countMember($keywords);
        $config['per_page'] = 20;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchMember($keywords,$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Member Search';
        
        $this->load->view('search/member_search',$data);
    }
    
    public function ewallet(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','Keywords','min_length[3]');
        
        $config['base_url'] = site_url().'memsearch/ewallet/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countMember($keywords);
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchMember($keywords,$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Member Search';
        
        $this->load->view('search/member_ewallet_search',$data);
    }
	
	/* Created by Boby 20140120 */
	public function ewalletso(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','Keywords','min_length[3]');
        
        $config['base_url'] = site_url().'memsearch/ewalletso/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countMember($keywords);
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchMemberSo($keywords,$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Member Search';
        
        $this->load->view('search/member_ewallet_search_so',$data);
    }
	
	public function ewalletsostc(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','Keywords','min_length[3]');
        
        $config['base_url'] = site_url().'memsearch/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(2))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(3); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countMember($keywords);
        $config['per_page'] = 20;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchMember($keywords,$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Member Search';
        
        $this->load->view('search/member_search_sostc',$data);
    }
	/* End created by Boby 20140120 */

}
?>