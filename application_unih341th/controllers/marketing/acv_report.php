<?php // UPDATE `menu` SET `url`='sls_inc' WHERE `id`='92';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Acv_report extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('','refresh');
        }
        $this->load->model(array('Mmarketing','MMenu','Msales'));
    }

    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('tahun','','');
		$this->form_validation->set_rules('quart','','');
        
        if($this->form_validation->run()){
			$data['period_']="Quart";
			if($this->input->post('quart')=="00-00"){
				$data['results']=$this->Mmarketing->acv_reports($this->input->post('tahun'),0);
				$data['results_total']=$this->Msales->acv_reports_total($this->input->post('tahun'),0);
			}else{
				/*
				$period = $this->input->post('tahun');
				$period.= '-';
				$period.= $this->input->post('quart');
				//echo $period;
				$data['results']=$this->Mmarketing->acv_reports($this->input->post('tahun'),$period);
				//$data['results_total']=$this->Mmarketing->sales_so_cluster_rpt_total($this->input->post('tahun'),$period);
				//$data['results']=$this->Mmarketing->report($this->input->post('tahun'),$this->input->post('quart'));
				$data['period_']="Month";
				*/
				$thn = $this->input->post('tahun');
				$data['b'] = substr($this->input->post('quart'), 0,2);

			}
            $data['thn']=$this->input->post('tahun');
			$data['total']=0;
        }else{
            $data['results']=false;
			$data['thn']=date("Y");
            $data['total']=false;
            $data['b']=false;
        }
        $data['dropdownyear'] = $this->Mmarketing->get_year_report();
		$data['dropdownq'] = $this->Mmarketing->get_q(1);
        $data['page_title'] = 'Product Sales Achivement Report';
        //$data['testtgl'] = array("2019-07-31","2019-08-31","2019-09-30");
        //$data['report'] = $this->Mmarketing->acv_reports_2();
		if($this->input->post('submit')=="export")
		$this->load->view('marketing/prod_sales_acv_report_export',$data);
		else
		$this->load->view('marketing/prod_sales_acv_report',$data); 
		
    }

    
    // By Annisa 23082019
}
?>