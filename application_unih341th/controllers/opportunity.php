<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Opportunity extends CI_Controller {
    function __construct()
    {
	parent::__construct();
		$this->load->model('MFrontend');
	}
	
	public function index($id=0){
		$data['banner'] = $this->MFrontend->list_banner(50,0);
		
		$data['content'] = 'index/opportunity_why';
		$this->load->view('index/index',$data);
	}
	public function started($id=0){
		$data['banner'] = $this->MFrontend->list_banner(50,0);
		
		$data['content'] = 'index/opportunity_started';
		$this->load->view('index/index',$data);
	}
	/* Create by Andrew 201304 */
	public function program_rekrut(){
		$data['banner'] = $this->MFrontend->list_banner(50,0);
		
		$data['content'] = 'index/program_rekrut';
		$this->load->view('index/index',$data);
	}
	public function conference(){
		$data['banner'] = $this->MFrontend->list_banner(50,0);
		
		$data['content'] = 'index/conference';
		$this->load->view('index/index',$data);
	}
	/* End create by Andrew 201304 */
}
