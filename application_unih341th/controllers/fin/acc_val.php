<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Acc_val extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MAuth','MSearchadmin', 'Maccountvalidation'));
    }
    
	public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','pagination'));
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
			$this->session->set_userdata('type_',$this->db->escape_str($this->input->post('type')));
        }else{
            if(!$this->uri->segment(4)){ 
				$this->session->unset_userdata('keywords');
				$this->session->unset_userdata('type');
			}
        }
        
		$config['base_url'] = site_url().'fin/acc_val/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
		$data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
		
		$keywords = $this->session->userdata('keywords');
		$type = $this->session->userdata('type_');
        $config['total_rows'] = $this->Maccountvalidation->countAccVal($keywords,$type);
		$this->pagination->initialize($config);
		$data['base_url'] = $config['base_url'];
		$data['results'] = $this->Maccountvalidation->getAccVal($keywords, $config['per_page'], $this->uri->segment($config['uri_segment']),$type);
		$data['type'] = $this->Maccountvalidation->getSearchType();
		
        $data['page_title'] = 'Transfer Cost of BCA Account Validation';
        $this->load->view('finance/v_acc_val',$data);
    }
    
	public function update($id, $ch){
		// $id = $this->uri->segment(4);
		// $ch = $this->uri->segment(5);
		$this->Maccountvalidation->updateAccVal($id, $ch);
		$this->session->set_flashdata('message','Update complete..');
		redirect('fin/acc_val/','refresh');
	}
	
    public function create(){
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('norek','Account Number','trim|required|numeric|min_length[10]|callback__check_acc');
        $this->form_validation->set_rules('nama','','trim|required|min_length[4]');
		$this->form_validation->set_rules('type','','');
				
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->Maccountvalidation->createAccount();
                $this->session->set_flashdata('message','Complete..');
            }
            redirect('fin/acc_val/','refresh');
        }
        $data['type'] = $this->Maccountvalidation->getValType();
        $data['page_title'] = 'Create Relocation Ewallet';
        $this->load->view('finance/v_acc_val_create',$data);
    }
	
	public function _check_acc(){
		if(strlen($this->input->post('norek'))>10){
			$this->form_validation->set_message('_check_acc','BCA Account Number is 10 digits');
			return false;
		}else{
			if($this->Maccountvalidation->getExistAcc($this->input->post('norek'))){
				$this->form_validation->set_message('_check_acc','Data already exists');
				return false;
			}else{
				// $this->form_validation->set_message('_check_acc','Cool');
				return true;
			}
		}
    }
}
?>