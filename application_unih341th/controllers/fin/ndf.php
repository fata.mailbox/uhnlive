<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ndf extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        $this->load->model(array('MMenu','MRekap'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        $this->form_validation->set_rules('whsid','','');
                
        if($this->form_validation->run()){
            $data['dps'] = $this->MRekap->sum_deposit($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'));
            if($this->input->post('whsid') == 'all')$data['wdr'] = $this->MRekap->sum_withdrawal('',$this->input->post('fromdate'),$this->input->post('todate'));
            else {
                $data['wdr']['famount'] = 0;
                $data['wdr']['amount'] = 0;
            }
            
            $data['total12']= number_format($data['dps']['total_approved'] - $data['wdr']['amount'],0);
            
            $data['directso'] = $this->MRekap->sum_SO($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'),'so','order');
            $data['directsoentry'] = $this->MRekap->sum_SO($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'),'so','entry');
            
            $data['stcso'] = $this->MRekap->sum_SO($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'),'stc','order');
            $data['stcsoentry'] = $this->MRekap->sum_SO($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'),'stc','entry');
            
            $data['totalso'] = $this->MRekap->sum_SO($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'),'total','order');
            $data['totalsoentry'] = $this->MRekap->sum_SO($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'),'total','entry');
            
            $data['kit'] = $this->MRekap->sum_SOKIT($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'),'so');
            $data['kitstc'] = $this->MRekap->sum_SOKIT($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'),'stc');
            
            $data['ro'] = $this->MRekap->sum_RO($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'),'company');
            $data['rostc'] = $this->MRekap->sum_RO($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'),'stc');
            
            $data['pinjaman'] = $this->MRekap->sum_Pinjaman($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'));
            $data['pelunasan'] = $this->MRekap->sum_Pelunasan($this->input->post('fromdate'),$this->input->post('todate'));
            
            $data['saldotitipan'] = $this->MRekap->sum_saldotitipan();
            $data['saldopinjaman'] = $this->MRekap->sum_saldopinjaman('');
            
            $data['ewalletmember'] =$this->MRekap->sum_saldoewallet('member');
            $data['ewalletstc'] =$this->MRekap->sum_saldoewallet('stockiest');;

        }else{
            $data['dps'] = false;
            $data['wdr'] = false;
            $data['directso'] = false;
            $data['directsoentry'] =false;
            $data['stcso'] = false;
            $data['stcsoentry'] =false;
            $data['totalso'] = false;
            $data['totalsoentry'] = false;
            $data['ro'] = false;
            $data['rostc'] = false;
            $data['kit'] = false;
            $data['kitstc'] =false;
            $data['total12']='0';
            $data['pinjaman'] =false;
            $data['pelunasan'] =false;
            $data['saldopinjaman'] =false;
            $data['saldotitipan'] =false;
            $data['ewalletmember'] =false;
            $data['ewalletstc'] =false;
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['warehouse'] = $this->MRekap->getDropDownWhs();
        $data['page_title'] = 'Daily Balancesheet';
        $this->load->view('finance/neracadailyfund_index',$data);
    }
    
}
?>