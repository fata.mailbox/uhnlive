<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Autowdw extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        $this->load->model(array('MMenu','MWithdrawal'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
		$this->load->library(array('form_validation'));
        $data['results'] = $this->MWithdrawal->listAutoWithdrawal(0);
        
		$cd = strtotime(date("Y-m-d"));
		$bln = date('F Y', mktime(0,0,0,date('m',$cd)-1,date('d',$cd),date('Y',$cd)));
		
		$data['temp']=10;
		if($data['temp']==1){
			$data['page_title'] = 'List Transfer E-wallet Periode '.$bln;
		}else{
			$data['page_title'] = 'Automatic Withdrawal';
			$data['bln'] = $bln;
		}
        $this->load->view('finance/auto_withdrawal_index',$data);
    }
	//incident
	public function dummy(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
		$this->load->library(array('form_validation'));
        //$data['results'] = $this->MWithdrawal->listAutoWithdrawal(0);
        $data['results'] = $this->MWithdrawal->listAutoWithdrawalDummy(0);
        
		$cd = strtotime(date("Y-m-d"));
		$bln = date('F Y', mktime(0,0,0,date('m',$cd)-1,date('d',$cd),date('Y',$cd)));
		
		$data['temp']=10;
		if($data['temp']==1){
			$data['page_title'] = 'List Transfer E-wallet Periode '.$bln;
		}else{
			$data['page_title'] = 'Automatic Withdrawal';
			$data['bln'] = $bln;
		}
        $this->load->view('finance/auto_withdrawal_dummy_index',$data);
    }
	
	//incident
	public function cetak(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
		//$this->load->helper(�bhowbee_csv_helper�);
		$this->load->library(array('form_validation'));
        $data['results'] = $this->MWithdrawal->listAutoWithdrawal(1);
		
        $cd = strtotime(date("Y-m-d"));
		$bln = date('F Y', mktime(0,0,0,date('m',$cd)-1,date('d',$cd),date('Y',$cd)));
		
		$data['temp']=1;
		if($data['temp']==1){
			$data['page_title'] = 'List Transfer E-wallet Periode '.$bln;
		}else{
			$data['page_title'] = 'Automatic Withdrawal';
			$data['bln'] = $bln;
		}
        $this->load->view('finance/auto_withdrawal_index',$data);
    }
	
    public function cetak2(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
		$this->load->library(array('form_validation'));
        $data['results'] = $this->MWithdrawal->listAutoWithdrawal(1);
		
        $cd = strtotime(date("Y-m-d"));
		$bln = date('F Y', mktime(0,0,0,date('m',$cd)-1,date('d',$cd),date('Y',$cd)));
		
		$data['temp']=1;
		$data['page_title'] = 'List Transfer E-wallet Periode '.$bln;
        $this->load->view('finance/auto_withdrawal_csv',$data);
    }
    
    public function approved(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        if(!$this->MMenu->blocked()){
            $this->MWithdrawal->addAutoWdw();
			$this->session->set_flashdata('message','Automatic withdrawal success...');
        }
        redirect('fin/autowdw','refresh');
		
    }  
}
?>