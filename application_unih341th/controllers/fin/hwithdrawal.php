<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Hwithdrawal extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        $this->load->model(array('MMenu','MRekap','MWithdrawal'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        $this->form_validation->set_rules('type_withdrawal_id','','');
                
        if($this->form_validation->run()){
            $data['results'] = $this->MRekap->list_withdrawal($this->input->post('type_withdrawal_id'),$this->input->post('fromdate'),$this->input->post('todate'));
            $data['total'] = $this->MRekap->sum_withdrawal($this->input->post('type_withdrawal_id'),$this->input->post('fromdate'),$this->input->post('todate'));
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        //$data['type_wdr']=$this->MWithdrawal->dropdown_type_wdr();
        
        $data['page_title'] = 'Summary Withdrawal';
        $this->load->view('finance/rekapwithdrawal_index',$data);
    }
    public function autotransfer(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        if(!$this->MMenu->blocked()){
            $this->MWithdrawal->autoWithdrawal();
            $this->session->set_flashdata('message','Auto withdrawal ewallet successfully');
        }
        redirect('fin/hwithdrawal','refresh');
    }
}
?>