<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dps extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MDeposit'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'fin/dps/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MDeposit->countDeposit($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MDeposit->searchDeposit($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MDeposit->countDeposit($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MDeposit->searchDeposit($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Deposit Confirmation';
        $this->load->view('finance/deposit_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('date','','');
        $this->form_validation->set_rules('amount','amount transfer','required|callback__check_amount');
        $this->form_validation->set_rules('bank_id','','');
        $this->form_validation->set_rules('remark','','');
        $this->form_validation->set_rules('pin','PIN','trim|required|callback__check_pin');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MDeposit->addDeposit();
                $this->session->set_flashdata('message','Create deposit confirmation successfully');
            }
            redirect('fin/dps','refresh');
        }

        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-green.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['bank'] = $this->MDeposit->getDropDownBank();
        $data['page_title'] = 'Create Deposit Confirmation';
        $this->load->view('finance/deposit_form',$data);
    }
    
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MDeposit->getDeposit($id);
        
        if(!count($row)){
            redirect('fin/dps','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Deposit';
        $this->load->view('finance/deposit_view',$data);
    }
    public function _check_pin(){
        if(!$this->MAuth->check_pin($this->session->userdata('username'),$this->input->post('pin'))){
            $this->form_validation->set_message('_check_pin','Sorry, your pin invalid');
            return false;
        }
        return true;
    }
    public function _check_amount(){
        $amount = str_replace(".","",$this->input->post('amount'));
        if($amount < 1){
            $this->form_validation->set_message('_check_amount','Minimum transfer Rp. 10.000,-');
            return false;
        }
        return true;
    }  
    
}
?>