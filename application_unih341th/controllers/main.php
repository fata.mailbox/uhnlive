<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Main extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MFrontend'));
    }
    
    public function index(){
		if($this->session->userdata('group_id') == 102 || $this->session->userdata('group_id') == 103)
        	$data['row'] = $this->MMenu->get_member2_new($this->session->userdata('userid'));
		else
        	$data['row'] = $this->MMenu->get_member2($this->session->userdata('userid'));
		$data['banner'] = $this->MFrontend->list_banner_backend(50,0);
		
        $this->load->view('main_backend',$data);
    }
    
}
?>