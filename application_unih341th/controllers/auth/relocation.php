<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Relocation extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MAuth','MSearchadmin'));
    }
    
	public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','pagination'));
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4)) $this->session->unset_userdata('keywords');
        }
        
		$config['base_url'] = site_url().'auth/relocation/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
		$data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
		
		$keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MAuth->countRellocation($keywords);
		$this->pagination->initialize($config);
		$data['base_url'] = $config['base_url'];
		$data['results'] = $this->MAuth->getRellocation($keyword, $config['per_page'], $this->uri->segment($config['uri_segment']));
		
        $data['page_title'] = 'Relocation Member';
        $this->load->view('auth/register_relloc_list',$data);
    }
    
	
    public function create($id=0){
        $this->load->library(array('form_validation','messages'));
        
        // updated by Boby 20140905
		$this->form_validation->set_rules('member_id0','Member ID','trim|required|callback__check_age');
		// $this->form_validation->set_rules('member_id0','Member ID','trim|required');
		// end updated by Boby 20140905
		
        $this->form_validation->set_rules('name0','','');
        $this->form_validation->set_rules('member_id1','Upline ID','trim|required');
        $this->form_validation->set_rules('name1','','');
		$this->form_validation->set_rules('member_id2','Sponsor ID','trim|required');
        $this->form_validation->set_rules('name2','','');
				
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MAuth->rellocation();
                $this->session->set_flashdata('message','Register complete..');
            }
            redirect('auth/relocation/','refresh');
        }
        
        $data['page_title'] = 'Relocation Member';
        // Updated by Boby 20140905
		// $this->load->view('auth/register_relloc',$data);
        $this->load->view('auth/register_relloc_mem',$data);
		// End updated by Boby 20140905
    }
	
	//Start 20170622 ASP
    public function create_khusus($id=0){
        $this->load->library(array('form_validation','messages'));
        
        // updated by Boby 20140905
		$this->form_validation->set_rules('member_id0','Member ID','trim|required|callback__check_age');
		// $this->form_validation->set_rules('member_id0','Member ID','trim|required');
		// end updated by Boby 20140905
		
        $this->form_validation->set_rules('name0','','');
        $this->form_validation->set_rules('member_id1','Upline ID','trim|required');
        $this->form_validation->set_rules('name1','','');
		$this->form_validation->set_rules('member_id2','Sponsor ID','trim|required');
        $this->form_validation->set_rules('name2','','');
				
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MAuth->rellocation();
                $this->session->set_flashdata('message','Register complete..');
            }
            redirect('auth/relocation/','refresh');
        }
        
        $data['page_title'] = 'Relocation Member';
        // Updated by Boby 20140905
		// $this->load->view('auth/register_relloc',$data);
        // $this->load->view('auth/register_relloc_mem',$data);
        $this->load->view('auth/register_relloc_mem',$data);
		// End updated by Boby 20140905
    }
	//End 20170622 ASP
	
	/* Created by Boby 20140905 */
	 public function _check_age(){
        if($this->MAuth->getAccountID($this->input->post('member_id0'), $this->input->post('member_id1'))){
            $this->form_validation->set_message('_check_age','Upline are younger than member..');
            return false;
        }
		return true;
    }  
	/* End created by Boby 20140905 */
}
?>