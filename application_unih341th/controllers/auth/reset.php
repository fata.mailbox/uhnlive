<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reset extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MReset'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        
        $config['base_url'] = site_url().'auth/reset/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MReset->countResetMember($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MReset->searchResetMember($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Reset Password';
        $this->load->view('auth/resetm_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('idkategori','','required');
        $this->form_validation->set_rules('member_id','Member ID','required');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('remark','','');
        $this->form_validation->set_rules('passwd','password','|min_length[6]|matches[passwd2]');
        $this->form_validation->set_rules('passwd2','','');
        $this->form_validation->set_rules('pin','pin','matches[pin2]');
        $this->form_validation->set_rules('pin2','','');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MReset->addResetMember();
                $this->session->set_flashdata('message','Create reset password successfully');
            }
            redirect('auth/reset','refresh');
        }

        $data['page_title'] = 'Create Reset Password';
        $this->load->view('auth/resetm_form',$data);
    }
    public function _check_reset(){
        if(!$this->input->post('passwd') && !$this->input->post('pin')){
            $this->form_validation->set_message('_check_reset','Reset Password or PIN ?');
            return false;
        }
        return true;
    }
    
}
?>