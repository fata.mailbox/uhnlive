<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cpstc extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if($this->session->userdata('group_id') < 100){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MAuth'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('oldpassword','Old Password','trim|required|callback__check_oldpassword');
        $this->form_validation->set_rules('newpassword','New Password','trim|required|min_length[8]|matches[newpassword2]');
        $this->form_validation->set_rules('pin','PIN','trim|required|callback__check_pin');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MAuth->change_password_member();
                $this->session->set_flashdata('message','Change password successfully');
            }
            redirect('auth/cpstc/','refresh');
        }
        $data['page_title'] = 'Change Password';
        $this->load->view('auth/changepassword_stc',$data);
    }
    
    public function pin(){
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('pin','Old PIN','trim|required|callback__check_pin');
        $this->form_validation->set_rules('newpin','New PIN','trim|required|numeric|min_length[6]|matches[newpin2]');
        $this->form_validation->set_rules('newpin2','Retype New PIN','');
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                //$this->MUsers->change_pin();
                $this->session->set_flashdata('message','Change pin successfully');
            }
            redirect('auth/cpstc/pin/','refresh');
        }
        $data['page_title'] = 'Change PIN';
        $this->load->view('auth/change_pin_stc',$data);
    }
    
    public function _check_oldpassword(){
        if(!$this->MAuth->check_passwordmember($this->session->userdata('username'),$this->input->post('oldpassword'))){
            $this->form_validation->set_message('_check_oldpassword','Sorry, your old password invalid');
            return false;
        }
        return true;
    }
    
    public function _check_pin(){
        if(!$this->MAuth->check_pin($this->session->userdata('username'),$this->input->post('pin'))){
            $this->form_validation->set_message('_check_pin','Sorry, your pin invalid');
            return false;
        }
        return true;
    }
    
}
?>