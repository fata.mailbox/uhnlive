<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Regnik extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MAuth','MSearchadmin'));
    }
    
	public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','pagination'));
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        
		$keywords = $this->session->userdata('keywords');
		
		$config['base_url'] = site_url().'auth/regnik/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->MSearchadmin->countEmp($keywords);
		$this->pagination->initialize($config);
		
		$data['base_url'] = $config['base_url'];
		$data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $data['results'] = $this->MSearchadmin->searchEmp($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
		
        $data['page_title'] = 'Sohogroup Employee';
        $this->load->view('auth/register_nik_list',$data);
    }
    
	
    public function create($id=0){
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('emp_id','Employee ID','trim|min_length[11]|required');
        $this->form_validation->set_rules('emp_name','Employee Name','trim|min_length[3]|required');
        $this->form_validation->set_rules('member_id','Member ID','trim');
        $this->form_validation->set_rules('name','','');
        
		$data['result']=$this->MSearchadmin->getEmp($id);
		
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MSearchadmin->saveEmployee(
					$this->input->post('emp_id')
					, $this->input->post('emp_name')
					, $this->input->post('member_id')
					, $this->session->userdata('user')
				);
                //$this->session->set_flashdata('message','Register complete..');
            }
            redirect('auth/Regnik/','refresh');
        }
        
        $data['page_title'] = 'Register NIK';
        $this->load->view('auth/register_nik',$data);
    }
}
?>