<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cancelro extends CI_Controller {
  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in')){
      redirect('','refresh');
    }
    $this->load->model(array('MMenu','MAuth','MSearchadmin','MCancelRO'));
  }

  public function index(){
    if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
      redirect('error','refresh');
    }
    $this->load->library(array('form_validation','pagination'));
    $this->form_validation->set_rules('search','','');

    if($this->form_validation->run()){
      $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
    }else{
      if(!$this->uri->segment(4)) $this->session->unset_userdata('keywords');
    }

    $config['base_url'] = site_url().'auth/cancelro/index/';
    $config['per_page'] = 20;
    $config['uri_segment'] = 4;
    $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging

    $keywords = $this->session->userdata('keywords');
    $config['total_rows'] = $this->MCancelRO->countCancelRO($keywords);
    $this->pagination->initialize($config);
    $data['base_url'] = $config['base_url'];
    $data['results'] = $this->MCancelRO->getCancelRO($keywords, $config['per_page'], $this->uri->segment($config['uri_segment']));

    $data['page_title'] = 'RO Canceled';
    $this->load->view('auth/cancelro_list',$data);
  }


  public function create($id=0){
    if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
      redirect('error','refresh');
    }
    $this->load->model(array('MAuth'));
    $this->load->model(array('MCancelRO'));
    $this->load->library(array('form_validation','messages'));

    $this->form_validation->set_rules('ro_id','','');
    $this->form_validation->set_rules('remark','','');

    if($this->form_validation->run()){
      $row = $this->MCancelRO->getInfoRo($this->input->post('ro_id'));
      $data['row'] = $row;

      if(!count($row)){
        $this->session->set_flashdata('message','RO.'.$this->input->post('ro_id').' not found');
        redirect('auth/cancelro/create','refresh');
      }else{
        if($row['note']==0){
          $row2 = $this->MCancelRO->getInfoStockStc($this->input->post('ro_id'));
          $data['row2'] = $row2;
          if(!count($row2)){
            $this->session->set_flashdata('message','RO.'.$this->input->post('ro_id').' stock not found');
            redirect('auth/cancelro/create','refresh');
          }else{
            if($row2['note']==1){
              $this->session->set_flashdata('message','RO.'.$this->input->post('ro_id').' stock already used');
              redirect('auth/cancelro/create','refresh');
            }else if($row2['note']==2){
              $this->session->set_flashdata('message','RO.'.$this->input->post('ro_id').' is not from this month');
              redirect('auth/cancelro/create','refresh');
            }else if($row2['note']==0){
              if($_POST['submit'] == 'Submit'){
                if(!$this->MMenu->blocked()){
                  $this->MCancelRO->CancelRO();
                  $this->session->set_flashdata('message','RO has been canceled');
                }
                redirect('auth/cancelro','refresh');
              }
            }
          }
        }else if($row['note']==1){
          $this->session->set_flashdata('message','RO.'.$this->input->post('ro_id').' cannot canceled twice.');
          redirect('auth/cancelro/create','refresh');
        }else if($row['note']==2){
          $this->session->set_flashdata('message','RO.'.$this->input->post('ro_id').' is canceled RO.');
          redirect('auth/cancelro/create','refresh');
        }else if($row['note']==3){
          $this->session->set_flashdata('message','Cannot canceled closed transaction.');
          redirect('auth/cancelro/create','refresh');
        }else if($row['note']==4){
          $this->session->set_flashdata('message','Starter Kit cannot canceled.');
          redirect('auth/cancelro/create','refresh');
        }else{
          redirect('auth/cancelro/create','refresh');
        }
      }
    }

    $data['items'] = false;
    $data['page_title'] = 'Canceling RO'; // $row['note']; // print_r($row); //
    $this->load->view('auth/cancelro_form',$data);
  }
}
?>
