<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rp_mem extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MAuth'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        
        $config['base_url'] = site_url().'auth/rp/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MAuth->countResetPassword($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MAuth->searchResetPassword($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Reset Password';
        $this->load->view('auth/resetpassword_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('member_id','Member ID','required');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('ewallet','Ewallet','callback__check_ewallet');
        $this->form_validation->set_rules('passwd','','');
        $this->form_validation->set_rules('pin','','callback__check_reset');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MAuth->addResetPassword();
                $this->session->set_flashdata('message','Create reset password/ pin successfully');
            }
            redirect('auth/rp','refresh');
        }

        $data['page_title'] = 'Create Reset Password';
        $this->load->view('auth/resetpassword_form',$data);
    }
    public function _check_reset(){
        if(!$this->input->post('passwd') && !$this->input->post('pin')){
            $this->form_validation->set_message('_check_reset','Reset Password or PIN ?');
            return false;
        }
        return true;
    }
    public function _check_ewallet(){
        $memberid = $this->input->post('member_id');
        if($memberid){
            $row = $this->MAuth->getEwallet($memberid);
            if(25000 > $row->ewallet){
                $this->form_validation->set_message('_check_ewallet','Ewallet anda tidak mencukupi');
                return false;
            }
        }
        return true;
    }
    public function view($id=0){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row =array();
        $row = $this->MAuth->getResetPassword($id);
        
        if(!count($row)){
            redirect('auth/rp','refresh');
        }
        $data['row'] = $row;
        $data['page_title'] = 'View Reset Password';
        $this->load->view('auth/resetpassword_view',$data);
    }
    
}
?>