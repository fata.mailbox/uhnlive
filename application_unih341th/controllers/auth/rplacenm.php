<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rplacenm extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MAuth'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('member_id','Member ID','trim|required');
		$this->form_validation->set_rules('name','Name','trim|required');
        $this->form_validation->set_rules('name1','Replacement Name','trim|required');
        
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MAuth->change_name();
                $this->session->set_flashdata('message','Change name successfully');
            }
            redirect('auth/rplacenm/','refresh');
        }
        
        $data['page_title'] = 'Rename Member';
        $this->load->view('auth/rename_member',$data);
    }
    
    public function pin(){
        if($this->session->userdata('group_id') <= 100){
            redirect('auth/cp/','refresh');
        }
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('pin','Old PIN','trim|required|callback__check_pin');
        $this->form_validation->set_rules('newpin','New PIN','trim|required|numeric|min_length[6]|matches[newpin2]');
        $this->form_validation->set_rules('newpin2','Retype New PIN','');
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MAuth->change_pin();
                $this->session->set_flashdata('message','Change pin successfully');
            }
            redirect('auth/change_pin/','refresh');
        }
        $data['page_title'] = 'Change PIN';
        $this->load->view('auth/change_pin',$data);
    }
}
?>