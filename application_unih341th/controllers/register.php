<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Register extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('captcha'));
	}
	
	public function index(){
		$this->activation();
	}
	
	public function activation(){
		$this->load->model(array('MMenu', 'MSignup', 'MFrontend', 'MProfile')); // Updated by Boby 20131212
		$this->load->library(array('form_validation','messages'));
		
		$this->form_validation->set_rules('introducerid','Introducer ID','trim|required|callback__introducerid_check');
		$this->form_validation->set_rules('userid','Member ID','trim|required|callback__check_userid');
		$this->form_validation->set_rules('activation','Activation Code','');
		$this->form_validation->set_rules('hp','No. HP','');
		$this->form_validation->set_rules('email','Email','valid_email');
		$this->form_validation->set_rules('name','Name Member','required|min_length[1]');
		
		$this->form_validation->set_rules('jk','Gender Type','required');
        $this->form_validation->set_rules('tempatlahir','Place of Birth','required');
		$this->form_validation->set_rules('tgllahir','Date of Birth','required|callback__check_17th');
		
		$this->form_validation->set_rules('kelurahan','Kelurahan','required');
		$this->form_validation->set_rules('kecamatan','kecamatan','required');
		$this->form_validation->set_rules('kota_id','city','required');
		$this->form_validation->set_rules('city','city','required');
		$this->form_validation->set_rules('propinsi','propinsi','');
		$this->form_validation->set_rules('kodepos','post code','required|numeric|min_length[5]|max_length[5]');
		
		$this->form_validation->set_rules('confirmCaptcha','security code','required|callback_check_captcha');
		$this->form_validation->set_rules('placementid','Sponsor ID','trim|required|callback__check_placementid');
		
		/*Modified by Boby : 2010-01-21*/
		$this->form_validation->set_rules('ktp','Your KTP Number','required|callback__check_existing_ktp');
		$this->form_validation->set_rules('alamat','Your Address','');
		$this->form_validation->set_rules('ahliwaris','Your Heir / heiress (ahli waris)','required');
		/*End Modified by Boby : 2010-01-21*/
		
		/* Created by Boby 20131212*/
		$this->form_validation->set_rules('bank_id','','');
		$this->form_validation->set_rules('norek','Account Number','min_length[10]|callback__getAccountBank');
		$this->form_validation->set_rules('area','Bank Area','');
		/* End created by Boby 20131212*/
		
		
		if($this->form_validation->run()){
			if(!$this->MMenu->blocked()){
				$introducerid = strtoupper($this->input->post('introducerid'));                                
				$placementid = strtoupper($this->input->post('placementid'));
				$memberid = strtoupper($this->input->post('userid'));
				$heir = $this->db->escape_str($this->input->post('ahliwaris'));
				
				$name = str_replace("'", "`", $this->db->escape_str($this->input->post('name')));
				$email = $this->input->post('email');
				$tgl_lahir_ = date("Y-m-d", strtotime($this->input->post('tgllahir')));
				$signup = $this->MSignup->signup(
					$introducerid,
					$memberid,
					$this->input->post('activation'),
					$placementid,
					$name,
					
					$this->input->post('jk'),
					$this->db->escape_str(strtoupper($this->input->post('tempatlahir'))),
					$tgl_lahir_,
					$this->db->escape_str(strtoupper($this->input->post('keluarahan'))),
					$this->db->escape_str(strtoupper($this->input->post('kecamatan'))),
					$this->input->post('kota_id'),
					$this->input->post('kodepos'),
					
					$this->input->post('hp'),
					$email,
					$this->db->escape_str($this->input->post('question')),
					$this->db->escape_str($this->input->post('answer')),
					$this->input->post('ktp'),
					$this->input->post('alamat'),
					$heir
				);
				
				switch($signup)
				{
					case 'REGISTRATION_SUCCESS':
						if (strlen($this->input->post('norek'))>9){
							$data = array(
								'bank_id' => $this->input->post('bank_id'),
								'member_id' => $memberid,
								'name' => $name,
								'no' => $this->input->post('norek'),
								'area' => $this->input->post('area'),
								'flag' => '0',
								'createdby' => $memberid,
								'created' => date('Y-m-d H:i:s', now())
							);
							$this->db->insert('account',$data);
							$id = $this->db->insert_id();
							$this->db->update('member',array('account_id'=>$id),array('id' => $memberid));
						}
						
						$this->session->set_flashdata('message','Your signup is successfully, please update your profile');
						redirect('main/','refresh');
					break;
					default: // Failed
						redirect('');
					break;
				}
			}else{
				redirect('register/activation/','refresh');
			}
		}else{
			$vals = array(
				'img_path'	 => './captcha/',
				'img_url'	 => base_url().'captcha/',
				'font_path'     => './unih341th_system_files/fonts/texb.ttf',			
				'img_width'	 => '140',
				'img_height' => '40',
				'expiration' => 3600
				);
			    
			    $cap = create_captcha($vals);
			    
			    $data['captcha'] = $cap;
			    $this->session->set_userdata(array('captchaWord'=> $cap['word']));
			    
			$data['bank'] = $this->MProfile->getDropDownBank(); // created by Boby 20131212
			$data['testi'] = $this->MFrontend->list_testimonial(10,0);
			$data['banner'] = $this->MFrontend->list_banner(50,0); // Created by Takwa 2013
			$data['content'] = 'index/activation_member';
			$data['title'] = "Activation Member";
			
			$data['reportDate'] = date("Y-m-d");
			$data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
			$data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
			$data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
			$data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
			
			// $this->load->view('index/index', $data, 1);
			$this->load->view('register', $data);
		}
	}
	
	public function check_captcha($confirmCaptcha){
		if ($this->_check_captcha($confirmCaptcha) == 0)
		{
		    $this->form_validation->set_message('check_captcha','The confirm security code is not available');
		    return false;
		}
		return true;
	}
	
	protected function _check_captcha($confirmCaptcha){
		$captchaWord = $this->session->userdata('captchaWord');
		
		$this->session->unset_userdata('captchaWord');
		if(strcasecmp($captchaWord, $confirmCaptcha) == 0){
			return true;
		}
		return false;
	}
	
	public function _introducerid_check()
	{
		$introducerid = $this->input->post('introducerid');
		if ($this->MSignup->check_introducerid($introducerid)){
			$this->form_validation->set_message('_introducerid_check', 'The introducer id <b>'.$introducerid.'</b> is not available.');
			return false;
		}
		return true;
	}
        
	public function _check_placementid()
	{
		$placementid = strtoupper($this->input->post('placementid'));
		if ($this->MSignup->check_placementid($placementid)){
			$this->form_validation->set_message('_check_placementid', 'The placement id <b>'.$placementid.'</b> is not available.');
			return false;
		}else{
			$introducerid = strtoupper($this->input->post('introducerid'));
			if ($placementid != $introducerid && !validation_errors()){
				$param = $this->MSignup->check_crossline($placementid,$introducerid);
				if ($param == 'error'){
					$this->form_validation->set_message('_check_placementid', 'The placement id <b>'.$placementid.'</b> crossline !');
					return false;
				}
			}
			return true;
		}
	}
	
	public function _check_userid()
	{
		if ($this->MSignup->_check_userid($this->input->post('userid'),$this->input->post('activation'))){			
			$this->form_validation->set_message('_check_userid', 'The user id  or activation code is not available.');
			return false;
		/* Modified by Boby 20110113 */
		}else{
			if ($this->MSignup->cekSKit($this->input->post('userid'),$this->input->post('activation'))){
				$this->form_validation->set_message('_check_userid', 'The user id  or activation code is expired.');
				return false;
			}
		}
		/* Modified by Boby 20110113 */
		return true;
	}

	public function _check_17th()
	{
		/*
		$dateOfBirth = $this->input->post('tgllahir')." 00:00:00";
		$today = date("Y-m-d H:i:s");
		$diff = date_diff(date_create($dateOfBirth), date_create($today));
		$age = $diff->format('%y');
		*/
		
		$dob = $this->input->post('tgllahir');
		list($y, $m, $d) = array_pad(explode('-', $dob, 3), 3, 0);
		//echo var_dump(checkdate($m, $d, $y));
		if(checkdate($m,$d,$y))// && !array_sum($dt::getLastErrors());
		{
			$age = $this->ageCalculator($this->input->post('tgllahir'));
			if ($age < 18){			
				$this->form_validation->set_message('_check_17th', 'Minimum Age to register is 18 years old');
				return false;
			}
		}else{
			$this->form_validation->set_message('_check_17th', 'Wrong Format Date');
			return false;
		}
		return true;
	}
	
	public function ageCalculator($dob){
	    if(!empty($dob)){
	        $birthdate = new DateTime($dob);
	        $today   = new DateTime('today');
	        $age = $birthdate->diff($today)->y;
	        return $age;
	    }else{
	        return 0;
	    }
	}

	public function _check_existing_ktp()
	{
		if ($this->MSignup->_check_ktp($this->input->post('ktp'))==false){			
			$this->form_validation->set_message('_check_existing_ktp', 'KTP Number is already exist');
			return false;
		}
		/* Modified by Boby 20110113 */
		return true;
	}
	
	protected function send_mail(){ 
		$data['memberid'] = strtoupper($this->input->post('userid'));
		$data['passwd'] = $this->db->escape_str($this->input->post('password'));
		$data['pin'] = $this->db->escape_str($this->input->post('pin'));
		$data['name'] = $this->db->escape_str($this->input->post('name'));
		$data['email'] = $this->input->post('email');	
				
	    $this->load->library('Email');
	    
	    $config['mailtype'] = 'html';
	    $config['wordwrap'] = TRUE;     
	    $this->email->initialize($config);     
	    
	    $this->email->from('info@uni-health.com', 'MLM Online Register');
	    $this->email->to($data['email']);
	    //$this->email->bcc('info@smartindo-technology.com');
	    $this->email->subject('Registration Online MLM UNIHEALTH');
		$body = $this->load->view('mail/registration', $data, true);
		$this->email->message($body);
		$this->email->send();
	   
		//echo $this->email->print_debugger();
	}
	
	public function member(){
		$this->load->model(array('MMenu','MSignup','MFrontend'));
                $this->load->library(array('form_validation','messages'));
		
		$this->form_validation->set_rules('name','Name Member','required|min_length[3]');
		$this->form_validation->set_rules('address','Alamat','required');
		$this->form_validation->set_rules('email','Email','required|valid_email');
		$this->form_validation->set_rules('zip','Kode Pos','required|numeric|min_length[5]');
		$this->form_validation->set_rules('infofrom','','');
		$this->form_validation->set_rules('delivery','','');
		$this->form_validation->set_rules('hp','No. HP','trim|required|numeric|min_length[9]');
		$this->form_validation->set_rules('confirmCaptcha','Confirm Captcha','required|callback_check_captcha');
		
		if($this->form_validation->run()){
                        if(!$this->MMenu->blocked()){
                                $this->MSignup->add_register();
				//$this->send_mail_register();
				$this->session->set_flashdata('message','Thank you, register successfully...');
			}
			redirect('register/member/','refresh');
		}else{
                        $vals = array(
				'img_path'	 => './captcha/',
				'img_url'	 => base_url().'captcha/',
				'font_path'     => './unih341th_system_files/fonts/texb.ttf',			
				'img_width'	 => '140',
				'img_height' => '40',
				'expiration' => 3600
				);
			    
			    $cap = create_captcha($vals);
			    
			    $data['captcha'] = $cap;
			    $this->session->set_userdata(array('captchaWord'=> $cap['word']));
			    
			$data['content'] = 'index/register_member';
			$data['title'] = "Register Member";
			$this->load->view('index/index', $data, 1);
                }
	}
	
	protected function send_mail_register(){ 
		$data['name'] = $this->db->escape_str($this->input->post('name'));
		$data['email'] = $this->input->post('email');	
				
		$this->load->library('Email');
	    
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;     
		$this->email->initialize($config);     
	    
		$this->email->from('info@uni-health.com', 'MLM Online Register');
		$this->email->to($data['email']);
		
		//$this->email->bcc('info@smartindo-technology.com');
		$this->email->subject('Registration Online MLM UNIHEALTH');
		$body = $this->load->view('mail/register_member', $data, true);
		$this->email->message($body);
		$this->email->send();
	   
		//echo $this->email->print_debugger();
	}
	
	/* Created by Boby 20131212 */
	public function _getAccountBank(){
		if (strlen($this->input->post('check_norekeningnorek'))>9){
			//$q=$this->db->get_where('account',array('no'=>$this->input->post('norek')));
			// echo $this->db->last_query();
			// return $var = ($q->num_rows>0) ? $q->row() : false;
			if($this->MSignup->check_norekening($this->input->post('norek')))
			{
				$this->form_validation->set_message('_getAccountBank', 'The account number is already exist.');
				return false;
			}else{
				if (strlen($this->input->post('area'))==0){
					$this->form_validation->set_message('_getAccountBank', 'Please fill the bank area.');
					return false;
				}
			}
		}
		return true;
    }
	/* End created by Boby 20131212 */

}?>