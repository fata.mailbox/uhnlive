<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Aboutus extends CI_Controller {
    function __construct()
    {
		parent::__construct();
		$this->load->model('MFrontend');
	}
	
	public function index(){
		$data['banner'] = $this->MFrontend->list_banner(50,0);
		
		$data['content'] = 'index/aboutus';
		$this->load->view('index/index',$data);
	}
	public function soho_group(){
		$data['banner'] = $this->MFrontend->list_banner(50,0);
		
		$data['content'] = 'index/soho_group';
		$this->load->view('index/index',$data);
	}
	/* update by Andrew 201304 */
	public function contactus(){
		$data['banner'] = $this->MFrontend->list_banner(50,0);
		
		$data['content'] = 'index/contactus';
		$this->load->view('index/index',$data);
	}
	/* end update by Andrew 201304 */
}?>
