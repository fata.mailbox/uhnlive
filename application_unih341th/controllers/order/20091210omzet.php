<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Omzet extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
            $this->load->model(array('MMenu','MRekap'));
    }

    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        
        if($this->form_validation->run()){
            $data['ro'] = $this->MRekap->omzet_ro('stc',$this->input->post('fromdate'),$this->input->post('todate'));
            $data['ro_mstc'] = $this->MRekap->omzet_ro('m-stc',$this->input->post('fromdate'),$this->input->post('todate'));
            
            $data['totalro'] = $this->MRekap->sum_omzet_ro('stc',$this->input->post('fromdate'),$this->input->post('todate'));
            if(!$data['totalro']['totalharga']){
                $data['totalro']['ftotalharga']="0";
                $data['totalro']['totalharga']="0";
            }
            
            $data['totalro_mstc'] = $this->MRekap->sum_omzet_ro('m-stc',$this->input->post('fromdate'),$this->input->post('todate'));
            if(!$data['totalro_mstc']['totalharga']){
                $data['totalro_mstc']['ftotalharga']="0";
                $data['totalro_mstc']['totalharga']="0";
            }
            
            $data['so'] = $this->MRekap->omzet_so($this->input->post('fromdate'),$this->input->post('todate'));
            $data['totalso'] = $this->MRekap->sum_omzet_so($this->input->post('fromdate'),$this->input->post('todate'));
            if(!$data['totalso']['totalharga']){
                $data['totalso']['ftotalharga']="0";
                $data['totalso']['totalharga']="0";
            }
                        
            /*$data['retur'] = $this->MRekap->omzet_retur($this->input->post('fromdate'),$this->input->post('todate'));
            $data['totalretur'] = $this->MRekap->sum_omzet_retur($this->input->post('fromdate'),$this->input->post('todate'));
            if(!$data['totalretur']['totalharga']){
                $data['totalretur']['ftotalharga']="0";
                $data['totalretur']['totalharga']="0";
            }
            */
            //echo $data['totalro']['totalharga'];
            $data['total']=number_format($data['totalro']['totalharga'] + $data['totalso']['totalharga'] + $data['totalro_mstc']['totalharga'],0,"",",");
            
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'Omzet Penjualan';
        $this->load->view('order/omzet_index',$data);
    }
    
}
?>