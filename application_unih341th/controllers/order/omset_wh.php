<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Omset_wh extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
            $this->load->model(array('MMenu','MRekap'));
    }

    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        $this->form_validation->set_rules('whid','','');
        
        if($this->form_validation->run()){
			//$data['rekap'] = $this->MRekap->omset_ro_($this->input->post('fromdate'),$this->input->post('todate'));
			// 20160930 ASP Start
			//$data['rekap'] = $this->MRekap->omset_ro_new_($this->input->post('fromdate'),$this->input->post('todate'));
			// 20160930 ASP End
			// START ASP 20180512
			if($this->input->post('whsid')=='all')
				$data['rekap'] = $this->MRekap->omset_ro_new_($this->input->post('fromdate'),$this->input->post('todate'));
			else
				$data['rekap'] = $this->MRekap->omset_ro_new_wh($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('whsid'));
			// EOF ASP 20180512
        }else{
            $data['rekap']=false;
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
		
	    $data['warehouse']=$this->MRekap->getDropDownWhs('');
		//20150929 ASP Start
        //$data['page_title'] = 'Omset Per Product';
        $data['page_title'] = 'Omset By SKU (per Warehouse)';
		//20150929 ASP End
		if($this->input->post('submit')=="export")
		$this->load->view('order/omset_table_export',$data);
		else
        $this->load->view('order/omset_wh_index',$data);
    }
    
}
?>