<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class omset_product_nett extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
            $this->load->model(array('MMenu','MRekap'));
    }

    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        
        if($this->form_validation->run()){
			//$data['rekap'] = $this->MRekap->omset_ro_product_gross_($this->input->post('fromdate'),$this->input->post('todate'));
			// 20161010 ASP Start
			//$data['rekap'] = $this->MRekap->omset_ro_product_gross_new_($this->input->post('fromdate'),$this->input->post('todate'));
			// 20161010 ASP End
			
			//20180901 ASP Start
			if($this->input->post('submit')=="export")
			$data['rekap'] = $this->MRekap->omset_ro_product_nett_new2_($this->input->post('fromdate'),$this->input->post('todate'));
			else
			//$data['rekap'] = $this->MRekap->omset_ro_product_gross_new_($this->input->post('fromdate'),$this->input->post('todate'));
			$data['rekap'] = $this->MRekap->omset_ro_product_nett_new2_($this->input->post('fromdate'),$this->input->post('todate'));
			//20180901 ASP End
        }else{
            $data['rekap']=false;
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'Omset By Product';
		if($this->input->post('submit')=="export")
		$this->load->view('order/omset_product_nett_table_export',$data);
		else
        $this->load->view('order/omset_product_nett_index',$data);
    }
    
}
?>