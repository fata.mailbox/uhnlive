<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sostaff extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
            redirect('');
        }
        $this->load->model(array('MMenu','SO_staff_model','MMaster'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        $config['base_url'] = site_url().'order/sostaff/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->SO_staff_model->count_so_staff($keywords);
        //echo $config['total_rows'];
        $this->pagination->initialize($config);
        $data['results'] = $this->SO_staff_model->search_so_staff($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Sales Order Staff';
        $this->load->view('order/so_staff_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
		
		// Update by Boby 20140121
        $this->form_validation->set_rules('stc_id','','');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('namestc','','');
        
		$this->form_validation->set_rules('member_id','Member','required');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('remark','','');
        $this->form_validation->set_rules('ewallet','','');
            
        $groupid = $this->session->userdata('group_id');
	
        if($groupid>100){
            $this->form_validation->set_rules('pin','PIN','required|callback__check_pin');
        }elseif($this->input->post('stc_id') == '0'){
            $this->form_validation->set_rules('pu','','');
            $this->form_validation->set_rules('tunai','','');
            $this->form_validation->set_rules('debit','','');
            $this->form_validation->set_rules('credit','','');
            $this->form_validation->set_rules('totalbayar','Total Bayar','callback__check_bayar');
        }
        
		$this->form_validation->set_rules('total','Total repeat order','required|callback__check_order');
        $this->form_validation->set_rules('totalpv','','');
        $this->form_validation->set_rules('totalbv','','');
        
		if($_POST['action'] == 'Go'){
			$rowx = $this->input->post('rowx');
			$counti = count($_POST['counter'])+$rowx;
		}elseif($_POST['action'] == 'Submit'){
			$counti = count($_POST['counter']);
		}else{
			$counti=5;
		}
		$this->session->set_userdata(array('counti'=>$counti));
	
		$i=0;
		while($counti > $i){
			$this->form_validation->set_rules('itemcode'.$i,'','');
			$this->form_validation->set_rules('itemname'.$i,'');
			$this->form_validation->set_rules('qty'.$i,'');
			$this->form_validation->set_rules('price'.$i,'');
			$this->form_validation->set_rules('price'.$z.'_','','');
			$this->form_validation->set_rules('subtotal'.$i,'');
			$this->form_validation->set_rules('subtotal_'.$i,'');
			$this->form_validation->set_rules('pv'.$i,'');
			$this->form_validation->set_rules('subtotalpv'.$i,'');
			$this->form_validation->set_rules('titipan_id'.$i,'');
			$this->form_validation->set_rules('bv'.$i,'');
			$this->form_validation->set_rules('subtotalbv'.$i,'');
			
			$i++;
		}
		$data['counti']=$counti;
		
        if($this->form_validation->run() && $_POST['action'] != 'Go'){
            if(!$this->MMenu->blocked()){
				$this->SO_staff_model->add_so_staff();
				$this->session->set_flashdata('message','Sales Order Staff successfully');
            }
            redirect('order/sostaff','refresh');
        }
	
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-green.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['groupid'] = $groupid;
        $data['page_title'] = 'Create Sales Order Staff';
        $this->load->view('order/so_staff_form',$data);
    }
    
    function _check_order(){
        $amount = str_replace(".","",$this->input->post('total'));
		if($amount <= 0){
			$this->form_validation->set_message('_check_order','browse product...');
			return false;
		}else{
			if(!validation_errors()){
			$memberid = "STAFF";
			$data = array(
				'member_id'=> $memberid
			);
			$this->db->insert('so_temp',$data);
			
			$id = $this->db->insert_id();
			
			$key=0;
			$whsid = 1;
			while($key < count($_POST['counter'])){
				$qty = str_replace(".","",$this->input->post('qty'.$key));
				$itemid = $this->input->post('itemcode'.$key);
				if($itemid and $qty > 0){
				$data=array(
					'so_id' => $id,
					'item_id' => $itemid,
					'qty' => $qty
				);
				$this->db->insert('so_temp_d',$data);
				
				$rs = $this->SO_staff_model->check_stock($id,$itemid,$whsid);
				if($rs->stock < $rs->qty){
					$this->form_validation->set_message('_check_order',"We only have ".$rs->stock." ".$rs->name);
					
					$this->db->delete('so_temp',array('id'=>$id));
					$this->db->delete('so_temp_d',array('so_id'=>$id));
					return false;
				}
				}
				$key++;
			}
			
			$this->db->delete('so_temp',array('id'=>$id));
			$this->db->delete('so_temp_d',array('so_id'=>$id));
			}
		}
        return true;
    }
    
    public function _check_bayar(){
        $amount = str_replace(".","",$this->input->post('total'));
        $bayar = str_replace(".","",$this->input->post('totalbayar'));
        
		if($amount > $bayar){
			$this->form_validation->set_message('_check_bayar','Pembayaran tidak mencukupi');
			return false;
		}
        return true;
    }
    public function _check_tglso(){
        if(date('Y-m-d',now()) > $this->input->post('date')){
			$allow = 0;
			if($this->session->userdata('group_id')>100){
	            $this->form_validation->set_message('_check_tglso','Tanggal sales order tidak boleh mundur');
				return false;
			}else{
				if($this->session->userdata('group_id')<100){
					$row=$this->SO_staff_model->get_blocked();
					if($this->input->post('date') <= $row->tglrunning){
						$this->form_validation->set_message('_check_tglso','Tanggal '.$row->tglrunning.' bonus sudah dijalankan');
						return false;
					}
				}
			}
        }
        return true;
    }
    
    public function _check_pin(){
        if(!$this->MAuth->check_pin($this->session->userdata('username'),$this->input->post('pin'))){
            $this->form_validation->set_message('_check_pin','Sorry, your pin invalid');
            return false;
        }
        return true;
    }
	
	public function _check_delivery(){
        if($this->input->post('pu')==1){
			if(strlen($this->input->post('city'))<1 OR strlen($this->input->post('addr'))<1){
				$this->form_validation->set_message('_check_delivery','Please fill the address.');
				return false;
			}
        }
        return true;
    }
	
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->SO_staff_model->get_so($id);
        
        if(!count($row)){
            redirect('order/sostaff','refresh');
        }
        $data['row'] = $row;
        $data['items'] = $this->SO_staff_model->get_so_detail($id);
	$data['page_title'] = 'View Sales Order Staff';
        $this->load->view('order/so_staff_view',$data);
    }
    
}
?>
