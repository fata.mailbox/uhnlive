<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ac extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MActivationcode'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3)) $this->session->unset_userdata('keywords');
        }
        
        $config['base_url'] = site_url().'order/ac/index/';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        
        $keywords = $this->session->userdata('keywords');
        $config['total_rows'] = $this->MActivationcode->countActivationCode($keywords);
        $this->pagination->initialize($config);
        $data['results'] = $this->MActivationcode->searchActivationCode($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        
        $data['page_title'] = 'Member ID & Activation Code';
        $this->load->view('order/activationcode_index',$data);
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('qty','Quantity','required|numeric|callback__check_min');
        if($this->session->userdata('group_id') > 100){
            $this->form_validation->set_rules('pin','PIN','required|callback__check_pin');
        }
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MActivationcode->addActivationCode();
                $this->session->set_flashdata('message','Created activation code successfully');
            }
            redirect('order/ac','refresh');
        }
        $row = $this->MActivationcode->getTitipanKit($this->session->userdata('userid'));
        if($row)$data['qty'] = $row->qty;
        else $data['qty'] = 0;
        $data['warehouse'] = $this->MActivationcode->getWarehouse();
        $data['page_title'] = 'Create Activation Code';
        $this->load->view('order/activationcode_form',$data);
    }
    
    public function _check_min(){
        if($this->session->userdata('group_id') >100){
            $qty = str_replace(".","",$this->input->post('qty'));
            if($qty <= 0){
                $this->form_validation->set_message('_check_min','Quantity harus > 0 !');
                return false;
            }else{
                $row = $this->MActivationcode->getTitipanKit($this->session->userdata('userid'));
                if($row)$stock = $row->qty;
                else $stock = 0;
                if($qty > $stock){
                    $this->form_validation->set_message('_check_min','Stock titipan kit anda tidak mencukupi');
                    return false;
                }else{
                    $row = $this->MActivationcode->getACNotUsed($this->session->userdata('userid'));
                    if($row)$account = $row->account;
                    else $account = 0;
                    if($qty > ($stock - $account)){
                        $this->form_validation->set_message('_check_min','Stock titipan kit anda tidak mencukupi');
                        return false;
                    }
                }
            }
        }
        return true;
    }
        
    public function _check_pin(){
        if(!$this->MAuth->check_pin($this->session->userdata('username'),$this->input->post('pin'))){
            $this->form_validation->set_message('_check_pin','Sorry, your pin invalid');
            return false;
        }
        return true;
    }
    public function reject(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        if(!$this->MMenu->blocked()){
           $this->MActivationcode->reject();
        }
        redirect('order/ac/','refresh');
    }
    
}
?>