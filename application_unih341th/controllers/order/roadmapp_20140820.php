<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class roadmapp extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
        
        $this->load->model(array('MMenu','MRo'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'order/roadmapp/index/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']);
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
            $keywords = $this->session->userdata('keywords');
            $config['total_rows'] = $this->MRo->countRO($keywords);
            $this->pagination->initialize($config);
            $data['results'] = $this->MRo->searchRO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }else{
            if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');
            
            $keywords = $this->session->userdata('keywords');            
            $config['total_rows'] = $this->MRo->countRO($keywords);
            $this->pagination->initialize($config);
        
            $data['results'] = $this->MRo->searchRO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        }
        
        $data['page_title'] = 'Request Order Approval';
        $this->load->view('order/request_order2',$data);
    }
    
    public function view($id){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $row = $this->MRo->getRequestOrder($id);
        
        if(!count($row)){
            redirect('order/roadmapp','refresh');
        }
		$bns = $this->MRo->getFree($id);
		$data['bns'] = $bns;
		$data['row'] = $row;
        $data['items'] = $this->MRo->getRequestOrderDetail($id);
        $data['page_title'] = 'View Request Order Approval';
        $this->load->view('order/requestorder_approved_view',$data);
    }
    
    public function approved(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        if(!$this->MMenu->blocked()){
            $this->MRo->requestOrderApproved();
        }
        redirect('order/roadmapp/','refresh');
    }
    
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        $this->load->model(array('MAuth'));
        $this->load->library(array('form_validation','messages'));
        
		$this->form_validation->set_rules('member_id','Stockiest','required');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('name','','');
        $this->form_validation->set_rules('remark','','');
        $this->form_validation->set_rules('total','Total repeat order','required|callback__check_min');
        $this->form_validation->set_rules('totalpv','','');
        $this->form_validation->set_rules('persen','','');
		$this->form_validation->set_rules('rpdiskon','','');
        $this->form_validation->set_rules('rpdiskon2','','');
        $this->form_validation->set_rules('totalbayar','','');
		$this->form_validation->set_rules('persen1','','');
		$this->form_validation->set_rules('rpdiskon1','','');
		
		$this->form_validation->set_rules('kota_id','','');
		$this->form_validation->set_rules('propinsi','','');
		$this->form_validation->set_rules('timur','','');
		$this->form_validation->set_rules('deli_ad','','');
		$this->form_validation->set_rules('pu','','');
		$this->form_validation->set_rules('addr','','');
		$this->form_validation->set_rules('addr1','','');
		$this->form_validation->set_rules('kota_id1','','');
		$this->form_validation->set_rules('city','','');
		
		for($i=0;$i<=9;$i++){
			$this->form_validation->set_rules('itemcode'.$i,'','');
			$this->form_validation->set_rules('itemname'.$i,'','');
			$this->form_validation->set_rules('qty'.$i,'','');
			$this->form_validation->set_rules('price'.$i,'','');
			$this->form_validation->set_rules('gprice'.$i,'','');
			$this->form_validation->set_rules('gprice2'.$i,'','');
			$this->form_validation->set_rules('subtotal'.$i,'','');
			$this->form_validation->set_rules('pv'.$i,'','');
			$this->form_validation->set_rules('subtotalpv'.$i,'','');
			$this->form_validation->set_rules('diskonrp'.$i,'','');
			$this->form_validation->set_rules('subdiskonrp'.$i,'','');
		}
		
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MRo->roadmappCreate();
                $this->session->set_flashdata('message','Request Order successfully');
            }
            redirect('order/roadmapp','refresh');
        }
        $data['page_title'] = 'Create Request Order';
		$this->load->view('order/roadm_create',$data);
    }
    
    public function _check_min(){
        $amount = str_replace(".","",$this->input->post('total'));
		
        if($this->input->post('persen') >0){
            $totalbayar = $amount - ($amount * $this->input->post('persen') / 100);
			$totalbayar = $this->input->post('totalbayar'); // updated by Boby 20140725
        }else{
            $totalbayar = $amount;
        }
		
        if($amount <= 0){
            $this->form_validation->set_message('_check_min','browse product...');
            return false;
        }else{
            if($this->input->post('member_id')){
				$ecb = 0;
				$ecb = str_replace(".","",$this->input->post('rpdiskon1'));
				if($ecb > 0){
					$row = $this->MRo->getECB($this->input->post('member_id'));
					if($row)
					{
						$ecbsaldo = $row->saldoecb;
						if($ecb > $ecbsaldo){
							$this->form_validation->set_message('_check_min','ECB anda tidak mencukupi');
							return false;
						}
					}else{
						$this->form_validation->set_message('_check_min','anda tidak MEMPUNYAI ECB');
						return false;
					}
				}
				
				$row = $this->MRo->getEwallet($this->input->post('member_id'));
				if(($totalbayar-$ecb) > $row->ewallet){
					$this->form_validation->set_message('_check_min','Ewallet anda tidak mencukupi');
					return false;
				}else{
					$flag = 0;
					if($this->input->post('itemcode0')){$item[$flag]=$this->input->post('itemcode0'); $qty[$flag]=str_replace(".","",$this->input->post('qty0')); $itemname[$flag]=$this->input->post('itemname0');	$prc[$flag]=str_replace(".","",$this->input->post('price0'));	$flag+=1;}
					if($this->input->post('itemcode1')){$item[$flag]=$this->input->post('itemcode1'); $qty[$flag]=str_replace(".","",$this->input->post('qty1')); $itemname[$flag]=$this->input->post('itemname1');	$prc[$flag]=str_replace(".","",$this->input->post('price1'));	$flag+=1;}
					if($this->input->post('itemcode2')){$item[$flag]=$this->input->post('itemcode2'); $qty[$flag]=str_replace(".","",$this->input->post('qty2')); $itemname[$flag]=$this->input->post('itemname2');	$prc[$flag]=str_replace(".","",$this->input->post('price2'));	$flag+=1;}
					if($this->input->post('itemcode3')){$item[$flag]=$this->input->post('itemcode3'); $qty[$flag]=str_replace(".","",$this->input->post('qty3')); $itemname[$flag]=$this->input->post('itemname3');	$prc[$flag]=str_replace(".","",$this->input->post('price3'));	$flag+=1;}
					if($this->input->post('itemcode4')){$item[$flag]=$this->input->post('itemcode4'); $qty[$flag]=str_replace(".","",$this->input->post('qty4')); $itemname[$flag]=$this->input->post('itemname4');	$prc[$flag]=str_replace(".","",$this->input->post('price4'));	$flag+=1;}

					if($this->input->post('itemcode5')){$item[$flag]=$this->input->post('itemcode5'); $qty[$flag]=str_replace(".","",$this->input->post('qty5')); $itemname[$flag]=$this->input->post('itemname5');	$prc[$flag]=str_replace(".","",$this->input->post('price5'));	$flag+=1;}
					if($this->input->post('itemcode6')){$item[$flag]=$this->input->post('itemcode6'); $qty[$flag]=str_replace(".","",$this->input->post('qty6')); $itemname[$flag]=$this->input->post('itemname6');	$prc[$flag]=str_replace(".","",$this->input->post('price6'));	$flag+=1;}
					if($this->input->post('itemcode7')){$item[$flag]=$this->input->post('itemcode7'); $qty[$flag]=str_replace(".","",$this->input->post('qty7')); $itemname[$flag]=$this->input->post('itemname7');	$prc[$flag]=str_replace(".","",$this->input->post('price7'));	$flag+=1;}
					if($this->input->post('itemcode8')){$item[$flag]=$this->input->post('itemcode8'); $qty[$flag]=str_replace(".","",$this->input->post('qty8')); $itemname[$flag]=$this->input->post('itemname8');	$prc[$flag]=str_replace(".","",$this->input->post('price8'));	$flag+=1;}
					if($this->input->post('itemcode9')){$item[$flag]=$this->input->post('itemcode9'); $qty[$flag]=str_replace(".","",$this->input->post('qty9')); $itemname[$flag]=$this->input->post('itemname9');	$prc[$flag]=str_replace(".","",$this->input->post('price9'));	$flag+=1;}

					$temp = 0;$twin=0;
					$whsid = $this->session->userdata('whsid');
					
					for($i=0;$i<=$flag;$i++){
						if($temp>0){
							for($j=0;$j<$temp;$j++){
								if($brg[$j]==$item[$i]){//jika sama
									$twin = 1;
									$jml[$j]+=$qty[$i];
									//echo $qty[$i]."<br>";
								}
							}
						}			
						if($twin==0){//jika tidak ada yang sama
							$jml[$temp]=$qty[$i];
							$brg[$temp]=$item[$i];
							$price[$temp]=$prc[$i];
							$nama[$temp]=$itemname[$i];
							$temp+=1;
						}else{$twin=0;}
					}
					for($i=0;$i<count($jml)-1;$i++){
						//echo "<br><br>".$brg[$i]." = ".$jml[$i]." ".$nama[$i]." ";
						$readystok=$this->MRo->cekStok($whsid, $brg[$i], $price[$i], $jml[$i], $this->session->userdata('userid'));
						if($readystok!='Ready'){$twin=1;$stok="We only have ".$readystok." ".$nama[$i];}
						//echo "Stok = ".$readystok."<br>";
					}
					if($twin > 0){
						$this->form_validation->set_message('_check_min','Out of order ( '.$stok.' )');
						return false;
					}
				}
			}
        }
        return true;
    }
}
?>