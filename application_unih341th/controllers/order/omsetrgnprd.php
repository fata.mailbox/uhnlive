<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class omsetrgnprd extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
            $this->load->model(array('MMenu','MRekap'));
    }

    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
		$this->form_validation->set_rules('leader','','');
		$this->form_validation->set_rules('member_id','','');
		$this->form_validation->set_rules('kota_id','','');
        
        if($this->form_validation->run()){
			$data['rekap'] = $this->MRekap->periodeOmsetPerProduct(
				$this->input->post('fromdate'), 
				$this->input->post('leader'),
				$this->input->post('member_id'), 
				$this->input->post('kota_id')
				);
			$data['field_'] = $this->MRekap->periodeOmset($this->input->post('fromdate'));
        }else{
            $data['rekap']=false;
			$data['field_']=false;
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'Omset Per Regional';
        $this->load->view('order/omset_prd_rgn_index',$data);
    }
    
}
?>