<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Omzet extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
            redirect('');
        }
            $this->load->model(array('MMenu','MRekap'));
    }

    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        
        if($this->form_validation->run()){
			/* Modified by Boby (2009-12-10)*/
			
			/*RO*/
            $data['ro'] = $this->MRekap->omzet_ro('stc',$this->input->post('fromdate'),$this->input->post('todate'));//getDataRO
            $data['totalro'] = $this->MRekap->sum_omzet_ro('stc',$this->input->post('fromdate'),$this->input->post('todate'));//getSumRO
            if(!$data['totalro']['totalharga']){
                $data['totalro']['ftotalharga']="0";
                $data['totalro']['totalharga']="0";
            }
			/*RO M-Stc*/
            $data['ro_mstc'] = $this->MRekap->omzet_ro('m-stc',$this->input->post('fromdate'),$this->input->post('todate'));//getDataROMStc
            $data['totalro_mstc'] = $this->MRekap->sum_omzet_ro('m-stc',$this->input->post('fromdate'),$this->input->post('todate'));//getSumROMStc
            if(!$data['totalro_mstc']['totalharga']){
                $data['totalro_mstc']['ftotalharga']="0";
                $data['totalro_mstc']['totalharga']="0";
            }
            /*SO*/
            $data['so'] = $this->MRekap->omzet_so($this->input->post('fromdate'),$this->input->post('todate'));//getDataSO
            $data['totalso'] = $this->MRekap->sum_omzet_so($this->input->post('fromdate'),$this->input->post('todate'));//getSumSO
            if(!$data['totalso']['totalharga']){
                $data['totalso']['ftotalharga']="0";
                $data['totalso']['totalharga']="0";
            }
			/*SC Payment*/
			$data['sc'] = $this->MRekap->omzet_scp($this->input->post('fromdate'),$this->input->post('todate'));//getDataSCPayment
			$data['totalsc'] = $this->MRekap->sum_omzet_scp($this->input->post('fromdate'),$this->input->post('todate'));//getSumSCPayment
            if(!$data['totalsc']['totalhrg']){
                $data['totalsc']['totalhrg']="0";
                $data['totalsc']['totalpv']="0";
            }
            $data['total']=number_format($data['totalro']['totalharga'] + $data['totalso']['totalharga'] + $data['totalro_mstc']['totalharga'] + $data['totalsc']['totalhrg'],0,"",",");
            /* End modified by Boby (2009-12-10)*/
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'Summary of sales';
        $this->load->view('order/omzet_index',$data);
    }
    
}
?>