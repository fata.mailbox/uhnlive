<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Hsostc extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('');
        }
        $this->load->model(array('MMenu','MRekap'));
    }
    
    public function index(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
            redirect('error','refresh');
        }
        $this->load->library(array('form_validation'));
        
        $this->form_validation->set_rules('fromdate','','');
        $this->form_validation->set_rules('todate','','');
        $this->form_validation->set_rules('member_id','','');
        $this->form_validation->set_rules('no_stc','','');
        $this->form_validation->set_rules('name','','');
                
        if($this->form_validation->run()){
            $data['results'] = $this->MRekap->list_salesorder_stc($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('member_id'));
            $data['total'] = $this->MRekap->sum_salesorder_stc($this->input->post('fromdate'),$this->input->post('todate'),$this->input->post('member_id'));
        }else{
            $data['results'] = false;
            $data['total'] = false;
        }
        
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
        $data['page_title'] = 'Sumary Sales Order Stockiest';
        $this->load->view('order/rekapsostc_index',$data);
    }
    
}
?>