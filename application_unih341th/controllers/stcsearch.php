<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stcsearch extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MSearch'));
    }
    
    public function all(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'stcsearch/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countStockiest($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchStockiest($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Stockiest Search';
        
        $this->load->view('search/stockiest_search',$data);
    }
	
	
	// updated by Boby 20100614
	public function all2(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'stcsearch/all2/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countStockiest($keywords,'0');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchStockiest($keywords,'0',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Stockiest Search';
        
        $this->load->view('search/stockiest_search2',$data);
    }
    // end updated by Boby 20100614
    
    public function all3(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'stcsearch/all3/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countStockiest($keywords,'0');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchStockiest($keywords,'0',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Stockiest Search';
        
        $this->load->view('search/stockiest_search3',$data);
    }
    
    public function stc(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'stcsearch/stc/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countStockiest($keywords,'0');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchStockiest($keywords,'0',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Stockiest Search';
        
        $this->load->view('search/stockiest_search',$data);
    }
    public function mstc(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'stcsearch/mstc/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countStockiest($keywords,'mstc');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
	  if($this->session->userdata('group_id')<100){
		$data['results'] = $this->MSearch->searchStockiest($keywords,'0',$config['per_page'],$data['from_rows']);
	  }else{
        	$data['results'] = $this->MSearch->searchStockiest($keywords,'mstc',$config['per_page'],$data['from_rows']);
	  }
        $data['page_title'] = 'Stockiest Search';
        
        $this->load->view('search/stockiest_search',$data);
    }
    
    public function romstc(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'stcsearch/romstc/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countStockiest($keywords,'romstc');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchStockiest($keywords,'romstc',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Stockiest Search';
        
        $this->load->view('search/stockiest_search',$data);
    }
	
	/* Created by Boby 20130429 */
	public function stcactive(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'stcsearch/stcactive/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(3); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countStockiestAct($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Stockiest Search';
        
        $this->load->view('search/stockiest_search',$data);
    }
	/* End created by Boby 20130429 */
	
	/* Created by Boby 20140129 */
	public function stcactive_ro(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'stcsearch/stcactive/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(3); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countStockiestAct($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Stockiest Search';
        
        $this->load->view('search/stockiest_search_ro',$data);
    }
	/* End created by Boby 20140129 */
	
	/* Created by Boby 20130429 */
	public function stcpjm(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'stcsearch/stcactive/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(3); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countStockiestAct($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearch->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Stockiest Search';
        
        $this->load->view('search/stockiest_search_pjm',$data);
    }
	/* End created by Boby 20130429 */
}
?>