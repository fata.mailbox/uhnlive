<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Membersearch2 extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }

        $this->load->model(array('Search_Model2'));
    }

    public function index(){
        $this->load->library(array('form_validation','pagination'));

        $this->form_validation->set_rules('search','Keywords','min_length[3]');

        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');

        $config['base_url'] = site_url().'search/membersearch2/index/';
        $config['total_rows'] = $this->Search_Model2->countMember($keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $this->pagination->initialize($config);

        $data['results'] = $this->Search_Model2->searchMember($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['totalrow'] = $config['total_rows'];
        $data['page_title'] = 'Search Member';
        $this->load->view('smartindo/member_search2',$data);
    }

    public function ewalletso(){
        $this->load->library(array('form_validation','pagination'));

        $this->form_validation->set_rules('search','Keywords','min_length[3]');

        $config['base_url'] = site_url().'search/membersearch2/ewalletso/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(5); //untuk no urut paging
        $config['total_rows'] = $this->Search_Model2->countMember($keywords);
        $config['per_page'] = 20;
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);

        $data['results'] = $this->Search_Model2->searchMemberSo($keywords,$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Member Search';

        $this->load->view('smartindo/member_ewallet_search_so2',$data);
    }

    public function ewallet(){
        $this->load->library(array('form_validation','pagination'));

        $this->form_validation->set_rules('search','Keywords','min_length[3]');

        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');

        $config['base_url'] = site_url().'search/membersearch2/ewallet/index/';
        $config['total_rows'] = $this->Search_Model2->countMember($keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 5;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $this->pagination->initialize($config);

	$data['totalrow'] = $config['total_rows'];
        $data['results'] = $this->Search_Model2->searchMember($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['page_title'] = 'Reset Password';

        $this->load->view('search/member_ewallet_search2',$data);
    }

    public function so_v(){
        $this->load->library(array('form_validation','pagination'));

        $this->form_validation->set_rules('search','Keywords','min_length[3]');

        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');

        $config['base_url'] = site_url().'search/membersearch2/so_v/index/';
        $config['total_rows'] = $this->Search_Model2->countMember($keywords);
        //$config['total_rows'] = $this->Search_Model2->countMember($keywords);
        $config['per_page'] = 20;
        $config['uri_segment'] = 5;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $this->pagination->initialize($config);

        //$data['results'] = $this->Search_Model2->searchMember($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['results'] = $this->Search_Model2->searchMemberSoMandiri($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['totalrow'] = $config['total_rows'];
        $data['page_title'] = 'Search Member';
        $this->load->view('smartindo/member_search_so_v2',$data);
    }
}
?>
