<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Search_member extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MSearchMember'));
    }
    
    public function member(){
        $this->load->library(array('form_validation','pagination'));
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/search_member/member/'.$this->uri->segment(4);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(5))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        
        $data['from_rows'] = $this->uri->segment(5); //untuk no urut paging
        $config['total_rows'] = $this->MSearchMember->count_search_member($keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearchMember->search_member($keywords,$config['per_page'], $this->uri->segment(5));
        $data['page_title'] = 'Search Member';
        
        $this->load->view('search/member_v',$data);
    }
    
    public function stockiest(){
        $this->load->library(array('form_validation','pagination'));
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/search_member/member/'.$this->uri->segment(4);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(5))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        
        $data['from_rows'] = $this->uri->segment(5); //untuk no urut paging
        $config['total_rows'] = $this->MSearchMember->count_search_stockiest($keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSearchMember->search_stockiest($keywords,$config['per_page'], $this->uri->segment(5));
        $data['page_title'] = 'Search Stockiest';
        
        $this->load->view('search/member_v',$data);
    }
}
?>