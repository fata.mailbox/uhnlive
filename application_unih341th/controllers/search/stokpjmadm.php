<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stokpjmadm extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MSearchadmin'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/stokpjmadm/index/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            //if(!$this->uri->segment(4))
			$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        
        $data['from_rows'] = $this->uri->segment(6); //untuk no urut paging
        $config['total_rows'] = $this->MSearchadmin->countSearchTtpStock($keywords,$this->uri->segment(4,0));
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
        $data['results'] = $this->MSearchadmin->searchTtpStock($keywords,$config['per_page'],$this->uri->segment(6),$this->uri->segment(4,0));
        
        $data['page_title'] = 'Pinjaman Search';
        
        $this->load->view('search/pinjaman_admin_search',$data);
    }
    
}
?>