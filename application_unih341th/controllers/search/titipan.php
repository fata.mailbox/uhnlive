<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Titipan extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('Search_model'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/titipan/index/'.$this->uri->segment(4);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(5))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->countSearchTtpSO($keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->Search_model->searchTtpSO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['page_title'] = 'Titipan Search';
        
        $this->load->view('smartindo/titipanso_search',$data);
    }

    public function so_v(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/titipan/so_v/'.$this->uri->segment(4);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(5))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->countSearchTtpSO($keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->Search_model->searchTtpSO($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['page_title'] = 'Titipan Search';
        
        $this->load->view('smartindo/titipanso_search_v',$data);
    }

}
?>