<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Scsearch extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model('MSc');
		 $this->load->model(array('Search_model'));
    }
    
    public function item_sc_whs(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/scsearch/item_sc_whs/'.$this->uri->segment(4).'/'.$this->uri->segment(5).'/'.$this->uri->segment(6).'/'.$this->uri->segment(7).'/'.$this->uri->segment(8);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(5))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(9); //untuk no urut paging
        $config['total_rows'] = $this->MSc->count_search_stock_whs($this->uri->segment(4),$this->uri->segment(5));
		
        $config['per_page'] = 10;
        $config['uri_segment'] = 9;
		
        $this->pagination->initialize($config);
        
        //$data['results'] = $this->MSc->search_stock_whs(whs,keyword,flag,$num,$offset);
        $data['results'] = $this->MSc->search_stock_whs($this->uri->segment(4),$keywords,$this->uri->segment(6),$this->uri->segment(7),$this->uri->segment(9));
        
        $data['page_title'] = 'Item By Warehouse';
        $this->load->view('stockiest/search_itemtitipan',$data);
    }
	
	 public function member_search(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','Keywords','min_length[3]');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        
        $config['base_url'] = site_url().'search/scsearch/member_search/'.$this->uri->segment(4);
        $config['total_rows'] = $this->MSc->countSearchMember($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 5;
		
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $this->pagination->initialize($config);
        
		
        $data['results'] = $this->MSc->searchMember($keywords,'all',$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['totalrow'] = $config['total_rows'];
        $data['page_title'] = 'Search Stockist';
        $this->load->view('stockiest/search_stockiest',$data);
    }
	
	 public function member_sc(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','Keywords','min_length[3]');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        
        $config['base_url'] = site_url().'search/scsearch/member_sc/';
        $config['total_rows'] = $this->MSc->countSearchMember($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSc->searchMember($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['totalrow'] = $config['total_rows'];
        $data['page_title'] = 'Search Stockist';
        $this->load->view('stockiest/stockist_search_sc_admin',$data);
    }
	
	public function sc_city_search(){
		 $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/scsearch/sc_city_search/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSc->countSearchCity($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MSc->searchCity($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Stockiest City Search';
        
        $this->load->view('stockiest/search_sc_city',$data);
	}
	
	

}
?>