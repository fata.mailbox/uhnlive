<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Itemtopup extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
		$this->load->model(array('ValueTopup_model'));
	}

	public function index()
	{
		$data['items'] =  $this->ValueTopup_model->item_get();
		$data['page_title'] = 'Item Search';
		$this->load->view('search/itemtopup_search', $data);
	}
}

/* End of file itemtopup.php */
/* Location: ./application_unih341th/controllers/search/itemtopup.php */
