<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Search_rpt_cluster extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('Search_model'));
    }
    
    public function index(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		$reg = $this->uri->segment(10);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
		//Others
		if($cluster_id=='11') { 
			$data['results'] = $this->Search_model->searchStockiestSalesMtdOthers($reg,$tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		}else{
			$data['results'] = $this->Search_model->searchStockiestSalesMtd($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		}
        $data['page_title'] = 'Stockiest List  - '.$cluster;
        $this->load->view('smartindo/stockist_search_ro_rpt_cluster',$data);
    }
    
    public function nr(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchNewRecruit($tgl,$cluster_id,$havingoms);
        $data['results'] = $this->Search_model->searchNewRecruit($tglawal,$tglakhir,$cluster_id,$havingoms);
		
        $data['page_title'] = 'New Recruit List  - '.$cluster;
        $this->load->view('smartindo/nr_search_ro_rpt_cluster',$data);
    }
    public function qspmup(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['results'] = $this->Search_model->searchQspmup($tglawal,$tglakhir,$cluster_id);
		
        $data['page_title'] = 'Qualified SPM and Up List  - '.$cluster;
        $this->load->view('smartindo/qspmup_search_ro_rpt_cluster',$data);
    }
    public function stclastyear(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		$reg = $this->uri->segment(10);
	
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        //$data['results'] = $this->Search_model->searchStockiestSalesLastYear($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		if($cluster_id=='11') { 
			$data['results'] = $this->Search_model->searchStockiestSalesLastYearOthers($reg,$tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		}else{
			$data['results'] = $this->Search_model->searchStockiestSalesLastYear($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		}

		
        $data['page_title'] = 'Stockiest List  - '.$cluster;
        $this->load->view('smartindo/stockist_search_ro_rpt_cluster',$data);
    }
	
    public function other(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $type = $this->uri->segment(6);
        
        //echo '<br>'.$tgl;
        //echo '<br>'.$cluster_id;
        //echo '<br>'.$cluster;
        //echo '<br>'.$havingoms;
        //echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searchOtherSales($tglawal,$tglakhir,$type);
        
        $data['page_title'] = 'Other Sales List  - '.$type;
        $this->load->view('smartindo/othersales_search_ro_rpt_cluster',$data);
    }
    public function otherreg(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $type = $this->uri->segment(6);
        $region = $this->uri->segment(7);
        
        //echo '<br>'.$tgl;
        //echo '<br>'.$cluster_id;
        //echo '<br>'.$cluster;
        //echo '<br>'.$havingoms;
        //echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searchOtherSalesReg($tglawal,$tglakhir,$type,$region);
        
        $data['page_title'] = 'Other Sales List  - '.$type;
        $this->load->view('smartindo/othersalesreg_search_ro_rpt_cluster',$data);
    }
	
    public function indexso(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searchSOSalesMtd($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster',$data);
    }
	
    public function saleslastyear(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searchSOSalesLastYear($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster',$data);
    }
    public function nam(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searchnam($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster',$data);
    }
    public function oam(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searchoam($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster',$data);
    }
    public function newjenjang(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $jenjang = $this->uri->segment(9);
        $act = 'no';
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searchnew($tglawal,$tglakhir,$cluster_id,$jenjang,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster2',$data);
    }
    public function newspm(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searchnewspm($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster2',$data);
    }
    public function oldqspm(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searcholdqspm($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster2',$data);
    }
    public function newl(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searchnewl($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster2',$data);
    }
    public function oldql(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searcholdql($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster2',$data);
    }
    public function oldnql(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searcholdnql($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster2',$data);
    }
    public function newsl(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searchnewsl($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster2',$data);
    }
    public function oldqsl(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searcholdqsl($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster2',$data);
    }
    public function oldnqsl(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->Search_model->searcholdnqsl($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster2',$data);
    }
    public function newstar(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        //$data['results'] = $this->Search_model->searchnewsl($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster3',$data);
    }
    public function oldqstar(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        //$data['results'] = $this->Search_model->searcholdqsl($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster3',$data);
    }
    public function oldnqstar(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $cluster_id = $this->uri->segment(6);
        $cluster = $this->uri->segment(7);
        $havingoms = $this->uri->segment(8);
        $act = $this->uri->segment(9);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$act;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        //$data['results'] = $this->Search_model->searcholdnqsl($tglawal,$tglakhir,$cluster_id,$havingoms,$act);
		
        $data['page_title'] = 'Member List  - '.$cluster;
        $this->load->view('smartindo/member_search_so_rpt_cluster3',$data);
    }
}
?>