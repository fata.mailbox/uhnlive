<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stock extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('Search_model','GLobal_model','Search_model2','GLobal_model2'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/stock/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->count_search_stock_whs($this->uri->segment(4),$keywords,'so');
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->Search_model->search_stock_whs($this->uri->segment(4),$keywords,'so',$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['page_title'] = 'Stock Warehouse';
        
        $this->load->view('smartindo/stock_so_search',$data);
    }
    
    public function ro(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/stock/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->count_search_stock_whs($this->uri->segment(4),$keywords,'ro');
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->Search_model->search_stock_whs($this->uri->segment(4),$keywords,'ro',$config['per_page'],$this->uri->segment($config['uri_segment']));
        
	//$data['stc'] = $this->GLobal_model->get_ewallet_stc($this->session->userdata('r_member_id'));
	
        $data['page_title'] = 'Stock Warehouse';
        $this->load->view('smartindo/stock_ro_search',$data);
    }
	
	public function rostc(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/stock/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->count_search_stock_whs($this->uri->segment(4),$keywords,'ro');
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->Search_model->search_stock_whs($this->uri->segment(4),$keywords,'ro',$config['per_page'],$this->uri->segment($config['uri_segment']));
        
	//$data['stc'] = $this->GLobal_model->get_ewallet_stc($this->session->userdata('r_member_id'));
	
        $data['page_title'] = 'Stock Warehouse';
        $this->load->view('smartindo/stock_rostc_search',$data);
    }
    public function so_v(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/stock/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->count_search_stock_whs($this->uri->segment(4),$keywords,'so');
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->Search_model->search_stock_whs($this->uri->segment(4),$keywords,'so',$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['page_title'] = 'Stock Warehouse V';
        
        $this->load->view('smartindo/stock_so_search_v',$data);
    }
	
    public function ro_v(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/stock/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->count_search_stock_whs($this->uri->segment(4),$keywords,'ro');
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->Search_model->search_stock_whs($this->uri->segment(4),$keywords,'ro',$config['per_page'],$this->uri->segment($config['uri_segment']));
        
	//$data['stc'] = $this->GLobal_model->get_ewallet_stc($this->session->userdata('r_member_id'));
	
        $data['page_title'] = 'Stock Warehouse';
        $this->load->view('smartindo/stock_ro_search_v',$data);
    }
	
	public function rostc_v(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/stock/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->count_search_stock_whs($this->uri->segment(4),$keywords,'ro');
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->Search_model->search_stock_whs($this->uri->segment(4),$keywords,'ro',$config['per_page'],$this->uri->segment($config['uri_segment']));
        
	//$data['stc'] = $this->GLobal_model->get_ewallet_stc($this->session->userdata('r_member_id'));
	
        $data['page_title'] = 'Stock Warehouse';
        $this->load->view('smartindo/stock_rostc_search_v',$data);
    }
	
    public function ro_vwh(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/stock/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->count_search_stock_whs_vwh($this->uri->segment(4),$keywords,'ro');
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
        //echo $this->uri->segment($config['uri_segment']);
        //$data['results'] = $this->Search_model->search_stock_whs($this->uri->segment(4),$keywords,'ro',$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['results'] = $this->Search_model->search_stock_whs_vwh($this->uri->segment(4),$keywords,'ro',$config['per_page'],$this->uri->segment($config['uri_segment']));
        
	//$data['stc'] = $this->GLobal_model->get_ewallet_stc($this->session->userdata('r_member_id'));
	
        $data['page_title'] = 'Stock Warehouse';
        $this->load->view('smartindo/stock_ro_search_vwh',$data);
    }
	
	public function rostc_vwh(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/stock/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->count_search_stock_whs_vwh($this->uri->segment(4),$keywords,'ro');
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
        
        //$data['results'] = $this->Search_model->search_stock_whs($this->uri->segment(4),$keywords,'ro',$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['results'] = $this->Search_model->search_stock_whs_vwh($this->uri->segment(4),$keywords,'ro',$config['per_page'],$this->uri->segment($config['uri_segment']));
        
	//$data['stc'] = $this->GLobal_model->get_ewallet_stc($this->session->userdata('r_member_id'));
	
        $data['page_title'] = 'Stock Warehouse';
        $this->load->view('smartindo/stock_rostc_search_vwh',$data);
    }
	
	//START AND 20190117
	public function so_v2(){
        $this->load->library(array('form_validation','pagination'));

        $this->form_validation->set_rules('search','','');

        $config['base_url'] = site_url().'search/stock/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }

        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model2->count_search_stock_whs($this->uri->segment(4),$keywords,'so');
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);

        $data['results'] = $this->Search_model2->search_stock_whs($this->uri->segment(4),$keywords,'so',$config['per_page'],$this->uri->segment($config['uri_segment']));
        $data['page_title'] = 'Stock Warehouse';

          $this->load->view('smartindo/stock_so_search_v2',$data);
    }
	//END AND 20190117
}
?>