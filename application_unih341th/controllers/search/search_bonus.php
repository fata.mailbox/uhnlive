<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Search_bonus extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('MBonus'));
    }
    
    public function index(){
        $tglawal = $this->uri->segment(4);
        $tglakhir = $this->uri->segment(5);
        $member_id = $this->uri->segment(6);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$tglawal;
		//echo '<br>'.$tglakhir;
		//echo '<br>'.$member_id;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->MBonus->getBonusDetailPerMonth($tglawal,$tglakhir,$member_id);
		
		
        $data['page_title'] = 'Akumulasi Bonus  ('.date("F Y",strtotime($tglawal)).' - '.date("F Y",strtotime($tglakhir)).')';
        $this->load->view('smartindo/search_detail_bonus',$data);
    }
    public function detail(){
        $tgl = $this->uri->segment(4);
        $member_id = $this->uri->segment(5);
		
		//echo '<br>'.$tgl;
		//echo '<br>'.$cluster_id;
		//echo '<br>'.$cluster;
		//echo '<br>'.$havingoms;
		//echo '<br>'.$tglawal;
		//echo '<br>'.$tglakhir;
		//echo '<br>'.$member_id;
        //$data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        //$data['results'] = $this->Search_model->searchStockiestSalesMtd($tgl,$cluster_id,$havingoms,$act);
        $data['results'] = $this->MBonus->getBonusDetailPerMonthDet($tgl,$member_id);
		
		
        $data['page_title'] = 'Detail Bonus  ('.date("F Y",strtotime($tgl)).')';
        $this->load->view('smartindo/search_detail_bonus_permonth',$data);
    }
}
?>