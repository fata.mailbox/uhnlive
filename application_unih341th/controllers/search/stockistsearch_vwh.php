<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stockistsearch_vwh extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('Search_model'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','Keywords','min_length[3]');
        
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        
        $config['base_url'] = site_url().'search/stockistsearch_vwh/index/';
        $config['total_rows'] = $this->Search_model->countStockiestAct($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $this->pagination->initialize($config);
        
        $data['results'] = $this->Search_model->searchStockiestAct($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['totalrow'] = $config['total_rows'];
        $data['page_title'] = 'Search Stockist';
        $this->load->view('smartindo/stockist_search_ro_admin_vwh',$data);
    }
    
    public function xxx(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','Keywords','min_length[3]');
        
        $config['base_url'] = site_url().'search/membersearch/ewalletso/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(4))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(5); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->countMember($keywords);
        $config['per_page'] = 20;
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->Search_model->searchMemberSo($keywords,$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'Member Search';
        
        $this->load->view('smartindo/member_ewallet_search_so',$data);
    }
    
}
?>