<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Voucher extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }
        
        $this->load->model(array('Search_model','GLobal_model'));
    }
    
    public function index(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/voucher/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->count_search_voucher_so($this->uri->segment(4),$keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->Search_model->search_voucher_so($this->uri->segment(4),$keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
		
		$data['prosestambahan'] = "";
        $data['page_title'] = 'E-Wallet Voucher';
        $data['typevouchersearch'] = '0';
        $this->load->view('smartindo/voucher_search',$data);
    }
    
    public function ro(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/voucher/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->count_search_voucher_ro($this->uri->segment(4),$keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->Search_model->search_voucher_ro($this->uri->segment(4),$keywords,'ro',$config['per_page'],$this->uri->segment($config['uri_segment']));
        
	//$data['stc'] = $this->GLobal_model->get_ewallet_stc($this->session->userdata('r_member_id'));
		$data['prosestambahan'] = "
		window.opener.document.form.totalrpdiskon.value=totaldiskon_curr(".$this->session->userdata('counti').",'window.opener.document.form.subrpdiskon');
                    totalbayardiskon(window.opener.document.form.total, window.opener.document.form.totalrpdiskon, window.opener.document.form.totalbayar);
		";
        $data['page_title'] = 'E-Wallet Voucher';
        $data['typevouchersearch'] = '1';
        $this->load->view('smartindo/voucher_search',$data);
    }
	
	public function rostc(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/stock/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->count_search_stock_whs($this->uri->segment(4),$keywords,'ro');
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->Search_model->search_stock_whs($this->uri->segment(4),$keywords,'ro',$config['per_page'],$this->uri->segment($config['uri_segment']));
        
	//$data['stc'] = $this->GLobal_model->get_ewallet_stc($this->session->userdata('r_member_id'));
	
        $data['page_title'] = 'E-Wallet Voucher';
        $this->load->view('smartindo/stock_rostc_search',$data);
    }
	
    public function sostc(){
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','','');
        
        $config['base_url'] = site_url().'search/voucher/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(6))$this->session->unset_userdata('keywords');
        }
        
        $keywords = $this->session->userdata('keywords');
        $config['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging
        $config['total_rows'] = $this->Search_model->count_search_voucher_sostc($this->uri->segment(4), $this->session->userdata('userid'),$keywords);
        $config['per_page'] = 10;
        $config['uri_segment'] = 6;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->Search_model->search_voucher_sostc($this->uri->segment(4), $this->session->userdata('userid'),$keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
		
		$data['prosestambahan'] = "";
        $data['page_title'] = 'E-Wallet Voucher';
        $data['typevouchersearch'] = '0';

        $this->load->view('smartindo/voucher_search',$data);
    }
}
?>