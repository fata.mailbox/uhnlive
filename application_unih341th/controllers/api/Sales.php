<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101) {
			redirect('');
		}
		$this->load->model(array('MMenu', 'esales_model', 'getro_model', 'salesso_model', 'getso_model', 'esales_scmodel', 'esales_scpaymentmodel', 'esales_screturmodel', 'esales_roreturmodel', 'esales_scadminmodel', 'esales_scadminreturmodel', 'esales_cancelbilling','esales_cancelbillingso','esales_getsomodel'));
	}
	public function index()
	{

		$data['page_title'] = 'Interface E-Sales';
		$this->load->view('api/sales/sales', $data);
	}
	public function get_data_sc()
	{

		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$list = $this->esales_scmodel->get_datatables($status, $from, $to);
		$data = array();
		$i = 1;
		$act = 'SC';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$row = array();
			$tgl = str_replace('.', '-', $field['U_UHINV']);
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_SODAT'];
			if ($field['status'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			}else if ($field['status'] == 'N') {
				$row[] = 'New Data Transaction';
			} else if ($field['status'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['status'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = "<button onclick=Jejak('$tgl','$act') type='button' class='btn btn-sm btn-primary'>Detail</a>";
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->esales_scmodel->count_all($status, $from, $to),
			"recordsFiltered" => $this->esales_scmodel->count_filtered($status, $from, $to),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}





	public function get_data_scpayment()
	{

		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$list = $this->esales_scpaymentmodel->get_datatables($status, $from, $to);
		$data = array();
		$i = 1;
		$act = 'SCPayment';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$row = array();
			$tgl = str_replace('.', '-', $field['U_UHINV']);
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_SODAT'];
			if ($field['status'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			}else if ($field['status'] == 'N') {
				$row[] = 'New Data Transaction';
			} else if ($field['status'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['status'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = "<button onclick=Jejak('$tgl','$act') type='button' class='btn btn-sm btn-primary'>Detail</a>";
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->esales_scpaymentmodel->count_all($status, $from, $to),
			"recordsFiltered" => $this->esales_scpaymentmodel->count_filtered($status, $from, $to),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}










	public function get_data_roretur()
	{

		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$list = $this->esales_roreturmodel->get_datatables($status, $from, $to);
		$data = array();
		$i = 1;
		$act = 'RORetur';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$row = array();
			$tgl = str_replace('.', '-', $field['U_UHINV']);
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_SODAT'];
			if ($field['status'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			}else if ($field['status'] == 'N') {
				$row[] = 'New Data Transaction';
			} else if ($field['status'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['status'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = "<button onclick=Jejak('$tgl','$act') type='button' class='btn btn-sm btn-primary'>Detail</a>";
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->esales_roreturmodel->count_all($status, $from, $to),
			"recordsFiltered" => $this->esales_roreturmodel->count_filtered($status, $from, $to),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}



	public function get_data_scretur()
	{

		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$list = $this->esales_screturmodel->get_datatables($status, $from, $to);
		$data = array();
		$i = 1;
		$act = 'SCRetur';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$row = array();
			$tgl = str_replace('.', '-', $field['U_UHINV']);
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_SODAT'];
			if ($field['status'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			}else if ($field['status'] == 'N') {
				$row[] = 'New Data Transaction';
			} else if ($field['status'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['status'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = "<button onclick=Jejak('$tgl','$act') type='button' class='btn btn-sm btn-primary'>Detail</a>";
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->esales_screturmodel->count_all($status, $from, $to),
			"recordsFiltered" => $this->esales_screturmodel->count_filtered($status, $from, $to),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}






	public function get_data_scadmin()
	{

		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$list = $this->esales_scadminmodel->get_datatables($status, $from, $to);
		$data = array();
		$i = 1;
		$act = 'ScAdmin';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$row = array();
			$tgl = str_replace('.', '-', $field['U_UHINV']);
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_SODAT'];
			if ($field['status'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			}else if ($field['status'] == 'N') {
				$row[] = 'New Data Transaction';
			} else if ($field['status'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['status'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = "<button onclick=Jejak('$tgl','$act') type='button' class='btn btn-sm btn-primary'>Detail</a>";
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->esales_scadminmodel->count_all($status, $from, $to),
			"recordsFiltered" => $this->esales_scadminmodel->count_filtered($status, $from, $to),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}


	//tambah waktu pada sc

	public function get_data_scadminretur()
	{

		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$list = $this->esales_scadminreturmodel->get_datatables($status, $from, $to);
		$data = array();
		$i = 1;
		$act = 'RequestOrder';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$row = array();
			$tgl = str_replace('.', '-', $field['U_UHINV']);
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_SODAT'];
			if ($field['status'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			}else if ($field['status'] == 'N') {
				$row[] = 'New Data Transaction';
			} else if ($field['status'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['status'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = "<button onclick=Jejak('$tgl','$act') type='button' class='btn btn-sm btn-primary'>Detail</a>";
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->esales_scadminreturmodel->count_all($status, $from, $to),
			"recordsFiltered" => $this->esales_scadminreturmodel->count_filtered($status, $from, $to),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}








	public function get_data_getro()
	{
		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		

		$list = $this->esales_model->get_datatables($status, $from, $to);
		$data = array();
		$i = 1;
		$act = 'RequestOrder';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$row = array();
			$tgl = str_replace('.', '-', $field['U_UHINV']);
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_SODAT'];
			if ($field['status'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			}else if ($field['status'] == 'N') {
				$row[] = 'New Data Transaction';
			} else if ($field['status'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['status'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = "<button onclick=Jejak('$tgl','$act') type='button' class='btn btn-sm btn-primary'>Detail</a>";
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->esales_model->count_all($status, $from, $to),
			"recordsFiltered" => $this->esales_model->count_filtered($status, $from, $to),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}



	public function get_data_cancel()
	{
		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');
        $source = $this->input->post('source');
		$list = $this->esales_cancelbilling->get_datatables($status, $from, $to,$source);
		$data = array();
		$i = 1;
		$act = 'CancelBilling';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$row = array();
			
			$tgl = str_replace('.', '-', $field['U_UHINV']);
            $row[] = $field['U_UHINV'];
            $ref = $field['U_REF_CNCL_AUART'];
            
            if ($ref == 'ZU02') {
                $flag = "SalesOrder";
                $row[] = "Sales Order";
            }else{
                $flag = "RequestOrder";
                $row[] = "Resuest Order";
            }

			
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_SODAT'];
		if ($field['status'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			}else if ($field['status'] == 'N') {
				$row[] = 'New Data Transaction';
			} else if ($field['status'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['status'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = "<button onclick=Jejak('$tgl','$act','$flag') type='button' class='btn btn-sm btn-primary'>Detail</a>";
			$data[] = $row;
			$i++;
		}

		$recordstotal = $this->esales_cancelbilling->count_all($status, $from, $to,$source);
		$recordsfilter = $this->esales_cancelbilling->count_filtered($status, $from,$to,$source);
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $recordstotal,
			"recordsFiltered" => $recordsfilter,
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}



	public function get_data_getso()
	{
		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$list = $this->salesso_model->get_datatables($status, $from, $to);
		$data = array();
		$i = 1;
		$act = 'SalesOrder';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$row = array();
			$tgl = str_replace('.', '-', $field['U_UHINV']);
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_UHINV'];
			$row[] = $field['U_SODAT'];
			if ($field['status'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			}else if ($field['status'] == 'N') {
				$row[] = 'New Data Transaction';
			} else if ($field['status'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['status'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = "<button onclick=Jejak('$tgl','$act') type='button' class='btn btn-sm btn-primary'>Detail</a>";
			$data[] = $row;
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->salesso_model->count_all($status, $from, $to),
			"recordsFiltered" => $this->salesso_model->count_filtered($status, $from, $to),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}






	public function get_data_user()
	{
		$id = $this->input->post('id');
		$act = $this->input->post('act');
        $param = $this->input->post('param');
		if ($act == 'RequestOrder' ||  $act == 'SC' || $act == 'SCPayment' || $act == 'SCRetur' || $act == 'RORetur' || $act == 'ScAdmin' || $act == 'ScAdminRetur' || $act == 'SalesOrder') {
			$list = $this->getro_model->get_datatables($id, $act,$param);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$row = array();
				$row[] = $field['U_AUART'];
				$row[] = $field['U_UHINV'];
				$row[] = $field['U_POSNR'];
				$row[] = $field['U_SUBNR'];
				$row[] = $field['U_SODAT'];
				$row[] = $field['U_KUNNR'];
				$row[] = $field['U_CTYPE'];
				$row[] = $field['U_PL_WE'];
				$row[] = $field['U_TAXCLASS'];
				$row[] = $field['VKBUR'];
				$row[] = $field['WERKS'];
				$row[] = $field['MATNR'];
				$row[] = $field['ARKTX'];
				$row[] = $field['MATKL'];
				$row[] = $field['CHARG'];
				$row[] = $field['KWMENG'];
				$row[] = $field['COGS'];
				$row[] = $field['ZUHN'];
				$row[] = $field['ZUA1'];
				$row[] = $field['ZUA2'];
				$row[] = $field['ZUA4'];
				$row[] = $field['U_VCDNO'];
				$row[] = $field['U_FGOOD'];
				$row[] = $field['PV'];
				$row[] = $field['BV'];
				$row[] = $field['YYBANDCD'];
				$row[] = $field['EWALLET_HDR'];
				$row[] = $field['EWALLET_ITM'];
				$row[] = $field['ITEM_TYPE'];
				$row[] = $field['WAERS'];
				$row[] = $field['AUGRU'];
				$row[] = $field['U_REF_CNCL_AUART'];
				$row[] = $field['U_REF_CNCL_INV'];
				$row[] = $field['U_REF_CNCL_SUBNR'];
				
				$row[] = $field['UHDAT'];
				$row[] = $field['UHZET'];

			if ($field['status'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			}else if ($field['status'] == 'N') {
				$row[] = 'New Data Transaction';
			} else if ($field['status'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['status'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
				$row[] = $field['description'];
				$row[] = '';
				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->getro_model->count_all($id, $act,$param),
				"recordsFiltered" => $this->getro_model->count_filtered($id, $act,$param),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}else if($act == 'CancelBilling'){
		    $list = $this->getro_model->get_datatables($id, $act,$param);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$row = array();
				$row[] = $field['U_AUART'];
				$row[] = $field['U_UHINV'];
				$row[] = $field['U_POSNR'];
				$row[] = $field['U_SUBNR'];
				$row[] = $field['U_SODAT'];
				$row[] = $field['U_KUNNR'];
				$row[] = $field['U_CTYPE'];
				$row[] = $field['U_PL_WE'];
				$row[] = $field['U_TAXCLASS'];
				$row[] = $field['VKBUR'];
				$row[] = $field['WERKS'];
				$row[] = $field['MATNR'];
				$row[] = $field['ARKTX'];
				$row[] = $field['MATKL'];
				$row[] = $field['CHARG'];
				$row[] = $field['KWMENG'];
				$row[] = $field['COGS'];
				$row[] = $field['ZUHN'];
				$row[] = $field['ZUA1'];
				$row[] = $field['ZUA2'];
				$row[] = $field['ZUA4'];
				$row[] = $field['U_VCDNO'];
				$row[] = $field['U_FGOOD'];
				$row[] = $field['PV'];
				$row[] = $field['BV'];
				$row[] = $field['YYBANDCD'];
				$row[] = $field['EWALLET_HDR'];
				$row[] = $field['EWALLET_ITM'];
				$row[] = $field['ITEM_TYPE'];
				$row[] = $field['WAERS'];
				$row[] = $field['AUGRU'];
				$row[] = $field['U_REF_CNCL_AUART'];
				$row[] = $field['U_REF_CNCL_INV'];
				$row[] = $field['U_REF_CNCL_SUBNR'];
				
				$row[] = $field['UHDAT'];
				$row[] = $field['UHZET'];

			if ($field['status'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			}else if ($field['status'] == 'N') {
				$row[] = 'New Data Transaction';
			} else if ($field['status'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['status'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
				$row[] = $field['description'];
				$row[] = '';
				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->getro_model->count_all($id, $act,$param),
				"recordsFiltered" => $this->getro_model->count_filtered($id, $act,$param),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		    
		} else {
			$list = $this->getso_model->get_datatables($id);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$row = array();
				$row[] = $field['U_AUART'];
				$row[] = $field['U_UHINV'];
				$row[] = $field['U_POSNR'];
				$row[] = $field['U_SUBNR'];
				$row[] = $field['U_SODAT'];
				$row[] = $field['U_KUNNR'];
				$row[] = $field['U_CTYPE'];
				$row[] = $field['U_PL_WE'];
				$row[] = $field['U_TAXCLASS'];
				$row[] = $field['VKBUR'];
				$row[] = $field['WERKS'];
				$row[] = $field['MATNR'];
				$row[] = $field['ARKTX'];
				$row[] = $field['MATKL'];
				$row[] = $field['CHARG'];
				$row[] = $field['KWMENG'];
				$row[] = $field['COGS'];
				$row[] = $field['ZUHN'];
				$row[] = $field['ZUA1'];
				$row[] = $field['ZUA2'];
				$row[] = $field['ZUA4'];
				$row[] = $field['U_VCDNO'];
				$row[] = $field['U_FGOOD'];
				$row[] = $field['PV'];
				$row[] = $field['BV'];
				$row[] = $field['YYBANDCD'];
				$row[] = $field['EWALLET_HDR'];
				$row[] = $field['EWALLET_ITM'];
				$row[] = $field['ITEM_TYPE'];
				$row[] = $field['WAERS'];
				$row[] = $field['AUGRU'];
				$row[] = $field['U_REF_CNCL_AUART'];
				$row[] = $field['U_REF_CNCL_INV'];
				$row[] = $field['U_REF_CNCL_SUBNR'];
			if ($field['status'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			}else if ($field['status'] == 'N') {
				$row[] = 'New Data Transaction';
			} else if ($field['status'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['status'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
				$row[] = $field['description'];
				$row[] = '';
				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->getso_model->count_all($id),
				"recordsFiltered" => $this->getso_model->count_filtered($id),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}
	}
}

/* End of file Sales.php */
/* Location: ./application/controllers/Sales.php */
