<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Material extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
		    redirect('');
		}
		$this->load->model(array('MMenu','topupbydemand_model','ncm_model','adjrsk_model','adjmove_model','whsfrom_model','whsto_model','sloc_model'));
	}

	public function index()
	{
		$data['page_title'] = 'Interface E-Material';
		$this->load->view('api/material/material', $data);
	}


	
	public function get_data_ncm()
	{
		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$list = $this->ncm_model->get_datatables($status,$from,$to);
		$data = array();
		$i = 1;
		$act = 'NCM';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$row = array();
			$tgl =  $field['YMBLNR_UHN'];
			$row[] = $field['YMBLNR_UHN'];
			$row[] = $field['YMBLNR_UHN'];
			$row[] = $field['BUDAT_MKPF'];
		
				if ($field['STATUS'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			} else if ($field['STATUS'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['STATUS'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = "<button onclick=Jejak('$tgl','$act') type='button' class='btn btn-sm btn-primary'>Detail</a>";
			$data[] = $row;
			$i++;
		}
	
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->ncm_model->count_all($status,$from,$to),
			"recordsFiltered" => $this->ncm_model->count_filtered($status,$from,$to),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);

	}


	public function get_data_whsfrom()
	{
		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');	
		$list = $this->whsfrom_model->get_datatables($status,$from,$to);
		$data = array();
		$i = 1;
		$act = 'WHSFROM';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$tgl = $field['YMBLNR_UHN'];
			$row = array();
			$row[] = $field['YMBLNR_UHN'];
			$row[] = $field['YMBLNR_UHN'];
			$row[] = $field['BUDAT_MKPF'];
		
				if ($field['STATUS'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			} else if ($field['STATUS'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['STATUS'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = "<button onclick=Jejak('$tgl','$act') class='btn btn-sm btn-primary'>Detail</button>";
			$data[] = $row;
			$i++;
		}
	
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->whsfrom_model->count_all($status,$from,$to),
			"recordsFiltered" => $this->whsfrom_model->count_filtered($status,$from,$to),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);


	}



	public function get_data_whsto()
	{
		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');	
		$list = $this->whsto_model->get_datatables($status,$from,$to);
		$data = array();
		$i = 1;
		$act = 'WHSFROM';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$tgl = $field['YMBLNR_UHN'];
			$row = array();
			$row[] = $field['YMBLNR_UHN'];
			$row[] = $field['YMBLNR_UHN'];
			$row[] = $field['BUDAT_MKPF'];
			$row[] = $field['STATUS'];
			$row[] = "<button onclick=Jejak('$tgl','$act') class='btn btn-sm btn-primary'>Detail</button>";
			$data[] = $row;
			$i++;
		}
	
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->whsto_model->count_all($status,$from,$to),
			"recordsFiltered" => $this->whsto_model->count_filtered($status,$from,$to),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);

	}

	public function get_data_adjrsk()
	{
		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');	
		$list = $this->adjrsk_model->get_datatables($status,$from,$to);
		$data = array();
		$i = 1;
		$act = 'ADJRUKSAK';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$tgl = $field['YMBLNR_UHN'];
			$row = array();
			$row[] = $field['YMBLNR_UHN'];
			$row[] = $field['YMBLNR_UHN'];
			$row[] = $field['BUDAT_MKPF'];
		
				if ($field['STATUS'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			} else if ($field['STATUS'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['STATUS'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = "<button onclick=Jejak('$tgl','$act') class='btn btn-sm btn-primary'>Detail</button>";
			$data[] = $row;
			$i++;
		}
	
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->adjrsk_model->count_all($status,$from,$to),
			"recordsFiltered" => $this->adjrsk_model->count_filtered($status,$from,$to),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);

	}


	
	public function get_data_adjmove()
	{
		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');	
		$list = $this->adjmove_model->get_datatables($status,$from,$to);
		$data = array();
		$i = 1;
		$act = 'ADJMOVE';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$tgl = $field['YMBLNR_UHN'];
			$row = array();
			$row[] = $field['YMBLNR_UHN'];
			$row[] = $field['YMBLNR_UHN'];
			$row[] = $field['BUDAT_MKPF'];
	
	
				if ($field['STATUS'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			} else if ($field['STATUS'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['STATUS'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
	
			$row[] = "<button onclick=Jejak('$tgl','$act') class='btn btn-sm btn-primary'>Detail</button>";
			$data[] = $row;
			$i++;
		}
	
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->adjmove_model->count_all($status,$from,$to),
			"recordsFiltered" => $this->adjmove_model->count_filtered($status,$from,$to),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);

	}



	
	public function get_data_sloc()
	{
		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');	
		$list = $this->sloc_model->get_datatables($status,$from,$to);
		$data = array();
		$i = 1;
		$act = 'SLOC';
		$no = $_POST['start'];
		foreach ($list as $field) {
			$tgl = $field['YMBLNR_UHN'];
			$row = array();
			$row[] = $field['YMBLNR_UHN'];
			$row[] = $field['YMBLNR_UHN'];
			$row[] = $field['BUDAT_MKPF'];
			
				if ($field['STATUS'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			} else if ($field['STATUS'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['STATUS'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = "<button onclick=Jejak('$tgl','$act') class='btn btn-sm btn-primary'>Detail</button>";
			$data[] = $row;
			$i++;
		}
	
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->sloc_model->count_all($status,$from,$to),
			"recordsFiltered" => $this->sloc_model->count_filtered($status,$from,$to),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);

	}




    public function get_data_user()
	{
		$id = $this->input->post('id');
		$act = $this->input->post('act');
		if ($act == 'ADJRUKSAK') {
			$ct = 'ADJ RUSAK';
		}else if ($act == 'ADJMOVE') {
			$ct = 'ADJ MOVE';
		}else if($act == 'WHSFROM'){
			$ct =  'WHS From';
		}else if($act == 'WHSTO' ){
			$ct =  'WHS To';
		}
		else {
			$ct = $act;
		}
	
		$list = $this->topupbydemand_model->get_datatables($id,$ct);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
		
			$row = array();
			$row[] = $field['EKGRP'];
			$row[] = $field['YMBLNR_UHN'];
			$row[] = $field['YID_UHN'];
			$row[] = $field['YMJAHR_UHN'];
			$row[] = $field['BWART'];
			$row[] = $field['WERKS'];
			$row[] = $field['LGORT'];
			$row[] = $field['MATNR'];
			$row[] = $field['ERFMG'];
			$row[] = $field['ERFME'];
			$row[] = $field['CHARG'];
			$row[] = $field['VFDAT'];
			$row[] = $field['KOSTL'];
			$row[] = $field['PRCTR'];
			$row[] = $field['BUDAT_MKPF'];
			$row[] = $field['CPUTM_MKPF'];
			$row[] = $field['USNAM_MKPF'];
			$row[] = $field['YRETURN_IND'];
			$row[] = $field['WAERS'];
			$row[] = $field['SAKTO'];
			$row[] = $field['DMBTR'];
			$row[] = $field['YZEILE_UHN'];
		if ($field['STATUS'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			} else if ($field['STATUS'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['STATUS'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = $field['description'];
            $row[] = '';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->topupbydemand_model->count_all($id,$ct),
			"recordsFiltered" => $this->topupbydemand_model->count_filtered($id,$ct),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);

	}


    
}

/* End of file Material.php */
/* Location: ./application/controllers/Material.php */
