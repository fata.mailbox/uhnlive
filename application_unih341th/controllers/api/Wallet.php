<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Wallet extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') == 101){
		    redirect('');
		}

		$this->load->model(array('MMenu','wallet_model','data_wallet_model'));
	}
    public function index()
    {
		$data['page_title'] = 'Interface E-Wallet';
		$wallet = "select distinct(doc_head),status,doc_date FROM temp_ewallet ";
		$data['wallet'] = $this->db->query($wallet)->result_array();
		$this->load->view('api/wallet/wallet', $data);
	}


	public function get_data_wallet($param)
	{
		$status = $this->input->post('status');
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		
		$list = $this->data_wallet_model->get_datatables($param,$status,$from,$to);
		$data = array();
		$i = 1;
		$no = $_POST['start'];
		foreach ($list as $field) {
			$row = array();
			$tgl =  $field['doc_head'];
			$row[] = $field['doc_head'];
			$row[] = $field['doc_head'];
			$row[] = $field['doc_date'];
			
			
			if ($field['status'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			}else if ($field['status'] == 'N') {
				$row[] = 'New Data Transaction';
			} else if ($field['status'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['status'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = "<button onclick=Jejak('$tgl','$param') type='button' class='btn btn-sm btn-primary'>Detail</a>";
			$data[] = $row;
			$i++;
		}
	
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->data_wallet_model->count_all($param,$status,$from,$to),
			"recordsFiltered" => $this->data_wallet_model->count_filtered($param,$status,$from,$to),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}


	

	public function get_data_user()
	{
		$id = $this->input->post('id');
		$list = $this->wallet_model->get_datatables($id);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$row = array();
		
			$row[] = $field['doc_head'];
			$row[] = $field['doc_det'];
			$row[] = $field['cat_cust'];
			$row[] = $field['doc_date'];
			$row[] = $field['doc_post'];
			$row[] = $field['kunnr'];
			$row[] = $field['name_cus'];
			$row[] = $field['deposit_id'];
			$row[] = $field['withdrawal_id'];
			$row[] = $field['cat_doc'];
			$row[] = $field['WERKS'];
			$row[] = $field['bank1'];
			$row[] = $field['pay_meth'];
			$row[] = $field['cre_deb'];
			$row[] = $field['PPh_21'];
			$row[] = $field['WEB_DMBTR_COR'];
			$row[] = $field['web_dmbtr'];
			$row[] = $field['remark'];
			$row[] = $field['web_CPUDT'];
			$row[] = $field['web_CPUTM'];
			if ($field['status'] == 'S') {
				$row[] = 'SUCCESS SEND DATA';
			}else if ($field['status'] == 'N') {
				$row[] = 'New Data Transaction';
			} else if ($field['status'] == 'E') {
				$row[] = 'ERROR SEND DATA';
			} else if ($field['status'] == 'W') {
				$row[] = 'WARNING SEND DATA';
			} else {
				$row[] = $field['status'];
			}
			$row[] = $field['description'];
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->wallet_model->count_all($id),
			"recordsFiltered" => $this->wallet_model->count_filtered($id),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);

	}



}

/* End of file Controllername.php */
