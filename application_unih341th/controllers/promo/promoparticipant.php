<?php
// INSERT INTO `menu`(`id`,`submenu_id`,`title`,`folder`,`url`,`comment`,`sort`)VALUES(NULL,'12','Promo trip 2012','promo','prm1201','','5');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Promoparticipant extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){ 
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MPromosponsor'));
    }
	public function index(){
		
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
			redirect('error','refresh');
		}
		
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('promoid','PromoID','required');
        $this->form_validation->set_rules('search','Keywords','min_length[3]');
        
        $config['base_url'] = site_url().'promo/promoparticipant/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('promoid',$this->db->escape_str($this->input->post('promoid')));
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(2))$this->session->unset_userdata('keywords');
        }
		
        $promoid = $this->session->userdata('promoid');
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(3); //untuk no urut paging
        $config['total_rows'] = $this->MPromosponsor->countPromoParticipant($promoid,$keywords);
        $config['per_page'] = 20;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
		$data['countResults'] = $config['total_rows'];
        $data['results'] = $this->MPromosponsor->getPromoParticipantList($promoid,$keywords,$config['per_page'],$data['from_rows']);
		
		$data['promolist'] = $this->MPromosponsor->getAllPromoList();
		$data['page_title'] = 'Promo Sponsoring Participant';
		$data['urlform'] = 'promo/promoparticipant';
        $this->load->view('promo/promoparticipant',$data);
	}
}
?>