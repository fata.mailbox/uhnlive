<?php 
// INSERT INTO `menu`(`id`,`submenu_id`,`title`,`folder`,`url`,`comment`,`sort`)VALUES(NULL,'12','Promo trip 2012','promo','prm1201','','5');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class prm1201 extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){ 
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MPromo12'));
    }
    
    public function index(){
		
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
			redirect('error','refresh');
		}
		
		$this->load->library(array('form_validation'));

		$this->form_validation->set_rules('member_id','','');
		$this->form_validation->set_rules('name','','');

		if($this->form_validation->run()){
			if($this->session->userdata('userid')>100){
				$id = $this->session->userdata('userid');
			}else{
				$id = $this->input->post('member_id');
			}
			$row = array();
			$data["member"] = $this->MPromo12->getDataPribadi($id);
			$data["dl"] = $this->MPromo12->getDetail($id);
			//$row["uig"] = $this->MPromo->uigResult2($id);
			//$data["results"] = $this->MPromo->uigDet2($id);
        }else{
			$row["uig"] = false;
			$data["results"] = false;
		}
		$data['row'] = $row;
		
		$data['page_title'] = 'Leadership Award';
		$data['urlform'] = 'promo/prm1201';
        $this->load->view('promo/prm1201',$data);
	}
}
?>