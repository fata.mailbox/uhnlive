<?php 
// INSERT INTO `menu`(`id`,`submenu_id`,`title`,`folder`,`url`,`comment`,`sort`)VALUES(NULL,'12','Promo trip 2012','promo','prm1201','','5');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Promosponsoringmaster extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){ 
            redirect('','refresh');
        }
        $this->load->model(array('MMenu','MPromosponsor'));
    }
	public function index(){
		
		if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
			redirect('error','refresh');
		}
		
        $this->load->library(array('form_validation','pagination'));
        
        $this->form_validation->set_rules('search','Keywords','min_length[3]');
        
        $config['base_url'] = site_url().'promo/promosponsoringmaster/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(2))$this->session->unset_userdata('keywords');
        }
		
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(3); //untuk no urut paging
        $config['total_rows'] = $this->MPromosponsor->countPromo($keywords);
        $config['per_page'] = 20;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        $data['results'] = $this->MPromosponsor->getPromoList($keywords,$config['per_page'],$data['from_rows']);
		
		$data['page_title'] = 'Promo Sponsoring Master';
		$data['urlform'] = 'promo/promosponsoringmaster';
        $this->load->view('promo/promosponsoringmaster',$data);
	}
	
    public function create(){
        if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
            redirect('error','refresh');
        }
        
        $this->load->library(array('form_validation','messages'));
        
        $this->form_validation->set_rules('nik','NIK','required|callback__check_id');
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('grade','Grade','required');
		$this->form_validation->set_rules('saldo','Saldo','required');
        if($this->form_validation->run()){
            if(!$this->MMenu->blocked()){
                $this->MPromosponsor->add();
                $this->session->set_flashdata('message','Promo Sponsoring had been created successfully!');
            }
            redirect('promo/promosponsoring','refresh');
        }
        
        $data['page_title'] = 'Create Promo Sponsoring';
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        $this->load->view('promo/promosponsoring_create',$data);
    }
	
}
?>