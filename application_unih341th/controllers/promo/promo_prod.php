<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Promo_prod extends CI_Controller {
  function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('logged_in') or $this->session->userdata('group_id') > 100){
      redirect('');
    }

    $this->load->model(array('MMenu','Mpromo_prod'));
  }

  public function index(){
    if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
      redirect('error','refresh');
    }

    $this->load->library(array('form_validation','pagination'));

    $this->form_validation->set_rules('search','','');

    $config['base_url'] = site_url().'promo/promo_prod/index/';
    $config['per_page'] = 20;
    $config['uri_segment'] = 4;
    $data['from_rows'] = $this->uri->segment($config['uri_segment']); //untuk no urut paging

    if($this->form_validation->run()){
      $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
      $keywords = $this->session->userdata('keywords');
      $config['total_rows'] = $this->Mpromo_prod->countPromo($keywords);
      $this->pagination->initialize($config);
      $data['results'] = $this->Mpromo_prod->searchPromo($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
    }else{
      if(!$this->uri->segment($config['uri_segment'])) $this->session->unset_userdata('keywords');

      $keywords = $this->session->userdata('keywords');
      $config['total_rows'] = $this->Mpromo_prod->countPromo($keywords);
      $this->pagination->initialize($config);

      $data['results'] = $this->Mpromo_prod->searchPromo($keywords,$config['per_page'],$this->uri->segment($config['uri_segment']));
    }

    $data['page_title'] = 'Promo Product';
    $this->load->view('promo/promo_prod_index',$data);
  }

  public function create(){
    if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
      redirect('error','refresh');
    }

    $this->load->library(array('form_validation','messages'));

    $this->form_validation->set_rules('password1','Password','required|callback__check_password');
    $this->form_validation->set_rules('item_id','','required');
    $this->form_validation->set_rules('item_qty','','required');
    $this->form_validation->set_rules('free_item_id','','required');
    $this->form_validation->set_rules('free_item_qty','','required');
    $this->form_validation->set_rules('multiples','','required'); 
    $this->form_validation->set_rules('end_periode','','required');

    if($this->form_validation->run()){
      if(!$this->MMenu->blocked()){
          $this->Mpromo_prod->addPromo();
          $this->session->set_flashdata('message','Create item product successfully');
      }
      redirect('promo/promo_prod','refresh');
    }

        $data['reportDate'] = date("Y-m-d");
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
    
      $data['page_title'] = 'Create Item Product';
      $this->load->view('promo/promo_prod_form',$data);
  }

  public function _check_id(){
    if($this->Mpromo_prod->check_productid($this->input->post('id'))){
      $this->form_validation->set_message('_check_id','product id available !');
      return false;
    }
    return true;
  }

  public function _check_password(){
        $this->load->model('MAuth');
        if(!$this->MAuth->check_password($this->session->userdata('user'),$this->input->post('password1'))){
            $this->form_validation->set_message('_check_password','Sorry, your password invalid');
            return false;
        }
        return true;
    }

  public function view($id){
    if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'view')){
      redirect('error','refresh');
    }
    $row =array();
    $row = $this->Mpromo_prod->getPromo($id);

    if(!count($row)){
      redirect('promo/promo_prod','refresh');
    }
    $data['row'] = $row;
    $data['page_title'] = 'View Item Product';
    $this->load->view('promo/promo_prod_view',$data);
  }

  public function edit($id){
    if($this->MMenu->access($this->session->userdata('group_id'),$this->uri->segment(2),'save')){
      redirect('error','refresh');
    }

    $this->load->library(array('form_validation','messages'));

    $this->form_validation->set_rules('password1','Password','required|callback__check_password');
    $this->form_validation->set_rules('item_id','','');
    $this->form_validation->set_rules('item_qty','','');
    $this->form_validation->set_rules('free_item_id','','');
    $this->form_validation->set_rules('free_item_qty','','');
    $this->form_validation->set_rules('multiples','',''); 
    $this->form_validation->set_rules('end_periode','','');

    $row =array();
    $row = $this->Mpromo_prod->getPromo($id);

    if($this->form_validation->run()){
      if(!$this->MMenu->blocked()){
          $this->Mpromo_prod->editPromo($id);
          $this->session->set_flashdata('message','Edit promo product successfully');
      }
      redirect('promo/promo_prod','refresh');
    }

        $data['reportDate'] = date("Y-m-d");
        $data['extraHeadContent'] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"". base_url()."jscalender/calendar-blue2.css\" />\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-en.js\"></script>\n";
        $data['extraHeadContent'] .= "<script type=\"text/javascript\" src=\"". base_url()."jscalender/calendar-setup.js\"></script>\n";
        
      $data['row'] = $row;
      $data['page_title'] = 'Edit Promo Product';
      $this->load->view('promo/promo_prod_edit',$data);
  }

}
?>