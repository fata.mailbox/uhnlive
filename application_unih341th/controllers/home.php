<div class="content">
            	<!--about-->
                <img src="<?=base_url();?>images/frontend/about_top.png" style="margin-bottom:-1px;"/>
                <div class="about">
                	<div class="aboutCont" style="float:right; width:304px; overflow:inherit; height:auto; text-align:justify; background:url(<?=base_url();?>images/frontend/about_cont_bg.png) repeat;">
                    	<p class="bold">Selamat dan terima kasih telah bergabung dalam bisnis MLM <strong>PT. Universal Health Network</strong>!</p>
                        <p>Kami adalah perusahaan yang mengedepankan kualitas atas produk-produk yang berhubungan dengan kesehatan dan kecantikan serta mengajak segenap masyarakat dari berbagai golongan dan latar belakang untuk meraih kesuksesan dan kesejahteraan bersama...</p>
                        <img src="<?=base_url();?>images/frontend/dot2.png" style="float:left; margin:5px 0 0 75px;" />
                        <div class="readmore"><a href="<?=site_url();?>aboutus">Read More &raquo;</a></div>
                    </div>
                  <div class="clearBoth"></div>
                </div><!--end about-->
                
                <!--Promo_box-->
                <div class="promo_box">
                	<h2 style="margin-left:10px;">Promotion</h2>
                    <div class="promoMore"><a href="<?=site_url();?>news/promo">More... &raquo;</a></div>
                    <!--promo slide-->
                    <div class="promoSlide">
                        <div id="index_portfolio">
                            <ul id="sm" class="sm" style="padding:0px;">
                            	<?php if($promo): 
										foreach($promo as $row): ?>
                                <li style="width:39px;">
                                    <img src="<?=base_url();?>images/frontend/btn_prm<?=$row['i'];?>.png" style="float:left; cursor:pointer;"/>
                                    <div class="prm1">
                                        <div class="prm2">
                                            <div class="prm3">
                                                <h3><?=$row['title'];?></h3>
                                                <p><?=$row['shortdesc'];?></p>
                                                <p align="right"><a href="<?=site_url();?>news/detail/<?=$row['id'];?>">Selengkapnya &raquo;</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php endforeach;
                                         else:?>
                                <li style="width:39px;">
                                    <img src="<?=base_url();?>images/frontend/btn_prm1.png" style="float:left; cursor:pointer;"/>
                                    <div class="prm1">
                                        <div class="prm2">
                                            <div class="prm3">
                                                <p>Saat ini tidak ada promo</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php endif;?>
                            </ul>
                        </div>
                      <div class="clearBoth"></div>
                    </div><!--end promo slide-->
                </div><!--end promo_box-->
                
                <!--gallery-bpx-->
                <div class="gallery_box">
    			<?php if($gallery): 
                	foreach($gallery as $row): ?>
                    <div class="cover">
                    	<a href="<?=site_url();?>gallery/album/<?=$row['album'];?>" class="tooltip post" title="<?=$row['album'];?>"><img src="<?=base_url();?>userfiles/gallery_photo/t_<?=$row['file'];?>" width="105"/></a>
                    </div>                    
                    <?php endforeach;?>
                <?php else:?>
	                <div class="cover">
                    	<a href="#" class="tooltip post" title="ALBUM UNAVAILABLE"><img src="<?=base_url();?>images/frontend/cover_album_unavailable.jpg" width="105"/></a>
                    </div>
                    
                	<div class="cover">
                    	<a href="#" class="tooltip post" title="ALBUM UNAVAILABLE"><img src="<?=base_url();?>images/frontend/cover_album_unavailable.jpg" width="105"/></a>
                    </div>
                	<div class="cover">
                    	<a href="#" class="tooltip post" title="ALBUM UNAVAILABLE"><img src="<?=base_url();?>images/frontend/cover_album_unavailable.jpg" width="105"/></a>
                    </div>
                    <div class="cover">
                    	<a href="#" class="tooltip post" title="ALBUM UNAVAILABLE"><img src="<?=base_url();?>images/frontend/cover_album_unavailable.jpg" width="105"/></a>
                    </div>
                 <?php endif;?>
                </div><!--end gallery-bpx-->
                
              <div class="clearBoth"></div>
            </div><!--end content-->
            