<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Citysearch2 extends CI_Controller {
    function __construct()
    {
	parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect('','refresh');
        }

        $this->load->model(array('MSearch'));
    }

    public function all(){
        $this->load->library(array('form_validation','pagination'));

        $this->form_validation->set_rules('search','','');

        $config['base_url'] = site_url().'citysearch/all/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countSearchCity($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);

        $data['results'] = $this->MSearch->searchCity($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'City Search';

        $this->load->view('search/city_search',$data);
    }

	/* Created by Boby 20140124 */
	public function allso(){
        $this->load->library(array('form_validation','pagination'));

        $this->form_validation->set_rules('search','','');

        $config['base_url'] = site_url().'citysearch/allso/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countSearchCity($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);

        $data['results'] = $this->MSearch->searchCity($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'City Search';

        $this->load->view('search/city_search_so',$data);
    }
	/* End created by Boby 20140124 */

	/* Created by Boby 20140307 */
	public function allro(){
        $this->load->library(array('form_validation','pagination'));

        $this->form_validation->set_rules('search','','');

        $config['base_url'] = site_url().'citysearch/allso/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countSearchCity($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);

        $data['results'] = $this->MSearch->searchCity($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'City Search';

        $this->load->view('search/city_search_ro',$data);
    }
	/* End created by Boby 20140307 */

	/* Created by Boby 20140310 */
	public function all2(){
        $this->load->library(array('form_validation','pagination'));

        $this->form_validation->set_rules('search','','');

        $config['base_url'] = site_url().'citysearch/all/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countSearchCity($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);

        $data['results'] = $this->MSearch->searchCity($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'City Search';

        $this->load->view('search/city_search2',$data);
    }
	/* End created by Boby 20140310 */

	/* Created by ASP 20180405 */
	public function allpjm(){
        $this->load->library(array('form_validation','pagination'));

        $this->form_validation->set_rules('search','','');

        $config['base_url'] = site_url().'citysearch/allso/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countSearchCity($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);

        $data['results'] = $this->MSearch->searchCity($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'City Search';

        $this->load->view('search/city_search_pjm',$data);
    }
	/* End created by ASP 20180405 */
		/* Created by ASP 20180427 */
	public function allso_v(){
        $this->load->library(array('form_validation','pagination'));

        $this->form_validation->set_rules('search','','');

        $config['base_url'] = site_url().'citysearch/allso_v/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countSearchCity($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);

        $data['results'] = $this->MSearch->searchCity($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'City Search';

        $this->load->view('search/city_search_ro_v',$data);
    }
	/* End created by ASP 20180427 */

	/* Created by ASP 20181230 */
	public function allro_vwh(){
        $this->load->library(array('form_validation','pagination'));

        $this->form_validation->set_rules('search','','');

        $config['base_url'] = site_url().'citysearch/allro_vwh/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countSearchCity($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);

        $data['results'] = $this->MSearch->searchCity($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'City Search';

        $this->load->view('search/city_search_ro_vwh',$data);
    }
	/* End created by ASP 20181230 */
  // START AND 20190114
  public function allso2(){
        $this->load->library(array('form_validation','pagination'));

        $this->form_validation->set_rules('search','','');

        $config['base_url'] = site_url().'citysearch/allso/index/';
        if($this->form_validation->run()){
            $this->session->set_userdata('keywords',$this->db->escape_str($this->input->post('search')));
        }else{
            if(!$this->uri->segment(3))$this->session->unset_userdata('keywords');
        }
        $keywords = $this->session->userdata('keywords');
        $data['from_rows'] = $this->uri->segment(4); //untuk no urut paging
        $config['total_rows'] = $this->MSearch->countSearchCity($keywords,'all');
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);

        $data['results'] = $this->MSearch->searchCity($keywords,'all',$config['per_page'],$data['from_rows']);
        $data['page_title'] = 'City Search';

        $this->load->view('search/city_search_so2',$data);
    }
    // END AND 20190114

}
?>
