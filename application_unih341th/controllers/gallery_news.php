<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gallery_news extends CI_Controller {
    function __construct()
    {
	parent::__construct();
		$this->load->model('MFrontend');
	}
	
	public function index(){
	    $this->event();
	}
    
	public function event(){
		$this->load->library('pagination');
		
		//$data['news_archive'] = $this->MFrontend->list_archive('news');
		//$data['promo_archive'] = $this->MFrontend->list_archive('promo');
		$config['base_url'] = site_url().'news/gallery_news/index/';
			$config['per_page'] = 10;
			$config['uri_segment'] = 4;
			$config['total_rows'] = $this->MFrontend->count_list_news('news');
			$this->pagination->initialize($config);
			$data['news'] = $this->MFrontend->list_news('news','',$config['per_page'],$this->uri->segment($config['uri_segment']));
		
		$data['banner'] = $this->MFrontend->list_banner(50,0);
		$data['content'] = 'index/news';
		$data['title'] = 'Berita Perusahaan';
		$this->load->view('index/index',$data);
	}
	
	public function promo(){
		$this->load->library('pagination');
		
		//$data['news_archive'] = $this->MFrontend->list_archive('news');
		//$data['promo_archive'] = $this->MFrontend->list_archive('promo');
			$config['base_url'] = site_url().'news/promo/index/';
			$config['per_page'] = 10;
			$config['uri_segment'] = 4;
			$config['total_rows'] = $this->MFrontend->count_list_news('promo');
			$this->pagination->initialize($config);
			$data['news'] = $this->MFrontend->list_news('promo','',$config['per_page'],$this->uri->segment($config['uri_segment']));
		
		$data['banner'] = $this->MFrontend->list_banner(50,0);
		$data['content'] = 'index/news';
		$data['title'] = 'Promo !!';
		$this->load->view('index/index',$data);
	}
	public function detail($id=0){
		$row = $this->MFrontend->get_news($id);
		if(!count($row)){
			redirect('news/','refresh');
		}
		$data['row'] = $row;
		$this->MFrontend->update_countview($row['id']);
		
		$data['banner'] = $this->MFrontend->list_banner(50,0);
		$data['content'] = 'index/news_detail';
		if($row['promo'] == 'promo')$data['maintitle']='promo';
		else $data['maintitle']='news';
		$this->load->view('index/index',$data);
	}
}
?>