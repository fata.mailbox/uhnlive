<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
* Excel library for Code Igniter applications
* Author: Derek Allard, Dark Horse Consulting, www.darkhorse.to, April 2006
*/

function to_excel($query, $filename = 'exceloutput')
{
     $headers = ''; // just creating the var for field headers to append to below
     $data = ''; // just creating the var for field data to append to below

     $obj = &get_instance();

     $fields = $query->field_data();
     if ($query->num_rows() == 0) {
          echo '<p>The table appears to have no data.</p>';
     } else {
          foreach ($fields as $field) {
               $headers .= $field->name . "\t";
          }

          foreach ($query->result() as $row) {
               $line = '';
               foreach ($row as $value) {
                    if ((!isset($value)) or ($value == "")) {
                         $value = "\t";
                    } else {
                         $value = str_replace('"', '""', $value);
                         if ($line == '') {
                              $value = '"' . $value . '"';
                         } else {
                              $value = ', "' . $value . '"';
                         }
                    }
                    $line .= $value;
               }
               $data .= trim($line) . "\r\n";
          }

          $data = str_replace("\r", "", $data);

          header("Content-type: application/x-msdownload");
          header("Content-Disposition: attachment; filename=$filename.csv");
          echo "$headers\n$data";
     }
}

function call($text, $param)
{
     $ci = get_instance();
     if ($param == 'member') {
          $ai = $ci->db->query("SELECT nama from member where id = '$text' ")->row_array();
          if (empty($ai)) {
               return 'tidak ada';
          } else {
               return $ai['nama'];
          }
     } else {
          $ai = $ci->db->query("SELECT nama from member where id = '$text'")->row_array();
          if (empty($ai)) {
               return $text;
          } else {
               return $ai['nama'];
          }
     }
}

function get($voucher)
{
     $ci = get_instance();
     $ai = $ci->db->query("SELECT stc_id from voucher where vouchercode = '$voucher' ")->row_array();
     return $ai['stc_id'];
}


function callback($voucher)
{
     $ci = get_instance();
     $ai = $ci->db->query("SELECT stc_id, member_id,price from voucher where vouchercode = '$voucher' ")->row_array();
     return $ai;
}


function detail($id)
{
     $ci = get_instance();
     $ai = $ci->db->query("SELECT valid_from,valid_to from voucher_user where id = '$id' ")->row_array();
     return $ai;
}



function get_data_by_tabel_id($id, $table)
{
     $ci = get_instance();
     $data = $ci->db->query("SELECT * from  $table where id = '$id' ")->row();
     return $data;
}

function cekadj($item, $whs_id)
{
     $ci = get_instance();
     $a = $ci->db->query("SELECT qty from stock where warehouse_id = '$whs_id' and item_id = '$item' ")->row_array();
     $qty = intval($a['qty']);
     return $qty;
}

function cekaj($vouchercode)
{
     $ci = get_instance();
     $a = $ci->db->query("SELECT status,posisi,expired_date from voucher where vouchercode = '$vouchercode'  ")->row_array();
     return $a;
}

function format_digit($angka){
	//var_dump($angka); 
	if($angka == '0' || $angka == null){
		return '- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	}else{
		$digit = number_format(str_replace(' ', '', $angka),0,',','.');
		return $digit;
	}
}
function yui($topup)
{
     $ci = get_instance();
     $a = $ci->db->query("SELECT condition_value from topupbyvalue where topupno = '$topup'  ")->row_array();
     return intval($a['condition_value']);
}
function iur($topup)
{
     $ci = get_instance();
     $a = $ci->db->query("SELECT kelipatan from topupbyvalue where topupno = '$topup'  ")->row_array();
     return intval($a['kelipatan']);
}
function covel($total, $price)
{
     $y = $total % $price;
     return round($y);
}
function codestc($stc)
{
     $ci = get_instance();
     $a = $ci->db->query("SELECT no_stc from stockiest where id = '$stc'  ")->row_array();
     return $a['no_stc'];
}


function cek_topup($id)
{
     $ci = get_instance();
     $a = $ci->db->query("SELECT topupno from history_demand where topupno = '$id'  ")->num_rows();
     return $a;
}



function HitungDiscount($item,$persen)
{
     $ci = get_instance();
     $item_not  = $ci->db->query("SELECT item_id from item_not_discount where item_id  = '$item' ")->num_rows();
     if ($item_not > 1 ) {
         return $t = '';
     }else{
          return intval($persen);
     }
}
