<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

function get_token()
{

    $data = [
        'username' => 'solutech',
        'password' => 's0lut3ch'
    ];

    $ch = curl_init("http://149.129.232.205/unidev/rest/V1/integration/admin/token");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $response = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    if ($error !== '') {
        throw new \Exception($error);
    };

    //$token = 'fg1uuipy1ttro5r3wnrmyi6ywo5bq8cq'; //Sample token
    //return $token;
    return json_decode($response);
}

function getWhsName($whs_id)
{

    $ci = get_instance();
    $a = $ci->db->query("SELECT name from warehouse where id = '$whs_id'")->row_array();
    $name = $a['name'];
    return $name;
}

function getItemWhsId($item_id)
{

    $ci = get_instance();
    $a = $ci->db->query("SELECT warehouse_id from item where id = '$item_id'")->row_array();
    $warehouse_id = intVal($a['warehouse_id']);
    return $warehouse_id;
}

function get_member_id_from_id_fe($id_fe)
{

    $ci = get_instance();
    $a = $ci->db->query("SELECT temp_member_id from temp_member where member_id_fe = '$id_fe'")->row_array();
    $id_be = intVal($a['temp_member_id']);
    return $id_be;
}

function extractToPromoDiscount($package_id)
{
    $ci = get_instance();
    $pkg = $ci->db->query("select a.id as package_id,a.price as pp_west,a.price2 as pp_east,a.pv as p_pv,a.bv as p_bv from item a where a.id = '$package_id'")->row();
    $package_price_west = $pkg->pp_west;
    $package_price_east = $pkg->pp_east;
    $package_pv = $pkg->p_pv;
    $package_bv = $pkg->p_bv;

    //get contain price
    $itm = $ci->db->query("select sum(c.price) as i_p_west,sum(c.price2) as i_p_east,sum(c.pv) as i_pv,sum(c.bv) as i_bv from item a join manufaktur b on a.id = b.manufaktur_id join item c on c.id = b.item_id where a.id = '$package_id'")->row();
    $item_price_west = $itm->i_p_west;
    $item_price_east = $itm->i_p_east;
    $item_pv = $itm->i_pv;
    $item_bv = $itm->i_bv;

    //get extract price
    $ext = $ci->db->query("select a.id as package_id,a.price as pp_west,a.price2 as pp_east,a.pv as p_pv,a.bv as p_bv,b.item_id,b.qty,c.price as i_p_west,c.price2 as i_p_east,c.pv as i_pv,c.bv as i_bv,c.hpp as i_hpp,c.warehouse_id as i_wh_id from item a join manufaktur b on a.id = b.manufaktur_id join item c on c.id = b.item_id where a.id = '$package_id'")->result();

    //Get next promo code & qty min order
    $promo_code = $ci->db->query("select coalesce(max(promo_code),0)+1 as promo_code from promo_discount")->row()->promo_code;
    $min = $ci->db->query("select * from item_rule_order_promo where item_id = '$package_id'");
    if ($min->num_rows() == 0) {
        $min_order = 1;
    } else {
        $min_order = $min->row()->min_order;
    }

    //Extract to promo discount
    $time = date('Y-m-d H:i:s');
    if ($package_price_west - $item_price_west == 0) {
        //header
        $header = array(
            'promo_code' => $promo_code,
            'promo_for' => '1;2;3;',
            'valid_from' => date('Y-m-d'),
            'valid_to' => date('Y-m-d'),
            'assembly_id' => $package_id,
            'minimum_order' => $min_order,
            'disc_head' => 0,
            'disc_barat_head' => 0,
            'disc_timur_head' => 0,
            'harga_barat' => $package_price_west,
            'harga_timur' => $package_price_east,
            'pv' => $package_pv,
            'bv' => $package_bv,
            'actived' => 1,
            'createdBy' => 'SYSTEM',
            'createdDate' => $time
        );
        $ci->db->insert('promo_discount', $header);
        $header_id = $ci->db->insert_id();

        //detail
        foreach ($ext as $row) {
            $detail = array(
                'promo_code' => $promo_code,
                'assembly_id' => $package_id,
                'item_id' => $row->item_id,
                'qty' => $row->qty,
                'warehouse_id' => $row->i_wh_id,
                'harga_barat' => $row->i_p_west,
                'harga_timur' => $row->i_p_east,
                'pv' => $row->i_pv,
                'bv' => $row->i_bv,
                'disc' => 0,
                'disc_barat' => 0,
                'disc_timur' => 0,
                'harga_barat_af' => $row->i_p_west,
                'harga_timur_af' => $row->i_p_east,
                'pv_af' => $row->i_pv,
                'bv_af' => $row->i_bv,
                'type' => 1,
                'createdBy' => 'SYSTEM',
                'createdDate' => $time
            );
            $ci->db->insert('promo_discount_d', $detail);
        }
    } else {
        //prorate data
        $selisih_barat = $item_price_west - $package_price_west;
        $selisih_timur = $item_price_east - $package_price_east;
        $selisih_pv    = $item_pv - $package_pv;
        $selisih_bv    = $item_bv - $package_bv;
        $disc_barat    = number_format($selisih_barat / $item_price_west * 100, 2);
        $disc_timur    = number_format($selisih_timur / $item_price_east * 100, 2);
        $disc_pv       = number_format($selisih_pv / $item_pv * 100, 2);
        $disc_bv       = number_format($selisih_bv / $item_bv * 100, 2);

        //header
        $header = array(
            'promo_code' => $promo_code,
            'promo_for' => '1;2;3;',
            'valid_from' => date('Y-m-d'),
            'valid_to' => date('Y-m-d'),
            'assembly_id' => $package_id,
            'minimum_order' => $min_order,
            'disc_head' => 0,
            'disc_barat_head' => $selisih_barat,
            'disc_timur_head' => $selisih_timur,
            'harga_barat' => $package_price_west,
            'harga_timur' => $package_price_east,
            'pv' => $package_pv,
            'bv' => $package_bv,
            'actived' => 1,
            'createdBy' => 'SYSTEM',
            'createdDate' => $time
        );
        $ci->db->insert('promo_discount', $header);
        $header_id = $ci->db->insert_id();

        //detail
        $no  = 1;
        $len = sizeof($ext);
        $harga_barat_af = 0;
        $harga_timur_af = 0;
        $pv_af = 0;
        $bv_af = 0;
        foreach ($ext as $row) {
            if ($len != $no) {
                $detail = array(
                    'promo_code' => $promo_code,
                    'assembly_id' => $package_id,
                    'item_id' => $row->item_id,
                    'qty' => $row->qty,
                    'warehouse_id' => $row->i_wh_id,
                    'harga_barat' => $row->i_p_west,
                    'harga_timur' => $row->i_p_east,
                    'pv' => $row->i_pv,
                    'bv' => $row->i_bv,
                    'disc' => $disc_barat,
                    'disc_barat' => $row->i_p_west * (str_replace(',', '.', $disc_barat) / 100),
                    'disc_timur' => $row->i_p_east * (str_replace(',', '.', $disc_timur) / 100),
                    'harga_barat_af' => round($row->i_p_west - ($row->i_p_west * (str_replace(',', '.', $disc_barat) / 100))),
                    'harga_timur_af' => round($row->i_p_east - ($row->i_p_east * (str_replace(',', '.', $disc_timur) / 100))),
                    'pv_af' => round($row->i_pv - ($row->i_pv * (str_replace(',', '.', $disc_barat) / 100))),
                    'bv_af' => round($row->i_bv - ($row->i_bv * (str_replace(',', '.', $disc_barat) / 100))),
                    'type' => 1,
                    'createdBy' => 'SYSTEM',
                    'createdDate' => $time
                );
                $ci->db->insert('promo_discount_d', $detail);
            } else {
                $detail = array(
                    'promo_code' => $promo_code,
                    'assembly_id' => $package_id,
                    'item_id' => $row->item_id,
                    'qty' => $row->qty,
                    'warehouse_id' => $row->i_wh_id,
                    'harga_barat' => $row->i_p_west,
                    'harga_timur' => $row->i_p_east,
                    'pv' => $row->i_pv,
                    'bv' => $row->i_bv,
                    'disc' => $disc_barat,
                    'disc_barat' => $row->i_p_west * (str_replace(',', '.', $disc_barat) / 100),
                    'disc_timur' => $row->i_p_east * (str_replace(',', '.', $disc_timur) / 100),
                    'harga_barat_af' => $package_price_west - $harga_barat_af,
                    'harga_timur_af' => $package_price_east - $harga_timur_af,
                    'pv_af' => $package_pv - $pv_af,
                    'bv_af' => $package_bv - $bv_af,
                    'type' => 1,
                    'createdBy' => 'SYSTEM',
                    'createdDate' => $time
                );
                $ci->db->insert('promo_discount_d', $detail);
            }
            $harga_barat_af = round($row->i_p_west - ($row->i_p_west * (str_replace(',', '.', $disc_barat) / 100)));
            $harga_timur_af = round($row->i_p_east - ($row->i_p_east * (str_replace(',', '.', $disc_timur) / 100)));
            $pv_af = round($row->i_pv - ($row->i_pv * (str_replace(',', '.', $disc_barat) / 100)));
            $bv_af = round($row->i_bv - ($row->i_bv * (str_replace(',', '.', $disc_barat) / 100)));
            $no++;
        }
    }
}
