<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="100%">
<?php echo form_open('report/mem_tracking/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
<?php if($this->session->userdata('group_id')>100): ?>
        <tr>
			<td valign='top'>Member ID / Nama </td>
			<td valign='top'>:</td>
			<td><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('userid')." / ".$this->session->userdata('name');?></b></td>
		</tr>
		<tr>
			<td width='19%' valign='top'>Posisi Sekarang ~ Posisi Tertinggi</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php echo $row['jenjang'].' ~ '.$row['jenjang2'];?></td>
		</tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><?php echo form_submit('submit','preview');?></td>
        </tr>
        <?php echo form_close();?>
        <?php else: ?>
        <tr>
                        	<td width="25%">Member ID</td>
                            <td width="1%">:</td>
                            <td width="74%">
<?php $data = array('name'=>'member_id','id'=>'member_id','maxlength'=>'20','size'=>'11','value'=>set_value('member_id'));
							echo form_input($data);?>
                            <?php $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?></td>                        
  </tr>
                    <tr>
                        	<td>Nama Member</td>
                            <td>:</td>
                            <td><?php $data = array('name'=>'name','id'=>'name','maxlength'=>'20','readonly'=>'1','value'=>set_value('name'));
							echo form_input($data); ?></td>                        
                    </tr>              
                <tr>
		</tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
		<td><?php echo form_submit('submit','preview');?></td>
	</tr>
                    <?php echo form_close();?>

        
<?php endif;?>
	</table>
<?php
if ($trip){ 
	foreach($trip as $rowtrip):
	if($rowtrip['end_date']>=date('Y-m-d'))
	$classRed = 'class="qualified red"';
	else
	$classRed = '';
?>
    <table class="stripe" border="1" style="font-family:Jill Sans MT;">
	<tr <?=$classRed?>>
	  <td colspan="100%"><strong><?php echo $rowtrip['trip_name']; ?></strong></td>
	  </tr>
    <tr <?=$classRed?> style=" font-weight:bold">
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date'])); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+1 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+2 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+3 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+4 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+5 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+6 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+7 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+8 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+9 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+10 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+11 Month")); ?></center></td>
    </tr>
	<tr <?=$classRed?> style=" font-weight:bold">
    <?php for($uy =0; $uy<12 ; $uy++) {?>
      <td width='5%'>Break Away</td>
      <td width='15%'>TGPV</td> 
      <td width='35%'>Personal PV</td>
     <td width='15%'>Qualified</td>
     <?php } ?>
	</tr>
<?php
/* //testing
	$c = '2017-09-30';
	$d = date('Y-m-d',strtotime($c."+1 Month"));
	$e = '2017-12-31';
	echo $d;
	if($e >= $d) echo 'anjay'; else echo 'bangke';
*/
	if($trackTrip){
		$dataTracking = $this->MTracking_mem->getTracking($this->input->post('member_id'),$rowtrip['start_date'],$rowtrip['end_date']);
		$dataTrackingCurr = $this->MTracking_mem->getTrackingCurr($this->input->post('member_id'));
		$getcurrMonth = $this->MTracking_mem->get_curr_month();
		$currMonth = 0;
		
		if($getcurrMonth){
			foreach($getcurrMonth as $rowCurrMonth):
				$currMonth = $rowCurrMonth['currmonth'];
				$currDate = date('Y-m-d',strtotime($rowCurrMonth['lastdate']."+1 Month"));
			endforeach;
		}
		if($dataTrackingCurr){
			foreach($dataTrackingCurr as $rowCurr):
				$qCurr = $rowCurr['Q'];
				$psCurr = $rowCurr['ps'];
				$tgpvCurr = $rowCurr['tgpv'];
				$baCurr = $rowCurr['ba'];
			endforeach;
			//echo $currDate.'|'.$currMonth.'|'.$qCurr.'|'.$psCurr.'|'.$tgpvCurr.'|'.$baCurr;
		}
		if($dataTracking){
				foreach($dataTracking as $rowTracking): 
				$q1='No';$q2='No';$q3='No';$q4='No';$q5='No';$q6='No';$q7='No';$q8='No';$q9='No';$q10='No';$q11='No';$q12='No';
				$qFinal = 0;
				if($rowTrip['end_date']>=$currDate){
					if($currMonth==1){ $rowTracking['Q1']=$qCurr;$rowTracking['ba1']=$baCurr;$rowTracking['tgpv1']=$tgpvCurr;$rowTracking['ps1']=$psCurr;}
					if($currMonth==2){ $rowTracking['Q2']=$qCurr;$rowTracking['ba2']=$baCurr;$rowTracking['tgpv2']=$tgpvCurr;$rowTracking['ps2']=$psCurr;}
					if($currMonth==3){ $rowTracking['Q3']=$qCurr;$rowTracking['ba3']=$baCurr;$rowTracking['tgpv3']=$tgpvCurr;$rowTracking['ps3']=$psCurr;}
					if($currMonth==4){ $rowTracking['Q4']=$qCurr;$rowTracking['ba4']=$baCurr;$rowTracking['tgpv4']=$tgpvCurr;$rowTracking['ps4']=$psCurr;}
					if($currMonth==5){ $rowTracking['Q5']=$qCurr;$rowTracking['ba5']=$baCurr;$rowTracking['tgpv5']=$tgpvCurr;$rowTracking['ps5']=$psCurr;}
					if($currMonth==6){ $rowTracking['Q6']=$qCurr;$rowTracking['ba6']=$baCurr;$rowTracking['tgpv6']=$tgpvCurr;$rowTracking['ps6']=$psCurr;}
					if($currMonth==7){ $rowTracking['Q7']=$qCurr;$rowTracking['ba7']=$baCurr;$rowTracking['tgpv7']=$tgpvCurr;$rowTracking['ps7']=$psCurr;}
					if($currMonth==8){ $rowTracking['Q8']=$qCurr;$rowTracking['ba8']=$baCurr;$rowTracking['tgpv8']=$tgpvCurr;$rowTracking['ps8']=$psCurr;}
					if($currMonth==9){ $rowTracking['Q9']=$qCurr;$rowTracking['ba9']=$baCurr;$rowTracking['tgpv9']=$tgpvCurr;$rowTracking['ps9']=$psCurr;}
					if($currMonth==10){ $rowTracking['Q10']=$qCurr;$rowTracking['ba10']=$baCurr;$rowTracking['tgpv10']=$tgpvCurr;$rowTracking['ps10']=$psCurr;}
					if($currMonth==11){ $rowTracking['Q11']=$qCurr;$rowTracking['ba11']=$baCurr;$rowTracking['tgpv11']=$tgpvCurr;$rowTracking['ps11']=$psCurr;}
					if($currMonth==12){ $rowTracking['Q12']=$qCurr;$rowTracking['ba12']=$baCurr;$rowTracking['tgpv12']=$tgpvCurr;$rowTracking['ps12']=$psCurr;}
				}
				if($rowTracking['Q1']==1) $q1 = 'Yes'; else $q1 = 'No';
				if($rowTracking['Q2']==1) $q2 = 'Yes'; else $q2 = 'No';
				if($rowTracking['Q3']==1) $q3 = 'Yes'; else $q3 = 'No';
				if($rowTracking['Q4']==1) $q4 = 'Yes'; else $q4 = 'No';
				if($rowTracking['Q5']==1) $q5 = 'Yes'; else $q5 = 'No';
				if($rowTracking['Q6']==1) $q6 = 'Yes'; else $q6 = 'No';
				if($rowTracking['Q7']==1) $q7 = 'Yes'; else $q7 = 'No';
				if($rowTracking['Q8']==1) $q8 = 'Yes'; else $q8 = 'No';
				if($rowTracking['Q9']==1) $q9 = 'Yes'; else $q9 = 'No';
				if($rowTracking['Q10']==1) $q10 = 'Yes'; else $q10 = 'No';
				if($rowTracking['Q11']==1) $q11 = 'Yes'; else $q11 = 'No';
				if($rowTracking['Q12']==1) $q12 = 'Yes'; else $q12 = 'No';
			
				$qFinal = $rowTracking['Q1']+$rowTracking['Q2']+$rowTracking['Q3']+$rowTracking['Q4']+$rowTracking['Q5']+$rowTracking['Q6']+$rowTracking['Q7']+$rowTracking['Q8']+$rowTracking['Q9']+$rowTracking['Q10']+$rowTracking['Q11']+$rowTracking['Q12'];
					echo "
				<tr>
				  <td width='5%'>".number_format($rowTracking['ba1'])."</td><td width='15%'>".number_format($rowTracking['tgpv1'])."</td><td width='35%'>".number_format($rowTracking['ps1'])."</td><td width='15%'>".$q1."</td>
				  <td width='5%'>".number_format($rowTracking['ba2'])."</td><td width='15%'>".number_format($rowTracking['tgpv2'])."</td><td width='35%'>".number_format($rowTracking['ps2'])."</td><td width='15%'>".$q2."</td>
				  <td width='5%'>".number_format($rowTracking['ba3'])."</td><td width='15%'>".number_format($rowTracking['tgpv3'])."</td><td width='35%'>".number_format($rowTracking['ps3'])."</td><td width='15%'>".$q3."</td>
				  <td width='5%'>".number_format($rowTracking['ba4'])."</td><td width='15%'>".number_format($rowTracking['tgpv4'])."</td><td width='35%'>".number_format($rowTracking['ps4'])."</td><td width='15%'>".$q4."</td>
				  <td width='5%'>".number_format($rowTracking['ba5'])."</td><td width='15%'>".number_format($rowTracking['tgpv5'])."</td><td width='35%'>".number_format($rowTracking['ps5'])."</td><td width='15%'>".$q5."</td>
				  <td width='5%'>".number_format($rowTracking['ba6'])."</td><td width='15%'>".number_format($rowTracking['tgpv6'])."</td><td width='35%'>".number_format($rowTracking['ps6'])."</td><td width='15%'>".$q6."</td>
				  <td width='5%'>".number_format($rowTracking['ba7'])."</td><td width='15%'>".number_format($rowTracking['tgpv7'])."</td><td width='35%'>".number_format($rowTracking['ps7'])."</td><td width='15%'>".$q7."</td>
				  <td width='5%'>".number_format($rowTracking['ba8'])."</td><td width='15%'>".number_format($rowTracking['tgpv8'])."</td><td width='35%'>".number_format($rowTracking['ps8'])."</td><td width='15%'>".$q8."</td>
				  <td width='5%'>".number_format($rowTracking['ba9'])."</td><td width='15%'>".number_format($rowTracking['tgpv9'])."</td><td width='35%'>".number_format($rowTracking['ps9'])."</td><td width='15%'>".$q9."</td>
				  <td width='5%'>".number_format($rowTracking['ba10'])."</td><td width='15%'>".number_format($rowTracking['tgpv10'])."</td><td width='35%'>".number_format($rowTracking['ps10'])."</td><td width='15%'>".$q10."</td>
				  <td width='5%'>".number_format($rowTracking['ba11'])."</td><td width='15%'>".number_format($rowTracking['tgpv11'])."</td><td width='35%'>".number_format($rowTracking['ps11'])."</td><td width='15%'>".$q11."</td>
				  <td width='5%'>".number_format($rowTracking['ba12'])."</td><td width='15%'>".number_format($rowTracking['tgpv12'])."</td><td width='35%'>".number_format($rowTracking['ps12'])."</td><td width='15%'>".$q12."</td>
				</tr>
				";
				echo "
					<tr ".$classRed.">
					  <td colspan='100%'><strong>Total Qualified : ".$qFinal."</strong></td>
					</tr>
				";
				if($qFinal>=8){
					echo "
					<tr class='qualified green'>
					  <td colspan='100%'><strong>Selamat Anda Berhasil Memenuhi Kualifikasi !</strong></td>
					</tr>
					";	
				}
				endforeach;
		}else{
		?>
        <tr>
          <td colspan="100%">Data is not available.</td>
        </tr>
        <?php	
		}
	}else{
	?>
    <tr>
       <td colspan="100%">Data is not available.</td>
     </tr>
     <?php	
	}
?>
<?php endforeach; ?>
    </table>
<?php } ?>
<?php $this->load->view('footer');?>
