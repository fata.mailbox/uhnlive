<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="100%">
<?php echo form_open('report/stc_tracking/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
<?php if($this->session->userdata('group_id')>100): ?>
        <tr>
			<td valign='top'>Member ID / Nama </td>
			<td valign='top'>:</td>
			<td><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('userid')." / ".$this->session->userdata('name');?></b></td>
		</tr>
		<tr>
			<td width='19%' valign='top'>Posisi Sekarang ~ Posisi Tertinggi</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php echo $row['jenjang'].' ~ '.$row['jenjang2'];?></td>
		</tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><?php echo form_submit('submit','preview');?></td>
        </tr>
        <?php echo form_close();?>
        <?php else: ?>
        <tr>
                        	<td width="25%">Member ID</td>
                            <td width="1%">:</td>
                            <td width="74%">
<?php $data = array('name'=>'member_id','id'=>'member_id','maxlength'=>'20','size'=>'11','value'=>set_value('member_id'));
							echo form_input($data);?>
                            <?php $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?></td>                        
  </tr>
                    <tr>
                        	<td>Nama Member</td>
                            <td>:</td>
                            <td><?php $data = array('name'=>'name','id'=>'name','maxlength'=>'20','readonly'=>'1','value'=>set_value('name'));
							echo form_input($data); ?></td>                        
                    </tr>              
                <tr>
		</tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
		<td><?php echo form_submit('submit','preview');?></td>
	</tr>
                    <?php echo form_close();?>

        
<?php endif;?>
	</table>
<?php
if ($trip){ 
	$pernahQ = FALSE;
	foreach($trip as $rowtrip): 
	
	if($rowtrip['end_date']>=date('Y-m-d'))
	$classRed = 'class="qualified red"';
	else
	$classRed = '';
?>
    <table class="stripe" border="1" style="font-family:Jill Sans MT;">
	<tr <?=$classRed?>>
	  <td colspan="100%"><strong><?php echo $rowtrip['trip_name']; ?></strong></td>
	</tr>

		
    <tr <?=$classRed?> style=" font-weight:bold">
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date'])); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+1 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+2 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+3 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+4 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+5 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+6 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+7 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+8 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+9 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+10 Month")); ?></center></td>
    	<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+11 Month")); ?></center></td>
		<?php
			//echo date('Y', strtotime($rowtrip['start_date']));
			if(date('Y', strtotime($rowtrip['start_date']))=='2019'){
		?>
			<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+12 Month")); ?></center></td>
			<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+13 Month")); ?></center></td>
			<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+14 Month")); ?></center></td>
			<td colspan="4"><center><?php echo  date('F Y',strtotime($rowtrip['start_date']."+15 Month")); ?></center></td>
		<?php
				}
		?>
			
    </tr>
	<tr <?=$classRed?> style=" font-weight:bold; ">
	<?php 
	 if(date('Y', strtotime($rowtrip['start_date']))=='2019')$maxMonth = 16; else $maxMonth = 12;
	 for($uy =0; $uy<$maxMonth ; $uy++) {?>
      <td width='5%'>RO</td>
      <td width='15%'>SO</td> 
      <td width='35%'>Personal PV</td>
     <td width='15%'>Qualified</td>
     <?php } ?>
	</tr>
<?php
//		echo $rowtrip['start_date'].','.$rowtrip['end_date'];
//echo strtotime('2019-03-31'."+1 Year")."<br>";
//echo date('Y-m-d',strtotime('2019-03-31'."+2 Month"));
	if($trackTrip){
		if(date('Y', strtotime($rowtrip['start_date']))=='2019'){
			$dataTracking = $this->MTracking->getTracking2019($this->input->post('member_id'),$rowtrip['start_date'],$rowtrip['end_date']);
		} else {
			$dataTracking = $this->MTracking->getTracking($this->input->post('member_id'),$rowtrip['start_date'],$rowtrip['end_date']);
		}	
		if($dataTracking){
				foreach($dataTracking as $rowTracking): 
				$q1='No';$q2='No';$q3='No';$q4='No';$q5='No';$q6='No';$q7='No';$q8='No';$q9='No';$q10='No';$q11='No';$q12='No';$q13='No';$q14='No';$q15='No';$q16='No';
				$qFinal = 0;
				$Note = "";
				if($pernahQ==FALSE){
					if($rowTracking['Q1']==1) $q1 = 'Yes'; else $q1 = 'No';
					if($rowTracking['Q2']==1) $q2 = 'Yes'; else $q2 = 'No';
					if($rowTracking['Q3']==1) $q3 = 'Yes'; else $q3 = 'No';
					if($rowTracking['Q4']==1) $q4 = 'Yes'; else $q4 = 'No';
					if($rowTracking['Q5']==1) $q5 = 'Yes'; else $q5 = 'No';
					if($rowTracking['Q6']==1) $q6 = 'Yes'; else $q6 = 'No';
					if($rowTracking['Q7']==1) $q7 = 'Yes'; else $q7 = 'No';
					if($rowTracking['Q8']==1) $q8 = 'Yes'; else $q8 = 'No';
					if($rowTracking['Q9']==1) $q9 = 'Yes'; else $q9 = 'No';
					if($rowTracking['Q10']==1) $q10 = 'Yes'; else $q10 = 'No';
					if($rowTracking['Q11']==1) $q11 = 'Yes'; else $q11 = 'No';
					if($rowTracking['Q12']==1) $q12 = 'Yes'; else $q12 = 'No';
					if(date('Y', strtotime($rowtrip['start_date']))=='2019'){
						if($rowTracking['Q13']==1) $q13 = 'Yes'; else $q13 = 'No';
						if($rowTracking['Q14']==1) $q14 = 'Yes'; else $q14 = 'No';
						if($rowTracking['Q15']==1) $q15 = 'Yes'; else $q15 = 'No';
						if($rowTracking['Q16']==1) $q16 = 'Yes'; else $q16 = 'No';
						if($rowTracking['created'] < '2019-03-01 00:00:00'){
							$Note = 'Periode Qualifikasi Conference = Juli 2019 - Juni 2020. ';
							$qFinal = $rowTracking['Q5']+$rowTracking['Q6']+$rowTracking['Q7']+$rowTracking['Q8']+$rowTracking['Q9']+$rowTracking['Q10']+$rowTracking['Q11']+$rowTracking['Q12']+$rowTracking['Q13']+$rowTracking['Q14']+$rowTracking['Q15']+$rowTracking['Q16'];
						}else{
							$Note = 'Periode Qualifikasi Conference = Maret 2019 - Juni 2020. ';
							$qFinal = $rowTracking['Q1']+$rowTracking['Q2']+$rowTracking['Q3']+$rowTracking['Q4']+$rowTracking['Q5']+$rowTracking['Q6']+$rowTracking['Q7']+$rowTracking['Q8']+$rowTracking['Q9']+$rowTracking['Q10']+$rowTracking['Q11']+$rowTracking['Q12']+$rowTracking['Q13']+$rowTracking['Q14']+$rowTracking['Q15']+$rowTracking['Q16'];
						}
					}else{
						$Note = "";
						$qFinal = $rowTracking['Q1']+$rowTracking['Q2']+$rowTracking['Q3']+$rowTracking['Q4']+$rowTracking['Q5']+$rowTracking['Q6']+$rowTracking['Q7']+$rowTracking['Q8']+$rowTracking['Q9']+$rowTracking['Q10']+$rowTracking['Q11']+$rowTracking['Q12'];
					}
					if($rowtrip['id']>=4){
						if($qFinal >=8 ) $pernahQ = TRUE;
					}
				}else{
					if(date('Y', strtotime($rowtrip['start_date']))=='2019'){
						if($rowTracking['oms1']>= 40000000 and $rowTracking['po1'] >= 1000000) {$q1 = 'Yes';if(date('Y', strtotime($rowtrip['start_date']))=='2019' && $rowTracking['created'] >= '2019-03-01 00:00:00')$qFinal=$qFinal+1;} else $q1 = 'No';
						if($rowTracking['oms2']>= 40000000 and $rowTracking['po2'] >= 1000000) {$q2 = 'Yes';if(date('Y', strtotime($rowtrip['start_date']))=='2019' && $rowTracking['created'] >= '2019-03-01 00:00:00')$qFinal=$qFinal+1;} else $q2 = 'No';
						if($rowTracking['oms3']>= 40000000 and $rowTracking['po3'] >= 1000000) {$q3 = 'Yes';if(date('Y', strtotime($rowtrip['start_date']))=='2019' && $rowTracking['created'] >= '2019-03-01 00:00:00')$qFinal=$qFinal+1;} else $q3 = 'No';
						if($rowTracking['oms4']>= 40000000 and $rowTracking['po4'] >= 1000000) {$q4 = 'Yes';if(date('Y', strtotime($rowtrip['start_date']))=='2019' && $rowTracking['created'] >= '2019-03-01 00:00:00')$qFinal=$qFinal+1;} else $q4 = 'No';
					}else{
						if($rowTracking['oms1']>= 40000000 and $rowTracking['po1'] >= 1000000) {$q1 = 'Yes';$qFinal=$qFinal+1;} else $q1 = 'No';
						if($rowTracking['oms2']>= 40000000 and $rowTracking['po2'] >= 1000000) {$q2 = 'Yes';$qFinal=$qFinal+1;} else $q2 = 'No';
						if($rowTracking['oms3']>= 40000000 and $rowTracking['po3'] >= 1000000) {$q3 = 'Yes';$qFinal=$qFinal+1;} else $q3 = 'No';
						if($rowTracking['oms4']>= 40000000 and $rowTracking['po4'] >= 1000000) {$q4 = 'Yes';$qFinal=$qFinal+1;} else $q4 = 'No';
					}
					if($rowTracking['oms5']>= 40000000 and $rowTracking['po5'] >= 1000000) {$q5 = 'Yes';$qFinal=$qFinal+1;} else $q5 = 'No';
					if($rowTracking['oms6']>= 40000000 and $rowTracking['po6'] >= 1000000) {$q6 = 'Yes';$qFinal=$qFinal+1;} else $q6 = 'No';
					if($rowTracking['oms7']>= 40000000 and $rowTracking['po7'] >= 1000000) {$q7 = 'Yes';$qFinal=$qFinal+1;} else $q7 = 'No';
					if($rowTracking['oms8']>= 40000000 and $rowTracking['po8'] >= 1000000) {$q8 = 'Yes';$qFinal=$qFinal+1;} else $q8 = 'No';
					if($rowTracking['oms9']>= 40000000 and $rowTracking['po9'] >= 1000000) {$q9 = 'Yes';$qFinal=$qFinal+1;} else $q9 = 'No';
					if($rowTracking['oms10']>= 40000000 and $rowTracking['po10'] >= 1000000) {$q10 = 'Yes';$qFinal=$qFinal+1;} else $q10 = 'No';
					if($rowTracking['oms11']>= 40000000 and $rowTracking['po11'] >= 1000000) {$q11 = 'Yes';$qFinal=$qFinal+1;} else $q11 = 'No';
					if($rowTracking['oms12']>= 40000000 and $rowTracking['po12'] >= 1000000) {$q12 = 'Yes';$qFinal=$qFinal+1;} else $q12 = 'No';
					if(date('Y', strtotime($rowtrip['start_date']))=='2019'){
						if($rowTracking['oms13']>= 40000000 and $rowTracking['po13'] >= 1000000) {$q13 = 'Yes';$qFinal=$qFinal+1;} else $q13 = 'No';
						if($rowTracking['oms14']>= 40000000 and $rowTracking['po14'] >= 1000000) {$q14 = 'Yes';$qFinal=$qFinal+1;} else $q14 = 'No';
						if($rowTracking['oms15']>= 40000000 and $rowTracking['po15'] >= 1000000) {$q15 = 'Yes';$qFinal=$qFinal+1;} else $q15 = 'No';
						if($rowTracking['oms16']>= 40000000 and $rowTracking['po16'] >= 1000000) {$q16 = 'Yes';$qFinal=$qFinal+1;} else $q16 = 'No';
						
						if($rowTracking['created'] < '2019-03-01 00:00:00'){
							$Note = 'Periode Qualifikasi Conference = Juli 2019 - Juni 2020. ';
						}else{
							$Note = 'Periode Qualifikasi Conference = Maret 2019 - Juni 2020. ';
						}
					}
				}
				//echo $rowTracking['created'];
				echo "
<tr>
				  <td width='5%'>".number_format($rowTracking['oms1'])."</td><td width='15%'>".number_format($rowTracking['so1'])."</td><td width='35%'>".number_format($rowTracking['po1'])."</td><td width='15%'>".$q1."</td>
				  <td width='5%'>".number_format($rowTracking['oms2'])."</td><td width='15%'>".number_format($rowTracking['so2'])."</td><td width='35%'>".number_format($rowTracking['po2'])."</td><td width='15%'>".$q2."</td>
				  <td width='5%'>".number_format($rowTracking['oms3'])."</td><td width='15%'>".number_format($rowTracking['so3'])."</td><td width='35%'>".number_format($rowTracking['po3'])."</td><td width='15%'>".$q3."</td>
				  <td width='5%'>".number_format($rowTracking['oms4'])."</td><td width='15%'>".number_format($rowTracking['so4'])."</td><td width='35%'>".number_format($rowTracking['po4'])."</td><td width='15%'>".$q4."</td>
				  <td width='5%'>".number_format($rowTracking['oms5'])."</td><td width='15%'>".number_format($rowTracking['so5'])."</td><td width='35%'>".number_format($rowTracking['po5'])."</td><td width='15%'>".$q5."</td>
				  <td width='5%'>".number_format($rowTracking['oms6'])."</td><td width='15%'>".number_format($rowTracking['so6'])."</td><td width='35%'>".number_format($rowTracking['po6'])."</td><td width='15%'>".$q6."</td>
				  <td width='5%'>".number_format($rowTracking['oms7'])."</td><td width='15%'>".number_format($rowTracking['so7'])."</td><td width='35%'>".number_format($rowTracking['po7'])."</td><td width='15%'>".$q7."</td>
				  <td width='5%'>".number_format($rowTracking['oms8'])."</td><td width='15%'>".number_format($rowTracking['so8'])."</td><td width='35%'>".number_format($rowTracking['po8'])."</td><td width='15%'>".$q8."</td>
				  <td width='5%'>".number_format($rowTracking['oms9'])."</td><td width='15%'>".number_format($rowTracking['so9'])."</td><td width='35%'>".number_format($rowTracking['po9'])."</td><td width='15%'>".$q9."</td>
				  <td width='5%'>".number_format($rowTracking['oms10'])."</td><td width='15%'>".number_format($rowTracking['so10'])."</td><td width='35%'>".number_format($rowTracking['po10'])."</td><td width='15%'>".$q10."</td>
				  <td width='5%'>".number_format($rowTracking['oms11'])."</td><td width='15%'>".number_format($rowTracking['so11'])."</td><td width='35%'>".number_format($rowTracking['po11'])."</td><td width='15%'>".$q11."</td>
				  <td width='5%'>".number_format($rowTracking['oms12'])."</td><td width='15%'>".number_format($rowTracking['so12'])."</td><td width='35%'>".number_format($rowTracking['po12'])."</td><td width='15%'>".$q12."</td>
				";
				if(date('Y', strtotime($rowtrip['start_date']))=='2019'){
					echo "
					<td width='5%'>".number_format($rowTracking['oms13'])."</td><td width='15%'>".number_format($rowTracking['so13'])."</td><td width='35%'>".number_format($rowTracking['po13'])."</td><td width='15%'>".$q13."</td>
					<td width='5%'>".number_format($rowTracking['oms14'])."</td><td width='15%'>".number_format($rowTracking['so14'])."</td><td width='35%'>".number_format($rowTracking['po14'])."</td><td width='15%'>".$q14."</td>
					<td width='5%'>".number_format($rowTracking['oms15'])."</td><td width='15%'>".number_format($rowTracking['so15'])."</td><td width='35%'>".number_format($rowTracking['po15'])."</td><td width='15%'>".$q15."</td>
					<td width='5%'>".number_format($rowTracking['oms16'])."</td><td width='15%'>".number_format($rowTracking['so16'])."</td><td width='35%'>".number_format($rowTracking['po16'])."</td><td width='15%'>".$q16."</td>
					";
				}
			  echo "
			  	  </tr>
				";

			echo "
					<tr ".$classRed.">
					<td colspan='100%'><strong>".$Note."Total Qualified : ".$qFinal."</strong></td>
					</tr>
				";
				

				if($qFinal>=8){
					echo "
					<tr class='qualified green'>
					  <td colspan='100%'><strong>Selamat Anda Berhasil Memenuhi Kualifikasi !</strong></td>
					</tr>
					";	
				}
				endforeach;
		}else{
		?>
        <tr>
          <td colspan="100%">Data is not available.</td>
        </tr>
        <?php	
		}
	}else{
	?>
    <tr>
       <td colspan="100%">Data is not available.</td>
     </tr>
     <?php	
	}
?>
<?php endforeach; ?>
    </table>
<?php } ?>
<?php $this->load->view('footer');?>
