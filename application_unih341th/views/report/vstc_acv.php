<?php 
	$this->load->view('header');
	$tps = 0;
	$tomsso = 0;
	$tomsro = 0;
	$tmktso = 0;
	$tmktro = 0;
	$tso = 0;
	$tro = 0;
?>
<h2><?php echo $page_title;?></h2>
<table width="100%">
<?php echo form_open('report/stc_acv/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<tr>
		<td valign='top' width="19%">Tahun</td>
		<td valign='top' width="1%">:</td>
		<td width="80%">
			<?php echo form_dropdown('tahun',$dropdownyear);?>
			<?php echo form_dropdown('bulan',$dropdownq);?>
		</td>
	</tr>
	<tr>
		<td valign='top'>&nbsp;</td>
		<td valign='top'>&nbsp;</td>
		<td><?php echo form_submit('submit','preview'); if($thn==0){}else{echo anchor('report/stc_acv/export/'.$thn.'/'.$bln,'Export'); }?></td>
	</tr>                
	 <?php echo form_close();?>
<tr><td colspan="3"><hr /></td></tr>
</table>
	
<table border="1" bordercolor="#0000FF" class="stripe">
	<tr>
	  <th width='6%'>Stc No </th>
	  <th width='23%'>Stockiest</th>
	  <th width='3%'>Reg</th>
	  <th width='7%'>PS</th>
	  <th width='10%'>TGPV</th>
	  <th width="20"><div align="center">RO</div></th>
	  <th width="20"><div align="center">SO</div></th>
	</tr>
   
<?php
if ($results): 
	foreach($results as $key => $row): 
?>
    <tr>
		<td><?php echo $row['no_stc'];?></td>
		<td><?php echo $row['nama'];?></td>
		<td><?php echo $row['region'];?></td>
		<td align="right"><?php echo number_format($row['ps']); $tps+=$row['ps'];?></td>
		<td align="right"><?php echo number_format($row['tgpv']);?></td>
		<!-- <td align="right"><?php echo number_format($row['ba']);?></td> -->
		
		<!-- <td align="right"><?php echo number_format($row['mktRo']); $tmktro+=$row['mktRo'];?></td> -->
		<td align="right"><div align="right"><?php echo number_format($row['omsRo']); $tomsro+=$row['omsRo'];?></div></td>
		<!-- <td align="right"><div align="right"><?php echo number_format($row['omsRo']+$row['mktRo']); $tro+=($row['omsRo']+$row['mktRo']);?></div></td> -->
		
		<!-- <td align="right"><?php echo number_format($row['mktSo']); $tmktso+=$row['mktSo'];?></td> -->
		<td align="right"><div align="right"><?php echo number_format($row['omsSo']); $tomsso+=$row['omsSo'];?></div></td>
		<!-- <td align="right"><div align="right"><?php echo number_format($row['omsSo']+$row['mktSo']); $tso+=($row['omsSo']+$row['mktSo']);?></div></td> -->
		
    </tr>
    <?php endforeach; 
else: ?>
    <tr>
		<td colspan="11">Data is not available.</td>
    </tr>
<?php endif; ?> 
	<tr>
		<td colspan="3"><b>Total</b></td>
		<td align="right"><b><?php echo number_format($tps);?></b></td>
		<!-- <td align="right"><b>-</b></td> -->
		<td align="right"><b>-</b></td>
		<!-- <td align="right"><b><?php echo number_format($tmktro);?></b></td> -->
		<td align="right"><b><?php echo number_format($tomsro);?></b></td>
		<!-- <td align="right"><b><?php echo number_format($tro);?></b></td> -->
		<!-- <td align="right"><b><?php echo number_format($tmktso);?></b></td> -->
		<td align="right"><b><?php echo number_format($tomsso);?></b></td>
		<!-- <td align="right"><b><?php echo number_format($tso);?></b></td> -->
    </tr>
</table>			                
<?php $this->load->view('footer');?>
