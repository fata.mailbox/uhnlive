<?php 
	$this->load->view('header');
	$trg = 0; 	$trg1 = 30000000;	$trg2 = 5000000;
	$sls1 = 0;	$trgt1 = 0;	$t1=0;
	$sls2 = 0;	$trgt2 = 0; $t2=0;
	$sls3 = 0;	$trgt3 = 0; $t3=0;
?>
<h2><?php echo $page_title;?></h2>
	<table width="100%">
	<?php echo form_open('report/stc_achievement/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
        <tr>
			<td valign='top' width="19%">Tahun</td>
			<td valign='top' width="1%">:</td>
			<td width="80%">
				<?php echo form_dropdown('tahun',$dropdownyear);?>
				<?php echo form_dropdown('quart',$dropdownq);?>
			</td>
		</tr>
        <tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><?php echo form_submit('submit','preview');?></td>
		</tr>                
         <?php echo form_close();?>
    <tr><td colspan="3"><hr /></td></tr>
	</table>
	<?php echo anchor('report/stc_achievement/create','Create Stockiest Target'); ?>
<table border="1" bordercolor="#0000FF" class="stripe">
	<tr>
	  <th width='5%' rowspan="2">No </th>
	  <th width='30%' rowspan="2">Stc No </th>
	  <th width='20%' rowspan="2">Stockiest</th>
	  <th width='3%' rowspan="2">Reg</th>
	  <th colspan="3"><div align="center"><?php echo $head1;?></div></th>
	  <th colspan="3"><div align="center"><?php echo $head2;?></div></th>
	  <th colspan="3"><div align="center"><?php echo $head3;?></div></th>
  </tr>
	<tr>
      <th width='10%'><div align="right">Sales</div></th>
      <th width='10%'><div align="right">Target</div></th>
      <th width='4%'><div align="right">%</div></th>
      <th width='10%'><div align="right">Sales</div></th>
      <th width='10%'><div align="right">Target</div></th>
      <th width='4%'><div align="right">%</div></th>
      <th width='10%'><div align="right">Sales</div></th>
      <th width='10%'><div align="right">Target</div></th>
	  <th width='4%'><div align="right">%</div></th>
	</tr>
   
<?php
if ($results): 
	$i=0;
	foreach($results as $key => $row): 
		$i++;
		$periode = substr($row['periode'], -5, 2);
		if($periode<=3 && $periode>0)$periode="Quart1";
		if($periode<=6 && $periode>3)$periode="Quart2";
		if($periode<=9 && $periode>6)$periode="Quart3";
		if($periode<=12 && $periode>9)$periode="Quart4";
		if($row['tipe']==1){$trg=$trg1;}else{$trg=$trg2;}
?>
    <tr>
		<td align="right"><?php echo $i;?></td>
		<td><?php echo $row['no_stc'];?></td>
		<td><?php echo $row['nama'];?></td>
		<td><?php echo $row['region'];?></td>
		<td align="right"><?php echo number_format($row['oms1']); $sls1+=$row['oms1']; ?></td>
		<td align="right"><div align="right"><?php if($row['trg1']>0){$t1=$row['trg1'];}else{$t1=$trg;}echo number_format($t1); $trgt1+=$t1; ?></div></td>
		<td align="right"><div align="right"><?php echo number_format(($row['oms1'] * 100)/$t1); ?>%</div></td>
		<td align="right"><?php echo number_format($row['oms2']); $sls2+=$row['oms2']; ?></td>
		<td align="right"><div align="right"><?php if($row['trg2']>0){$t2=$row['trg2'];}else{$t2=$trg;}echo number_format($t2); $trgt2+=$t2; ?></div></td>
		<td align="right"><div align="right"><?php echo number_format(($row['oms2'] * 100)/$t2); ?>%</div></td>
		<td align="right"><?php echo number_format($row['oms3']); $sls3+=$row['oms3']; ?></td>
        <td align="right"><div align="right"><?php if($row['trg3']>0){$t3=$row['trg3'];}else{$t3=$trg;}echo number_format($t3); $trgt3+=$t3; ?></div></td>
		<td align="right"><div align="right"><?php echo number_format(($row['oms3'] * 100)/$t3); ?>%</div></td>
    </tr>
    <?php endforeach; 
else: ?>
    <tr>
		<td colspan="11">Data is not available.</td>
    </tr>
<?php endif; ?> 
	<tr>
		<td colspan="4"><b>Total</b></td>
		<td align="right"><b><?php echo number_format($sls1);?></b></td>
		<td align="right"><b><?php echo number_format($trgt1);?></b></td>
		<td align="right"><b><?php if($trgt1>0){echo number_format($sls1*100/$trgt1);}else{echo 0;}?>%</b></td>
		<td align="right"><b><?php echo number_format($sls2);?></b></td>
		<td align="right"><b><?php echo number_format($trgt2);?></b></td>
		<td align="right"><b><?php if($trgt2>0){echo number_format($sls2*100/$trgt2);}else{echo 0;}?>%</b></td>
		<td align="right"><b><?php echo number_format($sls3);?></b></td>
		<td align="right"><b><?php echo number_format($trgt3);?></b></td>
		<td align="right"><b><?php if($trgt3>0){echo number_format($sls3*100/$trgt3);}else{echo 0;}?>%</b></td>
    </tr>
</table>			                
<?php $this->load->view('footer');?>
