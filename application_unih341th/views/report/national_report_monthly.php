<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="40%">
<?php echo form_open('report/national_report_monthly/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
	<tr>
		<td valign='top'>Periode</td>
		<td valign='top'>:</td>
		<td><?php echo form_dropdown('tahun',$dropdownyear);?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><?php echo form_submit('submit','preview');?></td>
	</tr>
	<?php echo form_close();?>
	</table>
	
    <table class="stripe">
	<tr>
      <th width='15%'>Bulan</th>
      <th width='15%'><div align="right">Sales</div></th>
      <th width='15%'><div align="right">Other Sales</div></th>
      <th width='15%'><div align="right">Total Sales</div></th>
	  <th width='15%'><div align="right">Last Year</div></th>
	  <th width='15%'><div align="right">Growth</div></th>
	  <th width='15%'><div align="right">New Member</div></th>
	  <th width='15%'><div align="right">New Recruit</div></th>
	  <th width='15%'><div align="right">Existing Stc</div></th>
	  <th width='15%'><div align="right">Active Stc</div></th>
	  <th width='15%'><div align="right">% Active Stc</div></th>
    </tr>
   
<?php
if ($results): 
	$omset_mtd=0;
	$omset_mtd_other=0;
	$total_sales=0;
	$total_sales_ly=0;
	$nm=0;
	$nr=0;
	$currstc=0;
	$actstc=0;
	foreach($rowdata as $row):
	?>
    <tr>
		<td><?php echo date("M", strtotime($row['periode']));?></td>
		<td align="right"><?php echo number_format($row['omset_mtd']);$omset_mtd=$omset_mtd+$row['omset_mtd'];?></td>
		<td align="right"><?php echo number_format($row['omset_mtd_other']);$omset_mtd_other=$omset_mtd_other+$row['omset_mtd_other'];?></td>
		<td align="right"><?php echo number_format($row['total_sales']);$total_sales=$total_sales+$row['total_sales'];?></td>
		<td align="right"><?php echo number_format($row['total_sales_ly']);$total_sales_ly=$total_sales_ly+$row['total_sales_ly'];?></td>
		<td align="right"><?php echo number_format($row['growth']);?>%</td>
		<td align="right"><?php echo number_format($row['nm']);$nm=$nm+$row['nm'];?></td>
		<td align="right"><?php echo number_format($row['nr']);$nr=$nr+$row['nr'];?></td>
		<td align="right"><?php echo number_format($row['currstc']);$currstc=$currstc+$row['currstc'];?></td>
		<td align="right"><?php echo number_format($row['actstc']);$actstc=$actstc+$row['actstc'];?></td>
		<td align="right"><?php echo number_format($row['persen_stc']);?>%</td>
    </tr>
    <?php
	endforeach;
	?>
    <tr>
		<td colspan="100%">&nbsp;</td>
    </tr>
    <tr style="font-weight:bold;">
		<td>Total <?php echo $thn;?></td>
		<td align="right"><?php echo number_format($omset_mtd);?></td>
		<td align="right"><?php echo number_format($omset_mtd_other);?></td>
		<td align="right"><?php echo number_format($total_sales);?></td>
		<td align="right"><?php echo number_format($total_sales_ly);?></td>
		<td align="right"><?php echo number_format((($total_sales/$total_sales_ly)-1)*100);?>%</td>
		<td align="right"><?php echo number_format($nm);?></td>
		<td align="right"><?php echo number_format($nr);?></td>
		<td align="right"><?php echo number_format($currstc);?></td>
		<td align="right"><?php echo number_format($actstc);?></td>
		<td align="right"><?php echo number_format(($actstc/$currstc)*100);?>%</td>
    </tr>
	
<?php else: ?>
    <tr>
		<td colspan="100%">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<?php $this->load->view('footer');?>
