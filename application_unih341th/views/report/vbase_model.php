<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="100%">
<?php echo form_open('report/base_model/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
	<tr>
		<td valign='top'>Periode</td>
		<td valign='top'>:</td>
		<td><?php echo form_dropdown('periode',$dropdown);?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><?php echo form_submit('submit','preview');?></td>
	</tr>
	<?php echo form_close();?>
	</table>
	
    <table class="stripe">
	<tr>
      <th width='5%'>No.</th>
      <th width='35%'>Description</th>
      <th width='15%'><div align="right">MTD <?php echo $field['blnskrg']." ".$field['thnskrg'];?></div></th>
      <th width='15%'><div align="right">LMTD <?php echo $field['blnmin1']." ".$field['thnmin1']?></div></th>
      <th width='15%'><div align="right">YTD <?php echo $field['blnskrg']." ".($field['thnskrg']-1); ?></div></th>
	  <th width='15%'><div align="right">Total Omset <?php echo date("M Y", NOW());?></div></th>
    </tr>
   
<?php
if ($results): 
	$flag = 1;?>
    <tr>
		<td><?php echo $flag++; ?></td>
		<td>New Member</td>
		<td align="right"><?php echo number_format($curr['nm']);?></td>
		<td align="right"><?php echo number_format($lmtd['nm']);?></td>
		<td align="right"><?php echo number_format($lytd['nm']);?></td>
		<td align="right"><?php echo number_format($skrg['nm']);?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>New Recruit</td>
		<td align="right"><?php echo number_format($curr['nr']);?></td>
		<td align="right"><?php echo number_format($lmtd['nr']);?></td>
		<td align="right"><?php echo number_format($lytd['nr']);?></td>
		<td align="right"><?php echo number_format($skrg['nr']);?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Ratio New Recruit</td>
		<td align="right"><?php echo number_format(round(($curr['nr']*100)/($curr['nm'] + $curr['nr'])));?>%</td>
		<td align="right"><?php echo number_format(round(($lmtd['nr']*100)/($lmtd['nm'] + $lmtd['nr'])));?>%</td>
		<td align="right"><?php echo number_format(round(($lytd['nr']*100)/($lytd['nm'] + $lytd['nr'])));?>%</td>
		<td align="right"><?php echo number_format(round(($skrg['nr']*100)/($skrg['nm'] + $skrg['nr'])));?>%</td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Sales New Recruit</td>
		<td align="right"><?php echo number_format($curr['nr_oms']);?></td>
		<td align="right"><?php echo number_format($lmtd['nr_oms']);?></td>
		<td align="right"><?php echo number_format($lytd['nr_oms']);?></td>
		<td align="right"><?php echo number_format($skrg['nr_oms']);?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Productivity New Recruit</td>
		<td align="right"><?php echo number_format(($curr['nr_oms'])/$curr['nr']);?></td>
		<td align="right"><?php echo number_format(($lmtd['nr_oms'])/$lmtd['nr']);?></td>
		<td align="right"><?php echo number_format(($lytd['nr_oms'])/$lytd['nr']);?></td>
		<td align="right"><?php echo number_format(($skrg['nr_oms'])/$skrg['nr']);?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Sponsoring</td>
		<td align="right"><?php echo number_format($sp['mtd']);?></td>
		<td align="right"><?php echo number_format($sp['lmtd']);?></td>
		<td align="right"><?php echo number_format($sp['lytd']);?></td>
		<td align="right"><?php echo 0;//$nm['mtd']?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Old Member Active</td>
		<td align="right"><?php echo number_format($curr['om']);?></td>
		<td align="right"><?php echo number_format($lmtd['om']);?></td>
		<td align="right"><?php echo number_format($lytd['om']);?></td>
		<td align="right"><?php echo number_format($skrg['om']);?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Reactive Member</td>
		<td align="right"><?php echo number_format($curr['rm']);?></td>
		<td align="right"><?php echo number_format($lmtd['rm']);?></td>
		<td align="right"><?php echo number_format($lytd['rm']);?></td>
		<td align="right"><?php echo number_format($skrg['rm']);?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Number of old member</td>
		<td align="right"><?php echo number_format($curr['om']+$curr['rm']);?></td>
		<td align="right"><?php echo number_format($lmtd['om']+$lmtd['rm']);?></td>
		<td align="right"><?php echo number_format($lytd['om']+$lytd['rm']);?></td>
		<td align="right"><?php echo number_format($skrg['om']+$skrg['rm']);?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Sales Old Member</td>
		<td align="right"><?php echo number_format($curr['om_oms']);?></td>
		<td align="right"><?php echo number_format($lmtd['om_oms']);?></td>
		<td align="right"><?php echo number_format($lytd['om_oms']);?></td>
		<td align="right"><?php echo number_format($skrg['om_oms']);?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Sales Reactive Member</td>
		<td align="right"><?php echo number_format($curr['rm_oms']);?></td>
		<td align="right"><?php echo number_format($lmtd['rm_oms']);?></td>
		<td align="right"><?php echo number_format($lytd['rm_oms']);?></td>
		<td align="right"><?php echo number_format($skrg['rm_oms']);?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Sales Staff Order</td>
		<td align="right"><?php echo number_format($curr['staff']);?></td>
		<td align="right"><?php echo number_format($lmtd['staff']);?></td>
		<td align="right"><?php echo number_format($lytd['staff']);?></td>
		<td align="right"><?php echo number_format($skrg['staff']);?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Total Sales</td>
		<td align="right"><?php echo number_format($curr['nm_oms']+$curr['nr_oms']+$curr['om_oms']+$curr['rm_oms']+$curr['staff']);?></td>
		<td align="right"><?php echo number_format($lmtd['nm_oms']+$lmtd['nr_oms']+$lmtd['om_oms']+$lmtd['rm_oms']+$lmtd['staff']);?></td>
		<td align="right"><?php echo number_format($lytd['nm_oms']+$lytd['nr_oms']+$lytd['om_oms']+$lytd['rm_oms']+$lytd['staff']);?></td>
		<td align="right"><?php echo number_format($skrg['nm_oms']+$skrg['nr_oms']+$skrg['om_oms']+$skrg['rm_oms']+$skrg['staff']);?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Productivity Total Old Member</td>
		<td align="right"><?php echo number_format(($curr['om_oms']+$curr['rm_oms'])/($curr['om']+$curr['rm']));?></td>
		<td align="right"><?php echo number_format(($lmtd['om_oms']+$lmtd['rm_oms'])/($lmtd['om']+$lmtd['rm']));?></td>
		<td align="right"><?php echo number_format(($lytd['om_oms']+$lytd['rm_oms'])/($lytd['om']+$lytd['rm']));?></td>
		<td align="right"><?php echo number_format(($skrg['om_oms']+$skrg['rm_oms'])/($skrg['om']+$skrg['rm']));?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Inactive Member</td>
		<td align="right"><?php echo 0;//$nm['mtd']?></td>
		<td align="right"><?php echo 0;//$nm['mtd']?></td>
		<td align="right"><?php echo 0;//$nm['mtd']?></td>
		<td align="right"><?php echo 0;//$nm['mtd']?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Sales Force Active</td>
		<td align="right"><?php echo number_format($curr['nr']+$curr['om']+$curr['rm']);?></td>
		<td align="right"><?php echo number_format($lmtd['nr']+$lmtd['om']+$lmtd['rm']);?></td>
		<td align="right"><?php echo number_format($lytd['nr']+$lytd['om']+$lytd['rm']);?></td>
		<td align="right"><?php echo number_format($skrg['nr']+$skrg['om']+$skrg['rm']);?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Total Productivity</td>
		<td align="right"><?php echo number_format(($curr['nm_oms']+$curr['nr_oms']+$curr['om_oms']+$curr['rm_oms']+$curr['staff'])/($curr['nr']+$curr['om']+$curr['rm']));?></td>
		<td align="right"><?php echo number_format(($lmtd['nm_oms']+$lmtd['nr_oms']+$lmtd['om_oms']+$lmtd['rm_oms']+$lmtd['staff'])/($lmtd['nr']+$lmtd['om']+$lmtd['rm']));?></td>
		<td align="right"><?php echo number_format(($lytd['nm_oms']+$lytd['nr_oms']+$lytd['om_oms']+$lytd['rm_oms']+$lytd['staff'])/($lytd['nr']+$lytd['om']+$lytd['rm']));?></td>
		<td align="right"><?php echo number_format(($skrg['nm_oms']+$skrg['nr_oms']+$skrg['om_oms']+$skrg['rm_oms']+$skrg['staff'])/($skrg['nr']+$skrg['om']+$skrg['rm']));?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Ratio Total Old member / SF</td>
		<td align="right"><?php echo number_format(($curr['om']+$curr['rm'])*100/($curr['nr']+$curr['om']+$curr['rm']));?>%</td>
		<td align="right"><?php echo number_format(($lmtd['om']+$lmtd['rm'])*100/($lmtd['nr']+$lmtd['om']+$lmtd['rm']));?>%</td>
		<td align="right"><?php echo number_format(($lytd['om']+$lytd['rm'])*100/($lytd['nr']+$lytd['om']+$lytd['rm']));?>%</td>
		<td align="right"><?php echo number_format(($skrg['om']+$skrg['rm'])*100/($skrg['nr']+$skrg['om']+$skrg['rm']));?>%</td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Ratio New Recruits /SF</td>
		<td align="right"><?php echo number_format($curr['nr']*100/($curr['nr']+$curr['om']+$curr['rm']));?>%</td>
		<td align="right"><?php echo number_format($lmtd['nr']*100/($lmtd['nr']+$lmtd['om']+$lmtd['rm']));?>%</td>
		<td align="right"><?php echo number_format($lytd['nr']*100/($lytd['nr']+$lytd['om']+$lytd['rm']));?>%</td>
		<td align="right"><?php echo number_format($skrg['nr']*100/($skrg['nr']+$skrg['om']+$skrg['rm']));?>%</td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>New Sales / Total Sales</td>
		<td align="right"><?php echo number_format($curr['nr_oms']*100/($curr['nm_oms']+$curr['nr_oms']+$curr['om_oms']+$curr['rm_oms']+$curr['staff']));?>%</td>
		<td align="right"><?php echo number_format($lmtd['nr_oms']*100/($lmtd['nm_oms']+$lmtd['nr_oms']+$lmtd['om_oms']+$lmtd['rm_oms']+$lmtd['staff']));?>%</td>
		<td align="right"><?php echo number_format($lytd['nr_oms']*100/($lytd['nm_oms']+$lytd['nr_oms']+$lytd['om_oms']+$lytd['rm_oms']+$lytd['staff']));?>%</td>
		<td align="right"><?php echo number_format($skrg['nr_oms']*100/($skrg['nm_oms']+$skrg['nr_oms']+$skrg['om_oms']+$skrg['rm_oms']+$skrg['staff']));?>%</td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Existing Sales / Total Sales</td>
		<td align="right"><?php echo number_format(($curr['rm_oms'] + $curr['rm_oms']) *100/ ($curr['nm_oms']+$curr['nr_oms']+$curr['om_oms']+$curr['rm_oms']+$curr['staff']));?>%</td>
		<td align="right"><?php echo number_format(($lmtd['rm_oms'] + $lmtd['rm_oms']) *100/ ($lmtd['nm_oms']+$lmtd['nr_oms']+$lmtd['om_oms']+$lmtd['rm_oms']+$lmtd['staff']));?>%</td>
		<td align="right"><?php echo number_format(($lytd['rm_oms'] + $lytd['rm_oms']) *100/ ($lytd['nm_oms']+$lytd['nr_oms']+$lytd['om_oms']+$lytd['rm_oms']+$lytd['staff']));?>%</td>
		<td align="right"><?php echo number_format(($skrg['rm_oms'] + $skrg['rm_oms']) *100/ ($skrg['nm_oms']+$skrg['nr_oms']+$skrg['om_oms']+$skrg['rm_oms']+$skrg['staff']));?>%</td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Sales Staff Order</td>
		<td align="right"><?php echo number_format(($curr['staff']*100)/($curr['nm_oms']+$curr['nr_oms']+$curr['om_oms']+$curr['rm_oms']+$curr['staff']));?>%</td>
		<td align="right"><?php echo number_format(($lmtd['staff']*100)/($lmtd['nm_oms']+$lmtd['nr_oms']+$lmtd['om_oms']+$lmtd['rm_oms']+$lmtd['staff']));?>%</td>
		<td align="right"><?php echo number_format(($lytd['staff']*100)/($lytd['nm_oms']+$lytd['nr_oms']+$lytd['om_oms']+$lytd['rm_oms']+$lytd['staff']));?>%</td>
		<td align="right"><?php echo number_format(($skrg['staff']*100)/($skrg['nm_oms']+$skrg['nr_oms']+$skrg['om_oms']+$skrg['rm_oms']+$skrg['staff']));?>%</td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Target
			<?php
				if($tcurr['target']){$tcurr_ = $tcurr['target'];}else{$tcurr_ = ($curr['nm_oms']+$curr['nr_oms']+$curr['om_oms']+$curr['rm_oms']+$curr['staff']);}
				if($tlmtd['target']){$tlmtd_ = $tlmtd['target'];}else{$tlmtd_ = ($lmtd['nm_oms']+$lmtd['nr_oms']+$lmtd['om_oms']+$lmtd['rm_oms']+$lmtd['staff']);}
				if($tlytd['target']){$tlytd_ = $tlytd['target'];}else{$tlytd_ = ($lytd['nm_oms']+$lytd['nr_oms']+$lytd['om_oms']+$lytd['rm_oms']+$lytd['staff']);}
				if($tskrg['target']){$tskrg_ = $tskrg['target'];}else{$tskrg_ = ($skrg['nm_oms']+$skrg['nr_oms']+$skrg['om_oms']+$skrg['rm_oms']+$skrg['staff']);}
			?>
		</td>
		<td align="right"><?php echo number_format($tcurr_);?></td>
		<td align="right"><?php echo number_format($tlmtd_);?></td>
		<td align="right"><?php echo number_format($tlytd_);?></td>
		<td align="right"><?php echo number_format($tskrg_);?></td>
    </tr>
	<tr>
		<td><?php echo $flag++; ?></td>
		<td>Varience Of Sales Target</td>
		<td align="right"><?php echo number_format(($curr['nm_oms']+$curr['nr_oms']+$curr['om_oms']+$curr['rm_oms']+$curr['staff'])*100/$tcurr_);?>%</td>
		<td align="right"><?php echo number_format(($lmtd['nm_oms']+$lmtd['nr_oms']+$lmtd['om_oms']+$lmtd['rm_oms']+$lmtd['staff'])*100/$tlmtd_);?>%</td>
		<td align="right"><?php echo number_format(($lytd['nm_oms']+$lytd['nr_oms']+$lytd['om_oms']+$lytd['rm_oms']+$lytd['staff'])*100/$tlytd_);?>%</td>
		<td align="right"><?php echo number_format(($skrg['nm_oms']+$skrg['nr_oms']+$skrg['om_oms']+$skrg['rm_oms']+$skrg['staff'])*100/$tskrg_);?>%</td>
    </tr>
    <tr>
		<td colspan="6">&nbsp;</td>
    </tr>
	
<?php else: ?>
    <tr>
		<td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<?php $this->load->view('footer');?>
