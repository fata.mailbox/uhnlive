<?php 
	$this->load->view('header');
	$q = 0;
	$s1 = 0;	
	$ts1 = 0;
	$s2 = 0;
	$ts2 = 0;
	$target = 0;
	$flag = 0;
	$flag_ = 0;
	$spt = 0;
	$kit1 = 0;
	$tkit1 = 0;
	$kit2 = 0;
	$tkit2 = 0;
	$nr = 0;
	$nr2 = 0;
	$tnr = 0;
	$sf = 0;
	$tsf = 0;
	$tnr2 = 0;
	$trct = 0;
?>
<h2><?php echo $page_title;?></h2>
	<table width="100%">
	<?php echo form_open('report/sls_inc_ro/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
        <tr>
			<td valign='top' width="19%">Tahun</td>
			<td valign='top' width="1%">:</td>
			<td width="80%">
				<?php echo form_dropdown('tahun',$dropdownyear);?>
				<?php echo form_dropdown('quart',$dropdownq);?>
			</td>
		</tr>
        <tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><?php echo form_submit('submit','preview');?></td>
		</tr>                
         <?php echo form_close();?>
    <tr><td colspan="3"><hr /></td></tr>
	</table>
	
<table class="stripe" width="100%">
	<tr>
      <th width='15%'>Region</th>
      <th width='15%'>PIC</th>
      <th width='15%'><div align="right">Sales <?php echo $thn-1;?></div></th>
      <th width='15%'><div align="right">Sales <?php echo $thn;?></div></th>
      <th width='15%'><div align="right">Sales Target</div></th>
      <th width='5%'><div align="right">Sales Acv</div></th>
      <th width='10%'><div align="right"><?php echo $thn-1 .' vs '.$thn;?></div></th>
	  <!--<th width='5%'><div align="right">New Recruit</div></th>
	  <th width='5%'><div align="right">New Member</div></th>
	  <th width='5%'><div align="right">Recruitor</div></th>-->
	  <!-- <th width='10%'><div align="right">Sales Force</div></th> modified by Boby 20130520 -->
	  <!-- Created by Boby 20130520 -->
	  <!--<th width='5%'><div align="right">NM Target</div></th>
	  <th width='5%'><div align="right">NM Acv</div></th>-->
	  <!-- End created by Boby 20130520 -->
    </tr>
   
<?php
if ($results): 
	foreach($results as $key => $row): ?>
	<?php
		
		if($q!=$row["periode"]){
			$q=$row["periode"];
			
			if($flag>0){
	?>
	<tr>
		<td colspan="2"><b>Sub Total</b></td>
		<td align="right"><b><?php echo number_format($ts1);  ?></b></td>
		<td align="right"><b><?php echo number_format($ts2);  ?></b></td>
		<td align="right"><b><?php echo number_format($tst); 	?></b></td>
		<td align="right"><b><?php if($tst>0){echo number_format((($ts2/$tst)-1)*100);}else{echo 0;}	$tspt= 0;	$flag_ = 0;?>%</b></td>
		<td align="right"><b><?php echo number_format((($ts2/$ts1)-1)*100);		?>%</b></td>
		<!--<td align="right"><b><?php if($tst>0){echo number_format(($ts2*100)/$tst);}else{echo 0;}	$tspt= 0;	$flag_ = 0;?>%</b></td>
		<td align="right"><b><?php echo number_format(($ts2*100)/$ts1);		?>%</b></td>-->
		<!--<td align="right"><b><?php //echo number_format($nr2);			?></b></td>
		<td align="right"><b><?php //echo number_format($nr);			?></b></td>
		<td align="right"><b><?php //echo number_format($rct);			?></b></td>-->
		<!-- <td align="right"><b><?php echo number_format($tsf);			?></b></td> modified by Boby 20130520 -->
		<!--<td align="right"><b><?php //echo number_format($kit2);			?></b></td>
		<td align="right"><b><?php //echo number_format($tnr*100/$kit2);			?>%</b></td>-->
	</tr>
	<?php
				$ts1 = 0;
				$ts2 = 0;
				$tst = 0;
				$kit1= 0;
				$kit2= 0;
				// $tnr2 = 0;
				// $tnr = 0;
				$tsf = 0;
				$nr = 0;
				$nr2 = 0;
				$rct = 0;
			}$flag = 1;
			$p1=$row["periode"];
			$p2=$period_." ";
			if($period_=="Month"){
				$p1="";
				if($row["periode"]==1){$p2='January';}
				if($row["periode"]==2){$p2='February';}
				if($row["periode"]==3){$p2='March';}
				if($row["periode"]==4){$p2='April';}
				if($row["periode"]==5){$p2='May';}
				if($row["periode"]==6){$p2='June';}
				if($row["periode"]==7){$p2='July';}
				if($row["periode"]==8){$p2='August';}
				if($row["periode"]==9){$p2='September';}
				if($row["periode"]==10){$p2='October';}
				if($row["periode"]==11){$p2='November';}
				if($row["periode"]==12){$p2='December';}
			}
	?>
	<tr>
		<td colspan="7"><b><?php echo $p2.$p1;?></b></td>
	</tr>
	<?php } $flag_ += 1;?>
    <tr>
		<td><?php echo "- ".$row['region'];?></td>
		<td><?php echo $row['region_pic'];?></td>
		<td align="right"><?php echo number_format($row['omset1']);			$s1+=$row['omset1'];		$ts1+=$row['omset1']; ?></td>
		<td align="right"><?php echo number_format($row['omset2']);			$s2+=$row['omset2'];		$ts2+=$row['omset2']; ?></td>
		<td align="right"><?php echo number_format($row['target']);			$st+=$row['target'];		$tst+=$row['target'];?></td>
		<td align="right"><?php echo number_format($row['salesVStarget']);	$spt+=$row['salesVStarget'];$tspt+=$row['salesVStarget'];?>%</td>
		<td align="right"><?php echo number_format($row['newVSold']);		?>%</td>
		<?php
			
		?>
        <!--
		<td align="right"><?php //echo number_format($row['nr2']);			$nr2+=$row['nr2'];			$tnr2+=$row['nr2'];?></td>
		<td align="right"><?php //echo number_format($row['kit2']);			$nr+=$row['kit2'];			$tnr+=$row['kit2'];?></td>
		<td align="right"><?php //echo number_format($row['recruit']);		$rct += $row['recruit'];	$trct+=$row['recruit'];?></td>-->
		<!-- <td align="right"><?php //echo number_format($row['sf']);				$sf+=$row['sf']; 			$tsf+=$row['sf'];?></td> -->
		<!-- created by Boby 20130520 -->
		<!--<td align="right"><?php //echo number_format($row['nr']);			$kit2+=$row['nr'];			$tkit2+=$row['nr'];?></td>
		<td align="right"><?php //if($row['nr']>0){echo number_format($row['kit2']*100/$row['nr']);}else{echo 0;} ?>%</td>-->
		<!-- end created by Boby 20130520 -->
    </tr>
    <?php endforeach; ?>
	<tr>
		<td colspan="2"><b>Sub Total</b></td>
		<td align="right"><b><?php echo number_format($ts1);  ?></b></td>
		<td align="right"><b><?php echo number_format($ts2);  ?></b></td>
		<td align="right"><b><?php echo number_format($tst); 	?></b></td>
		<!--<td align="right"><b><?php if($tst>0){echo number_format(($ts2*100)/$tst);}else{echo 0;}	$tspt= 0;	$flag_ = 0;?>%</b></td>-->
		<td align="right"><b><?php if($tst>0){echo number_format((($ts2/$tst)-1)*100);}else{echo 0;}	$tspt= 0;	$flag_ = 0;?>%</b></td>
		<td align="right"><b><?php echo number_format((($ts2/$ts1)-1)*100);		?>%</b></td>
		<!--<td align="right"><b><?php //echo number_format($nr2);			?></b></td>
		<td align="right"><b><?php //echo number_format($nr);			?></b></td>
		<td align="right"><b><?php //echo number_format($rct);			?></b></td>-->
		<!-- <td align="right"><b><?php echo number_format($tsf);			?></b></td> modified by Boby 20130520 -->
		<!--<td align="right"><b><?php //echo number_format($kit2);			?></b></td>
		<td align="right"><b><?php //echo number_format($tnr*100/$kit2);			?>%</b></td>-->
	</tr>
	
	<tr>
		<td colspan="5"><b> </b></td>
		<td align="right"><b><?php //echo '0';?></b></td>
		<td>&nbsp;</td>
    </tr>
<?php else: ?>
    <tr>
		<td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>
	<tr>
		<td colspan="2"><b>Grand Total</b></td>
		<td align="right"><b><?php echo number_format($s1);  ?></b></td>
		<td align="right"><b><?php echo number_format($s2);  ?></b></td>
		<td align="right"><b><?php echo number_format($st); 	?></b></td>
		<td align="right"><b><?php if($st>0){echo number_format((($s2/$st)-1)*100);}else{echo 0;}?>%</b></td>
		<td align="right"><b><?php if(s1>0){echo number_format((($s2/$s1)-1)*100);}else{echo 0;}		?>%</b></td>
		<!--<td align="right"><b><?php if($st>0){echo number_format(($s2*100)/$st);}else{echo 0;}?>%</b></td>
		<td align="right"><b><?php if(s1>0){echo number_format(($s2*100)/$s1);}else{echo 0;}		?>%</b></td>-->
		<!--<td align="right"><b><?php //echo number_format($tnr2);			?></b></td>
		<td align="right"><b><?php //echo number_format($tnr);			?></b></td>
		<td align="right"><b><?php //echo number_format($trct);			?></b></td>-->
		<!-- <td align="right"><b><?php echo number_format($sf);			?></b></td> modified by Boby 20130520 -->
		<!-- created by Boby 20130520 -->
		<!--<td align="right"><b><?php //echo number_format($tkit2);			?></b></td>
		<td align="right"><b><?php //if($tkit2>0){echo number_format($nr*100/$tkit2);}else{echo 0;}?>%</b></td>-->
		<!-- end created by Boby 20130520 -->
	</tr>
</table>			                

<?php $this->load->view('footer');?>
