	<?php 
	$this->load->view('header');
	$q = 0;
	$s1 = 0;	
	$ts1 = 0;
	$s2 = 0;
	$ts2 = 0;
	$target = 0;
	$flag = 0;
	$flag_ = 0;
	$spt = 0;
	$kit1 = 0;
	$tkit1 = 0;
	$kit2 = 0;
	$tkit2 = 0;
	$nr = 0;
	$nr2 = 0;
	$tnr = 0;
	$sf = 0;
	$tsf = 0;
	$tnr2 = 0;
	$trct = 0;
	
	$st = 0;
	$smtd = 0;
	$svst = 0;
	$nrt = 0;
	$snr = 0;
	$snrvst = 0;
	$sly = 0;
	$svsly = 0;
	$scstc = 0;
	$sastc = 0;
	$sratiostc = 0;
	$sqspm = 0;
	
	$sooam  = 0;
	$soam = 0;
	$sonam = 0;
	$snam = 0;
	$snewsm = 0;
	$snewgm = 0;
	$snewpm = 0;
	$snewspm = 0;
	$soldqspm = 0;
	$snewl = 0;
	$soldql = 0;
	$soldnql = 0;
	$snewsl = 0;
	$soldqsl = 0;
	$soldnqsl = 0;
	$snewstar = 0;
	$soldqstar = 0;
	$soldnqstar = 0;


	$trst = 0;
	$trsmtd = 0;
	$trsvst = 0;
	$trnrt = 0;
	$trsnr = 0;
	$trsnrvst = 0;
	$trsly = 0;
	$trsvsly = 0;
	$trscstc = 0;
	$trsastc = 0;
	$trsratiostc = 0;
	$trsqspm = 0;

	$trsooam  = 0;
	$trsoam = 0;
	$trsonam = 0;
	$trsnam = 0;
	$trsnewsm = 0;
	$trsnewgm = 0;
	$trsnewpm = 0;
	$trsnewspm = 0;
	$trsoldqspm = 0;
	$trsnewl = 0;
	$trsoldql = 0;
	$trsoldnql = 0;
	$trsnewsl = 0;
	$trsoldqsl = 0;
	$trsoldnqsl = 0;
	$trsnewstar = 0;
	$trsoldqstar = 0;
	$trsoldnqstar = 0;

	// pop up attributes
	$atts = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
	// eof pop up attributes
?>
<h2><?php echo $page_title;?></h2>
	<table width="100%">
	<?php echo form_open('report/sls_so_cluster/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
        <tr>
			<td valign='top' width="19%">Tahun</td>
			<td valign='top' width="1%">:</td>
			<td width="80%">
				<?php echo form_dropdown('tahun',$dropdownyear);?>
				<?php echo form_dropdown('quart',$dropdownq);?>
			</td>
		</tr>
        <tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td>
			<?php echo form_submit('submit','preview');?>
			<?php echo form_submit('submit','export');?>
            </td>
		</tr>                
         <?php echo form_close();?>
    <tr><td colspan="3"><hr /></td></tr>
	</table>
	
<table class="stripe" width="100%">
	<tr>
      <th width='25%' rowspan="2">Region</th>
      <th width='15%' rowspan="2"><div align="right">Sales <?php echo $thn - 1;?></div></th>
      <th width='14%' rowspan="2"><div align="right">Sales <?php echo $thn;?></div></th>
      <th width='10%' rowspan="2"><div align="right">Growth <?php echo ' vs '.($thn - 1);?></div></th>
      <th width='14%' rowspan="2"><div align="right">Sales Target</div></th>
      <th width='5%' rowspan="2"><div align="right">Sales Acv</div></th>
      <th width='10%' rowspan="2">Contribution</th>
      <th width='14%' colspan="3"><div align="right">New Active Member</div></th>
      <th width='14%' colspan="3"><div align="right">Old Active Member</div></th>
      <th width='5%' rowspan="2"><div align="right">New SM</div></th>
      <th width='5%' rowspan="2"><div align="right">New GM</div></th>
      <th width='5%' rowspan="2"><div align="right">New PM</div></th>
      <th width='5%' rowspan="2"><div align="right">New SPM</div></th>
      <th width='5%' rowspan="2"><div align="right">Old Q SPM</div></th>
      <th width='5%' rowspan="2"><div align="right">New L</div></th>
      <th width='5%' rowspan="2"><div align="right">Old Q L</div></th>
      <th width='5%' rowspan="2"><div align="right">Old Not-Q L</div></th>
      <th width='5%' rowspan="2"><div align="right">New SL</div></th>
      <th width='5%' rowspan="2"><div align="right">Old Q SL</div></th>
      <th width='5%' rowspan="2"><div align="right">Old Not-Q SL</div></th>
      <th width='5%' rowspan="2"><div align="right">New Star</div></th>
      <th width='5%' rowspan="2"><div align="right">Old Q Star</div></th>
      <th width='5%' rowspan="2"><div align="right">Old Not-Q Star</div></th>
      <th width='5%' rowspan="2"><div align="right">Total Q SPM and Up</div></th>
    </tr>
    <tr>
      <th width='14%'><div align="right">Omset</div></th>
      <th width='14%'><div align="right">Total</div></th>
      <th width='14%'><div align="right">Productivity</div></th>
      <th width='14%'><div align="right">Omset</div></th>
      <th width='14%'><div align="right">Total</div></th>
      <th width='14%'><div align="right">Productivity</div></th>
    </tr>
   
<?php
$curregion = '';
$curromsetnas = 0;
$tcurromsetnas = 0;
$curromsetnasother = 0;
$tcurromsetnasother = 0;
$curromsetnasall = 0;
$tcurromsetnasall = 0;
if ($results): 
	foreach($results as $key => $row): ?>
	<?php		
		if($q!=$row["periode"]){
			$q=$row["periode"];
			/*
			if($results_total){
				foreach($results_total as $keyt => $rowt):
					if($rowt["periode"]==$row["periode"]) {
						if($curromsetnas!=$rowt['omset_nasional']) $tcurromsetnas+=$rowt['omset_nasional'];
						$curromsetnas = $rowt['omset_nasional'];
					}
				endforeach;
			}
			*/
			$curromsetnas = $row['omset_nasional'];
			if($flag>0){
	?>
                     <tr>
                       <td><b>Total <?php echo $curregion; ?></b></td>
                       <td align="right"><b><?php echo number_format($trsly);  ?></b></td>
                       <td align="right"><b><?php echo number_format($trsmtd);  ?></b></td>
                       <td align="right"><b><?php  if($trsly>0){echo number_format((($trsmtd/$trsly)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trst);  ?></b></td>
                       <td align="right"><b><?php if($trst>0){echo number_format((($trsmtd/$trst)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php if($curromsetnasall>0) echo number_format(($trsmtd/$curromsetnasall)*100, 1); else echo '0';?>%</b></td>
                        <td align="right"><b><?php echo number_format($trsonam);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnam);  ?></b></td>
                        <td align="right"><b><?php  if($trsnam>0){echo number_format((($trsonam/$trsnam)));}else{echo 0;}		?>%</b></td>
                        <td align="right"><b><?php echo number_format($trsooam);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoam);  ?></b></td>
                        <td align="right"><b><?php  if($trsoam>0){echo number_format((($trsooam/$trsoam)));}else{echo 0;}		?>%</b></td>
                        <td align="right"><b><?php echo number_format($trsnewsm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewgm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewpm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewspm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldqspm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewl);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldql);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldnql);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewsl);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldqsl);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldnqsl);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewstar);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldqstar);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldnqstar);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsqspm);  ?></b></td>
                    </tr>
                    <?php
					//set sub total region to zero again
					$trst = 0;
					$trsmtd = 0;
					$trsvst = 0;
					$trnrt = 0;
					$trsnr = 0;
					$trsnrvst = 0;
					$trsly = 0;
					$trsvsly = 0;
					$trscstc = 0;
					$trsastc = 0;
					$trsratiostc = 0;
					$trsqspm = 0;
					
					$trsooam  = 0;
					$trsoam = 0;
					$trsonam = 0;
					$trsnam = 0;
					$trsnewsm = 0;
					$trsnewgm = 0;
					$trsnewpm = 0;
					$trsnewspm = 0;
					$trsoldqspm = 0;
					$trsnewl = 0;
					$trsoldql = 0;
					$trsoldnql = 0;
					$trsnewsl = 0;
					$trsoldqsl = 0;
					$trsoldnqsl = 0;
					$trsnewstar = 0;
					$trsoldqstar = 0;
					$trsoldnqstar = 0;
					//eof set sub total region to zero again
					
					?>
	<tr>
		<td><b>Sub Total</b></td>
		<td align="right"><b><?php echo number_format($sly);  ?></b></td>
		<td align="right"><b><?php echo number_format($smtd);  ?></b></td>
		<td align="right"><b><?php  if($sly>0){echo number_format((($smtd/$sly)-1)*100,2);}else{echo 0;}		?>%</b></td>
		<td align="right"><b><?php echo number_format($st);  ?></b></td>
		<td align="right"><b><?php if($st>0){echo number_format((($smtd/$st)-1)*100,2);}else{echo 0;}	$tspt= 0;	$flag_ = 0;?>%</b></td>
		<td align="right"><b><?php echo '100%'; ?></b></td>
		<td align="right"><b><?php echo number_format($sonam);  ?></b></td>
		<td align="right"><b><?php echo number_format($snam);  ?></b></td>
		<td align="right"><b><?php  if($snam>0){echo number_format((($sonam/$snam)));}else{echo 0;}		?>%</b></td>
		<td align="right"><b><?php echo number_format($sooam);  ?></b></td>
		<td align="right"><b><?php echo number_format($soam);  ?></b></td>
		<td align="right"><b><?php  if($soam>0){echo number_format((($sooam/$soam)));}else{echo 0;}		?>%</b></td>
		<td align="right"><b><?php echo number_format($snewsm);  ?></b></td>
		<td align="right"><b><?php echo number_format($snewgm);  ?></b></td>
		<td align="right"><b><?php echo number_format($snewpm);  ?></b></td>
		<td align="right"><b><?php echo number_format($snewspm);  ?></b></td>
		<td align="right"><b><?php echo number_format($soldqspm);  ?></b></td>
		<td align="right"><b><?php echo number_format($snewl);  ?></b></td>
		<td align="right"><b><?php echo number_format($soldql);  ?></b></td>
		<td align="right"><b><?php echo number_format($soldnql);  ?></b></td>
		<td align="right"><b><?php echo number_format($snewsl);  ?></b></td>
		<td align="right"><b><?php echo number_format($soldqsl);  ?></b></td>
		<td align="right"><b><?php echo number_format($soldnqsl);  ?></b></td>
		<td align="right"><b><?php echo number_format($snewstar);  ?></b></td>
		<td align="right"><b><?php echo number_format($soldqstar);  ?></b></td>
		<td align="right"><b><?php echo number_format($soldnqstar);  ?></b></td>
		<td align="right"><b><?php echo number_format($sqspm);  ?></b></td>
		<!--<td align="right"><b><?php //if($tst>0){echo number_format(($ts2*100)/$tst);}else{echo 0;}	$tspt= 0;	$flag_ = 0;?>%</b></td>
		<td align="right"><b><?php //echo number_format(($ts2*100)/$ts1);		?>%</b></td>-->
		<!--<td align="right"><b><?php //echo number_format($nr2);			?></b></td>
		<td align="right"><b><?php //echo number_format($nr);			?></b></td>
		<td align="right"><b><?php //echo number_format($rct);			?></b></td>-->
		<!-- <td align="right"><b><?php //echo number_format($tsf);			?></b></td> modified by Boby 20130520 -->
		<!--<td align="right"><b><?php //echo number_format($kit2);			?></b></td>
		<td align="right"><b><?php //echo number_format($tnr*100/$kit2);			?>%</b></td>-->
	</tr>
	<tr>
		<td colspan="100%" id="reg_separator">&nbsp;</td>
    </tr>
	<?php
				/*
				$ts1 = 0;
				$ts2 = 0;
				$tst = 0;
				$kit1= 0;
				$kit2= 0;
				// $tnr2 = 0;
				// $tnr = 0;
				$tsf = 0;
				$nr = 0;
				$nr2 = 0;
				$rct = 0;
				*/
				
				$st = 0;
				$smtd = 0;
				$svst = 0;
				$nrt = 0;
				$snr = 0;
				$snrvst = 0;
				$sly = 0;
				$svsly = 0;
				$scstc = 0;
				$sastc = 0;
				$sratiostc = 0;
				$sqspm = 0;
				
				$sooam  = 0;
				$soam = 0;
				$sonam = 0;
				$snam = 0;
				$snewsm = 0;
				$snewgm = 0;
				$snewpm = 0;
				$snewspm = 0;
				$soldqspm = 0;
				$snewl = 0;
				$soldql = 0;
				$soldnql = 0;
				$snewsl = 0;
				$soldqsl = 0;
				$soldnqsl = 0;
				$snewstar = 0;
				$soldqstar = 0;
				$soldnqstar = 0;
				
				$curregion = '';
			}$flag = 1;
			$p1=$row["periode"];
			$p2=$period_." ";
			if($period_=="Month"){
				$p1="";
				if($row["periode"]==1){$p2='January';}
				if($row["periode"]==2){$p2='February';}
				if($row["periode"]==3){$p2='March';}
				if($row["periode"]==4){$p2='April';}
				if($row["periode"]==5){$p2='May';}
				if($row["periode"]==6){$p2='June';}
				if($row["periode"]==7){$p2='July';}
				if($row["periode"]==8){$p2='August';}
				if($row["periode"]==9){$p2='September';}
				if($row["periode"]==10){$p2='October';}
				if($row["periode"]==11){$p2='November';}
				if($row["periode"]==12){$p2='December';}
			}
			
			if($period_=="Month"){
				$tglawal = $thn.'-'.$row["periode"].'-01';
				$ts = strtotime('-1 second', strtotime('+1 month',strtotime($tglawal)));
				$tglakhir = date('Y-m-d', $ts);
				
				$tglawally = date('Y-m-d', strtotime('-1 year',strtotime($tglawal)));
				$atsly = strtotime('-1 second', strtotime('+1 month',strtotime($tglawally)));
				$tglakhirly = date('Y-m-d',$tasly);
			}else if($period_=="Quart"){
				$tglawal = $thn.'-'.$row["periode"].'-01';
				$ts = strtotime('-1 second', strtotime('+4 month',strtotime($tglawal)));
				$tglakhir = date('Y-m-d', $ts);
				
				$tglawally = date('Y-m-d', strtotime('-1 year',strtotime($tglawal)));
				$atsly = strtotime('-1 second', strtotime('+1 month',strtotime($tglawally)));
				$tglakhirly = date('Y-m-d',$atsly);
			}
			//echo $tglawal.'<br>';
			//echo $tglakhir.'<br>';
			//echo $tglawally.'<br>';
			//echo $tglakhirly.'<br>';
			if($period_=="Month"){
				$results_other_total=$this->MSales->sales_ro_cluster_rpt_othersales_total($tglawal,$tglakhir, $row["periode"]);
			}else if($period_=="Quart"){
				$results_other_total=$this->MSales->sales_ro_cluster_rpt_othersales_total($tglawal,$tglakhir,'0');
			}
			if($results_other_total){
				foreach($results_other_total as $keyothert => $rowothert):
					if($rowothert["periode"]==$row["periode"]) {
						$curromsetnasother = $rowothert['omset_nasional'];
					}
				endforeach;
			}
			
			if($curromsetnasall!=($curromsetnas + $curromsetnasother)) $tcurromsetnasall+=$curromsetnas + $curromsetnasother;
			$curromsetnasall = $curromsetnas + $curromsetnasother;
			//echo $curromsetnasother;

	?>
	<tr>
		<td colspan="100%"><b><?php echo $p2.$p1;?></b></td>
	</tr>
	<?php } $flag_ += 1;?>
    <?php 
			if($curregion == '') {
				$curregion = $row['region'];
				if ($row['region'] == 'Region 0') 
					echo '<tr><td colspan = "100%"><b>Other Sales (Non PV)</b></td></tr>';
				else
					echo '<tr><td colspan = "100%"><b>'.$row['region'].' - '.$row['region_pic'].'</b></td></tr>';
			} else{
				if ($curregion != $row['region']) {
?>	
                     <tr>
                       <td><b>Total <?php echo $curregion; ?></b></td>
                       <td align="right"><b><?php echo number_format($trsly);  ?></b></td>
                       <td align="right"><b><?php echo number_format($trsmtd);  ?></b></td>
                       <td align="right"><b><?php  if($trsly>0){echo number_format((($trsmtd/$trsly)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trst);  ?></b></td>
                       <td align="right"><b><?php if($trst>0){echo number_format((($trsmtd/$trst)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php if($curromsetnasall>0) echo number_format(($trsmtd/$curromsetnasall)*100, 1); else echo '0';?>%</b></td>
                        <td align="right"><b><?php echo number_format($trsonam);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnam);  ?></b></td>
                        <td align="right"><b><?php  if($trsnam>0){echo number_format((($trsonam/$trsnam)));}else{echo 0;}		?>%</b></td>
                        <td align="right"><b><?php echo number_format($trsooam);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoam);  ?></b></td>
                        <td align="right"><b><?php  if($trsoam>0){echo number_format((($trsooam/$trsoam)));}else{echo 0;}		?>%</b></td>
                        <td align="right"><b><?php echo number_format($trsnewsm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewgm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewpm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewspm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldqspm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewl);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldql);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldnql);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewsl);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldqsl);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldnqsl);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewstar);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldqstar);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldnqstar);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsqspm);  ?></b></td>
                    </tr>
                    <?php
					//set sub total region to zero again
					$trst = 0;
					$trsmtd = 0;
					$trsvst = 0;
					$trnrt = 0;
					$trsnr = 0;
					$trsnrvst = 0;
					$trsly = 0;
					$trsvsly = 0;
					$trscstc = 0;
					$trsastc = 0;
					$trsratiostc = 0;
					$trsqspm = 0;
					
					$trsooam  = 0;
					$trsoam = 0;
					$trsonam = 0;
					$trsnam = 0;
					$trsnewsm = 0;
					$trsnewgm = 0;
					$trsnewpm = 0;
					$trsnewspm = 0;
					$trsoldqspm = 0;
					$trsnewl = 0;
					$trsoldql = 0;
					$trsoldnql = 0;
					$trsnewsl = 0;
					$trsoldqsl = 0;
					$trsoldnqsl = 0;
					$trsnewstar = 0;
					$trsoldqstar = 0;
					$trsoldnqstar = 0;
					//eof set sub total region to zero again

					$curregion = $row['region'];
					if ($row['region'] == 'Region 0') {
						echo '<tr><td colspan = "100%"><b>Other Sales</b></td></tr>';

					}
					else{
						echo '<tr><td colspan = "100%"><b>'.$row['region'].' - '.$row['region_pic'].'</b></td></tr>';
					}
				}
			}

	?>
    <tr>
		<td class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo 'Staff Order'; else echo anchor_popup('search/search_rpt_cluster/indexso/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/no/no', $row['cluster_alias'], $atts);?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['omset_ytd']); else echo anchor_popup('search/search_rpt_cluster/saleslastyear/'.$tglawally.'/'.$tglakhirly.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes', number_format($row['omset_ytd']), $atts);			$sly+=$row['omset_ytd'];			$trsly+=$row['omset_ytd'];		$tsly+=$row['omset_ytd']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['omset_mtd']); else echo anchor_popup('search/search_rpt_cluster/indexso/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes/no', number_format($row['omset_mtd']), $atts);			$smtd+=$row['omset_mtd'];			$trsmtd+=$row['omset_mtd'];		$tsmtd+=$row['omset_mtd']; ?></td>
		<td align="right"><?php echo number_format($row['vsly']);			$svsly+=$row['vsly'];		$tsvsly+=$row['vsly'];?>%</td>
		<td align="right"><?php echo number_format($row['sales_target']);			$st+=$row['sales_target'];			$trst+=$row['sales_target'];		$tst+=$row['sales_target']; ?></td>
		<td align="right"><?php echo number_format($row['vstarget']);			$svst+=$row['vstarget'];		$tsvst+=$row['vstarget'];?>%</td>
		<td align="right"><?php if($curromsetnasall>0) echo number_format(($row['omset_mtd']/$curromsetnasall)*100, 1); else echo '0';?>%</td>
		<td align="right"><?php echo number_format($row['omset_nam']);			$sonam+=$row['omset_nam'];			$trsonam+=$row['omset_nam'];		$tsonam+=$row['omset_nam']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['nam']); else echo anchor_popup('search/search_rpt_cluster/nam/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes', number_format($row['nam']), $atts);			$snam+=$row['nam'];			$trsnam+=$row['nam'];		$tsnam+=$row['nam']; ?></td>
		<td align="right"><?php echo number_format($row['nam_productivity']);			$snamprod+=$row['nam_productivity'];		$tsnamprod+=$row['nam_productivity'];?></td>
		<td align="right"><?php echo number_format($row['omset_oam']);			$sooam+=$row['omset_oam'];			$trsooam+=$row['omset_oam'];		$tsooam+=$row['omset_oam']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['oam']); else echo anchor_popup('search/search_rpt_cluster/oam/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes', number_format($row['oam']), $atts);			$soam+=$row['oam'];			$trsoam+=$row['oam'];		$tsoam+=$row['oam']; ?></td>
		<td align="right"><?php echo number_format($row['oam_productivity']);			$soamprod+=$row['oam_productivity'];		$tsoamprod+=$row['oam_productivity'];?></td>
		<!--<td align="right"><?php //echo number_format($row['omset_ytd']);			$sly+=$row['omset_ytd'];			$trsly+=$row['omset_ytd'];		$tsly+=$row['omset_ytd']; ?></td>-->
		<!--
        <td align="right"><?php //echo '<a href="#">'.number_format($row['currstc']).'</a>';			$scstc+=$row['currstc'];		$tscstc+=$row['currstc']; ?></td>
		<td align="right"><?php //echo '<a href="#">'.number_format($row['actstc']).'</a>';			$sastc+=$row['actstc'];		$tsastc+=$row['actstc']; ?></td>
        -->
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['newsm']); else echo anchor_popup('search/search_rpt_cluster/newjenjang/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes/2', number_format($row['newsm']), $atts);			$snewsm+=$row['newsm'];			$trsnewsm+=$row['newsm'];		$tsnewsm+=$row['newsm']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['newgm']); else echo anchor_popup('search/search_rpt_cluster/newjenjang/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes/3', number_format($row['newgm']), $atts);			$snewgm+=$row['newgm'];			$trsnewgm+=$row['newgm'];		$tsnewgm+=$row['newgm']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['newpm']); else echo anchor_popup('search/search_rpt_cluster/newjenjang/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes/4', number_format($row['newpm']), $atts);			$snewpm+=$row['newpm'];			$trsnewpm+=$row['newpm'];		$tsnewpm+=$row['newpm']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['newspm']); else echo anchor_popup('search/search_rpt_cluster/newspm/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes', number_format($row['newspm']), $atts);			$snewspm+=$row['newspm'];			$trsnewspm+=$row['newspm'];		$tsnewspm+=$row['newspm']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['oldqspm']); else echo anchor_popup('search/search_rpt_cluster/oldqspm/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes', number_format($row['oldqspm']), $atts);			$soldqspm+=$row['oldqspm'];			$trsoldqspm+=$row['oldqspm'];		$tsoldqspm+=$row['oldqspm']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['newl']); else echo anchor_popup('search/search_rpt_cluster/newjenjang/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes/5', number_format($row['newl']), $atts);			$snewl+=$row['newl'];			$trsnewl+=$row['newl'];		$tsnewl+=$row['newl']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['oldql']); else echo anchor_popup('search/search_rpt_cluster/oldql/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes', number_format($row['oldql']), $atts);			$soldql+=$row['oldql'];			$trsoldql+=$row['oldql'];		$tsoldql+=$row['oldql']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['oldnql']); else echo anchor_popup('search/search_rpt_cluster/oldnql/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes', number_format($row['oldnql']), $atts);			$soldnql+=$row['oldnql'];			$trsoldnql+=$row['oldnql'];		$tsoldnql+=$row['oldnql']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['newsl']); else echo anchor_popup('search/search_rpt_cluster/newjenjang/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes/6', number_format($row['newsl']), $atts);			$snewsl+=$row['newsl'];			$trsnewsl+=$row['newsl'];		$tsnewsl+=$row['newsl']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['oldqsl']); else echo anchor_popup('search/search_rpt_cluster/oldqsl/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes', number_format($row['oldqsl']), $atts);			$soldqsl+=$row['oldqsl'];			$trsoldqsl+=$row['oldqsl'];		$tsoldqsl+=$row['oldqsl']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['oldnqsl']); else echo anchor_popup('search/search_rpt_cluster/oldnqsl/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes', number_format($row['oldnqsl']), $atts);			$soldnqsl+=$row['oldnqsl'];			$trsoldnqsl+=$row['oldnqsl'];		$tsoldnqsl+=$row['oldnqsl']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['newstar']); else echo anchor_popup('search/search_rpt_cluster/newjenjang/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes/7', number_format($row['newstar']), $atts);			$snewstar+=$row['newstar'];			$trsnewstar+=$row['newstar'];		$tsnewstar+=$row['newstar']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['oldqstar']); else echo anchor_popup('search/search_rpt_cluster/oldqstar/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes', number_format($row['oldqstar']), $atts);		$soldqstar+=$row['oldqstar'];			$trsoldqstar+=$row['oldqstar'];		$tsoldqstar+=$row['oldqstar']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['oldnqstar']); else echo anchor_popup('search/search_rpt_cluster/oldnqstar/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes', number_format($row['oldnqstar']), $atts);		$soldnqstar+=$row['oldnqstar'];		$trsoldnqstar+=$row['oldnqstar'];	$tsoldnqstar+=$row['oldnqstar']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['qspmup']); else echo anchor_popup('search/search_rpt_cluster/qspmup/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes', number_format($row['qspmup']), $atts);			$sqspm+=$row['qspmup'];			$trsqspm+=$row['qspmup'];		$tsqspm+=$row['qspmup']; ?></td>
		<?php
			
		?>
        <!--
		<td align="right"><?php //echo number_format($row['nr2']);			$nr2+=$row['nr2'];			$tnr2+=$row['nr2'];?></td>
		<td align="right"><?php //echo number_format($row['kit2']);			$nr+=$row['kit2'];			$tnr+=$row['kit2'];?></td>
		<td align="right"><?php //echo number_format($row['recruit']);		$rct += $row['recruit'];	$trct+=$row['recruit'];?></td>-->
		<!-- <td align="right"><?php //echo number_format($row['sf']);				$sf+=$row['sf']; 			$tsf+=$row['sf'];?></td> -->
		<!-- created by Boby 20130520 -->
		<!--<td align="right"><?php //echo number_format($row['nr']);			$kit2+=$row['nr'];			$tkit2+=$row['nr'];?></td>
		<td align="right"><?php //if($row['nr']>0){echo number_format($row['kit2']*100/$row['nr']);}else{echo 0;} ?>%</td>-->
		<!-- end created by Boby 20130520 -->
    </tr>
    <?php 
	//if($curregion == 'Region 0'){
	if($curregion == 'Region 4'){
		/*
		// Other Sales
		$resultOther = $this->MSales->sales_ro_cluster_rpt_othersales($tglawal,$tglakhir);
		foreach ($resultOther as $rowo):
		*/
	?>
	<!--
            <tr>
                <td class="linkpopup"><?php echo anchor_popup('search/search_rpt_cluster/other/'.$tglawal.'/'.$tglakhir.'/'.$rowo['item_group'], $rowo['item_group'], $atts);?></td>
                <td align="right"><?php if($curromsetnasall>0) echo number_format(($rowo['omset']/$curromsetnasall)*100, 1); else echo '0';?>%</td>
                <td align="right"><?php echo '0'; ?></td>
                <td align="right" class="linkpopup"><?php echo anchor_popup('search/search_rpt_cluster/other/'.$tglawal.'/'.$tglakhir.'/'.$rowo['item_group'], number_format($rowo['omset']), $atts);			$smtd+=$rowo['omset'];			$trsmtd+=$rowo['omset'];		$tsmtd+=$rowo['omset']; ?></td>
                <td align="right"><?php echo '0';?>%</td>
                <td align="right"><?php echo '0';?></td>
                <td align="right"><?php echo '0';?></td>
                <td align="right"><?php echo '0';?>%</td>
                <td align="right" class="linkpopup"><?php echo anchor_popup('search/search_rpt_cluster/other/'.$tglawally.'/'.$tglakhirly.'/'.$rowo['item_group'], number_format($rowo['omset_ly']), $atts);			$sly+=$rowo['omset_ly'];			$trsly+=$rowo['omset_ly'];		$tsly+=$rowo['omset_ly']; ?></td>
                <td align="right"><?php echo number_format($rowo['vsly']);			$svsly+=$rowo['vsly'];		$tsvsly+=$rowo['vsly'];?>%</td>
                <td align="right"><?php echo '0'; ?></td>
                <td align="right"><?php echo '0'; ?></td>
                <td align="right"><?php echo '0';?>%</td>
                <td align="right" class="linkpopup"><?php echo '0'; ?></td>
            </tr>
    -->
    <?php
    		/*
			endforeach;
			// oef other sales
			*/
	?>	
					<!--
                     <tr>
                       <td><b>Total <?php echo 'Other Sales'; ?></b></td>
                       <td align="right"><b><?php if($curromsetnasall>0) echo number_format(($trsmtd/$curromsetnasall)*100, 1); else echo '0';?>%</b></td>
                       <td align="right"><b><?php echo number_format($trst);  ?></b></td>
                       <td align="right"><b><?php echo number_format($trsmtd);  ?></b></td>
                       <td align="right"><b><?php if($trst>0){echo number_format((($trsmtd/$trst)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trnrt);  ?></b></td>
                       <td align="right"><b><?php echo number_format($trsnr);  ?></b></td>
                       <td align="right"><b><?php  if($trnrt>0){echo number_format((($trsnr/$trnrt)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trsly);  ?></b></td>
                       <td align="right"><b><?php  if($trsly>0){echo number_format((($trsmtd/$trsly)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trscstc);  ?></b></td>
                       <td align="right"><b><?php echo number_format($trsastc);  ?></b></td>
                       <td align="right"><b><?php  if($trsastc>0){echo number_format((($trsastc/$trscstc))*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trsqspm);  ?></b></td>
                    </tr>
                    -->
                    <?php
					//set sub total region to zero again
					$trst = 0;
					$trsmtd = 0;
					$trsvst = 0;
					$trnrt = 0;
					$trsnr = 0;
					$trsnrvst = 0;
					$trsly = 0;
					$trsvsly = 0;
					$trscstc = 0;
					$trsastc = 0;
					$trsratiostc = 0;
					$trsqspm = 0;
					
					$trsooam  = 0;
					$trsoam = 0;
					$trsonam = 0;
					$trsnam = 0;
					$trsnewsm = 0;
					$trsnewgm = 0;
					$trsnewpm = 0;
					$trsnewspm = 0;
					$trsoldqspm = 0;
					$trsnewl = 0;
					$trsoldql = 0;
					$trsoldnql = 0;
					$trsnewsl = 0;
					$trsoldqsl = 0;
					$trsoldnqsl = 0;
					$trsnewstar = 0;
					$trsoldqstar = 0;
					$trsoldnqstar = 0;

					//eof set sub total region to zero again

					$curregion = $row['region'];
	}
	endforeach; 
	?>
                     <tr>
                       <td><b>Total <?php echo $curregion; ?></b></td>
                       <td align="right"><b><?php echo number_format($trsly);  ?></b></td>
                       <td align="right"><b><?php echo number_format($trsmtd);  ?></b></td>
                       <td align="right"><b><?php  if($trsly>0){echo number_format((($trsmtd/$trsly)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trst);  ?></b></td>
                       <td align="right"><b><?php if($trst>0){echo number_format((($trsmtd/$trst)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php if($curromsetnasall>0) echo number_format(($trsmtd/$curromsetnasall)*100, 1); else echo '0';?>%</b></td>
                        <td align="right"><b><?php echo number_format($trsonam);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnam);  ?></b></td>
                        <td align="right"><b><?php  if($trsnam>0){echo number_format((($trsonam/$trsnam)));}else{echo 0;}		?>%</b></td>
                        <td align="right"><b><?php echo number_format($trsooam);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoam);  ?></b></td>
                        <td align="right"><b><?php  if($trsoam>0){echo number_format((($trsooam/$trsoam)));}else{echo 0;}		?>%</b></td>
                        <td align="right"><b><?php echo number_format($trsnewsm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewgm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewpm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewspm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldqspm);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewl);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldql);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldnql);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewsl);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldqsl);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldnqsl);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsnewstar);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldqstar);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsoldnqstar);  ?></b></td>
                        <td align="right"><b><?php echo number_format($trsqspm);  ?></b></td>
                    </tr>
                    <?php
					//set sub total region to zero again
					$trst = 0;
					$trsmtd = 0;
					$trsvst = 0;
					$trnrt = 0;
					$trsnr = 0;
					$trsnrvst = 0;
					$trsly = 0;
					$trsvsly = 0;
					$trscstc = 0;
					$trsastc = 0;
					$trsratiostc = 0;
					$trsqspm = 0;
					
					$trsooam  = 0;
					$trsoam = 0;
					$trsonam = 0;
					$trsnam = 0;
					$trsnewsm = 0;
					$trsnewgm = 0;
					$trsnewpm = 0;
					$trsnewspm = 0;
					$trsoldqspm = 0;
					$trsnewl = 0;
					$trsoldql = 0;
					$trsoldnql = 0;
					$trsnewsl = 0;
					$trsoldqsl = 0;
					$trsoldnqsl = 0;
					$trsnewstar = 0;
					$trsoldqstar = 0;
					$trsoldnqstar = 0;
					//eof set sub total region to zero again
					?>
	<tr>
		<td><b>Sub Total</b></td>
		<td align="right"><b><?php echo number_format($sly);  ?></b></td>
		<td align="right"><b><?php echo number_format($smtd);  ?></b></td>
		<td align="right"><b><?php  if($sly>0){echo number_format((($smtd/$sly)-1)*100,2);}else{echo 0;}		?>%</b></td>
		<td align="right"><b><?php echo number_format($st);  ?></b></td>
		<td align="right"><b><?php if($st>0){echo number_format((($smtd/$st)-1)*100,2);}else{echo 0;}	$tspt= 0;	$flag_ = 0;?>%</b></td>
		<td align="right"><b><?php echo '100%'; ?></b></td>
		<td align="right"><b><?php echo number_format($sonam);  ?></b></td>
		<td align="right"><b><?php echo number_format($snam);  ?></b></td>
		<td align="right"><b><?php  if($snam>0){echo number_format((($sonam/$snam)));}else{echo 0;}		?></b></td>
		<td align="right"><b><?php echo number_format($sooam);  ?></b></td>
		<td align="right"><b><?php echo number_format($soam);  ?></b></td>
		<td align="right"><b><?php  if($soam>0){echo number_format((($sooam/$soam)));}else{echo 0;}		?></b></td>
		<td align="right"><b><?php echo number_format($snewsm);  ?></b></td>
		<td align="right"><b><?php echo number_format($snewgm);  ?></b></td>
		<td align="right"><b><?php echo number_format($snewpm);  ?></b></td>
		<td align="right"><b><?php echo number_format($snewspm);  ?></b></td>
		<td align="right"><b><?php echo number_format($soldqspm);  ?></b></td>
		<td align="right"><b><?php echo number_format($snewl);  ?></b></td>
		<td align="right"><b><?php echo number_format($soldql);  ?></b></td>
		<td align="right"><b><?php echo number_format($soldnql);  ?></b></td>
		<td align="right"><b><?php echo number_format($snewsl);  ?></b></td>
		<td align="right"><b><?php echo number_format($soldqsl);  ?></b></td>
		<td align="right"><b><?php echo number_format($soldnqsl);  ?></b></td>
		<td align="right"><b><?php echo number_format($snewstar);  ?></b></td>
		<td align="right"><b><?php echo number_format($soldqstar);  ?></b></td>
		<td align="right"><b><?php echo number_format($soldnqstar);  ?></b></td>
		<td align="right"><b><?php echo number_format($sqspm);  ?></b></td>
		<!--<td align="right"><b><?php //if($tst>0){echo number_format(($ts2*100)/$tst);}else{echo 0;}	$tspt= 0;	$flag_ = 0;?>%</b></td>
		<td align="right"><b><?php //echo number_format(($ts2*100)/$ts1);		?>%</b></td>-->
		<!--<td align="right"><b><?php //echo number_format($nr2);			?></b></td>
		<td align="right"><b><?php //echo number_format($nr);			?></b></td>
		<td align="right"><b><?php //echo number_format($rct);			?></b></td>-->
		<!-- <td align="right"><b><?php //echo number_format($tsf);			?></b></td> modified by Boby 20130520 -->
		<!--<td align="right"><b><?php //echo number_format($kit2);			?></b></td>
		<td align="right"><b><?php //echo number_format($tnr*100/$kit2);			?>%</b></td>-->
	</tr>
	
	<tr>
		<td colspan="100%" id="reg_separator">&nbsp;</td>
    </tr>
<?php else: ?>
    <tr>
		<td colspan="100%">Data is not available.</td>
    </tr>
<?php endif; ?>
	<tr>
		<td ><b>Grand Total</b></td>
		<td align="right"><b><?php echo number_format($tsly);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsmtd);  ?></b></td>
		<td align="right"><b><?php  if($tsly>0){echo number_format((($tsmtd/$tsly)-1)*100,2);}else{echo 0;}		?>%</b></td>
		<td align="right"><b><?php echo number_format($tst);  ?></b></td>
		<td align="right"><b><?php if($tst>0){echo number_format((($tsmtd/$tst)-1)*100,2);}else{echo 0;}?>%</b></td>
		<td align="right"><b><?php if($tcurromsetnasall>0) echo number_format(($tsmtd/$tcurromsetnasall)*100, 1); else echo '0';  ?>%</b></td>
		<td align="right"><b><?php echo number_format($tsonam);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsnam);  ?></b></td>
		<td align="right"><b><?php  if($tsnam>0){echo number_format((($tsonam/$tsnam)));}else{echo 0;}		?></b></td>
		<td align="right"><b><?php echo number_format($tsooam);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsoam);  ?></b></td>
		<td align="right"><b><?php  if($tsoam>0){echo number_format((($tsooam/$tsoam)));}else{echo 0;}		?></b></td>
		<td align="right"><b><?php echo number_format($tsnewsm);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsnewgm);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsnewpm);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsnewspm);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsoldqspm);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsnewl);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsoldql);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsoldnql);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsnewsl);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsoldqsl);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsoldnqsl);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsnewstar);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsoldqstar);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsoldnqstar);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsqspm);  ?></b></td>
		<!--<td align="right"><b><?php //if($tst>0){echo number_format(($ts2*100)/$tst);}else{echo 0;}	$tspt= 0;	$flag_ = 0;?>%</b></td>
		<td align="right"><b><?php //echo number_format(($ts2*100)/$ts1);		?>%</b></td>-->
		<!--<td align="right"><b><?php //echo number_format($nr2);			?></b></td>
		<td align="right"><b><?php //echo number_format($nr);			?></b></td>
		<td align="right"><b><?php //echo number_format($rct);			?></b></td>-->
		<!-- <td align="right"><b><?php //echo number_format($tsf);			?></b></td> modified by Boby 20130520 -->
		<!--<td align="right"><b><?php //echo number_format($kit2);			?></b></td>
		<td align="right"><b><?php //echo number_format($tnr*100/$kit2);			?>%</b></td>-->
	</tr>
</table>			                

<?php $this->load->view('footer');?>
