<?php 
	  header('Content-type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment; filename=stc_acv_yearly_'.date('YmdHis').'.xls');
	//$this->load->view('header');
	$trg = 0; 	$trg1 = 30000000;	$trg2 = 5000000;
	$sls1 = 0;	$trgt1 = 0;	$t1=0;
	$sls2 = 0;	$trgt2 = 0; $t2=0;
	$sls3 = 0;	$trgt3 = 0; $t3=0;
	$sls4 = 0;	$trgt4 = 0; $t4=0;
	$sls5 = 0;	$trgt5 = 0; $t5=0;
	$sls6 = 0;	$trgt6 = 0; $t6=0;
	$sls7 = 0;	$trgt7 = 0; $t7=0;
	$sls8 = 0;	$trgt8 = 0; $t8=0;
	$sls9 = 0;	$trgt9 = 0; $t9=0;
	$sls10 = 0;	$trgt10 = 0; $t10=0;
	$sls11 = 0;	$trgt11 = 0; $t11=0;
	$sls12 = 0;	$trgt12 = 0; $t12=0;
?>
<table border="1" bordercolor="#0000FF" class="stripe">
	<tr>
	  <th>No </th>
	  <th>Stc_No </th>
	  <th>Stockiest</th>
	  <th>Reg</th>
	  <th>January</th>
	  <th>February</th>
	  <th>Maret</th>
	  <th>April</th>
	  <th>May</th>
	  <th>Juni</th>
	  <th>July</th>
	  <th>August</th>
	  <th>September</th>
	  <th>October</th>
	  <th>November</th>
	  <th>December</th>
	</tr>
   
<?php
if ($results): 
	$i=0;
	foreach($results as $key => $row): 
		$i++;
		$periode = substr($row['periode'], -5, 2);
		if($periode<=3 && $periode>0)$periode="Quart1";
		if($periode<=6 && $periode>3)$periode="Quart2";
		if($periode<=9 && $periode>6)$periode="Quart3";
		if($periode<=12 && $periode>9)$periode="Quart4";
		if($row['tipe']==1){$trg=$trg1;}else{$trg=$trg2;}
?>
    <tr>
		<td align="right"><?php echo $i;?></td>
		<td><?php echo $row['no_stc'];?></td>
		<td><?php echo $row['nama'];?></td>
		<td><?php echo $row['region'];?></td>
		<td align="right"><?php echo number_format($row['oms1']); $sls1+=$row['oms1']; ?></td>
		<td align="right"><?php echo number_format($row['oms2']); $sls2+=$row['oms2']; ?></td>
		<td align="right"><?php echo number_format($row['oms3']); $sls3+=$row['oms3']; ?></td>
		<td align="right"><?php echo number_format($row['oms4']); $sls4+=$row['oms4']; ?></td>
		<td align="right"><?php echo number_format($row['oms5']); $sls5+=$row['oms5']; ?></td>
		<td align="right"><?php echo number_format($row['oms6']); $sls6+=$row['oms6']; ?></td>
		<td align="right"><?php echo number_format($row['oms7']); $sls7+=$row['oms7']; ?></td>
		<td align="right"><?php echo number_format($row['oms8']); $sls8+=$row['oms8']; ?></td>
		<td align="right"><?php echo number_format($row['oms9']); $sls9+=$row['oms9']; ?></td>
		<td align="right"><?php echo number_format($row['oms10']); $sls10+=$row['oms10']; ?></td>
		<td align="right"><?php echo number_format($row['oms11']); $sls11+=$row['oms11']; ?></td>
		<td align="right"><?php echo number_format($row['oms12']); $sls12+=$row['oms12']; ?></td>
    </tr>
    <?php endforeach; 
else: ?>
    <tr>
		<td colspan="11">Data is not available.</td>
    </tr>
<?php endif; ?> 
	<tr>
		<td colspan="4"><b>Total</b></td>
		<td align="right"><b><?php echo number_format($sls1);?></b></td>
		<td align="right"><b><?php echo number_format($sls2);?></b></td>
		<td align="right"><b><?php echo number_format($sls3);?></b></td>
		<td align="right"><b><?php echo number_format($sls4);?></b></td>
		<td align="right"><b><?php echo number_format($sls5);?></b></td>
		<td align="right"><b><?php echo number_format($sls6);?></b></td>
		<td align="right"><b><?php echo number_format($sls7);?></b></td>
		<td align="right"><b><?php echo number_format($sls8);?></b></td>
		<td align="right"><b><?php echo number_format($sls9);?></b></td>
		<td align="right"><b><?php echo number_format($sls10);?></b></td>
		<td align="right"><b><?php echo number_format($sls11);?></b></td>
		<td align="right"><b><?php echo number_format($sls12);?></b></td>
    </tr>
</table>			                
<?php //$this->load->view('footer');?>
