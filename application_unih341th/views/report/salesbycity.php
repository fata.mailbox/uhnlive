<?php 
	$this->load->view('header');
	
?>
<h2><?php echo $page_title;?></h2>
	<table width="100%">
	<?php echo form_open('report/salesbycity/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
        <tr>
			<td valign='top' width="19%">Tahun</td>
			<td valign='top' width="1%">:</td>
			<td width="80%">
				<?php echo form_dropdown('tahun',$dropdownyear);?>
				<?php //echo form_dropdown('quart',$dropdownq);?>
			</td>
		</tr>
        <tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><?php echo form_submit('submit','preview');?>
            <?php
			/*
			if ($results){
			echo '<a href="'.$this->baseurl.'salesbycity/exportxls/'.$thn.'" target="_blank">Export To XLS</a>';	
			}
			*/
			?>
            </td>
		</tr>                
         <?php echo form_close();?>
    <tr><td colspan="3"><hr /></td></tr>
	</table>
<table class="stripe" width="100%" border="1px" bordercolor="#0000FF">
	<tr>
      <th width='5%'>No</th>
      <th width='15%'>Province</th>
      <th width='15%'>City</th>
      <th width='5%'><div align="right">January</div></th>
      <th width='5%'><div align="right">February</div></th>
      <th width='5%'><div align="right">March</div></th>
      <th width='5%'><div align="right">April</div></th>
      <th width='5%'><div align="right">May</div></th>
	  <th width='5%'><div align="right">June</div></th>
	  <th width='5%'><div align="right">July</div></th>
	  <th width='5%'><div align="right">August</div></th>
	  <th width='5%'><div align="right">September</div></th>
	  <th width='5%'><div align="right">October</div></th>
	  <th width='5%'><div align="right">November</div></th>
	  <th width='5%'><div align="right">December</div></th>
    </tr>
    <?php
	if ($results){ 
	$no = 1;
	$ts1 = 0;
	$ts2 = 0;
	$ts3 = 0;
	$ts4 = 0;
	$ts5 = 0;
	$ts6 = 0;
	$ts7 = 0;
	$ts8 = 0;
	$ts9 = 0;
	$ts10 = 0;
	$ts11 = 0;
	$ts12 = 0;
	foreach($results as $key => $row):
	?>
    <tr>
		<td><?php echo $no;?></td>
		<td><?php echo $row['propinsi'];?></td>
		<td><?php echo $row['kota'];?></td>
		<td align="right"><?php echo number_format($row['omset1']);	$ts1+=$row['omset1'];?></td>
		<td align="right"><?php echo number_format($row['omset2']);	$ts2+=$row['omset2'];?></td>
		<td align="right"><?php echo number_format($row['omset3']);	$ts3+=$row['omset3'];?></td>
		<td align="right"><?php echo number_format($row['omset4']);	$ts4+=$row['omset4'];?></td>
		<td align="right"><?php echo number_format($row['omset5']);	$ts5+=$row['omset5'];?></td>
		<td align="right"><?php echo number_format($row['omset6']);	$ts6+=$row['omset6'];?></td>
		<td align="right"><?php echo number_format($row['omset7']);	$ts7+=$row['omset7'];?></td>
		<td align="right"><?php echo number_format($row['omset8']);	$ts8+=$row['omset8'];?></td>
		<td align="right"><?php echo number_format($row['omset9']);	$ts9+=$row['omset9'];?></td>
		<td align="right"><?php echo number_format($row['omset10']); $ts10+=$row['omset10'];?></td>
		<td align="right"><?php echo number_format($row['omset11']); $ts11+=$row['omset11'];?></td>
		<td align="right"><?php echo number_format($row['omset12']); $ts12+=$row['omset12'];?></td>
     </tr>
     <?php
	 $no++;
	endforeach;
	?>
    <tr style="font-weight:bold;">
		<td colspan="3"><?php echo 'Total';?></td>
		<td align="right"><?php echo number_format($ts1);?></td>
		<td align="right"><?php echo number_format($ts2);?></td>
		<td align="right"><?php echo number_format($ts3);?></td>
		<td align="right"><?php echo number_format($ts4);?></td>
		<td align="right"><?php echo number_format($ts5);?></td>
		<td align="right"><?php echo number_format($ts6);?></td>
		<td align="right"><?php echo number_format($ts7);?></td>
		<td align="right"><?php echo number_format($ts8);?></td>
		<td align="right"><?php echo number_format($ts9);?></td>
		<td align="right"><?php echo number_format($ts10);?></td>
		<td align="right"><?php echo number_format($ts11);?></td>
		<td align="right"><?php echo number_format($ts12);?></td>
     </tr>
	<?php
	}
	?>
</table>
<?php $this->load->view('footer');?>