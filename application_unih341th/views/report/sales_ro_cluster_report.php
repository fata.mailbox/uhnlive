	<?php 
	$this->load->view('header');
	$q = 0;
	$s1 = 0;	
	$ts1 = 0;
	$s2 = 0;
	$ts2 = 0;
	$target = 0;
	$flag = 0;
	$flag_ = 0;
	$spt = 0;
	$kit1 = 0;
	$tkit1 = 0;
	$kit2 = 0;
	$tkit2 = 0;
	$nr = 0;
	$nr2 = 0;
	$tnr = 0;
	$sf = 0;
	$tsf = 0;
	$tnr2 = 0;
	$trct = 0;
	
	$st = 0;
	$smtd = 0;
	$svst = 0;
	$nrt = 0;
	$snr = 0;
	$snrvst = 0;
	$sly = 0;
	$svsly = 0;
	$scstc = 0;
	$sastc = 0;
	$sratiostc = 0;
	$sqspm = 0;

	$trst = 0;
	$trsmtd = 0;
	$trsvst = 0;
	$trnrt = 0;
	$trsnr = 0;
	$trsnrvst = 0;
	$trsly = 0;
	$trsvsly = 0;
	$trscstc = 0;
	$trsastc = 0;
	$trsratiostc = 0;
	$trsqspm = 0;


	// pop up attributes
	$atts = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
	// eof pop up attributes
?>
<h2><?php echo $page_title;?></h2>
	<table width="100%">
	<?php echo form_open('report/sls_ro_cluster/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
        <tr>
			<td valign='top' width="19%">Tahun</td>
			<td valign='top' width="1%">:</td>
			<td width="80%">
				<?php echo form_dropdown('tahun',$dropdownyear);?>
				<?php echo form_dropdown('quart',$dropdownq);?>
			</td>
		</tr>
        <tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td>
			<?php echo form_submit('submit','preview');?>
			<?php //echo form_submit('submit','export');?>
            </td>
		</tr>                
         <?php echo form_close();?>
    <tr><td colspan="3"><hr /></td></tr>
	</table>
	
<table class="stripe" width="100%">
	<tr>
      <th width='25%'>Region</th>
      <th width='10%'>Contribution</th>
      <th width='14%'><div align="right">Sales Target</div></th>
      <th width='14%'><div align="right">Sales <?php echo $thn;?></div></th>
      <th width='5%'><div align="right">Sales Acv</div></th>
      <th width='14%'><div align="right">New Recruit Target</div></th>
      <th width='14%'><div align="right">New Recruit</div></th>
      <th width='5%'><div align="right">New Recruit Acv</div></th>
      <th width='15%'><div align="right">Sales <?php echo $thn - 1;?></div></th>
      <th width='10%'><div align="right">Growth <?php echo ' vs '.($thn - 1);?></div></th>
      <th width='15%'><div align="right">Total Existing Stockiest</div></th>
      <th width='15%'><div align="right">Number of Active Stockiest</div></th>
      <th width='15%'><div align="right">Ratio Act Stc</div></th>
      <th width='15%'><div align="right">Total Qualified SPM and Up</div></th>
	  <!--<th width='5%'><div align="right">New Recruit</div></th>
	  <th width='5%'><div align="right">New Member</div></th>
	  <th width='5%'><div align="right">Recruitor</div></th>-->
	  <!-- <th width='10%'><div align="right">Sales Force</div></th> modified by Boby 20130520 -->
	  <!-- Created by Boby 20130520 -->
	  <!--<th width='5%'><div align="right">NM Target</div></th>
	  <th width='5%'><div align="right">NM Acv</div></th>-->
	  <!-- End created by Boby 20130520 -->
    </tr>
   
<?php
$curregion = '';
$thereg = '';
$curromsetnas = 0;
$tcurromsetnas = 0;
$curromsetnasother = 0;
$tcurromsetnasother = 0;
$curromsetnasall = 0;
$tcurromsetnasall = 0;
if ($results): 
	foreach($results as $key => $row): ?>
	<?php		
		if($q!=$row["periode"]){
			$q=$row["periode"];
			
			if($results_total){
				foreach($results_total as $keyt => $rowt):
					if($rowt["periode"]==$row["periode"]) {
						if($curromsetnas!=$rowt['omset_nasional']) $tcurromsetnas+=$rowt['omset_nasional'];
						$curromsetnas = $rowt['omset_nasional'];
					}
				endforeach;
			}
			
			if($flag>0){
	?>

	<tr>
		<td><b>Sub Total</b></td>
		<td align="right"><b><?php echo '100%'; ?></b></td>
		<td align="right"><b><?php echo number_format($st);  ?></b></td>
		<td align="right"><b><?php echo number_format($smtd);  ?></b></td>
		<td align="right"><b><?php if($st>0){echo number_format((($smtd/$st)-1)*100,2);}else{echo 0;}	$tspt= 0;	$flag_ = 0;?>%</b></td>
		<td align="right"><b><?php echo number_format($nrt);  ?></b></td>
		<td align="right"><b><?php echo number_format($snr);  ?></b></td>
		<td align="right"><b><?php  if($nrt>0){echo number_format((($snr/$nrt)-1)*100,2);}else{echo 0;}		?>%</b></td>
		<td align="right"><b><?php echo number_format($sly);  ?></b></td>
		<td align="right"><b><?php  if($sly>0){echo number_format((($smtd/$sly)-1)*100,2);}else{echo 0;}		?>%</b></td>
		<td align="right"><b><?php echo number_format($scstc);  ?></b></td>
		<td align="right"><b><?php echo number_format($sastc);  ?></b></td>
		<td align="right"><b><?php  if($sastc>0){echo number_format((($sastc/$scstc))*100,2);}else{echo 0;}		?>%</b></td>
		<td align="right"><b><?php echo number_format($sqspm);  ?></b></td>
		<!--<td align="right"><b><?php //if($tst>0){echo number_format(($ts2*100)/$tst);}else{echo 0;}	$tspt= 0;	$flag_ = 0;?>%</b></td>
		<td align="right"><b><?php //echo number_format(($ts2*100)/$ts1);		?>%</b></td>-->
		<!--<td align="right"><b><?php //echo number_format($nr2);			?></b></td>
		<td align="right"><b><?php //echo number_format($nr);			?></b></td>
		<td align="right"><b><?php //echo number_format($rct);			?></b></td>-->
		<!-- <td align="right"><b><?php //echo number_format($tsf);			?></b></td> modified by Boby 20130520 -->
		<!--<td align="right"><b><?php //echo number_format($kit2);			?></b></td>
		<td align="right"><b><?php //echo number_format($tnr*100/$kit2);			?>%</b></td>-->
	</tr>
	<tr>
		<td colspan="100%" id="reg_separator">&nbsp;</td>
    </tr>
	<?php
				/*
				$ts1 = 0;
				$ts2 = 0;
				$tst = 0;
				$kit1= 0;
				$kit2= 0;
				// $tnr2 = 0;
				// $tnr = 0;
				$tsf = 0;
				$nr = 0;
				$nr2 = 0;
				$rct = 0;
				*/
				
				$st = 0;
				$smtd = 0;
				$svst = 0;
				$nrt = 0;
				$snr = 0;
				$snrvst = 0;
				$sly = 0;
				$svsly = 0;
				$scstc = 0;
				$sastc = 0;
				$sratiostc = 0;
				$sqspm = 0;
				
				$curregion = '';
				$thereg = '';
			}$flag = 1;
			$p1=$row["periode"];
			$p2=$period_." ";
			if($period_=="Month"){
				$p1="";
				if($row["periode"]==1){$p2='January';}
				if($row["periode"]==2){$p2='February';}
				if($row["periode"]==3){$p2='March';}
				if($row["periode"]==4){$p2='April';}
				if($row["periode"]==5){$p2='May';}
				if($row["periode"]==6){$p2='June';}
				if($row["periode"]==7){$p2='July';}
				if($row["periode"]==8){$p2='August';}
				if($row["periode"]==9){$p2='September';}
				if($row["periode"]==10){$p2='October';}
				if($row["periode"]==11){$p2='November';}
				if($row["periode"]==12){$p2='December';}
				
			}
			
			if($period_=="Month"){
				$tglawal = $thn.'-'.$row["periode"].'-01';
				$ts = strtotime('-1 second', strtotime('+1 month',strtotime($tglawal)));
				$tglakhir = date('Y-m-d', $ts);
				
				$tglawally = date('Y-m-d', strtotime('-1 year',strtotime($tglawal)));
				$atsly = strtotime('-1 second', strtotime('+1 month',strtotime($tglawally)));
				$tglakhirly = date('Y-m-d',$atsly);
			}else if($period_=="Quart"){
				//$tglawal = $thn.'-'.$row["periode"].'-01';
				if($row["periode"]=='1'){
					$tglawal = $thn.'-01-01';
				}else if($row["periode"]=='2') {
					$tglawal = $thn.'-04-01';
				}else if($row["periode"]=='3') {
					$tglawal = $thn.'-07-01';
				}else if($row["periode"]=='4') {
					$tglawal = $thn.'-10-01';
				}

				$ts = strtotime('-1 second', strtotime('+3 month',strtotime($tglawal)));
				$tglakhir = date('Y-m-d', $ts);
				
				$tglawally = date('Y-m-d', strtotime('-1 year',strtotime($tglawal)));
				$atsly = strtotime('-1 second', strtotime('+1 month',strtotime($tglawally)));
				$tglakhirly = date('Y-m-d',$atsly);
			}
			//echo $tglawal.'<br>';
			//echo $tglakhir.'<br>';
			//echo $tglawally.'<br>';
			//echo $tglakhirly.'<br>';
			if($period_=="Month"){
				$results_other_total=$this->MSales->sales_ro_cluster_rpt_othersales_total($tglawal,$tglakhir, $row["periode"]);
			}else if($period_=="Quart"){
				$results_other_total=$this->MSales->sales_ro_cluster_rpt_othersales_total($tglawal,$tglakhir,'0');
			}
			if($results_other_total){
				foreach($results_other_total as $keyothert => $rowothert):
					if($rowothert["periode"]==$row["periode"]) {
						$curromsetnasother = $rowothert['omset_nasional'];
					}
				endforeach;
			}
			
			if($curromsetnasall!=($curromsetnas + $curromsetnasother)) $tcurromsetnasall+=$curromsetnas + $curromsetnasother;
			$curromsetnasall = $curromsetnas + $curromsetnasother;
			//echo $curromsetnasother;

	?>
	<tr>
		<td colspan="100%"><b><?php echo $p2.$p1;?></b></td>
	</tr>
	<?php } $flag_ += 1;?>
    <?php 
			if($curregion == '') {
				$curregion = $row['region'];
				if ($row['region'] == 'Region 0') 
					echo '<tr><td colspan = "100%"><b>Other Sales (Non PV)</b></td></tr>';
				else
					echo '<tr><td colspan = "100%"><b>'.$row['region'].' - '.$row['region_pic'].'</b></td></tr>';
			} else{
				if ($curregion != $row['region']) {
				
				// Other Sales Reg
				//20190829 - ASP Start
				if($period_=="Quart")
					$resultOtherReg = $this->MSales->sales_ro_cluster_rpt_othersales_reg($tglawal,$tglakhir,$thereg,0);
				else
					$resultOtherReg = $this->MSales->sales_ro_cluster_rpt_othersales_reg($tglawal,$tglakhir,$thereg,1);
				//20190829 - ASP End
	?>
			<tr>
				<td colspan="100%"><b>Other Sales Region <?php echo $thereg; ?></b></td>
			</tr>
	<?php
				foreach ($resultOtherReg as $rowo):
				
	?>
            <tr>
                <td class="linkpopup"><?php echo anchor_popup('search/search_rpt_cluster/otherreg/'.$tglawal.'/'.$tglakhir.'/'.$rowo['item_group'].'/'.$thereg, $rowo['item_group'], $atts);?></td>
                <td align="right"><?php if($curromsetnasall>0) echo number_format(($rowo['omset']/$curromsetnasall)*100, 1); else echo '0';?>%</td>
                <td align="right"><?php echo '0'; ?></td>
                <td align="right" class="linkpopup"><?php echo anchor_popup('search/search_rpt_cluster/otherreg/'.$tglawal.'/'.$tglakhir.'/'.$rowo['item_group'].'/'.$thereg, number_format($rowo['omset']), $atts);			$smtd+=$rowo['omset'];			$trsmtd+=$rowo['omset'];		$tsmtd+=$rowo['omset']; ?></td>
                <td align="right"><?php echo '0';?>%</td>
                <td align="right"><?php echo '0';?></td>
                <td align="right"><?php echo '0';?></td>
                <td align="right"><?php echo '0';?>%</td>
                <td align="right" class="linkpopup"><?php echo anchor_popup('search/search_rpt_cluster/otherreg/'.$tglawally.'/'.$tglakhirly.'/'.$rowo['item_group'].'/'.$thereg, number_format($rowo['omset_ly']), $atts);			$sly+=$rowo['omset_ly'];			$trsly+=$rowo['omset_ly'];		$tsly+=$rowo['omset_ly'];?></td>
                <td align="right"><?php echo number_format($rowo['vsly']);			$svsly+=$rowo['vsly'];		$tsvsly+=$rowo['vsly'];?>%</td>
                <td align="right"><?php echo '0'; ?></td>
                <td align="right"><?php echo '0'; ?></td>
                <td align="right"><?php echo '0';?>%</td>
                <td align="right" class="linkpopup"><?php echo '0'; ?></td>
            </tr>
    <?php
			endforeach;
			// oef other sales reg
	?>	
                     <tr>
                       <td><b>Total <?php echo $curregion; ?></b></td>
                       <td align="right"><b><?php if($curromsetnasall>0) echo number_format(($trsmtd/$curromsetnasall)*100, 1); else echo '0';?>%</b></td>
                       <td align="right"><b><?php echo number_format($trst);  ?></b></td>
                       <td align="right"><b><?php echo number_format($trsmtd);  ?></b></td>
                       <td align="right"><b><?php if($trst>0){echo number_format((($trsmtd/$trst)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trnrt);  ?></b></td>
                       <td align="right"><b><?php echo number_format($trsnr);  ?></b></td>
                       <td align="right"><b><?php  if($trnrt>0){echo number_format((($trsnr/$trnrt)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trsly);  ?></b></td>
                       <td align="right"><b><?php  if($trsly>0){echo number_format((($trsmtd/$trsly)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trscstc);  ?></b></td>
                       <td align="right"><b><?php echo number_format($trsastc);  ?></b></td>
                       <td align="right"><b><?php  if($trsastc>0){echo number_format((($trsastc/$trscstc))*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trsqspm);  ?></b></td>
                    </tr>
                    <?php
					//set sub total region to zero again
					$trst = 0;
					$trsmtd = 0;
					$trsvst = 0;
					$trnrt = 0;
					$trsnr = 0;
					$trsnrvst = 0;
					$trsly = 0;
					$trsvsly = 0;
					$trscstc = 0;
					$trsastc = 0;
					$trsratiostc = 0;
					$trsqspm = 0;
					//eof set sub total region to zero again

					$curregion = $row['region'];
					if ($row['region'] == 'Region 0') {
						echo '<tr><td colspan = "100%"><b>Other Sales</b></td></tr>';

					}
					else{
						echo '<tr><td colspan = "100%"><b>'.$row['region'].' - '.$row['region_pic'].'</b></td></tr>';
					}
				}
			}
			if($curregion== 'Region 1') $thereg = '1';
			else if($curregion== 'Region 2') $thereg = '2';
			else if($curregion== 'Region 3') $thereg = '3';
			else $thereg = '0';
	?>
    <tr>
		<!--<td><a href="#"><?php //if ($row['cluster_alias'] == 'UNIHEALTH') echo 'Staff Order'; else echo $row['cluster_alias'];?></a></td>-->
		<td class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo 'Staff Order'; else echo anchor_popup('search/search_rpt_cluster/index/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes/no', $row['cluster_alias'], $atts);?></td>
		<td align="right"><?php if($curromsetnasall>0) echo number_format(($row['omset_mtd']/$curromsetnasall)*100, 1); else echo '0';?>%</td>
		<td align="right"><?php echo number_format($row['sales_target']);			$st+=$row['sales_target'];			$trst+=$row['sales_target'];		$tst+=$row['sales_target']; ?></td>
		<!--<td align="right"><?php //echo '<a href="#">'.number_format($row['omset_mtd']).'</a>';			$smtd+=$row['omset_mtd'];		$tsmtd+=$row['omset_mtd']; ?></td>-->
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['omset_mtd']); else echo anchor_popup('search/search_rpt_cluster/index/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes/no/'.$thereg, number_format($row['omset_mtd']), $atts);			$smtd+=$row['omset_mtd'];			$trsmtd+=$row['omset_mtd'];		$tsmtd+=$row['omset_mtd']; ?></td>
        
       
		<td align="right"><?php echo number_format($row['vstarget']);			$svst+=$row['vstarget'];		$tsvst+=$row['vstarget'];?>%</td>
		<td align="right"><?php echo number_format($row['nr_target']);			$nrt+=$row['nr_target'];			$trnrt+=$row['nr_target'];		$tnrt+=$row['nr_target']; ?></td>
		<!--<td align="right"><?php //echo '<a href="#">'.number_format($row['nr']).'</a>';			$snr+=$row['nr'];		$tsnr+=$row['nr']; ?></td>-->
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['nr']); else echo anchor_popup('search/search_rpt_cluster/nr/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes', number_format($row['nr']), $atts);			$snr+=$row['nr'];			$trsnr+=$row['nr'];		$tsnr+=$row['nr']; ?></td>
		<td align="right"><?php echo number_format($row['vstargetnr']);			$snrvst+=$row['vstargetnr'];		$tsnrvst+=$row['vstargetnr'];?>%</td>
		<!--<td align="right"><?php //echo number_format($row['omset_ytd']);			$sly+=$row['omset_ytd'];			$trsly+=$row['omset_ytd'];		$tsly+=$row['omset_ytd']; ?></td>-->
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['omset_ytd']); else echo anchor_popup('search/search_rpt_cluster/stclastyear/'.$tglawally.'/'.$tglakhirly.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes/no/'.$thereg, number_format($row['omset_ytd']), $atts);			$sly+=$row['omset_ytd'];			$trsly+=$row['omset_ytd'];		$tsly+=$row['omset_ytd'];?></td>
		<td align="right"><?php echo number_format($row['vsly']);			$svsly+=$row['vsly'];		$tsvsly+=$row['vsly'];?>%</td>
		<!--
        <td align="right"><?php //echo '<a href="#">'.number_format($row['currstc']).'</a>';			$scstc+=$row['currstc'];		$tscstc+=$row['currstc']; ?></td>
		<td align="right"><?php //echo '<a href="#">'.number_format($row['actstc']).'</a>';			$sastc+=$row['actstc'];		$tsastc+=$row['actstc']; ?></td>
        -->
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['currstc']); else echo anchor_popup('search/search_rpt_cluster/index/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/no', number_format($row['currstc']), $atts);			$scstc+=$row['currstc'];			$trscstc+=$row['currstc'];		$tscstc+=$row['currstc']; ?></td>
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['actstc']); else echo anchor_popup('search/search_rpt_cluster/index/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes/yes', number_format($row['actstc']), $atts);			$sastc+=$row['actstc'];			$trsastc+=$row['actstc'];		$tsastc+=$row['actstc']; ?></td>
		<td align="right"><?php echo number_format($row['stcratio']);			$sratiostc+=$row['stcratio'];		$tsratiostc+=$row['stcratio'];?>%</td>
		<!--<td align="right"><?php //echo '<a href="#">'.number_format($row['qspmup']).'</a>';			$sqspm+=$row['qspmup'];		$tsqspm+=$row['qspmup']; ?></td>-->
		<td align="right" class="linkpopup"><?php if ($row['cluster_alias'] == 'UNIHEALTH') echo number_format($row['qspmup']); else echo anchor_popup('search/search_rpt_cluster/qspmup/'.$tglawal.'/'.$tglakhir.'/'.$row['cluster_id'].'/'.$row['cluster_alias'].'/yes', number_format($row['qspmup']), $atts);			$sqspm+=$row['qspmup'];			$trsqspm+=$row['qspmup'];		$tsqspm+=$row['qspmup']; ?></td>
		<?php
			
		?>
        <!--
		<td align="right"><?php //echo number_format($row['nr2']);			$nr2+=$row['nr2'];			$tnr2+=$row['nr2'];?></td>
		<td align="right"><?php //echo number_format($row['kit2']);			$nr+=$row['kit2'];			$tnr+=$row['kit2'];?></td>
		<td align="right"><?php //echo number_format($row['recruit']);		$rct += $row['recruit'];	$trct+=$row['recruit'];?></td>-->
		<!-- <td align="right"><?php //echo number_format($row['sf']);				$sf+=$row['sf']; 			$tsf+=$row['sf'];?></td> -->
		<!-- created by Boby 20130520 -->
		<!--<td align="right"><?php //echo number_format($row['nr']);			$kit2+=$row['nr'];			$tkit2+=$row['nr'];?></td>
		<td align="right"><?php //if($row['nr']>0){echo number_format($row['kit2']*100/$row['nr']);}else{echo 0;} ?>%</td>-->
		<!-- end created by Boby 20130520 -->
    </tr>
    <?php 
	if($curregion == 'Region 0'){
	//if($curregion == 'Region 4'){
		// Other Sales Reg 0
		//20160329 - ASP Start
		//20190829 - ASP Start modify
		if($period_=="Quart")
			$resultOtherReg0 = $this->MSales->sales_ro_cluster_rpt_othersales_reg($tglawal,$tglakhir,$thereg,0);
		else
			$resultOtherReg0 = $this->MSales->sales_ro_cluster_rpt_othersales_reg($tglawal,$tglakhir,$thereg,1);
		//20160329 - ASP End
		foreach ($resultOtherReg0 as $rowo0):
	?>
            <tr>
                <td class="linkpopup"><?php echo anchor_popup('search/search_rpt_cluster/otherreg/'.$tglawal.'/'.$tglakhir.'/'.$rowo0['item_group'].'/'.$thereg, $rowo0['item_group'], $atts);?></td>
                <td align="right"><?php if($curromsetnasall>0) echo number_format(($rowo0['omset']/$curromsetnasall)*100, 1); else echo '0';?>%</td>
                <td align="right"><?php echo '0'; ?></td>
                <td align="right" class="linkpopup"><?php echo anchor_popup('search/search_rpt_cluster/otherreg/'.$tglawal.'/'.$tglakhir.'/'.$rowo0['item_group'].'/'.$thereg, number_format($rowo0['omset']), $atts);			$smtd+=$rowo0['omset'];			$trsmtd+=$rowo0['omset'];		$tsmtd+=$rowo0['omset']; ?></td>
                <td align="right"><?php echo '0';?>%</td>
                <td align="right"><?php echo '0';?></td>
                <td align="right"><?php echo '0';?></td>
                <td align="right"><?php echo '0';?>%</td>
                <td align="right" class="linkpopup"><?php echo anchor_popup('search/search_rpt_cluster/otherreg/'.$tglawally.'/'.$tglakhirly.'/'.$rowo0['item_group'].'/'.$thereg, number_format($rowo0['omset_ly']), $atts);			$sly+=$rowo0['omset_ly'];			$trsly+=$rowo0['omset_ly'];		$tsly+=$rowo0['omset_ly'];?></td>
                <td align="right"><?php echo number_format($rowo0['vsly']);			$svsly+=$rowo0['vsly'];		$tsvsly+=$rowo0['vsly'];?>%</td>
                <td align="right"><?php echo '0'; ?></td>
                <td align="right"><?php echo '0'; ?></td>
                <td align="right"><?php echo '0';?>%</td>
                <td align="right" class="linkpopup"><?php echo '0'; ?></td>
            </tr>
    <?php
    		
			endforeach;
			// oef other sales
			
	?>	
					
                     <tr>
                       <td><b>Total <?php echo 'Other Sales'; ?></b></td>
                       <td align="right"><b><?php if($curromsetnasall>0) echo number_format(($trsmtd/$curromsetnasall)*100, 1); else echo '0';?>%</b></td>
                       <td align="right"><b><?php echo number_format($trst);  ?></b></td>
                       <td align="right"><b><?php echo number_format($trsmtd);  ?></b></td>
                       <td align="right"><b><?php if($trst>0){echo number_format((($trsmtd/$trst)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trnrt);  ?></b></td>
                       <td align="right"><b><?php echo number_format($trsnr);  ?></b></td>
                       <td align="right"><b><?php  if($trnrt>0){echo number_format((($trsnr/$trnrt)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trsly);  ?></b></td>
                       <td align="right"><b><?php  if($trsly>0){echo number_format((($trsmtd/$trsly)-1)*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trscstc);  ?></b></td>
                       <td align="right"><b><?php echo number_format($trsastc);  ?></b></td>
                       <td align="right"><b><?php  if($trsastc>0){echo number_format((($trsastc/$trscstc))*100,2);}else{echo 0;}?>%</b></td>
                       <td align="right"><b><?php echo number_format($trsqspm);  ?></b></td>
                    </tr>
                   
                    <?php
					//set sub total region to zero again
					$trst = 0;
					$trsmtd = 0;
					$trsvst = 0;
					$trnrt = 0;
					$trsnr = 0;
					$trsnrvst = 0;
					$trsly = 0;
					$trsvsly = 0;
					$trscstc = 0;
					$trsastc = 0;
					$trsratiostc = 0;
					$trsqspm = 0;
					//eof set sub total region to zero again
					//$lastregion = $curregion;
					$curregion = $row['region'];
	}
	endforeach; 
	?>
	<tr>
		<td><b>Sub Total</b></td>
		<td align="right"><b><?php echo '100%'; ?></b></td>
		<td align="right"><b><?php echo number_format($st);  ?></b></td>
		<td align="right"><b><?php echo number_format($smtd);  ?></b></td>
		<td align="right"><b><?php if($st>0){echo number_format((($smtd/$st)-1)*100,2);}else{echo 0;}	$tspt= 0;	$flag_ = 0;?>%</b></td>
		<td align="right"><b><?php echo number_format($nrt);  ?></b></td>
		<td align="right"><b><?php echo number_format($snr);  ?></b></td>
		<td align="right"><b><?php  if($nrt>0){echo number_format((($snr/$nrt)-1)*100,2);}else{echo 0;}		?>%</b></td>
		<td align="right"><b><?php echo number_format($sly);  ?></b></td>
		<td align="right"><b><?php  if($sly>0){echo number_format((($smtd/$sly)-1)*100,2);}else{echo 0;}		?>%</b></td>
		<td align="right"><b><?php echo number_format($scstc);  ?></b></td>
		<td align="right"><b><?php echo number_format($sastc);  ?></b></td>
		<td align="right"><b><?php  if($sastc>0){echo number_format((($sastc/$scstc))*100,2);}else{echo 0;}		?>%</b></td>
		<td align="right"><b><?php echo number_format($sqspm);  ?></b></td>
		<!--<td align="right"><b><?php //if($tst>0){echo number_format(($ts2*100)/$tst);}else{echo 0;}	$tspt= 0;	$flag_ = 0;?>%</b></td>
		<td align="right"><b><?php //echo number_format(($ts2*100)/$ts1);		?>%</b></td>-->
		<!--<td align="right"><b><?php //echo number_format($nr2);			?></b></td>
		<td align="right"><b><?php //echo number_format($nr);			?></b></td>
		<td align="right"><b><?php //echo number_format($rct);			?></b></td>-->
		<!-- <td align="right"><b><?php //echo number_format($tsf);			?></b></td> modified by Boby 20130520 -->
		<!--<td align="right"><b><?php //echo number_format($kit2);			?></b></td>
		<td align="right"><b><?php //echo number_format($tnr*100/$kit2);			?>%</b></td>-->
	</tr>
	
	<tr>
		<td colspan="100%" id="reg_separator">&nbsp;</td>
    </tr>
<?php else: ?>
    <tr>
		<td colspan="100%">Data is not available.</td>
    </tr>
<?php endif; ?>
	<tr>
		<td ><b>Grand Total</b></td>
		<td align="right"><b><?php if($tcurromsetnasall>0) echo number_format(($tsmtd/$tcurromsetnasall)*100, 1); else echo '0';  ?>%</b></td>
		<td align="right"><b><?php echo number_format($tst);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsmtd);  ?></b></td>
		<td align="right"><b><?php if($tst>0){echo number_format((($tsmtd/$tst)-1)*100,2);}else{echo 0;}?>%</b></td>
		<td align="right"><b><?php echo number_format($tnrt);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsnr);  ?></b></td>
		<td align="right"><b><?php  if($tnrt>0){echo number_format((($tsnr/$tnrt)-1)*100,2);}else{echo 0;}		?>%</b></td>
		<td align="right"><b><?php echo number_format($tsly);  ?></b></td>
		<td align="right"><b><?php  if($tsly>0){echo number_format((($tsmtd/$tsly)-1)*100,2);}else{echo 0;}		?>%</b></td>
		<td align="right"><b><?php echo number_format($tscstc);  ?></b></td>
		<td align="right"><b><?php echo number_format($tsastc);  ?></b></td>
		<td align="right"><b><?php  if($tsastc>0){echo number_format((($tsastc/$tscstc))*100,2);}else{echo 0;}		?>%</b></td>
		<td align="right"><b><?php echo number_format($tsqspm);  ?></b></td>
		<!--<td align="right"><b><?php //if($tst>0){echo number_format(($ts2*100)/$tst);}else{echo 0;}	$tspt= 0;	$flag_ = 0;?>%</b></td>
		<td align="right"><b><?php //echo number_format(($ts2*100)/$ts1);		?>%</b></td>-->
		<!--<td align="right"><b><?php //echo number_format($nr2);			?></b></td>
		<td align="right"><b><?php //echo number_format($nr);			?></b></td>
		<td align="right"><b><?php //echo number_format($rct);			?></b></td>-->
		<!-- <td align="right"><b><?php //echo number_format($tsf);			?></b></td> modified by Boby 20130520 -->
		<!--<td align="right"><b><?php //echo number_format($kit2);			?></b></td>
		<td align="right"><b><?php //echo number_format($tnr*100/$kit2);			?>%</b></td>-->
	</tr>
</table>			                

<?php $this->load->view('footer');?>
