<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Mail Notofication</title>
</head>
<body>
<!-- Copy dari sini -->
<style type="text/css">
body { background:#999999; }
.kotakLuar		{ border:solid 4px #1c4763; margin:40px; }
.boxHeader		{ width:100%; background:#d9d9d9 url(http://www.uni-health.com/images/header-bg.gif) left top repeat-x; height:150px; border-bottom:0; margin:0; padding:0; }
.boxIsi   		{ padding:1px 20px 40px 40px; background:#fff; margin:0; }
.boxIsi p 		{ color:#333; font: .85em/1.5em 'Arial', Times, serif; }
.textIsiUl li 	{ color:#333; font: .85em/1.5em 'Arial', Times, serif; }
</style>
<div class="kotakLuar">
	<div class="boxHeader">
 		<table width="100%" border="0" cellspacing="0" cellpadding="0">
  			<tr>
    			<td width="1%" style="padding-left:10px;"><img src="http://www.uni-health.com/images/logo.gif" width="262" height="100" alt="UNIHEALTH NETWORK" /></td>
    			<td width="1%" style="padding:15px 30px 0 10px;"><img src="http://www.uni-health.com/images/line.gif" width="2" height="77" alt="" /></td>
    			<td width="98%"><h1 style="color:#666; font: 2.2em/1.5em 'Times New Roman', Times, serif; padding-top:15px;">Mail Notification</h1></td>
  			</tr>
		</table>
	</div>
	<div class="boxIsi">
		<h1 style="color:#999; font: 1.8em/1.5em 'Times New Roman', Times, serif; ">Selamat, anda telah Bergabung di bisnis MLM UNIHEALTH!</h1>
		<p>Terima kasih atas komitment Anda berbagung di bisnis jaringan kami. Kami adalah PT.Universal Health Network, yang merupakan bagian dari SOHO Group yang merupakan salah satu grup Farmasi terbesar di Indonesia. Melalui Unihealth, SOHO Group menyediakan produk-produk berkualitas tinggi dan terbukti manfaatnya.</p>
		<p>Hanya dengan selangkah lagi Anda akan tercatat sebagai sebagai Member MLM Unihealth. Berbanggalah menjadi bagian dari sistem pemasaran Unihealth yang akan membawa Anda menuju kehidupan yang lebih sehat dan lebih sejahtera!</p>
		<p style="font-weight:bold;">Langkah 1</p>
		<p>1. Melakukan pembayaran Starter Kit sebesar</p>
		<p style="padding-left:17px;">Rp.98.000,- plus Rp.25.000 (sebagai ongkos kirim dari pendaftaran via online)</p>
		<p>2. Melakukan konfirmasi pembayaran melalui:</p>
			<ul class="textIsiUl">
     			<li>YM: cs1_unihealth atau cs_2unihealth (via website: <a rel="nofollow" target="_blank" href="http://www.uni-health.com"><span class="yshortcuts" id="lw_1251095231_3">www.uni-health.com</span></a>)<br></li>
      			<li>Email: <span style="border-bottom: 1px dashed rgb(0, 102, 204); background: transparent none repeat scroll 0% 0%; cursor: pointer; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous;" class="yshortcuts" id="lw_1251095231_4">info@uni-health.com</span></li>
      			<li>Telp: 021-62310402 (Meilany/Meria/Iwan)</li>
      			<li>Fax: 021-62310403</li>
      			<li>SMS Center: 08588-5354-222<br></li>
			</ul>
		<p>3. Pembayaran ke Rekening A/n PT.Universal Health Network<br>&nbsp;&nbsp;&nbsp; Bank Mandiri: 125.000.972.3016</p>
		<p>Bank BCA: 003.305.9008</p>
		<p>4. Setelah melakukan pembayaran, kami akan memberikan Activation Code untuk keanggotaan Anda melalui email. Lakukan pengisian Activation Code segera setelah Anda menerimanya agar dapat login ke member Area.
		<p>5. Segera ubah password dan pin standar yang kami kirim sesuai dengan pilihan Anda dan simpan dengan baik dan aman.</p>
		<p style="font-weight:bold;">Mari kita gapai impian sehat sejahtera di masa depan bersama Unihealth!</p><br />
		<p>Salam,</p>
        <p>Manajemen</p>
	</div>
</div>
<!-- sampai sini -->
</body>
</html>
