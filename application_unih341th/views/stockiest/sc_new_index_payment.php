<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php 
	//echo anchor('inv/sc/create','Create Stockiest Consignment'); // Create by Budi Prayudi

	if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>

<table width='99%'>
<?php echo form_open('inv/sc', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='60%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='40%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b>
			<?php }?>
			
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<?php echo form_open('inv/sc/approved/', array('id' => 'my_form2', 'name' => 'my_form2', 'autocomplete' => 'off'));?>
<table class="stripe">
	<tr>
      <th width='50px'>No.</th>
      <th width='60px'>SC ID</th>
      <th width='150px'>SC Date</th>
      <th width='27%'>Stockiest ID / Name</th>
      <th width='11%'><div align="right">Total Price</div></th>
      <th width='11%'><div align="right">Total Item</div></th>
      <th width='11%'><div align="right">Total PV</div></th>
      <th width='11%'><div align="right">Qty Lunas</div></th>
	  <th width='150px'><div align="right">Req Time</div></th>
      
	  <!--<th width='6%'>Status</th>-->
    </tr>
   
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
		<td align="right"><?php echo anchor('inv/sc/payment_view/'.$row['id'], $counter);?></td>
		<td><?php echo anchor('inv/sc/payment_view/'.$row['id'], $row['id']);?></td>
		<td><?php echo anchor('inv/sc/payment_view/'.$row['id'], $row['date']);?></td>
		<td><?php echo anchor('inv/sc/payment_view/'.$row['id'], $row['no_stc']." - ".$row['nama']);?></td>
		<td align="right"><?php echo anchor('inv/sc/payment_view/'.$row['id'], $row['ftotalharga']);?></td>
		<td align="right"><?php echo anchor('inv/sc/payment_view/'.$row['id'], $row['jml_pinjam']);?></td>
		<td align="right"><?php echo anchor('inv/sc/payment_view/'.$row['id'], $row['ftotalpv']);?></td>
		<td align="right"><?php echo anchor('inv/sc/payment_view/'.$row['id'], $row['qty_lunas']);?></td>
		<td align="right"><?php echo anchor('inv/sc/payment_view/'.$row['id'], $row['created']);?></td>
		<!--<td><?php echo anchor('inv/sc/payment_view/'.$row['id'], $row['status']);?></td>-->
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="8">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php echo form_close();
$this->load->view('footer');3
?>