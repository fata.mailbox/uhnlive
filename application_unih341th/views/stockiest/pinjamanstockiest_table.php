<table width='99%'>
<?php echo form_open('inv/sc', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='50%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='50%' align='right'>search by: 
			<?php 
				
				if($this->session->userdata('group_id') < 100 && $this->session->userdata('group_id')!= 28){
					/*
					echo '<select name="whsid">';
					if ($warehouse){
						foreach($warehouse as $x => $x_value) {
							echo '<option value="'.$x_value.'">'.$x_value.'</option>';
						}
					}else{
						echo '<option value="all">Warehouse Tidak Tersedia Cantik..</option>';
					}
					echo '</select>';
					*/
					
					echo form_dropdown('whsid',$warehouse);
					
					$data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);
					echo form_submit('submit','go'); 
				}else if($this->session->userdata('keywords')){
					echo '<br />Your search keywords : <b>'.$this->session->userdata('keywords').'</b>';
				}
				
			?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<?php echo form_open('inv/sc/approved/', array('id' => 'my_form2', 'name' => 'my_form2', 'autocomplete' => 'off'));?>
<table class="stripe">
	<tr>
		<td colspan='2' valign='top'>remark: </td>
		<td colspan='11'><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
			echo form_textarea($data);?><br>
			<?php echo form_submit('submit','approved');?>
			<br>
			<br>
		</td>
		<td valign='top'></td>
	</tr>
	<tr>
	  <th style="min-width:20px;max-width:25px;">PST</th>
	  <th style="min-width:20px;max-width:25px;">HUB</th>
      <th  width='25px'>No.</th>
      <th>SC No.</th>
      <th width="15%;">Stockiest ID / Name</th>
      <th><div align="right">Total Price</div></th>
      <th><div align="right">Total PV</div></th>
      <th><div align="right">Warehouse</div></th>
      <th><div align="right">Warehouse Pengiriman</div></th>
      <th><div align="right">App Time</div></th>
      <th><div align="right">App Time HO</div></th>
      <th><div align="right">App Time HUB</div></th>
      <th>Created By</th>
      <th>Processed By</th>
    </tr>
   
<?php
if (isset($results)):
	$counter = $from_rows; 
	foreach($results as $key => $row): 
	$counter = $counter+1;
	// query wh pengiriman
	$xWhsName=$this->MSc->getWhPengiriman($row['id']);
?>
    <tr>
	  <td>
		<?php 
		//echo $this->session->userdata('group_id');
		if ($this->session->userdata('group_id')=='1'){
			if($row['status'] == 'pending'){ /* <-- Jangan di hapus if ini, bawaan Old System */
				$strStatus='';
				if (($row['tglapproved_ho']=='') OR ($row['tglapproved_ho']=='0000-00-00 00:00:00') OR ($row['approvedby_ho']=='')){
					if ($this->session->userdata('group_id')=='1'){
						$strStatus=$strStatus.'
						<input type="checkbox" name="p_id_ho[]" value="'.$row['id'].'" id="p_id_ho[]" style="border: none; background: rgb(255, 255, 255);">
						';
						/*
						$strStatus=$strStatus.'
						<div style="float:left;">
							<label class="container_biru" >
							  <input type="checkbox" name="p_id_ho[]" value="'.$row['id'].'" id="p_id_ho[]" >
							  <span class="checkmark"></span>
							</label>
						</div>
						';
						*/
					}else{
						$strStatus='<label class="container">
						  <input type="radio" checked="checked" name="approve'.$row['id'].'" id="approve'.$row['id'].'" >
						  <span class="checkmark"></span>
						</label>';
					}
				}
				echo $strStatus;
			} else {
				/*
				echo '
				<label class="container">
				  <input type="radio" checked="checked" name="approve'.$row['id'].'" id="approve'.$row['id'].'" >
				  <span class="checkmark"></span>
				</label>
				';
				*/
			}
		}
		?> 
	  </td>
	  <td>
		<?php 
		//echo $this->session->userdata('group_id');
		if($row['status'] == 'pending'){
			$strStatus='';
			if (($row['tglapproved_hub']=='') OR ($row['tglapproved_hub']=='0000-00-00 00:00:00') OR ($row['approvedby_hub']=='')){
				$strStatus=$strStatus.'
				<input type="checkbox" name="p_id_hub[]" value="'.$row['id'].'" id="p_id_hub[]" style="border: none; background: rgb(255, 255, 255);">
				';
				/*
				$strStatus=$strStatus.'
				<div style="float:left;">
					<label class="container_ijo" >
					  <input type="checkbox" name="p_id_hub[]" id="p_id_hub[]" value="'.$row['id'].'" >
					  <span class="checkmark"></span>
					</label>
				</div>
				';
				*/
			}
			echo $strStatus;
		} else {
			/*
			echo '
			<label class="container">
			  <input type="radio" checked="checked" name="approve'.$row['id'].'" id="approve'.$row['id'].'" >
			  <span class="checkmark"></span>
			</label>
			';
			*/
		}
		?> 

	  </td>
		<td align="right" ><?php echo anchor('inv/sc/view/'.$row['id'], $counter);?></td>
		<td><b><?php echo anchor('inv/sc/view/'.$row['id'], $row['id']);?></b></td>
		<td>
		<?php echo anchor('inv/sc/view/'.$row['id'], '<b>'.$row['no_stc']." - ".$row['nama']."</b><br> SC Date : ".$row['tgl'] );?>
		</td>
		<td align="right"><?php echo anchor('inv/sc/view/'.$row['id'], $row['ftotalharga']);?></td>
		<td align="right"><?php echo anchor('inv/sc/view/'.$row['id'], $row['ftotalpv']);?></td>
		<td align="right"><?php echo anchor('inv/sc/view/'.$row['id'], $row['warehouse_name']);?></td>
		<td align="right"><?php echo anchor('inv/sc/view/'.$row['id'], $xWhsName );?></td>
		<td align="right"><?php echo anchor('inv/sc/view/'.$row['id'], $row['appdate']);?></td>
		<td align="right"><?php echo anchor('inv/sc/view/'.$row['id'], $row['appdate_ho']);?></td>
		<td align="right"><?php echo anchor('inv/sc/view/'.$row['id'], $row['appdate_hub']);?></td>
      <td><?php echo anchor('inv/sc/view/'.$row['id'], $row['createdby']);?></td>
      <td><?php echo anchor('inv/sc/view/'.$row['id'], $row['approvedby']);?></td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php echo form_close();?>				
