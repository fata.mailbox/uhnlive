

<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php
echo form_open('inv/sc_retur/create/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off','style'=>'max-width:40%;float:left;'));
?>
<table width='100%'>
		<tr>
			<td width='25%'>Date</td>
			<td width='3px'>:</td>
			<td width='79%'>
				<?php echo date('Y-m-d',now());?>
			</td>
		</tr>
        <tr>
			<td class="error">Stockist ID / Name *</td>
			<td>:</td>
			<td>
				<input type="hidden" name="member_id_from" id="member_id_from" readonly="1" value="<?php echo $this->input->post('member_id_from'); ?>" size="30" />
				<input type="hidden" name="kota_id" id="kota_id" value="<?php echo $this->input->post('kota_id');?>" />
				<input type="hidden" name="propinsi" id="propinsi" value="<?php echo $this->input->post('propinsi');?>" />
				<input type="hidden" name="timur" id="timur" value="<?php echo $this->input->post('timur');?>" />
				
				<input type="hidden" name="addr1" id="addr1" value="<?php echo $this->input->post('addr1');?>" />
				<input type="hidden" name="kota_id1" id="kota_id1" value="<?php echo $this->input->post('kota_id1');?>" />
				<input type="hidden" name="pic_name1" id="pic_name1" value="<?php echo $this->input->post('pic_name1');?>" />
				<input type="hidden" name="pic_hp1" id="pic_hp1" value="<?php echo $this->input->post('pic_hp1');?>" />
				<input type="hidden" name="kecamatan1" id="kecamatan1" value="<?php echo $this->input->post('kecamatan1');?>" />
				<input type="hidden" name="kelurahan1" id="kelurahan1" value="<?php echo $this->input->post('kelurahan1');?>" />
				<input type="hidden" name="kodepos1" id="kodepos1" value="<?php echo $this->input->post('kodepos1');?>" />
				<input type="hidden" name="sc_date" id="sc_date" readonly="1" value="<?php echo $this->input->post('sc_date');?>" size="30" />

				<input type="text" name="no_stc_from" id="no_stc_from" readonly="1" value="<?php echo $this->input->post('no_stc_from');?>" size="5" required />
				<input type="text" name="name_from" id="name_from" readonly="1" value="<?php echo $this->input->post('name_from');?>" size="20" required />
				<!--<input type="button" id="cmd_Show" name="cmd_Show" onClick=showPopUp() value="Browse" >-->
				<span class='error'>*<?php echo form_error('member_id_from');?></span>
			</td>	
		</tr>
		<tr>
			<td>Diskon %</td>
			<td>:</td>
			<td><input class="aright" type="text" name="persen" id="persen" value="<?php echo $this->input->post('persen',0);?>"  /></td>
		</tr>
		<tr>
			<td width='20%'>Warehouse</td>
			<td width='1%'>:</td>
			<td width='79%'>
				<input type="text" name="whs_from" id="whs_from" readonly="1" value="<?php echo $this->input->post('whs_from');?>" size="30" />
				<input type="hidden" name="whsid" id="whsid" readonly="1" value="<?php echo $this->input->post('whsid');?>" size="30" />
			
			</td>
		</tr>
		<tr>
			<td width='20%'>SC ID</td>
			<td width='1%'>:</td>
			<td width='79%'><input type="text" name="sc_id" id="sc_id" readonly="1" value="<?php echo $this->input->post('sc_id');?>" size="30" /></td>
		</tr>
		<tr>
			<td>Nominal</td>
			<td>:</td>
			<td><input type="text" name="nominal" id="nominal" readonly="1" value="<?php echo $this->input->post('nominal');?>" size="30" /></td>
		</tr>
		<tr>
			<td colspan=3>Delivery Address</td>
		</tr>
		<tr>
			<td colspan=3>
				<textarea name="deli_ad" id="deli_ad" style="width:100%;height:90px;"><?php echo $this->input->post('deli_ad');?></textarea>
			</td>
		</tr>
		<tr><td colspan="3" class="error"><hr/><?php echo $pray_msg;?></td></tr>
		<tr><td colspan="3"><hr/><?php echo form_submit('submit', 'Submit');?></td></tr>
</table>

<div id="clearer"></div>
<?php echo form_close();?>

<?php
echo '<div id="div_sc_no" style="width:50%;float:right;">';
echo '<table>';
echo form_open('inv/sc_retur/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
echo '
	<tr>
		<td width="50%"><strong>'.$this->pagination->create_links().'</strong></td>
		<td width="50%" align="right">';
			echo 'search by:';
			echo form_dropdown('whsid',$warehouse);
			$data = array('name'=>'search','id'=>'search','size'=>20,'value'=>$this->input->post('search'));
			echo form_input($data); 
			echo form_submit('submit','go');
			if($this->session->userdata('keywords')){
				echo '<br />Your search keywords : <b>'. $this->session->userdata('keywords').'</b>';
			}
		echo '
    	</td>  
  </tr>
';  
echo form_close();
echo '</table>';

if ($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}

if (isset($results)){
	echo '
	<p>Please Select Stockiest Consignment From list</p>
	<table id="table_popup" class="stripe">';
	echo '
	<tr>
      <th width="25px">No.</th>
      <th >SC No.</th>
      <th >Date</th>
      <th>Stockiest ID / Name</th>
      <th><div align="right">Total Price</div></th>
      <th style="display:none;"><div align="right">Total PV</div></th>
      <th style="display:none;"><div align="right">Warehouse</div></th>
      <!--
	  <th><div align="right">App Time</div></th>
      <th>Created By</th>
      <th>Processed By</th>
	  -->
	  <th>Qty</th>
	  <th>Retur</th>
	  <th>Lunas</th>
	  <th>Total</th>
    </tr>
	';
	$counter = $from_rows; 
	foreach($results as $key => $row){
		if ($row['qty_total']>0){
			$counter = $counter+1;
			//$cmSubmit='<input type="button" onClick=showData("'.$counter.'") value="Select" >';
			/*
			if ($row['status']=='delivery'){
					$cmSubmit='';
			}
			*/
			echo '
			<tr id="tr_sel'.$counter.'" style="cursor: pointer;" onClick=onClick=showData("'.$counter.'") >
				<td>'.$counter.'</td>
				<td id="varScId'.$counter.'">'.$row['id'].'</td>
				<td id="varScDate'.$counter.'">'.$row['tgl'].'</td>
				<td>'.$row['no_stc']." - ".$row['nama'].'
					<div style="display:none;">
					<div id="varKota'.$counter.'">'.$row['kota_id'].'</div>
					<div id="varKeca'.$counter.'">'.$row['kecamatan'].'</div>
					<div id="varKelu'.$counter.'">'.$row['kelurahan'].'</div>
					<div id="varPos'.$counter.'">'.$row['kodepos'].'</div>
					<div id="varAlam'.$counter.'">'.$row['alamat'].'</div>
					<div id="varPicHp'.$counter.'">'.$row['hp'].'</div>
					<div id="varPicNm'.$counter.'">'.$row['nama'].'</div>
					<div id="varPersen'.$counter.'">'.$row['persen'].'</div>
					<div id="varStc'.$counter.'">'.$row['no_stc'].'</div>
					<div id="varMemberId'.$counter.'">'.$row['member_id'].'</div>
					<div id="varWhId'.$counter.'">'.$row['warehouse_id'].'</div>
					</div>
				</td>
				<td id="varNominal'.$counter.'" align="right">'.$row['ftotalharga'].'</td>
				<td align="right" style="display:none;">'.$row['ftotalpv'].'</td>
				<td style="display:none;" id="varWhName'.$counter.'" align="right">'.$row['warehouse_name'].'</td>
				<!--
				<td align="right">'.$row['appdate'].'</td>
				<td>'.$row['createdby'].'</td>
				<td>'.$row['approvedby'].'</td>
				-->
				<td align="right">'.$row['qty'].'</td>
				<td align="right">'.$row['qty_retur'].'</td>
				<td align="right">'.$row['qty_lunas'].'</td>
				<td align="right" >'.$row['qty_total'].'</td>
			</tr>';
		}
	}
	echo '</table>';
}else{
	echo 'Data Not Found';
}
echo '</div>';
?>
<br><br>
<div id=clearer style="height:40px;"></div>
<div id="div_pop_Up">
<!--http://localhost/sohomlm_prod/search/scsearch/no_sc-->
<?php
if ($item_results){
	//echo 'OKEEHHH...';
}
?>
</div>
<?php $this->load->view('footer');?>

<script type="text/javascript" src="<?php echo site_url() ?>js/jquery.js"></script>
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
	
	function showPopUp(){
		$("#div_pop_Up").show();
	}
	
	function hidePopUp(){
		$("#div_pop_Up").hide();
	}
	
	function showData(varCounter){
		$(".stripe tr").removeClass("trClick");
		$("#tr_sel"+varCounter).addClass("trClick");
		
		$("#kota_id").val($("#varKota"+varCounter).html());
		$("#kota_id1").val($("#varKota"+varCounter).html());
		$("#kecamatan1").val($("#varKeca"+varCounter).html());
		$("#kelurahan1").val($("#varKelu"+varCounter).html());
		$("#kodepos1").val($("#varPos"+varCounter).html());
		$("#deli_ad").val($("#varAlam"+varCounter).html());
		$("#addr1").val($("#varAlam"+varCounter).html());
		$("#pic_name1").val($("#varPicNm"+varCounter).html());
		$("#pic_hp1").val($("#varPicHp"+varCounter).html());
		$("#name_from").val($("#varPicNm"+varCounter).html());
		$("#persen").val($("#varPersen"+varCounter).html());
		$("#no_stc_from").val($("#varStc"+varCounter).html());
		$("#member_id_from").val($("#varMemberId"+varCounter).html());
		$("#sc_id").val($("#varScId"+varCounter).html());
		$("#nominal").val($("#varNominal"+varCounter).html());
		$("#whsid").val($("#varWhId"+varCounter).html());
		$("#whs_from").val($("#varWhName"+varCounter).html());
		$("#sc_date").val($("#varScDate"+varCounter).html());
		
		$.ajax({
		  method: "POST",
		  url: "<?php echo site_url() ?>inv/sc_retur/no_sc/"+$('#sc_id').val(),
		  dataType : 'text',
		  data: {
				search : $('#sc_id').val() 
		  },
		  success: function(data){
				//alert ($("#sc_id").val());
				$("#div_pop_Up").html(data);
		  },
		  error : function(XMLHttpRequest, textStatus, errorThrown) {
				$("#div_pop_Up").html("Error.."+textStatus);
		  }
		});
		
	}
</script>