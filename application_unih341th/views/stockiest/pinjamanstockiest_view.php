<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>

<div class="ViewHold">
	<div id="companyDetails">
		<?php echo "<img src='../../../images/backend/logo.jpg' border='0'><br>";?>
	</div>

	<p>
		<strong>
			<?php echo "No.Invoice SC : ";?> <?php echo $row['id'];?><br />
			<?php
				echo $row['tgl'];
			?>
		</strong>
		
	</p>
	<br />
		<h2>Invoice Stockiest Consignment</h2>
	<hr />

	<h3><?php echo $row['no_stc']," - ".$row['nama'];?></h3>

	<p>
		<?php
			echo $row['alamat']."<br />";
			echo $row['kota']." - ".$row['propinsi']." ".$row['kodepos']."<br />";
			echo "Remark: ".$row['remark']."<br />";
			echo "User ID: ".$row['createdby'];
		?>
	</p>
	
	<table class="stripe">
	<tr>
      <th width='10%'>Item Code</th>
      <th width='25%'>Item Name</th>
      <th width='10%'><div align="right">Qty</div></th>
      <th width='10%'><div align="right">Price</div></th>
      <th width='10%'><div align="right">PV</div></th>
      <th width='20%'><div align="right">Sub Total</div></th>      
      <th width='15%'><div align="right">Sub Total PV</div></th>
    </tr>
    
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><?php echo $item['item_id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td align="right"><?php echo $item['fqty'];?></td>
			<td align="right"><?php echo $item['fharga'];?></td>
			<td align="right"><?php echo $item['fpv'];?></td>
			<td align="right"><?php echo $item['fsubtotal'];?></td>
			<td align="right"><?php echo $item['fsubtotalpv'];?></td>
		</tr>
		<?php endforeach;?>
		
		<tr>
			<td colspan='5' align='right'><b>Total </b>&nbsp;</td>
			<td align="right"><b><?php echo $row['ftotalharga'];?></b></td>
            <td align="right"><b><?php echo $row['ftotalpv'];?></b></td>
		</tr>
        
	</table>

	<div  id=clearer style="height:20px" ></div>
	<div class="col-md-6">
	<?php
	$strFreeItem='';
	if ($items_free){
		echo '<h2>Free Item</h2>';
		echo '
		<table class="stripe">
		<tr>
		  <th width="80px">Item Code</th>
		  <th >Item Name</th>
		  <th width="50px"><div align="center">Qty</div></th>
		</tr>
		';
		foreach ($items_free as $var_freeitem){
			$strFreeItem=$strFreeItem.'
			<tr>
				<td>'.$var_freeitem['free_item_id'].'</td>
				<td>'.$var_freeitem['Free_Item_Name'].'</td>
				<td align="right">'.$var_freeitem['GetFree'].'</td>
			</tr>';
		}
		echo $strFreeItem;
		echo '</table>';
	}
	
	?>
	</div>
	<?php /* Created by Andrew 20120530*/ ?>
	<table width=100%>
	<tr>
	  <td colspan='1' align=center width='5%'> </td>
	  <td colspan='2' align=center width='25%'>Sales Counter</td>
	  <td colspan='1' align=center width='5%'> </td>
      <td colspan='2' align=center width='30%'>Warehouse</td>
	  <td colspan='1' align=center width='5%'> </td>
	  <td colspan='2' align=center width='25%'>Receiver</td> 	
	  <td colspan='1' align=center width='5%'> </td>
	  
	</tr>
	<tr>
	<td colspan='10'>
	<br><br><br><br><br><br>
	</td>
	</tr>
	
	<tr>
		<td></td>
		<td align='left'>(</td><td align='right'>)</td>
		<td></td>
		<td align='left'>(</td><td align='right'>)</td>
		<td></td>
		<td align='left'>(</td><td align='right'>)</td> 
		<td></td>
	</tr>
	</table>
	<?php /* End created by Andrew 20120530*/ ?>
	

	
	<?php
		echo '<table style="float:right">
		<tr>
			<td>Created</td>
			<td>'.$row['created'].'</td>
		</tr>
		<tr>
			<td>Approved</td>
			<td>'.$row['tglapproved'].'</td>
		</tr>
		</table>';
	?>

	<?php /* START created by ASP 20180407*/ ?>
    <br />
    <!-- PACKING LIST -->
	<?php 
	foreach ($items_get_wh as $items_wh_item) {
	//foreach ($items_wh as $items_wh_item){
		echo '<div  class="pageBreak" ></div>
		<div style="border-top: dotted 1px; clear:both; margin-bottom:2px" ></div>';
		echo '
		
		<div id="companyDetails" style="border-left:dashed 1px #000000; padding: 0 0 0 20px; width:300px;">
			
			<strong>
				Penerima : '.$row['pic_name'].'<br>
				'.$row['pic_hp'].'<br>
				Alamat Pengiriman :<br>
				'.$row['del_alamat'].'<br>
				'.$row['del_kecamatan'].' - '.$row['del_kelurahan'].'
				'.$row['del_kota'].' - '.$row['del_propinsi'].'
				'.$row['del_kodepos'].'
			</strong>
		</div>
		
		<p>
			No. Invoice: '.$row['id'].'<br>
			'.$row['tgl'].'
			<br />
			<h2>Packing List Stockiest Consignment</h2>
			Warehouse Name : '.$items_wh_item['warehouseName'].'
		</p>
		<hr>';
		
		
		echo '<h3>'.$row['no_stc']," - ".$row['nama'].'</h3>';
		echo '<p>'.$row['alamat']."<br />";
			echo $row['kota']." - ".$row['propinsi']." ".$row['kodepos']."<br />";
			echo "Remark: ".$row['remark']."<br />";
			echo "User ID: ".$row['createdby'].'</p>';
		echo '
		<div class="col-md-6">
		<table class="stripe" >
		<tr>
		  <th width="80px">Item Code</th>
		  <th>Item Name</th>
		  <th width="50px"><div align="center">Qty</div></th>
		</tr>';
		$counterpl =0;
		$items_pl = $this->MSc->getPinjamanDetail_p($row['id']);

		foreach ($items_pl as $itempl): 
		$counterpl = $counterpl+1;
		if ($itempl['warehouse_id']==$items_wh_item['warehouse_id']){
			echo '
			<tr>
				<td>'.$itempl['item_id'].'</td>
				<td>'.$itempl['name'].'</td>
				<td align="right">'.$itempl['fqty'].'</td>
			</tr>
			';
		}
		endforeach;
		echo '</table>';
		/** Free_Item_Name Packing List **/
		$strFreeItemo='';
		$item_pl_perwh = $this->MSc->search_FreeItemwhs($scid,$items_wh_item['warehouse_id'],"","");
		if($item_pl_perwh){
		//if ($items_free){
			$strFreeItemH='
			<h2>Free Item</h2>
			<table class="stripe">
			<tr>
			  <th width="80px">Item Code</th>
			  <th >Item Name</th>
			  <th width="50px"><div align="center">Qty</div></th>
			</tr>';
			$strFreeItemF='</table>';
			foreach ($items_free as $var_freeitem){
				if ($var_freeitem['warehouse_id']==$items_wh_item['warehouse_id']){
					$strFreeItemo=$strFreeItemo.'
					<tr>
						<td>'.$var_freeitem['free_item_id'].'</td>
						<td>'.$var_freeitem['Free_Item_Name'].'</td>
						<td align="right">'.$var_freeitem['GetFree'].'</td>
					</tr>';
				} /*else {
					$strFreeItemH='';
					$strFreeItemF='';
				}*/
			}
			
			if($strFreeItemo==''){
					$strFreeItemH='';
					$strFreeItemF='';
			}
			
			echo $strFreeItemH.''.$strFreeItemo.''.$strFreeItemF;
			
		}
		
		echo '</div>';
		/** End Free Item **/
		
		echo '<div id=clearer></div>';
		echo'
		<table width=100%>
		<tr>
		  <td colspan="1" align=center width="5%"> </td>
		  <td colspan="2" align=center width="25%">Sales Counter</td>
		  <td colspan="1" align=center width="5%"> </td>
		  <td colspan="2" align=center width="30%">Warehouse</td>
		  <td colspan="1" align=center width="5%"> </td>
		  <td colspan="2" align=center width="25%">Receiver</td> 	
		  <td colspan="1" align=center width="5%"> </td>
		  
		</tr>
		<tr>
		<td colspan="10">
		<br><br><br><br><br><br>
		</td>
		</tr>
		
		<tr>
			<td></td>
			<td align="left">(</td><td align="right">)</td>
			<td></td>
			<td align="left">(</td><td align="right">)</td>
			<td></td>
			<td align="left">(</td><td align="right">)</td> 
			<td></td>
		</tr>
		</table>
		';
	}
	
	?>

<?php $this->load->view('footer'); ?>
