<?php 
if($results) {
	echo '
	<table class="stripe" width="100%" border="0" cellpadding="0" cellspacing="0">				
	<tr>
		<th width="50px">No</th>
		<th width="180px">Item Code</th>
		<th width="200px">Item Name</th>
		<th width="70px">Qty</th>
		<th width="70px" style="display:none;" >Retur</th>
		<th width="110px" colspan="2"><center>Pelunasan</center></th>
		<th width="70x" align="right">Sisa</th>
		<th width="70px" >Price</th>
		<th width="80px">PV</th>
		<th width="80px">Sub Total Price</th>
		<th width="80px">Sub Total PV</th>
		<!--<th style="min-width:20px;max-width:25px;">Pay?</th>-->
	</tr>';
		
		$counter = $from_rows; 
		foreach($results as $item) { 
			$counter = $counter + 1; 
			
			$aSisa=($item['fqty']-$item['fqty_retur'])-$item['fqty_lunas'];
			$lockLunas='';
			if ($aSisa=='0'){
				$lockLunas='readonly="1"';	
			}
			$rpdiskon=0;
			$subrpdiskon=0;
			$price=$item['fharga'];
			$pricePV=$item['fpv'];
			
			$price=0;
			$pricePV=0;
			
			
			$dtPrice=$this->MSc_retur->getPriceItem($item['warehouse_id'],$item['item_id']);
			$persen = $this->session->userdata('r_persen');
			$dtMin=0;
			if ($dtPrice){
				foreach ($dtPrice as $dtPrice_Item){
					if($this->session->userdata('r_timur') == '1'){
						$price = number_format($dtPrice_Item['price2'],0); 
					}else{
						$price = number_format($dtPrice_Item['price'],0);
					}
					
					$pricePV=number_format($dtPrice_Item['pv'],0);
					$dtMin=$dtPrice_Item['min_order'];
					if($dtPrice_Item['pv'] > 0 and $persen > 0){
						if($dtPrice_Item['nonstcfee'] == 0) {
							$rpdiskon=number_format(round((($price - $dtPrice_Item['price_not_disc'])*$persen)/100,0),'0','','.');
							$subrpdiskon =number_format(round(((($price - $dtPrice_Item['price_not_disc'])*$persen)/100)*$dtPrice_Item['min_order'],0),'0','','.');
						}else{
							$rpdiskon='0'; 
							$subrpdiskon='0';
						}
					}else{
						$rpdiskon="0";
						$subrpdiskon="0";
					}
				}
			}
			/*
			echo '
			<tr>
				<td>
					<input type="hidden" id="pinjam_id'.$counter.'" name="pinjam_id'.$counter.'" value="'.$item['id'].'" readonly="1" size=10 >
					<input type="hidden" id="pin_d'.$counter.'" name="pin_d'.$counter.'" value="'.$item['id'].'" readonly="1" size=10 >
					<input type="text" id="itemcode'.$counter.'" name="itemcode'.$counter.'" value="'.$item['item_id'].'" readonly="1" size=10 >
					<input type="hidden" id="det_whsid'.$counter.'" name="det_whsid'.$counter.'" value="'.$item['warehouse_id'].'" readonly="1" size=10 >
					<input type="hidden" id="rpdiskon'.$counter.'" name="rpdiskon'.$counter.'" value="'.$rpdiskon.'" readonly="1" size=10 >
					<input type="hidden" id="subrpdiskon'.$counter.'" name="subrpdiskon'.$counter.'" value="'.$subrpdiskon.'" readonly="1" size=10 >
					<input class="textbold aright" type="hidden" name="min_qty'.$counter.'" id="min_qty'.$counter.'"  value="'.$dtMin.'" maxlength="12" size="3" tabindex="3"  >
					<span class="error">'.form_error('qty_lunas'.$counter).'</span>
				</td>
				<td><input type="text" id="itemname'.$counter.'" name="itemname'.$counter.'" value="'.$item['name'].'" readonly="1" size=33 ></td>
				<td align="right"><input class="textbold aright" type="text" id="qty'.$counter.'" name="qty'.$counter.'" value="'.$item['fqty'].'" maxlength="12" size="3" tabindex="3" autocomplete="off" readonly="1"  ></td>
				<td align="right" style="display:none;"><input class="textbold aright" type="hidden" id="qty_retur'.$counter.'" name="qty_retur'.$counter.'" value="'.$item['fqty_retur'].'" maxlength="12" size="3" tabindex="3" autocomplete="off" readonly="1"  ></td>
				<td align="right" ><input class="textbold aright" type="hidden" id="qty_lunas_op'.$counter.'" name="qty_lunas_op'.$counter.'" value="'.$item['fqty_lunas'].'" readpnly="1" maxlength="3" size="4" tabindex="4" autocomplete="off" ></td>
				<td align="right"><input class="textbold aright" type="text" id="qty_lunas'.$counter.'" name="qty_lunas'.$counter.'" value="'.$this->input->post('qty_lunas'.$counter).'" '.$lockLunas.' maxlength="3" size="5" tabindex="4" autocomplete="off" required onKeyUp=HitTotal("'.$counter.'") ></td>
				<td align="right"><input class="textbold aright" type="text" id="qty_sisa'.$counter.'" name="qty_sisa'.$counter.'" value="'.$aSisa.'" size="3" tabindex="5" autocomplete="off" readonly="1" ></td>
				<td align="right"><input class="aright" type="text" id="price'.$counter.'" name="price'.$counter.'" value="'.$price.'" size="9" readonly="1" autocomplete="off"></td>
				<td align="right"><input class="aright" type="text" id="pv'.$counter.'" name="pv'.$counter.'" value="'.$pricePV.'" size="9" readonly="1" ></td>
				<td align="right"><input class="aright" type="text" id="subtotal'.$counter.'" name="subtotal'.$counter.'" value="'.$this->input->post('subtotal'.$counter).'" readonly="1" size="12" ></td>
				<td align="right"><input class="aright" type="text" id="subtotalpv'.$counter.'" name="subtotalpv'.$counter.'" value="'.$this->input->post('subtotalpv'.$counter).'" readonly="1" size="12" ></td>
				<!--<td>'.$strCheck.'</td>-->
			</tr>
			';
			*/
			echo '
			<tr>
				<td>'.$counter.'</td>
				<td>'.$item['item_id'].'</td>
				<td>'.$item['name'].'</td>
				<td align="right">'.$item['fqty'].'</td>
				<td align="right" style="display:none;">'.$item['fqty_retur'].'</td>
				<td align="right" >'.$item['fqty_lunas'].'</td>
				<td align="right">'.$this->input->post('qty_lunas'.$counter).'</td>
				<td align="right">'.$aSisa.'</td>
				<td align="right">'.$price.'</td>
				<td align="right">'.$pricePV.'</td>
				<td align="right"></td>
				<td align="right"></td>
				<!--<td>'.$strCheck.'</td>-->
			</tr>
			';
			
		}
		
	echo '
	
	</table>';
}
?>

<script type="text/javascript">
function showDataPop(varId){
	//alert (varId);
	/*
	var no_stc=document.getElementById("p1"+varId).innerHTML;
	var member_id=document.getElementById("p0"+varId).innerHTML;
	var member_nama=document.getElementById("p2"+varId).innerHTML;
	var member_tipe=document.getElementById("p4"+varId).innerHTML;
	var member_ewal=document.getElementById("p3"+varId).innerHTML;
	var member_whs=document.getElementById("p5"+varId).innerHTML;
	var member_whs_nm=document.getElementById("p6"+varId).innerHTML;
	var member_timur=document.getElementById("p7"+varId).innerHTML;
	
	window.opener.document.getElementById("member_id").value =member_id;
	window.opener.document.getElementById("no_stc").value=no_stc;
	window.opener.document.getElementById("member_nama").value =member_nama;
	window.opener.document.getElementById("persen_to").value =member_tipe;
	window.opener.document.getElementById("ewal_to").value =member_ewal;
	window.opener.document.getElementById("whsid_to").value =member_whs;
	window.opener.document.getElementById("whs_nm_to").value =member_whs_nm;
	window.opener.document.getElementById("timur").value =member_timur;
	*/
	window.close();
}

</script>