<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->

<?php $this->load->view('header');?>

<h2><?php echo $page_title;?></h2>
	
	 <?php 
	 //echo form_open('order/pjm/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
	 
	 if ($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}
	 echo form_open('inv/sc/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
	 ?>

	<table width='50%' style="float:left;">
		<tr>
			<td width='20%'>Date</td>
			<td width='1%'>:</td>
			<td width='79%'>
				<?php echo date('Y-m-d',now());?></td>
		</tr>
        <tr>
			<td class='error' >Stockist ID*</td>
			<td>:</td>
			<td>
				<?php 
					echo form_hidden('member_id',set_value('member_id'));
					$data = array('name'=>'no_stc','id'=>'no_stc','size'=>15,'readonly'=>'1','value'=>set_value('no_stc'));
					echo form_input($data);
					$atts = array(
						'width'      => '450',
						'height'     => '600',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'yes',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					//echo anchor_popup('search/stockistsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					echo anchor_popup('search/scsearch/member_sc/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
				?>
				<span class='error'>*<?php echo form_error('member_id');?></span>
			</td>	
		</tr>
		<tr>
			<td>Stockist Name</td>
			<td>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
		<tr>
			<td>Diskon %</td>
			<td>:</td>
			<td><input class="aright" type="text" name="persen" id="persen" value="<?php echo set_value('persen',0);?>"  /></td>
		</tr>
	</table>
	<table width='40%'>
		<tr>
			<td>Remark :</td>
		</tr>
		<tr>
			<td>
				<textarea id="remark" name="remark" style="min-width:100%;max-width:100%;min-height:100px;max-height:100px;"><?php echo $this->input->post('remark'); ?></textarea>
			</td>
		</tr>	
	</table>
	
	<div id="clearer"></div>
	<hr>
	<div id="clearer"></div>
	<table width='35%' style="float:left">	
		<tr>
			<td colspan=3><h4>Pick up / Delivery</h4></td>
		</tr>
		<tr>
			<td >Option</td>
			<td>:</td>
            <td ><?php
					$options = array(						
						'1'    => 'Delivery',
						'0'  => 'Pick Up'
					);
					echo form_dropdown('pu', $options);
				?>
			</td>
		</tr>
		<tr>
			<td class="error">Penerima *</td>
			<td>:</td>
			<td valign="top"><input type="text" name="pic_name" id="pic_name" value="<?php echo $this->input->post('pic_name');?>" size="30" required /></td>
		</tr>
		<tr>
			<td class="error">No HP *</td>
			<td>:</td>
			<td><input type="text" name="pic_hp" id="pic_hp" value="<?php echo $this->input->post('pic_hp');?>" size="30" required  /></td>
		</tr>
	</table>
	<table width="35%" style="float:left;">
		<tr>
			<td colspan=3><h4>Delivery Address</h4></td>
		</tr>
		<tr>
			<td>City of delivery</td>
			<td>:</td>
			<td>
				<?php
				/** Ini Variabel Tidak Terpakai Tapi Kalau di hapus bikin error ga semapt Benerin.. **/
				echo form_hidden('pic_name1',$this->input->post('pic_name1') );
				echo form_hidden('pic_hp1',$this->input->post('pic_hp1') );
				echo form_hidden('kelurahan1',$this->input->post('kelurahan1') );
				echo form_hidden('kecamatan1',$this->input->post('kecamatan1') );
				echo form_hidden('kodepos1',$this->input->post('kodepos1') );
				/** End Off Ini Variabel Tidak Terpakai Tapi Kalau di hapus bikin error ga semapt Benerin.. **/
				
				//echo form_input('whsid', $whsid );
				
				if (empty($this->input->post('whsid'))){$varWh=$whsid;}else{$varWh=$this->input->post('whsid');}
				echo '<input type="hidden" name="whsid" id="whsid" value="'.$varWh.'" >';
				echo '<input type="hidden" name="kota_id" id="kota_id" value="'.$this->input->post('kota_id').'" >';
				echo '<input type="hidden" name="timur" id="timur" value="'.$this->input->post('timur').'" >';
				echo '<input type="hidden" name="propinsi" id="propinsi" value="'.$this->input->post('propinsi').'" >';

				echo form_hidden('deli_ad', $this->input->post('deli_ad'));
				
				echo form_hidden('addr1', $this->input->post('addr1'));
				echo form_hidden('kota_id1', $this->input->post('kota_id1'));
				
				//$data = array('name'=>'city','id'=>'city','readonly'=>'1','value'=>set_value('city',$row['kota']));
				$data = array('name'=>'city','id'=>'city','readonly'=>'1','value'=> $this->input->post('city') );
				echo form_input($data);
				$atts = array(
					'width'      => '400',
					'height'     => '400',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'yes',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				echo anchor_popup('search/scsearch/sc_city_search/', '<input class="button" type="button" name="Button" value="browse" />', $atts);
				?>
				<span class='error'>*<?php echo form_error('city'); ?></span>
				
			</td>
		</tr>
		<tr>
			<td class="error" >Warehouse Delivery *</td>
			<td >:</td>
			<td >
				<input type="text" name="whsname" id="whsname" value="<?php echo $this->input->post('whsname');?>" required >
			</td>
		</tr>
		<tr>
			<td class="error" >Kecamatan*</td>
			<td >:</td>
			<td >
				<input type="text" name="kecamatan" id="kecamatan" value="<?php echo $this->input->post('kecamatan');?>" required >
			</td>
		</tr>
		<tr>
			<td class="error" >Kelurahan*</td>
			<td>:</td>
			<td>
				<input type="text" name="kelurahan" id="kelurahan" value="<?php echo $this->input->post('kelurahan');?>" required >
			</td>
		</tr>
	</table>
	<table width='30%' style="float:left">	
		<tr>
			<td valign='top'>Delivery Address</td>
			<td valign='top'>:</td>
			<td valign="top"><input type="hidden" name="xaddr" id="xaddr" value="<?php echo $this->input->post('xaddr'); ?>" size="30" /></td>
		</tr>
		<tr>
			<td colspan='3'>
				<textarea id="addr" name="addr" style="min-width:100%;max-width:100%;min-height:60px;max-height:60px;"><?php echo $this->input->post('addr') ?></textarea>
			</td>
		</tr>	
		<tr>
			<td>Kodepos</td>
			<td>:</td>
			<td>
				<input type="text" name="kodepos" id="kodepos" value="<?php echo $this->input->post('kodepos') ?>" >
			</td>
		</tr>
	</table>
	<br>
	<div id="clearer" style="height:20px;"></div>
	<hr>
	<br>
		
		<table width='100%'>	
		<tr>
			<td width='18%'>Item Code</td>
			<td width='23%'>Item Name</td>
			<td width='8%'>Qty</td>
			<td width='12%'>Price</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>Del?</td>
		</tr>
		<?php
		$i=0;
		if ($counti<5){$counti=10;}
		while($i < $counti){
			echo '
				<tr>
					<td valign="top">
				'; 
				echo form_hidden('counter[]',$i); 
				$data = array('name'=>'itemcode'.$i,'id'=>'itemcode'.$i,'size'=>'8','readonly'=>'1','value'=>$this->input->post('itemcode'.$i)); 
				echo form_input($data);
				$atts = array(
					'width'      => '600',
					'height'     => '500',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'no',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				//$this->MSc->search_stock_whs(whs,keyword,flag,$num,$offset);
				//echo anchor_popup('search/scsearch/item_sc_whs/'.$whsid.'/0/'.$i.'/1/1/', '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
				//echo anchor_popup('itempjmsearch/index/0', '<input class="button" type="button" tabindex="2" name="Button" value="browse" />', $atts);
				echo '<input class="button" type="button" tabindex="2" name="Button" value="browse" style="background: rgb(255, 255, 255);" onclick=showPopUpItem("'.$i.'") >';
			
			
			
				
				echo '
				<td valign="top">
					<input type="hidden" name="det_whsid'.$i.'" id="det_whsid'.$i.'" value="'.$this->input->post('det_whsid'.$i).'" readonly="1" size="24" />
					<input type="hidden" name="bv'.$i.'" id="bv'.$i.'" value="'.$this->input->post('bv'.$i).'" readonly="1" size="24" />
					<input type="hidden" name="free_item_qty'.$i.'" id="free_item_qty'.$i.'" value="'.$this->input->post('free_item_qty'.$i).'" readonly="1" size="24" />
					<input type="text" name="itemname'.$i.'" id="itemname'.$i.'" value="'.$this->input->post('itemname'.$i).'" readonly="1" size="24" />
				</td>
				
			<td>
				<input class="textbold aright" type="text" name="qty'.$i.'" id="qty'.$i.'"  value="'.$this->input->post('qty'.$i).'" maxlength="12" size="3" tabindex="3" onChange=HitTotal("'.$i.'","'.$counti.'") >
				<input class="textbold aright" type="hidden" name="min_qty'.$i.'" id="min_qty'.$i.'"  value="'.$this->input->post('min_qty'.$i).'" maxlength="12" size="3" tabindex="3"  >
			</td>
			<td>
				<input class="aright" type="text" readonly="readonly" name="price'.$i.'" id="price'.$i.'" size="8" value="'.$this->input->post('price'.$i).'" >
				<input class="textbold aright" type="hidden" name="qty_avai'.$i.'" id="qty_avai'.$i.'"  value="'.$this->input->post('qty_avai'.$i).'" maxlength="12" size="2" tabindex="3" >
				
			</td>
			<td>
				<input class="aright" type="text" readonly="readonly" name="pv'.$i.'" id="pv'.$i.'"
					value="'.$this->input->post('pv'.$i).'" size="5" 
					onkeyup="this.value=formatCurrency(this.value); 
						jumlah(document.form.qty'.$i.',document.form.price'.$i.',document.form.subtotal'.$i.');
						jumlah(document.form.qty'.$i.',document.form.pv'.$i.',document.form.subtotalpv'.$i.');
						jumlah(document.form.qty'.$i.',document.form.rpdiskon'.$i.',document.form.subrpdiskon'.$i.');
						document.form.total.value=total_curr('.$counti.',"document.form.subtotal");
						document.form.totalpv.value=totalpv_curr('.$counti.',"document.form.subtotalpv");
                        document.form.totalrpdiskon.value=totaldiskon_curr('.$counti.',"document.form.subrpdiskon");
                        document.form.totalrpdiskon.value=totaldiskon_curr('.$counti.',"document.form.subrpdiskon");
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
				">
			</td>
			<td>
				<input class="aright" type="text" name="subtotal'.$i.'" id="subtotal'.$i.'" value="'.$this->input->post('subtotal'.$i).'" readonly="1" size="12">
			</td>
			<td>
				<input class="aright" type="text" name="subtotalpv'.$i.'" id="subtotalpv'.$i.'" value="'.$this->input->post('subtotalpv'.$i).'" readonly="1" size="10">
			</td>
			<td>
				<img alt="delete" 
					onclick="
					cleartext7(
							document.form.itemcode'.$i.'
							,document.form.itemname'.$i.'
							,document.form.qty'.$i.'
                            ,document.form.price'.$i.'
							,document.form.pv'.$i.'
                            ,document.form.subtotal'.$i.'
							,document.form.subtotalpv'.$i.'
						); 
						document.form.total.value=total_curr('.$counti.',document.form.subtotal);
						document.form.totalpv.value=totalpv_curr('.$counti.',document.form.subtotalpv);
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);" src="'.base_url().'images/backend/delete.png" border="0"
				/>
			 </td>
		</tr>
		';
		
			$i++;
		}
		
		?>
		<tr>
			<td colspan='5'> &nbsp; Add &nbsp;<input name="rowx" type="text" id="rowx" value="<?=set_value('rowx','1');?>" size="1" maxlength="2" />
				<input name="counti" type="hidden" id="counti" value="<?php echo $counti ?>" size="1" maxlength="2" />
				&nbsp; Row(s) &nbsp; <?php echo form_submit('action', 'Go');?>
			</td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo $this->input->post('total') ?>" readonly="1" size="12"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo $this->input->post('totalpv') ?>" readonly="1" size="10" ></td>
		</tr>	
        <!-- START 20180530 ASP - free item --> 
		<tr>
        	<td colspan="100%">
				<hr>
			</td>
        </tr>
		<tr>
        	<td colspan="7">
				<span id="pray_msg" class="error"> <?php echo $pray_msg; ?></span>
				<span id="pray_msg" class="error"> <?php if(validation_errors() or form_error('total')){echo form_error('total');} ?></span>
			</td>
			<td colspan="1">
				<input type="submit" name="cmdSimpan" id="cmdSimpan" value="Submit">
			</td>
        </tr>
		
        <!-- EOF 20180530 ASP - free item 
		<tr>
			<td colspan='6'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit', 'tabindex="23"');?></td>
		</tr>
		-->
		
		</table>
		
<?php echo form_close();?>

<?php
if (count($promo_aktif)<1){
	echo '<h3>No Promo Found</h3>';
}else{
	echo '<br><hr>';
	echo '<h4><input class="button" type="Button" id="openPromoNow" value="Check Promo Available (+)"  onclick=\'document.getElementById("promoNow").style.display="block";document.getElementById("openPromoNow").style.display="none";document.getElementById("closePromoNow").style.display="block";\'>';
	echo '<h4><input class="button" type="Button" id="closePromoNow" value="Check Promo Available (-)" style="display:none;"  onclick=\'document.getElementById("promoNow").style.display="none";document.getElementById("openPromoNow").style.display="block";document.getElementById("closePromoNow").style.display="none";\'>';
	echo '<table width="100%" id = "promoNow" style="display:none;"><tr><td>';
	/*
	echo '<table class="rwd-table" style="width:50%;float:left;">';
	echo '<tr style="background:#34495E;border-color:#34495E;" >
	<th colspan=3 style="background:#34495E;border-color:#34495E;" > 
		<h3 align=center>Informasi Promo</h3>
	</th>
	</tr>';
	*/
	//echo '<tr><th>Description</th><th>Term</th><th>Expired Date</th></tr>';
	$eRes='';
	foreach ($promo_aktif as $item_promo){
		$eTest='';
		/*
		foreach ($items as $item){
			if ($item_promo['item_id']==$item['item_id']){
				$startdate = $item_promo['created_date'];
				$expire = strtotime($item_promo['end_periode']);
				$today = strtotime($item['tgl']);

				if($today >= $expire){
					$eFree= "0";
				} else {
					if ($item_promo['term']<=$item['fqty']){
						$eFree=number_format($item['fqty']/$item_promo['term']);
						$eRes=$eRes.'<tr>
							<td>'.$item_promo['nama'].'<br><h5>Item Code :'.$item_promo['item_id'].'</h5></td>
							<td>'.$eFree.'</td>
							<td>'.$item_promo['end_periode'].'</td>
						</tr>';
					}else{
						$eFree=0;
					}
				}
				
				//$eTest='style="background:#34495E;"';
				
			}
		}
		*/
		
		/*
		echo '<tr>';
		echo '<td '.$eTest.'>
		'.$item_promo['nama'].'<br><h5>Item Code :'.$item_promo['item_id'].' '.$item_promo['id'].'</h5>
		</td>';
		echo '<td '.$eTest.'>'.$item_promo['term'].' Get '.$item_promo['free_item_qty'].'</td>';
		echo '<td '.$eTest.'>'.$item_promo['end_periode'].'</td>';
		echo '</tr>';
		*/
		echo '<div class="col-md-6" style="margin:0px;"><table class="rwd-table" style="width:100%;margin:0px;">';
		echo '<td '.$eTest.'>
		'.$item_promo['nama'].'<br><h5>Item Code :'.$item_promo['item_id'].'<br><h5>Promo Code :'.$item_promo['id'].'</h5>
		</td>';
		echo '<td width="70px;">'.$item_promo['term'].' Get '.$item_promo['free_item_qty'].'</td>';
		echo '<td width="70px;"><center>'.$item_promo['end_periode'].'</center></td>';
		echo '</table></div>';
		
	}
	echo $eRes;
	//echo '</table>';
	echo '</tr></td></table>';
	echo '<hr>';
}
?>

<?php $this->load->view('footer');?>

<script type="text/javascript" src="<?php echo site_url() ?>js/jquery.js"></script>
<script type="text/javascript">

	function pray_cdec(var_str) {
		var xStr="0";
		xStr=var_str.replace(/[,.]/g, function (m) {
			return m === ',' ? '' : '';
		});
		return xStr
	}
	
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
	
	function GranTotal(){
		var gTotal=0;
		var gTotalPv=0;
		var iTotal=Number(document.getElementById("counti").value);
		for (i = 0; i < iTotal-1; i++) {
			if (document.getElementById("subtotal"+i).value!='0'){
				var iPv=pray_cdec(document.getElementById("pv"+i).value);
				var tPv=Number(document.getElementById("qty"+i).value.replace(/[^0-9.-]+/g,""))*Number(iPv);
				var tTl=Number(document.getElementById("qty"+i).value.replace(/[^0-9.-]+/g,""))*Number(document.getElementById("price"+i).value.replace(/[^0-9.-]+/g,""))
				gTotal=gTotal+tTl;
				gTotalPv=gTotalPv+tPv;
			}
		}
		document.getElementById("total").value=formatCurrency(gTotal);
		document.getElementById("totalpv").value=formatCurrency(gTotalPv);
	}
	
	function HitTotal(iCount,iTotal){
		$("#cmdSimpan").hide();
		var iqty=Number(document.getElementById("qty"+iCount).value.replace(/[^0-9.-]+/g,""))
		var min_qty=Number(document.getElementById("min_qty"+iCount).value.replace(/[^0-9.-]+/g,""));
		var qty_avai=Number(document.getElementById("qty_avai"+iCount).value.replace(/[^0-9.-]+/g,""));
		var sisa = iqty % min_qty;
		
		if (iqty<1){
			alert("Qty Harus di isi");
		}else{
			if (sisa>0){
				alert("Qty Harus berupa kelipatan "+min_qty);
				document.getElementById("qty"+iCount).value=min_qty;
			}else if (iqty>qty_avai){
				alert("Stock Tidak Mencukupi");
				$("#cmdSimpan").show();
				document.getElementById("qty"+iCount).value=min_qty;
			}else{
				$("#cmdSimpan").show();
			}
		}
		
		var iX=pray_cdec(document.getElementById("pv"+iCount).value);
		var xVar=Number(document.getElementById("qty"+iCount).value.replace(/[^0-9.-]+/g,""))*Number(iX);
		var xPric=Number(document.getElementById("qty"+iCount).value.replace(/[^0-9.-]+/g,""))*Number(document.getElementById("price"+iCount).value.replace(/[^0-9.-]+/g,""));
		document.getElementById("subtotal"+iCount).value=formatCurrency(xPric);
		document.getElementById("subtotalpv"+iCount).value=formatCurrency(xVar);
		/*
		for (i = 0; i < iTotal-1; i++) {
			if (document.getElementById("subtotal"+i).value!='0'){
				var iPv=pray_cdec(document.getElementById("pv"+i).value);
				var tPv=Number(document.getElementById("qty"+i).value.replace(/[^0-9.-]+/g,""))*Number(iPv);
				var tTl=Number(document.getElementById("qty"+i).value.replace(/[^0-9.-]+/g,""))*Number(document.getElementById("price"+i).value.replace(/[^0-9.-]+/g,""))
				gTotal=gTotal+tTl;
				gTotalPv=gTotalPv+tPv;
			}
		}
		document.getElementById("total").value=formatCurrency(gTotal);
		document.getElementById("totalpv").value=formatCurrency(gTotalPv);
		*/
		GranTotal();
		//aCheck();
		
	}
	
	function showPopUpItem(iRow){
		var timur=document.getElementById("timur").value;
		if (timur==""){timur=0;} /* <-- Error If Null */
		
		
		var url="<?php echo site_url() ?>search/scsearch/item_sc_whs/"+document.getElementById("whsid").value;
		//var url_param='/'+timur+"/0/"+iRow+"/1/1/";
		var url_param='/'+timur+"/0/"+iRow+"/1/";
		
		window.open(url+url_param, 'Search', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=600, height=500');		
	}
	
function aCheck(){
	$.ajax({
	  method: "POST",
	  url: "<?php echo site_url() ?>inv/sc/chstock",
	  dataType : 'text',
	  data: {
			whsid : $('#whsid').val(),
			member_id : $('#member_id').val(),
			total : $('#total').val()
	  },
	  success: function(data){
			$("#pray_msg").html(data);
	  },
      error : function(XMLHttpRequest, textStatus, errorThrown) {
            $("#pray_msg").html("Error.."+textStatus);
      }
	});
}

function clearTxt(){
	document.getElementById("whsid").value="";
	document.getElementById("whsname").value="";
}
	
</script>