<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php 
	echo anchor('inv/sc_payment/create','create stockiest consignment payment'); // update by Boby 20140416

	if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>

<table width='99%'>
<?php echo form_open('inv/sc_payment', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='50%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='50%' align='right'>search by: <?php if($this->session->userdata('group_id') < 100 && $this->session->userdata('group_id')!= 28) echo form_dropdown('whsid',$warehouse);?> <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b>
			<?php }?>
			
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<?php $pay_Idx=$this->MSc_payment->getPayment('8310'); ?>
<table class="stripe">
	<tr>
      <th width='3%'>No.</th>
      <th width='8%'>Invoice No.</th>
      <th width='8%'>Date</th>
      <th width='20%'>Stockiest ID / Name</th>
      <th width='11%'><div align="right">Total Price</div></th>
      <th width='11%'><div align="right">Total PV</div></th>
      <th width='18%'><div align="center">Warehouse</div></th>
      <!--<th width='11%'><div align="right">Req Time</div></th>-->
      <!--<th width='11%'><div align="right">App Time</div></th>-->
      <th width='5%'><div align="right">Qty</div></th>
      <th width='5%'  style="display:none;" ><div align="right">Free</div></th>
      <th width='5%'><div align="right">Pelunasan</div></th>
    </tr>
   
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
	
	$strPay='';
	$pay_Id=$this->MSc_payment->getPayment($row['id']);
	$pay_FreeItem=$this->MSc->search_FreeItem($row['id'],"","");
	
	$freeQty=0;
	if ($pay_FreeItem){
		foreach ($pay_FreeItem as $pay_FreeItem_item){
			$freeQty=$freeQty+$pay_FreeItem_item['qty'];
		}
	}
	
	if ($pay_Id){
		foreach ($pay_Id as $pay_Id_item){
			$item_freeQty=0;
			if ($pay_FreeItem){
				foreach ($pay_FreeItem as $pay_FreeItem_item){
					if ($pay_FreeItem_item['pinjaman_d_id']==$pay_Id_item['pinjaman_d_id']){
						$item_freeQty=$pay_FreeItem_item['qty_lunas'];
					}
				}
			}
			$strPay=$strPay.'<tr style="background:none;">
			<td style="background:none;"></td>
			<td style="background:none;" align="right"><a href="'.site_url().'smartindo/roadmin_v/view/'.$pay_Id_item['ro_id'].'">'.$pay_Id_item['ro_id'].'</a></td>
			<td style="background:none;">'.anchor('smartindo/roadmin_v/view/'.$pay_Id_item['ro_id'],$pay_Id_item['date']).'</td>
			<td style="background:none;">'.anchor('smartindo/roadmin_v/view/'.$pay_Id_item['ro_id'],$pay_Id_item['member_nama_to'].' - '.$pay_Id_item['nama_item']).'</td>
			<td align="right" style="background:none;">'.anchor('smartindo/roadmin_v/view/'.$pay_Id_item['ro_id'],$pay_Id_item['ftotal_pay']).'</td>
			<td align="right" style="background:none;"></td>
			<td style="background:none;">'.anchor('smartindo/roadmin_v/view/'.$pay_Id_item['ro_id'],$pay_Id_item['warehouse_nama']).'</td>
			<td align="right" style="background:none;">'.anchor('smartindo/roadmin_v/view/'.$pay_Id_item['ro_id'],$pay_Id_item['qty']).'</td>
			<td align="right" style="background:none;display:none;">'.$item_freeQty.'</td>
			<td style="background:none;"></td>
			</tr>';
		}
	}
?>
    <tr>
		<td style="background:#F6F6F6;" align="right"><b><?php echo anchor('inv/sc/view/'.$row['id'], $counter);?></b></td>
		<td style="background:#F6F6F6;"><b>SC : <?php echo anchor('inv/sc/view/'.$row['id'], $row['id']);?></b></td>
		<td style="background:#F6F6F6;"><b><?php echo anchor('inv/sc/view/'.$row['id'], $row['tgl']);?></b></td>
		<td style="background:#F6F6F6;"><b><?php echo anchor('inv/sc/view/'.$row['id'], $row['no_stc']." - ".$row['nama']);?></b></td>
		<td style="background:#F6F6F6;" align="right"><b><?php echo anchor('inv/sc/view/'.$row['id'], $row['ftotalharga']);?></b></td>
		<td style="background:#F6F6F6;" align="right"><b><?php echo anchor('inv/sc/view/'.$row['id'], $row['ftotalpv']);?></b></td>
		<td style="background:#F6F6F6;" align="left"><b><?php echo anchor('inv/sc/view/'.$row['id'], $row['warehouse_name']);?></b></td>
		<!--<td align="right"><?php echo anchor('inv/sc/view/'.$row['id'], $row['created']);?></td>-->
		<!--<td align="right"><?php echo anchor('inv/sc/view/'.$row['id'], $row['appdate']);?></td>-->
		<td style="background:#F6F6F6;"  align="right"><b><?php echo $row['qty'] ?></b></td>
		<td style="background:#F6F6F6;display:none;"  align="right"><b><?php echo $freeQty ?></b></td>
		<td style="background:#F6F6F6;"  align="right"><b><?php echo $row['qty_lunas'] ?></b></td>
    </tr>
	
    <?php 
	echo $strPay;
	endforeach; 
 else:
 ?>
    <tr>
      <td colspan="9">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php 
$this->load->view('footer');3
?>