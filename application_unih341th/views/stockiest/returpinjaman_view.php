<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->

<?php $this->load->view('header');?>
<div class="ViewHold">
	<div id="companyDetails">
		<?php echo "<img src='".site_url()."/images/backend/logo.jpg' border='0' ><br>";?>
		
	</div>

	<p>
		<strong>
			<?php echo "No. Invoice: ";?> <?php echo $row['id'];?><br />
			<?php
				echo $row['tgl'];
			?>
		</strong>
		
	</p>
	<br />
			<h2>Retur Stock Stockiest</h2>
	<hr />

	<h3><?php echo $row['no_stc']," - ".$row['nama'];?></h3>

	<p>
		<?php
			echo $row['alamat']."<br />";
			echo $row['kota']." - ".$row['propinsi']." ".$row['kodepos'];
		?>
	</p>
	<p>
		<?php
			echo "Remark: ".$row['remark']."<br />";
			echo "Remark: ".$row['createdby'];
		?>
	</p>	
	
	<table class="stripe">
	<tr>
      <th width='10%'>Item Code</th>
      <th width='25%'>Item Name</th>
      <th width='10%'>Qty</th>
      <th width='10%'>Price</th>
      <th width='10%'>PV</th>
      <th width='20%'>Sub Total</th>      
      <th width='15%'>Sub Total PV</th>
    </tr>
    
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><?php echo $item['item_id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td align="right"><?php echo $item['fqty'];?></td>
			<td align="right"><?php echo $item['fharga'];?></td>
			<td align="right"><?php echo $item['fpv'];?></td>
			<td align="right"><?php echo $item['fsubtotal'];?></td>
			<td align="right"><?php echo $item['fsubtotalpv'];?></td>
		</tr>
		<?php endforeach;?>
		<tr>
			<td colspan='5' align='right'><b>Total Rp.</b>&nbsp;</td>
			<td align="right"><b><?php echo $row['ftotalharga'];?></b></td>
			<td align="right"><b><?php echo $row['ftotalpv'];?></b></td>
		</tr>
	</table>
	
	<div  id=clearer style="height:20px" ></div>
	<div class="col-md-6">
	<?php
	$strFreeItem='';
	if ($items_free){
		echo '<h2>Free Item</h2>';
		echo '
		<table class="stripe">
		<tr>
		  <th width="80px">Item Code</th>
		  <th >Item Name</th>
		  <th width="50px"><div align="center">Qty</div></th>
		</tr>
		';
		foreach ($items_free as $var_freeitem){
			$strFreeItem=$strFreeItem.'
			<tr>
				<td>'.$var_freeitem['free_item_id'].'</td>
				<td>'.$var_freeitem['Free_Item_Name'].'</td>
				<td align="right">'.$var_freeitem['GetFree'].'</td>
			</tr>';
		}
		echo $strFreeItem;
		echo '</table>';
	}
	
	?>
	</div>
<?php
$this->load->view('footer');
?>
