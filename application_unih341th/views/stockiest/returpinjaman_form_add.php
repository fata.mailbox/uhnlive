<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php
	if ($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}
	echo form_open('inv/sc_retur/add', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>

	<div class="col-md-12">
	<table width='80%'  style="float:left">
		<tr>
			<td colspan=3><h4>Stockiest Info :</h4></td>
		</tr>
		<tr>
			<td width='25%'>SC ID</td>
			<td width='1%'>:</td>
			<td width='79%'><?=$this->session->userdata('r_sc_id')?></td>
		</tr>
		<tr>
			<td>Date</td>
			<td>:</td>
			<td><?=date('Y-m-d',now());?></td>
		</tr>
        <tr>
			<td valign='top'>Stockist ID</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php echo form_hidden('member_id',$this->session->userdata('r_member_id_from')); echo $this->session->userdata('r_stc_id_from');?>
				<span class='error'><?php echo form_error('member_id');?></span>
			</td>	
		</tr>
		<tr>
			<td valign='top'>Stockist Name</td>
			<td valign='top'>:</td>
			<td><?php echo $this->session->userdata('r_name_from') ?></td>
		</tr>
		<tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td><?php echo $this->session->userdata('r_whs_from');?></td>
		</tr>
        <tr style="display:none">
			<td valign='top'>Ewallet Rp</td>
			<td valign='top'>:</td>
			<td><?=$row->fewallet;?></td>
		</tr>
		 <tr style="display:none;">
			<td valign='top'>Nominal SC</td>
			<td valign='top'>:</td>
			<td><?=$this->session->userdata('r_nominal');?></td>
		</tr>
	</table>
		
		</div>
		<div id="clearer"></div>

		<hr>
		
        <table width='100%'>	
		<tr>
			
			<td width='180px'>Item Code</td>
			<td width='200px'>Item Name</td>
			<td width='70px'>Qty</td>
			<td width='70px' style="display:none;" >Retur</td>
			<td width='110px' colspan="2"><center>Qty Retur</center></td>
			<td width='70x' align="right">Sisa</td>
			<td width='70px' >Price</td>
			<td width='80px'>PV</td>
			<td width='80px'>Sub Total Price</td>
			<td width='80px'>Sub Total PV</td>
			<!--<td style="min-width:20px;max-width:25px;">Pay?</td>-->
		</tr>
		<?php
		$iRow=0;
		$iTotal=0;
		foreach ($item_results as $item){
			/*
			$min_order=0;
			$dt_rule=$this->MSc_payment->get_FreeItem_Full($item['item_id'],"0","0");
			if ($dt_rule){
				foreach ($dt_rule as $dt_rule_item){
					$min_order=$dt_rule_item['min_order'];
				}
			}else{
				$min_order=0;
			}
			Min ordernya di ambil saat pengecekan harga terbaru
			*/
			$iTotal=$iTotal+str_replace(",","",$item['fsubtotal']);
			$iTotalPv=$iTotalPv+str_replace(",","",$item['fsubtotalpv']);
			
			$strCheck='
			<div style="float:left;">
				<label class="container_ijo" >
				  <input type="checkbox" name="p_id_pay[]" value="'.$item['id'].'" id="p_id_pay[]" >
				  <span class="checkmark" style="float:left;max-width:17px;"></span>
				</label>
			</div>';
			$aSisa=($item['fqty']-$item['fqty_retur'])-$item['fqty_lunas'];
			$lockLunas='';
			if ($aSisa=='0'){
				$lockLunas='readonly="1"';	
			}
			
			
			$rpdiskon=0;
			$subrpdiskon=0;
			$price=$item['fharga'];
			$pricePV=$item['fpv'];
			
			/*
			Tidak Melakukan Pengecekan harga terbaru agar saat melakukan pengecekan di tabel stok harga sesuai saat melakukan consignment, karena harga yang tersimpan di stok
			adalah harga saat melakukan consignment.
			$price=0;
			$pricePV=0;
			
			$dtPrice=$this->MSc_retur->getPriceItem($item['warehouse_id'],$item['item_id']);
			$persen = $this->session->userdata('r_persen');
			$dtMin=0;
			if ($dtPrice){
				foreach ($dtPrice as $dtPrice_Item){
					if($this->session->userdata('r_timur') == '1'){
						$price = number_format($dtPrice_Item['price2'],0); 
					}else{
						$price = number_format($dtPrice_Item['price'],0);
					}
					
					$pricePV=number_format($dtPrice_Item['pv'],0);
					$dtMin=$dtPrice_Item['min_order'];
					if($dtPrice_Item['pv'] > 0 and $persen > 0){
						if($dtPrice_Item['nonstcfee'] == 0) {
							$rpdiskon=number_format(round((($price - $dtPrice_Item['price_not_disc'])*$persen)/100,0),'0','','.');
                            $subrpdiskon =number_format(round(((($price - $dtPrice_Item['price_not_disc'])*$persen)/100)*$dtPrice_Item['min_order'],0),'0','','.');
						}else{
							$rpdiskon='0'; 
                            $subrpdiskon='0';
						}
					}else{
						$rpdiskon="0";
						$subrpdiskon="0";
					}
				}
			}
			*/
			
			$ttlSisa=$aSisa*str_replace(",","",$item['fsubtotal']);
			$ttlSisaPv=$aSisa*str_replace(",","",$item['fsubtotalpv']);
			echo '
			<tr>
				<td>
					<input type="hidden" id="pinjam_id'.$iRow.'" name="pinjam_id'.$iRow.'" value="'.$this->session->userdata('r_sc_id').'" readonly="1" size=10 >
					<input type="hidden" id="pin_d'.$iRow.'" name="pin_d'.$iRow.'" value="'.$item['id'].'" readonly="1" size=10 >
					<input type="text" id="itemcode'.$iRow.'" name="itemcode'.$iRow.'" value="'.$item['item_id'].'" readonly="1" size=10 >
					<input type="hidden" id="det_whsid'.$iRow.'" name="det_whsid'.$iRow.'" value="'.$item['warehouse_id'].'" readonly="1" size=10 >
					<input type="hidden" id="rpdiskon'.$iRow.'" name="rpdiskon'.$iRow.'" value="'.$rpdiskon.'" readonly="1" size=10 >
					<input type="hidden" id="subrpdiskon'.$iRow.'" name="subrpdiskon'.$iRow.'" value="'.$subrpdiskon.'" readonly="1" size=10 >
					<input class="textbold aright" type="hidden" name="min_qty'.$iRow.'" id="min_qty'.$iRow.'"  value="'.$dtMin.'" maxlength="12" size="3" tabindex="3"  >
					<span class="error">'.form_error('qty_lunas'.$iRow).'</span>
				</td>
				<td><input type="text" id="itemname'.$iRow.'" name="itemname'.$iRow.'" value="'.$item['name'].'" readonly="1" size=33 ></td>
				<td align="right"><input class="textbold aright" type="text" id="qty'.$iRow.'" name="qty'.$iRow.'" value="'.$item['fqty'].'" maxlength="12" size="3" tabindex="3" autocomplete="off" readonly="1"  ></td>
				<td align="right" style="display:none;"><input class="textbold aright" type="hidden" id="qty_retur'.$iRow.'" name="qty_retur'.$iRow.'" value="'.$item['fqty_retur'].'" maxlength="12" size="3" tabindex="3" autocomplete="off" readonly="1"  ></td>
				<td align="right" ><input class="textbold aright" type="hidden" id="qty_lunas_op'.$iRow.'" name="qty_lunas_op'.$iRow.'" value="'.$item['fqty_lunas'].'" readpnly="1" maxlength="3" size="4" tabindex="4" autocomplete="off" ></td>
				<td align="right"><input class="textbold aright" type="text" id="qty_lunas'.$iRow.'" name="qty_lunas'.$iRow.'" value="'.$this->input->post('qty_lunas'.$iRow).'" '.$lockLunas.' maxlength="3" size="5" tabindex="4" autocomplete="off" required onKeyUp=HitTotal("'.$iRow.'") ></td>
				<td align="right"><input class="textbold aright" type="text" id="qty_sisa'.$iRow.'" name="qty_sisa'.$iRow.'" value="'.$aSisa.'" size="3" tabindex="5" autocomplete="off" readonly="1" ></td>
				<td align="right"><input class="aright" type="text" id="price'.$iRow.'" name="price'.$iRow.'" value="'.$price.'" size="9" readonly="1" autocomplete="off"></td>
				<td align="right"><input class="aright" type="text" id="pv'.$iRow.'" name="pv'.$iRow.'" value="'.$pricePV.'" size="9" readonly="1" ></td>
				<td align="right"><input class="aright" type="text" id="subtotal'.$iRow.'" name="subtotal'.$iRow.'" value="'.$this->input->post('subtotal'.$iRow).'" readonly="1" size="12" ></td>
				<td align="right"><input class="aright" type="text" id="subtotalpv'.$iRow.'" name="subtotalpv'.$iRow.'" value="'.$this->input->post('subtotalpv'.$iRow).'" readonly="1" size="12" ></td>
				<!--<td>'.$strCheck.'</td>-->
			</tr>
			';
			
			/*
			echo '
			<tr>
			<td>'.$item['item_id'].'</td>
			<td>'.$item['name'].'</td>
			<td align="right">'.$item['fqty'].'</td>
			<td align="right">'.$item['fharga'].'</td>
			<td align="right">'.$item['fpv'].'</td>
			<td align="right">'.$item['fsubtotal'].'</td>
			<td align="right">'.$item['fsubtotalpv'].'</td>
			</tr>
			';
			*/
			$iRow=$iRow+1;
		}
		echo '
			<tr>
				<td colspan="8" align="right" >
					<b>Total Pembelanjaan &nbsp;</b>
				</td>
				<td><input class="textbold aright" type="text" name="total" id="total" value="0" readonly="1" size="12" ></td>
				<td><input class="textbold aright" type="text" name="totalpv" id="totalpv" value="0" readonly="1" size="12" >
					'.form_hidden('totalbv',set_value('totalbv',0)).'
					<input class="textbold aright" type="hidden" name="iRow" id="iRow" value="'.$iRow.'" readonly="1" size="12" >
				</td>
			</tr>
		';
		?>
		</table>
		<hr/>
		<div class="col-md-6">
		<table class="stripe">
		<tr>
		  <th width='80px'>Item Code</th>
		  <th >Item Name</th>
		  <th width='50px'><div align="center">Qty</div></th>
		</tr>
		<?php
		$strFreeItem='';
		echo '<h2>Free Item</h2>';
		foreach ($items_free as $var_freeitem){
			$strFreeItem=$strFreeItem.'
			<tr>
				<td>'.$var_freeitem['free_item_id'].'</td>
				<td>'.$var_freeitem['Free_Item_Name'].'</td>
				<td align="right">'.$var_freeitem['GetFree'].'</td>
			</tr>';
		}
		echo $strFreeItem;
		?>
		</table>
		</div>
		
		<div class="col-md-6">
		
		
		<table style="width:70%;float:right;">
		<tr style="display:none;">
			<td colspan=3><h2>Payment</h2></td>
		</tr>
		<tr style="display:none;>
			<td>Diskon %</td>
			<td>:</td>
			<td><input class="aright" type="text" name="persen" id="persen" readonly="readonly" value="<?php echo $this->session->userdata(r_persen) ?>" maxlength="2"/></td>
		</tr style="display:none;>
		<tr>
			<td>Jumlah Diskon</td>
			<td>:</td>
			<td><input class="aright" type="text" name="totalrpdiskon" id="totalrpdiskon" readonly="1" value="<?php echo $diskonRp ?>" ></td>
		</tr>
		<tr style="display:none;">
			<td>Jumlah Yang harus di Bayar</td>
			<td>:</td>
			<td><input class="textbold aright" type="text" name="totalbayar" id="totalbayar" value="<?php echo $this->input->post('totalbayar');?>"  readonly="1"/></td>
		</tr>
		<tr style="display:none;>
			<td>Total Pembayaran</td>
			<td>:</td>
			<td><input class="textbold aright" type="text" name="sisabayar" id="sisabayar" value="<?php echo $this->input->post('sisabayar');?>"  readonly="1"/></td>
		</tr>
		<tr>
			<td colspan=2>
				<p class="error">
					<?php echo $pray_msg ?>
					<?php if(validation_errors() or form_error('sisabayar')){echo form_error('sisabayar');} ?>
					<div id="msgValidation" class="error" >Isikan Jumlah qty retur pada isian yang telah di sediakan</div>
				</p>
			</td>
			
			<td>
				<input class="button" type="submit" id="cmSubmit" name="cmSubmit" value="Submit">
				<!--<button class="btn" id="cmSubmit" name="cmSubmit" >Submit</button>-->
			</td>
			
		</tr>
		</table>
		
		</div>
		<div id="clearer"></div>
		
		<table width="100%" >
		<tr><td colspan="8"><hr/></td></tr>
		<tr>
			<td colspan="2"><h4>Pick up / Delivery</h4></td>
			<td valign="top" colspan='3' align='left'>&nbsp;</td>
			<td colspan="2"><b></b></td>
		</tr>
		<tr>
			<td valign="top">Option</td>
			<td valign="top"><?php $pu = $this->session->userdata('r_pu'); if($pu == '1')echo ": Delivery"; else echo ": Pick Up";?></td>
            <td valign="top" colspan='4' align='right'></td>
			<td valign="top" colspan='2'></td>
		</tr>
		<tr>
			<td valign="top"><?php if($pu == '1')echo "City of delivery ";?></td>
			<td vaslign="top"><?php
				//START ASP 20180410
				echo form_hidden('sc_id', set_value('sc_id',$this->session->userdata('r_sc_id')));
				echo form_hidden('whsid', set_value('whsid',$this->session->userdata('r_whsid')));
				
				echo form_hidden('pic_name', set_value('pic_name',$this->session->userdata('r_pic_name')));
				echo form_hidden('pic_hp', set_value('pic_hp',$this->session->userdata('r_pic_hp')));
				echo form_hidden('kecamatan', set_value('kecamatan',$this->session->userdata('r_kecamatan')));
				echo form_hidden('kelurahan', set_value('kelurahan',$this->session->userdata('r_kelurahan')));
				echo form_hidden('kodepos', set_value('kodepos',$this->session->userdata('r_kodepos')));
				//EOF ASP 20180410
				echo form_hidden('kota_id', set_value('kota_id',$this->session->userdata('r_kota_id')));
				echo form_hidden('propinsi', set_value('propinsi',$this->session->userdata('r_propinsi')));
				echo form_hidden('timur', set_value('timur',$this->session->userdata('r_timur')));
				echo form_hidden('deli_ad', set_value('deli_ad',$this->session->userdata('r_deli_ad')));
				
				echo form_hidden('addr1', set_value('addr1',$this->session->userdata('r_addr1')));
				echo form_hidden('kota_id1', set_value('kota_id1',$this->session->userdata('r_kota_id1')));
				
				//START ASP 20180917
				echo form_hidden('pic_name1', set_value('pic_name1',$this->session->userdata('r_pic_name1')));
				echo form_hidden('pic_hp1', set_value('pic_hp1',$this->session->userdata('r_pic_hp1')));
				echo form_hidden('kecamatan1', set_value('kecamatan1',$this->session->userdata('r_kecamatan1')));
				echo form_hidden('kelurahan1', set_value('kelurahan1',$this->session->userdata('r_kelurahan1')));
				echo form_hidden('kodepos1', set_value('kodepos1',$this->session->userdata('r_kodepos1')));
				//EOF ASP 20180917
				
				
				
				echo form_hidden('city', set_value('city',$this->session->userdata('r_city'))); echo ": ".$this->session->userdata('r_city')." - ".$this->session->userdata('r_propinsi');
				echo form_hidden('pu', set_value('pu',$this->session->userdata('r_pu')));
				?>
				</td>
			<td valign="top" colspan='4' align='right'></td>
			<td colspan='2' valign='top'></td>
		</tr>
		<tr>
			<td valign='top'><?php echo "Delivery Address ";?></td>
			<td valign='top'colspan='4'><?php echo form_hidden('addr', set_value('addr',$this->session->userdata('r_addr1'))); echo ": ".$this->session->userdata('r_addr1');?>
            </td>
			<td valign="top" colspan='1' align='right'></td>
			<td colspan='2' valign='top'></td>
		</tr>
		<tr>
			<td valign='top'><?php echo "Kelurahan ";?></td>
			<td valign='top'colspan='5'><?php echo ": ".$this->session->userdata('r_kelurahan1');?>
            </td>
			<td colspan="2"><?php //$this->load->view('submit_confirm');?></td>
		</tr>
		<tr>
			<td valign='top'><?php echo "Kecamatan ";?></td>
			<td valign='top'colspan='6'><?php echo ": ".$this->session->userdata('r_kecamatan1');?>
            </td>
		</tr>
		<tr>
			<td valign='top'><?php if($pu == '1')echo "Kodepos ";?></td>
			<td valign='top'colspan='6'><?php echo ": ".$this->session->userdata('r_kodepos1');?>
            </td>
		</tr>
		<tr>
			<td valign='top'><?php echo "Penerima ";?></td>
			<td valign='top'colspan='6'><?php echo ": ".$this->session->userdata('r_pic_name');?>
            </td>
		</tr>
		<tr>
			<td valign='top'><?php echo "HP Penerima ";?></td>
			<td valign='top'colspan='6'><?php echo ": ".$this->session->userdata('r_pic_hp');?>
            </td>
		</tr>
		<tr>
			<td colspan='6'>&nbsp;</td>
			<td colspan="2"><?php //$this->load->view('submit_confirm');?></td>
		</tr>		
	</table>
<?php echo form_close();?>

<?php $this->load->view('footer');?>


<script type="text/javascript" src="<?php echo site_url() ?>js/jquery.js"></script>
<script type="text/javascript">

function pray_cdec(var_str) {
	var xStr="0";
	xStr=var_str.replace(/[,.]/g, function (m) {
		return m === ',' ? '' : '';
	});
	return xStr
}

function DelVoucher(varId){
	var rCount=document.getElementById("counti").value;
	xVar=Number(rCount)-1;
	document.getElementById("counti").value=xVar;
	document.getElementById("pray_row"+varId).remove();
	GranTotal();
}

function HitTotal(iCount){
	
	$("#cmdSimpan").hide();
	var iqty =Number(document.getElementById("qty"+iCount).value.replace(/[^0-9.-]+/g,""));
	var min_qty=Number(document.getElementById("min_qty"+iCount).value.replace(/[^0-9.-]+/g,""));
	var qty_lunas=Number(document.getElementById("qty_lunas"+iCount).value.replace(/[^0-9.-]+/g,""));
	var qty_sisa=Number(document.getElementById("qty_sisa"+iCount).value.replace(/[^0-9.-]+/g,""));
	var qty_lunas_op=Number(document.getElementById("qty_lunas_op"+iCount).value.replace(/[^0-9.-]+/g,""));
	var qty_retur=Number(document.getElementById("qty_retur"+iCount).value.replace(/[^0-9.-]+/g,""));
	var sisa_total = ((iqty-qty_retur)-qty_lunas_op) - qty_lunas;
	var sisa = qty_lunas % min_qty;
	
	if (qty_lunas<0){
		alert("Qty Harus di isi atau tidak boleh minus");
		document.getElementById("qty_lunas"+iCount).value="0";
	}else{
		if (sisa>0){
			alert("Qty Harus berupa kelipatan "+min_qty);
			$("#cmdSimpan").hide();
			document.getElementById("qty_lunas"+iCount).value="0";
		}else if (qty_lunas>((iqty-qty_retur)-qty_lunas_op)){
			alert("Stock Tidak Mencukupi");
			document.getElementById("qty_lunas"+iCount).value="0";
			$("#cmdSimpan").show();
		}else{
			var iX=pray_cdec(document.getElementById("pv"+iCount).value);
			var xPric=Number(qty_lunas)*Number(document.getElementById("price"+iCount).value.replace(/[^0-9.-]+/g,""));
			var xVar=Number(qty_lunas)*Number(iX);
			
			document.getElementById("qty_sisa"+iCount).value=sisa_total;
			document.getElementById("subtotal"+iCount).value=formatCurrency(xPric);
			document.getElementById("subtotalpv"+iCount).value=formatCurrency(xVar);
			document.getElementById("cmSubmit").style.display = 'block';
			
			$("#cmdSimpan").show();
		}
		
		
	}
	
	GranTotal();
}

function GranTotal(){

	var iTotal=Number(document.getElementById("iRow").value);

	var sisaTotal=0;
	var sisaTotalPv=0;
	
	var sisaTotal=0;
	var sisaTotalPv=0;
	var chQty=0;
	for (i = 0; i < iTotal; i++) {
		var iPv=pray_cdec(document.getElementById("subtotalpv"+i).value);
		var iTl=pray_cdec(document.getElementById("subtotal"+i).value);
		var iQty=pray_cdec(document.getElementById("qty_lunas"+i).value);
		chQty=chQty+Number(iQty);
		sisaTotal=sisaTotal+Number(iTl);
		sisaTotalPv=sisaTotalPv+Number(iPv);
	}
	document.getElementById("total").value=formatCurrency(sisaTotal);
	document.getElementById("totalpv").value=formatCurrency(sisaTotalPv);
	
	var iByr=pray_cdec(document.getElementById("total").value);
	var totalBayar=iByr-Number(iDiskon)-Number(vTotal);
	document.getElementById("totalbayar").value=formatCurrency(totalBayar);
	document.getElementById("sisabayar").value=formatCurrency(totalBayar);
	
	if (Number(chQty)<0){
		document.getElementById("msgValidation").innerHTML="Maaf Jumlah Retur Anda Minus";
		document.getElementById("msgValidation").style.display = 'block';
		document.getElementById("cmSubmit").style.display = 'none';
	}
	
	
}
	
GranTotal();




</script>