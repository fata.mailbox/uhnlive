<script type="text/javascript" src="<?php echo base_url();?>js/order.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/backend.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/popup.css" />

<table width="100%" border="0" cellpadding="5" cellspacing="5">
<tr><td>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr><td class='bg_border' align='center'><?php echo $page_title ?></td></tr>
		<tr><td class='td_border2'>
			
			<?php echo form_open('search/scsearch/item_sc_whs/'.$this->uri->segment(4).'/'.$this->uri->segment(5).'/'.$this->uri->segment(6).'/'.$this->uri->segment(7).'/'.$this->uri->segment(8), array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width='10%'>Search </td>
				<td width='1%'>:</td>
    			<td width='89%'><?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','search');?></td>
			</tr>
			<?php if($this->session->userdata('keywords_ws')){ ?>
				<tr><td colspan='2'>&nbsp;</td>
				<td>Your search keywords : <b><?php echo $this->session->userdata('keywords_ws');?></b></td>
				</tr>
			<?php }?>
			
			</table>	
			<?php 
			//$data['results'] = $this->MSc->search_stock_whs(whs,keyword,flag,$num,$offset);
			/*
			echo 'Variabel : WHS Id='.$this->uri->segment(4).'<br>'; 
			echo 'Keyword Id='.$this->uri->segment(5).'<br>'; 
			echo 'Flag Id='.$this->uri->segment(6).'<br>'; 
			echo 'Num Id='.$this->uri->segment(7).'<br>'; 
			echo 'Offset Id='.$this->uri->segment(9).'<br>'; 
			*/
			?>
			<?php echo form_close();?>
				<?php if($results) {?>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">				
				<tr class='title_table'>
					<td class='td_report' width='50px'>No.</td>
					<td class='td_report' width='15%'>Item Code</td>
					<td class='td_report' width='45%'>Item Name</td>
					<td class='td_report' width='20%'>Price</td>
					<td class='td_report' width='10%'>PV</td>
					<td class='td_report' width='10%'>Qty</td>
					<td class='td_report' width='200'>Warehouse</td>
				</tr>
				
				<?php 
				$counter = $from_rows; 
				$persen = $this->session->userdata('r_persen');
				foreach($results as $row) { 
					//Berdasarkan Id Kota
					//if($this->uri->segment(5) == '1' and $this->uri->segment(4) == '1')$price = $row['price2']; else $price = $row['price'];
					if($this->uri->segment(5) == '1')$price = $row['price2']; else $price = $row['price'];
					$counter = $counter + 1; 
					//'.$this->uri->segment(7).'
					echo '
					<tr height="22" class="lvtColData" onmouseover=this.className="lvtColDataHover" onmouseout=this.className="lvtColData"
					onClick=showDataPop("'.$counter.'"); >
						<td class="td_report"> 
							<div style="display:none;">'.$this->uri->segment(7).'
								
								<div id="p6'.$counter.'" >'.$row["min_order"].'</div>
								<div id="p7'.$counter.'" >'.$row["whsid"].'</div>
								<div id="p8'.$counter.'" >'.number_format($price*$row['min_order'],'0','','.').'</div>
								<div id="p9'.$counter.'" >'.number_format($row['pv'] * $row['min_order'],'0','','.').'</div>
							</div>
							'.$counter.'
						</td>
						<td id="p0'.$counter.'" class="td_report">'.$row["item_id"].'</td>
						<td id="p1'.$counter.'" class="td_report">'.$row["name"].'</td>
						<td id="p2'.$counter.'" class="td_report">'.number_format($price).'</td>
						<td id="p3'.$counter.'" class="td_report">'.number_format($row["pv"],"0","",".").'</td>
						<td id="p5'.$counter.'" class="td_report">'.$row["qty"].'</td>
						<td id="p4'.$counter.'" class="td_report">'.$row["stock_wh"].'</td>
					</tr>';
				}?>
				<tr><td colspan='5'><?php echo $this->pagination->create_links(); ?></td></tr>
				</table
				><?php }?>
		</td></tr>
</table>
		
</td></tr>
</table>

<script type="text/javascript">

window.onbeforeunload = function (e) {
	opener.GranTotal();
};

function showDataPop(varId){

	var item_id=document.getElementById("p0"+varId).innerHTML;
	var item_name=document.getElementById("p1"+varId).innerHTML;
	var price=document.getElementById("p2"+varId).innerHTML;
	var pv=document.getElementById("p3"+varId).innerHTML;
	var stock_wh=document.getElementById("p4"+varId).innerHTML;
	var qty=document.getElementById("p5"+varId).innerHTML;
	var min_order=document.getElementById("p6"+varId).innerHTML;
	var warehouse_id=document.getElementById("p7"+varId).innerHTML;
	var subtotal=document.getElementById("p8"+varId).innerHTML;
	var subtotalpv=document.getElementById("p9"+varId).innerHTML;
	
	window.opener.document.getElementById("itemcode<?php echo $this->uri->segment(7);?>").value=item_id;
	window.opener.document.getElementById("det_whsid<?php echo $this->uri->segment(7);?>").value=warehouse_id;
	window.opener.document.getElementById("itemname<?php echo $this->uri->segment(7);?>").value=item_name;
	window.opener.document.getElementById("price<?php echo $this->uri->segment(7);?>").value=price;
	window.opener.document.getElementById("pv<?php echo $this->uri->segment(7);?>").value=pv;
	window.opener.document.getElementById("qty<?php echo $this->uri->segment(7);?>").value=min_order;
	window.opener.document.getElementById("min_qty<?php echo $this->uri->segment(7);?>").value=min_order;
	window.opener.document.getElementById("subtotal<?php echo $this->uri->segment(7);?>").value=subtotal;
	window.opener.document.getElementById("subtotalpv<?php echo $this->uri->segment(7);?>").value=subtotalpv;
	window.opener.document.getElementById("qty_avai<?php echo $this->uri->segment(7);?>").value=qty;
	
	window.close();
}

</script>


<!--
<tr height='22' class='lvtColData' onmouseover='this.className="lvtColDataHover"' onmouseout='this.className="lvtColData"' 
 onclick="window.opener.document.form.itemcode<?php echo $this->uri->segment(5);?>.value ='<?php echo $row['item_id'];?>';
  window.opener.document.form.itemname<?php echo $this->uri->segment(5);?>.value ='<?php echo $row['name'];?>';
  window.opener.document.form.price<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($price,'0','','.');?>';
  window.opener.document.form.pv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['pv'],'0','','.');?>';
  window.opener.document.form.bv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['bv'],'0','','.');?>';
  window.opener.document.form.qty<?php echo $this->uri->segment(5);?>.value ='<?php echo $row['min_order'];?>';
  window.opener.document.form.subtotal<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($price*$row['min_order'],'0','','.');?>';
  window.opener.document.form.total.value=totalAfterVoucher(total_curr(<?=$this->session->userdata('counti');?>,'window.opener.document.form.subtotal'),vtotal_curr(<?=$this->session->userdata('countv');?>,'window.opener.document.form.vsubtotal'));
  window.opener.document.form.subtotalpv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['pv']*$row['min_order'],'0','','.');?>';
  window.opener.document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?=$this->session->userdata('counti');?>,'window.opener.document.form.subtotalpv'),vtotalpv_curr(<?=$this->session->userdata('countv');?>,'window.opener.document.form.vsubtotalpv'));
  window.opener.document.form.subtotalbv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['bv']*$row['min_order'],'0','','.');?>';
  window.opener.document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?=$this->session->userdata('counti');?>,'window.opener.document.form.subtotalbv'),vtotalbv_curr(<?=$this->session->userdata('countv');?>,'window.opener.document.form.vsubtotalbv'));
  <?php
  $persen = $this->session->userdata('r_persen');
	if($row['pv'] > 0 and $persen > 0){ 
		if($row['nonstcfee'] == 0) {
		?> 
			window.opener.document.form.rpdiskon<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format(round((($price - $row['price_not_disc'])*$persen)/100,0),'0','','.');?>';
			window.opener.document.form.subrpdiskon<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format(round(((($price - $row['price_not_disc'])*$persen)/100)*$row['min_order'],0),'0','','.');?>';
		<?php }else{ ?>
			window.opener.document.form.rpdiskon<?php echo $this->uri->segment(5);?>.value ='0'; 
			window.opener.document.form.subrpdiskon<?php echo $this->uri->segment(5);?>.value ='0';
		<?php } ?>
  <?php }else{ ?> 
		window.opener.document.form.rpdiskon<?php echo $this->uri->segment(5);?>.value ='0'; 
		window.opener.document.form.subrpdiskon<?php echo $this->uri->segment(5);?>.value ='0';
	<?php }?>
	window.opener.document.form.totalrpdiskon.value=totaldiskon_curr(<?=$this->session->userdata('counti');?>,'window.opener.document.form.subrpdiskon');
	totalbayardiskon(window.opener.document.form.total, window.opener.document.form.totalrpdiskon, window.opener.document.form.totalbayar);
  window.close();">
	<td class='td_report'><?php echo $counter;?></td>
	<td class='td_report'><?php echo $row['item_id'];?></td>
	<td class='td_report'><?php echo $row['name'];?></td>
	<td class='td_report'><?php echo number_format($price,'0','',',');?></td>
	<td class='td_report'><?php echo $row['fpv'];?></td>
	<td class='td_report'><?php echo $row['fqty'];?></td>
</tr>
-->