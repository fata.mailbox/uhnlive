<?php 
	/* ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- */
	/* Begin Part 1 Template */ // Created by Boby 20130625
	$this->load->view('header');
	echo "<h2>".$page_title."</h2>";
	echo anchor('auth/regnik/create','Register Employee');
	if($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>
	<table width="99%">
	<?php echo form_open($base_url, array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search by: 
			<?php 
				$data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    			echo form_input($data);?> <?php echo form_submit('submit','go');
				if($this->session->userdata('keywords')){echo "<br/>Your search keywords : <b>".$this->session->userdata('keywords')."</b>";}
			?>
    	</td>
	</tr>
	<?php echo form_close();?>				
	</table>
<?php
	/* End Part 1 Template */
	
	/* ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- */
	/* Content (edit for header field) */
?>
<table class="stripe">
	<?php /* Header of table */ ?>
	<tr><?php $field = 0; $tp=0;?>
		<?php $p=5;?>	<th width="<?=$p?>%">No.</th><?php $field+=1;$tp+=$p;?>
		<?php $p=25;?>	<th width='<?=$p?>%'>Employee ID / Name</th><?php $field+=1;$tp+=$p;?>
		<?php $p=25;?>	<th width='<?=$p?>%'>Member ID / Name</th><?php $field+=1;$tp+=$p;?>
		<?php $p=20;?>	<th width='<?=$p?>%'>Status</th><?php $field+=1;$tp+=$p;?>
		<?php $p=25;?>	<th width='<?=$p?>%'>User ID</th><?php $field+=1;$tp+=$p;?>
		<?php if($tp<>100){$tp=100-$tp; echo "Miss the percentage field width..".$tp."%";}?>
	</tr>
	<?php /* End header of table */ ?>
	
<?php
	/* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== */
	/* Looping data table */
if (isset($results)):
	$counter = $from_rows;
	foreach($results as $key => $row):
		$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor("/auth/regnik/create/".$row['id'],$counter);?></td>
      <td><?php echo anchor("/auth/regnik/create/".$row['id'],$row['emp_id']." / ".$row['nama']);?></td>
      <td><?php echo anchor("/auth/regnik/create/".$row['id'],$row['mid']." / ".$row['nama_member']." ");?></td>
      <td><?php echo anchor("/auth/regnik/create/".$row['id'],$row['note']." ");?></td>
      <td><?php echo anchor("/auth/regnik/create/".$row['id'],$row['updatedby']);?></td>
    </tr>
<?php 
	endforeach;
	/* End looping data table*/
	/* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== */
	else:
 ?>
    <tr>
      <td colspan="<?php echo $field;?>">Data is not available.</td>
    </tr>
<?php endif; ?>
</table>
<?php /* End Content */?>

<?php $this->load->view('footer');?>