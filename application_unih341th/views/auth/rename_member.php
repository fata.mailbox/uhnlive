<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: bhowbee
    	contact person	: Boby
        Handphone	: +62 818 0856 8111
    	Telphone 	: +62 21 995 1 888 2 
    	Yahoo Messenger	: gold_phoenix99
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php
	if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}
?>
<?php echo form_open('auth/rplacenm', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		
		<table width='100%'>
		<tr>
			<td valign='top'>Member ID</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php 
					$data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
				    echo form_input($data);?> <?php $atts = array(
						'width'      => '450',
						'height'     => '600',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'yes',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					echo anchor_popup('memsearch', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
				?>				
				<span class='error'>*<?php echo form_error('member_id');?></span>
			</td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="0" value="<?php echo set_value('name');?>" size="30" onchange="document.form.name1.value = document.form.name.value;"/></td>
		</tr>
		<tr>
			<td valign='top'>Replace Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name1" id="name1" value="<?php echo set_value('name1');?>" size="30"/><span class='error'>*<?php echo form_error('name1');?></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td><?php echo form_submit('submit', 'Submit');?></td> 
		</tr>
		</table>
	<?php echo form_close();?>
<?php $this->load->view('footer');?>
