<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
		<?php echo form_open('auth/user/create', array('id' => 'form'));?>
		<table width="99%">
		<tr>
			<td width='14%' valign='top'>Username</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php $data = array('name'=>'username','id'=>'username','value'=>set_value('username'));
 	   		echo form_input($data);?> <span class='error'>*<?php echo form_error('username'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Nama</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'name','id'=>'name','value'=>set_value('name'));
    			echo form_input($data);?> <span class='error'>*<?php echo form_error('name'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>e-Mail</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'email','id'=>'email','value'=>set_value('email'));
    			echo form_input($data);?> <span class='error'><?php echo form_error('email_error'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Group User</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('group_id',$group,$this->validation->group_id);?></td> 
		</tr>
		<tr>
			<td valign='top'>Gudang</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('warehouse_id',$warehouse,$this->validation->warehouse_id);?></td> 
		</tr>
		<tr>
			<td valign='top'>Status</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('status',array('active'=>'active','inactive'=>'inactive'),set_value('status'));?></td> 
		</tr>
		<tr>
			<td valign='top'>Password</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'passwd','id'=>'passwd','value'=>set_value('passwd'));
    			echo form_password($data);?> <span class='error'><?php echo form_error('passwd_error'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Ulangi Password</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'passwd2','id'=>'passwd2','value'=>set_value('passwd2'));
    			echo form_password($data);?></td> 
		</tr>
		<tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><?php echo form_submit('submit','Submit');?></td> 
		</tr>
		</table>
<?php echo form_close();?>
		
<?php
$this->load->view('footer');
?>
