<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<?php if($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
echo form_open('auth/cancelso/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	    
    <table width='100%'>
		<tr>
			<td width="20%">Search by so id</td>
			<td width="1%">:</td>
			<td width="79%"><?php 
				if($row['id'] > 1){
					$data = array('name'=>'so_id','id'=>'so_id','size'=>12, 'readonly'=>1,'value'=>set_value('so_id'));
				}else{
					$data = array('name'=>'so_id','id'=>'so_id','size'=>12,'value'=>set_value('so_id'));
				}
				echo form_input($data);
				echo form_submit('submit','go');?>
			</td>
		</tr>
		<?php if($row['id'] > 1){ ?>
		<tr>
			<td>Note</td>
			<td>:</td>
			<td>Available to cancel.</td> 
		</tr>
		<tr>
			<td>Remark</td>
			<td>:</td>
			<td>
            <?php
			$dataremark = array('name'=>'remark','id'=>'remark','value'=>set_value('remark'), 'rows'=> '5', 'cols' => '70');
            echo form_textarea($dataremark);
			?>
            </td> 
		</tr>
		<?php } ?>
    </table>
	<?php if($row['id'] > 1){ ?>
	<table width='80%'>	
		<tr>
			<td colspan='4'><?php echo form_submit('submit', 'Submit', 'tabindex="23"');?></td>
		</tr>
	</table>
	<?php } ?>
<?php echo form_close();?>