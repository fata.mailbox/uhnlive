<!-- Annisa Rahmawaty -->

<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<?php echo form_open('auth/reset/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		
	<table width='100%'>
	    <!--
		<tr>
			<td valign="top" width="20%">Pilih Kategori</td>
			<td valign="top">:</td>
			<td><select name="idkategori" id="idkategori">
				<option value="1">Member</option>
				<option value="2">Stockies</option>
			</select></td>
		</tr>
		-->
		<tr>
			<td valign='top'>Member ID</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $data = array('name'=>'member_id','id'=>'member_id','size'=>'8','readonly'=>'1','value'=>set_value('member_id')); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('search/search_member/member', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<!--<input class="button" type="button" name="Button" value="browse" onclick="handleClick()">-->
					 <span class='error'>*<?php echo form_error('member_id');?></span></td>
					
		</tr>
		<tr>
			<td valign='top'>Member</td>
			<td valign='top'>:</td>
			<td><input type="text" name="member" id="member" value="<?php echo set_value('member');?>" readonly="1" size="30" /></td>
		</tr>
        
		<tr>
			<td>Remark</td>
			<td>:</td>
			<td><textarea name="remark"></textarea></td>
		</tr>
		<tr>
			<td valign='top'>Pin</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'pin','id'=>'pin','value'=>set_value('pin'));
    			echo form_password($data);?> <span class='error'><?php echo form_error('pin_error'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Ulangi Pin</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'pin2','id'=>'pin2','value'=>set_value('pin2'));
    			echo form_password($data);?></td> 
		</tr>
		<tr>
			<td valign='top'>Password</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'passwd','id'=>'passwd','value'=>set_value('passwd'));
    			echo form_password($data);?> <span class='error'><?php echo form_error('passwd_error'); ?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Ulangi Password</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'passwd2','id'=>'passwd2','value'=>set_value('passwd2'));
    			echo form_password($data);?></td> 
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td><?php echo form_submit('submit', 'Submit');?></td> 
		</tr>
		</table>
	<?php echo form_close();?>
<?php $this->load->view('footer');?>

<script type="text/javascript">
	
	function handleClick() {
	    var dropDown = document.getElementById('idkategori');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    if (dropValue == 1) {
	    	window.open('http://localhost/sohomlm_prod/search/search_member/member','mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	    }else{
	    	window.open('http://localhost/sohomlm_prod/search/search_member/stockiest','mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	    }
	    
	}
</script>
