<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>UNI-HEALTH.COM : <?php echo $page_title;?></title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="author" content="takwa"/>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="robots" content="noindex, nofollow" />
<meta name="rating" content="general" />
<meta name="copyright" content="Copyright (c) 2009-<?php echo date("Y");?> | www.smartindo-technology.com | contact person: Takwa Handphone: +62 817 906 1982 | Telphone: +6221 5435 5600" />
<noscript>Javascript is not enabled! Please turn on Javascript to use this site.</noscript>


<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    contact person	: Takwa
    Handphone		: +62 817 906 1982
                  	  +62 812 9396 1982 
    Yahoo Messenger	: qtakwa
    Skype ID		: smartindo
-->

<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel();
    jQuery('#coba').jcarousel();
    jQuery('#center').jcarousel();
    jQuery('#right').jcarousel();
});


</script>

<script type="text/javascript">
//<![CDATA[
base_url = '<?php echo  base_url();?>';
//]]>
</script>

<link href="<?php echo base_url();?>booklet/jquery.booklet.latest.css" type="text/css" rel="stylesheet" media="screen, projection, tv" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script> window.jQuery || document.write('<script src="<?php echo base_url();?>booklet/jquery-2.1.0.min.js"><\/script>') </script>


<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>booklet/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url();?>booklet/jquery.booklet.latest.js"></script>



<script type="text/javascript" src="<?php echo base_url();?>js/backend.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/simpletreemenu.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url();?>js/prototype.js"></script> -->


<?php if($this->uri->segment(1) == 'main'){?>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>css/animasi_backend.css" />	<!-- Animasi-header -->
<script type="text/javascript" src="<?=base_url();?>js/jquery.min.js"></script><!-- Animasi-header -->
<script src="<?=base_url();?>js/script_animasi.js" 			type="text/javascript"></script>	<!-- Animasi-header -->
<script src="<?=base_url();?>js/jquery_002.js"  			type="text/javascript"></script>	<!-- Animasi-header -->
<?php }?>

<link rel="shortcut icon" href="<?php echo base_url();?>images/favicon.ico" type="image/ico" />

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/backend.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/video.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/simpletree.css" />	
<link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url();?>css/print.css" />

<script type="text/javascript" src="<?=base_url();?>js/video.js"></script>

<?php
	if (isset($extraHeadContent)) {
		echo $extraHeadContent;
	}
?>
<?php if($this->uri->segment(1) == 'main'){?>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/highslide-gallery_backend.css" />
<script src="<?=base_url();?>js/highslide-with-gallery.js" 	type="text/javascript"></script> 	<!--to img zom-->
    <!--to img zom-->
    <script type="text/javascript">
        hs.graphicsDir ='<?=base_url();?>images/graphics/';
        hs.align = 'center';
        hs.transitions = ['expand', 'crossfade'];
        hs.outlineType = 'rounded-white';
        hs.fadeInOut = true;
        hs.dimmingOpacity = 0.8;
        //hs.dimmingOpacity = 0.75;
        
        // Add the controlbar
        hs.addSlideshow({
            //slideshowGroup: 'group1',
            interval: 5000,
            repeat: false,
            useControls: true,
            fixedControls: 'fit',
            overlayOptions: {
                opacity: 0.75,
                position: 'bottom center',
                hideOnMouseOut: true
            }
        });
    </script>
	<?php }?>
    

    <script type="text/javascript">
		$(function() {
	    //single book
	    // $('#mybook').booklet();
	    if ($(document).width() <= 1080) {
	    	//alert('mobile');
	    	$('#mybook').booklet({
		        closed: true,
		        autoCenter: true,
		        width:  950 * 0.8,
	       		height: 430 * 0.8,
	       		tabs:  true,
		        tabWidth:  180,
		        tabHeight:  20
		    });
	    }

	    if ($(document).width() > 1080) {
	    	//alert('komp');
	    	$('#mybook').booklet({
		        closed: true,
		        autoCenter: true,
		        width:  1050 ,
	       		height: 430,
	       		tabs:  true,
		        tabWidth:  180,
		        tabHeight:  20
		    });
	    }
	});
	    
	</script>
</head>
<body>
<div id="allHolder">
	<div id="container">
		<div id="masthead">
			<?php if ($this->session->userdata('logged_in')): ?>
            
			<ul id="submenu">
				<li><a href="<?php echo site_url('logout')?>" class="submenu_link logout">logout</a></li>
			</ul>
			
			<ul id="submenu">
				<li class="submenu_userid userid"><?php if($this->session->userdata('group_id')>100){ echo $this->session->userdata('username');} else { echo $this->session->userdata('user');} echo " / ".$this->session->userdata('name')?></li>
			</ul>
            <?php else: ?>
            <ul id="submenu">
				<li class="submenu_userid userid">Guest</li>
			</ul>
            <?php endif;?>
            
            <ul id="submenu">
				<li><a href="<?php echo site_url('')?>" class="submenu_link frontend">go to front end</a></li>
			</ul>
		</div>
			
		<div id="action_menu">
        
        <?php if ($this->session->userdata('logged_in')): ?>
				<?php $menu = $this->MMenu->getMenuGroup($this->session->userdata('group_id'));
				if(isset($menu)){ ?>
                <a href="javascript:ddtreemenu.flatten('treemenu', 'expand')"><b>Open</b></a><a href="#"> | </a><a href="javascript:ddtreemenu.flatten('treemenu', 'contact')" class="link_leftmenu"><b>Close</b></a><br><br>
                
					<ul id="treemenu" class="treeview">
                    
                	<?php foreach($menu as $key =>$row){ ;?>
							<li><?php echo $row['title'];?><ul>
                            <?php
									$menu2 = $this->MMenu->getSubMenu($this->session->userdata('group_id'),$row['id']);
		 							foreach($menu2 as $rowx){ 
										if(isset($rowx['title'])){ 
											if($this->session->userdata('username')=='00000005'){
												if($rowx['url']!='netgen' || $rowx['url']!='activity' || $rowx['url']!='treelist'){
												?>
													<li class="menu"><?php echo anchor($rowx['folder']."/".$rowx['url'],$rowx['title']); ?></li>	
											<?php 
												}
											}else{
										?>
											<li class="menu"><?php echo anchor($rowx['folder']."/".$rowx['url'],$rowx['title']); ?></li>	
									<?php 
											}
									}}?></ul></li>
                                    
                            
                <?php }?>
					</ul>
                    <script type="text/javascript">
						//ddtreemenu.createTree(treeid, enablepersist, opt_persist_in_days (default is 1))
						ddtreemenu.createTree("treemenu", true)
					</script>

					<?php echo anchor('','Print', array('class'=>'print','onclick' => 'print(); return false;'));?>
                    
                    <?php }?>
				<?php else: ?>
					<!-- <li class="menu_promo">Simple,<br>Beautiful,<br>Open Source,<br>Online System</li> -->
			<?php endif; ?>
		</div>

		<div id="main_content">