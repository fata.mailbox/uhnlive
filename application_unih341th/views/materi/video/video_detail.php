<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<br>
	<table width="99%">
			<tr>
				<td>Title :</td>
				<td><input type="text" name="video-title" value="<?=$video_detail['dataObject']->title?>" disabled></td>
			</tr>
			<tr>
				<td>Description :</td>
				<td>
					<textarea name="video-description" rows="10" cols="40" disabled>
						<?=$video_detail['dataObject']->description?>
					</textarea>
				</td>
			</tr>
			<tr>
				<td>Youtube Link :</td>
				<td><input type="text" name="video-link" value="<?=$video_detail['dataObject']->youtube_link?>" disabled></td>
			</tr>
			<tr>
				<td>Category :</td>
				<td>
					<select name="video-category" disabled>
                        <option value="<?=$video_detail['dataObject']->cat_id?>"><?=$video_detail['dataObject']->name?></option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Thumbnail :</td>
				<td><img src="<?=base_url()?>images/video_thumbnail/<?=$video_detail['dataObject']->thumbnail?>" width="150" height="100"></td>
			</tr>
			<tr>
				<td>Sort :</td>
				<td><input type="text" name="sort" value="<?=$video_detail['dataObject']->sort_video?>" disabled></td>
			</tr>
			<tr>
				<td>Feature :</td>
				<td><input type="checkbox" name="feature" <?php if($video_detail['dataObject']->feature_video == 1) echo "checked";?> disabled>Featured</td>
			</tr>
			<tr>
				<td>Publish :</td>
				<td>
					<select name="publish" disabled>
                        <option value="1" <?php if($video_detail['dataObject']->publish == 1) echo "selected";?>>Publish</option>
						<option value="0" <?php if($video_detail['dataObject']->publish == 0) echo "selected";?>>Unpublish</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Download Link :</td>
				<td><input type="text" name="new-download-link" value="<?=$video_detail['dataObject']->download_link?>" disabled></td>
			</tr>
			<tr>
				<td>
					<br>
					<?php 
						echo anchor('materi/pengaturan_video','Back'); // update by pincul 20171412
					?>
				</td>
			</tr>
	</table>

<?php 
	$this->load->view('footer');
?>