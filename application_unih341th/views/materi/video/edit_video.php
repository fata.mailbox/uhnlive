<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<br>

<?php 
	echo anchor('materi/pengaturan_video','Back'); // update by pincul 20171412
	echo form_open_multipart('materi/pengaturan_video/process_edit_video', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));

?>
	<table width="99%">
		<input type="hidden" name="id" value="<?=$video_detail['dataObject']->video_id?>">
			<tr>
				<td>Title :</td>
				<td><input type="text" name="new-video-title" value="<?=$video_detail['dataObject']->title?>"></td>
			</tr>
			<tr>
				<td>Description :</td>
				<td>
					<textarea name="new-video-description" rows="10" cols="40"><?=$video_detail['dataObject']->description?></textarea>
				</td>
			</tr>
			<tr>
				<td>Youtube Link :</td>
				<td><input type="text" name="new-video-link" value="<?=$video_detail['dataObject']->youtube_link?>"></td>
			</tr>
			<tr>
				<td>Category :</td>
				<td>
					<select name="new-video-category">
                        <?php 
                            foreach ($category['data'] as $row) 
                        {
                                $category_id = $row['category_id'];
                                $category_name = $row['name'];
                        ?>
                            <option value="<?=$category_id?>" <?php if($category_id == $video_detail['dataObject']->cat_id) echo "selected";?>><?=$category_name?></option>
                        <?php
                        }
                        ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Current Thumbnail :</td>
				<td><img src="<?=base_url()?>images/video_thumbnail/<?=$video_detail['dataObject']->thumbnail?>" width="150" height="100"></td>
				<td>New Thumbnail :</td>
				<td><input type="file" name="thumbnail"> </td>
			</tr>
			<tr>
				<td>Sort :</td>
				<td><input type="text" name="new-sort" value="<?=$video_detail['dataObject']->sort_video?>"></td>
			</tr>
			<tr>
				<td>Feature :</td>
				<td><input type="checkbox" name="new-feature" <?php if($video_detail['dataObject']->feature_video == 1) echo "checked";?>></td>
			</tr>
			<tr>
				<td>Publish :</td>
				<td>
					<select name="new-publish">
                        <option value="1" <?php if($video_detail['dataObject']->publish == 1) echo "selected";?>>Publish</option>
						<option value="0" <?php if($video_detail['dataObject']->publish == 0) echo "selected";?>>Unpublish</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Download Link :</td>
				<td><input type="text" name="new-download-link" value="<?=$video_detail['dataObject']->download_link?>"></td>
			</tr>
			<tr>
				<td><input type="submit" name="edit-video" value="Edit Video"></td>
			</tr>
			
	</table>

<?php 
	echo form_close();
	$this->load->view('footer');
?>