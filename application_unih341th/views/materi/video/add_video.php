<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<br>

<?php 
	echo anchor('materi/pengaturan_video','Back'); // update by pincul 20171412
	echo form_open_multipart('materi/pengaturan_video/process_add_video', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));

?>
	<table width="99%">
			<tr>
				<td>Title :</td>
				<td><input type="text" name="video-title"></td>
			</tr>
			<tr>
				<td>Description :</td>
				<td>
					<textarea name="video-description" rows="10" cols="40"></textarea>
				</td>
			</tr>
			<tr>
				<td>Youtube Link :</td>
				<td><input type="text" name="video-link"></td>
			</tr>
			<tr>
				<td>Category :</td>
				<td>
					<select name="video-category">
						<option value="0">-Choose One-</option>
                        <?php 
                            foreach ($category['data'] as $row) 
                        {
                                $category_id = $row['catalogue_category_id'];
                                $category_name = $row['name'];
                        ?>
                            <option value="<?=$category_id?>"><?=$category_name?></option>
                        <?php
                        }
                        ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Thumbnail :</td>
				<td><input type="file" name="thumbnail"></td>
			</tr>
			<tr>
				<td>Sort :</td>
				<td><input type="text" name="sort"></td>
			</tr>
			<tr>
				<td>Feature :</td>
				<td><input type="checkbox" name="feature"> Featured</td>
			</tr>
			<tr>
				<td>Status :</td>
				<td>
					<select name="publish">
						<option value="1">Publish</option>
						<option value="0">Unpublish</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Download Link :</td>
				<td><input type="text" name="download-link"></td>
			</tr>
			<tr>
				<td><br><input type="submit" name="add-video" value="Add Video"></td>
			</tr>
	</table>

<?php 
	echo form_close();
	$this->load->view('footer');
?>