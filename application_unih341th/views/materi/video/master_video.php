<?php $this->load->view('header_video');?>
<h2><?php echo $page_title;?></h2>
<?php 
	if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>
	<div style="text-align:center; padding: 8px 40px; width: 80%; margin: auto; display: block;" >
		<div class="html5gallery-loading" style="display:block;margin:0 auto;width:300px;height:300px;background:url('<?=base_url()?>styles/html5gallery/loading.gif') no-repeat center center;"></div> 
		<div style="display:none;margin:0 auto;" class="html5gallery" data-onchange="onSlideChange" data-onthumbclick="onThumbClick" data-skin="gallery" data-autoplayvideo="false" data-responsive="true" data-resizemode="fill" data-html5player="true" data-autoslide="true" data-autoplayvideo="false" data-width="300" data-height="150" data-effect="fadeout" data-shownumbering="true" data-numberingformat="%NUM of %TOTAL - " data-googleanalyticsaccount="UA-29319282-1"  data-thumbwidth="220" data-thumbheight="120" data-thumbmargin="50" data-thumbgap="20" data-thumbtitlecss="{text-align:center; color:#000000; font-size:12px; font-family:Armata, sans-serif, Arial; overflow:hidden; white-space:wrap; padding:0px 0 10px 0;}" data-thumbtitleheight="40">

			<?php 
				foreach ($video_banner['data'] as $row1) {
					$id_1 = $row1['video_id'];
					$youtube_link_1 = $row1['youtube_link'];
					$thumbnail_1 = $row1['thumbnail'];
					$title_1 = $row1['title'];
					$description_1 = $row1['description'];
					$download_link_1 = $row1['download_link'];
			?>
					<a href="<?=$youtube_link_1?>"  ><img width="400px" src="<?=base_url()?>images/video_thumbnail/<?=$thumbnail_1?>" alt="<?=$title_1?>"  data-description="<?=$title?>"></a>
					
			<?php
				}
			 ?>
		</div>
	</div> 

<?php 
	if ($video_cat_1['countResult'] > 5) {
?>
		<h3><?=$video_cat_1['dataObject']->name?></h3>
		<hr>
		<br>
		<div class="gallery-wrapper">
			<section class="regular slider">
			<?php 
				foreach ($video_cat_1['data'] as $row2) {
					$id_2 = $row2['video_id'];
					$youtube_link_2 = $row2['youtube_link'];
					$thumbnail_2 = $row2['thumbnail'];
					$title_2 = $row2['title'];
					$description_2 = $row2['description'];
					$download_link_2 = $row2['download_link'];
			?>

					<div class="gallery">
					  <a href="<?=base_url()?>/materi/video/detail_video/<?=$id_2?>">
					    <img src="<?=base_url()?>images/video_thumbnail/<?=$thumbnail_2?>" width="320" height="180">
					  </a>
					  <a style="font-size: 12px; font-weight: bold; padding: 0 45px 0 0;" href="<?=$download_link_2?>" download="<?=$title_2?>">
						   <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/download.ico"> Download
					 	</a>
						<a style="font-size: 12px; font-weight: bold;" href="<?=base_url()?>/materi/video/detail_video/<?=$id_2?>">
						    <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/play.png"> View
						</a>
					  <div class="desc"><?=$title_2?></div>
					</div>
			<?php
				}
			?>
			</section>
		</div>
		<div style="clear: both;"></div>

<?php
	} elseif($video_cat_1['countResult'] > 0) {
 ?>
 		<h3><?=$video_cat_1['dataObject']->name?></h3>
		<hr>
		<br>
		<div class="gallery-wrapper">
			<?php 
				foreach ($video_cat_1['data'] as $row2) {
					$id_2 = $row2['video_id'];
					$youtube_link_2 = $row2['youtube_link'];
					$thumbnail_2 = $row2['thumbnail'];
					$title_2 = $row2['title'];
					$description_2 = $row2['description'];
					$download_link_2 = $row2['download_link'];
			?>

					<div class="gallery">
					  <a href="<?=base_url()?>/materi/video/detail_video/<?=$id_2?>">
					    <img src="<?=base_url()?>images/video_thumbnail/<?=$thumbnail_2?>" width="320" height="180">
					  </a>
					  <a style="font-size: 12px; font-weight: bold; padding: 0 45px 0 0;" href="<?=$download_link_2?>" download="<?=$title_2?>">
						   <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/download.ico"> Download
					 	</a>
						<a style="font-size: 12px; font-weight: bold;" href="<?=base_url()?>/materi/video/detail_video/<?=$id_2?>">
						    <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/play.png"> View
						</a>
					  <div class="desc"><?=$title_2?></div>
					</div>
			<?php
				}
			?>
		</div>
		<div style="clear: both;"></div>


 <?php 
 	}
 ?>

<?php 
	if ($video_cat_2['countResult'] > 5) {
?>	
		<h3><?=$video_cat_2['dataObject']->name?></h3>
		<hr>
		<br>
		<div class="gallery-wrapper">
			<section class="regular slider">
			<?php 
				foreach ($video_cat_2['data'] as $row3) {
					$id_3 = $row3['video_id'];
					$youtube_link_3 = $row3['youtube_link'];
					$thumbnail_3 = $row3['thumbnail'];
					$title_3 = $row3['title'];
					$description_3 = $row3['description'];
					$download_link_3 = $row3['download_link'];
			?>

					<div class="gallery">
					  <a href="<?=base_url()?>/materi/video/detail_video/<?=$id_3?>">
					    <img src="<?=base_url()?>images/video_thumbnail/<?=$thumbnail_3?>" width="320" height="180">
					  </a>
					  <a style="font-size: 12px; font-weight: bold; padding: 0 45px 0 0;" href="<?=$download_link_3?>" download="<?=$title_3?>">
						   <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/download.ico"> Download
					 	</a>
						<a style="font-size: 12px; font-weight: bold;" href="<?=base_url()?>/materi/video/detail_video/<?=$id_3?>">
						    <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/play.png"> View
						</a>
					  <div class="desc"><?=$title_3?></div>
					</div>
			<?php
				}
			?>
			</section>
		</div>
		<div style="clear: both;"></div>
<?php
	} elseif ($video_cat_2['countResult'] > 0) {
?>
		<h3><?=$video_cat_2['dataObject']->name?></h3>
		<hr>
		<br>
		<div class="gallery-wrapper">
			<?php 
				foreach ($video_cat_2['data'] as $row3) {
					$id_3 = $row3['video_id'];
					$youtube_link_3 = $row3['youtube_link'];
					$thumbnail_3 = $row3['thumbnail'];
					$title_3 = $row3['title'];
					$description_3 = $row3['description'];
					$download_link_3 = $row3['download_link'];
			?>

					<div class="gallery">
					  <a href="<?=base_url()?>/materi/video/detail_video/<?=$id_3?>">
					    <img src="<?=base_url()?>images/video_thumbnail/<?=$thumbnail_3?>" width="320" height="180">
					  </a>
					  	<a style="font-size: 12px; font-weight: bold; padding: 0 45px 0 0;" href="<?=$download_link_3?>" download="<?=$title_3?>">
						   <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/download.ico"> Download
					 	</a>
						<a style="font-size: 12px; font-weight: bold;" href="<?=base_url()?>/materi/video/detail_video/<?=$id_3?>">
						    <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/play.png"> View
						</a>
					  <div class="desc"><?=$title_3?></div>
					</div>
			<?php
				}
			?>
		</div>
		<div style="clear: both;"></div>
<?php
	}
 ?>

 <?php 
	if ($video_cat_3['countResult'] > 5) {
?>	
		<h3><?=$video_cat_3['dataObject']->name?></h3>
		<hr>
		<br>
		<div class="gallery-wrapper">
			<section class="regular slider">
			<?php 
				foreach ($video_cat_3['data'] as $row4) {
					$id_4 = $row4['video_id'];
					$youtube_link_4 = $row4['youtube_link'];
					$thumbnail_4 = $row4['thumbnail'];
					$title_4 = $row4['title'];
					$description_4 = $row4['description'];
					$download_link_4 = $row4['download_link'];
			?>

					<div class="gallery">
					  <a href="<?=base_url()?>/materi/video/detail_video/<?=$id_4?>">
					    <img src="<?=base_url()?>images/video_thumbnail/<?=$thumbnail_4?>" width="320" height="180">
					  </a>
					  <a style="font-size: 12px; font-weight: bold; padding: 0 45px 0 0;" href="<?=$download_link_4?>" download="<?=$title_4?>">
						   <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/download.ico"> Download
					 	</a>
						<a style="font-size: 12px; font-weight: bold;" href="<?=base_url()?>/materi/video/detail_video/<?=$id_4?>">
						    <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/play.png"> View
						</a>
					  <div class="desc"><?=$title_4?></div>
					</div>
			<?php
				}
			?>
			</section>
		</div>
		<div style="clear: both;"></div>
<?php
	} elseif ($video_cat_3['countResult'] > 0) {
?>
		<h3><?=$video_cat_3['dataObject']->name?></h3>
		<hr>
		<br>
		<div class="gallery-wrapper">
			<?php 
				foreach ($video_cat_3['data'] as $row4) {
					$id_4 = $row4['video_id'];
					$youtube_link_4 = $row4['youtube_link'];
					$thumbnail_4 = $row4['thumbnail'];
					$title_4 = $row4['title'];
					$description_4 = $row4['description'];
					$download_link_4 = $row4['download_link'];
			?>

					<div class="gallery">
					  <a href="<?=base_url()?>/materi/video/detail_video/<?=$id_4?>">
					    <img src="<?=base_url()?>images/video_thumbnail/<?=$thumbnail_4?>" width="320" height="180">
					  </a>
					  <a style="font-size: 12px; font-weight: bold; padding: 0 45px 0 0;" href="<?=$download_link_4?>" download="<?=$title_4?>">
						   <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/download.ico"> Download
					 	</a>
						<a style="font-size: 12px; font-weight: bold;" href="<?=base_url()?>/materi/video/detail_video/<?=$id_4?>">
						    <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/play.png"> View
						</a>
					  <div class="desc"><?=$title_4?></div>
					</div>
			<?php
				}
			?>
		</div>
		<div style="clear: both;"></div>
<?php
	}
 ?>

 <?php 
	if ($video_cat_4['countResult'] > 5) {
?>	
		<h3><?=$video_cat_4['dataObject']->name?></h3>
		<hr>
		<br>
		<div class="gallery-wrapper">
			<section class="regular slider">
			<?php 
				foreach ($video_cat_4['data'] as $row5) {
					$id_5 = $row5['video_id'];
					$youtube_link_5 = $row5['youtube_link'];
					$thumbnail_5 = $row5['thumbnail'];
					$title_5 = $row5['title'];
					$description_5 = $row5['description'];
					$download_link_5 = $row5['download_link'];
			?>

					<div class="gallery">
					  <a href="<?=base_url()?>/materi/video/detail_video/<?=$id_5?>">
					    <img src="<?=base_url()?>images/video_thumbnail/<?=$thumbnail_5?>" width="320" height="180">
					  </a>
					  <a style="font-size: 12px; font-weight: bold; padding: 0 45px 0 0;" href="<?=$download_link_5?>" download="<?=$title_5?>">
						   <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/download.ico"> Download
					 	</a>
						<a style="font-size: 12px; font-weight: bold;" href="<?=base_url()?>/materi/video/detail_video/<?=$id_5?>">
						    <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/play.png"> View
						</a>
					  <div class="desc"><?=$title_5?></div>
					</div>
			<?php
				}
			?>
			</section>
		</div>
		<div style="clear: both;"></div>
<?php
	} elseif ($video_cat_4['countResult'] > 0) {
?>
		<h3><?=$video_cat_4['dataObject']->name?></h3>
		<hr>
		<br>
		<div class="gallery-wrapper">
			<?php 
				foreach ($video_cat_4['data'] as $row5) {
					$id_5 = $row5['video_id'];
					$youtube_link_5 = $row5['youtube_link'];
					$thumbnail_5 = $row5['thumbnail'];
					$title_5 = $row5['title'];
					$description_5 = $row5['description'];
					$download_link_5 = $row5['download_link'];
			?>

					<div class="gallery">
					  <a href="<?=base_url()?>/materi/video/detail_video/<?=$id_5?>">
					    <img src="<?=base_url()?>images/video_thumbnail/<?=$thumbnail_5?>" width="320" height="180">
					  </a>
					  <a style="font-size: 12px; font-weight: bold; padding: 0 45px 0 0;" href="<?=$download_link_5?>" download="<?=$title_5?>">
						   <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/download.ico"> Download
					 	</a>
						<a style="font-size: 12px; font-weight: bold;" href="<?=base_url()?>/materi/video/detail_video/<?=$id_5?>">
						    <img style="width: 10%; height: 10%; position:relative; top: 4px;" alt="ImageName" src="<?=base_url()?>images/play.png"> View
						</a>
					  <div class="desc"><?=$title_5?></div>
					</div>
			<?php
				}
			?>
		</div>
		<div style="clear: both;"></div>
<?php
	}
 ?>
<?php 
$this->load->view('footer');
?>