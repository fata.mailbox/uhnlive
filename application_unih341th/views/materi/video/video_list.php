<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<br>
<div style="width: 100%px; height: 30px; float:right;">
<?php echo form_open('materi/pengaturan_video', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off')); ?>
		<input type="text" name="video-keyword" value="<?=$keyword?>" placeholder="Search Video..">
		<input type="submit" name="search">
<?php 	echo form_close(); ?>



</div>
<?php 
	echo anchor('materi/pengaturan_video/add_video','Add Video'); // update by pincul 20171412

	if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>
<br><br>
<?php 
		if(!empty($success_msg))
		{
			echo $success_msg ."<br><br>";
		}

		if(!empty($error_msg))
		{
			echo $error_msg ."<br><br>";
		}

		if(!empty($error_upload))
		{
			echo $error_upload ."<br><br>";
		}

	 ?>
	 <br>
<table class="stripe">
	<tr>
      <th width='5%'>No.</th>
      <th>Title</th>
      <th>Category</th>
      <th>Feature</th>
      <th>Thumbnail</th>
      <th>Action</th>
    </tr>
 <?php 
 $i = 0;
			if($video['countResult'] > 0)
			{
				foreach ($video['data'] as $row) {
					$id = $row['video_id'];
					$title = $row['title'];
					$description = $row['description'];
					$youtube_link = $row['youtube_link'];
					$category = $row['name'];
					$thumbnail = $row['thumbnail'];
					$sort = $row['sort_video'];
					$feature_video = $row['feature_video'];
					$i++;

		?>
				<tr>
					<td><?=$i?></td>
					<td><?=$title?></td>
					<td><?=$category?></td>
					<td><?php if($feature_video == 1) echo "Yes"; else echo"No";?></td>
					<td><img src="<?=base_url()?>images/video_thumbnail/<?=$thumbnail?>" width="150" height="100"></td>
					<td>
						<a href="<?php echo base_url()?>materi/pengaturan_video/video_detail/<?=$id?>">View</a>
						<a href="<?php echo base_url()?>materi/pengaturan_video/edit_video/<?=$id?>">Edit</a>
						<a href="<?php echo base_url()?>materi/pengaturan_video/delete_video/<?=$id?>">Delete</a>
					</td>
				</tr>
		<?php
				}
			} else {


		?>

				<tr>
					<td colspan="6" align="center">No Data</td>
				</tr>
		<?php
			}
		 ?>
</table>
<?php 
	echo $this->pagination->create_links();
$this->load->view('footer');
?>