<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<br>
<div style="width: 100%px; height: 30px; float:right;">
<?php echo form_open('materi/pengaturan_catalogue', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off')); ?>
	<table>
		<!-- <tr>
			<td>Start Period :</td>
			<td><input type="date" name="start-period"><br></td>
		</tr>
		<tr>
			<td>End Period :</td>
			<td><input type="date" name="end-period"><br></td>
		</tr> -->
		<tr>
			<td>Keyword : </td>
			<td><input type="text" name="catalogue-keyword"></td>
		</tr>
		<tr>
			<td><input type="submit" name="search"></td>
		</tr>
	</table>
<?php 	echo form_close(); ?>



</div>
<?php 
	echo anchor('materi/pengaturan_catalogue/add_catalogue','Add Catalogue'); // update by pincul 20171412

	if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>
<br><br>
<?php 
		if(!empty($success_msg))
		{
			echo $success_msg ."<br><br>";
		}

		if(!empty($error_msg))
		{
			echo $error_msg ."<br><br>";
		}

		if(!empty($error_upload))
		{
			echo $error_upload ."<br><br>";
		}

	 ?>
	 <br><br><br><br>
<table class="stripe">
	<tr>
      <th width='5%'>No.</th>
      <th>Title</th>
      <th>Start Period</th>
      <th>End Period</th>
      <th>Sort</th>
      <th>Thumbnail</th>
      <th>Action</th>
    </tr>
 <?php 
 $i = 0;
			if($catalogue['countResult'] > 0)
			{
				foreach ($catalogue['data'] as $row) {
					$id = $row['catalogue_id'];
					$title = $row['title'];
					$start_period = date_create($row['start_period']);
					$end_period = date_create($row['end_period']);
					$thumbnail = $row['thumbnail'];
					$sort = $row['sort_catalogue'];
					$i++;

		?>
				<tr>
					<td><?=$i?></td>
					<td><?=$title?></td>
					<td><?php echo date_format($start_period, 'j F Y');?></td>
					<td><?php echo date_format($end_period, 'j F Y');?></td>
					<td><?=$sort?></td>
					<td><img src="<?=base_url()?>images/catalogue_thumbnail/<?=$thumbnail?>" width="150" height="100"></td>
					<td>
						<a href="<?php echo base_url()?>materi/pengaturan_catalogue/catalogue_detail/<?=$id?>">View</a>
						<a href="<?php echo base_url()?>materi/pengaturan_catalogue/edit_catalogue/<?=$id?>">Edit</a>
						<a href="<?php echo base_url()?>materi/pengaturan_catalogue/delete_catalogue/<?=$id?>">Delete</a>
					</td>
				</tr>
		<?php
				}
			} else {


		?>

				<tr>
					<td colspan="6" align="center">No Data</td>
				</tr>
		<?php
			}
		 ?>
</table>
<?php 
	echo $this->pagination->create_links();
$this->load->view('footer');
?>