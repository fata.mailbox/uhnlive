<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<br>

<?php 
	echo anchor('materi/pengaturan_catalogue','Back'); // update by pincul 20171412

?>
		<table>
			<tr>
				<td>Title :</td>
				<td><input type="text" name="catalogue-title" value="<?=$catalogue_detail['dataObject']->title?>" disabled></td>
			</tr>
			<tr>
				<td>Start Period :</td>
				<td>
					<input type="date" name="start-date" value="<?=$catalogue_detail['dataObject']->start_period?>" disabled>
				</td>
			</tr>
			<tr>
				<td>Start Period :</td>
				<td>
					<input type="date" name="end-date" value="<?=$catalogue_detail['dataObject']->end_period?>" disabled>
				</td>
			</tr>
			<tr>
				<td>PDF File :</td>
				<td>
					<a href="/home/deploy/source/sohomlm/catalogue_file/<?=$catalogue_detail['dataObject']->pdf_file?>" download="<?=$catalogue_detail['dataObject']->pdf_file?>">Download</a>
				</td>
			</tr>
			<tr>
				<td>Thumbnail :</td>
				<td><img src="<?=base_url()?>images/catalogue_thumbnail/<?=$catalogue_detail['dataObject']->thumbnail?>" width="150" height="100"></td>
			</tr>
			<tr>
				<td>Title :</td>
				<td><input type="text" name="catalogue-title" value="<?=$catalogue_detail['dataObject']->name?>" disabled></td>
			</tr>
			<tr>
				<td>Sort :</td>
				<td><input type="text" name="sort" value="<?=$catalogue_detail['dataObject']->sort_catalogue?>" disabled></td>
			</tr>
		</table>
		<br><br>
		<table class="stripe">
			<thead>
				<tr>
					<th >Page Number</th>
					<th >Image</th>
				</tr>
			</thead>

			<tbody id="myForm">
				<?php 
				if ($catalogue_pages['countResult'] > 0) {
					foreach ($catalogue_pages['data'] as $row) {
						$id = $row['detail_catalogue_id'];
						$catalogue_id = $row['cat_id'];
						$page_number = $row['page_number'];
						$image_ = $row['image'];
						$image = str_replace(' ', '%20', $image_);
				?>
					<tr>
						<td><?=$page_number?></td>
						<td><img src="<?=base_url()?>images/catalogue_pages/<?=$image?>" width="150" height="100"><td>
					</tr>
				<?php
					}
				}
				 ?>
			</tbody>
			
		</table>
<?php 

	$this->load->view('footer');
?>