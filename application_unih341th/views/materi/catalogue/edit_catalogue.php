<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<br>

<?php 
	echo anchor('materi/pengaturan_catalogue','Back'); // update by pincul 20171412
	echo form_open_multipart('materi/pengaturan_catalogue/process_edit_catalogue', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>
		<table>
			<input type="hidden" name="id" value="<?=$catalogue_detail['dataObject']->catalogue_id?>">
			<tr>
				<td>Title :</td>
				<td><input type="text" name="new-catalogue-title" value="<?=$catalogue_detail['dataObject']->title?>" ></td>
			</tr>
			<tr>
				<td>Start Period :</td>
				<td>
					<input type="date" name="new-start-date" value="<?=$catalogue_detail['dataObject']->start_period?>" >
				</td>
			</tr>
			<tr>
				<td>Start Period :</td>
				<td>
					<input type="date" name="new-end-date" value="<?=$catalogue_detail['dataObject']->end_period?>" >
				</td>
			</tr>
			<tr>
				<td>Current PDF File :</td>
				<td style="padding-right: 50px;">
					<?=$catalogue_detail['dataObject']->pdf_file?>
				</td>
				<td>New PDF File :</td>
				<td><input type="file" name="catalogue-pdf"> </td>
			</tr>
			<tr>
				<td>Current Thumbnail :</td>
				<td><img src="<?=base_url()?>images/catalogue_thumbnail/<?=$catalogue_detail['dataObject']->thumbnail?>" width="150" height="100"></td>
				<td>New Thumbnail :</td>
				<td><input type="file" name="thumbnail"> </td>
			</tr>
			<tr>
				<td>Category :</td>
				<td>
					<select name="new-catalogue-category">
                        <?php 
                            foreach ($category['data'] as $row) 
                        {
                                $category_id = $row['catalogue_category_id'];
                                $category_name = $row['name'];
                        ?>
                            <option value="<?=$category_id?>" <?php if($category_id == $catalogue_detail['dataObject']->category_id) echo "selected";?>><?=$category_name?></option>
                        <?php
                        }
                        ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Sort :</td>
				<td><input type="text" name="new-sort" value="<?=$catalogue_detail['dataObject']->sort_catalogue?>"></td>
			</tr>
		</table>
		<br><br>
		<table class="stripe">
			<thead>
				<tr>
					<th >Page Number</th>
					<th >Image</th>
					<th>Remove</th>
				</tr>
			</thead>

			<tbody id="myForm">
				<?php
				if ($catalogue_pages['countResult'] > 0) {
					foreach ($catalogue_pages['data'] as $row) {
						$id = $row['detail_catalogue_id'];
						$catalogue_id = $row['cat_id'];
						$page_number = $row['page_number'];
						$image_ = $row['image'];
						$image = str_replace(' ', '%20', $image_);
				?>
						<tr>
							<td>
								<input type="text" name="page[]" value="<?=$page_number?>" style="margin-left: 20px;">
							</td>
							<td>
								<img src="<?=base_url()?>images/catalogue_pages/<?=$image?>" width="150" height="100">
								<input type="file" name="img[]" style="margin-left: 20px;">
								<input type="hidden" name="pic[]" value="<?=$image?>">
							<td>
						</tr>
				<?php
					}
				}
				 ?>
			</tbody>
			
		</table>
		<br>
		<button type=button onclick="pageFunction()">Add</button>
		<button type=button onclick="resetElements()">Reset</button>
			<br><br><br>

			
		<input type="submit" name="edit-catalogue" value="Edit Catalogue">
<?php 
	echo form_close();
	$this->load->view('footer');
?>