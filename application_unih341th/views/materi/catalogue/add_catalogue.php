<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<br>

<?php 
	echo anchor('materi/pengaturan_catalogue','Back'); // update by pincul 20171412
	echo form_open_multipart('materi/pengaturan_catalogue/process_add_catalogue', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));

?>
		<table>
			<tr>
				<td>Title :</td>
				<td><input type="text" name="catalogue-title"></td>
			</tr>
			<tr>
				<td>Start Period :</td>
				<td>
					<input type="date" name="start-date">
				</td>
			</tr>
			<tr>
				<td>Start Period :</td>
				<td>
					<input type="date" name="end-date">
				</td>
			</tr>
			<tr>
				<td>PDF File :</td>
				<td>
					<input type="file" name="catalogue-pdf">
				</td>
			</tr>
			<tr>
				<td>Thumbnail :</td>
				<td><input type="file" name="thumbnail"></td>
			</tr>
			<tr>
				<td>Category :</td>
				<td>
					<select name="catalogue-category">
						<option value="0">-Choose One-</option>
                        <?php 
                            foreach ($category['data'] as $row) 
                        {
                                $category_id = $row['catalogue_category_id'];
                                $category_name = $row['name'];
                        ?>
                            <option value="<?=$category_id?>"><?=$category_name?></option>
                        <?php
                        }
                        ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Sort :</td>
				<td><input type="text" name="sort"></td>
			</tr>
		</table>
		<br><br>
		<table class="stripe">
			<thead>
				<tr>
					<th >Page Number</th>
					<th >Image</th>
					<th >Remove</th>
				</tr>
			</thead>

			<tbody id="myForm">
			</tbody>
			
		</table>

		<br>
		<button type=button onclick="pageFunction()">Add</button>
		<button type=button onclick="resetElements()">Reset</button>
			<br><br><br>

			
		<input type="submit" name="add-catalogue" value="Add Catalogue">
<?php 
	echo form_close();
	$this->load->view('footer');
?>