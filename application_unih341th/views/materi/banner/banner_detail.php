<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<br>

<?php 
	echo anchor('materi/pengaturan_banner','Back'); // update by pincul 20171412

?>
		 <table>
		 	<tr>
				<td>ID</td>
				<td><input type="text" name="title" value="<?=$banner_detail['dataObject']->banner_id?>" disabled></td>
			</tr>
			<tr>
				<td>Title</td>
				<td><input type="text" name="title" value="<?=$banner_detail['dataObject']->title?>" disabled></td>
			</tr>
			<tr>
				<td>Category</td>
				<td>
					<select name="category" disabled>
						<option value="0">Choose One</option>
						<?php 
							foreach ($category['data'] as $row) {
								$id = $row['banner_category_id'];
								$name = $row['name'];
						?>
								<option value="<?=$id?>" <?php if($banner_detail['dataObject']->cat_id == $id) echo "selected"; ?>><?=$name?></option>
						<?php
							}
						 ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Banner Image</td>
				<td><img src="<?=base_url()?>images/banner/<?=$banner_detail['dataObject']->file_name?>" width="150" height="100"></td>
			</tr>
			<tr>
				<td>Banner File</td>
				<td>
					<a href="<?=base_url()?>banner_file/<?=$banner_detail['dataObject']->pdf_file?>" download="<?=$banner_detail['dataObject']->pdf_file?>">Download</a>
				</td>
			</tr>
			<tr>
				<td>Publish</td>
				<td>
					<select name="publish" disabled>
						<option value="1" <?php if($banner_detail['dataObject']->publish == 1) echo "selected"; ?>>Yes</option>
						<option value="0" <?php if($banner_detail['dataObject']->publish == 0) echo "selected"; ?>>No</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Sort</td>
				<td><input type="text" name="sort" value="<?=$banner_detail['dataObject']->sort_banner?>" disabled></td>
			</tr>
		</table>
		<br>
<?php 
	$this->load->view('footer');
?>