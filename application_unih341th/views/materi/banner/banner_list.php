<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<br>

<div style="width: 100%px; height: 30px; float:right;">
<?php echo form_open('materi/pengaturan_banner', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off')); ?>
		<input type="text" name="banner-keyword" value="<?=$keyword?>" placeholder="Search Banner..">
		<input type="submit" name="search">
<?php 	echo form_close(); ?>
</div>
<?php 
	echo anchor('materi/pengaturan_banner/add_banner','Add New Banner'); // update by pincul 20171412

	if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>
<br><br>
<?php 
	if(!empty($success_msg))
	{
		echo $success_msg ."<br><br>";
	}

	if(!empty($error_msg))
	{
		echo $error_msg ."<br><br>";
	}

	if(!empty($error_upload))
	{
		echo $error_upload ."<br><br>";
	}

?>
<br>
<table class="stripe">
	<tr>
			<th>No</th>
			<th>Title</th>
			<th>Image</th>
			<th>Type</th>
			<th>Action</th>
		</tr>
   
   <?php 
   			$i = 0;
   			if($banner['countResult'] > 0)
			{
			foreach ($banner['data'] as $row) {
					$id = $row['banner_id'];
					$name = $row['name'];
					$title = $row['title'];
					$category_id = $row['cat_id'];
					$file_name = $row['file_name'];
					$sort = $row['sort_banner'];
					$i++;

	?>
    <tr>
		<td><?=$i?></td>
		<td><?=$title?></td>
		<td><a href="#img<?=$id?>"><img src="<?=base_url()?>images/banner/<?=$file_name?>" width="200" height="100"></a></td>
		<td><?=$name?></td>
		<td>
			<a href="<?php echo base_url()?>materi/pengaturan_banner/banner_detail/<?=$id?>">View</a>
			<a href="<?php echo base_url()?>materi/pengaturan_banner/edit_banner/<?=$id?>">Edit</a>
			<a href="<?php echo base_url()?>materi/pengaturan_banner/delete_banner/<?=$id?>">Delete</a>
		</td>
    </tr>

     <a href="#_" style="padding: 50px 0 0 0;" class="lightbox" id="img<?=$id?>">
					<img src="<?=base_url()?>images/banner/<?=$file_name?>">
				</a>
    <?php
				}
			} else {


	?>

				<tr>
					<td colspan="5" align="center">No Data</td>
				</tr>
	<?php
		}
	?>
    
    <!-- <tr>
      <td colspan="7">Data is not available.</td>
    </tr> -->
</table>

<?php 
echo $this->pagination->create_links();
$this->load->view('footer');
?>