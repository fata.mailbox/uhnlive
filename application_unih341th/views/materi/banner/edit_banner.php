<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<br>

<?php 
	echo anchor('materi/pengaturan_banner','Back'); // update by pincul 20171412
	echo form_open_multipart('materi/pengaturan_banner/process_edit_banner', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));

?>
		 <table>
		 	<tr>
				<td><input type="hidden" name="id" value="<?=$banner_detail['dataObject']->banner_id?>"></td>
			</tr>
			<tr>
				<td>Title</td>
				<td><input type="text" name="new-title" value="<?=$banner_detail['dataObject']->title?>"></td>
			</tr>
			<tr>
				<td>Category</td>
				<td>
					<select name="new-category">
						<option value="0">Choose One</option>
						<?php 
							foreach ($category['data'] as $row) {
								$id = $row['banner_category_id'];
								$name = $row['name'];
						?>
								<option value="<?=$id?>" <?php if($banner_detail['dataObject']->cat_id == $id) echo "selected"; ?>><?=$name?></option>
						<?php
							}
						 ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Current Banner Image :</td>
				<td><img src="<?=base_url()?>images/banner/<?=$banner_detail['dataObject']->file_name?>" width="150" height="100"></td>
				<td>New Banner Image :</td>
				<td><input type="file" name="banner"> </td>
			</tr>
			<tr>
				<td>Current Banner File :</td>
				<td><?php echo $banner_detail['dataObject']->pdf_file;?></td>
				<td>New Banner File :</td>
				<td><input type="file" name="pdf"> </td>
			</tr>
			<tr>
				<td>Publish</td>
				<td>
					<select name="new-publish">
						<option value="1" <?php if($banner_detail['dataObject']->publish == 1) echo "selected"; ?>>Yes</option>
						<option value="0" <?php if($banner_detail['dataObject']->publish == 0) echo "selected"; ?>>No</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Sort</td>
				<td><input type="text" name="new-sort" value="<?=$banner_detail['dataObject']->sort_banner?>"></td>
			</tr>
		</table>
		<br>
		<input type="submit" name="edit-banner" value="Edit Banner">
<?php 
	echo form_close();
	$this->load->view('footer');
?>