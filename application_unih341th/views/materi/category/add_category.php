<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<br>

<?php 
	echo anchor('materi/category','Back'); // update by pincul 20171412
	echo form_open('materi/category/add_category', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));

?>
	<table width="99%">
		<tr>
			<td width='14%' valign='top'>Category Name :</td>
			<td><input type="text" name="category-name"></td>
		</tr>
		<tr>
			<td width='14%' valign='top'>Sort :</td>
			<td><input type="text" name="sort"></td>
		</tr>
		<tr>
			<td><input type="submit" name="add-category" value="Add Category"></td>
		</tr>
	</table>

<?php 
	echo form_close();
	$this->load->view('footer');
?>