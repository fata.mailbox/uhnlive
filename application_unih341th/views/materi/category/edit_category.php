<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<br>

<?php 
	echo anchor('materi/category','Back'); // update by pincul 20171412
	echo form_open('materi/category/process_edit_category', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));

?>
	<table width="99%">
		<input type="hidden" name="id" value="<?=$category_detail['dataObject']->category_id?>">
		<tr>
			<td width='14%' valign='top'>Category Name :</td>
			<td><input type="text" name="new-category-name" value="<?=$category_detail['dataObject']->name?>"></td>
		</tr>
		<tr>
			<td width='14%' valign='top'>Sort :</td>
			<td><input type="text" name="new-sort" value="<?=$category_detail['dataObject']->sort_category?>"</td>
		</tr>
		<tr>
			<td><input type="submit" name="edit-category" value="Edit Category"></td>
		</tr>
	</table>

<?php 
	echo form_close();
	$this->load->view('footer');
?>