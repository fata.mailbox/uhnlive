<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<br>

<div style="width: 100%px; height: 30px; float:right;">
<?php echo form_open('materi/category', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off')); ?>
		<input type="text" name="category-keyword" value="<?=$keyword?>" placeholder="Search Category..">
		<input type="submit" name="search">
<?php 	echo form_close(); ?>
</div>
<?php 
	echo anchor('materi/category/add_category','Add New Category'); // update by pincul 20171412

	if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>
<br><br>
<?php 
	if(!empty($success_msg))
	{
		echo $success_msg ."<br><br>";
	}

	if(!empty($error_msg))
	{
		echo $error_msg ."<br><br>";
	}

	if(!empty($error_upload))
	{
		echo $error_upload ."<br><br>";
	}

?>
<br>
<table class="stripe">
	<tr>
      <th>No.</th>
      <th>Title</th>
      <th>Sort</th>
      <th>Action</th>
    </tr>
   
   <?php 
   			$i = 0;
			if($category['countResult'] > 0)
			{
				foreach ($category['data'] as $row) {
					$id = $row['category_id'];
					$name = $row['name'];
					$sort = $row['sort_category'];
					$i++;

	?>
    <tr>
		<td><?=$i?></td>
		<td><?=$name?></td>
		<td><?=$sort?></td>
		<td>
			<a href="<?php echo base_url()?>materi/category/category_detail/<?=$id?>">View</a>
			<a href="<?php echo base_url()?>materi/category/edit_category/<?=$id?>">Edit</a>
			<a href="<?php echo base_url()?>materi/category/delete_category/<?=$id?>">Delete</a>
		</td>
    </tr>
    <?php
				}
			} else {


	?>

				<tr>
					<td colspan="4" align="center">No Data</td>
				</tr>
	<?php
		}
	?>
    
    <!-- <tr>
      <td colspan="7">Data is not available.</td>
    </tr> -->
</table>

<?php 
echo $this->pagination->create_links();
$this->load->view('footer');
?>