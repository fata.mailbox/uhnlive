<?php $this->load->view('header');?>
<div id="view"><?php echo anchor('promo/promo_prod/edit/'.$row->id,'edit item product');?></div>

<div class="ViewHold">
	<div id="companyDetails"><?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?></div>
	<p>
		<strong><?php echo "created By : ";?> <?php echo $row->created;?><br />
		</strong>
	</p>
	<br />
<h2>Promo Product</h2>
<hr />
<h3><?php echo $row->item_id." - ".$row->item_name;?></h3>

<table class="stripe">
	<tr>
      <th width='24%'>Description</th>
      <th width='1%'>:</th>
      <th width='75%'>Value</th>
	</tr>
	<tr>
		<td>Code Product</td>
		<td>:</td>
		<td><?php echo $row->item_id;?></td>
	</tr>
	<tr>
		<td>Product</td>
		<td>:</td>
		<td><?php echo $row->item_name;?></td>
	</tr>
	<tr>
		<td>Quantity Product</td>
		<td>:</td>
		<td><?php echo $row->item_qty;?></td>
	</tr>
	<tr>
		<td>Code Free Product</td>
		<td>:</td>
		<td><?php echo $row->free_item_id;?></td>
	</tr>
	<tr>
		<td>Free Product</td>
		<td>:</td>
		<td><?php echo $row->free_item_name;?></td>
	</tr>
    <tr>
		<td>Quatity</td>
		<td>:</td>
		<td><?php echo $row->free_item_qty;?></td>
	</tr>
	<tr>
		<td>Multiples</td>
		<td>:</td>
		<td><?php if($row->multiples == 1){
					echo "Yes";
				}else{echo "No";};?></td>
	</tr>
	<tr>
		<td>End Periode</td>
		<td>:</td>
		<td><?php echo $row->end_periode;?></td>
	</tr>
	<tr>
		<td>Created Date</td>
		<td>:</td>
		<td><?php echo $row->created_date;?></td>
	</tr>
	<tr>
		<td>Created</td>
		<td>:</td>
		<td><?php echo $row->created;?></td>
	</tr>
	<tr>
		<td>Expired</td>
		<td>:</td>
		<td><?php if($row->expired==1){
					echo "Yes";
				}else{echo "No";};?></td>
    </tr>    
</table>
	
<?php $this->load->view('footer');?>
