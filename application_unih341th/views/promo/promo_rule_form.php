<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('promo/promo_rule/create', array('id' => 'form', 'name' => 'form','autocomplete' => 'off'));?>
		<table>
		<tr>
			<td valign='top' width="30%">Pilih Product </td>
      <td width="80%"> : <?php $data = array('name'=>'item_id','id'=>'itemcode0','size'=>'8','readonly'=>'1','value'=>set_value('itemcode0')); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('itemsearch/cari_prod/0', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<input type="text" name="itemname" id="itemname0" value="<?php echo set_value('itemname0');?>" readonly="1" size="25"/>
	    </td>
		</tr>
    <tr>
      <td valign="top" width="20%">Minimal Order </td>
      <td width="80%">: <input type="number" name="min_order"></td>
    </tr>
    <tr>
      <td valign="top" width="20%">Maximal Order </td>
      <td width="80%">: <input type="number" name="max_order"></td>
    </tr>
		<tr>
			<td>
		   		<?php $data = array('name'=>'exp_date','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('expireddate',$reportDate)); ?>
          Exp Date </td>
      <td>: <?php echo form_input($data);?></td>
  	</tr>
        <tr>
      <td valign="top" width="20%">Multiples</td>
      <td width="80%">: <input type="radio" name="multiples" value="1" checked="" /> Yes
        <input type="radio" name="multiples" value="0" /> No</td> </tr>

  		<br/>
      <br/>
		<tr>
      <td valign='top'>Password</td>
      <td>: <input type="password" name="password1" id="password1" value="" maxlength="50" size="13" /> 
      <span class="error">* <?php echo form_error('password1');?></span></td>
    </tr>
    <tr><td colspan='3'><?php echo form_submit('submit', 'Submit');?></td></tr>
		</table>

<?php echo form_close();?>
<?php $this->load->view('footer');?>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
</script>
