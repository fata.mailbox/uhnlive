<!--
	Copyright (c) 2019-<?php echo date("Y");?> 
	developed by  	: Annisa Rahmawaty
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php 
echo anchor('promo/promo_rule/create','create item rule order promo');
if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
						
$this->load->view('promo/promo_rule_table');
$this->load->view('footer');
?>