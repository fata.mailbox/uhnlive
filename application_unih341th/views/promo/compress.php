<br>
<?php 
if($row){
	foreach($row as $data):
		$old_member = $data['old_member'];
		$exbon1 = $data['exbon1'];
		$exbon2 = $data['exbon2'];
		$pvg = $data['pvg'];
	endforeach;
}
?>
<table width='100%'>
<?php echo form_open('member/compress', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	
<?php if($this->session->userdata('group_id') > 100){ ?>
	<tr>
		<td width='24%'>Member ID / Name</td>
		<td width='1%'>:</td>
        <td width='75%'><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('userid')." / ".$this->session->userdata('name');?></b></td>
	</tr>   
  
<?php }else{ ?>
	<tr>
		<td width='24%' valign='top'>Member ID</td>
		<td width='1%' valign='top'>:</td>
		<td width='75%'>
			<?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
				echo form_input($data);?> <?php $atts = array(
				  'width'      => '450',
				  'height'     => '600',
				  'scrollbars' => 'yes',
				  'status'     => 'yes',
				  'resizable'  => 'yes',
				  'screenx'    => '0',
				  'screeny'    => '0'
				);
				echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
				?>
		</td>
	</tr>
	<tr>
		<td valign='top'>Name</td>
		<td valign='top'>:</td>
		<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
	</tr>
<?php }?>

	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
	</tr>
<?php echo form_close();?>				
</table>
<br>
<table>
	<?php /*
	<tr>
		<td><b>Member Baru:</b></td>
		<td align="right"><?php if($old_member==0){echo "Yes";}else{echo "None";}?> </td>
		<td>&nbsp&nbsp&nbsp&nbsp&nbsp</td>
		<td>&nbsp</td>
		<td align="right"><?php //echo number_format($molecool);?></td>
	</tr>
	
	<tr>
		<td><b>Total downline :</b></td>
		<td align="right"><?php echo number_format($dl)." downline";?> </td>
		<td>&nbsp </td>
		<td>&nbsp</td>
		<td align="right"><?php //echo number_format($magozai)." PV";?></td>
	</tr>
	*/?>
	<tr>
		<td>&nbsp </td>
		<td>&nbsp </td>
		<td>&nbsp </td>
		<td>&nbsp </td>
		<td>&nbsp </td>
	</tr>
	<tr>
		<td><b>PV Group (minimum 25.000 PV) :</b></td>
		<td align="right"><?php echo number_format($pvg);?></td>
		<td>&nbsp </td>
		<td><b>Kekurangan PV Group :</b></td>
		<td align="right"><?php if($pvg<25000){echo number_format(25000 - $pvg);}else{echo "Done";}?></td>
	</tr>
	<tr>
		<td><b>ExBon Level 1 :</b></td>
		<td align="right"><?php if($exbon1>0){echo "Done";}else{echo "None";}?></td>
		<td>&nbsp </td>
		<td><b>ExBon Level 2 :</b></td>
		<td align="right"><?php if($exbon2>0){echo "Done";}else{echo "None";}?></td>
	</tr>
</table>

<p><br>
Downline
<table class="stripe">
	<tr>
      <th><div align = "right">No.</div></th>
      <th><div align = "center">ID Downline</div></th>
      <th><div align = "left">Nama Downline</div></th>
      <th><div align="right">Total Group PV</div></th>
    </tr>

<?php
if ($results): 
	foreach($results as $key => $row): ?>
    <tr>
      <td align="right"><?php echo $row['i'];?></td>
      <td align="center"><?php echo $row['id'];?></td>
      <td align="left"><?php echo $row['nama'];?></td>
     <td align="right"><b><?php echo number_format($row['totalpv']);?></b></td>
    </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
      <td colspan="4">Data is not available.</td>
    </tr>
<?php endif; ?> 
</table><br>
<em>*) Hasil berikut ini adalah hasil perhitungan sementara</em>