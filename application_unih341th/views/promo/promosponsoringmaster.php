<!--
	Copyright (c) 2015-<?php echo date("Y");?> 
	developed by  	: andi satya perdana
    Handphone /WA	: +62 812 8836 9981
    Yahoo Messenger	: andi_squire@yahoo.com
    Skype			: andi_squire
-->
<?php 
	$this->load->view('header');
	echo "<h2>".$page_title."</h2>";
	echo anchor('promo/promosponsoringmaster/create','Create Promo');
	if($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>
<table width='99%'>
<?php echo form_open('promo/promosponsoringmaster/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search : <?php 
										//$option = array('namamember'=>'Nama Member','member_id'=>'Member ID'); 
										//echo form_dropdown('option',$option); 
										$data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				  					echo "&nbsp;".form_input($data);
										echo form_submit('submit','go');
										?>   
										<?php if($this->session->userdata('keywords')){ ?>
										<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b>
										<?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='5%'>No.</th>
      <th width='25%'>ID Promo</th>
      <th width='25%'>Title Promo</th>
      <th width='5%'>Start Date</th>
      <th width='12%'>End Date</th>
      <th width='10%'>Free Item</th>      
      <th width='10%'>Berlaku Kelipatan</th>
	  <th width='15%'>Jumlah Recruit</th>
	  <th width='15%'>Sponsor Tupo PV</th>
	  <th width='15%'>New Member Tupo PS PV</th>
	  <th width='15%'>Status</th>
	  <th width='15%'>Created</th>
	  <th width='15%'>Created By</th>
    <th width='8%'>Action</th>
        
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo $counter;?></td>
 	  <td><?php echo $row['id'];?></td>
      <td><?php echo $row['title'];?></td>
      <td><?php echo $row['startdate'];?></td>
      <td><?php echo $row['enddate'];?></td>
      <td><?php echo $row['free_item_name'].'('.$row['free_item_id'].')';?></td>
      <td><?php echo $row['multiples'];?></td>
      <td><?php echo $row['jmlrec'];?></td>
      <td><?php echo $row['ps'];?></td>
      <td><?php echo $row['psrec'];?></td>
      <td><?php echo $row['status'];?></td>
      <td><?php echo $row['created'];?></td>
      <td><?php echo $row['createdby'];?></td>
       <td>
      	<a href="<?php echo site_url()."promo/promosponsoringmaster/view/".$row['id'];?>"><img src="/images/backend/detail.gif" alt="view / edit profile" title="view / edit promo"></a> &nbsp; 
      	<a href="<?php echo site_url()."promo/promoparticipant/".$row['id'];?>"><img src="/images/backend/user.png"  alt="Banned member" title="Peserta Promo"></a>
      </td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<?php echo form_close();?>	

<?php
$this->load->view('footer');
?>