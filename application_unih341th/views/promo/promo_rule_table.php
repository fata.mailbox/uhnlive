<table width='99%'>
<?php echo form_open('promo/promo_rule/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='5%'>No.</th>
      <th width='10%'>Code</th>
      <th width='40%'>Name</th>
      <th width='10%'>Min Order</th>
      <th width='10%'>Max Order</th>
      <th>Exp Date</th>
      <th>Multiples</th>
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;

  if ($row['multiples'] == 1) {
    $m = "Yes";
  }
  else{
    $m = "No";
  }

?>
    <tr>
      <td><?php echo anchor("/promo/promo_rule/edit/".$row['id'],$counter);?></td>
      <td><?php echo anchor("/promo/promo_rule/edit/".$row['id'],$row['item_id']);?></td>
      <td><?php echo anchor("/promo/promo_rule/edit/".$row['id'],$row['name']);?></td>
      <td align="center"><?php echo anchor("/promo/promo_rule/edit/".$row['id'],$row['min_order']);?></td>
		  <td align="center"><?php echo anchor("/promo/promo_rule/edit/".$row['id'],$row['max_order']);?></td>
      <td><?php echo anchor("/promo/promo_rule/edit/".$row['id'],$row['expireddate']);?></td>
      <td><?php echo anchor("/promo/promo_prod/edit/".$row['id'],$m);?></td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
