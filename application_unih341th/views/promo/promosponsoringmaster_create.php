<!--
	Copyright (c) 2015-<?php echo date("Y");?> 
	developed by  	: andi satya perdana
    Handphone /WA	: +62 812 8836 9981
    Yahoo Messenger	: andi_squire@yahoo.com
    Skype			: andi_squire
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('promo/promosponsoringmaster/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		<tr>
			<td width='24%'>date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td valign='top'>Title Promo</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="title" value="<?php echo set_value('title',$row->title);?>" size"30" />
				<span class="error">* <?php echo form_error('title');?></span></td>
		</tr>
        <tr>
			<td valign='top'>Tanggal Awal</td>
			<td valign='top'>:</td>
			<td valign='top'>                
				<?php 
                    $data = array('name'=>'startdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('startdate',date('Y-m-d')));  
                    echo form_input($data);
                    
                ?>
				<span class="error">* <?php echo form_error('startdate');?></span></td>
		</tr>
        <tr>
			<td valign='top'>Tanggal Akhir</td>
			<td valign='top'>:</td>
			<td valign='top'>
            	<?php
                    $data = array('name'=>'enddate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('enddate',date('Y-m-d')));  
                    echo form_input($data);
				?>
				<span class="error">* <?php echo form_error('enddate');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Personal Sales</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="ps" value="<?php echo set_value('ps',$row->ps);?>" size"30" />
				<span class="error">* <?php echo form_error('ps');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Free Item</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="free_item_id" value="<?php echo set_value('free_item_id',$row->free_item_id);?>" size"30" />
				<span class="error">* <?php echo form_error('free_item_id');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Berlaku Kelipatan</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_dropdown('multiples',array(
				'Yes'=>'Yes'
				,'No'=>'No'
				),set_value('multiples',$row->multiples));?>
				<span class="error">* <?php echo form_error('multiples');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Jumlah Rekrut</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="jmlrec" value="<?php echo set_value('jmlrec',$row->jmlrec);?>" size"30" />
				<span class="error">* <?php echo form_error('jmlrec');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Maintain during period?</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_dropdown('maintain',array(
				'Yes'=>'Yes'
				,'No'=>'No'
				),set_value('maintain',$row->maintain));?>
				<span class="error">* <?php echo form_error('maintain');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Personal Sales</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="ps" value="<?php echo set_value('ps',$row->ps);?>" size"30" />
				<span class="error">* <?php echo form_error('ps');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Grade</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('grade',array(
				3=>3
				,4=>4
				,5=>5
				,6=>6
				,7=>7
				,8=>8
				,9=>9
				,10=>10
				,11=>11
				),set_value('grade',$row->grade));?></td> 
		</tr>
		<tr>
			<td valign='top'>Saldo Awal</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="saldo" value="<?php echo set_value('saldo',$row->saldo);?>" size"30" />
				<span class="error">* <?php echo form_error('saldo');?></span></td>
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>
		</table>

<?php echo form_close();?>
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
	Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
</script>

<?php $this->load->view('footer');?>
