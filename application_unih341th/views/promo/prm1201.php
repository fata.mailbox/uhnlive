<?php 
	$this->load->view('header');
	echo "<h2>".$page_title."</h2>";
	if ($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}
	
if($row){
	foreach($row as $data):
		$gm = $data['gm'];
		$dl = $data['dl'];
		$molecool = $data['molecool'];
		$magozai = $data['magozai'];
		$pvg = $data['totalpv'];
		
		$molecoolg = $data['molecool_g6000'];
		$magozaig = $data['magozai_g3000'];
		$maideng = $data['maiden_g2500'];
	endforeach;
}
?>
<table width='100%'>
<?php
	echo form_open($urlform, array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
	if($this->session->userdata('group_id') > 100){ ?>
		<tr>
			<td width='24%'>Member ID / Name</td>
			<td width='1%'>:</td>
			<td width='75%'><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('userid')." / ".$this->session->userdata('name');?></b></td>
		</tr><?php 
	}else{ ?>
		<tr>
			<td width='24%' valign='top'>Member ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='75%'>
				<?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
					echo form_input($data);?> <?php $atts = array(
					  'width'      => '450',
					  'height'     => '600',
					  'scrollbars' => 'yes',
					  'status'     => 'yes',
					  'resizable'  => 'yes',
					  'screenx'    => '0',
					  'screeny'    => '0'
					);
					echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?>
			</td>
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr><?php 
	}?>
		<tr>
			<td>Minimal Position SM (Silver Member)</td>
			<td>:</td>
			<td><?php echo $member['jenjang'];?></td>
		</tr>
		<tr>
			<td>Current Position</td>
			<td>:</td>
			<td><?php echo $member['new_jenjang'];?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><?php echo form_submit('submit','preview');?></td>
		</tr>
<?php echo form_close();?>				
</table>

<!--
<br>
<table>
	<tr>
		<td><b>Minimal Position SM (Silver Member) :</b></td>
		<td align="right"><?php echo $member['jenjang'];?> </td>
		<td>&nbsp&nbsp&nbsp&nbsp&nbsp</td>
		<td><b>Current Position :</b></td>
		<td align="right"><?php echo $member['new_jenjang'];?></td>
	</tr>
	<tr>
		<td><b>Total downline :</b></td>
		<td align="right"><?php //echo number_format($dl)." downline";?> </td>
		<td>&nbsp </td>
		<td><b>SCAP :</b></td>
		<td align="right"><?php echo number_format($magozai)." PV";?></td>
	</tr>
</table>
<br>
-->
<p>
Achievement
<table class="stripe">
	<tr>
      <th><div align = "center">Desc</div></th>
      <th><div align = "right">Nov'12</div></th>
      <th><div align = "right">Dec'12</div></th>
      <th><div align = "right">Jan'13</div></th>
	  <th><div align = "right">Feb'13</div></th>
      <th><div align = "right">Mar'13</div></th>
      <th><div align = "right">Apr'13</div></th>
    </tr>
	<tr>
      <td><div align = "center">3SM @3jt</div></td>
      <td><div align = "right"><?php echo number_format($member['q1']);?></div></td>
      <td><div align = "right"><?php echo number_format($member['q2']);?></div></td>
      <td><div align = "right"><?php echo number_format($member['q3']);?></div></td>
	  <td><div align = "right"><?php echo number_format($member['q4']);?></div></td>
      <td><div align = "right"><?php echo number_format($member['q5']);?></div></td>
      <td><div align = "right"><?php echo number_format($member['q6']);?></div></td>
    </tr>
	<tr>
      <td><div align = "center">Personal Sales</div></td>
      <td><div align = "right"><?php echo number_format($member['pv1']);?></div></td>
      <td><div align = "right"><?php echo number_format($member['pv2']);?></div></td>
      <td><div align = "right"><?php echo number_format($member['pv3']);?></div></td>
	  <td><div align = "right"><?php echo number_format($member['pv4']);?></div></td>
      <td><div align = "right"><?php echo number_format($member['pv5']);?></div></td>
      <td><div align = "right"><?php echo number_format($member['pv6']);?></div></td>
    </tr>
</table>

<p><br>
Downline
<table class="stripe">
	<tr>
      <th><div align = "right">No.</div></th>
      <th><div align = "center">ID Downline</div></th>
      <th><div align = "left">Nama Downline</div></th>
      <th><div align="right">PS1</div></th><th><div align="right">PVG1</div></th>
	  <th><div align="right">PS2</div></th><th><div align="right">PVG2</div></th>
	  <th><div align="right">PS3</div></th><th><div align="right">PVG3</div></th>
	  <th><div align="right">PS4</div></th><th><div align="right">PVG4</div></th>
	  <th><div align="right">PS5</div></th><th><div align="right">PVG5</div></th>
	  <th><div align="right">PS6</div></th><th><div align="right">PVG6</div></th>
    </tr

<?php
if ($dl): 
	foreach($dl as $key => $row): ?>
    <tr>
		<td align="right"><?php echo $row['i'];?></td>
		<td align="center"><?php echo $row['id'];?></td>
		<td align="left"><?php echo $row['nama'];?></td>
		<td align="right"><b><?php echo number_format($row['ps1']);?></b></td><td align="right"><b><?php echo number_format($row['pvg1']);?></b></td>
		<td align="right"><b><?php echo number_format($row['ps2']);?></b></td><td align="right"><b><?php echo number_format($row['pvg2']);?></b></td>
		<td align="right"><b><?php echo number_format($row['ps3']);?></b></td><td align="right"><b><?php echo number_format($row['pvg3']);?></b></td>
		<td align="right"><b><?php echo number_format($row['ps4']);?></b></td><td align="right"><b><?php echo number_format($row['pvg4']);?></b></td>
		<td align="right"><b><?php echo number_format($row['ps5']);?></b></td><td align="right"><b><?php echo number_format($row['pvg5']);?></b></td>
		<td align="right"><b><?php echo number_format($row['ps6']);?></b></td><td align="right"><b><?php echo number_format($row['pvg6']);?></b></td>
    </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
      <td colspan="4">Data is not available.</td>
    </tr>
<?php endif; ?> 
</table><p>
<em>*) Hasil berikut ini adalah hasil perhitungan sementara</em>
<?php
$this->load->view('footer');
?>