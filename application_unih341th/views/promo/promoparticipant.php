<!--
	Copyright (c) 2015-<?php echo date("Y");?> 
	developed by  	: andi satya perdana
    Handphone /WA	: +62 812 8836 9981
    Yahoo Messenger	: andi_squire@yahoo.com
    Skype			: andi_squire
-->
<?php 
	$this->load->view('header');
	echo "<h2>".$page_title."</h2>";
	//echo anchor('promo/promoparticipant/create','Create Promo');
	if($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>
<table width='99%'>
<?php echo form_open('promo/promoparticipant/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
    	<td colspan="100%">
        Promo Title : 
		<?php 
		$getPromo = array();
		//var_dump($promolist);
		$getPromo[0]= '-- Pilih Promo --';
		foreach($promolist as $dataPromo):
			$getPromo[$dataPromo['id']] = $dataPromo['title'];
		endforeach;
		//var_dump($getPromo);

		echo form_dropdown('promoid',$getPromo,set_value('promoid',$row->promoid));
		echo form_submit('submit','go');
		?>
		</td>
    </tr>
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search : <?php 
										//$option = array('namamember'=>'Nama Member','member_id'=>'Member ID'); 
										//echo form_dropdown('option',$option); 
										$data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				  					echo "&nbsp;".form_input($data);
										echo form_submit('submit','go');
										?>   
										<?php if($this->session->userdata('keywords')){ ?>
										<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b>
										<?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='5%'>No.</th>
      <th width='10%'>Member ID</th>
      <th width='15%'>Nama Member</th>
      <th width='10%'>Personal Sales PV</th>
      <th width='5%'>Jumlah Rekrut</th>
      <th width='5%'>Qualified Recruit bulan 1</th>
      <th width='5%'>Qualified Recruit bulan 2</th>
      <th width='5%'>Qualified Recruit bulan 3</th>
      <th width='10%'>Result</th>
    <th width='8%'>Action</th>
        
    </tr>
<?php
//if (isset($results)):
//echo $countResults;
if ($countResults>0):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo $counter;?></td>
 	  <td><?php echo $row['member_id'];?></td>
      <td><?php echo $row['member_name'];?></td>
      <td><?php echo $row['ps'];?></td>
      <td><?php echo $row['jmlrec'];?></td>
      <td> - </td>
      <td> - </td>
      <td> - </td>
      <td><?php echo $row['result'];?></td>
       <td>
      	<a href="<?php echo site_url()."promo/promoparticipant/detail/".$row['id'];?>"><img src="/images/backend/detail.gif" alt="view detail" title="view detail"></a> &nbsp; 
      </td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="100%" align="center">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php
$this->load->view('footer');
?>