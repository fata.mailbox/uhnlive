<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php
if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}
if($cekdata){
	foreach($getData as $keyData => $rowData) :
		$whs1_id = $rowData['warehouse_id']; 
		$whs1_name = $rowData['warehouse_name']; 
	endforeach;
}else{
	$whs1_id = 0;
	$whs1_name = '';
}
?>
	 <?php echo form_open('inv/rowhtrf/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
	
	<table width='55%'>
		<tr>
			<td width='24%'>date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td valign='top'>RO ID</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'ro_id','id'=>'ro_id','size'=>'8','value'=>set_value('ro_id')); echo form_input($data);?></td> 
		</tr>
		<tr>
			<td valign='top'>Warehouse Asal</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'whs1_name','id'=>'whs1_name','readonly'=>'1','size'=>'8','value'=>$whs1_name); echo form_input($data);echo form_hidden('whs1',$whs1_id);?></td> 
		</tr>
		
		<tr>
			<td valign='top'>Warehouse Tujuan</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('whs2',$warehouse);?></td> 
		</tr>
		<tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?></td> 
		</tr>
		
		</table>
		<table width='85%'>	
		<?php 
		if($cekdata){
		?>
        <tr><td colspan='3'>&nbsp;</td></tr>
		<tr>
			<td width='20%'>Item Code</td>
			<td width='35%'>Item Name</td>
			<td width='5%'>Qty</td>
			<td width='10%'>Qty WH Tujuan</td>
			<td width='15%'>Status</td>
		</tr>
        <?php 
		 	$notok = 0;
			//echo $notok;
			foreach($getDataDetail as $keyDataDetail => $rowDataDetail) :
        ?>
        <tr>
			<td width='20%'><?php echo  $rowDataDetail['item_id']; ?></td>
			<td width='35%'><?php echo  $rowDataDetail['item_name']; ?></td>
			<td width='5%'><?php echo  $rowDataDetail['qty']; ?></td>
			<td width='10%'><?php echo  $rowDataDetail['qty_stock_wh']; ?></td>
			<td width='15%'><?php 
				if($rowDataDetail['qty_stock_wh'] >= $rowDataDetail['qty']) {
					echo 'OK'; 
				}else{
					echo 'NOT OK';
					$notok = $notok + 1;
				}
				?>
             </td>
		</tr>
		<?php endforeach; ?>
        <?php } ?>
		<?php 
		if($getDataBns){
		?>
        <tr><td colspan='3'>&nbsp;<?php echo form_hidden('bns','1'); ?></td></tr>
        <tr><td colspan='3'><b>Support Item For Free</b></td></tr>
		<tr>
			<td width='20%'>Item Code</td>
			<td width='35%'>Item Name</td>
			<td width='5%'>Qty</td>
			<td width='10%'>Qty WH Tujuan</td>
			<td width='15%'>Status</td>
		</tr>
        <?php 
			//echo $notok;
			foreach($getDataBns as $keyDataBns => $rowDataBns) :
        ?>
        <tr>
			<td width='20%'><?php echo  $rowDataBns['item_id']; ?></td>
			<td width='35%'><?php echo  $rowDataBns['prd']; ?></td>
			<td width='5%'><?php echo  $rowDataBns['qty']; ?></td>
			<td width='10%'><?php echo  $rowDataBns['qty_stock_wh']; ?></td>
			<td width='15%'><?php 
				if($rowDataBns['qty_stock_wh'] >= $rowDataBns['qty']) {
					echo 'OK'; 
				}else{
					echo 'NOT OK';
					$notok = $notok + 1;
				}
				?>
             </td>
		</tr>
		<?php endforeach; ?>
        <?php } else { ?>
        <tr><td colspan='3'>&nbsp;<?php echo form_hidden('bns','0'); ?></td></tr>
        <?php } ?>
        <tr><td colspan='3'>&nbsp;</td></tr>
		<tr><td colspan='3'><?php if(!$cekdata) {
									echo form_submit('submit', 'Cek Data');
								  }else{
									if($whs1_id != $this->input->post('whs2')){
										if($notok > 0){
											echo 'Terdapat Qty Pada Warehouse Tujuan yang tidak mencukupi <br>';
											echo anchor('inv/rowhtrf','&laquo; Kembali').' &nbsp;';
											echo form_submit('submit', 'Cek Data');
											
										}else{
											echo form_submit('submit', 'Cek Data').' &nbsp;';
											echo form_submit('submit', 'Submit');
										}
									}else{
											echo 'Warehouse asal dan tujuan tidak boleh sama <br>';
											echo anchor('inv/rowhtrf','&laquo; Kembali').' &nbsp;';
											echo form_submit('submit', 'Cek Data');
									}
								  }
							?></td></tr>
		
		</table>
<?php echo form_close();?>

<?php $this->load->view('footer');?>
