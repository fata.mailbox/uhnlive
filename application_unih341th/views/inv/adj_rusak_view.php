<!--
	Copyright (c) 20018-<?php echo date("Y");?> 
    	contact person	: Annisa Rahmawaty
        Handphone	: +62 857 23439567
-->
<?php $this->load->view('header');?>

<div class="ViewHold">
	<div id="companyDetails">
		<?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?>
		
	</div>

	<p>
		<strong>
			<?php echo "No. Adjustment Rusak: ";?> <?php echo $row['id'];?><br />
			<?php
				echo $row['date']; // localized month
			?>
		</strong>		
	</p>
<h2><?php echo $page_title;?></h2>
	<hr />

	<h3><?php echo $row['name'];?></h3>

	<p>
		<?php
				echo "Adjustment: <b>".$row['flag']."</b><br />";
				echo "Remark: ".$row['remark']."<br />";
				echo "User ID: ".$row['createdby'];
		?>
	</p>

	<table class="stripe">
	<tr>
      <th width='10%'>No.</th>
      <th width='20%'>Item Code</th>
      <th width='50%'>Item Name</th>
      <th width='20%'>Qty</th>      
    </tr>
    
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><p><?php echo $counter;?></p></td>
			<td><?php echo $item['item_id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td><?php echo $item['qty'];?></td>
		</tr>
		<?php endforeach;?>
	</table>
	
<?php
$this->load->view('footer');
?>
