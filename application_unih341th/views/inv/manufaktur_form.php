<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('inv/mnf/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table width='65%'>
		<tr>
			<td valign='top'>ID BoM</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $data = array('name'=>'itemcode','id'=>'itemcode','size'=>'10','readonly'=>'1','value'=>set_value('itemcode')); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('search/invsearch/bom', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
					<span class="error">* <?=form_error('itemcode'); ?></span>
			</td>
		</tr>			
		<tr>
			<td valign='top'>Nama BoM</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $data = array('name'=>'itemname','id'=>'itemname','readonly'=>'1','value'=>set_value('itemname')); echo form_input($data);?>
			</td>
		</tr>
        </table>
		<hr />
		<table width='65%'>	
		<tr>
			<td width='30%'>Kode Barang</td>
			<td width='45%'>Nama Barang</td>
			<td width='20%'>Qty</td>
			<td width='5%'>Del?</td>
		</tr>
        <?php 
$key=0;

while($key < $counti){?>

<tr>
			<td valign='top'><?php echo form_hidden('counter[]',$key); $data = array('name'=>'itemcode'.$key,'id'=>'itemcode'.$key,'size'=>'8','value'=>set_value('itemcode'.$key),'readonly'=>'1'); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            ); 

					echo anchor_popup('search/invsearch/index/'.$key, '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<td valign='top'><input type="text" name="itemname<?=$key;?>" id="itemname<?=$key;?>" value="<?=set_value('itemname'.$key);?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty<?=$key;?>" id="qty<?=$key;?>" value="<?=set_value('qty'.$key);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode<?=$key;?>,document.form.itemname<?=$key;?>,document.form.qty<?=$key;?>);" src="<?= base_url();?>images/backend/delete.png" border="0"/></td>
</tr>

<?php $key++; }?>


<tr>
        <td colspan="3">
        	Add <input name="rowx" type="text" id="rowx" value="<?=set_value('rowx','1');?>" size="1" maxlength="2" />
          Row(s)<?php echo form_submit('action', 'Go');?>
</td>
        </tr>
		<tr><td colspan='3'><?php echo form_submit('action', 'Submit');?></td></tr>
		
		</table>
		
		<?php echo form_close();?>

<?php $this->load->view('footer');?>
