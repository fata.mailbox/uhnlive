<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php 
echo anchor('inv/rowhtrf/create','create RO warehouse transfer');
if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
						
?>
<table width='99%'>
<?php echo form_open('inv/rowhtrf/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='60%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='40%' align='right'>search by: <?php if($this->session->userdata('group_id') < 100 && $this->session->userdata('group_id')!= 28) echo form_dropdown('whsid',$warehouse);?> <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?><?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>
<?php echo form_open('inv/rowhtrf/approved/', array('id' => 'my_form2', 'name' => 'my_form2', 'autocomplete' => 'off'));?>
<table class="stripe">
	<!--<tr>
		<td colspan='2' valign='top'>remark: </td>
		<td colspan='9'><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
			echo form_textarea($data);?><br>
			<?php echo form_submit('submit','approved');?>
		</td>
	</tr>
    -->
    <tr>
      <!--<th width='3%'>&nbsp;</th>-->
      <th width='2%'>No.</th>
      <th width='5%'>Trf ID</th>
      <th width='5%'>RO ID</th>
      <th width='10%'>Date</th>
      <th width='20%'>Warehouse Asal</th>
      <th width='20%'>Warehouse Tujuan</th>
      <!--<th width='5%'>Adj ?</th>-->
      <th width='25%'>Remark</th>
      <!--<th width='11%'><div align="right">App Time</div></th>-->
      <th width='10%'>User ID</th>
      <!--<th width='8%'>Processed By</th>-->
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
    	<!--<td>
			<?php /*if($row['status'] == 'pending'){ 
				$data = array(
				'name'        => 'p_id[]',
				'id'          => 'p_id[]',
				'value'       => $row['id'],
				'checked'     => false,
				'style'       => 'border:none'
				);
				echo form_checkbox($data); } else {?>&nbsp;<?php }*/?> </td>-->
      <td><?php echo anchor('inv/rowhtrf/view/'.$row['id'], $counter);?></td>
      <td><?php echo anchor('inv/rowhtrf/view/'.$row['id'], $row['id']);?></td>
      <td><?php echo anchor('inv/rowhtrf/view/'.$row['id'], $row['ro_id']);?></td>
      <td><?php echo anchor('inv/rowhtrf/view/'.$row['id'], $row['date']);?></td>
      <td><?php echo anchor('inv/rowhtrf/view/'.$row['id'], $row['warehouse1_name']);?></td>
      <td><?php echo anchor('inv/rowhtrf/view/'.$row['id'], $row['warehouse2_name']);?></td>
      <!--<td><?php //echo anchor('inv/adj/view/'.$row['id'], $row['flag']);?></td>-->
      <td><?php echo anchor('inv/rowhtrf/view/'.$row['id'], $row['remark']." ");?></td>
	  <!--<td align="right"><?php //echo anchor('inv/adj/view/'.$row['id'], $row['appdate']);?></td>-->
      <td><?php echo anchor('inv/rowhtrf/view/'.$row['id'], $row['createdby']);?></td>
      <!--<td><?php //echo anchor('inv/adj/view/'.$row['id'], $row['approvedby']);?></td>-->
    </tr>
    <?php endforeach; 
  else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php echo form_close();?>				

<?php
$this->load->view('footer');
?>