<!--
	Copyright (c) 2018-<?php echo date("Y");?> 
    	contact person	: Annisa R
        Handphone	: +62 85723439567
    	gmail		: annisarahmawaty50@gmail.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('inv/adj_rusak/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
	
	<table width='55%'>
		<tr>
			<td width='24%'>date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td valign='top'>Adjustment ?</td>
			<td valign='top'>:</td>
			<td><input type="radio" id="flag" name="flag" value="Minus" <?php echo set_radio('flag', 'Minus', TRUE); ?> /> Minus (-) 
				<input type="radio" name="flag" value="Plus" <?php echo set_radio('flag', 'Plus'); ?> /> Plus (+)</td> 
		</tr>
		<tr>
			<td><input type="hidden" name=""></td>
			<td></td>
		    <td>
		      <select id="pilih" name="noted">
		        <option value="dihancurkan">Hancurkan</option>    
		        <option value="rusak">Rusak</option>  
		    </select></td>
		  </tr>
		
		<tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td>
				<?php echo form_dropdown('id_whr', $warehouse, '', 'id ="idRusak"');?></td>
			
		</tr>
		<tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?></td> 
		</tr>
		
		</table>
		
		<table width='65%'>	
		<tr>
			<td width='30%'>Item Code</td>
			<td width='45%'>Item Name</td>
			<td width='20%'>Qty</td>
			<td width='5%'>Del?</td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode0','id'=>'itemcode0','size'=>'8','readonly'=>'1','value'=>set_value('itemcode0')); echo form_input($data);?>
			<input class="button" type="button" name="Button" value="browse" onclick="handleClick()">
			<td valign='top'><input type="text" name="itemname0" id="itemname0" value="<?php echo set_value('itemname0');?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty0" id="qty0" value="<?php echo set_value('qty0',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode0,document.form.itemname0,document.form.qty0);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode1','id'=>'itemcode1','size'=>'8','readonly'=>'1','value'=>set_value('itemcode1')); echo form_input($data);?>
			<input class="button" type="button" name="Button" value="browse" onclick="handleClick1()">
			<td valign='top'><input type="text" name="itemname1" id="itemname1" value="<?php echo set_value('itemname1');?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty1" id="qty1" value="<?php echo set_value('qty1',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode1,document.form.itemname1,document.form.qty1);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode2','id'=>'itemcode2','size'=>'8','readonly'=>'1','value'=>set_value('itemcode2')); echo form_input($data);?>
			<input class="button" type="button" name="Button" value="browse" onclick="handleClick2()">
			<td valign='top'><input type="text" name="itemname2" id="itemname2" value="<?php echo set_value('itemname2');?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty2" id="qty2" value="<?php echo set_value('qty2',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode2,document.form.itemname2,document.form.qty2);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode3','id'=>'itemcode3','size'=>'8','readonly'=>'1','value'=>set_value('itemcode3')); echo form_input($data);?>
			<input class="button" type="button" name="Button" value="browse" onclick="handleClick3()">
			<td valign='top'><input type="text" name="itemname3" id="itemname3" value="<?php echo set_value('itemname3');?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty3" id="qty3" value="<?php echo set_value('qty3',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode3,document.form.itemname3,document.form.qty3);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode4','id'=>'itemcode4','size'=>'8','readonly'=>'1','value'=>set_value('itemcode4')); echo form_input($data);?>
			<input class="button" type="button" name="Button" value="browse" onclick="handleClick4()">
			<td valign='top'><input type="text" name="itemname4" id="itemname4" value="<?php echo set_value('itemname4');?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="qty4" id="qty4" value="<?php echo set_value('qty4',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext(document.form.itemcode4,document.form.itemname4,document.form.qty4);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td colspan='3' valign='top'>Password : <input type="password" name="password1" id="password1" value="" maxlength="50" size="13" /> 
			<span class="error">* <?php echo form_error('password1');?></span></td>
		</tr>
		<tr><td colspan='3'><?php echo form_submit('submit', 'Submit');?></td></tr>
		
		</table>
<?php echo form_close();?>

<?php $this->load->view('footer');?>

<script type="text/javascript">
	$(function () {
        $("#addd").change(function () {
            if ($(this).val() == "baik") {
              $("#rusak").hide();
              $("#baik").show();
            } else if ($(this).val() == "rusak"){
              $("#rusak").show();
              $("#baik").hide();
            }
        });
    });


    $(function() {
	    $('input[name=flag]').on('click init-flag', function() {
	        $('#pilih').toggle($('#flag').prop('checked'));
	    }).trigger('init-flag');
	});

	function handleClick() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/stock_rusak/0/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}

	function handleClick1() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/stock_rusak/1/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}
	function handleClick2() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/stock_rusak/2/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}
	function handleClick3() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/stock_rusak/3/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}
	function handleClick4() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/stock_rusak/4/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}
	    
</script>