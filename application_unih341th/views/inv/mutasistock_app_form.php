<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('inv/mtsapp/app/'.$row->id, array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
	
	<table width='55%'>
		<tr>
			<td width='24%'>date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td valign='top'>remark approved</td>
			<td valign='top'>:</td>
			<td><?php echo form_hidden('id',$row->id); echo form_hidden('warehouse_id2',$row->warehouse_id2); echo form_hidden('warehouse_id1',$row->warehouse_id1); $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?> <span class="error"><?php echo form_error('remark'); ?></span></td> 
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td></tr>
		</table>

		
<table class="stripe">
	<tr>
      <th width='10%'>No.</th>
      <th width='20%'>Item Code</th>
      <th width='50%'>Item Name</th>
      <th width='20%'>Qty</th>      
    </tr>
    
    <?php $counter =0; foreach ($items as $row): $counter = $counter+1;?>
		<tr>
			<td><?php echo $counter;?></td>
			<td><?php echo $row['item_id'];?></td>
			<td><?php echo $row['name'];?></td>
			<td><?php echo $row['fqty'];?></td>
		</tr>
		<?php endforeach;?>
		
	</table>
<?php echo form_close();?>
<?php $this->load->view('footer');?>
