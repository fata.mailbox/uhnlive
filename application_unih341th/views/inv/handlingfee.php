<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<table width='100%'>
<?php echo form_open('inv/handlingfee/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
<?php if($this->session->userdata('whsid') == '1'){ ?>
  <tr>
    	<td>Warehouse</td>
		<td> : <?php echo form_dropdown('whsid',$warehouse);?></td>
  </tr>
  <?php }else echo form_hidden('whsid',$this->session->userdata('whsid'));?>
	<tr>
    	<td>Transaksi</td>
		<td> : <?php echo form_dropdown('trxtype',$trxtype);?></td>
  </tr>
    <tr>
    	<td>Tanggal</td>
		<td> : <?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',$reportDate));
   echo form_input($data);?>
   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',$reportDate));
   echo "to: ".form_input($data);?>
   </td>
  </tr>

	<tr>
    	<td>&nbsp;</td>
		<td>&nbsp;&nbsp;<?php echo form_submit('submit','preview');?></td>
	</tr>
				  
<?php echo form_close();?>				
</table>
<!-- RESULT -->
<table class="stripe">
    <tr>
      <th width='5%'>No.</th>
      <th width='8%'>Invoice No.</th>
      <th width='10%'>Tanggal</th>
      <th width='10%'>Stc No.</th>
      <th width='15%'>Nama Stc</th>
      <th width='10%'>SKU No</th>
      <th width='20%'>SKU name</th>
      <th width='10%'>Item ID</th>
      <th width='20%'>Item Name</th>
      <th width='10%'>Qty</th>
      <th width='20%'>Remark</th>
     </tr>
<?php
if ($results):
	$counter = 0; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo $counter;?></td>
      <td><?php echo $row['inv_no'];?></td>
      <td><?php echo $row['inv_date'];?></td>
      <td><?php echo $row['stc_id'];?></td>
      <td><?php echo $row['stc_name'];?></td>
      <td><?php echo $row['sku_id'];?></td>
      <td><?php echo $row['sku_name'];?></td>
      <td><?php echo $row['item_id'];?></td>
      <td><?php echo $row['item_name'];?></td>
      <td><?php echo $row['qty'];?></td>
      <td><?php echo $row['remark'];?></td>
     </tr>
    <?php endforeach; 
else: ?>
    <tr>
      <td colspan="100%" align="center">Data is not available.</td>
    </tr>
<?php 
endif; ?>    
</table>
<!-- EOF RESULT -->
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>

<?php 
$this->load->view('footer');
?>