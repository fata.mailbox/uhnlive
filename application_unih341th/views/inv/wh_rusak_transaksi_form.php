
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('inv/wh_rusak_mov/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
	
	<table width='55%'>
		<tr>
			<td width='24%'>date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td>
				<?php echo form_dropdown('id_whr', $warehouse, '', 'id ="idRusak"');?></td>
			
		</tr>
		<tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?></td> 
		</tr>
		<tr><td>
			<p id="coba"></p></td></tr>
		
		</table>
		
		<table width='85%' border="0">	
		<tr>
			<td width='35%'>Item Code</td>
			<td width='45%'>Item Name</td>
			<td width='20%'>Price</td>
			<td width='20%'>Qty</td>
			<td width='20%'>Total Price</td>
			<td width='5%'>Del?</td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode0','id'=>'itemcode0','size'=>'8','readonly'=>'1','value'=>set_value('itemcode0')); echo form_input($data);?>
								
					<input class="button" type="button" name="Button" value="browse" onclick="handleClick()">
			<td valign='top'>
				<input type="text" name="itemname0" id="itemname0" value="<?php echo set_value('itemname0');?>" readonly="1" size="30" />
			</td>
			<td>
				<input class='textbold' type="text" name="price0" id="price0" value="<?php echo set_value('price0',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);">
			</td>
			<td>
				<input type="hidden" name="maks0" id="maks0" value="<?php echo set_value('qty0',0);?>">
				<input class='textbold' type="text" name="qty0" id="qty0" value="<?php echo set_value('qty0',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);subtotalMutasi(this,document.form.price0,document.form.tprice0);totalMutasi();">
			</td>
			<td>
				<input class='textbold' type="text" name="tprice0" id="tprice0" value="<?php echo set_value('tprice0',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);">
			</td>
			<td>
				<img alt="delete" onclick="cleartext2(document.form.itemcode0,document.form.itemname0,document.form.qty0,document.form.price0,document.form.tprice0);" src="<?php echo base_url();?>images/backend/delete.png" border="0"/>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode1','id'=>'itemcode1','size'=>'8','readonly'=>'1','value'=>set_value('itemcode1')); echo form_input($data);?>
		

					<input class="button" type="button" name="Button" value="browse" onclick="handleClick1()">
			<td valign='top'>
				<input type="text" name="itemname1" id="itemname1" value="<?php echo set_value('itemname1');?>" readonly="1" size="30" />
			</td>
			<td>
				<input class='textbold' type="text" name="price1" id="price1" value="<?php echo set_value('price1',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);">
			</td>
			<td>
				<input type="hidden" name="maks1" id="maks1" value="<?php echo set_value('qty1',0);?>">
				<input class='textbold' type="text" name="qty1" id="qty1" value="<?php echo set_value('qty1',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);subtotalMutasi(this,document.form.price1,document.form.tprice1);totalMutasi();">
			</td>
			<td>
				<input class='textbold' type="text" name="tprice1" id="tprice1" value="<?php echo set_value('tprice1',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);">
			</td>
			<td>
				<img alt="delete" onclick="cleartext2(document.form.itemcode1,document.form.itemname1,document.form.qty1,document.form.price1,document.form.tprice1);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/>
			</td>
		</tr>
		
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode2','id'=>'itemcode2','size'=>'8','readonly'=>'1','value'=>set_value('itemcode2')); echo form_input($data);?>
			
					<input class="button" type="button" name="Button" value="browse" onclick="handleClick2()">
			<td valign='top'>
				<input type="text" name="itemname2" id="itemname2" value="<?php echo set_value('itemname2');?>" readonly="1" size="30" />
			</td>
			<td>
				<input class='textbold' type="text" name="price2" id="price2" value="<?php echo set_value('price2',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);">
			</td>
			<td>
				<input type="hidden" name="maks2" id="maks2" value="<?php echo set_value('qty2',0);?>">
				<input class='textbold' type="text" name="qty2" id="qty2" value="<?php echo set_value('qty2',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);subtotalMutasi(this,document.form.price2,document.form.tprice2);totalMutasi();">
			</td>
			<td>
				<input class='textbold' type="text" name="tprice2" id="tprice2" value="<?php echo set_value('tprice2',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);">
			</td>
			<td>
				<img alt="delete" onclick="cleartext2(document.form.itemcode2,document.form.itemname2,document.form.qty2,document.form.price2,document.form.tprice2);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode3','id'=>'itemcode3','size'=>'8','readonly'=>'1','value'=>set_value('itemcode3')); echo form_input($data);?>
		

					<input class="button" type="button" name="Button" value="browse" onclick="handleClick3()">
			<td valign='top'><input type="text" name="itemname3" id="itemname3" value="<?php echo set_value('itemname3');?>" readonly="1" size="30" /></td>
			<td>
				<input class='textbold' type="text" name="price3" id="price3" value="<?php echo set_value('price3',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);">
			</td>
			<td>
				<input type="hidden" name="maks3" id="maks3" value="<?php echo set_value('qty3',0);?>">
				<input class='textbold' type="text" name="qty3" id="qty3" value="<?php echo set_value('qty3',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);subtotalMutasi(this,document.form.price3,document.form.tprice3);totalMutasi();">
			</td>
			<td>
				<input class='textbold' type="text" name="tprice3" id="tprice3" value="<?php echo set_value('tprice3',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);">
			</td>
			<td>
				<img alt="delete" onclick="cleartext2(document.form.itemcode3,document.form.itemname3,document.form.qty3,document.form.price3,document.form.tprice3);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/>
			</td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode4','id'=>'itemcode4','size'=>'8','readonly'=>'1','value'=>set_value('itemcode4')); echo form_input($data);?>
			

					<input class="button" type="button" name="Button" value="browse" onclick="handleClick4()">
			<td valign='top'>
				<input type="text" name="itemname4" id="itemname4" value="<?php echo set_value('itemname4');?>" readonly="1" size="30" />
			</td>
			<td>
				<input class='textbold' type="text" name="price4" id="price4" value="<?php echo set_value('price4',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);">
			</td>
			<td>
				<input type="hidden" name="maks4" id="maks4" value="<?php echo set_value('qty4',0);?>">
				<input class='textbold' type="text" name="qty4" id="qty4" value="<?php echo set_value('qty4',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);subtotalMutasi(this,document.form.price4,document.form.tprice4);totalMutasi();"></td>
			<td>
				<input class='textbold' type="text" name="tprice4" id="tprice4" value="<?php echo set_value('tprice4',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);">
			</td>
			<td>
				<img alt="delete" onclick="cleartext2(document.form.itemcode4,document.form.itemname4,document.form.qty4,document.form.price4,document.form.tprice4);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/>
			</td>
		</tr>
		
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode5','id'=>'itemcode5','size'=>'8','readonly'=>'1','value'=>set_value('itemcode5')); echo form_input($data);?>
		

					<input class="button" type="button" name="Button" value="browse" onclick="handleClick5()">
			<td valign='top'><input type="text" name="itemname5" id="itemname5" value="<?php echo set_value('itemname5');?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="price5" id="price5" value="<?php echo set_value('price5',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td>
				<input type="hidden" name="maks5" id="maks5" value="<?php echo set_value('qty5',0);?>">
				<input class='textbold' type="text" name="qty5" id="qty5" value="<?php echo set_value('qty5',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);subtotalMutasi(this,document.form.price5,document.form.tprice5);totalMutasi();"></td>
			<td><input class='textbold' type="text" name="tprice5" id="tprice5" value="<?php echo set_value('tprice5',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext2(document.form.itemcode5,document.form.itemname5,document.form.qty5,document.form.price5,document.form.tprice5);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode6','id'=>'itemcode6','size'=>'8','readonly'=>'1','value'=>set_value('itemcode6')); echo form_input($data);?>
			

					<input class="button" type="button" name="Button" value="browse" onclick="handleClick6()">
			<td valign='top'><input type="text" name="itemname6" id="itemname6" value="<?php echo set_value('itemname6');?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="price6" id="price6" value="<?php echo set_value('price6',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td>
				<input type="hidden" name="maks6" id="maks6" value="<?php echo set_value('qty6',0);?>">
				<input class='textbold' type="text" name="qty6" id="qty6" value="<?php echo set_value('qty6',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);subtotalMutasi(this,document.form.price6,document.form.tprice6);totalMutasi();"></td>
			<td><input class='textbold' type="text" name="tprice6" id="tprice6" value="<?php echo set_value('tprice6',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext2(document.form.itemcode6,document.form.itemname6,document.form.qty6,document.form.price6,document.form.tprice6);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode7','id'=>'itemcode7','size'=>'8','readonly'=>'1','value'=>set_value('itemcode7')); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );?>

					<input class="button" type="button" name="Button" value="browse" onclick="handleClick7()">
			<td valign='top'><input type="text" name="itemname7" id="itemname7" value="<?php echo set_value('itemname7');?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="price7" id="price7" value="<?php echo set_value('price7',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td>
				<input type="hidden" name="maks7" id="maks7" value="<?php echo set_value('qty7',0);?>">
				<input class='textbold' type="text" name="qty7" id="qty7" value="<?php echo set_value('qty7',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);subtotalMutasi(this,document.form.price7,document.form.tprice7);totalMutasi();"></td>
			<td><input class='textbold' type="text" name="tprice7" id="tprice7" value="<?php echo set_value('tprice7',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext2(document.form.itemcode7,document.form.itemname7,document.form.qty7,document.form.price7,document.form.tprice7);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode8','id'=>'itemcode8','size'=>'8','readonly'=>'1','value'=>set_value('itemcode8')); echo form_input($data);?>
			

					<input class="button" type="button" name="Button" value="browse" onclick="handleClick8()">
			<td valign='top'>
				<input type="text" name="itemname8" id="itemname8" value="<?php echo set_value('itemname8');?>" readonly="1" size="30" /></td>
			<td><input class='textbold' type="text" name="price8" id="price8" value="<?php echo set_value('price8',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td>
				<input type="hidden" name="maks8" id="maks8" value="<?php echo set_value('qty8',0);?>">
				<input class='textbold' type="text" name="qty8" id="qty8" value="<?php echo set_value('qty8',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);subtotalMutasi(this,document.form.price8,document.form.tprice8);totalMutasi();"></td>
			<td><input class='textbold' type="text" name="tprice8" id="tprice8" value="<?php echo set_value('tprice8',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td><img alt="delete" onclick="cleartext2(document.form.itemcode8,document.form.itemname8,document.form.qty8,document.form.price8,document.form.tprice8);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode9','id'=>'itemcode9','size'=>'8','readonly'=>'1','value'=>set_value('itemcode9')); echo form_input($data);?>
			

					<input class="button" type="button" name="Button" value="browse" onclick="handleClick9()">
			<td valign='top'>
				<input type="text" name="itemname9" id="itemname9" value="<?php echo set_value('itemname9');?>" readonly="1" size="30" />
			</td>
			<td>
				<input class='textbold' type="text" name="price9" id="price9" value="<?php echo set_value('price9',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);">
			</td>
			<td>
				<input type="hidden" name="maks9" id="maks9" value="<?php echo set_value('qty9',0);?>">
				<input class='textbold' type="text" name="qty9" id="qty9" value="<?php echo set_value('qty9',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);subtotalMutasi(this,document.form.price9,document.form.tprice9);totalMutasi();"></td>
			<td>
				<input class='textbold' type="text" name="tprice9" id="tprice9" value="<?php echo set_value('tprice9',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td>
				<img alt="delete" onclick="cleartext2(document.form.itemcode9,document.form.itemname9,document.form.qty9,document.form.price9,document.form.tprice9);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/>
			</td>
		</tr>
        <tr><td colspan="100%"><hr /></td></tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="left">Grand Total</td>
			<td><input class='textbold' type="text" name="totalprice" id="totalprice" value="<?php echo set_value('totalprice',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
			<td width='5%'></td>
        </tr>
        <tr><td colspan="100%"><hr /></td></tr>
		<tr>
			<td colspan='3' valign='top'>Password : <input type="password" name="password1" id="password1" value="" maxlength="50" size="13" /> 
			<span class="error">* <?php echo form_error('password1');?></span></td>
		</tr>
		<tr>
			<input type="hidden" name="cek" id="cek" value="" />
			<!--<span class="error"><?php // if(validation_errors() or form_error('qty')){echo form_error('qty');} ?></span>-->
			<td colspan='3'>
			<?php 
			$js = 'onClick="return cekQty();"';
			echo form_submit('submit', 'Submit',$js);			
			//echo form_button('test', 'test',$js);
			?>
			</td>
			<!--<td><button onclick="return cekQty()">Test</button></td>-->
		</tr>
		
		</table>
<?php echo form_close();?>

<?php $this->load->view('footer');?>

<script type="text/javascript">
	function idRusakfun(){
	  	var x = document.getElementById("idRusak");

	}

	function handleClick() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/index/0/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}

	function handleClick1() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/index/1/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}
	function handleClick2() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/index/2/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}
	function handleClick3() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/index/3/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}
	function handleClick4() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/index/4/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}
	function handleClick5() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/index/5/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}
	function handleClick6() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/index/6/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}
	function handleClick7() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/index/7/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}
	function handleClick8() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/index/8/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}
	function handleClick9() {
	    var dropDown = document.getElementById('idRusak');
	    var dropValue = dropDown.options[dropDown.selectedIndex].value;

	    window.open('http://localhost/sohomlm_prod/invinsearchrsk/index/9/' + dropValue ,'mywindow','width=450,height=500,scrollbars=yes,status=yes,resizable=no,screenx=0,screeny=0')
	}
</script>
