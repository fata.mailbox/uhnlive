<table width='99%'>
<?php echo form_open('inv/item_inactive/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?> 
  <tr>
    <td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
    <td width='60%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
            echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
        <br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
      </td>  
  </tr>  
<?php echo form_close();?>        
</table>

<table class="stripe">
    <tr>
      <th width='7%'>No.</th>
      <th width='10%'>Product ID</th>
    <th width='50%'>Product Name</th>
      <th width='15%'>Exp Date</th>
      <th></th>     
    </tr>
<?php
if (isset($results)):
  $counter = $from_rows; foreach($results as $key => $row): 
  $counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor("/inv/item_inactive/view/".$row['id'],$counter);?></td>
      <td><?php echo $row['item_id'];?></td>
      <td><?php echo $row['name'];?></td>
      <td><?php echo $row['exp_date'];?></td>
      <td><a href="<?php echo site_url()."inv/item_inactive/edit/".$row['id'];?>"><img src="/images/backend/edit.gif" alt="edit" title="edit"></a></td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="8">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
