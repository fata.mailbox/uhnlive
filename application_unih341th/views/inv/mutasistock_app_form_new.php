<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('inv/mtsapp/app/'.$row->id, array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
	
	<table width='55%'>
		<tr>
			<td width='24%'>date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td valign='top'>remark approved</td>
			<td valign='top'>:</td>
			<td><?php echo form_hidden('id',$row->id); echo form_hidden('warehouse_id2',$row->warehouse_id2); echo form_hidden('warehouse_id1',$row->warehouse_id1); $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?> <span class="error"><?php echo form_error('remark'); ?></span></td> 
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php 
			$js = 'onClick="return validOri();"';
			echo form_submit('submit', 'Submit',$js);			
			echo form_button('test', 'test',$js);
			?></td></tr>
		</table>

		
<table class="stripe">
	<tr>
      <th width='5%'>No.</th>
      <th width='10%'>Item Code</th>
      <th width='20%'>Item Name</th>
      <th width='10%'>Qty</th>      
      <th width='10%'>Price</th>      
      <th width='10%'>Total Price</th>   
    </tr>
    
    <?php $counter =0; foreach ($items as $row1): $counter = $counter+1;?>
		<tr>
			<td><?php echo $counter;?></td>
			<td><?php echo $row1['item_id'];?></td>
			<td><?php echo $row1['name'];?></td>
			<td><?php echo $row1['fqty'];?></td>
			<td><?php echo $row1['fharga'];?></td>
			<td><?php echo $row1['fjmlharga'];?></td>
		</tr>
        <tr>
        	<td colspan="3" align="right">Actual</td>	
             <td>
             <input type="hidden" name="detail_id<?=$counter?>" id="detail_id<?=$counter?>" value="<?php echo $row1['id'];?>" />
             <input type="hidden" name="itemcode<?=$counter?>" id="itemcode<?=$counter?>" value="<?php echo $row1['item_id'];?>" />
             <input type="hidden" name="qtyOri<?=$counter?>" id="qtyOri<?=$counter?>" value="<?php echo $row1['qty'];?>" />
             <input class='textbold' type="text" name="qty<?=$counter?>" id="qty<?=$counter?>" value="<?php echo $row1['qty'];?>" maxlength="12" size="10" autocomplete="off" onKeyUp="this.value=formatCurrency(this.value);subtotalMutasi(this,document.form.price<?=$counter?>,document.form.tprice<?=$counter?>);totalMutasiActualHilrus();"></td>
             <td><input class='textbold' type="text" name="price<?=$counter?>" id="price<?=$counter?>" value="<?php echo number_format($row1['harga'],0,"",".");?>" maxlength="12" size="10" autocomplete="off" onKeyUp="this.value=formatCurrency(this.value);"  readonly="readonly"></td>
<td><input class='textbold' type="text" name="tprice<?=$counter?>" id="tprice<?=$counter?>" value="<?php echo number_format($row1['jmlharga'],0,"",".");?>" maxlength="12" size="10" autocomplete="off" onKeyUp="this.value=formatCurrency(this.value);"  readonly="readonly"></td>
        </tr>
        <tr>
        	<td colspan="3" align="right">Hilang / Rusak</td>	
             <td><input class='textbold' type="text" name="rqty<?=$counter?>" id="rqty<?=$counter?>" value="0" maxlength="12" size="10" autocomplete="off" onKeyUp="this.value=formatCurrency(this.value);subtotalMutasi(this,document.form.rprice<?=$counter?>,document.form.rtprice<?=$counter?>);totalMutasiActualHilrus();"></td>
             <td><input class='textbold' type="text" name="rprice<?=$counter?>" id="rprice<?=$counter?>" value="<?php echo number_format($row1['harga'],0,"",".");?>" maxlength="12" size="10" autocomplete="off"  readonly="readonly" onKeyUp="this.value=formatCurrency(this.value);"></td>
<td><input class='textbold' type="text" name="rtprice<?=$counter?>" id="rtprice<?=$counter?>" value="0" maxlength="12" size="10" autocomplete="off" onKeyUp="this.value=formatCurrency(this.value);" readonly="readonly"></td>
        </tr>
		<?php endforeach;?>
        <tr>
        	<td colspan="100%"> <hr /></td>
        <tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="left">Grand Total</td>
			<td><?php echo number_format($row->totalharga,0,"","."); ?></td>
        </tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="left">Grand Total Actual</td>
			<td><input class='textbold' type="text" name="totalprice_act" id="totalprice_act" value="<?php echo number_format($row->totalharga_ori,0,"","."); ?>" maxlength="12" size="10" autocomplete="off" readonly="readonly" onKeyUp="this.value=formatCurrency(this.value);"></td>
        </tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="left">Grand Total Hilang / Rusak</td>
			<td><input class='textbold' type="text" name="totalprice_hilrus" id="totalprice_hilrus" value="0" maxlength="12" size="10" autocomplete="off" onKeyUp="this.value=formatCurrency(this.value);" readonly="readonly"></td>
        </tr>
	</table>
    <input type="hidden" name="counter" id="counter" value="<?php echo $counter; ?>" />
<?php echo form_close();?>
<?php $this->load->view('footer');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
</body>
</html>