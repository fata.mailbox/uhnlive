<?php
	$ti = 0;
	$to = 0;
	$ts = 0;
?>
<table width='100%'>
<?php echo form_open('inv/stock/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
<?php if($this->session->userdata('whsid') == '1'){ ?>
	<tr>
		<td><?php echo form_dropdown('whsid',$warehouse);?></td>
  </tr>
  <?php }else echo form_hidden('whsid',$this->session->userdata('whsid'));?>
	<tr>
		<td><?php echo form_dropdown('reptype',$reptype);?></td>
  </tr>
  	<tr>
		<td><?php $options = array('qoh' => 'Stock Quantity On Hand', 'ss' => 'Stock Summary','sc' => 'Stock Card');
    				echo form_dropdown('status',$options,set_value('status'))."&nbsp;";
					$data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',$reportDate));
   echo form_input($data);?>
   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',$reportDate));
   echo "to: ".form_input($data);?>
   </td>
  </tr>

  <tr>
		<td valign='top'>Item for Stock Card: <?php $data = array('name'=>'itemcode','id'=>'itemcode','size'=>'8','readonly'=>'1','value'=>set_value('itemcode')); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('itemsearch/all/', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<input type="text" name="itemname" id="itemname" value="<?php echo set_value('itemname');?>" readonly="1" size="25" />
			<span class="error"><?php echo form_error('status'); ?></span>
			</td>
	</tr>
	<tr>
		<td><?php echo form_submit('submit','preview');?></td>
	</tr>
				  
<?php echo form_close();?>				
</table>

<?php 
	/* Quantity on hand */
	if($this->input->post('status') == 'qoh'){
?>

<table class="stripe">
    <tr>
      <th width='10%'>No.</th>
      <th width='20%'>Item Code</th>
      <th width='50%'>Item Name</th>
      <th width='20%'>Qty Stock</th>
     </tr>
<?php
if ($results):
	$counter = 0; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo $counter;?></td>
      <td><?php echo $row['id'];?></td>
      <td><?php echo $row['name'];?></td>
      <td><?php echo $row['stock'];?></td>
     </tr>
    <?php endforeach; 
else: ?>
    <tr>
      <td colspan="4">Data is not available.</td>
    </tr>
<?php 
endif; ?>    
</table>

<?php } 
	/* Stock Summary */
	elseif($this->input->post('status') == 'ss'){ ?>

<table class="stripe">
    <tr>
      <th width='8%'>No.</th>
      <th width='14%'>Date</th>
      <th width='34%'>Item Code - Name</th>
      <th width='11%'><div align="right">Saldo Awal</div></th>
      <th width='11%'><div align="right">Stock In</div></th>
      <th width='11%'><div align="right">Stock Out</div></th>
      <th width='11%'><div align="right">Saldo Akhir</div></th>
     </tr>
<?php
if ($results):
	$counter = 0; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo $counter;?></td>
      <td><?php echo $row['tgl'];?></td>
      <td><?php echo $row['item_id']," - ".$row['name'];?></td>
      <td><div align="right"><?php echo $row['fsaldoawal'];?></div></td>
      <td><div align="right"><?php echo $row['fsaldoin'];?></div></td>
      <td><div align="right"><?php echo $row['fsaldoout'];?></div></td>
      <td><div align="right"><?php echo $row['fsaldoakhir'];?></div></td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>


<?php } 
	/* Stock Card */
	else{ ?>

<table class="stripe">
    <tr>
      <th width='9%'>No.</th>
      <th width='15%'>Date</th>
      <th width='30%'>Description</th>
      <th width='12%'><div align="right">Stock In</div></th>
      <th width='12%'><div align="right">Stock Out</div></th>
      <th width='12%'><div align="right">Saldo</div></th>
      <th width='10%'><div align="right">User ID</div></th>
     </tr>
<?php
if ($results):
	$counter = 1; ?> 
	<tr>
      <td>1</td>
      <td colspan='4'>Balance stok</td>
      <td><div align="right"><b><?php echo $results[0]['saldo_awal'];?></b></div></td>
		<td>&nbsp;</td>      
     </tr>
	<?php 
	$stcDet = '';
	$memDet = '';
	foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo $counter;?></td>
      <td><?php echo $row['date'];?></td>
      <td><?php 
	  if($this->input->post('status') == 'sc') {
		  if($row['trxtype'] == 'Request Order'){
			  $getDetRO = $this->MInvreport->getDetRO($row['trxid']);
			  foreach($getDetRO as $keyDetRO => $rowDetRO): 
				$stcDet = $rowDetRO['stc_name']."(".$rowDetRO['no_stc'].")";
			  endforeach;
			  echo $row['description']." - ".$stcDet;
			  $stcDet = '';
		  }else if($row['trxtype'] == 'PO'){
			  $getDetSO = $this->MInvreport->getDetSO($row['trxid']);
			  foreach($getDetSO as $keyDetSO => $rowDetSO): 
				$memDet = $rowDetSO['member_name']."(".$rowDetSO['member_id'].")";
			  endforeach;
			  echo $row['description']." - ".$memDet;
			  $memDet = '';
		  }else{
			echo $row['description'];
		  }
	  }else{
		echo $row['description'];
	  }
	   ?></td>
      <td><div align="right"><?php echo number_format($row['saldoin']);		$ti+=$row['saldoin']; ?></div></td>
      <td><div align="right"><?php echo number_format($row['saldoout']);	$to+=$row['saldoout']; ?></div></td>
      <td><div align="right"><?php echo number_format($row['saldoakhir']);	$ts+=$row['saldoakhir'];?></div></td>
      <td><div align="right"><?php echo $row['createdby'];?></div></td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>
	<tr>
      <td colspan="3">Total</td>
      <td><div align="right"><?php echo number_format($ti);?></div></td>
      <td><div align="right"><?php echo number_format($to);?></div></td>
      <!--<td><div align="right"><?php echo number_format($ts);?></div></td>-->
      <td><div align="right"><?php echo number_format($ti-$to);?></div></td>
      <td><div align="right"><?php echo $row['createdby'];?></div></td>
     </tr>
</table>

<?php }?>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>
