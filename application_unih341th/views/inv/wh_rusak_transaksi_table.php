<table width='99%'>
<?php echo form_open('inv/wh_rusak_mov/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?> 
  <tr>
    <td width='60%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
    <td width='40%' align='right'>search by: <?php if($this->session->userdata('group_id') < 100 && $this->session->userdata('group_id')!= 28) echo form_dropdown('wrsk',$warehouse);?> <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
            echo form_input($data);?> <?php echo form_submit('submit','go');?><?php if($this->session->userdata('keywords')){ ?>
        <br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
      </td>  
  </tr>  
<?php echo form_close();?>        
</table>
<?php echo form_open('inv/wh_rusak_mov/approved/', array('id' => 'my_form2', 'name' => 'my_form2', 'autocomplete' => 'off'));?>
<table class="stripe">
  <tr>
    <td colspan='2' valign='top'>remark: </td>
    <td colspan='9'><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
      echo form_textarea($data);?><br>
      <?php echo form_submit('submit','approved');?>
    </td>
  </tr>
    <tr>
      <th width='3%'>&nbsp;</th>
      <th width='2%'>No.</th>
      <th width='10%'>Date</th>
      <th width='20%'>Warehouse</th>
      <th width='5%'>Approved</th>
      <th width='35%'>Remark</th>
      <th width='11%'>Created By</th>
      <th width='10%'>Approved date</th>
      <th width='8%'>Approved By</th>
    </tr>
<?php
if (isset($results)):
  $counter = $from_rows; foreach($results as $key => $row): 
  $counter = $counter+1;
?>
    <tr>
      <td>
      <?php if($row['approved'] == 'N'){ 
        $data = array(
        'name'        => 'p_id[]',
        'id'          => 'p_id[]',
        'value'       => $row['id'],
        'checked'     => false,
        'style'       => 'border:none'
        );
        echo form_checkbox($data); } else {?>&nbsp;<?php }?> </td>
      <td><?php echo anchor('inv/wh_rusak_mov/view/'.$row['id'], $counter);?></td>
      <td><?php echo anchor('inv/wh_rusak_mov/view/'.$row['id'], $row['date']);?></td>
      <td><?php echo anchor('inv/wh_rusak_mov/view/'.$row['id'], $row['nama']);?></td>
      <td><?php echo anchor('inv/wh_rusak_mov/view/'.$row['id'], $row['approved']);?></td>
      <td><?php echo anchor('inv/wh_rusak_mov/view/'.$row['id'], $row['remark']." ");?></td>
      <td><?php echo anchor('inv/wh_rusak_mov/view/'.$row['id'], $row['createdby']);?></td>
      <td><?php echo anchor('inv/wh_rusak_mov/view/'.$row['id'], $row['appdate']);?></td>
      <td><?php echo anchor('inv/wh_rusak_mov/view/'.$row['id'], $row['approvedby']);?></td>
    </tr>
    <?php endforeach; 
  else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php echo form_close();?>        
