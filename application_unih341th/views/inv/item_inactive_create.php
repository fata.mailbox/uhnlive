
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('inv/item_inactive/create', array('id' => 'form', 'name' => 'form','autocomplete' => 'off'));?>
		<table>
		
		<tr>
			<td valign='top'>Pilih Item : <?php $data = array('name'=>'itemcode','id'=>'itemcode','size'=>'8','readonly'=>'1','value'=>set_value('itemcode')); echo form_input($data);?>
			<?php 
					  $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'no',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('itemsearch/all/', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			<input type="text" name="itemname" id="itemname" value="<?php echo set_value('itemname');?>" readonly="1" size="25" />
	
		</tr>
		<tr>
			<td>
		   		<?php $data = array('name'=>'exp_date','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('exp_date',$reportDate));
   				echo "Exp Date : ".form_input($data);?>
   			</td>
  		</tr>
  		<br/>
  		<tr></tr>
		<tr><!--<td colspan='1'>&nbsp;</td>-->
			<td><?php echo form_submit('submit', 'Submit');?></td></tr>
		</table>

<?php echo form_close();?>
<?php $this->load->view('footer');?>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
</script>
