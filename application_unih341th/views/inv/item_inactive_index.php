<!--
	Copyright (c) 2018-<?php echo date("Y");?> 
	developed by  	:
    	contact person	: Annisa
        Handphone	: +62 85723439567
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php 
echo anchor('inv/item_inactive/create','create product inactive');
if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
						
$this->load->view('inv/item_inactive_table');
$this->load->view('footer');
?>