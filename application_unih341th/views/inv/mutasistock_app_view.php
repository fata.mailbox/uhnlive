<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<?php if($row->status == 'N'){?><div id="view"><?php echo anchor('inv/mtsapp/app/'.$row->id,'approved');?></div><?php }?>

<div class="ViewHold">
	<div id="companyDetails"><?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?></div>
	<p>
		<strong>
			<?php echo "No. Mutasi : ";?> <?php echo $row->id;?><br />
			<?php 
				echo $row->date;
				
			?> 
		</strong>
	</p>
<h2>Mutasi Stock Approval</h2>
<hr />
<h3><?php echo $row->fromwhs." to : ".$row->towhs;?></h3>
	<p>
		<?php
				echo "App: ".$row->status." by: ".$row->approvedby." date: ".$row->approved."<br />";
				echo "Remark: ".$row->remark."<br />";
				echo "Remark App: ".$row->remarkapp."<br />";
				echo "User ID: ".$row->createdby;
		?>
	</p>

	<table class="stripe">
	<tr>
      <th width='10%'>No.</th>
      <th width='20%'>Item Code</th>
      <th width='25%'>Item Name</th>
      <th width='10%'><div align="right">Qty</div></th>      
      <th width='15%'><div align="right">Price</div></th>      
      <th width='20%'><div align="right">Sub Total</div></th>      
    </tr>
    
    <?php $counter =0; foreach ($items as $row1): $counter = $counter+1;?>
		<tr>
			<td><?php echo $counter;?></td>
			<td><?php echo $row1['item_id'];?></td>
			<td><?php echo $row1['name'];?></td>
			<td><div align="right"><?php echo $row1['fqty'];?></div></td>
			<td><div align="right"><?php echo $row1['fharga'];?></div></td>
			<td><div align="right"><?php echo $row1['fjmlharga'];?></div></td>
		</tr>
		<?php endforeach;?>
        <tr>
			<td colspan='5' align='right'><b>Total Rp.</b>&nbsp;</td>
			<td colspan="2" align="right"><b><?php echo $row->ftotalharga;?></b></td>
		</tr>
	</table>
	<table width=100%>
	<tr>
	  <td colspan='1' align=center width='5%'> </td>
	  <td colspan='2' align=center width='25%'><b>Warehouse</b></td>
	  <td colspan='1' align=center width='5%'> </td>
      <td colspan='2' align=center width='30%'> </td>
	  <td colspan='1' align=center width='5%'> </td>
	  <td colspan='2' align=center width='25%'><b>Receiver</b></td> 	
	  <td colspan='1' align=center width='5%'> </td>
	  
	</tr>
	<tr>
	<td colspan='10'>
	<br><br><br><br><br><br>
	</td>
	</tr>
	
	<tr>
		<td></td>
		<td align='left'>(</td><td align='right'>)</td>
		<td></td>
		<td align='left'></td>
		<td></td>
		<td align='left'>(</td><td align='right'>)</td> 
		<td></td>
	</tr>
	</table>
<br /><br />
<?php
$this->load->view('footer');
?>
