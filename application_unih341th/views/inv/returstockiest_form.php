<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
    <?php
if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}?>
    
	 <?php echo form_open('inv/returstc/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		
		<table width='100%'>
		<?php if(validation_errors()){?>
		<tr>
			<td colspan='3'><span class="error"><?php echo form_error('total');?></span></td>
		</tr>
		<?php }?>
		<tr>
			<td valign="top" width='20%'>date</td>
			<td width='1%' valign="top">:</td>
			<td width='79%'><?php echo date('Y-m-d',now());?></td> 
		</tr>
				        <tr>
			<td valign='top'>stockiest</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_hidden('member_id',set_value('member_id','')); $data = array('name'=>'no_stc','id'=>'no_stc','size'=>15,'readonly'=>'1','value'=>set_value('no_stc'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('stcsearch/stc/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?><span class='error'>*<?php echo form_error('member_id');?></span></td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>

        <tr>
					<td>Cabang</td>
					<td>:</td>
					<td><?php echo form_dropdown('warehouse_id',$warehouse);?></td>
				</tr>
        <tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','tabindex'=>'1','value'=>set_value('remark'));
    					echo form_textarea($data);?></td> 
		</tr>

        </table>
        
		<table width='100%'>	
		<tr>
			<td width='18%'>Item Code</td>
			<td width='23%'>Item Name</td>
			<td width='8%'>Qty</td>
			<td width='12%'>Price</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>Del?</td>
		</tr>
		
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode0','id'=>'itemcode0','size'=>'8','readonly'=>'1','value'=>set_value('itemcode0')); echo form_input($data);?>
                    <input class="button" onclick="window.open('../../itemreturttpsearch/index/'+document.form.member_id.value+'/0/0/','_blank','height=600,width=500,status=yes,screenx=0,screeny=0,resizable=no,scrollbars=yes');" type="button" name="Button" value="Browse" />
                    </td>
			<td valign='top'><input type="text" name="itemname0" id="itemname0" value="<?php echo set_value('itemname0');?>" readonly="1" size="24" /></td>
			<td><input class='textbold aright' type="text" name="qty0" id="qty0" value="<?php echo set_value('qty0',0);?>" maxlength="12" size="3" tabindex="3" autocomplete="off" 
			onkeyup="this.value=formatCurrency(this.value); 
			 jumlah(document.form.qty0,document.form.price0,document.form.subtotal0);
			 document.form.total.value=total_curr(10,'document.form.subtotal');
			 jumlah(document.form.qty0,document.form.pv0,document.form.subtotalpv0);
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');"></td>
			<td><input class="aright" type="text" name="price0" id="price0" value="<?php echo set_value('price0',0);?>" size="8" readonly="1"></td>
			<td><input class="aright" type="text" name="pv0" id="pv0" value="<?php echo set_value('pv0',0);?>" size="5" readonly="1"></td>
			<td><input class="aright" type="text" name="subtotal0" id="subtotal0" value="<?php echo set_value('subtotal0',0);?>" readonly="1" size="12"></td>
			<td><input class="aright" type="text" name="subtotalpv0" id="subtotalpv0" value="<?php echo set_value('subtotalpv0',0);?>" readonly="1" size="10"></td>
			<td><img alt="delete" onclick="cleartext8(document.form.itemcode0,document.form.itemname0,document.form.qty0,document.form.price0,document.form.pv0,document.form.subtotal0,document.form.subtotalpv0,document.form.titipan_id0); 
				document.form.total.value=total_curr(10,'document.form.subtotal');
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode1','id'=>'itemcode1','size'=>'8','readonly'=>'1','value'=>set_value('itemcode1')); echo form_input($data);?>
			<input class="button" onclick="window.open('../../itemreturttpsearch/index/'+document.form.member_id.value+'/1/0/','_blank','height=600,width=500,status=yes,screenx=0,screeny=0,resizable=no,scrollbars=yes');" type="button" name="Button" value="Browse" /></td>
			<td valign='top'><input type="text" name="itemname1" id="itemname1" value="<?php echo set_value('itemname1');?>" readonly="1" size="24" /></td>
			<td><input class='textbold aright' type="text" name="qty1" id="qty1" value="<?php echo set_value('qty1',0);?>" maxlength="12" size="3" tabindex="5" autocomplete="off" 
			onkeyup="this.value=formatCurrency(this.value); 
			 jumlah(document.form.qty1,document.form.price1,document.form.subtotal1);
			 document.form.total.value=total_curr(10,'document.form.subtotal');
			 jumlah(document.form.qty1,document.form.pv1,document.form.subtotalpv1);
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');"></td>
			<td><input class="aright" type="text" name="price1" id="price1" value="<?php echo set_value('price1',0);?>" size="8" readonly="1"></td>
			<td><input class="aright" type="text" name="pv1" id="pv1" value="<?php echo set_value('pv1',0);?>" size="5" readonly="1"></td>
			<td><input class="aright" type="text" name="subtotal1" id="subtotal1" value="<?php echo set_value('subtotal1',0);?>" readonly="1" size="12"></td>
			<td><input class="aright" type="text" name="subtotalpv1" id="subtotalpv1" value="<?php echo set_value('subtotalpv1',0);?>" readonly="1" size="10"></td>
			<td><img alt="delete" onclick="cleartext8(document.form.itemcode1,document.form.itemname1,document.form.qty1,document.form.price1,document.form.pv1,document.form.subtotal1,document.form.subtotalpv1,document.form.titipan_id1); 
				document.form.total.value=total_curr(10,'document.form.subtotal');
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode2','id'=>'itemcode2','size'=>'8','readonly'=>'1','value'=>set_value('itemcode2')); echo form_input($data);?>
			<input class="button" onclick="window.open('../../itemreturttpsearch/index/'+document.form.member_id.value+'/2/0/','_blank','height=600,width=500,status=yes,screenx=0,screeny=0,resizable=no,scrollbars=yes');" type="button" name="Button" value="Browse" /></td>
			<td valign='top'><input type="text" name="itemname2" id="itemname2" value="<?php echo set_value('itemname2');?>" readonly="1" size="24" /></td>
			<td><input class='textbold aright' type="text" name="qty2" id="qty2" value="<?php echo set_value('qty2',0);?>" maxlength="12" size="3" tabindex="7" autocomplete="off" 
			onkeyup="this.value=formatCurrency(this.value); 
			 jumlah(document.form.qty2,document.form.price2,document.form.subtotal2);
			 document.form.total.value=total_curr(10,'document.form.subtotal');
			 jumlah(document.form.qty2,document.form.pv2,document.form.subtotalpv2);
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');"></td>
			<td><input class="aright" type="text" name="price2" id="price2" value="<?php echo set_value('price2',0);?>" size="8" readonly="1"</td>
			<td><input class="aright" type="text" name="pv2" id="pv2" value="<?php echo set_value('pv2',0);?>" size="5" readonly="1"></td>
			<td><input class="aright" type="text" name="subtotal2" id="subtotal2" value="<?php echo set_value('subtotal2',0);?>" readonly="1" size="12"></td>
			<td><input class="aright" type="text" name="subtotalpv2" id="subtotalpv2" value="<?php echo set_value('subtotalpv2',0);?>" readonly="1" size="10"></td>
			<td><img alt="delete" onclick="cleartext8(document.form.itemcode2,document.form.itemname2,document.form.qty2,document.form.price2,document.form.pv2,document.form.subtotal2,document.form.subtotalpv2,document.form.titipan_id2); 
				document.form.total.value=total_curr(10,'document.form.subtotal');
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode3','id'=>'itemcode3','size'=>'8','readonly'=>'1','value'=>set_value('itemcode3')); echo form_input($data);?>
			<input class="button" onclick="window.open('../../itemreturttpsearch/index/'+document.form.member_id.value+'/3/0/','_blank','height=600,width=500,status=yes,screenx=0,screeny=0,resizable=no,scrollbars=yes');" type="button" name="Button" value="Browse" /></td>
			<td valign='top'><input type="text" name="itemname3" id="itemname3" value="<?php echo set_value('itemname3');?>" readonly="1" size="24" /></td>
			<td><input class='textbold aright' type="text" name="qty3" id="qty3" value="<?php echo set_value('qty3',0);?>" maxlength="12" size="3" tabindex="9" autocomplete="off" 
			onkeyup="this.value=formatCurrency(this.value); 
			 jumlah(document.form.qty3,document.form.price3,document.form.subtotal3);
			 document.form.total.value=total_curr(10,'document.form.subtotal');
			 jumlah(document.form.qty3,document.form.pv3,document.form.subtotalpv3);
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');"></td>
			<td><input class="aright" type="text" name="price3" id="price3" value="<?php echo set_value('price3',0);?>" size="8" readonly="1"></td>
			<td><input class="aright" type="text" name="pv3" id="pv3" value="<?php echo set_value('pv3',0);?>" size="5" readonly="1"></td>
			<td><input class="aright" type="text" name="subtotal3" id="subtotal3" value="<?php echo set_value('subtotal3',0);?>" readonly="1" size="12"></td>
			<td><input class="aright" type="text" name="subtotalpv3" id="subtotalpv3" value="<?php echo set_value('subtotalpv3',0);?>" readonly="1" size="10"></td>
			<td><img alt="delete" onclick="cleartext8(document.form.itemcode3,document.form.itemname3,document.form.qty3,document.form.price3,document.form.pv3,document.form.subtotal3,document.form.subtotalpv3,document.form.titipan_id3); 
				document.form.total.value=total_curr(10,'document.form.subtotal');
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode4','id'=>'itemcode4','size'=>'8','readonly'=>'1','value'=>set_value('itemcode4')); echo form_input($data);?> 
<input class="button" onclick="window.open('../../itemreturttpsearch/index/'+document.form.member_id.value+'/4/0/','_blank','height=600,width=500,status=yes,screenx=0,screeny=0,resizable=no,scrollbars=yes');" type="button" name="Button" value="Browse" /></td>
			<td valign='top'><input type="text" name="itemname4" id="itemname4" value="<?php echo set_value('itemname4');?>" readonly="1" size="24" /></td>
			<td><input class='textbold aright' type="text" name="qty4" id="qty4" value="<?php echo set_value('qty4',0);?>" maxlength="12" size="3" tabindex="11" autocomplete="off" 
			onkeyup="this.value=formatCurrency(this.value); 
			 jumlah(document.form.qty4,document.form.price4,document.form.subtotal4);
			 document.form.total.value=total_curr(10,'document.form.subtotal');
			 jumlah(document.form.qty4,document.form.pv4,document.form.subtotalpv4);
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');""></td>
			<td><input class="aright" type="text" name="price4" id="price4" value="<?php echo set_value('price4',0);?>" size="8" readonly="1"></td>
			<td><input class="aright" type="text" name="pv4" id="pv4" value="<?php echo set_value('pv4',0);?>" size="5" readonly="1"></td>
			<td><input class="aright" type="text" name="subtotal4" id="subtotal4" value="<?php echo set_value('subtotal4',0);?>" readonly="1" size="12"></td>
			<td><input class="aright" type="text" name="subtotalpv4" id="subtotalpv4" value="<?php echo set_value('subtotalpv4',0);?>" readonly="1" size="10"></td>
			<td><img alt="delete" onclick="cleartext8(document.form.itemcode4,document.form.itemname4,document.form.qty4,document.form.price4,document.form.pv4,document.form.subtotal4,document.form.subtotalpv4,document.form.titipan_id4); 
				document.form.total.value=total_curr(10,'document.form.subtotal');
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode5','id'=>'itemcode5','size'=>'8','readonly'=>'1','value'=>set_value('itemcode5')); echo form_input($data);?> <input class="button" onclick="window.open('../../itemreturttpsearch/index/'+document.form.member_id.value+'/5/0/','_blank','height=600,width=500,status=yes,screenx=0,screeny=0,resizable=no,scrollbars=yes');" type="button" name="Button" value="Browse" /></td>
            <td valign='top'><input type="text" name="itemname5" id="itemname5" value="<?php echo set_value('itemname5');?>" readonly="1" size="24" /></td>
			<td><input class='textbold aright' type="text" name="qty5" id="qty5" value="<?php echo set_value('qty5',0);?>" maxlength="12" size="3" tabindex="13" autocomplete="off" 
			onkeyup="this.value=formatCurrency(this.value); 
			 jumlah(document.form.qty5,document.form.price5,document.form.subtotal5);
			 document.form.total.value=total_curr(10,'document.form.subtotal');
			 jumlah(document.form.qty5,document.form.pv5,document.form.subtotalpv5);
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');"></td>
			<td><input class="aright" type="text" name="price5" id="price5" value="<?php echo set_value('price5',0);?>" size="8" readonly="1"></td>
			<td><input class="aright" type="text" name="pv5" id="pv5" value="<?php echo set_value('pv5',0);?>" size="5" readonly="1"></td>
			<td><input class="aright" type="text" name="subtotal5" id="subtotal5" value="<?php echo set_value('subtotal5',0);?>" readonly="1" size="12"></td>
			<td><input class="aright" type="text" name="subtotalpv5" id="subtotalpv5" value="<?php echo set_value('subtotalpv5',0);?>" readonly="1" size="10"></td>
			<td><img alt="delete" onclick="cleartext8(document.form.itemcode5,document.form.itemname5,document.form.qty5,document.form.price5,document.form.pv5,document.form.subtotal5,document.form.subtotalpv5,document.form.titipan_id5); 
				document.form.total.value=total_curr(10,'document.form.subtotal');
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode6','id'=>'itemcode6','size'=>'8','readonly'=>'1','value'=>set_value('itemcode6')); echo form_input($data);?> <input class="button" onclick="window.open('../../itemreturttpsearch/index/'+document.form.member_id.value+'/6/0/','_blank','height=600,width=500,status=yes,screenx=0,screeny=0,resizable=no,scrollbars=yes');" type="button" name="Button" value="Browse" /></td>
			<td valign='top'><input type="text" name="itemname6" id="itemname6" value="<?php echo set_value('itemname6');?>" readonly="1" size="24" /></td>
			<td><input class='textbold aright' type="text" name="qty6" id="qty6" value="<?php echo set_value('qty6',0);?>" maxlength="12" size="3" tabindex="15" autocomplete="off" 
			onkeyup="this.value=formatCurrency(this.value); 
			 jumlah(document.form.qty6,document.form.price6,document.form.subtotal6);
			 document.form.total.value=total_curr(10,'document.form.subtotal');
			 jumlah(document.form.qty6,document.form.pv6,document.form.subtotalpv6);
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');"></td>
			<td><input class="aright" type="text" name="price6" id="price6" value="<?php echo set_value('price6',0);?>" size="8" readonly="1"></td>
			<td><input class="aright" type="text" name="pv6" id="pv6" value="<?php echo set_value('pv6',0);?>" size="5" readonly="1"></td>
			<td><input class="aright" type="text" name="subtotal6" id="subtotal6" value="<?php echo set_value('subtotal6',0);?>" readonly="1" size="12"></td>
			<td><input class="aright" type="text" name="subtotalpv6" id="subtotalpv6" value="<?php echo set_value('subtotalpv6',0);?>" readonly="1" size="10"></td>
			<td><img alt="delete" onclick="cleartext8(document.form.itemcode6,document.form.itemname6,document.form.qty6,document.form.price6,document.form.pv6,document.form.subtotal6,document.form.subtotalpv6,document.form.titipan_id6); 
				document.form.total.value=total_curr(10,'document.form.subtotal');
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode7','id'=>'itemcode7','size'=>'8','readonly'=>'1','value'=>set_value('itemcode7')); echo form_input($data);?>
			<input class="button" onclick="window.open('../../itemreturttpsearch/index/'+document.form.member_id.value+'/7/0/','_blank','height=600,width=500,status=yes,screenx=0,screeny=0,resizable=no,scrollbars=yes');" type="button" name="Button" value="Browse" /></td>
			<td valign='top'><input type="text" name="itemname7" id="itemname7" value="<?php echo set_value('itemname7');?>" readonly="1" size="24" /></td>
			<td><input class='textbold aright' type="text" name="qty7" id="qty7" value="<?php echo set_value('qty7',0);?>" maxlength="12" tabindex="17" size="3" autocomplete="off" 
			onkeyup="this.value=formatCurrency(this.value); 
			 jumlah(document.form.qty7,document.form.price7,document.form.subtotal7);
			 document.form.total.value=total_curr(10,'document.form.subtotal');
			 jumlah(document.form.qty7,document.form.pv7,document.form.subtotalpv7);
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');"></td>
			<td><input class="aright" type="text" name="price7" id="price7" value="<?php echo set_value('price7',0);?>" size="8" readonly="1"</td>
			<td><input class="aright" type="text" name="pv7" id="pv7" value="<?php echo set_value('pv7',0);?>" size="5" readonly="1"></td>
			<td><input class="aright" type="text" name="subtotal7" id="subtotal7" value="<?php echo set_value('subtotal7',0);?>" readonly="1" size="12"></td>
			<td><input class="aright" type="text" name="subtotalpv7" id="subtotalpv7" value="<?php echo set_value('subtotalpv7',0);?>" readonly="1" size="10"></td>
			<td><img alt="delete" onclick="cleartext8(document.form.itemcode7,document.form.itemname7,document.form.qty7,document.form.price7,document.form.pv7,document.form.subtotal7,document.form.subtotalpv7,document.form.titipan_id7); 
				document.form.total.value=total_curr(10,'document.form.subtotal');
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode8','id'=>'itemcode8','size'=>'8','readonly'=>'1','value'=>set_value('itemcode8')); echo form_input($data);?> <input class="button" onclick="window.open('../../itemreturttpsearch/index/'+document.form.member_id.value+'/8/0/','_blank','height=600,width=500,status=yes,screenx=0,screeny=0,resizable=no,scrollbars=yes');" type="button" name="Button" value="Browse" /></td>
            <td valign='top'><input type="text" name="itemname8" id="itemname8" value="<?php echo set_value('itemname8');?>" readonly="1" size="24" /></td>
			<td><input class='textbold aright' type="text" name="qty8" id="qty8" value="<?php echo set_value('qty8',0);?>" maxlength="12" size="3" tabindex="19" autocomplete="off" 
			onkeyup="this.value=formatCurrency(this.value); 
			 jumlah(document.form.qty8,document.form.price8,document.form.subtotal8);
			 document.form.total.value=total_curr(10,'document.form.subtotal');
			 jumlah(document.form.qty8,document.form.pv8,document.form.subtotalpv8);
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');"></td>
			<td><input class="aright" type="text" name="price8" id="price8" value="<?php echo set_value('price8',0);?>" size="8" readonly="1"></td>
			<td><input class="aright" type="text" name="pv8" id="pv8" value="<?php echo set_value('pv8',0);?>" size="5" readonly="1"></td>
			<td><input class="aright" type="text" name="subtotal8" id="subtotal8" value="<?php echo set_value('subtotal8',0);?>" readonly="1" size="12"></td>
			<td><input class="aright" type="text" name="subtotalpv8" id="subtotalpv8" value="<?php echo set_value('subtotalpv8',0);?>" readonly="1" size="10"></td>
			<td><img alt="delete" onclick="cleartext8(document.form.itemcode8,document.form.itemname8,document.form.qty8,document.form.price8,document.form.pv8,document.form.subtotal8,document.form.subtotalpv8,document.form.titipan_id8); 
				document.form.total.value=total_curr(10,'document.form.subtotal');
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		<tr>
			<td valign='top'><?php $data = array('name'=>'itemcode9','id'=>'itemcode9','size'=>'8','readonly'=>'1','value'=>set_value('itemcode9')); echo form_input($data);?>
		<input class="button" onclick="window.open('../../itemreturttpsearch/index/'+document.form.member_id.value+'/9/0/','_blank','height=600,width=500,status=yes,screenx=0,screeny=0,resizable=no,scrollbars=yes');" type="button" name="Button" value="Browse" /></td>
        	<td valign='top'><input type="text" name="itemname9" id="itemname9" value="<?php echo set_value('itemname9');?>" readonly="1" size="24" /></td>
			<td><input class='textbold aright' type="text" name="qty9" id="qty9" value="<?php echo set_value('qty9',0);?>" maxlength="12" size="3" tabindex="21" autocomplete="off" 
			onkeyup="this.value=formatCurrency(this.value); 
			 jumlah(document.form.qty9,document.form.price9,document.form.subtotal9);
			 document.form.total.value=total_curr(10,'document.form.subtotal');
			 jumlah(document.form.qty9,document.form.pv9,document.form.subtotalpv9);
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');""></td>
			<td><input class="aright" type="text" name="price9" id="price9" value="<?php echo set_value('price9',0);?>" size="8" readonly="1"></td>
			<td><input class="aright" type="text" name="pv9" id="pv9" value="<?php echo set_value('pv9',0);?>" size="5" readonly="1"></td>
			<td><input class="aright" type="text" name="subtotal9" id="subtotal9" value="<?php echo set_value('subtotal9',0);?>" readonly="1" size="12"></td>
			<td><input class="aright" type="text" name="subtotalpv9" id="subtotalpv9" value="<?php echo set_value('subtotalpv9',0);?>" readonly="1" size="10"></td>
			<td><img alt="delete" onclick="cleartext8(document.form.itemcode9,document.form.itemname9,document.form.qty9,document.form.price9,document.form.pv9,document.form.subtotal9,document.form.subtotalpv9,document.form.titipan_id9); 
				document.form.total.value=total_curr(10,'document.form.subtotal');
			 document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/></td>
		</tr>
		
		<tr>
			<td colspan='5'></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total',0);?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv',0);?>" readonly="1" size="9"></td>
		</tr>	
		<tr>
			<td colspan='6'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit', 'tabindex="23"');?></td>
		</tr>
		
		</table>
		 
<?php echo form_close();?>

<?php $this->load->view('footer');?>