<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php
	if ($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}
	echo form_open('smartindo/ro_vwh/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>
	<table width='100%'>
		<tr>
			<td width='17%'>Date</td>
			<td width='1%'>:</td>
			<td width='82%'>
				<?php echo date('Y-m-d',now());?></td>
		</tr>
        <tr>
			<td valign='top'>Stockist ID</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php echo form_hidden('member_id',set_value('member_id',$this->session->userdata('userid'))); echo $this->session->userdata('username');?>
			</td>	
		</tr>
		<tr>
			<td valign='top'>Stockist Name</td>
			<td valign='top'>:</td>
			<td><?php echo $this->session->userdata('name');?></td>
		</tr>
        <tr>
			<td valign='top'>Saldo eWallet Stockist</td>
			<td valign='top'>:</td>
			<td>Rp. <?php echo $address->fewalletstc;?></td>
		</tr>
		
		<tr>
			<td valign='top'>Diskon %</td>
			<td valign='top'>:</td>
			<td><?php echo form_hidden('persen',set_value('persen',$address->persen)); echo $address->persen;?>%</td>
		</tr>
		<tr><td colspan="3"><hr/></td></tr>
		<tr>
			<td colspan="100%"><font style="color:#F00"><i>Hanya berlaku satu alamat pengiriman per transaksi</i></font></td>
		</tr>
		<tr>
			<td><b>Pick up / Delivery</b></td>
			<td valign="top" align='left'>:</td>
			<td><?php
					$options = array(						
						'1'    => 'Delivery',
						'0'  => 'Pick Up'
					);
					echo form_dropdown('pu', $options,set_value('pu'));
				?></td>
		</tr>
		<tr>
			<td valign="top">Delivery Address</td>
		  <td align='top'></td>
          <td valign="top">
          <table width="100%">
          	<tr>
            	<td width="48%"><label for="radio1"><?php echo form_radio("oaddr", "C", (set_value("oaddr",'C') == "C"),"id='radio1'"); ?>
              Default Address</label></td>
            	<td width="17%"><?php echo form_radio("oaddr", "N", (set_value("oaddr") == "N"),"id='radio2'"); ?> Others Address</td>
                <td width="35%">&nbsp;</td>
            </tr>
            <tr>
              <td><?=$address->alamat;?></td>
              <td valign="top">History Address</td>
              <td><?=form_dropdown('idaddr',$alamathistory,'','style="width:300px"');?>
              <br /><small>Note :<br /><i>Silahkan pilih "alamat baru" pada history address dan isi form alamat dibawah ini jika ingin mengirimkan paket ke alamat yang belum tercantum</i></small>
              </td>
            </tr>
            <!-- START ASP 20180427 -->
            <tr>
            	<td><?=$address->namakota." - ".$address->propinsi." - ".$address->warehouse_id;?></td>
                <td valign='top'>Penerima</td>
                <td valign="top"><input type="text" name="pic_name" id="pic_name" value="<?php echo set_value('pic_name');?>" size="30" /></td>
            </tr>
            <tr>
            	<td></td>
                <td valign='top'>No HP</td>
                <td valign="top"><input type="text" name="pic_hp" id="pic_hp" value="<?php echo set_value('pic_hp');?>" size="30" /></td>
            </tr>
            <!-- EOF ASP 20180427 -->
            <tr>
            	<td></td>
            	<td  valign="top">Address :</td>
                <td><?php $data = array('name'=>'addr','id'=>'addr','rows'=>2, 'cols'=>'30','value'=>set_value('addr'));
    					echo form_textarea($data);?></td>
            </tr>
            <tr>
            	<td>&nbsp;</td>
            	<td>City of delivery :</td>
                <td><?php
				echo form_hidden('propinsi', set_value('propinsi',0));
				echo form_hidden('timur', set_value('timur',$address->timur));
				echo form_hidden('deli_ad', set_value('deli_ad',$address->deli_ad));
				echo form_hidden('kota_id', set_value('kota_id',0));
				
				echo form_hidden('whsid', set_value('whsid',$address->warehouse_id));
				
				$data = array('name'=>'city','id'=>'city','readonly'=>'1','value'=>set_value('city',$row['kota']));
				echo form_input($data);
				$atts = array(
					'width'      => '400',
					'height'     => '400',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'yes',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				echo anchor_popup('citysearch/allso_v/', "<img src='/images/backend/search.gif' border='0'>", $atts);
				?>
				<span class='error'>*<?php echo form_error('city'); ?></span></td>
            </tr>
		<tr>
        	<td></td>
			<td valign='top'>Kecamatan</td>
			<td valign="top"><input type="text" name="kecamatan" id="kecamatan" value="<?php echo set_value('kecamatan');?>" size="30" /></td>
		</tr>
		<tr>
        	<td></td>
			<td valign='top'>Kelurahan</td>
			<td valign="top"><input type="text" name="kelurahan" id="kelurahan" value="<?php echo set_value('kelurahan');?>" size="30" /></td>
		</tr>
		<tr>
        	<td></td>
			<td valign='top'>Kodepos</td>
			<td valign="top"><input type="text" name="kodepos" id="kodepos" value="<?php echo set_value('kodepos');?>" size="30" /></td>
		</tr>
          </table>
          </td>
			
		</tr>
		<tr>
			<td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>		
	</table>
<?php echo form_close();?>

<?php $this->load->view('footer');?>
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>