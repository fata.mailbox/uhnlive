
<table width='99%'>
<?php echo form_open('smartindo/soadmin_v', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='60%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='40%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b>
			<?php }?>
			
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
	<tr>
      <th width='7%'>No.</th>
      <th width='10%'>Invoice No.</th>
      <th width='12%'>Date</th>
      <th width='27%'>Member ID / Name</th>
      <th width='11%'><div align="right">Total Price</div></th>
      <th width='11%'><div align="right">Total PV</div></th>
      <th width='11%'><div align="right">Warehouse</div></th>
      <th width='11%'><div align="right">Req Time</div></th>
      <!--<th width='11%'><div align="right">App Time</div></th>-->
      <th width='8%'>Kit?</th>
    </tr>
   
<?php
if (isset($results)):
	$counter = $from_rows; 
	foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor('smartindo/soadmin_v/view/'.$row['id'], $counter);?></td>
      <td><?php echo anchor('smartindo/soadmin_v/view/'.$row['id'], $row['id']);?></td>
      <td><?php echo anchor('smartindo/soadmin_v/view/'.$row['id'], $row['tgl']);?></td>
      <td><?php echo anchor('smartindo/soadmin_v/view/'.$row['id'], $row['member_id']." - ".$row['nama']);?></td>
      <td align="right"><?php echo anchor('smartindo/soadmin_v/view/'.$row['id'], $row['ftotalharga']);?></td>
		<td align="right"><?php echo anchor('smartindo/soadmin_v/view/'.$row['id'], $row['ftotalpv']);?></td>
		<td align="right"><?php echo anchor('smartindo/soadmin_v/view/'.$row['id'], $row['warehouse_name']);?></td>
		<td align="right"><?php echo anchor('smartindo/soadmin_v/view/'.$row['id'], $row['created']);?></td>
		<!--<td align="right"><?php echo anchor('smartindo/soadmin_v/view/'.$row['id'], $row['appdate']);?></td>-->
      <td><?php echo anchor('smartindo/soadmin_v/view/'.$row['id'], $row['kit']);?></td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>