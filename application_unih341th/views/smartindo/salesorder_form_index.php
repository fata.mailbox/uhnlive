<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<?php
	if ($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}
	echo form_open('order/so/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>
	<table width='100%'>
		<tr>
			<td width='20%'>Date</td>
			<td width='1%'>:</td>
			<td width='79%'>
				<?php 
					$data = array('name'=>'date','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('date',date('Y-m-d',now())));
					echo form_input($data);
				?>
				<span class='error'><?php echo form_error('date');?></span>
			</td>
		</tr>
        <tr>
			<td valign='top'>Member ID</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php 
					$data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
					echo form_input($data);
					$atts = array(
						'width'      => '450',
						'height'     => '600',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'yes',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					echo anchor_popup('memsearch/ewalletso', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
				?>
				<span class='error'>*<?php echo form_error('member_id');?></span>
			</td>	
		</tr>
		<tr>
			<td valign='top'>Member Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
        <tr>
			<td valign='top'>Ewallet Rp</td>
			<td valign='top'>:</td>
			<td><input type="text" name="ewallet" id="ewallet" readonly="1" value="<?php echo set_value('ewallet');?>" size="15" /></td>
		</tr>
        <tr>
          <td valign="top">City of delivery</td>
          <td valign='top'>:</td>
          <td width="23%" valign="top">
            <?php
					$options = array(
						''  => 'Choose Delivery',
						'0'  => 'Pick Up',
						'1'    => 'Delivery',
					);
					echo form_dropdown('pu', $options);
				?><span class='error'>*<?php echo form_error('pu'); ?></span></td>
        </tr>
        <tr>
          <td valign='top'>Delivery Address</td>
          <td valign='top'>:</td>
          <td valign="top"><?php
				echo form_hidden('kota_id', set_value('kota_id',0));
				echo form_hidden('propinsi', set_value('propinsi',0));
				echo form_hidden('timur', set_value('timur',0));
				echo form_hidden('deli_ad', set_value('deli_ad',0));
				
				echo form_hidden('addr1', set_value('addr1',0));
				echo form_hidden('kota_id1', set_value('kota_id1',0));
				
					$data = array('name'=>'city','id'=>'city','readonly'=>'1','value'=>set_value('city',$row['kota']));
				echo form_input($data);
				$atts = array(
					'width'      => '400',
					'height'     => '400',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'yes',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				echo anchor_popup('citysearch/allso/', "<img src='/images/backend/search.gif' border='0'>", $atts);
				?>
            <span class='error'>*<?php echo form_error('city'); ?></span></td>
        </tr>
        <tr>
          <td valign='top'>&nbsp;</td>
          <td valign='top'>:</td>
          <td valign='top'><?php $data = array('name'=>'addr','id'=>'addr','rows'=>5, 'cols'=>'50','value'=>set_value('addr'));
				echo form_textarea($data);?></td>
        </tr>
        <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
            <td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>
	</table>
		
<?php echo form_close();?>

<?php $this->load->view('footer');?>
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>