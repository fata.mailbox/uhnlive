<?php 
	$this->load->view('header');
	echo "<h2>".$page_title."</h2>";
	// echo anchor('auth/regnik/create','Qualified Member List');
	echo "Qualified Member List&nbsp;||&nbsp;";
	echo anchor('smartindo/car_bns/newcarbns','New Qualified Member List');
	if($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>
	<table width="99%">
	<?php echo form_open($base_url, array('i' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search by: 
			<?php 
				$data = array('name'=>'search','i'=>'search','size'=>20,'value'=>set_value('search'));
    			echo form_input($data);?> <?php echo form_submit('submit','go');
				if($this->session->userdata('keywords')){echo "<br/>Your search keywords : <b>".$this->session->userdata('keywords')."</b>";}
			?>
    	</td>
	</tr>
	<?php echo form_close();?>				
	</table>
	
<table class="stripe">
	<?php /* Header of table */ ?>
	<tr><?php $field = 0; $tp=0;?>
		<?php $p=5;?>	<th width="<?=$p?>%">No.</th><?php $field+=1;$tp+=$p;?>
		<?php $p=25;?>	<th width='<?=$p?>%'>Member ID / Name</th><?php $field+=1;$tp+=$p;?>
		<?php $p=5;?>	<th width='<?=$p?>%'>Level</th><?php $field+=1;$tp+=$p;?>
		<?php $p=20;?>	<th width='<?=$p?>%'>CCB / CCB Count</th><?php $field+=1;$tp+=$p;?>
		<?php $p=20;?>	<th width='<?=$p?>%'>COP / COP Count</th><?php $field+=1;$tp+=$p;?>
		<?php $p=10;?>	<th width='<?=$p?>%'>Started</th><?php $field+=1;$tp+=$p;?>
		<?php $p=15;?>	<th width='<?=$p?>%'>Status</th><?php $field+=1;$tp+=$p;?>
		<?php if($tp<>100){$tp=100-$tp; echo "Miss the percentage field width..".$tp."%";}?>
	</tr>
	<?php /* End header of table */ ?>
	
<?php
	/* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== */
	/* Looping data table */
if (isset($results)):
	$counter = $from_rows;
	foreach($results as $key => $row):
		$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor("/smartindo/car_bns/view/".$row['id'],$counter);?></td>
      <td><?php echo anchor("/smartindo/car_bns/view/".$row['id'],$row['member_id']." / ".$row['nama']." ");?></td>
	  <td><?php echo anchor("/smartindo/car_bns/view/".$row['id'],$row['jenjang']);?></td>
      <td><?php echo anchor("/smartindo/car_bns/view/".$row['id'],number_format($row['ccb'])." | ".$row['ccbCount']."x");?></td>
      <td><?php echo anchor("/smartindo/car_bns/view/".$row['id'],number_format($row['cop'])." | ".$row['copCount']."x");?></td>
      <td><?php echo anchor("/smartindo/car_bns/view/".$row['id'],$row['cop_start']);?></td>
      <td><?php echo anchor("/smartindo/car_bns/view/".$row['id'],$row['tipe']." -> " .$row['status_']);?></td>
    </tr>
<?php 
	endforeach;
	/* End looping data table*/
	/* ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== */
	else:
 ?>
    <tr>
      <td colspan="<?php echo $field;?>">Data is not available.</td>
    </tr>
<?php endif; ?>
</table>
<?php /* End Content */?>

<?php $this->load->view('footer');?>