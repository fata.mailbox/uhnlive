<?php $this->load->view('header'); ?>
<h2><?php echo $page_title; ?></h2>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>



<style>
#myTable {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#myTable td, #myTable th {
  border: 1px solid #ddd;
  padding: 8px;
}

#myTable tr:nth-child(even){background-color: #f2f2f2;}

#myTable tr:hover {background-color: #ddd;}

#myTable th {
  padding-top: 12px;
  padding-bottom: 12px;
  font-size: 13px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		tbl = $('#myTable').DataTable();
	});
</script>
<table>
	<tr>
		<td>Stokiest ID</td>
		<td> : </td>
		<td><?php echo codestc($result->stockiest_id) ?></td>
		<td>Stokiest Name</td>
		<td> : </td>
		<td><?php echo call($result->stockiest_id, 'member'); ?></td>
	</tr>
	<br>
	<tr>
		<td>Valid from</td>
		<td> : </td>
		<td><?php echo $result->valid_from; ?></td>
		<td>Valid to</td>
		<td> : </td>
		<td><?php echo $result->valid_to; ?></td>
		<hr>
	</tr>
	<tr>
		<td>No. Ref Request Order (RO)</td>
		<td> : </td>
		<td><?php echo $no_ro; ?></td>
		<td>No. Ref Sales Order (SO)</td>
		<td> : </td>
		<td><?php echo $noso; ?></td>
	</tr>
	<tr></tr>
	<tr>
		<td>Jumlah Awal</td>
		<td> : </td>
		<td><?php echo $jml_awal; ?></td>
	</tr>
	<tr></tr>
	<tr>
		<td>Jumlah Pakai</td>
		<td> : </td>
		<td><?php echo $jml_pakai; ?></td>
	</tr>
	<tr></tr>
	<tr>
		<td>Jumlah Akhir</td>
		<td> : </td>
		<td><?php echo $jml_akhir; ?></td>
	</tr>

	<tr>
		<td>Last Update</td>
		<td> : </td>
		<td><?= $result->last_update; ?></td>
	</tr>
</table>
<br>
<table width="100%" id="myTable">
	<thead>
		<tr class="ths">
			<th width="5%">No</th>
			<th>Member ID</th>
			<th>Member Name</th>
			<th>Item Code</th>
			<th>Item Name</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1;
		foreach ($rows as $value) {
			$item = get_data_by_tabel_id($value->item_code, 'item');
		?>
			<tr>
				<td><?php echo $no; ?></tds>
				<td><?php echo $value->member_id; ?></td>
				<td><?php echo call($value->member_id, 'member'); ?></td>
				<td><?php echo $value->item_code; ?></td>
				<td><?php echo $item->name; ?></td>

			</tr>
		<?php $no++;
		} ?>
	</tbody>
</table>
<div style="float: right; padding: 10px;">
	<a href="<?php echo base_url(); ?>smartindo/loyalti"><button style="width: 150px; height: 30px;">Back</button></a>
</div>
<?php
$this->load->view('footer');
?>