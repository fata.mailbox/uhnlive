<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php
	if ($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}
	echo form_open('smartindo/soadmin_v/add', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>
	<table width='100%'>
		<tr>
			<td width='20%'>Date</td>
			<td width='1%'>:</td>
			<td width='79%'><?=date('Y-m-d',now());?></td>
		</tr>
        <tr>
			<td valign='top'>Member ID</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php echo form_hidden('member_id',$this->session->userdata('s_member_id')); echo $this->session->userdata('s_member_id');?>
				<span class='error'><?php echo form_error('member_id');?></span>
			</td>	
		</tr>
		<tr>
			<td valign='top'>Member Name</td>
			<td valign='top'>:</td>
			<td><?=$row->nama;?></td>
		</tr>
        <tr>
			<td valign='top'>Ewallet Rp</td>
			<td valign='top'>:</td>
			<td><?=$row->fewallet;?></td>
		</tr>
		<tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td><?php //echo $this->session->userdata('r_whsid'); 
			$getWhsName = $this->SO_model->getWhsName($this->session->userdata('s_whsid'));
			foreach ($getWhsName as $rowWhsDetail) :
				echo $rowWhsDetail['name'];
			endforeach;
			?></td>
		</tr>
		<tr>
			<td valign='top'>Remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','tabindex'=>'1','value'=>set_value('remark'));
				echo form_textarea($data);?>
			</td> 
		</tr>
        </table>
        
        <table width='100%'>	
		<tr>
			<td width='18%'>Item Code</td>
			<td width='23%'>Item Name</td>
			<td width='8%'>Qty</td>
			<td width='12%'>Price</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>Del?</td>
		</tr>
		
        
		<?php $i=0; 
while($i < $counti){?>
		<tr>
			<td valign='top'>
			<?php 
				echo form_hidden('counter[]',$i); 
				$data = array('name'=>'itemcode'.$i,'id'=>'itemcode'.$i,'size'=>'8','readonly'=>'1','value'=>set_value('itemcode'.$i)); 
				echo form_input($data);
				$atts = array(
					'width'      => '600',
					'height'     => '500',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'no',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				//echo anchor_popup('search/stock/index/1/'.$i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
				echo anchor_popup('search/stock/so_v/'.$this->session->userdata('s_whsid').'/'.$i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
				echo form_hidden('bv'.$i,set_value('bv'.$i,0));
				echo form_hidden('subtotalbv'.$i,set_value('subtotalbv'.$i,0));
			?>
				
			<td valign='top'>
				<input type="text" name="itemname<?php echo $i;?>" id="itemname<?php echo $i;?>" value="<?php echo set_value('itemname'.$i);?>" readonly="1" size="24" />
			</td>
			<td>
				<input class='textbold aright' type="text" name="qty<?php echo $i;?>" id="qty<?php echo $i;?>"
					value="<?php echo set_value('qty'.$i,0);?>" 
					maxlength="12" size="3" tabindex="3" autocomplete="off" 
					onkeyup="
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.bv<?php echo $i;?>,document.form.subtotalbv<?php echo $i;?>);
						document.form.total.value=formatCurrency(ReplaceDoted(total_curr(<?=$counti;?>,'document.form.subtotal'))-ReplaceDoted(document.form.vtotal.value));
						document.form.totalpv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?=$counti;?>,'document.form.subtotalpv'))-ReplaceDoted(document.form.vtotalpv.value));
						document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?=$counti;?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));
                        if(document.form.total.value < document.form.vminorder.value ) {
                        	alert('Voucher Tidak Dapat digunakan. Minimum Pembelanjaan Rp. '+document.form.vfminorder.value+'. Silahkan tambah order anda dan pilih voucher kembali');
                            document.form.vminorder.value = '0';
                            document.form.vfminorder.value = '0';
                            if(document.form.vselectedvoucher.value == document.form.vouchercode0.value){
                                                        document.form.vselectedvoucher.value = '0';
                                                }else{
                                                        var listselectedvoucher = document.form.vselectedvoucher.value;
                                                        var splitList = listselectedvoucher.split(',');
                                                        var changeList = '';
                                                        var ol;
                                                        for (ol = 0; ol < splitList.length; ol++) {
                                                            if (splitList[ol] != document.form.vouchercode0.value) {
                                                                if(changeList == '')
                                                                    changeList = splitList[ol];
                                                                else
                                                                    changeList = changeList+','+splitList[ol];
                                                            }
                                                        }	
                                                        document.form.vselectedvoucher.value = changeList;
                                                }
                                                cleartext7a(
                                                        document.form.vouchercode0
                                                        ,document.form.vprice0
                                                        ,document.form.vpv0
                                                        ,document.form.vsubtotal0
                                                        ,document.form.vsubtotalpv0
                                                        ,document.form.vbv0
                                                        ,document.form.vsubtotalbv0
                                                    ); 
                                                    document.form.vtotal.value=vtotal_curr(<?=$countv;?>,'document.form.vsubtotal');
                                                    document.form.vtotalpv.value=vtotalpv_curr(<?=$countv;?>,'document.form.vsubtotalpv');
                                                    document.form.vtotalbv.value=vtotalbv_curr(<?=$countv;?>,'document.form.vsubtotalbv');
                                                    if(vtotal_curr(<?=$countv;?>,'document.form.vsubtotal')=='0'){
                                                        document.form.total.value=total_curr(<?=$counti;?>,'document.form.subtotal');
                                                        document.form.totalpv.value=totalpv_curr(<?=$counti;?>,'document.form.subtotalpv');
                                                        document.form.totalbv.value=totalbv_curr(<?=$counti;?>,'document.form.subtotalbv');
                                                    }else{
                                                        document.form.total.value=totalAfterVoucher(total_curr(<?=$counti;?>,'document.form.subtotal'),vtotal_curr(<?=$countv;?>,'document.form.vsubtotal'));
                                                        document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?=$counti;?>,'document.form.subtotalpv'),vtotalpv_curr(<?=$countv;?>,'document.form.vsubtotalpv'));
                                                        document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?=$counti;?>,'document.form.subtotalbv'),vtotalbv_curr(<?=$countv;?>,'document.form.vsubtotalbv'));
                                                    }
                             return true;
                        };
				">
			</td>
			<td>
				<input class="aright" type="text" name="price<?php echo $i;?>" id="price<?php echo $i;?>" size="8" 
					value="<?php echo set_value('price'.$i,0);?>"
                    readonly="readonly">
			</td>
			<td>
				<input class="aright" type="text" name="pv<?php echo $i;?>" id="pv<?php echo $i;?>"
					value="<?php echo set_value('pv'.$i,0);?>" size="5" 
                	readonly="readonly">
			</td>
			<td>
				<input class="aright" type="text" name="subtotal<?php echo $i;?>" id="subtotal<?php echo $i;?>" value="<?php echo set_value('subtotal'.$i,0);?>" readonly="1" size="12">
			</td>
			<td>
				<input class="aright" type="text" name="subtotalpv<?php echo $i;?>" id="subtotalpv<?php echo $i;?>" value="<?php echo set_value('subtotalpv'.$i,0);?>" readonly="1" size="10">
			</td>
			<td>
            <!--
				<img alt="delete" 
					onclick="
					cleartext9(
							document.form.itemcode<?php echo $i;?>
							,document.form.itemname<?php echo $i;?>
							,document.form.qty<?php echo $i;?>
                            ,document.form.price<?php echo $i;?>
							,document.form.pv<?php echo $i;?>
                            ,document.form.subtotal<?php echo $i;?>
							,document.form.subtotalpv<?php echo $i;?>
							,document.form.bv<?php echo $i;?>
							,document.form.subtotalbv<?php echo $i;?>
						); 
						document.form.total.value=total_curr(<?=$counti;?>,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(<?=$counti;?>,'document.form.subtotalpv');
						document.form.totalbv.value=totalbv_curr(<?=$counti;?>,'document.form.subtotalbv');" 
					src="<?php echo  base_url();?>images/backend/delete.png" border="0"
				/>
                -->
				<img alt="delete" 
					onclick="
					cleartext9(
							document.form.itemcode<?php echo $i;?>
							,document.form.itemname<?php echo $i;?>
							,document.form.qty<?php echo $i;?>
                            ,document.form.price<?php echo $i;?>
							,document.form.pv<?php echo $i;?>
                            ,document.form.subtotal<?php echo $i;?>
							,document.form.subtotalpv<?php echo $i;?>
							,document.form.bv<?php echo $i;?>
							,document.form.subtotalbv<?php echo $i;?>
						); 
                        document.form.total.value=totalAfterVoucher(total_curr(<?=$counti;?>,'document.form.subtotal'),vtotal_curr(<?=$countv;?>,'document.form.vsubtotal'));
                        document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?=$counti;?>,'document.form.subtotalpv'),vtotalpv_curr(<?=$countv;?>,'document.form.vsubtotalpv'));
                        document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?=$counti;?>,'document.form.subtotalbv'),vtotalbv_curr(<?=$countv;?>,'document.form.vsubtotalbv'));
                        " 
					src="<?php echo  base_url();?>images/backend/delete.png" border="0"
				/>
			 </td>
		</tr>
			<?php $i++;
		}
		?>
        <tr>
			<td colspan='5'>Add <input name="rowx" type="text" id="rowx" value="<?=set_value('rowx','1');?>" size="1" maxlength="2" />
          Row(s)<?php echo form_submit('action', 'Go');?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
        <?php //Created By ASP 20151201 ?>
		<tr><td colspan="8"><hr/></td></tr>
		<tr>
			<td width='18%'>Voucher Code</td>
			<td width='23%'>&nbsp;</td>
			<td width='8%'>&nbsp;</td>
			<td width='12%'>Rp Value</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>&nbsp;</td>
		</tr>
		<?php $v=0; 
		echo form_hidden('vselectedvoucher',set_value('vselectedvoucher',0));
		echo form_hidden('vminorder',set_value('vminorder',0));
		echo form_hidden('vfminorder',set_value('vfminorder',0));
		while($v < $countv){?>
		<tr>
			<td colspan='3'><?php 
				echo form_hidden('vcounter[]',$v); 
				$data = array('name'=>'vouchercode'.$v,'id'=>'vouchercode'.$v,'size'=>'8','readonly'=>'1','value'=>set_value('vouchercode'.$v)); 
				echo form_input($data);
				$atts = array(
					'width'      => '600',
					'height'     => '500',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'no',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				echo anchor_popup('search/voucher/index/'.$this->session->userdata('s_member_id').'/'.$v, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts); 
				echo form_hidden('vbv'.$v,set_value('vbv'.$v,0));
				echo form_hidden('vsubtotalbv'.$v,set_value('vsubtotalbv'.$v,0));

			?></td>
			<td>
				<input class="aright" type="text" name="vprice<?php echo $v;?>" id="vprice<?php echo $v;?>" size="8" 
					value="<?php echo set_value('vprice'.$v,0);?>"
                    readonly="readonly">
			</td>
			<td>
				<input class="aright" type="text" name="vpv<?php echo $v;?>" id="vpv<?php echo $v;?>"
					value="<?php echo set_value('vpv'.$v,0);?>" size="5" 
                	readonly="readonly">
			</td>
            <td>
				<input class="aright" type="text" name="vsubtotal<?php echo $v;?>" id="vsubtotal<?php echo $v;?>" value="<?php echo set_value('vsubtotal'.$v,0);?>" readonly="1" size="12">
			</td>
			<td>
				<input class="aright" type="text" name="vsubtotalpv<?php echo $v;?>" id="vsubtotalpv<?php echo $v;?>" value="<?php echo set_value('vsubtotalpv'.$v,0);?>" readonly="1" size="10">
			</td>

		</tr>	
        <?php $v++;
		}
		?>
        <tr>
			<td colspan='5'>&nbsp;</td>
			<td><input class='textbold aright' type="text" name="vtotal" id="vtotal" value="<?php echo set_value('vtotal',0);?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="vtotalpv" id="vtotalpv" value="<?php echo set_value('vtotalpv',0);?>" readonly="1" size="9">
            	<?php echo form_hidden('vtotalbv',set_value('vtotalbv',0));?>
            </td>
		</tr>	
		<tr><td colspan="8"><hr/></td></tr>
        <tr>
			<td colspan='5' align="right">&nbsp; <b>Total Pembelanjaan</b></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total',0);?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv',0);?>" readonly="1" size="9">
            	<?php echo form_hidden('totalbv',set_value('totalbv',0));?>
            </td>
		</tr>

        
		<?php if(validation_errors() or form_error('totalbayar')){?>
		<tr>
			<td colspan='100%' align="center" bgcolor="#FFAABF"><span class="error"><?php echo form_error('total');?> <?php echo form_error('totalbayar');?> <?php echo form_error('vminorder');?></span></td>
		</tr>
		<?php }?>
		
		
		<?php // Created by Boby 20140120 ?>
		<tr><td colspan="8"><hr/></td></tr>
		<tr>
			<td colspan="2"><b>Pick up / Delivery</b></td>
			<td valign="top" colspan='3' align='left'>&nbsp;</td>
			<td colspan="2"><b>Payment</b></td>
		</tr>
		<tr>
			<td colspan="100%"><font style="color:#F00"><i>Hanya berlaku satu alamat pengiriman per transaksi</i></font></td>
		</tr>
		<tr>
			<td valign="top">Option</td>
			<td valign="top"><?php $pu = $this->session->userdata('s_pu'); if($pu == '1')echo ": Delivery"; else echo ": Pick Up";?>
			</td>
			<td valign="top" colspan='4' align='right'>Cash :</td>
			<td colspan='2' valign='top'></span><input type="text" class="textbold" size="15" name="tunai" id="tunai" autocomplete="off" value="<?php echo set_value('tunai',0);?>" onkeyup="this.value=formatCurrency(this.value); totalBayar(document.form.tunai,document.form.debit,document.form.credit,document.form.totalbayar)">
            </td>
		</tr>
		<tr>
			<td valign="top"><?php if($pu == '1')echo "City of delivery ";?></td>
			<td vaslign="top"><?php
				//START ASP 20180410
				echo form_hidden('whsid', set_value('whsid',$this->session->userdata('s_whsid')));
				
				echo form_hidden('pic_name', set_value('pic_name',$this->session->userdata('s_pic_name')));
				echo form_hidden('pic_hp', set_value('pic_hp',$this->session->userdata('s_pic_hp')));
				echo form_hidden('kecamatan', set_value('kecamatan',$this->session->userdata('s_kecamatan')));
				echo form_hidden('kelurahan', set_value('kelurahan',$this->session->userdata('s_kelurahan')));
				echo form_hidden('kodepos', set_value('kodepos',$this->session->userdata('s_kodepos')));
				//EOF ASP 20180410
				
				echo form_hidden('kota_id', set_value('kota_id',$this->session->userdata('s_kota_id')));
				echo form_hidden('propinsi', set_value('propinsi',$this->session->userdata('s_propinsi')));
				echo form_hidden('timur', set_value('timur',$this->session->userdata('s_timur')));
				echo form_hidden('deli_ad', set_value('deli_ad',$this->session->userdata('s_deli_ad')));
				
				echo form_hidden('addr1', set_value('addr1',$this->session->userdata('s_addr1')));
				echo form_hidden('kota_id1', set_value('kota_id1',$this->session->userdata('s_kota_id1')));
				echo form_hidden('city', set_value('city',$this->session->userdata('s_city'))); echo ": ".$this->session->userdata('s_city')." - ".$this->session->userdata('s_propinsi');
				?>
				</td>
			<td valign="top" colspan='4' align='right'>Debit Card :</td>
			<td colspan='2' valign='top'></span><input type="text" class="textbold" size="15" name="debit" id="debit" autocomplete="off" value="<?php echo set_value('debit',0);?>" onkeyup="this.value=formatCurrency(this.value); totalBayar(document.form.tunai,document.form.debit,document.form.credit,document.form.totalbayar)">
            </td>
		</tr>
		<tr>
			<td valign='top'><?php if($pu == '1')echo "Delivery Address ";?></td>
			<td valign='top'colspan='4'><?php echo form_hidden('addr', set_value('addr',$this->session->userdata('s_addr'))); echo ": ".$this->session->userdata('s_addr');?></td>
			<td valign="top" colspan='1' align='right'>Credit Card :</td>
			<td colspan='2' valign='top'></span><input type="text" class="textbold" size="15" name="credit" id="credit" autocomplete="off" value="<?php echo set_value('credit',0);?>" onkeyup="this.value=formatCurrency(this.value); totalBayar(document.form.tunai,document.form.debit,document.form.credit,document.form.totalbayar)">
            </td>
		</tr>
		<tr>
        <tr>
			<td valign='top'><?php if($pu == '1')echo "Kelurahan ";?></td>
			<td valign='top' colspan='4'><?php echo ": ".$this->session->userdata('s_kelurahan');?>
			<td valign="top" colspan='1' align="right">Total Payment Rp.</td>
			<td colspan="2" valign='top'><input class='textbold' type="text" name="totalbayar" id="totalbayar" size="15" value="<?php echo set_value('totalbayar',0);?>" readonly="1" size="11"></td>
		</tr>	
		<tr>
			<td valign='top'><?php if($pu == '1')echo "Kecamatan ";?></td>
			<td valign='top'colspan='5'><?php echo ": ".$this->session->userdata('s_kecamatan');?>
			<td><?php $this->load->view('submit_confirm');?></td>
            <td>&nbsp;</td>
		</tr>	
		<tr>
			<td valign='top'><?php if($pu == '1')echo "Kodepos ";?></td>
			<td valign='top'colspan='6'><?php echo ": ".$this->session->userdata('s_kodepos');?>
            </td>
		</tr>
		<tr>
			<td valign='top'><?php if($pu == '1')echo "Penerima ";?></td>
			<td valign='top'colspan='6'><?php echo ": ".$this->session->userdata('s_pic_name');?>
            </td>
		</tr>
		<tr>
			<td valign='top'><?php if($pu == '1')echo "HP Penerima ";?></td>
			<td valign='top'colspan='6'><?php echo ": ".$this->session->userdata('s_pic_hp');?>
            </td>
		</tr>
	</table>
<?php echo form_close();?>

<?php $this->load->view('footer');?>
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>