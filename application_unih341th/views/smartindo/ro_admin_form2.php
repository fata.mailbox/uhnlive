<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php
	if ($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}
	echo form_open('smartindo/roadmin/add', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>
	<table width='100%'>
		<tr>
			<td width='20%'>Date</td>
			<td width='1%'>:</td>
			<td width='79%'><?=date('Y-m-d',now());?></td>
		</tr>
        <tr>
			<td valign='top'>Stockist ID</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php echo form_hidden('member_id',$this->session->userdata('r_member_id')); echo $row->no_stc;?>
				<span class='error'><?php echo form_error('member_id');?></span>
			</td>	
		</tr>
		<tr>
			<td valign='top'>Stockist Name</td>
			<td valign='top'>:</td>
			<td><?=$row->nama;?></td>
		</tr>
        <tr>
			<td valign='top'>Ewallet Rp</td>
			<td valign='top'>:</td>
			<td><?=$row->fewallet;?></td>
		</tr>
		<tr>
			<td valign='top'>Remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','tabindex'=>'1','value'=>set_value('remark'));
				echo form_textarea($data);?>
			</td> 
		</tr>
        </table>
        
        <table width='100%'>	
		<tr>
			<td width='18%'>Item Code</td>
			<td width='23%'>Item Name</td>
			<td width='8%'>Qty</td>
			<td width='12%'>Price</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>Del?</td>
		</tr>
		
        
		<?php $i=0; 
while($i < $counti){?>
		<tr>
			<td valign='top'>
			<?php 
				echo form_hidden('counter[]',$i); 
				$data = array('name'=>'itemcode'.$i,'id'=>'itemcode'.$i,'size'=>'8','readonly'=>'1','value'=>set_value('itemcode'.$i)); 
				echo form_input($data);
				$atts = array(
					'width'      => '600',
					'height'     => '500',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'no',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				echo anchor_popup('search/stock/ro/1/'.$i, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts);
				echo form_hidden('bv'.$i,set_value('bv'.$i,0));
				echo form_hidden('subtotalbv'.$i,set_value('subtotalbv'.$i,0));
				echo form_hidden('rpdiskon'.$i,set_value('rpdiskon'.$i,0));
				echo form_hidden('subrpdiskon'.$i,set_value('subrpdiskon'.$i,0));
			?>
				
			<td valign='top'>
				<input type="text" name="itemname<?php echo $i;?>" id="itemname<?php echo $i;?>" value="<?php echo set_value('itemname'.$i);?>" readonly="1" size="24" />
			</td>
			<td>
				<input class='textbold aright' type="text" name="qty<?php echo $i;?>" id="qty<?php echo $i;?>"
					value="<?php echo set_value('qty'.$i,0);?>" 
					maxlength="12" size="3" tabindex="3" autocomplete="off" 
					onkeyup="
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.bv<?php echo $i;?>,document.form.subtotalbv<?php echo $i;?>);
                        jumlah(document.form.qty<?php echo $i;?>,document.form.rpdiskon<?php echo $i;?>,document.form.subrpdiskon<?php echo $i;?>);
						document.form.total.value=total_curr(<?=$counti;?>,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(<?=$counti;?>,'document.form.subtotalpv');
						document.form.totalbv.value=totalbv_curr(<?=$counti;?>,'document.form.subtotalbv');                        
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?=$counti;?>,'document.form.subrpdiskon');
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
				">
			</td>
			<td>
				<input class="aright" type="text" readonly="readonly" name="price<?php echo $i;?>" id="price<?php echo $i;?>" size="8" 
					value="<?php echo set_value('price'.$i,0);?>"
					onkeyup="
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.rpdiskon<?php echo $i;?>,document.form.subrpdiskon<?php echo $i;?>);
						document.form.total.value=total_curr(<?=$counti;?>,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(<?=$counti;?>,'document.form.subtotalpv');
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?=$counti;?>,'document.form.subrpdiskon');
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
					">
			</td>
			<td>
				<input class="aright" type="text" readonly="readonly" name="pv<?php echo $i;?>" id="pv<?php echo $i;?>"
					value="<?php echo set_value('pv'.$i,0);?>" size="5" 
					onkeyup="this.value=formatCurrency(this.value); 
						jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.rpdiskon<?php echo $i;?>,document.form.subrpdiskon<?php echo $i;?>);
						document.form.total.value=total_curr(<?=$counti;?>,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(<?=$counti;?>,'document.form.subtotalpv');
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?=$counti;?>,'document.form.subrpdiskon');
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?=$counti;?>,'document.form.subrpdiskon');
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);
				">
			</td>
			<td>
				<input class="aright" type="text" name="subtotal<?php echo $i;?>" id="subtotal<?php echo $i;?>" value="<?php echo set_value('subtotal'.$i,0);?>" readonly="1" size="12">
			</td>
			<td>
				<input class="aright" type="text" name="subtotalpv<?php echo $i;?>" id="subtotalpv<?php echo $i;?>" value="<?php echo set_value('subtotalpv'.$i,0);?>" readonly="1" size="10">
			</td>
			<td>
				<img alt="delete" 
					onclick="
					cleartext11(
							document.form.itemcode<?php echo $i;?>
							,document.form.itemname<?php echo $i;?>
							,document.form.qty<?php echo $i;?>
                            ,document.form.price<?php echo $i;?>
							,document.form.pv<?php echo $i;?>
                            ,document.form.subtotal<?php echo $i;?>
							,document.form.subtotalpv<?php echo $i;?>
							,document.form.bv<?php echo $i;?>
							,document.form.subtotalbv<?php echo $i;?>
                            ,document.form.rpdiskon<?php echo $i;?>
							,document.form.subrpdiskon<?php echo $i;?>
						); 
						document.form.total.value=total_curr(<?=$counti;?>,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(<?=$counti;?>,'document.form.subtotalpv');
						document.form.totalbv.value=totalbv_curr(<?=$counti;?>,'document.form.subtotalbv');
                        document.form.totalrpdiskon.value=totaldiskon_curr(<?=$counti;?>,'document.form.subrpdiskon');
                        totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);" src="<?php echo  base_url();?>images/backend/delete.png" border="0"
				/>
			 </td>
		</tr>
			<?php $i++;
		}
		?>
		
		<tr>
			<td colspan='5'>Add <input name="rowx" type="text" id="rowx" value="<?=set_value('rowx','1');?>" size="1" maxlength="2" />
          Row(s)<?php echo form_submit('action', 'Go');?></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total',0);?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv',0);?>" readonly="1" size="9">
            	<?php echo form_hidden('totalbv',set_value('totalbv',0));?>
            </td>
		</tr>	
        	
		<?php if(validation_errors() or form_error('totalbayar')){?>
		<tr>
        	<td colspan='6'>&nbsp;</td>
			<td colspan='2'><span class="error"><?php echo form_error('total');?> <?php echo form_error('totalbayar');?></span></td>
		</tr>
		<?php }?>
		
		
		<?php // Created by Boby 20140120 ?>
		<tr><td colspan="8"><hr/></td></tr>
		<tr>
			<td colspan="2"><b>Pick up / Delivery</b></td>
			<td valign="top" colspan='3' align='left'>&nbsp;</td>
			<td colspan="2"><b>Payment</b></td>
		</tr>
		<tr>
			<td valign="top">Option</td>
			<td valign="top"><?php $pu = $this->session->userdata('r_pu'); if($pu == '1')echo ": Delivery"; else echo ": Pick Up";?>
			</td>
            <td valign="top" colspan='4' align='right'>Diskon % :</td>
			<td valign="top" colspan='2'><input class="aright" type="text" name="persen" id="persen" readonly="readonly" value="<?php echo set_value('persen',$this->session->userdata(r_persen));?>" 
				onkeyup="
					this.value=formatCurrency(this.value);
					diskon(document.form.total,document.form.persen,document.form.totalrpdiskon); 
					totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);"
				maxlength="2"/></td>
		</tr>
		<tr>
			<td valign="top"><?php if($pu == '1')echo "City of delivery ";?></td>
			<td vaslign="top"><?php
				echo form_hidden('kota_id', set_value('kota_id',$this->session->userdata('r_kota_id')));
				echo form_hidden('propinsi', set_value('propinsi',$this->session->userdata('r_propinsi')));
				echo form_hidden('timur', set_value('timur',$this->session->userdata('r_timur')));
				echo form_hidden('deli_ad', set_value('deli_ad',$this->session->userdata('r_deli_ad')));
				
				echo form_hidden('addr1', set_value('addr1',$this->session->userdata('r_addr1')));
				echo form_hidden('kota_id1', set_value('kota_id1',$this->session->userdata('r_kota_id1')));
				echo form_hidden('city', set_value('city',$this->session->userdata('r_city'))); echo ": ".$this->session->userdata('r_city')." - ".$this->session->userdata('r_propinsi');
				?>
				</td>
			<td valign="top" colspan='4' align='right'>Diskon Rp. :</td>
			<td colspan='2' valign='top'><input class="aright" type="text" name="totalrpdiskon" id="totalrpdiskon" readonly="readonly" value="<?php echo set_value('totalrpdiskon',0);?>" onkeyup="this.value=formatCurrency(this.value);
				totalbayardiskon(document.form.total, document.form.totalrpdiskon, document.form.totalbayar);" /></td>
		</tr>
		<tr>
			<td valign='top'><?php if($pu == '1')echo "Delivery Address ";?></td>
			<td valign='top'colspan='4'><?php echo form_hidden('addr', set_value('addr',$this->session->userdata('r_addr'))); echo ": ".$this->session->userdata('r_addr');?></td>
			<td valign="top" colspan='1' align='right'>Total Bayar :</td>
			<td colspan='2' valign='top'><input class="textbold aright" type="text" name="totalbayar" id="totalbayar" value="<?php echo set_value('totalbayar',0);?>"  readonly="readonly"/></td>
		</tr>
		<tr>
			<td colspan='6'>&nbsp;</td>
			<td colspan="2"><?php $this->load->view('submit_confirm');?></td>
		</tr>		
	</table>
<?php echo form_close();?>

<?php $this->load->view('footer');?>
