<?php $this->load->view('header'); ?>
<h2><?php echo $page_title; ?></h2>
<?php echo anchor('smartindo/topupdemand/add', 'Create TopUp On Demand'); ?>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		tbl = $('#myTable').DataTable();
	});

	function myfunction(id) {
		if (confirm("Hapus Data ?")) {
			window.location.href = "<?php echo base_url(); ?>smartindo/topupdemand/delete/" + id;
			//alert(id);
		}
	}
</script>


<style>
#myTable {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#myTable td, #myTable th {
  border: 1px solid #ddd;
  padding: 8px;
}

#myTable tr:nth-child(even){background-color: #f2f2f2;}

#myTable tr:hover {background-color: #ddd;}

#myTable th {
  padding-top: 12px;
  padding-bottom: 12px;
  font-size: 13px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>

<?php
	if ($this->session->flashdata('message')){
		echo "<br /><div class='message'>".$this->session->flashdata('message')."</div>";
	}
?>
<hr>

<table  width="100%" id="myTable">
	<thead>
		<tr>
			<th>No</th>
			<th>TopUp No</th>
			<th>File Name</th>
			<th>Valid From</th>
			<th>Valid To</th>
			<th>Status</th>
			<th>Option</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1;
		foreach ($result as $value) { ?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $value->topupno; ?></td>
				<td><?php echo $value->filename; ?></td>
				<td><?php echo $value->valid_from; ?></td>
				<td><?php echo $value->valid_to; ?></td>
				<td><?php if ($value->status == 1) {
						echo "Aktif";
					} else {
						echo "Tidak Aktif";
					}; ?></td>
				<td><a href="<?php echo base_url(); ?>smartindo/topupdemand/edit/<?php echo $value->topupno; ?>">Edit</a> |
					<?php if (cek_topup($value->topupno)  == 0) : ?>
				 <a onclick="myfunction('<?php echo $value->topupno; ?>');">Delete</a></td>
					<?php endif;?>
			</tr>
		<?php $no++;
		} ?>

	</tbody>
</table>
<?php
//$this->load->view('inv/returstockiest_table');
$this->load->view('footer');
?>