<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="100%">
<?php echo form_open('smartindo/bonusperingkat/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
<?php if($this->session->userdata('group_id') <= 100){ ?>
				 <tr>
			<td width='19%' valign='top'>Member ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?>				
					 <span class='error'>*<?php echo form_error('member_id');?></span></td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
        <?php }else{ ?>
        <tr>
			<td width='19%' valign='top'>Member ID / Nama </td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('userid')." / ".$this->session->userdata('name');?></b></td>
		</tr>
        <?php }?>
        
        <tr>
			<td valign='top'>Periode</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('periode',$dropdown);?></td>
		</tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
		<td><?php echo form_submit('submit','preview');?></td>
	</tr>
                    <?php echo form_close();?>
	</table>
	
    <?php echo form_open('smartindo/bonusperingkat/approved/', array('id' => 'form2', 'name' => 'form2', 'autocomplete' => 'off'));?>
    <table class="stripe" width="100%">
	<tr>
		<td colspan='2' valign='top'>remark: </td>
		<td colspan='10'>
			<?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?><br><?php 
				echo form_submit('submit','approved');
			?>
		</td>
	</tr>
    <tr>
	  <th width='1%'>&nbsp;</th>
      <th width='3%' align="center">No.</th>
      <th width='7%' align="left">Month</th>
      <th width='11%' align="left">Description</th>
      <th width='7%' align="left">Member ID</th>
      <th width='15%' align="left">Name</th>
      <th width='7%'>Position</th>
      <th width='8%'>HP</th>
      <th width='9%'><div align="right">Nominal Rp</div></th>
      <th width='17%' align="left">Remark</th>
      <th width='7%' align="center">Approved</th>
      <th width='8%'>Approvedby</th>
    </tr>
   
<?php
if ($results): 
	foreach($results as $key => $row): ?>
    <tr>
      <td><?=form_hidden('counter[]',$key);
			if($row['flag'] < 1){ 
				$data = array(
				'name'		=> 'p_id'.$key,
				'id'        => 'p_id'.$key,
				'value'     => $row['id'],
				'checked'   => true,
				'style'     => 'border:none');
				// echo $row['flag_'];
				if($row['flag_'] < 1){echo form_checkbox($data);}
			}else{
				echo "&nbsp";
			}
		?></td>
      <td align="center"><?php echo anchor('smartindo/bonusperingkat/detail/'.$row['id'], $row['i']);?></td>
      <td><?php echo anchor('smartindo/bonusperingkat/detail/'.$row['id'], $row['tgl']);?></td>
      <td><?php echo anchor('smartindo/bonusperingkat/detail/'.$row['id'], $row['titlebonus']);?></td>
      <td><?php echo anchor('smartindo/bonusperingkat/detail/'.$row['id'], $row['member_id']);?></td>
      <td><?php echo anchor('smartindo/bonusperingkat/detail/'.$row['id'], $row['nama']);?></td>
      <td><?php echo anchor('smartindo/bonusperingkat/detail/'.$row['id'], $row['jenjang']);?></td>
      <td><?php echo anchor('smartindo/bonusperingkat/detail/'.$row['id'], $row['hp']);?></td>
     <td align="right"><?php echo anchor('smartindo/bonusperingkat/detail/'.$row['id'], $row['fnominal']);?></td>
     <td align="left"><?php echo anchor('smartindo/bonusperingkat/detail/'.$row['id'], $row['remark']." ");?></td>
     <td align="center"><?php echo anchor('smartindo/bonusperingkat/detail/'.$row['id'], $row['ftglapproved']);?></td>
     <td align="left"><?php echo anchor('smartindo/bonusperingkat/detail/'.$row['id'], $row['approvedby']." ");?></td>
    </tr>
    <?php endforeach; ?>
	<tr>
	      <td colspan="6"><b>Total Bonus Rp. </b></td>
	      <td align="right">&nbsp;</td>
	      <td align="right">&nbsp;</td>
     	<td align="right"><b><?php echo $total['fnominal'];?></b></td>
     	<td align="right">&nbsp;</td>
     	<td align="right">&nbsp;</td>
     	<td align="right">&nbsp;</td>
    </tr>
    <!-- 
    <tr>
	      <td colspan="5"><br />Keterangan: <br />
          - PPH 21 dikenakan pada semua bonus per bulan, sesuai dengan ketentuan perpajakan yang berlaku.<br />
          - Bonus diatas Rp 50.000.000,- ke atas mengikuti ketentuan perpajakan yang berlaku (progresif).</td>
    </tr>
    -->
<?php else: ?>
    <tr>
      <td colspan="12">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>			
<?php echo form_close();?>                
                
<?php $this->load->view('footer');?>
