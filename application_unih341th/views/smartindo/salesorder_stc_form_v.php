<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
    <?php
if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}?>
    
	 <?php echo form_open('smartindo/so_v/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		
		<table width='100%'>
		<tr>
			<td valign="top" width='20%'>date</td>
			<td width='1%' valign="top">:</td>
			<td width='79%'><?php $data = array('name'=>'date','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('date',date('Y-m-d',now())));
   echo form_input($data);?><span class='error'><?php echo form_error('date');?></span></td> 
		</tr>
		<tr>
			<td>name</td>
			<td>:</td>
			<td></b><?php echo $this->session->userdata('username')." / ".$this->session->userdata('name');
			 echo form_hidden('stc_id',$this->session->userdata('userid'));?></td> 
		</tr>
        <tr>
			<td valign='top'>Member ID</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '450',
              'height'     => '600',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					//echo anchor_popup('search/membersearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					echo anchor_popup('search/membersearch/so_v/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?>				
					 <span class='error'>*<?php echo form_error('member_id');?></span></td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
        <tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','tabindex'=>'1','value'=>set_value('remark'));
    					echo form_textarea($data);?>
                        <span class="error"><?php echo form_error('total');?></span>
                        </td> 
		</tr>
        </table>
		
		
        <table width='100%'>	
		<tr>
			<td width='18%'>Item Code</td>
			<td width='23%'>Item Name</td>
			<td width='8%'>Qty</td>
			<td width='12%'>Price</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>Del?</td>
		</tr>
		
        
		<?php $i=0; 
while($i < $counti){?>
		<tr>
			<td valign='top'>
			<?php 
				echo form_hidden('counter[]',$i); 
				$data = array('name'=>'itemcode'.$i,'id'=>'itemcode'.$i,'size'=>'8','readonly'=>'1','value'=>set_value('itemcode'.$i)); 
				echo form_input($data);
				$atts = array(
					'width'      => '600',
					'height'     => '500',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'no',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				echo anchor_popup('search/titipan/so_v/'.$i, '<input class="button" type="button" tabindex="2" name="Button" value="browse" onclick="afterklik()"/>', $atts);
				echo form_hidden('titipan_id'.$i,set_value('titipan_id'.$i,0));
				echo form_hidden('bv'.$i,set_value('bv'.$i,0));
				echo form_hidden('subtotalbv'.$i,set_value('subtotalbv'.$i,0));
			?>
				
			<td valign='top'>
				<input type="text" name="itemname<?php echo $i;?>" id="itemname<?php echo $i;?>" value="<?php echo set_value('itemname'.$i);?>" readonly="1" size="24" />
			</td>
			<td>
				<input class='textbold aright' type="text" name="qty<?php echo $i;?>" id="qty<?php echo $i;?>" value="<?php echo set_value('qty'.$i,0);?>" maxlength="12" size="3" tabindex="3" autocomplete="off" onkeyup="
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.bv<?php echo $i;?>,document.form.subtotalbv<?php echo $i;?>);
						document.form.total.value=formatCurrency(ReplaceDoted(total_curr(<?=$counti;?>,'document.form.subtotal'))-ReplaceDoted(document.form.vtotal.value));
						document.form.totalpv.value=formatCurrency(ReplaceDoted(totalpv_curr(<?=$counti;?>,'document.form.subtotalpv'))-ReplaceDoted(document.form.vtotalpv.value));
						document.form.totalbv.value=formatCurrency(ReplaceDoted(totalbv_curr(<?=$counti;?>,'document.form.subtotalbv'))-ReplaceDoted(document.form.vtotalbv.value));
                        if(document.form.total.value < document.form.vminorder.value ) {
                        	alert('Voucher Tidak Dapat digunakan. Minimum Pembelanjaan Rp. '+document.form.vfminorder.value+'. Silahkan tambah order anda dan pilih voucher kembali');
                            document.form.vminorder.value = '0';
                            document.form.vfminorder.value = '0';
                            if(document.form.vselectedvoucher.value == document.form.vouchercode0.value){
                                                        document.form.vselectedvoucher.value = '0';
                                                }else{
                                                        var listselectedvoucher = document.form.vselectedvoucher.value;
                                                        var splitList = listselectedvoucher.split(',');
                                                        var changeList = '';
                                                        var ol;
                                                        for (ol = 0; ol < splitList.length; ol++) {
                                                            if (splitList[ol] != document.form.vouchercode0.value) {
                                                                if(changeList == '')
                                                                    changeList = splitList[ol];
                                                                else
                                                                    changeList = changeList+','+splitList[ol];
                                                            }
                                                        }	
                                                        document.form.vselectedvoucher.value = changeList;
                                                }
                                                cleartext7a(
                                                        document.form.vouchercode0
                                                        ,document.form.vprice0
                                                        ,document.form.vpv0
                                                        ,document.form.vsubtotal0
                                                        ,document.form.vsubtotalpv0
                                                        ,document.form.vbv0
                                                        ,document.form.vsubtotalbv0
                                                    ); 
                                                    document.form.vtotal.value=vtotal_curr(<?=$countv;?>,'document.form.vsubtotal');
                                                    document.form.vtotalpv.value=vtotalpv_curr(<?=$countv;?>,'document.form.vsubtotalpv');
                                                    document.form.vtotalbv.value=vtotalbv_curr(<?=$countv;?>,'document.form.vsubtotalbv');
                                                    if(vtotal_curr(<?=$countv;?>,'document.form.vsubtotal')=='0'){
                                                        document.form.total.value=total_curr(<?=$counti;?>,'document.form.subtotal');
                                                        document.form.totalpv.value=totalpv_curr(<?=$counti;?>,'document.form.subtotalpv');
                                                        document.form.totalbv.value=totalbv_curr(<?=$counti;?>,'document.form.subtotalbv');
                                                    }else{
                                                        document.form.total.value=totalAfterVoucher(total_curr(<?=$counti;?>,'document.form.subtotal'),vtotal_curr(<?=$countv;?>,'document.form.vsubtotal'));
                                                        document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?=$counti;?>,'document.form.subtotalpv'),vtotalpv_curr(<?=$countv;?>,'document.form.vsubtotalpv'));
                                                        document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?=$counti;?>,'document.form.subtotalbv'),vtotalbv_curr(<?=$countv;?>,'document.form.vsubtotalbv'));
                                                    }
                             return true;
                        };
				">
			</td>
			<td>
				<input class="aright" type="text" name="price<?php echo $i;?>" id="price<?php echo $i;?>" size="8" readonly="readonly" value="<?php echo set_value('price'.$i,0);?>"
                    readonly="readonly">
			</td>
			<td>
				<input class="aright" type="text" name="pv<?php echo $i;?>" id="pv<?php echo $i;?>" readonly="readonly" value="<?php echo set_value('pv'.$i,0);?>" size="5" 
                    readonly="readonly">
			</td>
			<td>
				<input class="aright" type="text" name="subtotal<?php echo $i;?>" id="subtotal<?php echo $i;?>" value="<?php echo set_value('subtotal'.$i,0);?>" readonly="1" size="12">
			</td>
			<td>
				<input class="aright" type="text" name="subtotalpv<?php echo $i;?>" id="subtotalpv<?php echo $i;?>" value="<?php echo set_value('subtotalpv'.$i,0);?>" readonly="1" size="10">
			</td>
			<td>
            <!--
				<img alt="delete" 
					onclick="
					cleartext10(
							document.form.itemcode<?php echo $i;?>
							,document.form.itemname<?php echo $i;?>
							,document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>
							,document.form.pv<?php echo $i;?>,document.form.subtotal<?php echo $i;?>
							,document.form.subtotalpv<?php echo $i;?>
							,document.form.titipan_id<?php echo $i;?>
							,document.form.bv<?php echo $i;?>
							,document.form.subtotalbv<?php echo $i;?>
						); 
						document.form.total.value=total_curr(<?=$counti;?>,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(<?=$counti;?>,'document.form.subtotalpv');
						document.form.totalbv.value=totalbv_curr(<?=$counti;?>,'document.form.subtotalbv');" 
					src="<?php echo  base_url();?>images/backend/delete.png" border="0"
				/>
             -->
				<img alt="delete" 
					onclick="
					cleartext9(
							document.form.itemcode<?php echo $i;?>
							,document.form.itemname<?php echo $i;?>
							,document.form.qty<?php echo $i;?>
                            ,document.form.price<?php echo $i;?>
							,document.form.pv<?php echo $i;?>
                            ,document.form.subtotal<?php echo $i;?>
							,document.form.subtotalpv<?php echo $i;?>
							,document.form.bv<?php echo $i;?>
							,document.form.subtotalbv<?php echo $i;?>
						); 
                        document.form.total.value=totalAfterVoucher(total_curr(<?=$counti;?>,'document.form.subtotal'),vtotal_curr(<?=$countv;?>,'document.form.vsubtotal'));
                        document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?=$counti;?>,'document.form.subtotalpv'),vtotalpv_curr(<?=$countv;?>,'document.form.vsubtotalpv'));
                        document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?=$counti;?>,'document.form.subtotalbv'),vtotalbv_curr(<?=$countv;?>,'document.form.vsubtotalbv'));
                        " 
					src="<?php echo  base_url();?>images/backend/delete.png" border="0"
				/>
			 </td>
		</tr>
			<?php $i++;
		}
		?>
		<!--
		<tr>
			<td colspan='5'>Add <input name="rowx" type="text" id="rowx" value="<?=set_value('rowx','1');?>" size="1" maxlength="2" />
          Row(s)<?php echo form_submit('action', 'Go');?></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total',0);?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv',0);?>" readonly="1" size="9">
            	<?php echo form_hidden('totalbv',set_value('totalbv',0));?>
            </td>
		</tr>
        -->
		<tr>
			<td colspan='5'>Add <input name="rowx" type="text" id="rowx" value="<?=set_value('rowx','1');?>" size="1" maxlength="2" />
          Row(s)<?php echo form_submit('action', 'Go');?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
        <!-- Voucher by ASP 20151202 -->	
        <?php //Created By ASP 20151201 ?>
		<tr><td colspan="8"><hr/></td></tr>
		<tr>
			<td width='18%'>Voucher Code</td>
			<td width='23%'>&nbsp;</td>
			<td width='8%'>&nbsp;</td>
			<td width='12%'>Rp Value</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>&nbsp;</td>
		</tr>
		<?php $v=0; 
		echo form_hidden('vselectedvoucher',set_value('vselectedvoucher',0));
		echo form_hidden('vminorder',set_value('vminorder',0));
		echo form_hidden('vfminorder',set_value('vfminorder',0));
		while($v < $countv){?>
		<tr>
			<td colspan='3'><?php 
				echo form_hidden('vcounter[]',$v); 
				$data = array('name'=>'vouchercode'.$v,'id'=>'vouchercode'.$v,'size'=>'8','readonly'=>'1','value'=>set_value('vouchercode'.$v)); 
				echo form_input($data);
				$atts = array(
					'width'      => '600',
					'height'     => '500',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'no',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				//echo anchor_popup('search/voucher/index/'.$this->session->userdata('userid').'/'.$v, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts); 
				//echo anchor_popup("search/voucher/index/'+document.form.member_id.value+'/".$v, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts); 
				echo anchor_popup("search/voucher/sostc/'+document.form.member_id.value+'/".$v, '<input class="button" type="button" tabindex="2" name="Button" value="browse"/>', $atts); 
				echo form_hidden('vbv'.$v,set_value('vbv'.$v,0));
				echo form_hidden('vsubtotalbv'.$v,set_value('vsubtotalbv'.$v,0));
			?></td>
			<td>
				<input class="aright" type="text" name="vprice<?php echo $v;?>" id="vprice<?php echo $v;?>" size="8" 
					value="<?php echo set_value('vprice'.$v,0);?>"
                    readonly="readonly">
			</td>
			<td>
				<input class="aright" type="text" name="vpv<?php echo $v;?>" id="vpv<?php echo $v;?>"
					value="<?php echo set_value('vpv'.$v,0);?>" size="5" 
                	readonly="readonly">
			</td>
            <td>
				<input class="aright" type="text" name="vsubtotal<?php echo $v;?>" id="vsubtotal<?php echo $v;?>" value="<?php echo set_value('vsubtotal'.$v,0);?>" readonly="1" size="12">
			</td>
			<td>
				<input class="aright" type="text" name="vsubtotalpv<?php echo $v;?>" id="vsubtotalpv<?php echo $v;?>" value="<?php echo set_value('vsubtotalpv'.$v,0);?>" readonly="1" size="10">
			</td>
			<td>
				<img alt="delete" 
					onclick="
                    if(document.form.vselectedvoucher.value == document.form.vouchercode<?php echo $v;?>.value){
                            document.form.vselectedvoucher.value = '0';
                    }else{
                            var listselectedvoucher = document.form.vselectedvoucher.value;
                            var splitList = listselectedvoucher.split(',');
                            var changeList = '';
                            var ol;
                            for (ol = 0; ol < splitList.length; ol++) {
                                if (splitList[ol] != document.form.vouchercode<?php echo $v;?>.value) {
                                	if(changeList == '')
                                        changeList = splitList[ol];
                                    else
                                        changeList = changeList+','+splitList[ol];
                                }
                            }	
                            document.form.vselectedvoucher.value = changeList;
                    }
					cleartext7a(
							document.form.vouchercode<?php echo $v;?>
                            ,document.form.vprice<?php echo $v;?>
							,document.form.vpv<?php echo $v;?>
                            ,document.form.vsubtotal<?php echo $v;?>
							,document.form.vsubtotalpv<?php echo $v;?>
							,document.form.vbv<?php echo $v;?>
							,document.form.vsubtotalbv<?php echo $v;?>
						);
                        document.form.vtotal.value=vtotal_curr(<?=$countv;?>,'document.form.vsubtotal');
                        document.form.vtotalpv.value=vtotalpv_curr(<?=$countv;?>,'document.form.vsubtotalpv');
                        document.form.vtotalbv.value=vtotalbv_curr(<?=$countv;?>,'document.form.vsubtotalbv');
                        if(vtotal_curr(<?=$countv;?>,'document.form.vsubtotal')=='0'){
                        	document.form.total.value=total_curr(<?=$counti;?>,'document.form.subtotal');
                        	document.form.totalpv.value=totalpv_curr(<?=$counti;?>,'document.form.subtotalpv');
                        	document.form.totalbv.value=totalbv_curr(<?=$counti;?>,'document.form.subtotalbv');
                        }else{
                            document.form.total.value=totalAfterVoucher(total_curr(<?=$counti;?>,'document.form.subtotal'),vtotal_curr(<?=$countv;?>,'document.form.vsubtotal'));
                            document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?=$counti;?>,'document.form.subtotalpv'),vtotalpv_curr(<?=$countv;?>,'document.form.vsubtotalpv'));
                            document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?=$counti;?>,'document.form.subtotalbv'),vtotalbv_curr(<?=$countv;?>,'document.form.vsubtotalbv'));
                        }
                        "
					src="<?php echo  base_url();?>images/backend/delete.png" border="0"
				/>
			 </td>
		</tr>	
        <?php $v++;
		}
		?>
        <tr>
			<td colspan='5'>&nbsp;</td>
			<td><input class='textbold aright' type="text" name="vtotal" id="vtotal" value="<?php echo set_value('vtotal',0);?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="vtotalpv" id="vtotalpv" value="<?php echo set_value('vtotalpv',0);?>" readonly="1" size="9">
            	<?php echo form_hidden('vtotalbv',set_value('vtotalbv',0));?>
            </td>
		</tr>	
		<tr><td colspan="8"><hr/></td></tr>
        <tr>
			<td colspan='5' align="right">&nbsp; <b>Total Pembelanjaan</b></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total',0);?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv',0);?>" readonly="1" size="9">
            	<?php echo form_hidden('totalbv',set_value('totalbv',0));?>
            </td>
		</tr>
		<!-- EOF Voucher by ASP 20151202-->  
		<?php if(validation_errors()){?>
		<tr>
			<td colspan='100%' align="center" bgcolor="#FFAABF"><span class="error"><?php echo form_error('pin'); ?><?php echo form_error('vminorder');?></span></td>
		</tr>
		<?php }?>
        <tr>
			<td colspan='7' align='right' valign='top'>PIN : <input type="password" name="pin" id="pin" value="" maxlength="50" size="12" tabindex="22"/> 
			<!--<span class="error">* <?php echo form_error('pin'); ?></span>-->
			</td>
		</tr>
		<tr>
			<td colspan='6'>&nbsp;</td>
			<td><!-- <input type="submit" onclick="this.disabled=true; this.value='Sending, please wait...'; this.form.submit();" />-->
            <?php $this->load->view('submit_confirm');?></td>
		</tr>
		
		</table>
        
<?php echo form_close();?>

<?php $this->load->view('footer');?>
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>