<script type="text/javascript" src="<?php echo base_url();?>js/order.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/popup.css" />
<table width="100%" border="0" cellpadding="5" cellspacing="5">
<tr><td>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr><td class='bg_border' align='center'>Browse Voucher</td></tr>
		<tr><td class='td_border2'>
			<?php echo form_open('search/voucher/index/'.$this->uri->segment(4).'/'.$this->uri->segment(5), array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width='10%'>Search </td>
				<td width='1%'>:</td>
    			<td width='89%'><?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','search');?></td>
			</tr>
			<?php if($this->session->userdata('keywords')){ ?>
				<tr><td colspan='2'>&nbsp;</td>
				<td>Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b></td>
				</tr>
			<?php }?>
			
			</table>	
			<?php echo form_close();?>
				<?php if($results) { ?>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">				
				<tr class='title_table'>
					<td class='td_report' width='5%'>No.</td>
					<td class='td_report' width='11%'>Voucher Code</td>
					<td class='td_report' width='17%'>Rp Value</td>
					<td class='td_report' width='17%'>PV</td>
					<td class='td_report' width='17%'>BV</td>
					<td class='td_report' width='17%'>Remark</td>
				</tr>
				<?php $counter = $from_rows; 
				foreach($results as $row) { $counter = $counter + 1; 
				$price = $row['price'];
				$minorder = $row['minorder'];
				?>
                
				<tr height='22' class='lvtColData' onmouseover='this.className="lvtColDataHover"' onmouseout='this.className="lvtColData"' 
				 onclick="
                 <?php if($typevouchersearch == '0'){ ?>
                        if(parseInt (ReplaceDoted(window.opener.document.form.total.value)) >= <?php echo $row['minorder']; ?>){
                            if(window.opener.document.form.vselectedvoucher.value =='0'){ 
                                window.opener.document.form.vselectedvoucher.value= '<?php echo $row['vouchercode'];?>';
                                window.opener.document.form.vminorder.value= '<?php echo $row['minorder'];?>';
                                window.opener.document.form.vfminorder.value= '<?php echo $row['fminorder'];?>';
                                }else{
                                    if(window.opener.document.form.vselectedvoucher.value == '<?php echo $row['vouchercode'];?>'){
                                            alert('Voucher Sudah dipilih, silahkan pilih yang lain!');
                                            return False;
                                    }else{
                                    var svoucher = window.opener.document.form.vselectedvoucher.value;
                                    var listSvoucher = svoucher.split(',');
                                    
                                    if(containsObject('<?php echo $row['vouchercode'];?>', listSvoucher)){
                                            alert('Voucher Sudah dipilih, silahkan pilih yang lain!');
                                            return False;
                                    }else{
                                        window.opener.document.form.vselectedvoucher.value= window.opener.document.form.vselectedvoucher.value+','+'<?php echo $row['vouchercode'];?>';
                                        window.opener.document.form.vminorder.value= window.opener.document.form.vminorder.value+','+'<?php echo $row['minorder'];?>';
                                        window.opener.document.form.vfminorder.value= window.opener.document.form.vfminorder.value+','+'<?php echo $row['fminorder'];?>';
                                    }
                                    }
                                }
                        }else{
                            alert('Voucher tidak dapat digunakan. Minimum Pembelanjaan Rp. <?php echo $row['fminorder']; ?>');
                            return False;
                        };
                        <?php }elseif( $typevouchersearch == '1'){ ?>
                            if(window.opener.document.form.vselectedvoucher.value =='0'){ 
                                window.opener.document.form.vselectedvoucher.value= '<?php echo $row['vouchercode']; ?>';
                            }else{
                                    if(window.opener.document.form.vselectedvoucher.value == '<?php echo $row['vouchercode']; ?>'){
                                            alert('Voucher Sudah dipilih, silahkan pilih yang lain!');
                                            return False;
                                    }else{
                                    var svoucher = window.opener.document.form.vselectedvoucher.value;
                                    var listSvoucher = svoucher.split(',');
                                    
                                    if(containsObject('<?php echo $row['vouchercode'];?>', listSvoucher)){
                                            alert('Voucher Sudah dipilih, silahkan pilih yang lain!');
                                            return False;
                                    }else{
                                        window.opener.document.form.vselectedvoucher.value= window.opener.document.form.vselectedvoucher.value+','+'<?php echo $row['vouchercode']; ?>';
                                    }
                                    }
                            };
                        <?php } ?>
                  window.opener.document.form.vouchercode<?php echo $this->uri->segment(5);?>.value ='<?php echo $row['vouchercode'];?>';
                  window.opener.document.form.vprice<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($price,'0','','.');?>';
				  window.opener.document.form.vpv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['pv'],'0','','.');?>';
                  window.opener.document.form.vbv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['bv'],'0','','.');?>';
				  window.opener.document.form.vsubtotal<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($price,'0','','.');?>';
				    window.opener.document.form.vsubtotalpv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['pv'],'0','','.');?>';
                    window.opener.document.form.vsubtotalbv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['bv'],'0','','.');?>';
                  window.opener.document.form.vtotalbv.value=vtotalbv_curr(<?=$this->session->userdata('countv');?>,'window.opener.document.form.vsubtotalbv');
                  window.opener.document.form.vtotal.value=vtotal_curr(<?=$this->session->userdata('countv');?>,'window.opener.document.form.vsubtotal');
                  window.opener.document.form.vtotalpv.value=vtotalpv_curr(<?=$this->session->userdata('countv');?>,'window.opener.document.form.vsubtotalpv');
                  
                  window.opener.document.form.total.value=totalAfterVoucher(total_curr(<?=$this->session->userdata('counti');?>,'window.opener.document.form.subtotal'),vtotal_curr(<?=$this->session->userdata('countv');?>,'window.opener.document.form.vsubtotal'));
                  window.opener.document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?=$this->session->userdata('counti');?>,'window.opener.document.form.subtotalpv'),vtotalpv_curr(<?=$this->session->userdata('countv');?>,'window.opener.document.form.vsubtotalpv'));
                  window.opener.document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?=$this->session->userdata('counti');?>,'window.opener.document.form.subtotalbv'),vtotalbv_curr(<?=$this->session->userdata('countv');?>,'window.opener.document.form.vsubtotalbv'));
                  window.opener.document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?=$this->session->userdata('counti');?>,'window.opener.document.form.subtotalbv'),vtotalbv_curr(<?=$this->session->userdata('countv');?>,'window.opener.document.form.vsubtotalbv'));
                  window.opener.document.form.oi.value='1';
                  window.opener.document.form.coba.value='1';
                  <?php echo $prosestambahan; ?>
                  window.close(); ">
					<td class='td_report'><?php echo $counter;?></td>
					<td class='td_report'><?php echo $row['vouchercode'];?></td>
					<td class='td_report'><?php echo number_format($price,'0','',',');?></td>
					<td class='td_report'><?php echo number_format($row['pv']);?></td>
                    <td class='td_report'><?php echo number_format($row['bv']);?></td>
                    <td class='td_report'><?php echo $row['remark'];?></td>
				</tr>
				<?php }?>
				<tr><td colspan='5'><?php echo $this->pagination->create_links(); ?></td></tr>
				</table>
				<?php }?>
		</td></tr>
</table>
		
</td></tr>
</table>