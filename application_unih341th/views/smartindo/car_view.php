<?php $this->load->view('header');?>
<div id="view"><?php echo anchor('smartindo/car_bns/edit/'.$row->id,'Edit Term');?></div>

<div class="ViewHold">
	<div id="companyDetails"><?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?></div>
	<p>
		<table>
			<tr>	
				<td><strong>Member id / Name</strong></td>
				<td><strong>: <?php echo $row->member_id." / ".$row->nama;?></strong></td>
			</tr>
			<tr>
				<td><strong>Title</strong></td>
				<td><strong>: <?php echo $row->jenjang;?></strong></td>
			</tr>
			<tr>
				<td><strong>Bonus Type</strong></td>
				<td><strong>: <?php echo $row->tipe;?> (
					<?php 
						if($row->tipe=="COP"){
							echo number_format($row->cop)." / ".number_format($row->copCount)."x";
						}else{
							echo number_format($row->ccb)." / ".number_format($row->ccbCount)."x";
						} ?>)</strong></td>
			</tr>
			<tr>
				<td><strong>Start from</strong></td>
				<td><strong>: 
					<?php 
						if($row->tipe=="COP"){
							echo ($row->cop_start);
						}else{
							echo ($row->ccb_start);
						} ?></strong></td>
			</tr>
			<tr>
				<td><strong>Status</strong></td>
				<td><strong>: 
					<?php echo ($row->status_); ?></strong></td>
			</tr>
		</table>
	</p>
</div>
<?php $this->load->view('footer');?>