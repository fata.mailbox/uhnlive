<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php 
	echo anchor('smartindo/ro_vwh/create','create request order'); // update by Boby 20140416

	if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>

<table width='99%'>
<?php echo form_open('smartindo/ro_vwh', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='60%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='40%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b>
			<?php }?>
			
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
	<tr>
      <th width='5%'>No.</th>
      <th width='5%'>Invoice No.</th>
      <th width='13%'>Date</th>
      <th width='38%'>Stockiest ID / Name</th>
      <th width='12%'><div align="right">Total Price</div></th>
      <th width='12%'><div align="right">Total PV</div></th>
      <th width='8%'>Status</th>
    </tr>
   
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
		<td><?php echo anchor('smartindo/ro_vwh/view/'.$row['id'], $counter);?></td>
		<td><?php echo anchor('smartindo/ro_vwh/view/'.$row['id'], $row['id']);?></td>
		<td><?php echo anchor('smartindo/ro_vwh/view/'.$row['id'], $row['date']);?></td>
		<td><?php echo anchor('smartindo/ro_vwh/view/'.$row['id'], $row['no_stc']." - ".$row['nama']);?></td>
		<td align="right"><?php echo anchor('smartindo/ro_vwh/view/'.$row['id'], $row['ftotalharga']);?></td>
		<td align="right"><?php echo anchor('smartindo/ro_vwh/view/'.$row['id'], $row['ftotalpv']);?></td>
		<td><?php echo anchor('smartindo/ro_vwh/view/'.$row['id'], $row['status']);?></td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php 
$this->load->view('footer');
?>