<?php //var_dump($result); 
?>

<!DOCTYPE html>
<html>

<head>
	<title>Pilih Item Top Up Demand</title>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>css/popup.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<script type="text/javascript">
	function checkAll(ele) {
		var checkboxes = document.getElementsByTagName('input');
		if (ele.checked) {
			for (var i = 0; i < checkboxes.length; i++) {
				if (checkboxes[i].type == 'checkbox') {
					checkboxes[i].checked = true;
				}
			}
		} else {
			for (var i = 0; i < checkboxes.length; i++) {
				if (checkboxes[i].type == 'checkbox') {
					checkboxes[i].checked = false;
				}
			}
		}
	}

	function getval() {

			var vals = [];
			$("input:checkbox[name=code]:checked").each(function() {
				vals.push($(this).val());
				var getvalue = $(this).val().split('|');
				window.opener.update_demand();
			});
			window.opener.getArrfrom(vals, <?php echo $multiple; ?>, 'topupdemand');
			window.opener.doneForm(1);
			window.opener.update_demand();
			window.close();
			window.opener.checkvalue();
	}
</script>
<body>
	<input id=" result" type="hidden" value="<?= count($result) ?>">
	<h5>Pilih Item Top Up Demand</h5>
	<table width="100%"  style="border: solid 0.5px gray" cellpadding="5" cellspacing="5">
	<tr>
		<td>
		<table width="100%"  cellpadding="0" cellspacing="0">
		<thead>
			<tr class='title_table'>
				<td>
				<input type="checkbox" onclick="checkAll(this);">
				</td>
				<td>TopUp No</td>
				<td>Item Code</td>
				<td>Item Name</td>
				<td >Price</td>
				<td>PV</td>
				<td>WH Pengirim</td>
				<td>QTY</td>
				<td>Stock Tersedia</td>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($result as $value) { ?>
				<?php 
			 	$getitem = $this->ValueTopup_model->item_id($value->item_code);
				?>
				<?php 
				$whs_id = $getitem->warehouse_id;
				if ($whs_id == 0) {
					$s = $this->session->userdata('r_whsid');
				}else{
					$s = $whs_id;
				}
				$wh_pengirim = get_data_by_tabel_id($s,'warehouse');
				$item = $value->item_code;
				$qty = intval($value->qty_sum);
				$get = cekadj($item, $s);
				$asli = HitungDiscount($value->item_code,$persen);
				$bv = $getitem->bv;
				$pv = $getitem->pv;
				if (empty($bv) and empty($pv)) {
					$act_diskon = 'xnxx';
				}else {
					if (!empty($asli)) {
						$act_diskon = 'diskon';
					}else {
						$act_diskon = 'xnxx';
					}
				}
				$harga_discount_price=  $getitem->price;
				$harga_discount_pv = $pv;
				
				$qty_asli = $get;
				if ($get > $qty) {
					$qty_asli_masuk = $qty;
				}else {
					$qty_asli_masuk = $get;
				}

				if ($this->session->userdata('ro_whsid') != '') {
					$wareware = $this->session->userdata('ro_whsid');
				} elseif ($this->session->userdata('s_whsid') != '') {
					$wareware = $this->session->userdata('s_whsid');
				} else {
					$wareware = $getitem->warehouse_id;
				}
				?>
				<tr class='lvtColData'>
					<input type="hidden" id="auto" value="<?= $s; ?>" />
					<input type="hidden" id="adj" value="<?= $st; ?>" />
					<input type="hidden" id="asli" value="<?= $qty_asli; ?>" />
					<td class='td_report'>
					<?php if($qty_asli > 0 ) :?>
						<input type="checkbox" name="code" class="messageCheckbox" id="foo"
						 value="<?php echo
					$value->item_code . '|' . $qty_asli_masuk . '|' . $getitem->name . '|' . $harga_discount_price * intval($qty_asli_masuk) . '|' . $harga_discount_pv * intval($qty_asli_masuk) . '|' . $harga_discount_pv * intval($qty_asli_masuk) . '|1' . '|' . $wareware . '|' . $st . '|' . intval($qty_asli_masuk) . '|' . $value->parent_upload . '|' . $value->id . '|' . $stc_id . '|' .  $harga_discount_price . '|' . $harga_discount_pv . '|' . $get . '|' . $s.'|'.'yes'.'|'.$act_diskon ?>" />
						<?php endif;?>
					</td>
					<td class='td_report'><?php echo $value->parent_upload; ?></td>
					<td class='td_report'><?php echo $value->item_code; ?></td>
					<td class='td_report'><?php echo $getitem->name; ?></td>
					<td class='td_report'><?php echo number_format($harga_discount_price); ?></td>
					<td class='td_report'><?php echo number_format($harga_discount_pv); ?></td>
					<td class='td_report'><?php echo $wh_pengirim->name; ?></td>
					<td class='td_report'><?php echo $qty; ?></td>
					<td class='td_report'><?php echo $qty_asli; ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</td>
</tr>
<tr>
<td>
<button onclick="getval();" class="button" style="padding: 3px 6px; margin: 5px;">Submit</button>
</td>
</tr>
	</div>
</table>	
</body>
</html>