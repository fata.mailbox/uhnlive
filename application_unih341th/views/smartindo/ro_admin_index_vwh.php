<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php 
	echo anchor('smartindo/roadmin_vwh/create','create request order'); // update by Boby 20140416

	if ($this->session->flashdata('message')){
	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>

<table width='99%'>
<?php echo form_open('smartindo/roadmin_vwh', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='50%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='50%' align='right'>search by: <?php if($this->session->userdata('group_id') < 100 && $this->session->userdata('group_id')!= 28) echo form_dropdown('whsid',$warehouse);?> <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b>
			<?php }?>
			
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<?php echo form_open('smartindo/roadmin_vwh/approved/', array('id' => 'my_form2', 'name' => 'my_form2', 'autocomplete' => 'off'));?>
<table class="stripe">
	<tr>
		<td colspan='2' valign='top'>remark: </td>
		<td colspan='11'><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
			echo form_textarea($data);?><br>
			<?php echo form_submit('submit','approved');?>
		</td>
	</tr>
	<tr>
	    <th width='1%'>PST</th>
	    <th width='1%'>HUB</th>
      <th width='5%'>No.</th>
      <th width='5%'>Invoice No.</th>
      <th width='13%'>Date</th>
      <th width='27%'>Stockiest ID / Name</th>
      <th width='11%'><div align="right">Total Price</div></th>
      <th width='11%'><div align="right">Total PV</div></th>
      <th width='11%'><div align="right">Warehouse</div></th>
      <th width='11%'><div align="right">Warehouse Pengiriman</div></th>
      <th width='11%'><div align="right">Req Time</div></th>
      <th width='11%'><div align="right">App Time</div></th>
      <th width='6%'>Status</th>
    </tr>
   
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
	$getWhPengiriman = $this->GLobal_model->getWhPengiriman($row['id']);
?>
    <tr>
		<td>
			<?php if($row['status1'] == 'pending'){ 
				$data = array(
				'name'        => 'p1_id[]',
				'id'          => 'p1_id[]',
				'value'       => $row['id'],
				'checked'     => false,
				'style'       => 'border:none'
				);
				echo form_checkbox($data); } else {?>&nbsp;<?php }?> </td>
		<td>
			<?php if($row['status2'] == 'pending'){ 
				$data = array(
				'name'        => 'p2_id[]',
				'id'          => 'p2_id[]',
				'value'       => $row['id'],
				'checked'     => false,
				'style'       => 'border:none'
				);
				echo form_checkbox($data); } else {?>&nbsp;<?php }?> </td>
		<td><?php echo anchor('smartindo/roadmin_vwh/view/'.$row['id'], $counter);?></td>
		<td><?php echo anchor('smartindo/roadmin_vwh/view/'.$row['id'], $row['id']);?></td>
		<td><?php echo anchor('smartindo/roadmin_vwh/view/'.$row['id'], $row['date']);?></td>
		<td><?php echo anchor('smartindo/roadmin_vwh/view/'.$row['id'], $row['no_stc']." - ".$row['nama']);?></td>
		<td align="right"><?php echo anchor('smartindo/roadmin_vwh/view/'.$row['id'], $row['ftotalharga']);?></td>
		<td align="right"><?php echo anchor('smartindo/roadmin_vwh/view/'.$row['id'], $row['ftotalpv']);?></td>
		<td align="right"><?php echo anchor('smartindo/roadmin_vwh/view/'.$row['id'], $row['warehouse_name']);?></td>
		<td align="right"><?php echo anchor('smartindo/roadmin_vwh/view/'.$row['id'], $getWhPengiriman);?></td>
		<td align="right"><?php echo anchor('smartindo/roadmin_vwh/view/'.$row['id'], $row['created']);?></td>
		<td align="right"><?php echo anchor('smartindo/roadmin_vwh/view/'.$row['id'], $row['appdate']);?></td>
		<td><?php echo anchor('smartindo/roadmin_vwh/view/'.$row['id'], $row['status']);?></td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="8">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php echo form_close();
$this->load->view('footer');3
?>