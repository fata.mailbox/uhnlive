<!--
	Copyright (c) 2014-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('smartindo/point_ord/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		
		<table width='100%'>
		<?php if(validation_errors()){?>
		<tr>
			<td colspan='3'><span class="error"><?php echo form_error('total');?></span></td>
		</tr>
		<?php }?>
		<tr>
			<td width='20%'>date</td>
			<td width='1%'>:</td>
			<td width='79%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td valign='top'>NIK</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_hidden('member_id',set_value('member_id','')); $data = array('name'=>'no_stc','id'=>'no_stc','size'=>15,'readonly'=>'1','value'=>set_value('no_stc'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('search/searchuhnemp/index/0', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?><span class='error'>*<?php echo form_error('member_id');?></span></td>
					
		</tr>
		<tr>
			<td valign='top'>Employee Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>Point Balance</td>
			<td valign='top'>:</td>
			<td>
				<input type="text" name="point" id="point" readonly="1" value="<?php echo set_value('point');?>" size="30" />
				<span class='error'>*<?php echo form_error('point');?></span>
			</td>
		</tr>
        <tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('whsid',$warehouse);?></td>
		</tr>
        <tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','tabindex'=>'1','value'=>set_value('remark'));
    					echo form_textarea($data);?></td> 
		</tr>
		
		</table>
		
		<table width='100%'>	
		<tr>
			<td width='18%'>Item Code</td>
			<td width='23%'>Item Name</td>
			<td width='8%'>Qty</td>
			<td width='21%'>Price</td>
			<td width='26%'>Sub Total Price</td>
			<td width='5%'>Del?</td>
		</tr>
		<?php
			for($i=0;$i<10;$i++){
		?>
		<tr>
			<td valign='top'>
				<?php 
					$data = array('name'=>'itemcode'.$i,'id'=>'itemcode'.$i,'size'=>'8','readonly'=>'1','value'=>set_value('itemcode'.$i)); echo form_input($data);
					$atts = array(
						'width'      => '600',
						'height'     => '500',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'no',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					echo anchor_popup('itemstfpointsearch/index/'.$i, '<input class="button" type="button" tabindex="2" name="Button" value="browse" />', $atts); 
				?>
			<td valign='top'><input type="text" name="itemname<?php echo $i;?>" id="itemname<?php echo $i;?>" value="<?php echo set_value('itemname'.$i);?>" readonly="1" size="24" /></td>
			<td><input class='textbold aright' type="text" name="qty<?php echo $i;?>" id="qty<?php echo $i;?>" value="<?php echo set_value('qty'.$i,0);?>" maxlength="12" size="3" tabindex="3" autocomplete="off" 
				onkeyup="this.value=formatCurrency(this.value); 
				jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
				document.form.total.value=total_curr(5,'document.form.subtotal');">
			</td>
			<td><input class="aright" type="text" name="price<?php echo $i;?>" id="price<?php echo $i;?>" value="<?php echo set_value('price'.$i,0);?>" size="8" readonly="1"></td>
			<td><input class="aright" type="text" name="subtotal<?php echo $i;?>" id="subtotal<?php echo $i;?>" value="<?php echo set_value('subtotal'.$i,0);?>" readonly="1" size="12"></td>
			<td><img alt="delete" onclick="cleartext7(document.form.itemcode<?php echo $i;?>
				,document.form.itemname<?php echo $i;?>
				,document.form.qty<?php echo $i;?>
				,document.form.price<?php echo $i;?>
				,document.form.subtotal<?php echo $i;?>
				);
				document.form.total.value=total_curr(10,'document.form.subtotal');" src="<?php echo  base_url();?>images/backend/delete.png" border="0"/>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td colspan='4'></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total',0);?>" readonly="1" size="11"></td>
		</tr>	
		<tr>
			<td colspan='6'>&nbsp;</td>
			<td><?php $this->load->view('submit_confirm');?><?php // echo form_submit('submit', 'Submit', 'tabindex="23"');?></td>
		</tr>
		
		</table>
		
<?php echo form_close();?>

<?php $this->load->view('footer');?>
