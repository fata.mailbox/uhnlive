<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>



<style>
#detail {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#detail td, #detail th {
  border: 1px solid #ddd;
  padding: 8px;
}

#detail tr:nth-child(even){background-color: #f2f2f2;}

#detail tr:hover {background-color: #ddd;}

#detail th {
  padding-top: 12px;
  padding-bottom: 12px;
  font-size: 13px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>

<script type="text/javascript">
	$(document).ready( function () {
	    tbl = $('#detail').DataTable();
	} );
</script>

<table   id="detail">
	<thead>
		<tr >
			<th>No</th>
			<th>TopUp No</th>
			<th>Valid From</th>
			<th>Valid To</th>
			<th>Status</th>
			<th>Option</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach ($result as $value) { ?>
		<tr>
			<td><?php echo $no; ?></td>
			<td><?php echo $value->topupno; ?></td>
			<td><?php echo $value->valid_from; ?></td>
			<td><?php echo $value->valid_to; ?></td>
			<td><?php if($value->status == 1) {echo "Aktif";} else { echo "Tidak Aktif";}; ?></td>
			<td><a href="<?php echo base_url(); ?>smartindo/loyalti/view/<?php echo $value->topupno; ?>">Detail</a></td>
		</tr>
		<?php $no++;} ?>
	</tbody>
</table>

<?php
$this->load->view('footer');
?>