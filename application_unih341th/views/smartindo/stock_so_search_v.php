<script type="text/javascript" src="<?php echo base_url();?>js/order.js"></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/popup.css" />


<table width="100%" border="0" cellpadding="5" cellspacing="5">
<tr><td>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr><td class='bg_border' align='center'>Browse Stock</td></tr>
		<tr><td class='td_border2'>
			
			<?php echo form_open('search/stock/so_v/'.$this->uri->segment(4).'/'.$this->uri->segment(5), array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width='10%'>Search </td>
				<td width='1%'>:</td>
    			<td width='89%'><?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','search');?></td>
			</tr>
			<?php if($this->session->userdata('keywords')){ ?>
				<tr><td colspan='2'>&nbsp;</td>
				<td>Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b></td>
				</tr>
			<?php }?>
			
			</table>	
			<?php echo form_close();?>
				<?php if($results) {?>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">				
				<tr class='title_table'>
					<td class='td_report' width='5%'>No.</td>
					<td class='td_report' width='11%'>Item Code</td>
					<td class='td_report' width='40%'>Item Name</td>
					<td class='td_report' width='17%'>Price</td>
					<td class='td_report' width='17%'>PV</td>
                    <td class='td_report' width='10%'>Qty</td>
				</tr>
				<?php $counter = $from_rows; 
				foreach($results as $row) { $counter = $counter + 1; 
				if($this->session->userdata('s_timur') == '1' and $this->session->userdata('s_pu') == '1')$price = $row['price2']; else $price = $row['price'];?>
                <!--
				<tr height='22' class='lvtColData' onmouseover='this.className="lvtColDataHover"' onmouseout='this.className="lvtColData"' 
				 onclick="window.opener.document.form.itemcode<?php echo $this->uri->segment(5);?>.value ='<?php echo $row['item_id'];?>';
				  window.opener.document.form.itemname<?php echo $this->uri->segment(5);?>.value ='<?php echo $row['name'];?>';
                  window.opener.document.form.price<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($price,'0','','.');?>';
				  window.opener.document.form.pv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['pv'],'0','','.');?>';
                  window.opener.document.form.bv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['bv'],'0','','.');?>';
				  window.opener.document.form.qty<?php echo $this->uri->segment(5);?>.value ='1';
				  window.opener.document.form.subtotal<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($price,'0','','.');?>';
				  window.opener.document.form.total.value=total_curr(<?=$this->session->userdata('counti');?>,'window.opener.document.form.subtotal');
				  window.opener.document.form.subtotalpv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['pv'],'0','','.');?>';
                  window.opener.document.form.totalpv.value=totalpv_curr(<?=$this->session->userdata('counti');?>,'window.opener.document.form.subtotalpv');
                  window.opener.document.form.subtotalbv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['bv'],'0','','.');?>';
				  window.opener.document.form.totalbv.value=totalbv_curr(<?=$this->session->userdata('counti');?>,'window.opener.document.form.subtotalbv');
				  window.close();">
					<td class='td_report'><?php echo $counter;?></td>
					<td class='td_report'><?php echo $row['item_id'];?></td>
					<td class='td_report'><?php echo $row['name'];?></td>
					<td class='td_report'><?php echo number_format($price,'0','',',');?></td>
					<td class='td_report'><?php echo $row['fpv'];?></td>
                    <td class='td_report'><?php echo $row['fqty'];?></td>
				</tr>
                -->
				<tr height='22' class='lvtColData' onmouseover='this.className="lvtColDataHover"' onmouseout='this.className="lvtColData"' 
				 onclick="window.opener.document.form.itemcode<?php echo $this->uri->segment(5);?>.value ='<?php echo $row['item_id'];?>';
				  window.opener.document.form.itemname<?php echo $this->uri->segment(5);?>.value ='<?php echo $row['name'];?>';
                  window.opener.document.form.price<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($price,'0','','.');?>';
				  window.opener.document.form.pv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['pv'],'0','','.');?>';
                  window.opener.document.form.bv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['bv'],'0','','.');?>';
				  window.opener.document.form.qty<?php echo $this->uri->segment(5);?>.value ='1';
				  window.opener.document.form.subtotal<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($price,'0','','.');?>';
                  window.opener.document.form.total.value=totalAfterVoucher(total_curr(<?=$this->session->userdata('counti');?>,'window.opener.document.form.subtotal'),vtotal_curr(<?=$this->session->userdata('countv');?>,'window.opener.document.form.vsubtotal'));
				  window.opener.document.form.subtotalpv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['pv'],'0','','.');?>';
                  window.opener.document.form.totalpv.value=totalAfterVoucher(totalpv_curr(<?=$this->session->userdata('counti');?>,'window.opener.document.form.subtotalpv'),vtotalpv_curr(<?=$this->session->userdata('countv');?>,'window.opener.document.form.vsubtotalpv'));
                  window.opener.document.form.subtotalbv<?php echo $this->uri->segment(5);?>.value ='<?php echo number_format($row['bv'],'0','','.');?>';
                  window.opener.document.form.totalbv.value=totalAfterVoucher(totalbv_curr(<?=$this->session->userdata('counti');?>,'window.opener.document.form.subtotalbv'),vtotalbv_curr(<?=$this->session->userdata('countv');?>,'window.opener.document.form.vsubtotalbv'));
				  window.close();">
					<td class='td_report'><?php echo $counter;?></td>
					<td class='td_report'><?php echo $row['item_id'];?></td>
					<td class='td_report'><?php echo $row['name'];?></td>
					<td class='td_report'><?php echo number_format($price,'0','',',');?></td>
					<td class='td_report'><?php echo $row['fpv'];?></td>
                    <td class='td_report'><?php echo $row['fqty'];?></td>
				</tr>
				<?php }?>
				<tr><td colspan='5'><?php echo $this->pagination->create_links(); ?></td></tr>
				</table>
				<?php }?>
		</td></tr>
</table>
		
</td></tr>
</table>