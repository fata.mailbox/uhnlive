<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php
	if ($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}
	echo form_open('smartindo/soadmin_v/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>
	<table width='100%'>
		<tr>
			<td width='20%'>Date</td>
			<td width='1%'>:</td>
			<td width='79%'>
				<?php echo date('Y-m-d',now());?></td>
		</tr>
        <tr>
			<td valign='top'>Member ID</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php 
					$data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
					echo form_input($data);
					$atts = array(
						'width'      => '450',
						'height'     => '600',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'yes',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					echo anchor_popup('search/membersearch/ewalletso', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
				?>
				<span class='error'>*<?php echo form_error('member_id');?></span>
			</td>	
		</tr>
		<tr>
			<td valign='top'>Member Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
        <tr>
			<td valign='top'>Ewallet Rp</td>
			<td valign='top'>:</td>
			<td><input type="text" name="ewallet" id="ewallet" readonly="1" value="<?php echo set_value('ewallet');?>" size="15" /></td>
		</tr>
        <tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('whsid',$warehouse);?></td>
		</tr>
		<tr><td colspan="3"><hr/></td></tr>
		
		<tr>
			<td><b>Pick up / Delivery</b></td>
			<td valign="top" align='left'>&nbsp;</td>
			<td></td>
		</tr>
		<tr>
			<td colspan="100%"><font style="color:#F00"><i>Hanya berlaku satu alamat pengiriman per transaksi</i></font></td>
		</tr>
		<tr>
			<td valign="top">Option</td>
			<td align='top'>:</td>
            <td valign="top"><?php
					$options = array(
						'0'  => 'Pick Up',
						'1'    => 'Delivery'
					);
					echo form_dropdown('pu', $options);
				?>
			</td>
			
		</tr>
		<tr>
			<td valign="top">City of delivery</td>
			<td valign="top">:</td>
			<td valign="top"><?php
				echo form_hidden('kota_id', set_value('kota_id',0));
				echo form_hidden('propinsi', set_value('propinsi',0));
				echo form_hidden('timur', set_value('timur',0));
				echo form_hidden('deli_ad', set_value('deli_ad',0));
				
				echo form_hidden('addr1', set_value('addr1',0));
				echo form_hidden('kota_id1', set_value('kota_id1',0));
				
				$data = array('name'=>'city','id'=>'city','readonly'=>'1','value'=>set_value('city',$row['kota']));
				echo form_input($data);
				$atts = array(
					'width'      => '400',
					'height'     => '400',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'yes',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				echo anchor_popup('citysearch/allso/', "<img src='/images/backend/search.gif' border='0'>", $atts);
				?>
				<span class='error'>*<?php echo form_error('city'); ?></span></td>
		</tr>
		<tr>
			<td valign='top'>Penerima</td>
			<td valign='top'>:</td>
			<td valign="top"><input type="text" name="pic_name" id="pic_name" value="<?php echo set_value('pic_name');?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>No HP</td>
			<td valign='top'>:</td>
			<td valign="top"><input type="text" name="pic_hp" id="pic_hp" value="<?php echo set_value('pic_hp');?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>Delivery Address</td>
			<td valign='top'>:</td>
			<td valign="top"><input type="text" name="addr" id="addr" value="<?php echo set_value('addr');?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>Kecamatan</td>
			<td valign='top'>:</td>
			<td valign="top"><input type="text" name="kecamatan" id="kecamatan" value="<?php echo set_value('kecamatan');?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>Kelurahan</td>
			<td valign='top'>:</td>
			<td valign="top"><input type="text" name="kelurahan" id="kelurahan" value="<?php echo set_value('kelurahan');?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>Kodepos</td>
			<td valign='top'>:</td>
			<td valign="top"><input type="text" name="kodepos" id="kodepos" value="<?php echo set_value('kodepos');?>" size="30" /></td>
		</tr>
		<tr>
			<td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>		
	</table>
<?php echo form_close();?>

<?php $this->load->view('footer');?>
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>