<?php $this->load->view('header'); ?>
<h2><?php echo $page_title; ?></h2>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript">
	function parseToInt(value) {
		var res = value.replace(/-/g, '');
		return parseInt(res);
	}
	$(document).ready(function() {
		var dateToday = new Date();
		$("#from").datepicker({
			defaultDate: "+1h",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1,
			minDate: dateToday,
			onClose: function(selectedDate) {
				$("#to").datepicker("option", "minDate", selectedDate);
			}
		});
		$("#to").datepicker({
			defaultDate: "+1w",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1,
			minDate: dateToday,
			onClose: function(selectedDate) {
				$("#from").datepicker("option", "maxDate", selectedDate);
			}
		});


	});

	function validate(form) {
		var from = parseToInt($('#from').val());
		var to = parseToInt($('#to').val());
		if (to < from) {
			alert("Valid to harus lebih dari Valid From !");
			return false;
		} else {
			return true;
		}
		return false;
	}
</script>
<form method="post" onsubmit="return validate(this);" action="<?php echo base_url(); ?>smartindo/topupdemand/import" enctype="multipart/form-data">
	<table>
		<tr>
			<td>List Penerima</td>
			<td> : </td>
			<td><input type="file" name="file" required></td>
		</tr>
		<tr>
			<td>Valid from</td>
			<td> : </td>
			<td><input autocomplete="off" type="text" data-language="en" name="datefrom" id="from" required></td>
		</tr>
		<tr>
			<td>Valid to</td>
			<td> : </td>
			<td><input autocomplete="off" type="text" data-language="en" name="dateto" id="to" required></td>
		</tr>

		<select hidden name="multiple">
			<option value="1" selected>Yes</option>
			<option value="0">No</option>
		</select>
		</td>
		</tr>
		<tr>
			<td>Status</td>
			<td> : </td>
			<td>
				<select name="status">
					<option value="1">Aktif</option>
					<option value="0">Tidak Aktif</option>
				</select>
			</td>
		</tr>
	</table>
	<div style="float: right; padding: 10px;">
		<input style="width: 160px; height: 30px;" type="submit" value="Save TopUp On Demand">
	</div>
</form>

<?php if ($this->session->flashdata('message')) : ?>
	<h4 style="color: red;">Data tidak dapat di upload, Silahkan perbaiki Rows Berikut : </h4>
	<hr />
	<table style="border-collapse: collapse;border: 1px solid; width: 100%; padding: 8px;" id="voucher_table" style="width:100%">
		<thead>
			<tr>
				<th width="10%">Rows</th>
				<th>Stokiest ID</th>
				<th>Member ID</th>
				<th>Item Code</th>
				<th>QTY</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$res = $this->session->flashdata('message');
			for ($i = 0; $i < count($res); $i++) {
				$a[$i] =  "'" . $res[$i] . "'" . ',';
			}
			$b =  implode(',', $a);
			$c =  str_replace(',,', ',', $b);
			$d =  rtrim($c, ',');
			$ac = "WHERE no IN ($d)";
			$ata = $this->ValueTopup_model->temp_demand($ac);
			
			foreach ($ata as $key) : ?>
				<?php if ($key['no'] == $res[$no]) ?>
				<?php $get = get_data_by_tabel_id($key['item_code'], 'item'); ?>
				<tr>
					<td align="center"><?= $key['no']; ?></td>

					<?php if (call($key['stockiest_id'], 'member') == 'tidak ada') : ?>
						<td style="background-color:red"><?= call($key['stockiest_id'], 'member') ?></td>
					<?php else : ?>
						<td><?= call($key['stockiest_id'], 'member') ?></td>
					<?php endif; ?>

					<?php if (call($key['member_id'], 'member') == 'tidak ada') : ?>
						<td style="background-color:red"><?= call($key['member_id'], 'member'); ?></td>
					<?php else : ?>
						<td><?= call($key['member_id'], 'member'); ?></td>
					<?php endif; ?>

					<?php if (empty($get)) : ?>
						<td style="background-color:red"><?= $get->name; ?></td>
					<?php else : ?>
						<td><?= $get->name; ?></td>
					<?php endif; ?>

					<?php if (empty($key['qty']) || $key['qty'] == 0) : ?>
						<td style="background-color:red"><?= $key['qty']; ?></td>
					<?php else : ?>
						<td align="center"><?= $key['qty']; ?></td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php endif; ?>

<?php
$this->load->view('footer');
?>