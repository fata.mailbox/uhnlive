<!--
	Copyright (c) 2014-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>

<div class="ViewHold">
	<div id="companyDetails">
		<?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?>
		
	</div>

	<p>
		<strong>
			<?php echo "No. Invoice: ";?> <?php echo $row['id'];?><br />
			<?php
				echo $row['tgl'];
			?>
		</strong>
		
	</p>
	<br />
			<h2>Staff Point Order</h2>
	<hr />

	<h3><?php echo $row['no_stc']," - ".$row['nama'];?></h3>

	<p>
		<?php
			echo $row['alamat']."<br />";
			//echo $row['kota']." - ".$row['propinsi']." ".$row['kodepos']."<br />";
			echo "Remark: ".$row['remark']."<br />";
			echo "User ID: ".$row['createdby'];
		?>
	</p>
	
	<table class="stripe">
	<tr>
      <th width='10%'>Item Code</th>
      <th width='25%'>Item Name</th>
      <th width='10%'><div align="right">Qty</div></th>
      <th width='20%'><div align="right">Price</div></th>
      <th width='35%'><div align="right">Sub Total</div></th>
    </tr>
    
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><?php echo $item['item_id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td align="right"><?php echo $item['fqty'];?></td>
			<td align="right"><?php echo $item['fharga'];?></td>
			<td align="right"><?php echo $item['fsubtotal'];?></td>
		</tr>
		<?php endforeach;?>
		
		<tr>
			<td colspan='4' align='right'><b>Total </b>&nbsp;</td>
			<td align="right"><b><?php echo $row['ftotalharga'];?></b></td>
		</tr>
        
	</table>
<?php /* Created by Andrew 20120530*/ ?>
	<table width=100%>
		<tr>
		  <td colspan='1' align=center width='5%'> </td>
		  <td colspan='2' align=center width='25%'>Sales Counter</td>
		  <td colspan='1' align=center width='5%'> </td>
		  <td colspan='2' align=center width='30%'>Warehouse</td>
		  <td colspan='1' align=center width='5%'> </td>
		  <td colspan='2' align=center width='25%'>Receiver</td> 	
		  <td colspan='1' align=center width='5%'> </td>
		</tr>
		
		<tr>
			<td colspan='10'>
				<br><br><br><br><br><br>
			</td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
			<td align='left'>(</td><td align='right'>)</td>
			<td>&nbsp;</td>
			<td align='left'>(</td><td align='right'>)</td>
			<td>&nbsp;</td>
			<td align='left'>(</td><td align='right'>)</td> 
			<td>&nbsp;</td>
		</tr>
	</table>
	<?php /* End created by Andrew 20120530*/ ?>
<?php $this->load->view('footer'); ?>
