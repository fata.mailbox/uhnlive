<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php
	if ($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}
	echo form_open('smartindo/roadmin_vwh/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>
	<table width='100%'>
		<tr>
			<td width='20%'>Date</td>
			<td width='1%'>:</td>
			<td width='79%'>
				<?php echo date('Y-m-d',now());?></td>
		</tr>
        <tr>
			<td valign='top'>Stockist ID</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php echo form_hidden('member_id',set_value('member_id'));
					$data = array('name'=>'no_stc','id'=>'no_stc','size'=>15,'readonly'=>'1','value'=>set_value('no_stc'));
					echo form_input($data);
					$atts = array(
						'width'      => '450',
						'height'     => '600',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'yes',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					echo anchor_popup('search/stockistsearch_vwh/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
				?>
				<span class='error'>*<?php echo form_error('member_id');?></span>
			</td>	
		</tr>
		<tr>
			<td valign='top'>Stockist Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>Diskon %</td>
			<td valign='top'>:</td>
			<td><input class="aright" type="text" name="persen" id="persen" value="<?php echo set_value('persen',0);?>"  /></td>
		</tr>
        <!--
        <tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td><?php //echo form_dropdown('whsid',$warehouse);?></td>
		</tr>
        -->
		<tr><td colspan="3"><hr/></td></tr>
		<tr>
			<td><b>Pick up / Delivery</b></td>
			<td valign="top" align='left'>&nbsp;</td>
			<td></td>
		</tr>
		<tr>
			<td valign="top">Option</td>
			<td align='top'>:</td>
            <td valign="top"><?php
					$options = array(						
						'1'    => 'Delivery',
						'0'  => 'Pick Up'
					);
					echo form_dropdown('pu', $options);
				?>
			</td>
			
		</tr>
		<tr>
			<td valign="top">City of delivery</td>
			<td valign="top">:</td>
			<td valign="top"><?php
				echo form_hidden('kota_id', set_value('kota_id',0));
				echo form_hidden('propinsi', set_value('propinsi',0));
				echo form_hidden('timur', set_value('timur',0));
				echo form_hidden('deli_ad', set_value('deli_ad',0));
				
				echo form_hidden('addr1', set_value('addr1',0));
				echo form_hidden('kota_id1', set_value('kota_id1',0));
				
				//START 20180917 ASP
				echo form_hidden('pic_name1', set_value('pic_name1',0));
				echo form_hidden('pic_hp1', set_value('pic_hp1',0));
				echo form_hidden('kecamatan1', set_value('kecamatan1',0));
				echo form_hidden('kelurahan1', set_value('kelurahan1',0));
				echo form_hidden('kodepos1', set_value('kodepos1',0));
				//END 20180917 ASP
				
				//START 20181230 ASP
				echo form_hidden('whsid', set_value('whsid',1));
				//END 20181230 ASP
				
				$data = array('name'=>'city','id'=>'city','readonly'=>'1','value'=>set_value('city',$row['kota']));
				echo form_input($data);
				$atts = array(
					'width'      => '400',
					'height'     => '400',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'yes',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				echo anchor_popup('citysearch/allro_vwh/', "<img src='/images/backend/search.gif' border='0'>", $atts);
				?>
				<span class='error'>*<?php echo form_error('city'); ?></span></td>
		</tr>
		<tr>
			<td valign='top'>Penerima</td>
			<td valign='top'>:</td>
			<td valign="top"><input type="text" name="pic_name" id="pic_name" value="<?php echo set_value('pic_name');?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>No HP</td>
			<td valign='top'>:</td>
			<td valign="top"><input type="text" name="pic_hp" id="pic_hp" value="<?php echo set_value('pic_hp');?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>Delivery Address</td>
			<td valign='top'>:</td>
			<td valign="top"><input type="text" name="addr" id="addr" value="<?php echo set_value('addr');?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>Kecamatan</td>
			<td valign='top'>:</td>
			<td valign="top"><input type="text" name="kecamatan" id="kecamatan" value="<?php echo set_value('kecamatan');?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>Kelurahan</td>
			<td valign='top'>:</td>
			<td valign="top"><input type="text" name="kelurahan" id="kelurahan" value="<?php echo set_value('kelurahan');?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>Kodepos</td>
			<td valign='top'>:</td>
			<td valign="top"><input type="text" name="kodepos" id="kodepos" value="<?php echo set_value('kodepos');?>" size="30" /></td>
		</tr>
		<tr>
			<td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>		
	</table>
<?php echo form_close();?>

<?php $this->load->view('footer');?>
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>