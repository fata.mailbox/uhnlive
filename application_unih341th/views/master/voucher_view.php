<?php $this->load->view('header'); ?>
<!-- <div id="view"><?php echo anchor('master/voucher/edit/' . $row->id, 'edit Voucher Data'); ?></div> -->

<div class="ViewHold">
  <!-- <div id="companyDetails"><?php echo "<img src='/images/backend/logo.jpg' border='0'><br>"; ?></div> -->
  <!-- <p>
		<strong><?php echo "created : "; ?> <?php echo $row->created; ?><br />
		<?php echo "createdby : "; ?> <?php echo $row->createdby; ?>
		</strong>
	</p>
	<br /> -->
  <h2>Data Voucher</h2>
  <hr />

  <table class="stripe">
    <tr>
      <th width='5%'>No.</th>
      <th width='10%'>Voucher Code</th>
      <th width='10%'>Member ID</th>
      <th width='10%'>Member Name</th>
      <th width='10%'>Stockiest ID</th>
      <th width='10%'>Stockiest Name</th>
      <th width='10%'>Expired Date</th>
      <th width='15%'>Status</th>
      <th style="text-align:right;" width='10%'>Price</th>
    </tr>
    <?php
    if (isset($results)) :
      $counter = $from_rows;
      foreach ($results as $key => $row) :
        $counter = $counter + 1;
        $key = callback($row['vouchercode']);
        $st = cekaj($row['vouchercode']);
        $date_to = date_create($row['expired_date']);
    ?>
        <tr>
          <td><?= $counter; ?></td>
          <td><?= $row['vouchercode']; ?></td>
          <td><?= $key['member_id'] ?></td>
          <td><?= call($key['member_id'], 'member'); ?></td>
          <td><?= codestc($key['stc_id']) ?></td>
          <td><?= call($key['stc_id'], 'stock'); ?></td>
          <td><?= date_format($date_to, "d  - M - Y ") ?></td>
          <td>
            <?php
            if ($st['status'] == 0) {
              echo "Belum digunakan";
            } else {
              echo "Digunakan";
            }
            ?>
          </td>
          <td align="right"><?= number_format(intval($key['price'])); ?></td>
        </tr>
      <?php endforeach;
    else :
      ?>
      <tr>
        <td colspan="7">Data is not available.</td>
      </tr>
    <?php endif; ?>
  </table>


  <?php $this->load->view('footer'); ?>