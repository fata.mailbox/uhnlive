<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('master/supplier/edit/'.$row->id, array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		<tr>
			<td width='24%'>date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td valign='top'>supplier name</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="name" id="name" value="<?php echo set_value('name',$row->name);?>" size"30" />
			<span class="error">* <?php echo form_error('name');?></span></td>
		</tr>
		<tr>
			<td valign='top'>address</td>
			<td valign='top'>:</td>
			<td><?php echo form_hidden('id',$row->id); $data = array('name'=>'address','id'=>'address','rows'=>2, 'cols'=>'30','value'=>set_value('address',$row->address));
    					echo form_textarea($data);?></td> 
		</tr>
		<tr>
			<td valign='top'>telephone</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="telp" id="telp" value="<?php echo set_value('telp',$row->telp);?>" size"30" /></td>
		</tr>
		<tr>
			<td valign='top'>fax</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="fax" id="fax" value="<?php echo set_value('fax',$row->fax);?>" size"30" /></td>
		</tr>
        <tr>
			<td valign='top'>contact person</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="cp" id="cp" value="<?php echo set_value('cp',$row->cp);?>" size"30" /></td>
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td></tr>
		</table>

<?php echo form_close();?>
<?php $this->load->view('footer');?>
