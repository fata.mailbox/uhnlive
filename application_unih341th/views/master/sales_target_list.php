<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	<table width="100%">
	<?php echo form_open('master/input_target/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
        <tr>
			<td valign='top' width="19%">Tahun</td>
			<td valign='top' width="1%">:</td>
			<td width="80%">
				<?php echo form_dropdown('tahun',$dropdownyear);?>
			</td>
		</tr>
        <tr>
			<td valign='top'>&nbsp;</td>
			<td valign='top'>&nbsp;</td>
			<td><?php echo form_submit('submit','preview');?></td>
		</tr>                
         <?php echo form_close();?>
    <tr><td colspan="3"><hr /></td></tr>
	</table>
	<?php echo anchor('master/input_target/create','Create Sales Target'); ?>
<table class="stripe">
	<tr>
      <th width='20%'>Periode</th>
      <th width='20%'>Region</th>
      <th width='20%'><div align="right">Sales Target</div></th>
      <th width='20%'><div align="right">New Recruitment Target</div></th>
      <th width='20%'><div align="center">Action</div></th>
    </tr>
   
<?php
if ($results): 
	foreach($results as $key => $row): 
		$periode = $row['thn']." ".$row['namaBln'];
		/*
		$periode = substr($row['periode'], -5, 2);
		if($periode<=3 && $periode>0)$periode="Quart1";
		if($periode<=6 && $periode>3)$periode="Quart2";
		if($periode<=9 && $periode>6)$periode="Quart3";
		if($periode<=12 && $periode>9)$periode="Quart4";
		*/
?>
    <tr>
		<td><?php echo $periode;?></td>
		<td><?php echo $row['nama'];?></td>
		<td align="right"><?php echo number_format($row['target']);?></td>
		<td align="right"><?php echo number_format($row['nr']);?></td>
		<td align="center">Edit / Closed</td>
    </tr>
    <?php endforeach; 
else: ?>
    <tr>
		<td colspan="5">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>			                
<?php $this->load->view('footer');?>
