<?php $this->load->view('header'); ?>
<h2><?= $page_title; ?></h2>
<style type="text/css">
	tr.border_bottom td {
		border-bottom: 1pt solid black;
	}

	tr.border_ td {
		border: 1px solid black;
	}

	.texts {
		background-color: #ADFF2F;
		color: black;
		font-size: 18px;
		text-align: center;
		font-weight: bold;
	}
</style>
<form autocomplete="off" method="post" onsubmit="return validate(this);" action="<?php echo base_url(); ?>master/voucher/create" enctype="multipart/form-data">
	<table width='100%'>
		<tr>
			<td valign='center'>List Penerima</td>
			<td valign='center'>:</td>
			<td><input type="file" id="upload" name="upload" size="20" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
				<span class='error'>* <?= form_error('upload'); ?></span></td>
		</tr>

		<tr>
			<td valign='top'>Valid Form</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<!-- <input type="date" name="valid-from"> -->
				<input id="from" name="valid-from" type="text" data-date-format="yyyy-mm-dd" data-language="en" class="datepicker-here" />
				<span class="error">* <?= form_error('valid-from'); ?></span>
			</td>
		</tr>

		<tr>
			<td valign='top'>Valid To</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<!-- <input type="date" name="valid-to"> -->
				<input id="to" name="valid-to" type="text" data-date-format="yyyy-mm-dd" data-language="en" class="datepicker-here" />
				<span class="error">* <?= form_error('valid-to'); ?></span>
			</td>
		</tr>

		<tr>
			<td valign='top'>Remarks</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<textarea name="remarks"></textarea>
				<span class="error">* <?= form_error('remarks'); ?></span>
			</td>
		</tr>
		<tr>
			<td colspan='2'>&nbsp;</td>
			<td><?= form_submit('submit', 'Submit'); ?></td>
		</tr>
	</table>
	<div id="data">
	</div>
	<div id="show">
	</div>

	<?php if ($this->session->flashdata('message')) : ?>
		<h4 style="color: red;">Data tidak dapat di upload, Silahkan perbaiki Rows Berikut : </h4>
		<hr />
		<table style="border-collapse: collapse;border: 1px solid; width: 100%; padding: 8px;" id="voucher_table" style="width:100%">
			<thead>
				<tr>
					<th width="10%">Rows</th>
					<th>Stokiest ID</th>
					<th>Member ID</th>
					<th align="right">Price</th>
					<th align="right">PV</th>
					<th align="right">BV</th>
					<th align="right">Remarks</th>
					<th align="right">Status</th>
					<th align="right">Positition</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$res = $this->session->flashdata('message');
				for ($i = 0; $i < count($res); $i++) {
					$a[$i] =  "'" . $res[$i] . "'" . ',';
				}
				$b =  implode(',', $a);
				$c =  str_replace(',,', ',', $b);
				$d =  rtrim($c, ',');
				$ac = "WHERE no IN ($d)";

				$ata = $this->db->query("SELECT * from temp_voucher $ac ")->result_array();
				foreach ($ata as $key) : ?>
					<?php if ($key['no'] == $res[$no]) ?>
					<tr>
						<td align="center"><?= $key['no']; ?></td>
						<?php if (call($key['stc_id'], 'member') == 'tidak ada') : ?>
							<td style="background-color:red"><?= call($key['stc_id'], 'member') ?></td>
						<?php else : ?>
							<td><?= call($key['stc_id'], 'member') ?></td>
						<?php endif; ?>

						<?php if (call($key['member_id'], 'member') == 'tidak ada') : ?>
							<td style="background-color:red"><?= call($key['member_id'], 'member'); ?></td>
						<?php else : ?>
							<td><?= call($key['member_id'], 'member'); ?></td>
						<?php endif; ?>

						<td align="right"><?= $key['nominal']; ?></td>
						<td align="right"><?= $key['pv']; ?></td>
						<td align="right"><?= $key['bv']; ?></td>
						<td align="right"><?= $key['remarks']; ?></td>
						<td align="right"><?= $key['status']; ?></td>
						<td align="right"><?= $key['posisi']; ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	<?php endif; ?>
</form>
<?php $this->load->view('footer'); ?>

<script>
	function parseToInt(value) {
		var res = value.replace(/-/g, '');
		return parseInt(res);
	}

	function validate(form) {
		var from = parseToInt($('#from').val());
		var to = parseToInt($('#to').val());

		if (to < from) {
			alert("Valid to harus lebih dari Valid From !");
			return false;
		} else {

			return true;
		}
	}

	$('#form').on('submit', function() {
		validate(this);
	});

	$('#from').datepicker({
		minDate: new Date()
	})
	$('#to').datepicker({
		minDate: new Date()
	})
</script>