<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

	 <?php $hidden = array('subcategory_id'=>'0'); echo form_open_multipart('master/item/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'),$hidden);?>
		<table width='100%'>
		<tr>
			<td width='19%'>date</td>
			<td width='1%'>:</td>
			<td width='80%'><?php echo date('Y-m-d');?></td>
		</tr>
		<tr>
			<td valign='top'>item code</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="id" id="id" value="<?php echo set_value('id');?>" size"15" />
			<span class="error">* <?php echo form_error('id');?></span></td>
		</tr>
		<tr>
			<td valign='top'>item name</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="name" id="name" value="<?php echo set_value('name');?>" size"30" />
			<span class="error">* <?php echo form_error('name');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Type</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_dropdown('type',$type);?></td>
		</tr>
		<tr>
			<td valign='top'>Price for Member</td>
			<td valign='top'>:</td>
			<td valign='top'><input class='textbold' type="text" name="price" id="price" value="<?php echo set_value('price',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
		</tr>
		<tr>
			<td valign='top'>Price for East area</td>
			<td valign='top'>:</td>
			<td valign='top'><input class='textbold' type="text" name="price2" id="price2" value="<?php echo set_value('price2',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
		</tr>
		<tr>
			<td valign='top'>Poin Value (PV)</td>
			<td valign='top'>:</td>
			<td valign='top'><input class='textbold' type="text" name="pv" id="pv" value="<?php echo set_value('pv',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
		</tr>
        <tr>
			<td valign='top'>Bonus Value (BV)</td>
			<td valign='top'>:</td>
			<td valign='top'><input class='textbold' type="text" name="bv" id="bv" value="<?php echo set_value('bv',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
		</tr>
		<tr>
			<td valign='top'>Price for Customer</td>
			<td valign='top'>:</td>
			<td valign='top'><input class='textbold' type="text" name="pricecust" id="pricecust" value="<?php echo set_value('pricecust',0);?>" maxlength="12" size="10" autocomplete="off" onkeyup="this.value=formatCurrency(this.value);"></td>
		</tr>
	</tr>
			<tr>
		<td valign='top'>Minimum buy</td>
		<td valign='top'>:</td>
		<td valign='top'><input class='textbold' type="text" name="minbuy" id="minbuy" value="<?php echo set_value('minbuy',1);?>" maxlength="12" size="3" autocomplete="off"></td>
	</tr>
	<tr>
			<td valign='top'>is sales?</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $options = array('Yes' => 'Yes','No' => 'No');
    				echo form_dropdown('sales',$options);?></td>
		</tr>
		<tr>
			<td valign='top'>is manufaktur?</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $options = array('No' => 'No','Yes' => 'Yes');
    				echo form_dropdown('manufaktur',$options);?></td>
		</tr>
		<tr>
			<td valign='top'>is display?</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $options = array('Yes' => 'Yes','No' => 'No');
    				echo form_dropdown('display',$options);?></td>
		</tr>
		<tr>
			<td valign='top'>is TopUp?</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $options = array('No' => 'No', 'Yes' => 'Yes');
					echo form_dropdown('topup', $options); ?></td>
		</tr>
		<tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td valign='top'><?php
				echo form_dropdown('warehouse_id',$warehouse,1);?></td>
		</tr>
		<tr>
			<td valign='top'>image product</td>
			<td valign='top'>:</td>
			<td><input type="file" name="userfile" size="20" />
 				<span class='error'><?php if(isset($error)) print_r($error['error']);?></span></td>
		</tr>
		<tr>
			<td valign='top'>headline</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'headline','id'=>'headline','rows'=>2, 'cols'=>'30','value'=>set_value('headline'));
    					echo form_textarea($data);?></td>
		</tr>
		<tr>
			<td valign='top'>description product</td>
			<td valign='top'>:</td>
			<td><?php $data = array(
              'name'        => 'longdesc',
              'id'          => 'longdesc',
              'toolbarset'  => 'Default',
             // 'basepath'    => '/sohomlm_prod/fckeditor/',
              'basepath'    => '/fckeditor/',
              'width'       => '98%',
              'height'      => '400',
              'value'		 => set_value('longdesc')
              );
	echo form_fckeditor($data);?> <span class='error'><?php echo form_error('longdesc');?></span></td>
		</tr>
        <tr>
			<td valign='top'>Promo?</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $options = array('0' => 'No','1' => 'Yes');
    				echo form_dropdown('promo',$options);?></td>
		</tr>
        <tr>
			<td valign='top'>New product?</td>
			<td valign='top'>:</td>
			<td valign='top'><?php $options = array('0' => 'No','1' => 'Yes');
    				echo form_dropdown('new',$options);?></td>
		</tr>
        <tr>
			<td valign='top'>Category</td>
			<td valign='top'>:</td>
			<td valign='top'><?php
				echo form_dropdown('category_id',$category);?></td>
		</tr>
        <!--
        <tr>
			<td valign='top'>Sub Category</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_dropdown('subcategory_id',$subcategory);?></td>
		</tr>
        -->
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td></tr>
		</table>

<?php echo form_close();?>
<?php $this->load->view('footer');?>
