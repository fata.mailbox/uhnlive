<table width='99%'>
<?php echo form_open('master/kota/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search by: <?php echo form_dropdown('whsid',$warehouse); ?> <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='8%'>No.</th>
      <th width='28%'>Kota</th>
      <th width='20%'>Propinsi</th>
      <th width='20%'>Warehouse</th>
      <th width='10%'>Region</th>
      <th width='20%'>Harga</th>
      <th width='12%'>Minimum Order</th>
      <th width='12%'>Charge</th>
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor("/master/kota/view/".$row['id'],$counter);?></td>
      <td><?php echo anchor("/master/kota/view/".$row['id'],$row['name']." ");?></td>
      <td><?php echo anchor("/master/kota/view/".$row['id'],$row['propinsi']);?></td>
      <td><?php echo anchor("/master/kota/view/".$row['id'],$row['warehouse']);?></td>      
      <td><?php echo anchor("/master/kota/view/".$row['id'],$row['region']);?></td>      
      <td><?php echo anchor("/master/kota/view/".$row['id'],$row['harga']);?></td>      
		<td><?php echo anchor("/master/kota/view/".$row['id'],$row['fmqo']);?></td>
		<td><?php echo anchor("/master/kota/view/".$row['id'],$row['fongkoskirim']);?></td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
