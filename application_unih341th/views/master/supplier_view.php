<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<div id="view"><?php echo anchor('master/supplier/edit/'.$row->id,'edit supplier');?></div>

<div class="ViewHold">
	<div id="companyDetails"><?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?></div>
	<p>
		<strong><?php echo "created : ";?> <?php echo $row->created;?><br />
		<?php echo "createdby : ";?> <?php echo $row->createdby;?>
		</strong>
	</p>
	<br />
<h2>Supplier</h2>
<hr />
<h3><?php echo $row->name;?></h3>
<p>
		<?php
			echo $row->address." ";
		?>
	</p>
<table class="stripe">
	<tr>
      <th width='34%'>Description</th>
      <th width='1%'>:</th>
      <th width='65%'>Value</th>
	</tr>
	<tr>
		<td width='16%'>Telp</td>
		<td width='1%'>:</td>
		<td width='83%'><?php echo $row->telp." ";?></td>
    </tr>
	<tr>
		<td>Fax</td>
		<td>:</td>
		<td><?php echo $row->fax." ";?></td>
	</tr>
    <tr>
		<td>Contact Person</td>
		<td>:</td>
		<td><?php echo $row->cp." ";?></td>
	</tr>
	<tr>
		<td>Updated</td>
		<td>:</td>
		<td><?php echo $row->updated; ?></td>
	</tr>
	<tr>
		<td>Update By</td>
		<td>:</td>
		<td><?php echo $row->updatedby." "; ?></td>
	</tr>
</table>
	
<?php $this->load->view('footer');?>
