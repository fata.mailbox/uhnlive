<table width='99%'>
<?php echo form_open('master/item/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='5%'>No.</th>
      <th width='10%'>Code</th>
      <th width='40%'>Name</th>
      <th width='15%'>Type</th>
      <th width='10%'>Sales</th>
      <th width='10%'>Display</th>
      <th width='10%'>Mnf</th>
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor("/master/item/view/".$row['id'],$counter);?></td>
      <td><?php echo anchor("/master/item/view/".$row['id'],$row['id']);?></td>
      <td><?php echo anchor("/master/item/view/".$row['id'],$row['name']);?></td>
      <td><?php echo anchor("/master/item/view/".$row['id'],$row['type']);?></td>
		<td><?php echo anchor("/master/item/view/".$row['id'],$row['sales']);?></td>
		<td><?php echo anchor("/master/item/view/".$row['id'],$row['display']);?></td>
		<td><?php echo anchor("/master/item/view/".$row['id'],$row['manufaktur']);?></td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
