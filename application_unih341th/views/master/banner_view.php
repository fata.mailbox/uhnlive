<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<div id="view"><?php echo anchor('master/banner/edit/'.$row->id,'edit banner');?> | <?php echo anchor('master/banner/edit_img/'.$row->id,'edit banner popup besar');?></div>

<div class="ViewHold">
	<div id="companyDetails"><?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?></div>
	<p>
		<strong><?php echo "banner id : ";?> <?php echo $row->id;?><br />
		<?php echo "created : ";?> <?php echo $row->created;?>
		</strong>
	</p>
	
<h2>Banner</h2>
<hr />
<h3><?php echo $row->name;?></h3>
<table  class="stripe">
	<tr>
      <th width='30%'>Description</th>
      <th width='70%'>Value</th>
   </tr>
   <tr>
		<td>Exp. Date</td>
		<td><?php echo $row->expdate." ";?></td>
    </tr>
   <tr>
		<td>Title</td>
		<td><?php echo $row->title." ";?></td>
    </tr>
	<tr>
		<td>Description</td>
		<td><?php echo $row->description." ";?></td>
    </tr>
	<tr>
		<td>Status</td>
		<td><?=$row->status;?></td>
	</tr>
    <tr>
		<td>Created By</td>
		<td><?=$row->createdby;?></td>
	</tr>
    <tr>
		<td>Updated Date</td>
		<td><?=$row->updated;?></td>
	</tr>
    <tr>
		<td>Updated By</td>
		<td><?=$row->updatedby;?></td>
	</tr>
</table>
</table>
	
<?php $this->load->view('footer');?>
