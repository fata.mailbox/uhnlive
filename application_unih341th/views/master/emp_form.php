<!--
	Copyright (c) 2014-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('master/emp/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		<tr>
			<td width='24%'>date</td>
			<td width='1%'>:</td>
			<td width='75%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td valign='top'>NIK</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="nik" value="<?php echo set_value('nik',$row->nik);?>" size"30" />
				<span class="error">* <?php echo form_error('nik');?></span></td>
		</tr>
        <tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="name" value="<?php echo set_value('name',$row->name);?>" size"30" />
				<span class="error">* <?php echo form_error('name');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Grade</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('grade',array(
				3=>3
				,4=>4
				,5=>5
				,6=>6
				,7=>7
				,8=>8
				,9=>9
				,10=>10
				,11=>11
				),set_value('grade',$row->grade));?></td> 
		</tr>
		<tr>
			<td valign='top'>Saldo Awal</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" name="saldo" value="<?php echo set_value('saldo',$row->saldo);?>" size"30" />
				<span class="error">* <?php echo form_error('saldo');?></span></td>
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>
		</table>

<?php echo form_close();?>
<?php $this->load->view('footer');?>
