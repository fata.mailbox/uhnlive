<style>

#voucher_table {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#voucher_table td, #voucher_table th {
  border: 1px solid #ddd;
  padding: 8px;
}

#voucher_table tr:nth-child(even){background-color: #f2f2f2;}

#voucher_table tr:hover {background-color: #ddd;}

#voucher_table th {
  padding-top: 12px;
  padding-bottom: 12px;
  font-size: 13px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
<br>
<div>
<br>  
<table id="voucher_table" style="width:100%">
    <thead>
      <tr class="border_ texts">
        <th width="4%">No.</th>
        <th width="15%">Valid From</th>
        <th width="15%">Valid To</th>
        <th>Remarks</th>
        <th width="12%">Action</th>

      </tr>
    </thead>
    <tbody><?php
            if (isset($results)) :
              $counter = $from_rows;
              foreach ($results as $key => $row) :
                $counter = $counter + 1;

                $date_from = date_create($row['valid_from']);
                $date_to = date_create($row['valid_to']);
				$canDel = $this->MVoucher->checkCanDelVou($row['id']);
            ?><tr>
            <td style="text-align:left"><?= $counter; ?></td>
            <td ><?= date_format($date_from, "d  - M - Y ") ?></td>
            <td ><?= date_format($date_to, "d  - M - Y ") ?></td>
            <td ><?= $row['remarks'] ?></td>
            <td ><a href="<?= site_url("/master/voucher/view/" . $row['id']); ?>">Detail</a>&nbsp;
              &nbsp;
              &nbsp;
              &nbsp;
              <a href="<?= site_url("/master/voucher/update/" . $row['id']); ?>">Edit</a>&nbsp;
              &nbsp;
              &nbsp;
              &nbsp;
              <?php if($canDel->nRow == $canDel->cSRO){?><a onclick="return confirm('Yakin data voucher akan dihapus ?');" href="<?= site_url("/master/voucher/delete/" . $row['id']); ?>">Hapus</a>
			  <?php }?>
			  </td>
          </tr><?php endforeach;
            else :
                ?><tr>
          <td colspan=" 7">Data is not available.</td>
        </tr><?php endif; ?></tbody>
  </table>
</div>