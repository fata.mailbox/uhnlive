<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<div id="view"><?php echo anchor('master/bank/edit/'.$row->id,'edit bank');?></div>

<div class="ViewHold">
	<div id="companyDetails"><?php echo "<img src='/images/backend/logo.jpg' border='0'><br>";?></div>
	<p>
		<strong><?php echo "bank id : ";?> <?php echo $row->id;?><br />
		<?php echo "created : ";?> <?php echo $row->created;?>
		</strong>
	</p>
	
<h2>Bank</h2>
<hr />
<h3><?php echo $row->name;?></h3>
<table class="stripe">
	<tr>
      <th width='34%'>Description</th>
      <th width='1%'>:</th>
      <th width='65%'>Value</th>
	</tr>
	<tr>
		<td width='16%'>Account No</td>
		<td width='1%'>:</td>
		<td width='83%'><?php echo $row->accountno." ";?></td>
    </tr>
	<tr>
		<td>Account Name</td>
		<td>:</td>
		<td><?php echo $row->accountname." ";?></td>
	</tr>
	<tr>
		<td>Account Area</td>
		<td>:</td>
		<td><?php echo $row->accountarea." ";?></td>
	</tr>
	<tr>
		<td>Status</td>
		<td>:</td>
		<td><?php echo $row->status." ";?></td>
	</tr>
	<tr>
		<td>Creatad by</td>
		<td>:</td>
		<td><?php echo $row->createdby;?></td>
	</tr>
	<tr>
		<td>Updated</td>
		<td>:</td>
		<td><?php echo $row->updated; ?></td>
	</tr>
	<tr>
		<td>Update By</td>
		<td>:</td>
		<td><?php echo $row->updatedby." "; ?></td>
	</tr>
</table>
	
<?php $this->load->view('footer');?>
