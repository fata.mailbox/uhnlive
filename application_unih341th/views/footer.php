
        <div id="footer">
        <p><?php echo "PT. Universal Health Network";?> &copy; 2009<p style="font-size:11px !important;">Webmaster: <a href="mailto:webmaster@uni-health.com">webmaster@uni-health.com</a>&nbsp;</p>
    </div>
    </div>
  </div>
</div>
<script type="text/javascript" charset="utf8" src="<?= site_url() ?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="<?= site_url() ?>assets/js/dataTables.bootstrap.min.js"></script>
<script src="<?= site_url() ?>assets/js/jszip.js"></script>
<script src="<?= site_url() ?>assets/js/xlsx.js"></script>
<script src="<?= site_url() ?>assets/dist/js/datepicker.js"></script>
<script src="<?= site_url() ?>assets/dist/js/i18n/datepicker.en.js"></script>


<script src="<?= site_url() ?>assets/dist/js/moment.min.js"></script>
<script src="<?= site_url() ?>assets/dist/js/daterangepicker.js"></script>
<script>
  $('.fromToDateRange').daterangepicker({
    minDate: <?= isset($minimunValidateDate) && !empty($minimunValidateDate) ? '"' . $minimunValidateDate . '"' : "new Date()" ?>,
    locale: {
      format: 'YYYY-MM-DD'
    }
  });

  $(document).ready(function() {
	//$('.priceN0').number(true, 0, ',', '.');

    $('#monitoring').DataTable();
    $('#voucher_table').DataTable();
    $('#topupdemand').DataTable();
    var ot = $('#edit_voucher').DataTable();
    $('#detail').DataTable();


  });
</script>

</html>