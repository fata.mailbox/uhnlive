<?php $this->load->view('header');?>

<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('fin/dpsapp/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		<tr>
			<td width='24%' valign='top'>Member ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='75%'><?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '450',
              'height'     => '600',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?>				
					 <span class='error'>*<?php echo form_error('member_id');?></span></td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>Ewallet Member / Stc ?</td>
			<td valign='top'>:</td>
			<td><?php $data=array('mem'=>'Deposit ewallet member','stc'=>'Deposit ewallet STC'); echo form_dropdown('flag',$data);?>
             <span class="error">* <?php echo form_error('flag');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Bank</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_dropdown('bank_id',$bank);?></td> 
		</tr>
		
		<tr>
			<td>Payment Type</td>
			<td>:</td>
			<td><select name="payment_type">
			<option value="TRF">Transfer</option>
			<option value="CASH">Cash</option>
			<option value="DC">Debit Card</option>
			<option value="CC">Credit Card</option>
			</select></td> 
		</tr>
		
		<!--tr>
			<td>Transfer Rp.</td>
			<td>:</td>
			<td><input type="text" name="transfer" id="transfer" autocomplete="off" value="<?php echo set_value('transfer',0);?>" onkeyup="this.value=formatCurrency(this.value); 
			 totalDeposit(document.form.transfer,document.form.tunai,document.form.debitcard,document.form.creditcard,document.form.total);"></td> 
		</tr>
		<tr>
			<td>Cash Rp.</td>
			<td>:</td>
			<td><input type="text" name="tunai" id="tunai" autocomplete="off" value="<?php echo set_value('tunai',0);?>" onkeyup="this.value=formatCurrency(this.value); 
			 totalDeposit(document.form.transfer,document.form.tunai,document.form.debitcard,document.form.creditcard,document.form.total);"></td> 
		</tr>
		<tr>
			<td>Debit Card Rp.</td>
			<td>:</td>
			<td><input type="text" name="debitcard" id="debitcard" autocomplete="off" value="<?php echo set_value('debitcard',0);?>" onkeyup="this.value=formatCurrency(this.value); 
			 totalDeposit(document.form.transfer,document.form.tunai,document.form.debitcard,document.form.creditcard,document.form.total)"></td> 
		</tr>
		<tr>
			<td>Credit Card Rp.</td>
			<td>:</td>
			<td><input type="text" name="creditcard" id="creditcard" autocomplete="off" value="<?php echo set_value('creditcard',0);?>" onkeyup="this.value=formatCurrency(this.value); 
			 totalDeposit(document.form.transfer,document.form.tunai,document.form.debitcard,document.form.creditcard,document.form.total)"></td> 
		</tr>
		<tr>
			<td valign='top'>Total Rp.</td>
			<td valign='top'>:</td>
			<td><input type="text" class="textbold" readonly="1" name="total" id="total" autocomplete="off" value="<?php echo set_value('total',0);?>" onkeyup="this.value=formatCurrency(this.value);"> <span class="error">* <?php echo form_error('total');?></span></td> 
		</tr-->
		<tr>
			<td valign='top'>Total Amount</td>
			<td valign='top'>:</td>
			<td><input type="text" class="textbold" name="total" id="total" autocomplete="off" value="<?php echo set_value('total',0);?>" onkeyup="this.value=formatCurrency(this.value);"> <span class="error">* <?php echo form_error('total');?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Payment/Transfer/Transaction Date</td>
			<td valign='top'>:</td>
			<td><?php 
					$data = array('name'=>'fromdate','id'=>'date1','size'=>12,'required' ,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',$row->tgl2));  echo form_input($data);
				?>
				</td> 
		</tr>
		<tr>
			<td valign='top'>Remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?></td> 
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td>
			<?php // echo form_submit('submit', 'Submit');?>
			<input type="submit" id="submit" name="submit" value="Submit" style="background: rgb(255, 255, 255);" onclick="setValidation()">
			</td>
		</tr>
		</table>

<?php echo form_close();?>
<script type="text/javascript">
	function setValidation(){
		if($('#date1').val()==''){
			alert('Tanggal transaksi wajib diisi!');
			return false;
		}
	}

	function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
		daFormat	   : 	"%Y-%m-%d",
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
</script>
<?php $this->load->view('footer');?>
