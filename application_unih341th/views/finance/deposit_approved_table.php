<table width='99%'>
<?php echo form_open('fin/dpsapp/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
    <tr>
      <th width='3%'>No Deposit</th>
      <th width='13%'>Date</th>
      <th width='4%'>Bank</th>
      <th width='8%'>Member ID</th>
      <th width='20%'>Name</th>
      <th width='12%'><div align="right">Total Rp.</div></th>
      <th width='12%'><div align="right">Total App Rp.</div></th>
      <th width='8%'>Approved</th>
      <th width='8%'>Verified</th>
		<th width='35%'>Remark Finance</th>
		<th width='10%'>Action</th>
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor("/fin/dpsapp/view/".$row['id'],$counter);?></td>
      <td><?php echo anchor("/fin/dpsapp/view/".$row['id'],$row['created']);?></td>
      <td><?php echo anchor("/fin/dpsapp/view/".$row['id'],$row['bank_id']);?></td>
      <?php if($row['flag'] == 'mem'){ ?>
	      <td><?php echo anchor("/fin/dpsapp/view/".$row['id'], $row['member_id']);?></td>
      <?php }else{ ?>
            <td><?php echo anchor("/fin/dpsapp/view/".$row['id'], $row['no_stc']);?></td>
      <?php }?>
      <td><?php echo anchor("/fin/dpsapp/view/".$row['id'],$row['nama']);?></td>
      <td align="right"><?php echo anchor("/fin/dpsapp/view/".$row['id'],$row['ftotal']);?></td>
      <td align="right"><?php echo anchor("/fin/dpsapp/view/".$row['id'],$row['ftotal_approved']);?></td>
		<td><?php echo anchor("/fin/dpsapp/view/".$row['id'],$row['approved']);?></td>
		<td><?php echo anchor("/fin/dpsapp/view/".$row['id'],$row['verified']=='1'?'Yes':'No');?></td>
		<td><?php echo anchor("/fin/dpsapp/view/".$row['id'],$row['remark_fin']." ");?></td>
		<td>
			<!--
			<?php if($row['approved']=='approved'){?>
				<a href="<?=site_url("fin/dpsapp/editDetail/".$row['id']); ?>"><img alt="edit" src="<?php echo  base_url();?>images/backend/edit.gif" border="0"/></a>
			<?php }elseif($row['approved']=='verified' and substr($this->session->userdata('user'),0,3)=='FIN'){ ?>
				<a href="<?=site_url("fin/dpsapp/editDetail/".$row['id']); ?>"><img alt="edit" src="<?php echo  base_url();?>images/backend/edit.gif" border="0"/></a>
			<?php } ?>
			-->
			<?php if(($this->session->userdata('group_id') == 16) OR ($this->session->userdata('group_id') == 1 AND $row['approved'] === 'pending')){ if($row['verified']!='1'){
				?>
				<a href="<?=site_url("fin/dpsapp/editDetail/".$row['id']); ?>"><img alt="edit" src="<?php echo  base_url();?>images/backend/edit.gif" border="0"/></a>
			<?php } } ?>
		</td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="8">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
