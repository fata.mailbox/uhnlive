<?php $this->load->view('header');?>

<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('fin/dpsapp/edit/'.$row->id, array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		<tr>
			<td width='34%'>Deposit Id</td>
			<td width='1%'>:</td>
			<td width='65%'><?php echo $row->id;?></td> 
		</tr>
		<tr>
			<td width='34%'>Created</td>
			<td width='1%'>:</td>
			<td width='65%'><?php echo $row->created;?></td> 
		</tr>
		<tr>
			<td valign='top'>Transfer date</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo $row->tgl_transfer;?></td> 
		</tr>
		<tr>
			<td valign='top'>Stockiest no / name</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo $row->no_stc." / ".$row->nama;?></td> 
		</tr>
		<!--
		<tr>
			<td valign='top'>Bank</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo $row->bank_id;?></td> 
		</tr>
		-->
		
		<tr>
			<td valign='top'>Bank</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_dropdown('bank_id',$bank, $row->bank_id);?></td> 
		</tr>
		
		<tr>
			<td valign='top'>Transfer amount Rp.</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo $row->ftotal;?></td> 
		</tr>
		<tr>
			<td valign='top'>Remark</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo $row->remark;?></td> 
		</tr>
		<tr>
			<td valign='top'>Approved?</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_hidden('id',$row->id); echo form_hidden('flag',$row->flag); echo form_hidden('member_id',$row->member_id); $loop=array('approved'=>'Approved','reject'=>'Reject'); echo form_dropdown('status',$loop);?></td> 
		</tr>
		<tr>
			<td valign='top'>Approved Rp.</td>
			<td valign='top'>:</td>
			<td valign='top'><input type="text" class="textbold" name="amount" id="amount" autocomplete="off" value="<?php echo set_value('amount',$this->MMenu->numformat($row->total));?>" onkeyup="this.value=formatCurrency(this.value);">
					 <span class='error'>*<?php echo form_error('amount');?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Remark Fin</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?></td> 
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>
		</table>

<?php echo form_close();?>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>    
<?php $this->load->view('footer');?>
