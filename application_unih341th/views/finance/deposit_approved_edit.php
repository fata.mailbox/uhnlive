<?php $this->load->view('header');?>
<style>
.error{
	color:red;font-weight:normal !important;font-size:7pt
}
</style>
<h2><?php echo $page_title;?></h2>
	 <?php echo form_open('fin/dpsapp/editDetail/'.$this->uri->segment(4), array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		<tr>
			<td valign='top'>Deposit ID</td>
			<td valign='top'>:</td>
			<td><input type="text" name="depID" id="depID" readonly="1" value="<?php echo $this->uri->segment(4);?>" size="5"  style="background:#F5F5F5"/> <span class="error">Not Editable  <?php echo form_error('flag');?></span></td></td>
		</tr>
		<tr>
			<td width='24%' valign='top'>Member ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='75%'><?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>$row->member_id);
    echo form_input($data);?> <?php $atts = array(
              'width'      => '450',
              'height'     => '600',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					//echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?>				
					 <span class='error'>Not Editable <?php echo form_error('member_id');?></span></td>
					
		</tr>
		<?php
		if($row->no_stc!=''){
			$stcx = ' ('.$row->no_stc.')';
		}else{
			$stcx = '';
		}
		?>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo $row->name.$stcx;?>" size="30" /> <span class="error">Not Editable  <?php echo form_error('flag');?></span></td></td>
		</tr>
		<tr>
			<td valign='top'>Ewallet Member / Stc ?</td>
			<td valign='top'>:</td>
			<td><?php $data=array('mem'=>'Deposit ewallet member','stc'=>'Deposit ewallet STC'); echo form_dropdown('flag',$data);?>
             <span class="error">Not Editable  <?php echo form_error('flag');?></span></td>
		</tr>
		<tr>
			<td valign='top'>Bank</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_dropdown('bank_id',$bank);?></td> 
		</tr>
		
		<tr>
			<td>Payment Type</td>
			<td>:</td>
			<td><select name="payment_type">
			<option value="TRF">Transfer</option>
			<option value="CASH">Cash</option>
			<option value="DC">Debit Card</option>
			<option value="CC">Credit Card</option>
			</select></td> 
		</tr>
		
		<!--tr>
			<td>Transfer Rp.</td>
			<td>:</td>
			<td><input type="text" name="transfer" id="transfer" autocomplete="off" value="<?php echo set_value('transfer',0);?>" onkeyup="this.value=formatCurrency(this.value); 
			 totalDeposit(document.form.transfer,document.form.tunai,document.form.debitcard,document.form.creditcard,document.form.total);"></td> 
		</tr>
		<tr>
			<td>Cash Rp.</td>
			<td>:</td>
			<td><input type="text" name="tunai" id="tunai" autocomplete="off" value="<?php echo set_value('tunai',0);?>" onkeyup="this.value=formatCurrency(this.value); 
			 totalDeposit(document.form.transfer,document.form.tunai,document.form.debitcard,document.form.creditcard,document.form.total);"></td> 
		</tr>
		<tr>
			<td>Debit Card Rp.</td>
			<td>:</td>
			<td><input type="text" name="debitcard" id="debitcard" autocomplete="off" value="<?php echo set_value('debitcard',0);?>" onkeyup="this.value=formatCurrency(this.value); 
			 totalDeposit(document.form.transfer,document.form.tunai,document.form.debitcard,document.form.creditcard,document.form.total)"></td> 
		</tr>
		<tr>
			<td>Credit Card Rp.</td>
			<td>:</td>
			<td><input type="text" name="creditcard" id="creditcard" autocomplete="off" value="<?php echo set_value('creditcard',0);?>" onkeyup="this.value=formatCurrency(this.value); 
			 totalDeposit(document.form.transfer,document.form.tunai,document.form.debitcard,document.form.creditcard,document.form.total)"></td> 
		</tr>
		<tr>
			<td valign='top'>Total Rp.</td>
			<td valign='top'>:</td>
			<td><input type="text" class="textbold" readonly="1" name="total" id="total" autocomplete="off" value="<?php echo set_value('total',0);?>" onkeyup="this.value=formatCurrency(this.value);"> <span class="error">* <?php echo form_error('total');?></span></td> 
		</tr-->
		<tr>
			<td valign='top'>Total Amount</td>
			<td valign='top'>:</td>
			<td><input type="text" readonly="true" style="background:#f5f5f5" class="textbold" name="total" id="total" autocomplete="off" value="<?php echo number_format($row->total_approved , 0, ',', '.');?>" onkeyup="this.value=formatCurrency(this.value);"> <span class="error">Not Editable <?php echo form_error('total_approved');?></span></td> 
		</tr>
		<tr>
			<td valign='top'>Payment/Transfer/Transaction Date</td>
			<td valign='top'>:</td>
			<td><?php 
					$data = array('name'=>'fromdate','id'=>'date1','size'=>12,'required' ,'readonly'=>'1','maxlength'=>'10', 'value'=>$row->tgl_transfer);  echo form_input($data);
				?>
				</td> 
		</tr>
		<tr>
			<td valign='top'>Remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>$row->remark_fin);
    					echo form_textarea($data);?></td> 
		</tr>
		<tr <?php if(substr($this->session->userdata('user'),0,3)=='FIN'){ echo 'style="visibility:visible"'; }else{ echo 'style="visibility:hidden"'; } ?> >
			<!--
			<td>Status</td>
			<td>:</td>
			<td><select name="status">
			<option value="approved" <?php echo $row->approved=='approved'?'selected':'';?>>Approved</option>
			<option value="verified" <?php echo $row->approved=='verified'?'selected':'';?>>Verified</option>
			</select></td>
			-->
			<td>Status</td>
			<td>:</td>
			<td><select name="status">
			<option value="verified" selected>Verified</option>
			<option value="approved">Approved</option>
			</select></td>
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td>
			<?php // echo form_submit('submit', 'Submit');?>
			<input type="submit" id="submit" name="submit" value="Submit" style="background: rgb(255, 255, 255);" onclick="setValidation()">
			</td>
		</tr>
		</table>

<?php echo form_close();?>
<?php if($row->flag=='stc'){?>
<script>
$('[name=flag]').val('stc');
</script>
<?php }else{ ?>
<script>
$('[name=flag]').val('mem');
</script>	
<?php } ?>
<?php
if($row->transfer!=0){
	$type = 'TRF';
}elseif($row->tunai!=0){
	$type = 'CASH';
}elseif($row->debit_card!=0){
	$type = 'DC';
}elseif($row->credit_card!=0){
	$type = 'CC';
}
?>
<script type="text/javascript">
	$('[name=bank_id]').val('<?php echo $row->bank_id;?>');
	$('[name=payment_type]').val('<?php echo $type;?>');
	$('#member_id').css('background','#f5f5f5');
	$('#name').css('background','#f5f5f5');
	$('[name=flag]').attr('disabled',true);
	$('[name=flag]').css('background','#f5f5f5');
	function setValidation(){
		if($('#date1').val()==''){
			alert('Tanggal transaksi wajib diisi!');
			return false;
		}
	}

	function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
		daFormat	   : 	"%Y-%m-%d",
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
</script>
<?php $this->load->view('footer');?>
