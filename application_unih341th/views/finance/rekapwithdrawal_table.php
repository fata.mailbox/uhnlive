
<table width='100%'>
<?php echo form_open('fin/hwithdrawal', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  echo form_input($data);?>   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
   echo "to: ".form_input($data);?>
   </td>
   </tr>
   <tr>
			<td valign='top'>Approved ?</td>
			<td valign='top'>:</td>
			<td><?php $data = array(''=>'Pilih jenis withdrawal','wd1'=>'User Request via BCA','wd2'=>'User Request via MANDIRI','wd3'=>'Ongkos Kirim','wd4'=>'Biaya Admin','wd5'=>'Withdrawal Koreksi (Cash)','wd6'=>'Withdrawal Koreksi (BCA)','wd7'=>'Withdrawal Koreksi (MANDIRI)','wd8'=>'Refund Withdrawal');
    					echo form_dropdown('type_withdrawal_id',$data);?></td> 
		</tr>
  </tr>
  
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
	</tr>
<?php echo form_close();?>				
<?php echo form_open('fin/hwithdrawal/autotransfer', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
</tr>
  		        <td colspan="2"><?php echo form_submit('submit','Create Auto Withdrawal');?></td>
		<td>&nbsp;</td>

	</tr>
<?php echo form_close();?>				
</table>

<table width="100%" class="stripe">
	<tr>
      <td colspan="8" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <th width='5%'>No.</th>
		<th width='10%'>Tanggal</th>
      <th width='10%'>Member ID</th>
       <th width='20%'>Nama member</th>
      <th width='5%'>Wdr ID</th>
      <th width='10%'><div align="right">Total Rp</div></th>
      <th width='10%'><div align="right">B. Adm Rp</div></th>
      <th width='10%'><div align="right">Bayar Rp</div></th>
   </tr>
	<tr>
      <td colspan="8" style="border-bottom:solid thin #000099"></td>
  </tr>
   
<?php
if ($results):
	foreach($results as $row): 
?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['approved'];?></td>
      <td><?php if($row['flag'] == 'mem') echo $row['member_id']; else echo $row['no_stc'];?></td>
      <td><?php echo $row['nama'];?></td>
      <td><?php echo $row['id'];?></td>
      <td align="right"><?php echo $row['famount'];?></td>
	<td align="right"><?php echo $row['fbiayaadm'];?></td>
	<td align="right"><?php echo $row['fbayar'];?></td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td><em><?php echo $row['bank_id'];?></em></td>
    <td><em><?php echo $row['no'];?></em></td>
      <td ><em><?php echo $row['name'];?></em></td>
        <td colspan="3"><em><?php echo $row['area'];?></em></td>
        <td><em><?php echo $row['approvedby'];?></em></td>
    </tr>

  <?php endforeach;?>
  <tr>
      <td colspan="5" align="right"><b>Grand Total: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      	<td align="right"><strong><?php echo $total['famount'];?></strong></td>
      <td align="right"><strong><?php echo $total['fbiayaadm'];?></strong></td>
      <td align="right"><strong><?php echo $total['fbayar'];?></strong></td>
    </tr>
    <tr>
      <td colspan="8" style="border-bottom:double medium #000099"></td>
    </tr>
    
 <?php else: ?>
    <tr>
      <td colspan="9">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>
