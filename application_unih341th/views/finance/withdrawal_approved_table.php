<table width='99%'>
<?php echo form_open('fin/wdrapp/', array('id' => 'my_form', 'name' => 'my_form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='40%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='60%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b><?php }?>
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>
<?php echo form_open('fin/wdrapp/refund/', array('id' => 'my_form2', 'name' => 'my_form2', 'autocomplete' => 'off'));?>
<table class="stripe">
<tr>
					<td colspan='3' valign='top'>remark: </td>
					<td colspan='5'><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?><br>
    					<?php echo form_submit('submit','Refund eWallet');?></td>					
				</tr>

    <tr>
      <th width='5%'>Refund</th>
      <th width='5%'>No.</th>
      <th width='10%'>Date</th>
      <th width='10%'>Member ID</th>
      <th width='18%'>Name</th>
      <th width='12%'><div align="right">Withdrawal Rp.</div></th>      
      <th width='12%'>Status</th>
		<th width='23%'>Remark Finance</th>
    </tr>
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1; 
?>

    <tr>
      <td align="center"> <?=form_hidden('counter[]',$key);?> <?php if($row['refund'] == 'N'){ 
					$data = array(
    'name'        => 'p_id'.$key,
    'id'          => 'p_id'.$key,
    'value'       => $row['id'],
    'checked'     => false,
    'style'       => 'border:none'
    );					
					echo form_checkbox($data); } else {?>&nbsp;<?php }?> </td>
      <td><?php echo anchor("/fin/wdrapp/view/".$row['id'],$counter);?></td>
     <td><?php echo anchor("/fin/wdrapp/view/".$row['id'],$row['tgl']);?></td>
     <?php if($row['flag'] == 'mem'){ ?>
      <td><?php echo anchor("/fin/wdrapp/view/".$row['id'], $row['member_id']);?></td>
      <?php }else{ ?>
            <td><?php echo anchor("/fin/wdrapp/view/".$row['id'], $row['no_stc']);?></td>
      <?php }?>
      <td><?php echo anchor("/fin/wdrapp/view/".$row['id'],$row['nama']);?></td>
      <td align="right"><?php echo anchor("/fin/wdrapp/view/".$row['id'],$row['famount']);?></td>
      <td><?php echo anchor("/fin/wdrapp/view/".$row['id'],$row['status']);?></td>
		<td><?php echo anchor("/fin/wdrapp/view/".$row['id'],$row['remarkapp']." ");?></td>
     </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="8">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php echo form_close();?>	