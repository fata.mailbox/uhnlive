<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>

<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('fin/wdrapp/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		<table>
		<tr>
			<td width='24%' valign='top'>Member ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='75%'><?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '450',
              'height'     => '600',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?>				
					 <span class='error'>*<?php echo form_error('member_id');?></span></td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
		<tr>
			<td valign='top'>Ewallet Memer / Stc ?</td>
			<td valign='top'>:</td>
			<td><?php $data=array('mem'=>'Withdrawal ewallet member','stc'=>'Withdrawal ewallet STC'); echo form_dropdown('flag',$data);?>
             <span class="error">* <?php echo form_error('flag');?></span></td>
		</tr>
        <tr>
			<td valign='top'>Approved ?</td>
			<td valign='top'>:</td>
			<td><?php $data = array(''=>'Pilih jenis withdrawal','wd1'=>'User Request via BCA','wd2'=>'User Request via MANDIRI','wd3'=>'Ongkos Kirim','wd4'=>'Biaya Admin','wd5'=>'Withdrawal Koreksi (Cash)','wd6'=>'Withdrawal Koreksi (BCA)','wd7'=>'Withdrawal Koreksi (MANDIRI)');
    					echo form_dropdown('type_withdrawal_id',$data);?></td> 
		</tr>
        
        <tr>
			<td valign='top'>Via ?</td>
			<td valign='top'>:</td>
			<td><?php //echo form_dropdown('type_withdrawal_id',$type_wdr);?>
             <span class="error">* <?php echo form_error('type_withdrawal_id');?></span></td>
		</tr>
		<tr>
			<td>transfer amount Rp.</td>
			<td>:</td>
			<td><input type="text" class="textbold" name="amount" id="amount" autocomplete="off" value="<?php echo set_value('amount',0);?>" onkeyup="this.value=formatCurrency(this.value);"><span class="error">* <?php echo form_error('amount');?></span></td> 
		</tr>
		<tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
    					echo form_textarea($data);?></td> 
		</tr>
		<tr><td colspan='2'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit');?></td>
		</tr>
		</table>

<?php echo form_close();?>

<?php $this->load->view('footer');?>
