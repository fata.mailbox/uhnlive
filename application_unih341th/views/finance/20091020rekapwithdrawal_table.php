
<table width='100%'>
<?php echo form_open('fin/hwithdrawal', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  echo form_input($data);?>   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
   echo "to: ".form_input($data);?>
   </td>
  </tr>
  
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
	</tr>
<?php echo form_close();?>				
</table>

<table width="100%">
	<tr>
      <td colspan="11" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <td width='5%'><b>No.</b></td>
		<td width='10%'><b>Tanggal</b></td>
      <td width='10%'><b>Member ID</b></td>
       <td width='20%'><b>Nama member</b></td>
      <td width='5%'><b>Wdr ID</b></td>
      <td width='10%'><b>Total Rp</b></td>
      <td width='5%'><b>Bank ID</b></td>
      <td width='20%'><b>Nama Nasabah</b></td>
      <td width='12%'><b>No. Rekening</b></td>
      <td width='12%'><b>Cabang Bank</b></td>
      <td width='11%'><b>User ID</b></td>
   </tr>
	<tr>
      <td colspan="11" style="border-bottom:solid thin #000099"></td>
  </tr>
   
<?php
if ($results):
	foreach($results as $row): 
?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['approved'];?></td>
      <td><?php if($row['flag'] == 'mem') echo $row['member_id']; else echo $row['no_stc'];?></td>
      <td><?php echo $row['nama'];?></td>
      <td><?php echo $row['id'];?></td>
      <td align="right"><?php echo $row['famount'];?></td>
      <td><?php echo $row['bank_id'];?></td>
      <td><?php echo $row['name'];?></td>
		<td><?php echo $row['no'];?></td>
        <td><?php echo $row['area'];?></td>
        <td><?php echo $row['approvedby'];?></td>
    </tr>
	<tr>
      <td colspan="11" style="border-bottom:solid thin #000099"></td>
  </tr>

  <?php endforeach;?>
  <tr>
      <td colspan="5" align="right"><b>Grand Total: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      	<td align="right"><strong><?php echo $total['famount'];?></strong></td>
      <td colspan="5">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="11" style="border-bottom:double medium #000099"></td>
    </tr>
    
 <?php else: ?>
    <tr>
      <td colspan="9">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>
