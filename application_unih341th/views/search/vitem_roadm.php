<script type="text/javascript" src="<?php echo base_url();?>js/order.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/backend.js"></script>

<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/popup.css" />

<table width="100%" border="0" cellpadding="5" cellspacing="5">
<tr><td>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr><td class='bg_border' align='center'>Browse Item Request Order</td></tr>
		<tr><td class='td_border2'>
			
			<?php echo form_open('item_roadm/index/'.$this->uri->segment(3), array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width='10%'>Search </td>
				<td width='1%'>:</td>
    			<td width='89%'><?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','search');?></td>
			</tr>
			<?php if($this->session->userdata('keywords')){ ?>
				<tr><td colspan='2'>&nbsp;</td>
				<td>Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b></td>
				</tr>
			<?php }?>
			
			</table>	
			<?php echo form_close();?>
				<?php if($results) {?>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">				
				<tr class='title_table'>
					<td class='td_report' width='10%'>No.</td>
					<td class='td_report' width='15%'>Item Code</td>
					<td class='td_report' width='45%'>Item Name</td>
					<td class='td_report' width='20%'>Price</td>
					<td class='td_report' width='10%'>PV</td>
				</tr>
				<?php $counter = $from_rows; foreach($results as $row) { $counter = $counter + 1; ?>
				<tr height='22' class='lvtColData' onmouseover='this.className="lvtColDataHover"' onmouseout='this.className="lvtColData"' 
				 onclick="
					window.opener.document.form.itemcode<?php echo $this->uri->segment(3);?>.value ='<?php echo $row['id'];?>';
					window.opener.document.form.itemname<?php echo $this->uri->segment(3);?>.value ='<?php echo $row['name'];?>';
					window.opener.document.form.price<?php echo $this->uri->segment(3);?>.value ='<?php echo number_format($row['price'],'0','','.');?>';
					window.opener.document.form.gprice<?php echo $this->uri->segment(3);?>.value ='<?php echo $row['price'];?>';
					window.opener.document.form.gprice2<?php echo $this->uri->segment(3);?>.value ='<?php echo $row['price2'];?>';
					window.opener.document.form.pv<?php echo $this->uri->segment(3);?>.value ='<?php echo number_format($row['pv'],'0','','.');?>';
					window.opener.document.form.qty<?php echo $this->uri->segment(3);?>.value ='1';
					window.opener.document.form.subtotal<?php echo $this->uri->segment(3);?>.value ='<?php echo number_format($row['price'],'0','','.');?>';
					window.opener.document.form.subtotalpv<?php echo $this->uri->segment(3);?>.value ='<?php echo number_format($row['pv'],'0','','.');?>';
					window.opener.document.form.total.value=total_curr(10,'window.opener.document.form.subtotal');
					window.opener.document.form.totalpv.value=totalpv_curr(10,'window.opener.document.form.subtotalpv');
					
					if(window.opener.document.form.timur.value==1 && window.opener.document.form.pu.value==1){
						window.opener.document.form.price<?php echo $this->uri->segment(3);?>.value ='<?php echo number_format($row['price2'],'0','','.');?>';
						window.opener.document.form.subtotal<?php echo $this->uri->segment(3);?>.value ='<?php echo number_format($row['price2'],'0','','.');?>';
						<?php if($row['pv'] > 0){ ?>
						window.opener.document.form.diskonrp<?php echo $this->uri->segment(3);?>.value =window.opener.document.form.persen.value * window.opener.document.form.gprice2<?php echo $this->uri->segment(3);?>.value / 100;
						window.opener.document.form.subdiskonrp<?php echo $this->uri->segment(3);?>.value =window.opener.document.form.persen.value * window.opener.document.form.gprice2<?php echo $this->uri->segment(3);?>.value / 100;
						<?php }else{ ?> window.opener.document.form.diskonrp<?php echo $this->uri->segment(3);?>.value ='0'; window.opener.document.form.subdiskonrp<?php echo $this->uri->segment(3);?>.value = 0; <?php } ?>
					}else{
						window.opener.document.form.price<?php echo $this->uri->segment(3);?>.value ='<?php echo number_format($row['price'],'0','','.');?>';
						window.opener.document.form.subtotal<?php echo $this->uri->segment(3);?>.value ='<?php echo number_format($row['price'],'0','','.');?>';
						<?php if($row['pv'] > 0){ ?>
						window.opener.document.form.diskonrp<?php echo $this->uri->segment(3);?>.value =window.opener.document.form.persen.value * window.opener.document.form.gprice<?php echo $this->uri->segment(3);?>.value / 100;
						window.opener.document.form.subdiskonrp<?php echo $this->uri->segment(3);?>.value =window.opener.document.form.persen.value * window.opener.document.form.gprice<?php echo $this->uri->segment(3);?>.value / 100;
						<?php }else{ ?> window.opener.document.form.diskonrp<?php echo $this->uri->segment(3);?>.value ='0'; window.opener.document.form.subdiskonrp<?php echo $this->uri->segment(3);?>.value = 0;<?php } ?>
					}
					
					window.opener.document.form.rpdiskon.value = total_disc(10,'window.opener.document.form.subdiskonrp');
					window.opener.document.form.total.value=total_curr(10,'window.opener.document.form.subtotal');
					window.opener.document.form.totalpv.value=totalpv_curr(10,'window.opener.document.form.subtotalpv');
					totalbayardiskon(window.opener.document.form.total, window.opener.document.form.rpdiskon, window.opener.document.form.totalbayar);
					
					window.close();
				  ">
					<td class='td_report'><?php echo $counter;?></td>
					<td class='td_report'><?php echo $row['id'];?></td>
					<td class='td_report'><?php echo $row['name'];?></td>
					<td class='td_report'><?php echo $row['fprice'];?></td>
					<td class='td_report'><?php echo $row['fpv'];?></td>
				</tr>
				<?php }?>
				<tr><td colspan='5'><?php echo $this->pagination->create_links(); ?></td></tr>
				</table>
				<?php }?>
		</td></tr>
</table>
		
</td></tr>
</table>