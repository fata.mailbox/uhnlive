<!--
	Copyright (c) 2009-<?=date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>



<?php //if($this->session->userdata('group_id') > 100){
	if($row->description!='')
		$titleuserxx = '('.ucfirst($row->description).')';
	else
		$titleuserxx ='';
	if($this->session->userdata('group_id') == 102){ // stockiest area
	?>
    <h2 style="font-family:'Times New Roman', Times, serif; font-style:italic">Welcome <?=ucfirst($this->session->userdata('name'));?> (<?=$row->kotaasal;?> - <?=$row->no_stc;?>)</h2>
	<div>
		<p style="text-align:justify">
			<p><img border="0" width="200" style="padding: 10px; float: right;" src="<?=base_url();?>images/backend/rs-2019.jpg" alt="" />
            <b>Saat ini anda berstatus Sebagai Stockiest Area  dengan discount Tambahan 6 %.</b> 
			<br />Pastikan anda terus berkualifikasi untuk mengikuti <b>Stockiest Conference Unihealth 2020</b>.
			<br /><br />
            <?php
				if($row->region == 1) 
					$rsm = 'Riky Triady (0813-80070457) / Jafar Heri (0812-90664477) ';
				else if($row->region == 2) 
					$rsm = 'Riky Triady (0813-80070457) / Fransisca Josefina (0812-84232223)';
				else if($row->region == 3) 
					$rsm = 'Riky Triady (0813-80070457) / Raymond Harivan (0813-84926176)';
				else if($row->region == 4) 
					$rsm = 'Riky Triady (0813-80070457) / Mellyna Atika Rahayu (0812-1969109)';
				else
					$rsm ='';
					
			if ($rsm!='')
			echo 'Informasi lebih lanjut hubungi Regional Sales Manager Wilayah '.$row->kotaasal.' : <br />';
			echo $rsm.'<br /><br />';
			?>
            Bersama UNIHEALTH Anda mendapatkan hidup sehat, kesejahteraan dan gaya hidup. Karena kesehatan Anda sangat berharga, UNIHEALTH mempersembahkan produk natural kombinasi dari kebaikan alam dan ilmu pengetahuan. Kami memberikan peluang Anda untuk sukses dan menikmati gaya hidup sehat alami.<br /><br />
Layanan online 24 jam setiap hari ini kami sediakan sebagai sarana dan media Anda untuk mengembangkan dan memantau seluruh aktivitas bisnis online Anda.<br /><br />
Selamat menikmati layanan online kami. Sukses dan sehat bersama UNIHEALTH.</p>
		</p>
	</div>
    <?php
	}else if($this->session->userdata('group_id') == 103){ // stockiest mobile 
	?>
    <h2 style="font-family:Times New Roman', Times, serif; font-style:italic">Welcome <?=ucfirst($this->session->userdata('name'));?> (<?=$row->kotaasal;?> - <?=$row->no_stc;?>)</h2>
	<div>
		<p style="text-align:justify">
			<p><img border="0" width="200" style="padding: 10px; float: right;" src="<?=base_url();?>images/backend/rs-2019.jpg" alt="" />
            <b>Saat ini anda berstatus sebagai mobile Stockiest dengan discount tambahan 2 %.</b>
            <br /><br />
            Segera upgrade Status Stockiest Anda menjadi Stockiest Area dan dapatkan Benefit tambahan.<br />
            Antara lain :<br />
            &nbsp;&nbsp;&nbsp;&nbsp;1.	Discount 6 %<br />
            &nbsp;&nbsp;&nbsp;&nbsp;2.	Bonus Distribusi Stockiest 2% Penjualan minimum 35 juta<br />
            &nbsp;&nbsp;&nbsp;&nbsp;3.	Hit Bonus sebesar 2 Juta<br />
            &nbsp;&nbsp;&nbsp;&nbsp;4.	Stockiest Conference 1 tiket Gratis<br />
            <br /><br />    
            <?php
				if($row->region == 1) 
					$rsm = 'Riky Triady (0813-80070457) / Jafar Heri (0812-90664477) ';
				else if($row->region == 2) 
					$rsm = 'Riky Triady (0813-80070457) / Fransisca Josefina (0812-84232223)';
				else if($row->region == 3) 
					$rsm = 'Riky Triady (0813-80070457) / Raymond Harivan (0813-84926176)';
				else if($row->region == 4) 
					$rsm = 'Riky Triady (0813-80070457) / Mellyna Atika Rahayu (0812-1969109)';
				else
					$rsm ='';
					
			if ($rsm!='')
			echo 'Informasi lebih lanjut hubungi Regional Sales Manager Wilayah '.$row->kotaasal.' : <br />';
			echo $rsm.'<br /><br />';
			?>
                    
            Bersama UNIHEALTH Anda mendapatkan hidup sehat, kesejahteraan dan gaya hidup. Karena kesehatan Anda sangat berharga, UNIHEALTH mempersembahkan produk natural kombinasi dari kebaikan alam dan ilmu pengetahuan. Kami memberikan peluang Anda untuk sukses dan menikmati gaya hidup sehat alami.<br /><br />
Layanan online 24 jam setiap hari ini kami sediakan sebagai sarana dan media Anda untuk mengembangkan dan memantau seluruh aktivitas bisnis online Anda.<br /><br />
Selamat menikmati layanan online kami. Sukses dan sehat bersama UNIHEALTH.</p>
		</p>
	</div>
    <?php
	} else {
	?>
    <h2 style="font-family:Times New Roman', Times, serif; font-style:italic">Welcome <?=$this->session->userdata('name');?></h2>
	<div>
		<p style="text-align:justify">
			<p><img border="0" width="200" style="padding: 10px; float: right;" src="<?=base_url();?>images/backend/rs-2019.jpg" alt="" />
			<b>Selamat datang <?=$this->session->userdata('name');?> <?=$titleuserxx?> di UNIHEALTH.</b><br /><br />Terima kasih Anda telah bergabung dan menjadi bagian dari keluarga UNIHEALTH.<br /><br />
            
            Bersama UNIHEALTH Anda mendapatkan hidup sehat, kesejahteraan dan gaya hidup. Karena kesehatan Anda sangat berharga, UNIHEALTH mempersembahkan produk natural kombinasi dari kebaikan alam dan ilmu pengetahuan. Kami memberikan peluang Anda untuk sukses dan menikmati gaya hidup sehat alami.<br /><br />
Layanan online 24 jam setiap hari ini kami sediakan sebagai sarana dan media Anda untuk mengembangkan dan memantau seluruh aktivitas bisnis online Anda.<br /><br />
Selamat menikmati layanan online kami. Sukses dan sehat bersama UNIHEALTH.</p>
		</p>
	</div>
	<?PHP
	}
	//}?>
    
        <!--slider -->
        <div id="slider">
        	<!-- start slideshow -->
          	<div id="slideshow"><?php 
				if($banner){
					$x=count($banner); 
					foreach($banner as $key => $row){ 
						?>
						<div <?php if($key == 0)echo "class='slider-item'";?>>
							<a href="<?=base_url();?>userfiles/banner/<?=$row['bigfile'];?>" class="highslide" onclick="return hs.expand(this)">
                                <img src="<?=base_url();?>userfiles/banner/<?=$row['file'];?>" alt="Highslide JS" title="Click to enlarge" />
                            </a>
						</div><?php 
				
						if($x == 1){?>
							<div <?php if($key == 0)echo "class='slider-item'";?>>
								<a href="<?=base_url();?>userfiles/banner/<?=$row['bigfile'];?>" class="highslide" onclick="return hs.expand(this)">
                                	<img src="<?=base_url();?>userfiles/banner/<?=$row['file'];?>" alt="Highslide JS" title="Click to enlarge" />
                            	</a>
							</div><?php 
						}
					}
				}else{?>
                <!--
                    <div class='slider-item'>
						<a href="#"><img src="<?=base_url();?>images/01-Banner.png" alt="image" width="1000" height="300" border="0"></a>
					</div>
                -->
				<?php 
				}?>
          	</div>
			<!-- end #slideshow -->
            
			<!-- Button Slide -->
          	<div class="controls-center">
                <div id="slider_controls">
                    <ul id="slider_nav"><?php 
						if($banner){ 
							$x=count($banner); 
							foreach($banner as $key => $row){?>
								<li><a class="li_1" href="#"></a></li><?php 
								if($x == 1){?>
									<li><a class="li_2" href="#"></a></li><?php 
								}
							}
						}else{?>
                        <!-- <li><a class="li_1" href="#"></a></li><!-- Slide 1 -->
                        <!-- <li><a class="li_2" href="#"></a></li><!-- Slide 2 -->
                        <!-- <li><a class="li_3" href="#"></a></li><!-- Slide 3 -->
                        <!-- <li><a class="li_4" href="#"></a></li><!-- Slide 4 -->
                        <!-- <li><a class="li_5" href="#"></a></li><!-- Slide 5 --><?php 
						}?>
                    </ul>
                </div>
        	</div>
			<!-- End button Slide -->
        </div>
		<!--/slider -->
        
                    
        <div style="clear:both;"></div>
<?php $this->load->view('footer');
?>