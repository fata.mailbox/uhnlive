<?php $this->load->view('header'); ?>
<h2><?php echo $page_title; ?></h2>

<link rel="stylesheet" type="text/css" href="<?= site_url()?>assets/css/jquery.dataTables.css " >
<link rel="stylesheet" href="<?=site_url()?>assets/css/bootstrap.min.css">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script src="<?=site_url()?>assets/js/jquery-1.9.1.js"></script>
<script type="text/javascript" charset="utf8" src="<?= site_url()?>assets/js/jquery.dataTables.js"> </script>
<script src="<?=site_url()?>assets/js/bootstrap.min.js"></script>


<script>
	$(document).ready(function() {
		var dateToday = new Date();
		$(".from").datepicker({
			defaultDate: "+1h",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1
		});
		$(".to").datepicker({
			defaultDate: "+1w",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1
		});
	})
</script>
<style>
	.container {
		padding-right: 15px;
		padding-left: 15px;
		margin-right: auto;
		margin-left: auto;
	}

	@media (min-width: 768px) {
		.container {
			width: 750px;
		}
	}

	@media (min-width: 992px) {
		.container {
			width: 970px;
		}
	}

	@media (min-width: 1200px) {
		.container {
			width: 1170px;
		}
	}

	.container-fluid {
		padding-right: 15px;
		padding-left: 15px;
		margin-right: auto;
		margin-left: auto;
	}

	.nav {
		padding-left: 0;
		margin-bottom: 0;
		list-style: none;
	}

	.nav>li {
		position: relative;
		display: block;
	}

	.nav>li>a {
		position: relative;
		display: block;
		padding: 10px 15px;
	}

	.nav>li>a:hover,
	.nav>li>a:focus {
		text-decoration: none;
		background-color: #eeeeee;
	}

	.nav>li.disabled>a {
		color: #777777;
	}

	.nav>li.disabled>a:hover,
	.nav>li.disabled>a:focus {
		color: #777777;
		text-decoration: none;
		cursor: not-allowed;
		background-color: transparent;
	}

	.nav .open>a,
	.nav .open>a:hover,
	.nav .open>a:focus {
		background-color: #eeeeee;
		border-color: #337ab7;
	}

	.nav .nav-divider {
		height: 1px;
		margin: 9px 0;
		overflow: hidden;
		background-color: #e5e5e5;
	}

	.nav>li>a>img {
		max-width: none;
	}

	.nav-tabs {
		border-bottom: 1px solid #ddd;
	}

	.nav-tabs>li {
		float: left;
		margin-bottom: -1px;
	}

	.nav-tabs>li>a {
		margin-right: 2px;
		line-height: 1.42857143;
		border: 1px solid transparent;
		border-radius: 4px 4px 0 0;
	}

	.nav-tabs>li>a:hover {
		border-color: #eeeeee #eeeeee #ddd;
	}

	.nav-tabs>li.active>a,
	.nav-tabs>li.active>a:hover,
	.nav-tabs>li.active>a:focus {
		color: #555555;
		cursor: default;
		background-color: #fff;
		border: 1px solid #ddd;
		border-bottom-color: transparent;
	}

	.nav-tabs.nav-justified {
		width: 100%;
		border-bottom: 0;
	}

	.nav-tabs.nav-justified>li {
		float: none;
	}

	.nav-tabs.nav-justified>li>a {
		margin-bottom: 5px;
		text-align: center;
	}

	.nav-tabs.nav-justified>.dropdown .dropdown-menu {
		top: auto;
		left: auto;
	}

	@media (min-width: 768px) {
		.nav-tabs.nav-justified>li {
			display: table-cell;
			width: 1%;
		}

		.nav-tabs.nav-justified>li>a {
			margin-bottom: 0;
		}
	}

	.nav-tabs.nav-justified>li>a {
		margin-right: 0;
		border-radius: 4px;
	}

	.nav-tabs.nav-justified>.active>a,
	.nav-tabs.nav-justified>.active>a:hover,
	.nav-tabs.nav-justified>.active>a:focus {
		border: 1px solid #ddd;
	}

	@media (min-width: 768px) {
		.nav-tabs.nav-justified>li>a {
			border-bottom: 1px solid #ddd;
			border-radius: 4px 4px 0 0;
		}

		.nav-tabs.nav-justified>.active>a,
		.nav-tabs.nav-justified>.active>a:hover,
		.nav-tabs.nav-justified>.active>a:focus {
			border-bottom-color: #fff;
		}
	}

	.nav-pills>li {
		float: left;
	}

	.nav-pills>li>a {
		border-radius: 4px;
	}

	.nav-pills>li+li {
		margin-left: 2px;
	}

	.nav-pills>li.active>a,
	.nav-pills>li.active>a:hover,
	.nav-pills>li.active>a:focus {
		color: #fff;
		background-color: #337ab7;
	}

	.modal-lg {
		width: 1300px !important;
	}

	.nav-stacked>li {
		float: none;
	}

	.nav-stacked>li+li {
		margin-top: 2px;
		margin-left: 0;
	}

	.nav-justified {
		width: 100%;
	}

	.nav-justified>li {
		float: none;
	}

	.nav-justified>li>a {
		margin-bottom: 5px;
		text-align: center;
	}

	.nav-justified>.dropdown .dropdown-menu {
		top: auto;
		left: auto;
	}

	@media (min-width: 768px) {
		.nav-justified>li {
			display: table-cell;
			width: 1%;
		}

		.nav-justified>li>a {
			margin-bottom: 0;
		}
	}

	.nav-tabs-justified {
		border-bottom: 0;
	}

	.nav-tabs-justified>li>a {
		margin-right: 0;
		border-radius: 4px;
	}

	.nav-tabs-justified>.active>a,
	.nav-tabs-justified>.active>a:hover,
	.nav-tabs-justified>.active>a:focus {
		border: 1px solid #ddd;
	}

	.tab-content>.tab-pane {
		display: none;
	}

	.tab-content>.active {
		display: block;
	}

	.tab-content>.tab-pane {
		display: none;
	}

	.tab-content>.active {
		display: block;
	}

	.Stand {
		font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		border-collapse: collapse;
		width: 100%;
	}

	.Stand td,
	.Stand th {
		border: 1px solid #ddd;
		padding: 8px;
		color: black;
	}

	.Stand tr:nth-child(even) {
		background-color: #f2f2f2;
	}

	.Stand tr:hover {
		background-color: #ddd;
	}

	.Stand th {
		padding-top: 12px;
		padding-bottom: 12px;
		font-size: 13px;
		text-align: left;
		background-color: #4CAF50;
		color: white;
	}
</style>
<hr>
<div class="ViewHold">
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#home">Get Request Order</a></li>
		<li><a data-toggle="tab" href="#menu1">Get Sales Order</a></li>
		<li><a data-toggle="tab" href="#sc">Get SC</a></li>
		<li><a data-toggle="tab" href="#scpay">Get SC Payment</a></li>
		<li><a data-toggle="tab" href="#scretur">Get SC Retur</a></li>
		<li><a data-toggle="tab" href="#roretur">Get RO Retur</a></li>
		<li><a data-toggle="tab" href="#scadm">Get SC Admin</a></li>
		<li><a data-toggle="tab" href="#scadmrtr">Get SC Admin Retur</a></li>
		<li><a data-toggle="tab" href="#cancelbilling">Get Cancel Billing</a></li>
	</ul>

	<div class="tab-content">
		<!-- Home -->
		<div id="home" class="tab-pane fade in active">
			<br>
			<div class="row">

				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Status :</label>
						<select class="form-control" name="" id="status_getro">
							<option value="all">All</option>
							<option value="N">New Data </option>
							<option value="E">Error</option>
							<option value="S">Success</option>
						</select>
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date From :</label>
						<input type="text" class="form-control from" id="from_getro" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date To :</label>
						<input type="text" class="form-control to" autocomplete="off" id="to_getro" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-3">
					<br>
					<button onclick="filter('getro');" style="margin-right:10px" id="filter" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
					<button style="margin-right:5px" id="resendro" onclick="getval('ro')" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
					<button id="reload" onclick="getroOrder();" class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
				</div>

			</div>



			<br>
			<table class="Stand" id="myTable">
				<thead style="color:black">
					<tr>
						<th width="3%"><input id="chkParentro" type="checkbox"></th>
						<th width="14%">Header ID</th>
						<th width="40px">Transaction Date</th>
						<th width="50px">Status Response</th>
						<th width="10%">Action</th>
					</tr>
				</thead>

			</table>
		</div>
		<!-- Akhir Home -->
		<div id="menu1" class="tab-pane fade">
			<br>
			<div class="row">
				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Status :</label>
						<select class="form-control" name="" id="status_getso">
							<option value="all">All</option>
							<option value="N">New Data </option>
							<option value="E">Error</option>
							<option value="S">Success</option>
						</select>
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date From :</label>
						<input type="text" class="form-control from" id="from_getso" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date To :</label>
						<input type="text" class="form-control to" autocomplete="off" id="to_getso" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-3">
					<br>
					<button onclick="filter('getso');" style="margin-right:10px" id="filterso" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
					<button style="margin-right:5px" id="resendaso" onclick="getval('so')" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
					<button id="reload" onclick="getsoOrder();" class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
				</div>

			</div>
			<br>

			<table style="width:100%" class="Stand" id="Getso">
				<thead style="color:black">
					<tr>
						<th width="3%"><input id="chkParentso" type="checkbox"></th>
						<th>Header ID</th>
						<th>Transaction Date</th>
						<th>Status Response</th>
						<th width="10%">Action</th>
					</tr>
				</thead>
			</table>
		</div>

		<!-- Sc -->
		<div id="sc" class="tab-pane fade">
			<br>
			<div class="row">
				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Status :</label>
						<select class="form-control" name="" id="status_getsc">
							<option value="all">All</option>
							<option value="N">New Data </option>
							<option value="E">Error</option>
							<option value="S">Success</option>
						</select>
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date From :</label>
						<input type="text" class="form-control from" id="from_getsc" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date To :</label>
						<input type="text" class="form-control to" autocomplete="off" id="to_getsc" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-3">
					<br>
					<button onclick="filter('getsc');" style="margin-right:10px" id="filtersc" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
					<button style="margin-right:5px" id="resendasc" onclick="getval('sc')" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
					<button id="reload" onclick="getSC();" class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
				</div>

			</div>
			<br>
			<table style="width:100%" class="Stand" id="TblSc">
				<thead style="color:black">
					<tr>
						<th width="5%"><input id="chkParentSc" type="checkbox"></th>
						<th>Header ID</th>
						<th>Transaction Date</th>
						<th>Status Response</th>
						<th width="10%">Action</th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- Sc Out -->


		<!-- Sc -->
		<div id="scpay" class="tab-pane fade">
			<br>
			<div class="row">

				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Status :</label>
						<select class="form-control" name="" id="status_getscpayment">
							<option value="all">All</option>
							<option value="N">New Data </option>
							<option value="E">Error</option>
							<option value="S">Success</option>
						</select>
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date From :</label>
						<input type="text" class="form-control from" id="from_getscpayment" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date To :</label>
						<input type="text" class="form-control to" autocomplete="off" id="to_getscpayment" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-3">
					<br>
					<button onclick="filter('getscpayment');" style="margin-right:10px" id="filterscpayment" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
					<button style="margin-right:5px" id="resendascpayment" onclick="getval('scpayment')" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
					<button id="reload" onclick="getSCPayment();" class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
				</div>

			</div>
			<br>
			<table style="width:100%" class="Stand" id="TblScpayment">
				<thead style="color:black">
					<tr>
						<th width="5%"><input id="chkParentScPayment" type="checkbox"></th>
						<th>Header ID</th>
						<th>Transaction Date</th>
						<th>Status Response</th>
						<th width="10%">Action</th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- Sc Out -->




		<!-- Sc -->
		<div id="scretur" class="tab-pane fade">
			<br>
			<div class="row">
				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Status :</label>
						<select class="form-control" name="" id="status_getscretur">
							<option value="all">All</option>
							<option value="N">New Data </option>
							<option value="E">Error</option>
							<option value="S">Success</option>
						</select>
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date From :</label>
						<input type="text" class="form-control from" id="from_getscretur" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date To :</label>
						<input type="text" class="form-control to" autocomplete="off" id="to_getscretur" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-3">
					<br>
					<button onclick="filter('getscretur');" style="margin-right:10px" id="filterscretur" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
					<button style="margin-right:5px" id="resendascretur" onclick="getval('scretur')" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
					<button id="reload" onclick="getSCretur();" class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
				</div>

			</div>
			<br>
			<table style="width:100%" class="Stand" id="TblScRetur">
				<thead style="color:black">
					<tr>
						<th width="5%"><input id="chkParentScReturn" type="checkbox"></th>
						<th>Header ID</th>
						<th>Transaction Date</th>
						<th>Status Response</th>
						<th width="10%">Action</th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- Sc Out -->




		<!-- Sc -->
		<div id="roretur" class="tab-pane fade">
			<br>
			<div class="row">
				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Status :</label>
						<select class="form-control" name="" id="status_getroretur">
							<option value="all">All</option>
							<option value="N">New Data </option>
							<option value="E">Error</option>
							<option value="S">Success</option>
						</select>
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date From :</label>
						<input type="text" class="form-control from" id="from_getroretur" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date To :</label>
						<input type="text" class="form-control to" autocomplete="off" id="to_getroretur" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-3">
					<br>
					<button onclick="filter('getroretur');" style="margin-right:10px" id="filterroretur" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
					<button style="margin-right:5px" id="resendroretur" onclick="getval('roretur')" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
					<button id="reload" onclick="getROretur();" class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
				</div>

			</div>
			<br>
			<table style="width:100%" class="Stand" id="TblROretur">
				<thead style="color:black">
					<tr>
						<th width="5%"><input id="chkParentROReturn" type="checkbox"></th>
						<th>Header ID</th>
						<th>Transaction Date</th>
						<th>Status Response</th>
						<th width="10%">Action</th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- Sc Out -->


		<!-- Sc -->
		<div id="scadm" class="tab-pane fade">
			<br>
			<div class="row">
				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Status :</label>
						<select class="form-control" name="" id="status_getscadmin">
							<option value="all">All</option>
							<option value="N">New Data </option>
							<option value="E">Error</option>
							<option value="S">Success</option>
						</select>
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date From :</label>
						<input type="text" class="form-control from" id="from_getscadmin" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date To :</label>
						<input type="text" class="form-control to" autocomplete="off" id="to_getscadmin" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-3">
					<br>
					<button onclick="filter('getscadmin');" style="margin-right:10px" id="filterscadmin" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
					<button style="margin-right:5px" id="resendscadmin" onclick="getval('scadmin')" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
					<button id="reload" onclick="getSCAdm();" class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
				</div>

			</div>
			<br>
			<table style="width:100%" class="Stand" id="TblscAdm">
				<thead style="color:black">
					<tr>
						<th width="5%"><input id="chkParentSCAdm" type="checkbox"></th>
						<th>Header ID</th>
						<th>Transaction Date</th>
						<th>Status Response</th>
						<th width="10%">Action</th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- Sc Out -->

		<!-- Sc -->
		<div id="scadmrtr" class="tab-pane fade">
			<br>
			<div class="row">
				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Status :</label>
						<select class="form-control" name="" id="status_getscadminretur">
							<option value="all">All</option>
							<option value="N">New Data </option>
							<option value="E">Error</option>
							<option value="S">Success</option>
						</select>
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date From :</label>
						<input type="text" class="form-control from" id="from_getscadminretur" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date To :</label>
						<input type="text" class="form-control to" autocomplete="off" id="to_getscadminretur" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-3">
					<br>

					<button onclick="filter('getscadminretur');" style="margin-right:10px" id="filterscadminretur" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
					<button style="margin-right:5px" id="resendscadminretur" onclick="getval('scadminretur')" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
					<button id="reload" onclick="getSCAdmRetur();" class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>

				</div>

			</div>
			<br>
			<table style="width:100%" class="Stand" id="TblScAdmRetur">
				<thead style="color:black">
					<tr>
						<th width="5%"><input id="chkParentAdmRetur" type="checkbox"></th>
						<th>Header ID</th>
						<th>Transaction Date</th>
						<th>Status Response</th>
						<th width="10%">Action</th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- Sc Out -->







		<!-- Sc -->
		<div id="cancelbilling" class="tab-pane fade">
			<br>
			<div class="row">
				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Status :</label>
						<select class="form-control" name="" id="status_getcancelbilling">
							<option value="all">All</option>
							<option value="N">New Data </option>
							<option value="E">Error</option>
							<option value="S">Success</option>
						</select>
					</div>
				</div>

				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Source :</label>
						<select class="form-control" name="" id="source_data">
							<option value="all">All</option>
							<option value="ZU02">Sales Order</option>
							<option value="ZU01">Request Order</option>
						</select>
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date From :</label>
						<input type="text" class="form-control from" id="from_getcancelbilling" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-2">
					<div style="color:black;font-size:11px" class="form-group">
						<label for="">Date To :</label>
						<input type="text" class="form-control to" autocomplete="off" id="to_getcancelbilling" data-language="en" placeholder="YYYY-MM-DD" name="datefrom">
					</div>
				</div>


				<div class="col-md-3">
					<br>

					<button onclick="filter('getcancelbilling');" style="margin-right:10px" id="filterscadminretur" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
					<button style="margin-right:5px" id="resendcancelbilling" onclick="getval('cancelbilling')" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
					<button id="reload" onclick="getCancel();" class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>

				</div>

			</div>
			<br>
			<table style="width:100%" class="Stand" id="TblCancel">
				<thead style="color:black">
					<tr>
						<th width="5%"><input id="chkParentCancel" type="checkbox"></th>
						<th>Source</th>
						<th>Header ID</th>
						<th>Transaction Date</th>
						<th>Status Response</th>
						<th width="10%">Action</th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- Sc Out -->



	</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 style="color:black" class="modal-title">Detail E-Sales </h3>
				<h4 style="color:black" id="text"></h4>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="csss" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>U_AUART</th>
								<th>U_UHINV</th>
								<th>U_POSNR</th>
								<th>U_SUBNR</th>
								<th>U_SODAT</th>
								<th>U_KUNNR</th>
								<th>U_CTYPE</th>
								<th>U_PL_WE</th>
								<th>U_TAXCLASS</th>
								<th>VKBUR</th>
								<th>WERKS</th>
								<th>MATNR</th>
								<th>ARKTX</th>
								<th>MATKL</th>
								<th>CHARG</th>
								<th>KWMENG</th>
								<th>COGS</th>
								<th>ZUHN</th>
								<th>ZUA1</th>
								<th>ZUA2</th>
								<th>ZUA4</th>
								<th>U_VCDNO</th>
								<th>U_FGOOD</th>
								<th>PV</th>
								<th>BV</th>
								<th>YYBANDCD</th>
								<th>EWALLET_HDR</th>
								<th>EWALLET_ITM</th>
								<th>ITEM_TYPE</th>
								<th>WAERS</th>
								<th>AUGRU</th>
								<th>U_REF_CNCL_AUART</th>
								<th>U_REF_CNCL_INV</th>
								<th>U_REF_CNCL_SUBNR</th>
								<th>UHDAT</th>
								<th>UHZET</th>
								<th>Status</th>
								<th>Description</th>
							</tr>
						</thead>

					</table>
				</div>

			</div>
			<div class="modal-footer">
				<button onclick="resetT()" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>


<!--Modal Kedual-->



<!-- Modal -->
<div id="myModalSo" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 style="color:black" class="modal-title">Detail E-Sales </h3>
				<h4 style="color:black" id="text_so"></h4>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="csss_so" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>U_AUART</th>
								<th>U_UHINV</th>
								<th>U_POSNR</th>
								<th>U_SUBNR</th>
								<th>U_SODAT</th>
								<th>U_KUNNR</th>
								<th>U_CTYPE</th>
								<th>U_PL_WE</th>
								<th>U_TAXCLASS</th>
								<th>VKBUR</th>
								<th>WERKS</th>
								<th>MATNR</th>
								<th>ARKTX</th>
								<th>MATKL</th>
								<th>CHARG</th>
								<th>KWMENG</th>
								<th>COGS</th>
								<th>ZUHN</th>
								<th>ZUA1</th>
								<th>ZUA2</th>
								<th>ZUA4</th>
								<th>U_VCDNO</th>
								<th>U_FGOOD</th>
								<th>PV</th>
								<th>BV</th>
								<th>YYBANDCD</th>
								<th>EWALLET_HDR</th>
								<th>EWALLET_ITM</th>
								<th>ITEM_TYPE</th>
								<th>WAERS</th>
								<th>AUGRU</th>
								<th>U_REF_CNCL_INV</th>
								<th>U_REF_CNCL_AUART</th>
								<th>Status</th>
								<th>Description</th>
							</tr>
						</thead>

					</table>
				</div>

			</div>
			<div class="modal-footer">
				<button onclick="resetT()" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>
<!--akhirmodal--->
<?php
$this->load->view('footer');
?>

<script>
	function resetT() {
		if ($.fn.dataTable.isDataTable('#csss')) {
			var table = $("#csss").DataTable();
			table.destroy();
		}

		if ($.fn.dataTable.isDataTable('#csss_so')) {
			var table = $("#csss_so").DataTable();
			table.destroy();
		}

	}


	function get(tgl, table) {
		var table = $(table).DataTable();
		$("." + tgl).change(function() {
			var rows = table.rows({
				'search': 'applied'
			}).nodes();
			$('.' + tgl, rows).prop('checked', this.checked);
		});
	}

	function getroOrder(act = 'txt') {
		if ($.fn.dataTable.isDataTable('#myTable')) {
			var table = $("#myTable").DataTable();
			table.destroy();
		}

		if (act == 'running') {
			var ct = 'getro';
			var status = $('#status_' + ct).val();
			var from = $('#from_' + ct).val();
			var to = $('#to_' + ct).val();

		} else {
			var status = '';
			var from = '';
			var to = '';
		}
		var table = $('#myTable').DataTable({
			'ajax': {
				"url": "<?= base_url(); ?>api/Sales/get_data_getro",
				"type": "POST",
				"data": {
					"status": status,
					"from": from,
					"to": to
				}
			},
			'language': {
				'processing': 'Silahkan Tunggu !'
			},
			'columnDefs': [{
				'targets': 0,
				'searchable': false,
				'orderable': false,
				'render': function(data, type, full, meta) {
					var tgl = full[2].replace(".", "-").replace(".", "-");
					return '<input onclick=get("' + tgl + '","#myTable") type="checkbox" name="code" class="' + tgl + '" value="' +
						$('<div/>').text(data).html() + '">';
				}
			}],
			'order': [2, 'desc']
		});

		$('#chkParentro').on('click', function() {
			var rows = table.rows({
				'search': 'applied'
			}).nodes();
			$('input[type="checkbox"]', rows).prop('checked', this.checked);
		});
	}


	function getSCAdmRetur(act = 'txt') {
		if ($.fn.dataTable.isDataTable('#TblScAdmRetur')) {
			var table = $("#TblScAdmRetur").DataTable();
			table.destroy();
		}

		if (act == 'running') {
			var ct = 'getscadm';
			var status = $('#status_' + ct).val();
			var from = $('#from_' + ct).val();
			var to = $('#to_' + ct).val();

		} else {
			var status = '';
			var from = '';
			var to = '';
		}
		var table = $('#TblScAdmRetur').DataTable({
			'ajax': {
				"url": "<?= base_url(); ?>api/Sales/get_data_scadminretur",
				"type": "POST",
				"data": {
					"status": status,
					"from": from,
					"to": to
				}
			},
			'language': {
				'processing': 'Silahkan Tunggu !'
			},
			'columnDefs': [{
				'targets': 0,
				'searchable': false,
				'orderable': false,
				'render': function(data, type, full, meta) {
					var tgl = full[2].replace(".", "-").replace(".", "-");
					return '<input onclick=get("' + tgl + '","#TblScAdmRetur") type="checkbox" name="code" class="' + tgl + '" value="' +
						$('<div/>').text(data).html() + '">';
				}
			}],
			'order': [2, 'desc']
		});

		$('#chkParentAdmRetur').on('click', function() {
			var rows = table.rows({
				'search': 'applied'
			}).nodes();
			$('input[type="checkbox"]', rows).prop('checked', this.checked);
		});
	}


	function getSCAdm(act = 'txt') {
		if ($.fn.dataTable.isDataTable('#TblscAdm')) {
			var table = $("#TblscAdm").DataTable();
			table.destroy();
		}

		if (act == 'running') {
			var ct = 'getscadm';
			var status = $('#status_' + ct).val();
			var from = $('#from_' + ct).val();
			var to = $('#to_' + ct).val();

		} else {
			var status = '';
			var from = '';
			var to = '';
		}
		var table = $('#TblscAdm').DataTable({
			'ajax': {
				"url": "<?= base_url(); ?>api/Sales/get_data_scadmin",
				"type": "POST",
				"data": {
					"status": status,
					"from": from,
					"to": to
				}
			},
			'language': {
				'processing': 'Silahkan Tunggu !'
			},
			'columnDefs': [{
				'targets': 0,
				'searchable': false,
				'orderable': false,
				'render': function(data, type, full, meta) {
					var tgl = full[2].replace(".", "-").replace(".", "-");
					return '<input onclick=get("' + tgl + '","#TblscAdm") type="checkbox" name="code" class="' + tgl + '" value="' +
						$('<div/>').text(data).html() + '">';
				}
			}],
			'order': [2, 'desc']
		});

		$('#chkParentSCAdm').on('click', function() {
			var rows = table.rows({
				'search': 'applied'
			}).nodes();
			$('input[type="checkbox"]', rows).prop('checked', this.checked);
		});
	}



	function getSC(act = 'txt') {
		if ($.fn.dataTable.isDataTable('#TblSc')) {
			var table = $("#TblSc").DataTable();
			table.destroy();
		}

		if (act == 'running') {
			var ct = 'getsc';
			var status = $('#status_' + ct).val();
			var from = $('#from_' + ct).val();
			var to = $('#to_' + ct).val();

		} else {
			var status = '';
			var from = '';
			var to = '';
		}
		var table = $('#TblSc').DataTable({
			'ajax': {
				"url": "<?= base_url(); ?>api/Sales/get_data_sc",
				"type": "POST",
				"data": {
					"status": status,
					"from": from,
					"to": to
				}
			},
			'language': {
				'processing': 'Silahkan Tunggu !'
			},
			'columnDefs': [{
				'targets': 0,
				'searchable': false,
				'orderable': false,
				'render': function(data, type, full, meta) {
					var tgl = full[2].replace(".", "-").replace(".", "-");
					return '<input onclick=get("' + tgl + '","#TblSc") type="checkbox" name="code" class="' + tgl + '" value="' +
						$('<div/>').text(data).html() + '">';
				}
			}],
			'order': [2, 'desc']
		});

		$('#chkParentSc').on('click', function() {
			var rows = table.rows({
				'search': 'applied'
			}).nodes();
			$('input[type="checkbox"]', rows).prop('checked', this.checked);
		});
	}





	function getCancel(act = 'txt', source = 'all') {
		if ($.fn.dataTable.isDataTable('#TblCancel')) {
			var table = $("#TblCancel").DataTable();
			table.destroy();
		}

		if (act == 'running') {
			var ct = 'getcancelbilling';
			var status = $('#status_' + ct).val();
			var from = $('#from_' + ct).val();
			var to = $('#to_' + ct).val();
			var source = source;
		} else {
			var status = '';
			var from = '';
			var to = '';
			var source = '';
		}
		var table = $('#TblCancel').DataTable({
			'ajax': {
				"url": "<?= base_url(); ?>api/Sales/get_data_cancel",
				"type": "POST",
				"data": {
					"status": status,
					"from": from,
					"to": to,
					"source": source
				}
			},
			'language': {
				'processing': 'Silahkan Tunggu !'
			},
			'columnDefs': [{
				'targets': 0,
				'searchable': false,
				'orderable': false,
				'render': function(data, type, full, meta) {
					var tgl = full[3].replace(".", "-").replace(".", "-");
					return '<input onclick=get("' + tgl + '","#TblCancel") type="checkbox" name="code" class="' + tgl + '" value="' +
						$('<div/>').text(data).html() + '">';
				}
			}],
			'order': [1, 'desc']
		});

		$('#chkParentCancel').on('click', function() {
			var rows = table.rows({
				'search': 'applied'
			}).nodes();
			$('input[type="checkbox"]', rows).prop('checked', this.checked);
		});
	}


	function getSCPayment(act = 'txt') {
		if ($.fn.dataTable.isDataTable('#TblScpayment')) {
			var table = $("#TblScpayment").DataTable();
			table.destroy();
		}

		if (act == 'running') {
			var ct = 'getscpayment';
			var status = $('#status_' + ct).val();
			var from = $('#from_' + ct).val();
			var to = $('#to_' + ct).val();

		} else {
			var status = '';
			var from = '';
			var to = '';
		}
		var table = $('#TblScpayment').DataTable({
			'ajax': {
				"url": "<?= base_url(); ?>api/Sales/get_data_scpayment",
				"type": "POST",
				"data": {
					"status": status,
					"from": from,
					"to": to
				}
			},
			'language': {
				'processing': 'Silahkan Tunggu !'
			},
			'columnDefs': [{
				'targets': 0,
				'searchable': false,
				'orderable': false,
				'render': function(data, type, full, meta) {
					var tgl = full[2].replace(".", "-").replace(".", "-");
					return '<input onclick=get("' + tgl + '","#TblScpayment") type="checkbox" name="code" class="' + tgl + '" value="' +
						$('<div/>').text(data).html() + '">';
				}
			}],
			'order': [2, 'desc']
		});

		$('#chkParentScPayment').on('click', function() {
			var rows = table.rows({
				'search': 'applied'
			}).nodes();
			$('input[type="checkbox"]', rows).prop('checked', this.checked);
		});
	}



	function getSCretur(act = 'txt') {
		if ($.fn.dataTable.isDataTable('#TblScRetur')) {
			var table = $("#TblScRetur").DataTable();
			table.destroy();
		}

		if (act == 'running') {
			var ct = 'getscretur';
			var status = $('#status_' + ct).val();
			var from = $('#from_' + ct).val();
			var to = $('#to_' + ct).val();

		} else {
			var status = '';
			var from = '';
			var to = '';
		}
		var table = $('#TblScRetur').DataTable({
			'ajax': {
				"url": "<?= base_url(); ?>api/Sales/get_data_scretur",
				"type": "POST",
				"data": {
					"status": status,
					"from": from,
					"to": to
				}
			},
			'language': {
				'processing': 'Silahkan Tunggu !'
			},
			'columnDefs': [{
				'targets': 0,
				'searchable': false,
				'orderable': false,
				'render': function(data, type, full, meta) {
					var tgl = full[2].replace(".", "-").replace(".", "-");
					return '<input onclick=get("' + tgl + '","#TblScRetur") type="checkbox" name="code" class="' + tgl + '" value="' +
						$('<div/>').text(data).html() + '">';
				}
			}],
			'order': [2, 'desc']
		});

		$('#chkParentScReturn').on('click', function() {
			var rows = table.rows({
				'search': 'applied'
			}).nodes();
			$('input[type="checkbox"]', rows).prop('checked', this.checked);
		});
	}




	function getROretur(act = 'txt') {
		if ($.fn.dataTable.isDataTable('#TblROretur')) {
			var table = $("#TblROretur").DataTable();
			table.destroy();
		}

		if (act == 'running') {
			var ct = 'getscpayment';
			var status = $('#status_' + ct).val();
			var from = $('#from_' + ct).val();
			var to = $('#to_' + ct).val();

		} else {
			var status = '';
			var from = '';
			var to = '';
		}
		var table = $('#TblROretur').DataTable({
			'ajax': {
				"url": "<?= base_url(); ?>api/Sales/get_data_roretur",
				"type": "POST",
				"data": {
					"status": status,
					"from": from,
					"to": to
				}
			},
			'language': {
				'processing': 'Silahkan Tunggu !'
			},
			'columnDefs': [{
				'targets': 0,
				'searchable': false,
				'orderable': false,
				'render': function(data, type, full, meta) {
					var tgl = full[2].replace(".", "-").replace(".", "-");
					return '<input onclick=get("' + tgl + '","#TblROretur") type="checkbox" name="code" class="' + tgl + '" value="' +
						$('<div/>').text(data).html() + '">';
				}
			}],
			'order': [2, 'desc']
		});

		$('#chkParentROReturn').on('click', function() {
			var rows = table.rows({
				'search': 'applied'
			}).nodes();
			$('input[type="checkbox"]', rows).prop('checked', this.checked);
		});
	}



	function getsoOrder(act = 'txt') {
		if ($.fn.dataTable.isDataTable('#Getso')) {
			var table = $("#Getso").DataTable();
			table.destroy();
		}

		if (act == 'running') {
			var ct = 'getso';
			var status = $('#status_' + ct).val();
			var from = $('#from_' + ct).val();
			var to = $('#to_' + ct).val();

		} else {
			var status = '';
			var from = '';
			var to = '';
		}
		var table = $('#Getso').DataTable({
			'ajax': {
				"url": "<?= base_url(); ?>api/Sales/get_data_getso",
				"type": "POST",
				"data": {
					"status": status,
					"from": from,
					"to": to
				}
			},
			'language': {
				'processing': 'Silahkan Tunggu !'
			},
			'columnDefs': [{
				'targets': 0,
				'searchable': false,
				'orderable': false,
				'render': function(data, type, full, meta) {
					var tgl = full[2].replace(".", "-").replace(".", "-");
					return '<input onclick=get("' + tgl + '","#Getso") type="checkbox" name="code" class="' + tgl + '" value="' +
						$('<div/>').text(data).html() + '">';
				}
			}],
			'order': [2, 'desc']
		});

		$('#chkParentso').on('click', function() {
			var rows = table.rows({
				'search': 'applied'
			}).nodes();
			$('input[type="checkbox"]', rows).prop('checked', this.checked);
		});
	}



	$(document).ready(function() {
		getroOrder();
		getsoOrder();
		getSC();
		getSCPayment();
		getSCretur();
		getROretur();
		getSCAdm();
		getSCAdmRetur();
		getCancel();
	});


	function resetT() {
		if ($.fn.dataTable.isDataTable('#csss')) {
			var table = $("#csss").DataTable();
			table.destroy();
		}

		if ($.fn.dataTable.isDataTable('#csss_so')) {
			var table = $("#csss_so").DataTable();
			table.destroy();
		}
	}

	function parseToInt(value) {
		var res = value.replace(/-/g, '');
		$('#adjMove').DataTable();
		$('#scpaytbl').DataTable();
		$('#screturtbl').DataTable();
		$('#roreturtbl').DataTable();
		$('#scadmtbl').DataTable();
		$('#scadmrtrtbl').DataTable();
		return parseInt(res);
	}

	function filter(act) {
		var status = $('#status_' + act).val();
		var from = $('#from_' + act).val();
		var to = $('#to_' + act).val();
		if (to == '' || from == '') {
			alert('Silahkan Pilih Tanggal !');
			return false;
		} else {
			var from = parseToInt($('#from_' + act).val());
			var to = parseToInt($('#to_' + act).val());
			if (to < from) {
				alert("Date To harus lebih dari Date From !");
				return false;
			} else {
				if (act == 'getro') {
					getroOrder('running');
				} else if (act == 'getso') {
					getsoOrder('running');
				} else if (act == 'getsc') {
					getSC('running');
				} else if (act == 'getscpayment') {
					getSCPayment('running');
				} else if (act == 'getscretur') {
					getSCretur('running');
				} else if (act == 'getroretur') {
					getROretur('running');
				} else if (act == 'getscadmin') {
					getSCAdm('running');
				} else if (act == 'getscadminretur') {
					getSCAdmRetur('running');
				} else if (act == 'getcancelbilling') {
					var source = $('#source_data').val();
					getCancel('running', source);
				}
			}
		}
	}



	function Jejak(id, act, param = '') {
		if (act == 'RequestOrder' || act == 'SC' || act == 'SCPayment' || act == 'ScAdmin' || act == 'SCRetur' || act == 'RORetur' || act == 'SCRetur' || act == 'SalesOrder') {
			$('#myModal').modal({
				show: 'true'
			});
			if ($.fn.dataTable.isDataTable('#csss')) {
				var table = $("#csss").DataTable();
				table.destroy();
			}
			$('#text').text('E-Sales' + ' - ' + act + ' : ' + id);
			var table = $('#csss').DataTable({
				'ajax': {
					"url": "<?= base_url(); ?>api/Sales/get_data_user",
					"type": "POST",
					"data": {
						"id": id,
						"act": act,
						"param": param
					}
				},
				'language': {
					'processing': 'Silahkan Tunggu !'
				},
				"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

					if (aData[36] == 'ERROR SEND DATA') {
						$('td', nRow).css('background-color', '#ff872b', 'color', 'white');
					} else if (aData[36] == 'WARNING SEND DATA') {
						$('td', nRow).css('background-color', '#f2c911', 'color', 'white');
					} else {
						$('td', nRow).css('background-color', 'White');
					}
				},

				'columnDefs': [{
					//'targets': 0,
					'searchable': false,
					'targets': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
					'orderable': false
				}]
			});

		} else if (act == 'CancelBilling') {
			$('#myModal').modal({
				show: 'true'
			});
			if ($.fn.dataTable.isDataTable('#csss')) {
				var table = $("#csss").DataTable();
				table.destroy();
			}
			$('#text').text('E-Sales' + ' - ' + act + ' ' + param.toUpperCase() + ' : ' + id);
			var table = $('#csss').DataTable({
				'ajax': {
					"url": "<?= base_url(); ?>api/Sales/get_data_user",
					"type": "POST",
					"data": {
						"id": id,
						"act": act,
						"param": param
					}
				},
				'language': {
					'processing': 'Silahkan Tunggu !'
				},
				"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

					if (aData[36] == 'ERROR SEND DATA') {
						$('td', nRow).css('background-color', '#ff872b', 'color', 'white');
					} else if (aData[36] == 'WARNING SEND DATA') {
						$('td', nRow).css('background-color', '#f2c911', 'color', 'white');
					} else {
						$('td', nRow).css('background-color', 'White');
					}
				},

				'columnDefs': [{
					//'targets': 0,
					'searchable': false,
					'targets': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
					'orderable': false
				}]
			});
		} else {

			$('#myModalSo').modal({
				show: 'true'
			});
			if ($.fn.dataTable.isDataTable('#csss_so')) {
				var table = $("#csss_so").DataTable();
				table.destroy();
			}
			$('#text_so').text('E-Sales' + ' - ' + act + ' : ' + id);
			var table = $('#csss_so').DataTable({
				'ajax': {
					"url": "<?= base_url(); ?>api/Sales/get_data_user",
					"type": "POST",
					"data": {
						"id": id,
						"act": act
					}
				},
				'language': {
					'processing': 'Silahkan Tunggu !'
				},
				"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

					if (aData[33] == 'ERROR SEND DATA') {
						$('td', nRow).css('background-color', '#ff872b', 'color', 'white');
					} else if (aData[36] == 'WARNING SEND DATA') {
						$('td', nRow).css('background-color', '#f2c911', 'color', 'white');
					} else {
						$('td', nRow).css('background-color', 'White');
					}
				},

				'columnDefs': [{
					//'targets': 0,
					'searchable': false,
					'targets': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
					'orderable': false
				}]
			});
		}


	}




	function xhrAjax(data, url) {

		if (url == 'ro') {
			var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/ESales/ajax_post_ro_get/ZU01";
		} else if (url == 'so') {
			var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/ESales/ajax_post_ro_get/ZU02";
		} else if (url == 'sc') {
			var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/ESales/ajax_post_ro_get/ZU03";
		} else if (url == 'scpayment') {
			var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/ESales/ajax_post_ro_get/ZU05";
		} else if (url == 'scretur') {
			var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/ESales/ajax_post_ro_get/ZU04";
		} else if (url == 'roretur') {
			var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/ESales/ajax_post_ro_get/ZU07";
		} else if (url == 'scadmin') {
			var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/ESales/ajax_post_ro_get/ZU03";
		} else if (url == 'scadminretur') {
			var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/ESales/ajax_post_ro_get/ZU08";
		} else if (url == 'cancelbilling') {
			var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/ESales/ajax_post_cancel_get/ZU06";
		}


		$.ajax({
			url: curl,
			method: 'POST',
			dataType: "json",
			data: {
				'data': data
			},
			success: function(datza) {
				alert(datza);
				if (url == 'ro') {
					getroOrder();
					document.getElementById('from_getro').value = '';
					document.getElementById('to_getro').value = '';
					document.getElementById("resendro").disabled = false;
					document.getElementById("chkParentro").checked = false;
				} else if (url == 'so') {
					getsoOrder();
					document.getElementById('from_getso').value = '';
					document.getElementById('to_getso').value = '';
					document.getElementById("resendaso").disabled = false;
					document.getElementById("chkParentso").checked = false;
				} else if (url == 'sc') {
					getSC();
					document.getElementById('from_getsc').value = '';
					document.getElementById('to_getsc').value = '';
					document.getElementById("resendasc").disabled = false;
					document.getElementById("chkParentSc").checked = false;
				} else if (url == 'scpayment') {
					getSCPayment()
					document.getElementById('from_getscpayment').value = '';
					document.getElementById('to_getscpayment').value = '';
					document.getElementById("resendascpayment").disabled = false;
					document.getElementById("chkParentScPayment").checked = false;
				} else if (url == 'scretur') {
					getSCretur();
					document.getElementById('from_getscretur').value = '';
					document.getElementById('to_getscretur').value = '';
					document.getElementById("resendascretur").disabled = false;
					document.getElementById("chkParentScReturn").checked = false;
				} else if (url == 'roretur') {
					getROretur();
					document.getElementById('from_getroretur').value = '';
					document.getElementById('to_getroretur').value = '';
					document.getElementById("resendroretur").disabled = false;
					document.getElementById("chkParentROReturn").checked = false;
				} else if (url == 'scadmin') {
					getSCAdm();
					document.getElementById('from_getscadmin').value = '';
					document.getElementById('to_getscadmin').value = '';
					document.getElementById("resendscadmin").disabled = false;
					document.getElementById("chkParentSCAdm").checked = false;
				} else if (url == 'getscadminretur') {
					getSCAdmRetur();
					document.getElementById('from_getscadminretur').value = '';
					document.getElementById('to_getscadminretur').value = '';
					document.getElementById("resendscadminretur").disabled = false;
					document.getElementById("chkParentAdmRetur").checked = false;
				} else if (url == 'cancelbilling') {
					getCancel();
					document.getElementById('from_getcancelbilling').value = '';
					document.getElementById('to_getcancelbilling').value = '';
					document.getElementById("resendcancelbilling").disabled = false;
					document.getElementById("chkParentCancel").checked = false;
				}
			},
			error: function() {
				alert('Gagal Resend');
				if (url == 'ro') {
					getroOrder();
					document.getElementById('from_getro').value = '';
					document.getElementById('to_getro').value = '';
					document.getElementById("resendro").disabled = false;
					document.getElementById("chkParentro").checked = false;
				} else if (url == 'so') {
					getsoOrder();
					document.getElementById('from_getso').value = '';
					document.getElementById('to_getso').value = '';
					document.getElementById("resendaso").disabled = false;
					document.getElementById("chkParentso").checked = false;
				} else if (url == 'sc') {
					getSC();
					document.getElementById('from_getsc').value = '';
					document.getElementById('to_getsc').value = '';
					document.getElementById("resendasc").disabled = false;
					document.getElementById("chkParentSc").checked = false;
				} else if (url == 'scpayment') {
					getSCPayment()
					document.getElementById('from_getscpayment').value = '';
					document.getElementById('to_getscpayment').value = '';
					document.getElementById("resendascpayment").disabled = false;
					document.getElementById("chkParentScPayment").checked = false;
				} else if (url == 'scretur') {
					getSCretur();
					document.getElementById('from_getscretur').value = '';
					document.getElementById('to_getscretur').value = '';
					document.getElementById("resendascretur").disabled = false;
					document.getElementById("chkParentScReturn").checked = false;
				} else if (url == 'roretur') {
					document.getElementById('from_getroretur').value = '';
					document.getElementById('to_getroretur').value = '';
					document.getElementById("resendroretur").disabled = false;
					document.getElementById("chkParentROReturn").checked = false;
				} else if (url == 'scadmin') {
					getSCAdm();
					document.getElementById('from_getscadmin').value = '';
					document.getElementById('to_getscadmin').value = '';
					document.getElementById("resendscadmin").disabled = false;
					document.getElementById("chkParentSCAdm").checked = false;
				} else if (url == 'getscadminretur') {
					getSCAdmRetur();
					document.getElementById('from_getscadminretur').value = '';
					document.getElementById('to_getscadminretur').value = '';
					document.getElementById("resendscadminretur").disabled = false;
					document.getElementById("chkParentAdmRetur").checked = false;
				} else if (url == 'cancelbilling') {
					getCancel();
					document.getElementById('from_getcancelbilling').value = '';
					document.getElementById('to_getcancelbilling').value = '';
					document.getElementById("resendcancelbilling").disabled = false;
					document.getElementById("chkParentCancel").checked = false;
				}
			}
		});
	}


	function getval(act) {
		var arr = [];

		if (act == 'ro') {
			let table = $('#myTable').DataTable();
			let checkedvalues = table.$('input:checked').each(function() {
				arr.push($(this).val());
			});
		} else if (act == 'so') {
			let table = $('#Getso').DataTable();
			let checkedvalues = table.$('input:checked').each(function() {
				arr.push($(this).val());
			});
		} else if (act == 'sc') {
			let table = $('#TblSc').DataTable();
			let checkedvalues = table.$('input:checked').each(function() {
				arr.push($(this).val());
			});
		} else if (act == 'scpayment') {
			let table = $('#TblScpayment').DataTable();
			let checkedvalues = table.$('input:checked').each(function() {
				arr.push($(this).val());
			});
		} else if (act == 'scretur') {
			let table = $('#TblScRetur').DataTable();
			let checkedvalues = table.$('input:checked').each(function() {
				arr.push($(this).val());
			});
		} else if (act == 'roretur') {
			let table = $('#TblROretur').DataTable();
			let checkedvalues = table.$('input:checked').each(function() {
				arr.push($(this).val());
			});
		} else if (act == 'scadmin') {
			let table = $('#TblscAdm').DataTable();
			let checkedvalues = table.$('input:checked').each(function() {
				arr.push($(this).val());
			});
		} else if (act == 'scadminretur') {
			let table = $('#TblScAdmRetur').DataTable();
			let checkedvalues = table.$('input:checked').each(function() {
				arr.push($(this).val());
			});
		} else if (act == 'cancelbilling') {
			let table = $('#TblCancel').DataTable();
			let checkedvalues = table.$('input:checked').each(function() {
				arr.push($(this).val());
			});
		}



		if (arr.length == 0) {
			alert('Silahkan Pilih Data ! ');
			return false;
		} else {
			xhrAjax(arr, act);
			if (act == 'ro') {
				document.getElementById("resendro").disabled = true;
			} else if (act == 'so') {
				document.getElementById("resendaso").disabled = true;
			} else if (act == 'sc') {
				document.getElementById("resendasc").disabled = true;
			} else if (act == 'scpayment') {
				document.getElementById("resendascpayment").disabled = true;
			} else if (act == 'scretur') {
				document.getElementById("resendascretur").disabled = true;
			} else if (act == 'roretur') {
				document.getElementById("resendroretur").disabled = true;
			} else if (act == 'scadmin') {
				document.getElementById("resendscadmin").disabled = true;
			} else if (act == 'scadminretur') {
				document.getElementById("resendscadminretur").disabled = true;
			} else if (act == 'cancelbilling') {
				document.getElementById("resendcancelbilling").disabled = true;
			}
		}

	}
</script>