<?php $this->load->view('header'); ?>
<h2><?php echo $page_title; ?></h2>

<link rel="stylesheet" type="text/css" href="<?= site_url()?>assets/css/jquery.dataTables.css " >
<link rel="stylesheet" href="<?=site_url()?>assets/css/bootstrap.min.css">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script src="<?=site_url()?>assets/js/jquery-1.9.1.js"></script>
<script type="text/javascript" charset="utf8" src="<?= site_url()?>assets/js/jquery.dataTables.js"> </script>
<script src="<?=site_url()?>assets/js/bootstrap.min.js"></script>
<script>
$(document).ready(function() {
  var dateToday = new Date();
  $(".from").datepicker({
			defaultDate: "+1h",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1,
			onClose: function(selectedDate) {
				$(".to").datepicker("option", "minDate", selectedDate);
			}
		});
		$(".to").datepicker({
			defaultDate: "+1w",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1,
			onClose: function(selectedDate) {
				$(".from").datepicker("option", "maxDate", selectedDate);
			}
		});
})
</script>

<style>

.container {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}
@media (min-width: 768px) {
  .container {
    width: 750px;
  }
}
@media (min-width: 992px) {
  .container {
    width: 970px;
  }
}
@media (min-width: 1200px) {
  .container {
    width: 1170px;
  }
}
.container-fluid {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}
.nav {
  padding-left: 0;
  margin-bottom: 0;
  list-style: none;
}
.nav > li {
  position: relative;
  display: block;
}
.nav > li > a {
  position: relative;
  display: block;
  padding: 10px 15px;
}
.nav > li > a:hover,
.nav > li > a:focus {
  text-decoration: none;
  background-color: #eeeeee;
}
.nav > li.disabled > a {
  color: #777777;
}
.nav > li.disabled > a:hover,
.nav > li.disabled > a:focus {
  color: #777777;
  text-decoration: none;
  cursor: not-allowed;
  background-color: transparent;
}
.nav .open > a,
.nav .open > a:hover,
.nav .open > a:focus {
  background-color: #eeeeee;
  border-color: #337ab7;
}
.nav .nav-divider {
  height: 1px;
  margin: 9px 0;
  overflow: hidden;
  background-color: #e5e5e5;
}
.nav > li > a > img {
  max-width: none;
}
.nav-tabs {
  border-bottom: 1px solid #ddd;
}
.nav-tabs > li {
  float: left;
  margin-bottom: -1px;
}
.nav-tabs > li > a {
  margin-right: 2px;
  line-height: 1.42857143;
  border: 1px solid transparent;
  border-radius: 4px 4px 0 0;
}
.nav-tabs > li > a:hover {
  border-color: #eeeeee #eeeeee #ddd;
}
.nav-tabs > li.active > a,
.nav-tabs > li.active > a:hover,
.nav-tabs > li.active > a:focus {
  color: #555555;
  cursor: default;
  background-color: #fff;
  border: 1px solid #ddd;
  border-bottom-color: transparent;
}
.nav-tabs.nav-justified {
  width: 100%;
  border-bottom: 0;
}
.modal-lg{
  width: 1300px !important;
}
.nav-tabs.nav-justified > li {
  float: none;
}
.nav-tabs.nav-justified > li > a {
  margin-bottom: 5px;
  text-align: center;
}
.nav-tabs.nav-justified > .dropdown .dropdown-menu {
  top: auto;
  left: auto;
}
@media (min-width: 768px) {
  .nav-tabs.nav-justified > li {
    display: table-cell;
    width: 1%;
  }
  .nav-tabs.nav-justified > li > a {
    margin-bottom: 0;
  }
}
.nav-tabs.nav-justified > li > a {
  margin-right: 0;
  border-radius: 4px;
}
.nav-tabs.nav-justified > .active > a,
.nav-tabs.nav-justified > .active > a:hover,
.nav-tabs.nav-justified > .active > a:focus {
  border: 1px solid #ddd;
}
@media (min-width: 768px) {
  .nav-tabs.nav-justified > li > a {
    border-bottom: 1px solid #ddd;
    border-radius: 4px 4px 0 0;
  }
  .nav-tabs.nav-justified > .active > a,
  .nav-tabs.nav-justified > .active > a:hover,
  .nav-tabs.nav-justified > .active > a:focus {
    border-bottom-color: #fff;
  }
}
.nav-pills > li {
  float: left;
}
.nav-pills > li > a {
  border-radius: 4px;
}
.nav-pills > li + li {
  margin-left: 2px;
}
.nav-pills > li.active > a,
.nav-pills > li.active > a:hover,
.nav-pills > li.active > a:focus {
  color: #fff;
  background-color: #337ab7;
}
.nav-stacked > li {
  float: none;
}
.nav-stacked > li + li {
  margin-top: 2px;
  margin-left: 0;
}
.nav-justified {
  width: 100%;
}
.nav-justified > li {
  float: none;
}
.nav-justified > li > a {
  margin-bottom: 5px;
  text-align: center;
}
.nav-justified > .dropdown .dropdown-menu {
  top: auto;
  left: auto;
}
@media (min-width: 768px) {
  .nav-justified > li {
    display: table-cell;
    width: 1%;
  }
  .nav-justified > li > a {
    margin-bottom: 0;
  }
}
.nav-tabs-justified {
  border-bottom: 0;
}
.nav-tabs-justified > li > a {
  margin-right: 0;
  border-radius: 4px;
}
.nav-tabs-justified > .active > a,
.nav-tabs-justified > .active > a:hover,
.nav-tabs-justified > .active > a:focus {
  border: 1px solid #ddd;
}
.tab-content > .tab-pane {
  display: none;
}
.tab-content > .active {
  display: block;
}
.tab-content > .tab-pane {
  display: none;
}
.tab-content > .active {
  display: block;
}

.Stand {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

.Stand td, .Stand th {
  border: 1px solid #ddd;
  padding: 8px;
  color:black;
}

.Stand tr:nth-child(even){background-color: #f2f2f2;}

.Stand tr:hover {background-color: #ddd;}

.Stand th {
  padding-top: 12px;
  padding-bottom: 12px;
  font-size: 13px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>

<hr>
<div class="ViewHold">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">E-Wallet Stockiest</a></li>
    <li class=""><a data-toggle="tab" href="#member">E-Wallet Member</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
    <br>

    <div class="">

    <div class="row">
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Status :</label>
            <select class="form-control" name="" id="status_stc">
            <option value="all">All</option>
            <option value="E">Error</option>
            <option value="S">Success</option>
            </select>
            </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD"  name="datefrom" id="from_stc" >
    </div>
    </div>


    <div class="col-md-2">

    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="to_stc">
      </div>
    </div>
   

<div class="col-md-3">
  <br>
<button onclick="filter('stc');" style="margin-right:10px" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
<button onclick="getval('stc');" style="margin-right:5px" id="resendstc" class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
<button onclick="ewallet();" id="reload"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>

</div>
   </div>   

    </div>
    <br>
    <br>
      <table class="Stand" id="whsfrom">
        <thead style="color:black"> 
          <tr>
          <th width="3%"><input name="select_all"  id="example-select-all" type="checkbox"></th>
            <th>Header ID</th>
            <th>Header Date</th>
            <th>Status</th>
            <th width="10%">Action</th>
          </tr>
        </thead>
      </table>
</div>

<div id="member" class="tab-pane fade">
     <br>
    <div>
    <div class="row">
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Status :</label>
            <select class="form-control" name="" id="status_mem">
            <option value="all">All</option>
            <option value="E">Error</option>
            <option value="S">Success</option>
            </select>
            </div>
    </div>
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" autocomplete="off" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from_mem" >
    </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="to_mem" >
      </div>
    </div>
   

<div class="col-md-3">
  <br>
<button onclick="filter('mem');" style="margin-right:10px" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
<button style="margin-right:5px" onclick="getval('mem');" id="resendmem"  class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
<button onclick="sloc();" id="reload"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
</div>
   </div>   
 </div>
   <br>
     <table cellspacing="0" width="100%" class="Stand" id="sloc">
       <thead style="color:black"> 
         <tr>
         <th width="3%"><input type="checkbox" name="select_all_sloc"  id="example-select-all-sloc"></th>
           <th>Header ID</th>
           <th >Header Date</th>
           <th>Status</th>
           <th width="10%">Action</th>
         </tr>
       </thead>
     </table>
     </div>
   </div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button  style="color:black !important;" type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 style="color:black" class="modal-title">Detail E-Wallet </h3><h4 style="color:black" id="text"></h4>
      </div>
      <div class="modal-body">
      <div class="table-responsive">
      <table id="csss" class="table table-bordered table-striped">
      <thead style="color:black"> 
        <tr>
          <th>Doc Head</th>
          <th>Doc Det</th>
          <th>Cat Cust</th>
          <th>Doc Date</th>
          <th>Doc Post</th>
          <th>Kunnr</th>
          <th>Name Cust</th>
          <th>DepositID</th>
          <th>WithdrawalID</th>
          <th>Cat Doc</th>
          <th>Werks</th>
          <th>Bank</th>
          <th>Pay Meth</th>
          <th>Cre Deb</th>
          <th>PPH 21</th>
          <th>Dmbtr Cor</th>
          <th>Dmbtr</th>
          <th>Remark</th>
          <th>CPUDT</th>
          <th>CPUTM</th>
          <th>STATUS</th>
          <th>DESCRIPTION</th>
        </tr>
      </thead>
     <tbody style="color:black"></tbody>
    </table>
      </div>
      
      </div>
      <div class="modal-footer">
        <button onclick="resetT()" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<?php
$this->load->view('footer');
?>

<script>
  $(document).ready(function() {
    ewallet();
    sloc();
});


function resetT()
{
  if ($.fn.dataTable.isDataTable('#csss')) {
    var table = $("#csss").DataTable();
    table.destroy();
  }

}


function ewallet(act = 'txt'){
  if ($.fn.dataTable.isDataTable('#whsfrom')) {
    var table = $("#whsfrom").DataTable();
    table.destroy();
  }

  if (act == 'running') {
      var ct = 'stc';
      var status = $('#status_'+ct).val();
      var from  = $('#from_'+ct).val();
      var to = $('#to_'+ct).val();    
     
    }else {
      var status = '';
      var from  = '';
      var to = '';
    }
    var table = $('#whsfrom').DataTable({
      'ajax': {
            "url": "<?= base_url(); ?>api/Wallet/get_data_wallet/STOCKIST",
            "type": "POST",
            "data": {"status" : status,"from":from,"to":to} 
        },
      'language': {
            'processing': 'Silahkan Tunggu !'
          },
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'render': function (data, type, full, meta){
             return '<input onclick=get("'+full[2]+'","#whsfrom") type="checkbox" name="code" class="'+full[2]+'" value="' 
                + $('<div/>').text(data).html() + '">';
         }
      }],
      'order': [2, 'desc']
   });

$('#example-select-all').on('click', function(){
   var rows = table.rows({ 'search': 'applied' }).nodes();
   $('input[type="checkbox"]', rows).prop('checked', this.checked);
});

}
function parseToInt(value) {
		var res = value.replace(/-/g, '');
		return parseInt(res);
	}


function sloc(act = 'txt'){
    if ($.fn.dataTable.isDataTable('#sloc')) {
    var table = $("#sloc").DataTable();
    table.destroy();
    }

  if (act == 'running') {
      var ct = 'mem';
      var status = $('#status_'+ct).val();
      var from  = $('#from_'+ct).val();
      var to = $('#to_'+ct).val();    
     
    }else {
      var status = '';
      var from  = '';
      var to = '';
    }

    var table = $('#sloc').DataTable({
      'ajax': {
            "url": "<?= base_url(); ?>api/Wallet/get_data_wallet/MEMBER",
            "type": "POST",
            "data": {"status" : status,"from":from,"to":to} 
        }, 
      'language': {
            'processing': 'Silahkan Tunggu !'
          },
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'render': function (data, type, full, meta){
             return '<input onclick=get("'+full[2]+'","#sloc") type="checkbox" name="code" class="'+full[2]+'" value="' 
                + $('<div/>').text(data).html() + '">';
         }
      }],
      'order': [2, 'desc']
   });

$('#example-select-all-sloc').on('click', function(){
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });
}


function filter(act){
  var status = $('#status_'+act).val();
  var from  = $('#from_'+act).val();
  var to = $('#to_'+act).val();
    if (to == '' || from == '') {
      alert('Silahkan Pilih Tanggal !');
      return false;
    } else {
      var from  = parseToInt($('#from_'+act).val());
      var to = parseToInt($('#to_'+act).val());
      if (to < from) {
            alert("Date To harus lebih dari Date From !");
            return false;
          } else {
            if (act == 'stc') {
              ewallet('running');
            }else if(act == 'mem')
            {
              sloc('running');
            }     
          }
      }
}


function get(tgl,table){
  var table = $(table).DataTable();
  $("."+tgl).change(function(){
    var rows = table.rows({ 'search': 'applied' }).nodes();
    $('.'+tgl, rows).prop('checked', this.checked);
  });
}
function Jejak(id,act)
{
  $('#myModal').modal({
        show: 'true'
    }); 
    if ($.fn.dataTable.isDataTable('#csss')) {
    var table = $("#csss").DataTable();
    table.destroy();
    }
    $('#text').text('E-wallet' + ' - '+ act +'ID : '+ id );
        var table = $('#csss').DataTable({ 
           'ajax': {
                "url": "<?= base_url(); ?>api/Wallet/get_data_user",
                "type": "POST",
                "data": {"id" : id,"act":act} 
            },
        'language': {
              'processing': 'Silahkan Tunggu !'
            },
        'columnDefs': [{
          //'targets': 0,
          'searchable':false,
          'targets': [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18], 
          'orderable':false 
        }]
    });
       
}

function xhrAjax(data,url){

  if (url == 'stc') {
    var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/EWallet/ajax_post_ewallet_stc";
  }else if(url == 'mem') {
    var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/EWallet/ajax_post_ewallet_mem";
  }


  $.ajax({
			url:curl,
			method: 'POST',
			dataType:"json",
      data: {
				'data': data
			},
			success: function(datza) {
        alert(datza);
        if (url == 'stc') {
          ewallet();
          document.getElementById("resendstc").disabled = false;
          document.getElementById("example-select-all").checked = false;
        }else if(url == 'mem') {
          sloc();
          document.getElementById("resendmem").disabled = false;
          document.getElementById("example-select-all-sloc").checked = false;
        }
      },
      error: function(){
        alert('Gagal Resend');
        if (url == 'stc') {
          ewallet();
          document.getElementById("resendstc").disabled = false;
          document.getElementById("example-select-all").checked = false;
        }else if(url == 'mem') {
          sloc();
          document.getElementById("resendmem").disabled = false;
          document.getElementById("example-select-all-sloc").checked = false;
        }
      }
		});
}

function getval(act) {
var arr = [];

    if (act == 'stc') {  
      let table=$('#whsfrom').DataTable();
      let checkedvalues = table.$('input:checked').each(function () {
      arr.push($(this).val());
    });
    }else if(act == 'mem') {
      let table=$('#sloc').DataTable();
      let checkedvalues = table.$('input:checked').each(function () {
      arr.push($(this).val());
    });
    }


    if (arr.length == 0) {
      alert('Silahkan Pilih Data ! ');
      return false;
    }else {
      xhrAjax(arr,act);
      if (act == 'stc') {  
          document.getElementById("resendstc").disabled = true;
        }else if(act == 'mem') {
          document.getElementById("resendmem").disabled = true;
        }
    }

}
</script>