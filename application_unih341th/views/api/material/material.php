<?php $this->load->view('header'); ?>
<h2><?php echo $page_title; ?></h2>
<link rel="stylesheet" type="text/css" href="<?= site_url()?>assets/css/jquery.dataTables.css " >
<link rel="stylesheet" href="<?=site_url()?>assets/css/bootstrap.min.css">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script src="<?=site_url()?>assets/js/jquery-1.9.1.js"></script>
<script type="text/javascript" charset="utf8" src="<?= site_url()?>assets/js/jquery.dataTables.js"> </script>
<script src="<?=site_url()?>assets/js/bootstrap.min.js"></script>
<script>
$(document).ready(function() {
  var dateToday = new Date();
  $(".from").datepicker({
			defaultDate: "+1h",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1
		});
		$(".to").datepicker({
			defaultDate: "+1w",
			dateFormat: 'yyyy-mm-dd',
			changeMonth: true,
			numberOfMonths: 1
		});
})
</script>

<style>

.container {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}
.modal-lg{
  width: 1300px !important;
}
@media (min-width: 768px) {
  .container {
    width: 750px;
  }
}
@media (min-width: 992px) {
  .container {
    width: 970px;
  }
}
@media (min-width: 1200px) {
  .container {
    width: 1000px;
  }
}
.container-fluid {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}
.nav {
  padding-left: 0;
  margin-bottom: 0;
  list-style: none;
}
.nav > li {
  position: relative;
  display: block;
}
.nav > li > a {
  position: relative;
  display: block;
  padding: 10px 15px;
}
.nav > li > a:hover,
.nav > li > a:focus {
  text-decoration: none;
  background-color: #eeeeee;
}
.nav > li.disabled > a {
  color: #777777;
}
.nav > li.disabled > a:hover,
.nav > li.disabled > a:focus {
  color: #777777;
  text-decoration: none;
  cursor: not-allowed;
  background-color: transparent;
}
.nav .open > a,
.nav .open > a:hover,
.nav .open > a:focus {
  background-color: #eeeeee;
  border-color: #337ab7;
}
.nav .nav-divider {
  height: 1px;
  margin: 9px 0;
  overflow: hidden;
  background-color: #e5e5e5;
}
.nav > li > a > img {
  max-width: none;
}
.nav-tabs {
  border-bottom: 1px solid #ddd;
}
.nav-tabs > li {
  float: left;
  margin-bottom: -1px;
}
.nav-tabs > li > a {
  margin-right: 2px;
  line-height: 1.42857143;
  border: 1px solid transparent;
  border-radius: 4px 4px 0 0;
}
.nav-tabs > li > a:hover {
  border-color: #eeeeee #eeeeee #ddd;
}
.nav-tabs > li.active > a,
.nav-tabs > li.active > a:hover,
.nav-tabs > li.active > a:focus {
  color: #555555;
  cursor: default;
  background-color: #fff;
  border: 1px solid #ddd;
  border-bottom-color: transparent;
}
.nav-tabs.nav-justified {
  width: 100%;
  border-bottom: 0;
}
.nav-tabs.nav-justified > li {
  float: none;
}
.nav-tabs.nav-justified > li > a {
  margin-bottom: 5px;
  text-align: center;
}
.nav-tabs.nav-justified > .dropdown .dropdown-menu {
  top: auto;
  left: auto;
}
@media (min-width: 768px) {
  .nav-tabs.nav-justified > li {
    display: table-cell;
    width: 1%;
  }
  .nav-tabs.nav-justified > li > a {
    margin-bottom: 0;
  }
}
.nav-tabs.nav-justified > li > a {
  margin-right: 0;
  border-radius: 4px;
}
.nav-tabs.nav-justified > .active > a,
.nav-tabs.nav-justified > .active > a:hover,
.nav-tabs.nav-justified > .active > a:focus {
  border: 1px solid #ddd;
}
@media (min-width: 768px) {
  .nav-tabs.nav-justified > li > a {
    border-bottom: 1px solid #ddd;
    border-radius: 4px 4px 0 0;
  }
  .nav-tabs.nav-justified > .active > a,
  .nav-tabs.nav-justified > .active > a:hover,
  .nav-tabs.nav-justified > .active > a:focus {
    border-bottom-color: #fff;
  }
}
.nav-pills > li {
  float: left;
}
.nav-pills > li > a {
  border-radius: 4px;
}
.nav-pills > li + li {
  margin-left: 2px;
}
.nav-pills > li.active > a,
.nav-pills > li.active > a:hover,
.nav-pills > li.active > a:focus {
  color: #fff;
  background-color: #337ab7;
}
.nav-stacked > li {
  float: none;
}
.nav-stacked > li + li {
  margin-top: 2px;
  margin-left: 0;
}
.nav-justified {
  width: 100%;
}
.nav-justified > li {
  float: none;
}
.nav-justified > li > a {
  margin-bottom: 5px;
  text-align: center;
}
.nav-justified > .dropdown .dropdown-menu {
  top: auto;
  left: auto;
}
@media (min-width: 768px) {
  .nav-justified > li {
    display: table-cell;
    width: 1%;
  }
  .nav-justified > li > a {
    margin-bottom: 0;
  }
}
.nav-tabs-justified {
  border-bottom: 0;
}
.nav-tabs-justified > li > a {
  margin-right: 0;
  border-radius: 4px;
}
.nav-tabs-justified > .active > a,
.nav-tabs-justified > .active > a:hover,
.nav-tabs-justified > .active > a:focus {
  border: 1px solid #ddd;
}
.tab-content > .tab-pane {
  display: none;
}
.tab-content > .active {
  display: block;
}
.tab-content > .tab-pane {
  display: none;
}
.tab-content > .active {
  display: block;
}

.Stand {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

.Stand td, .Stand th {
  border: 1px solid #ddd;
  padding: 8px;
  color: black;
}

.Stand tr:nth-child(even){background-color: #f2f2f2;}
.Stand tr:hover {background-color: #ddd;}
.Stand th {
  padding-top: 12px;
  padding-bottom: 12px;
  font-size: 13px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
<hr>
<div class="ViewHold">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">NCM</a></li>
    <li><a data-toggle="tab" href="#menu1">Adjustment Rusak</a></li>
    <li><a data-toggle="tab" href="#menu2">Adjustment Move</a></li>
    <li><a data-toggle="tab" href="#menu3">Warehouse From</a></li>
    <li><a data-toggle="tab" href="#menu4">Warehouse To</a></li>
    <li><a data-toggle="tab" href="#menu5">Sloc</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
   <br>
   <div>
   <div class="row">
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Status :</label>
            <select class="form-control" name="" id="status_ncm">
            <option value="all">All</option>
            <option value="E">Error</option>
			 <option value="W">Warning</option>
            <option value="S">Success</option>
            </select>
            </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" id="from_ncm" autocomplete="off" data-language="en" placeholder="YYYY-MM-DD" name="datefrom" id="to" >
    </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" id="to_ncm" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from" >
      </div>
    </div>
   

<div class="col-md-3">
  <br>
<button style="margin-right:10px" id="filter" onclick="filter('ncm');" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
<button  style="margin-right:6px" onclick="getval('ncm');" id="resend"  class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
<button onclick="ncm();" id="reload"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
</div>
   </div>         
</div>
  <br>
    <table cellspacing="0" width="100%" class="Stand display select" id="myTable">
      <thead style="color:black"> 
        <tr>
          <th width="3%"><input type="checkbox" name="select_all"  id="example-select-all"></th>
          <th width="14%">NCM ID</th>
          <th width="40px">Transaction Date</th>
          <th width="50px">Status Response</th>
          <th width="10%">Action</th>
        </tr>
      </thead>
    </table>
    </div>
    <div id="menu1" class="tab-pane fade">
    <br>
   <div>
   <div class="row">
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Status :</label>
            <select class="form-control" name="" id="status_adjrusak">
            <option value="all">All</option>
            <option value="E">Error</option>
			 <option value="W">Warning</option>
            <option value="S">Success</option>
            </select>
            </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" autocomplete="off" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from_adjrusak" >
    </div>
    </div>
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="to_adjrusak">
      </div>
    </div>
   

<div class="col-md-3">
  <br>
<button style="margin-right:10px" onclick="filter('adjrusak');" class="btn btn-sm btn-primary mr-3" id type="button"><u>F</u>ilter Data</button>
<button style="margin-right:5px" onclick="getval('adjrusak');" id="resendrsk"  class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
<button onclick="adjRsk();" id="reload"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
</div>
   </div>    
  
</div>
  <br>
    <table cellspacing="0" width="100%" class="Stand" id="adjRsk">
      <thead style="color:black"> 
        <tr>
        <th style="width: 10px;"><input type="checkbox" name="select_all"  id="example-select-all-adjrsk"></th>
          <th>Adjustment Rusak ID</th>
          <th>Transaction Date</th>
          <th>Status Response</th>
          <th width="10%">Action</th>
        </tr>
      </thead>
    
    </table>
    </div>
    <div id="menu2" class="tab-pane fade">
    <br>
   <div>

   <div class="row">
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Status :</label>
            <select class="form-control" name="" id="status_adjmove">
            <option value="all">All</option>
            <option value="E">Error</option>
			 <option value="W">Warning</option>
            <option value="S">Success</option>
            </select>
            </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" autocomplete="off" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from_adjmove">
    </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="to_adjmove">
      </div>
    </div>
   

<div class="col-md-3">
  <br>
<button style="margin-right:10px" onclick="filter('adjmove');" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
<button style="margin-right:5px" onclick="getval('adjmove');" id="resendmove"  class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
<button onclick="adjMove();" id="reload" class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
</div>
</div>  
</div>
  <br>
    <table cellspacing="0" width="100%" class="Stand" id="adjMove">
      <thead style="color:black"> 
        <tr>
        <th width="3%"><input type="checkbox" name="select_all"  id="example-select-all-adjmove"></th>
          <th>Header ID</th>
          <th >Transaction Date</th>
          <th>Status Response</th>
          <th width="10%">Action</th>
        </tr>
      </thead>
    
    </table>
    </div>
    <div id="menu3" class="tab-pane fade">
    <br>
   <div>

   <div class="row">
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Status :</label>
            <select class="form-control" name="" id="status_whsfrom">
            <option value="all">All</option>
            <option value="E">Error</option>
			 <option value="W">Warning</option>
            <option value="S">Success</option>
            </select>
            </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" autocomplete="off" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from_whsfrom"  >
    </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="to_whsfrom" >
      </div>
    </div>
   

<div class="col-md-3">
  <br>
<button style="margin-right:10px" onclick="filter('whsfrom');" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
<button style="margin-right:5px" onclick="getval('whsfrom');" id="resendfrom"  class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
<button onclick="whsfrom();" id="reload" class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
</div>


   </div>         
  
</div>
  <br>
    <table cellspacing="0" width="100%" class="Stand" id="whsfrom">
      <thead style="color:black"> 
        <tr>
        <th width="3%"><input type="checkbox" name="select_all"  id="example-select-all-whsfrom"></th>
          <th>Header ID</th>
          <th >Transaction Date</th>
          <th>Status Response</th>
          <th width="10%">Action</th>
        </tr>
      </thead>
    
    </table>


    </div>

    <div id="menu4" class="tab-pane fade">
    <br>
   <div>
   <div class="row">
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Status :</label>
            <select class="form-control" name="" id="status_whsto">
            <option value="all">All</option>
            <option value="E">Error</option>
			 <option value="W">Warning</option>
            <option value="S">Success</option>
            </select>
            </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" autocomplete="off" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from_whsto"  >
    </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="to_whsto">
      </div>
    </div>
   

<div class="col-md-3">
  <br>
<button style="margin-right:10px" onclick="filter('whsto');" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
<button style="margin-right:5px" onclick="getval('whsto');" id="resendto"  class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
<button onclick="whsto();" id="reload" class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
</div>


   </div>
  
</div>
  <br>
    <table cellspacing="0" width="100%" class="Stand" id="whsto">
      <thead style="color:black"> 
        <tr>
          <th width="3%"><input type="checkbox" name="select_all"  id="example-select-all-whsto"></th>
          <th>Header ID</th>
          <th >Transaction Date</th>
          <th>Status Response</th>
          <th width="10%">Action</th>
        </tr>
      </thead>
    </table>
    </div>
    <div id="menu5" class="tab-pane fade">
    <br>
   <div>

   <div class="row">
    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Status :</label>
            <select class="form-control" name="" id="status_sloc">
            <option value="all">All</option>
            <option value="E">Error</option>
			 <option value="W">Warning</option>
            <option value="S">Success</option>
            </select>
            </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date From :</label>
    <input type="text" class="form-control from" autocomplete="off" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="from_sloc" >
    </div>
    </div>


    <div class="col-md-2">
    <div style="color:black;font-size:11px" class="form-group">
    <label for="">Date To :</label>
    <input type="text" class="form-control to" autocomplete="off" data-language="en"  placeholder="YYYY-MM-DD" name="datefrom" id="to_sloc" >
      </div>
    </div>
   

<div class="col-md-3">
  <br>
<button style="margin-right:10px" onclick="filter('sloc');" class="btn btn-sm btn-primary mr-3" type="button"><u>F</u>ilter Data</button>
<button style="margin-right:5px" onclick="getval('sloc');" id="resendsloc"  class="btn btn-sm btn-primary" type="button"><u>R</u>esend Data</button>
<button onclick="sloc();" id="reload" class="btn btn-sm btn-primary" type="button"><i class="fa fa-rotate-right"></i></button>
</div>
   </div>    
</div>
  <br>
    <table cellspacing="0" width="100%" class="Stand" id="sloc">
      <thead style="color:black"> 
        <tr>
        <th width="3%"><input type="checkbox" name="select_all_sloc"  id="example-select-all-sloc"></th>
          <th>Header ID</th>
          <th >Transaction Date</th>
          <th>Status Response</th>
          <th width="10%">Action</th>
        </tr>
      </thead>
    </table>
    </div>
  </div>

</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button style="color:black !important;" type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 style="color:black" class="modal-title">Detail E-Material </h3><h4 style="color:black" id="text"></h4>
      </div>
      <div class="modal-body">
      <div class="table-responsive">
      <table id="csss" class="table table-bordered table-striped">
      <thead> 
        <tr>
          <th>EKGRP</th>
          <th>YMBLNR </th>
          <th>YID </th>
          <th>YMJAHR</th>
          <th>BWART</th>
          <th>WERKS</th>
          <th>LGORT</th>
          <th>MATNR</th>
          <th>ERFMG</th>
          <th>ERFME</th>
          <th>CHARG</th>
          <th>VFDAT</th>
          <th>KOSTL</th>
          <th>PRCTR</th>
          <th>BUDAT</th>
          <th>CPUTM</th>
          <th>USNAM</th>
          <th>YRETURN</th>
          <th>WAERS</th>
          <th>SAKTO</th>
          <th>DMBTR</th>
          <th>YZEILE</th>
          <th>STATUS</th>
          <th>DESCRIPTION</th>
        </tr>
      </thead>
     
    </table>
      </div>
      
      </div>
      <div class="modal-footer">
        <button onclick="resetT()" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php
$this->load->view('footer');
?>
<script>
function ncm(act = 'txt'){
    if ($.fn.dataTable.isDataTable('#myTable')) {
    var table = $("#myTable").DataTable();
    table.destroy();
    }
    if (act == 'running') {
      var ct = 'ncm';
      var status = $('#status_'+ct).val();
      var from  = $('#from_'+ct).val();
      var to = $('#to_'+ct).val();    
    }else {
      var status = '';
      var from  = '';
      var to = '';
    }
    var table = $('#myTable').DataTable({
       'ajax': {
            "url": "<?= base_url(); ?>api/Material/get_data_ncm",
            "type": "POST",
            "data": {"status" : status,"from":from,"to":to} 
          },
      'language': {
            'processing': 'Silahkan Tunggu !'
          },
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'render': function (data, type, full, meta){
             return '<input onclick=get("'+full[2]+'","#myTable") type="checkbox" name="code" class="'+full[2]+'" value="' 
                + $('<div/>').text(data).html() + '">';
         }
      }],
      'order': [2, 'desc']
   });

    $('#example-select-all').on('click', function(){
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });
    document.getElementById("filter").disabled = false;

  }


function get(tgl,table){
  var table = $(table).DataTable();
  $("."+tgl).change(function(){
    var rows = table.rows({ 'search': 'applied' }).nodes();
    $('.'+tgl, rows).prop('checked', this.checked);
  });
}

function adjRsk(act = 'txt'){
    if ($.fn.dataTable.isDataTable('#adjRsk')) {
    var table = $("#adjRsk").DataTable();
    table.destroy();
    }
    if (act == 'running') {
      var ct = 'adjrusak';
      var status = $('#status_'+ct).val();
      var from  = $('#from_'+ct).val();
      var to = $('#to_'+ct).val();    
     
    }else {
      var status = '';
      var from  = '';
      var to = '';
    }
    var table = $('#adjRsk').DataTable({
      'ajax': {
            "url": "<?= base_url(); ?>api/Material/get_data_adjrsk",
            "type": "POST",
            "data": {"status" : status,"from":from,"to":to} 
          },
      'language': {
            'processing': 'Silahkan Tunggu !'
          },
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         
         'render': function (data, type, full, meta){
             return '<input onclick=get("'+full[2]+'","#adjRsk") type="checkbox" name="code" class="'+full[2]+'" value="' 
                + $('<div/>').text(data).html() + '">';
         }
      }],
      'order': [2, 'desc']
   });
    $('#example-select-all-adjrsk').on('click', function(){
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

  }


  function adjMove(act = 'txt'){
    if ($.fn.dataTable.isDataTable('#adjMove')) {
    var table = $("#adjMove").DataTable();
    table.destroy();
    }
    if (act == 'running') {
      var ct = 'adjmove';
      var status = $('#status_'+ct).val();
      var from  = $('#from_'+ct).val();
      var to = $('#to_'+ct).val();    
     
    }else {
      var status = '';
      var from  = '';
      var to = '';
    }
    var table = $('#adjMove').DataTable({
      'ajax': {
            "url": "<?= base_url(); ?>api/Material/get_data_adjmove",
            "type": "POST",
            "data": {"status" : status,"from":from,"to":to} 
          },
      'language': {
            'processing': 'Silahkan Tunggu !'
          },
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'render': function (data, type, full, meta){
             return '<input onclick=get("'+full[2]+'","#adjMove") type="checkbox" name="code" class="'+full[2]+'" value="' 
                + $('<div/>').text(data).html() + '">';
         }
      }],
      'order': [2, 'desc']
   });
    $('#example-select-all-adjmove').on('click', function(){
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

  }


  function whsfrom(act = 'txt'){
    if ($.fn.dataTable.isDataTable('#whsfrom')) {
    var table = $("#whsfrom").DataTable();
    table.destroy();
    }
    if (act == 'running') {
      var ct = 'whsfrom';
      var status = $('#status_'+ct).val();
      var from  = $('#from_'+ct).val();
      var to = $('#to_'+ct).val();    
     
    }else {
      var status = '';
      var from  = '';
      var to = '';
    }
    var table = $('#whsfrom').DataTable({
      'ajax': {
            "url": "<?= base_url(); ?>api/Material/get_data_whsfrom",
            "type": "POST",
            "data": {"status" : status,"from":from,"to":to} 
          }, 
      'language': {
            'processing': 'Silahkan Tunggu !'
          },
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'render': function (data, type, full, meta){
             return '<input onclick=get("'+full[2]+'","#whsfrom") type="checkbox" name="code" class="'+full[2]+'" value="' 
                + $('<div/>').text(data).html() + '">';
         }
      }],
      'order': [2, 'desc']
   });
    $('#example-select-all-whsfrom').on('click', function(){
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

  }


  function whsto(act = 'txt'){
    if ($.fn.dataTable.isDataTable('#whsto')) {
    var table = $("#whsto").DataTable();
    table.destroy();
    }
     if (act == 'running') {
      var ct = 'whsto';
      var status = $('#status_'+ct).val();
      var from  = $('#from_'+ct).val();
      var to = $('#to_'+ct).val();    
     
    }else {
      var status = '';
      var from  = '';
      var to = '';
    }
    var table = $('#whsto').DataTable({
      'ajax': {
            "url": "<?= base_url(); ?>api/Material/get_data_whsto",
            "type": "POST",
            "data": {"status" : status,"from":from,"to":to} 
          }, 
      'language': {
            'processing': 'Silahkan Tunggu !'
          },
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'render': function (data, type, full, meta){
             return '<input onclick=get("'+full[2]+'","#whsto") type="checkbox" name="code" class="'+full[2]+'" value="' 
                + $('<div/>').text(data).html() + '">';
         }
      }],
      'order': [2, 'desc']
   });
    $('#example-select-all-whsto').on('click', function(){
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

  }


function sloc(act = 'txt'){
    if ($.fn.dataTable.isDataTable('#sloc')) {
    var table = $("#sloc").DataTable();
    table.destroy();
    }
    if (act == 'running') {
      var ct = 'sloc';
      var status = $('#status_'+ct).val();
      var from  = $('#from_'+ct).val();
      var to = $('#to_'+ct).val();    
     
    }else {
      var status = '';
      var from  = '';
      var to = '';
    }
    var table = $('#sloc').DataTable({
      'ajax': {
            "url": "<?= base_url(); ?>api/Material/get_data_sloc",
            "type": "POST",
            "data": {"status" : status,"from":from,"to":to} 
          }, 
      'language': {
            'processing': 'Silahkan Tunggu !'
          },
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'render': function (data, type, full, meta){
             return '<input onclick=get("'+full[2]+'","#sloc") type="checkbox" name="code" class="'+full[2]+'" value="' 
                + $('<div/>').text(data).html() + '">';
         }
      }],
      'order': [2, 'desc']
   });

$('#example-select-all-sloc').on('click', function(){
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });
}


$(document).ready(function() { 
    ncm();
    adjRsk();
    adjMove();
    whsfrom();
    whsto();
    sloc();
});
function resetT()
{
  if ($.fn.dataTable.isDataTable('#csss')) {
    var table = $("#csss").DataTable();
    table.destroy();
  }

}
function Jejak(id,act)
{
  $('#myModal').modal({
        show: 'true'
    }); 
    $('#text').text(act + ' ' +'ID : '+ id );
    if ($.fn.dataTable.isDataTable('#csss')) {
    var table = $("#csss").DataTable();
    table.destroy();
    }
    var table = $('#csss').DataTable({ 
           'ajax': {
                "url": "<?= base_url(); ?>api/Material/get_data_user",
                "type": "POST",
                "data": {"id" : id,"act":act} 
            },
        'language': {
              'processing': 'Silahkan Tunggu !'
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		
					if (aData[22] == 'ERROR SEND DATA') {
						$('td', nRow).css('background-color', '#ff872b', 'color', 'white');
					} else if (aData[22] == 'WARNING SEND DATA') {
						$('td', nRow).css('background-color', '#f2c911', 'color', 'white');
					} else {
						$('td', nRow).css('background-color', 'White');
					}
				},
        'columnDefs': [{
          //'targets': 0,
          'searchable':false,
          'orderable':false,
           'targets': [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21 ], 
        }]
    });
       
    
}






function xhrAjax(data,url){
  if (url == 'ncm') {
    var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/EMaterial/ajax_post_ncm";
  }else if(url == 'adjrusak') {
    var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/EMaterial/ajax_post_adj_rusak";
  }else if (url == 'adjmove') {
    var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/EMaterial/ajax_post_adj_move";
  }else if (url == 'whsfrom') {
    var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/EMaterial/ajax_post_whs_from";
  }else if (url == 'whsto') {
    var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/EMaterial/ajax_post_whs_to";
  }else {
    var curl = "http://<?php echo $_SERVER['HTTP_HOST'] . '/'; ?>sohomlm_push/EMaterial/ajax_post_sloc";
  }
     
  $.ajax({
			url:curl,
			method: 'POST',
			dataType:"json",
      data: {
				'data': data
			},
			success: function(datza) {
        alert(datza);
        if (url == 'ncm') {
          ncm();
          document.getElementById("resend").disabled = false;
          document.getElementById("example-select-all").checked = false;
        }else if(url == 'adjrusak') {
          adjRsk();
          document.getElementById("resendrsk").disabled = false;
          document.getElementById("example-select-all-adjrsk").checked = false;
        }else if (url == 'adjmove') {
          adjMove();
          document.getElementById("resendmove").disabled = false;
          document.getElementById("example-select-all-adjmove").checked = false;
        }else if (url == 'whsfrom') {
          whsfrom();
          document.getElementById("resendfrom").disabled = false;
          document.getElementById("example-select-all-whsfrom").checked = false;
        }else if (url == 'whsto') {
         whsto();
         document.getElementById("resendto").disabled = false;
         document.getElementById("example-select-all-whsto").checked = false;
        }else {
         sloc();
         document.getElementById("resendsloc").disabled = false;
         document.getElementById("example-select-all-sloc").checked = false;
        }
      },
      error: function(){
        alert('Gagal Resend');
        if (url == 'ncm') {
          ncm();
          document.getElementById("resend").disabled = false;
          document.getElementById("example-select-all").checked = false;
        }else if(url == 'adjrusak') {
          adjRsk();
          document.getElementById("resendrsk").disabled = false;
          document.getElementById("example-select-all-adjrsk").checked = false;
        }else if (url == 'adjmove') {
          adjMove();
          document.getElementById("resendmove").disabled = false;
          document.getElementById("example-select-all-adjmove").checked = false;
        }else if (url == 'whsfrom') {
          whsfrom();
          document.getElementById("resendfrom").disabled = false;
          document.getElementById("example-select-all-whsfrom").checked = false;
        }else if (url == 'whsto') {
         whsto();
         document.getElementById("resendto").disabled = false;
         document.getElementById("example-select-all-whsto").checked = false;
        }else {
         sloc();
         document.getElementById("resendsloc").disabled = false;
         document.getElementById("example-select-all-sloc").checked = false;
        }
      }
		});
}


function parseToInt(value) {
		var res = value.replace(/-/g, '');
		return parseInt(res);
	}

function filter(act){
  var status = $('#status_'+act).val();
  var from  = $('#from_'+act).val();
  var to = $('#to_'+act).val();
    if (to == '' || from == '') {
      alert('Silahkan Pilih Tanggal !');
      return false;
    } else {
  var from  = parseToInt($('#from_'+act).val());
  var to = parseToInt($('#to_'+act).val());
      if (to < from) {
            alert("Date To harus lebih dari Date From !");
            return false;
          } else {
            if (act == 'ncm') {
              ncm('running');
            }else if(act == 'adjrusak')
            {
              adjRsk('running');
            }else if(act == 'adjmove'){
              adjMove('running');
            }else if(act == 'whsfrom'){
              whsfrom('running')
            }else if(act == 'whsto'){
              whsto('running')
            }else if(act == 'sloc'){
              sloc('running')
            }       
          }
      }
}


function getval(act) {
    let arr= [];
    if (act == 'ncm') {  
      let table=$('#myTable').DataTable();
      let checkedvalues = table.$('input:checked').each(function () {
    arr.push($(this).val());
    });
    }else if(act == 'adjrusak') {
      let table=$('#adjRsk').DataTable();
      let checkedvalues = table.$('input:checked').each(function () {
    arr.push($(this).val());
    });
    }else if (act == 'adjmove') {
      let table=$('#adjMove').DataTable();
      let checkedvalues = table.$('input:checked').each(function () {
    arr.push($(this).val());
    });
    }else if (act == 'whsfrom') {
      let table=$('#whsfrom').DataTable();
      let checkedvalues = table.$('input:checked').each(function () {
    arr.push($(this).val());
    });
    }else if (act == 'whsto') {
      let table=$('#whsto').DataTable();
      let checkedvalues = table.$('input:checked').each(function () {
    arr.push($(this).val());
    });
    }else if (act == 'sloc'){
      let table=$('#sloc').DataTable();
      let checkedvalues = table.$('input:checked').each(function () {
    arr.push($(this).val());
    });
    }

 
 


 if (arr.length == 0) {
   alert('Silahkan Pilih Data ! ');
   return false;
 }else {
    xhrAjax(arr,act);
      if (act == 'ncm') {  
          document.getElementById("resend").disabled = true;
        }else if(act == 'adjrusak') {
          document.getElementById("resendrsk").disabled = true;
        }else if (act == 'adjmove') {
          document.getElementById("resendmove").disabled = true;
        }else if (act == 'whsfrom') {
          document.getElementById("resendfrom").disabled = true;
        }else if (act == 'whsto') {
         document.getElementById("resendto").disabled = true;
        }else {
         document.getElementById("resendsloc").disabled = true;
        }
 }
}
</script>