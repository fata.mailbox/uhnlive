<script type="text/javascript" src="/js/backend.js"></script>
<script language="JavaScript">
function pickup(){
	var ratio = 0;
	var jml = 0;
	
	if(document.form.pu.value==1){
		if(document.form.timur.value==0){
			ratio_ = 1;
		}else{
			ratio_ = 1.1;
		}
	}else{
		ratio_ = 1;
	}
	
<?php 
for($i=0;$i<10;$i++){ ?>
	if(ratio_ > 1){
		document.form.price<?php echo $i; ?>.value = formatCurrency(document.form.gprice2<?php echo $i; ?>.value);
	}else{
		document.form.price<?php echo $i; ?>.value = formatCurrency(document.form.gprice<?php echo $i; ?>.value);
	}
	jumlah(document.form.qty<?php echo $i; ?>,document.form.price<?php echo $i; ?>,document.form.subtotal<?php echo $i; ?>);
<?php 
} ?>
	document.form.total.value=total_curr(10,'document.form.subtotal');
	document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');
	
	diskon(document.form.total,document.form.persen,document.form.rpdiskon); 
	totalbayardiskon(document.form.total,document.form.rpdiskon,document.form.totalbayar);
}

</script>

<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('order/roadm/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		
		<table width='100%'>
		<?php if(validation_errors()){?>
		<tr>
			<td colspan='3'><span class="error"><?php echo form_error('total');?></span></td>
		</tr>
		<?php }?>
		<tr>
			<td width='20%'>date</td>
			<td width='1%'>:</td>
			<td width='79%'><?php echo date('Y-m-d');?></td> 
		</tr>
		
       <tr>
			<td valign='top'>Stockiest</td>
			<td valign='top'>:</td>
			<td valign='top'><?php 
				echo form_hidden('member_id',set_value('member_id','')); 
				$data = array('name'=>'no_stc','id'=>'no_stc','size'=>15,'readonly'=>'1','value'=>set_value('no_stc'));
				echo form_input($data);
				$atts = array(
					'width'      => '400',
					'height'     => '400',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'yes',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				echo anchor_popup('stc_ro/stcactive_ro/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
				?><span class='error'>*<?php echo form_error('member_id');?></span>
			</td>
		</tr>
		
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
        
        <tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php 
				$data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','tabindex'=>'1','value'=>set_value('remark'));
				echo form_textarea($data);?>
			</td> 
		</tr>
	</table>
		
	<table width='100%'>	
		<tr>
			<td width='18%'>Item Code</td>
			<td width='23%'>Item Name</td>
			<td width='8%'>Qty</td>
			<td width='12%'>Price</td>
			<td width='9%'>PV</td>
			<td width='16%'>Sub Total Price</td>
			<td width='10%'>Sub Total PV</td>
			<td width='5%'>Del?</td>
		</tr>
		
		<?php
		for($i=0;$i<10;$i++){ $idx = $i+9;
		?>
		<tr>
			<td valign='top'>
				<?php 
				$data = array('name'=>'itemcode'.$i,'id'=>'itemcode'.$i,'size'=>'8','readonly'=>'1','value'=>set_value('itemcode'.$i));
				echo form_input($data);
				$atts = array(
					'width'      => '600',
					'height'     => '500',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'no',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				echo anchor_popup('item_ro/index/'.$i, '<input class="button" type="button" name="Button" value="browse" tabindex="'.$idx.'"/>', $atts); 
				echo form_hidden('gprice'.$i,set_value('gprice'.$i,0));
				echo form_hidden('gprice2'.$i,set_value('gprice2'.$i,0));
				?>
				<td valign='top'><input type="text" name="itemname<?php echo $i; ?>" id="itemname<?php echo $i; ?>" value="<?php echo set_value('itemname'.$i);?>" readonly="1" size="24" /></td>
				<td>
					<input class='textbold aright' type="text" name="qty<?php echo $i; ?>" id="qty<?php echo $i; ?>" value="<?php echo set_value('qty'.$i,0);?>" maxlength="12" tabindex="17" size="3" autocomplete="off"
						onkeyup="this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.price<?php echo $i; ?>,document.form.subtotal<?php echo $i; ?>);
						jumlah(document.form.qty<?php echo $i; ?>,document.form.pv<?php echo $i; ?>,document.form.subtotalpv<?php echo $i; ?>);
						document.form.total.value=total_curr(10,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');
						diskon(document.form.total,document.form.persen,document.form.rpdiskon);
						totalbayardiskon(document.form.total,document.form.rpdiskon,document.form.totalbayar);
					">
				</td>
				<td><input class="aright" type="text" name="price<?php echo $i; ?>" id="price<?php echo $i; ?>" value="<?php echo set_value('price'.$i,0);?>" size="8" readonly="1"></td>
				<td><input class="aright" type="text" name="pv<?php echo $i; ?>" id="pv<?php echo $i; ?>" value="<?php echo set_value('pv'.$i,0);?>" size="5" readonly="1"></td>
				<td><input class="aright" type="text" name="subtotal<?php echo $i; ?>" id="subtotal<?php echo $i; ?>" value="<?php echo set_value('subtotal'.$i,0);?>" readonly="1" size="12"></td>
				<td><input class="aright" type="text" name="subtotalpv<?php echo $i; ?>" id="subtotalpv<?php echo $i; ?>" value="<?php echo set_value('subtotalpv'.$i,0);?>" readonly="1" size="10"></td>
			<td>
				<img alt="delete" 
				onclick="cleartext<?php echo $i; ?>(
						document.form.itemcode<?php echo $i; ?>
						,document.form.itemname<?php echo $i; ?>
						,document.form.qty<?php echo $i; ?>
						,document.form.price<?php echo $i; ?>
						,document.form.pv<?php echo $i; ?>
						,document.form.subtotal<?php echo $i; ?>
						,document.form.subtotalpv<?php echo $i; ?>);
					document.form.total.value=total_curr(10,'document.form.subtotal');
					document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');
				"
				src="<?php echo  base_url();?>images/backend/delete.png" border="0"/>
			</td>
		</tr>
		<?php
		}
		?>
		
		<tr>
			<td colspan='5'></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total',0);?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv',0);?>" readonly="1" size="9"></td>
		</tr>	
		<tr><td colspan="7"><hr/></td></tr>
		<tr>
			<td><b>Pick up / delivery</b></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td colspan="3"><b>Payment</b></td>
		</tr>
		<tr>
			<td valign="top">Option</td>
			<td valign="top">:
				<?php
					$options = array(
						'1'    => 'Delivery',
						'0'  => 'Pick Up'
					);
					$js = 'id="pu" onChange="pickup();"';
					echo form_dropdown('pu', $options, '1', $js);
				?>
			</td>
        	<td colspan='3' align='right' valign='top'>Diskon % Rp. :</td>
			<td colspan='2' valign='top'><input class="aright" type="text" name="persen" id="persen" value="<?php echo set_value('persen',6);?>" 
				onkeyup="
					this.value=formatCurrency(this.value);
					diskon(document.form.total,document.form.persen,document.form.rpdiskon); 
					totalbayardiskon(document.form.total,document.form.rpdiskon,document.form.totalbayar);"
				maxlength="2" size="5"/> Rp. <input class="aright" type="text" name="rpdiskon" id="rpdiskon" value="<?php echo set_value('rpdiskon',0);?>" size="12" onkeyup="this.value=formatCurrency(this.value);
				totalbayardiskon(document.form.total,document.form.rpdiskon,document.form.totalbayar);" />
			</td>
		</tr>
        <tr>
			<td valign="top">City of delivery</td>
			<td valign="top">:<?php
				echo form_hidden('kota_id', set_value('kota_id',0));
				echo form_hidden('propinsi', set_value('propinsi',0));
				echo form_hidden('timur', set_value('timur',0));
				echo form_hidden('deli_ad', set_value('deli_ad',0));
				
				echo form_hidden('addr1', set_value('addr1',0));
				echo form_hidden('kota_id1', set_value('kota_id1',0));
				
				$data = array('name'=>'city','id'=>'city','readonly'=>'1','value'=>set_value('city',$row['kota']));
				echo form_input($data);
				$atts = array(
					'width'      => '400',
					'height'     => '400',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'yes',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				echo anchor_popup('citysearch/allro/', "<img src='/images/backend/search.gif' border='0'>", $atts);
				?>
				<span class='error'>*<?php echo form_error('city'); ?></span>
			</td>
        	<td colspan='3' align='right' valign='top'>ECB % Rp. :</td>
			<td colspan='2' valign='top'><input class="aright" type="text" name="persen1" id="persen1" value="<?php echo set_value('persen1',0);?>" onkeyup="this.value=formatCurrency(this.value);
			 diskon(document.form.total,document.form.persen1,document.form.rpdiskon1); 
             totalbayardiskon1(document.form.total,document.form.rpdiskon,document.form.rpdiskon1,document.form.totalbayar);" maxlength="2" size="5"/> Rp. <input class="aright" type="text" name="rpdiskon1" id="rpdiskon1" value="<?php echo set_value('rpdiskon1',0);?>" size="12" onkeyup="this.value=formatCurrency(this.value);
             totalbayardiskon1(document.form.total,document.form.rpdiskon,document.form.rpdiskon1,document.form.totalbayar);" /></td>
		</tr>	
        <tr>
			<td valign='top'>Delivery Address</td>
			<td valign='top'>:&nbsp;<input type="text" name="addr" id="addr" value="<?php echo set_value('addr');?>" size="30" /></td>
        	<td colspan='3' align='right' valign='top'>Total Bayar Rp :</td>
			<td colspan='2' valign='top'><input class="textbold aright" type="text" name="totalbayar" id="totalbayar" value="<?php echo set_value('totalbayar',0);?>"  readonly="readonly" size="15"/></td>
		</tr>
		<tr>
			<td colspan='5'>&nbsp;</td>
			<td colspan="2"><?php echo form_submit('submit', 'Submit', 'tabindex="23"');?></td>
		</tr>
		
	</table>
		
<?php echo form_close();?>

<?php $this->load->view('footer');?>
