
<table width='100%'>
<?php echo form_open('order/omset_product_nett', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'>
			<?php 
				$data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  
				echo form_input($data);
			?>   
			<?php 
				$data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
				echo "to: ".form_input($data);
			?>
   </td>
  </tr>
 
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td>
			<?php echo form_submit('submit','preview');?>
			<?php echo form_submit('submit','export');?>
        </td>
  </tr>
<?php echo form_close();?>				
</table>


<table border="1" bordercolor="#999999" class="stripe">
	<tr>
		<td colspan="100%" style="border-bottom:solid thin #000099"></td>
	</tr>
	<tr>
		<th width="3%" rowspan="2" align="right"><div align="left">SKU</div></th>
		<th width="20%" rowspan="2" align="right"><div align="left">Product</div></th>
		<th colspan="2" align="right" width="18%"><div align="center">(SO + RO)</div></th>
		<th colspan="2" align="right" width="18%"><div align="center">SC Payment</div></th>
		<th colspan="2" align="right" width="18%"><div align="center">NC Product</div></th>
    	<!-- 20161010 ASP Start -->
		<th colspan="2" align="right" width="18%"><div align="center">Adjustment Product</div></th>
    	<!-- 20161010 ASP End -->
		<th colspan="2" align="right" width="18%"><div align="center">Retur</div></th>
		<th colspan="2" align="right" width="18%"><div align="center">Total <br />(SO + RO) - Retur </div></th>
    	<!-- 20180901 ASP Start -->
		<th colspan="2" align="right" width="18%"><div align="center">Product Top Up</div></th>
		<th colspan="2" align="right" width="18%"><div align="center">Product Event</div></th>
        <th colspan="2" align="right" width="18%"><div align="center">Grand Total <br /> (Total + Product Top Up + Product Event)</div></th>
    	<!-- 20180901 ASP End -->
	</tr>
	<tr>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Amount</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Amount</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Amount</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Amount</div></th>
	</tr>
	<tr>
		<td colspan="100%" style="border-bottom:solid thin #000099"></td>
	</tr>
	<?php
		$item = 0;
		if ($rekap):
			$jml=0;			$total=0;
			$jmlsc=0;		$totsc=0;
			$jmlnc=0;		$totnc=0;
			$jmladjstock=0;		$totadjstock=0;
			$jmlretur=0;	$totretur=0;
			$totalqty=0;	$totalnominal=0;
			
			$jmltopup=0;		$tottopup=0;
			$jmlevent=0;	$totevent=0;
			$grandtotalqty=0;	$grandtotalnominal=0;

			$currGroup = '';
			foreach($rekap as $dt1):
			if($currGroup == '') {
				$currGroup =  $dt1['group_name'];
				echo '
				<tr>
					<td colspan="100%" style="border-bottom:solid thin #000099"><b>'.$currGroup.'</b></td>
				</tr>
				';
			}else{
				if($currGroup !=  $dt1['group_name']){
					$currGroup =  $dt1['group_name'];
					echo '
					<tr>
						<td colspan="100%" style="border-bottom:solid thin #000099;border-top:solid thin #000099"><b>'.$currGroup.'</b></td>
					</tr>
					';
				}
			}
			// if($item!=$dt1['id']){$item = $dt1['id'];}else{$dt1['jmlnc']=0;$dt1['totnc']=0;}
	?>
	<tr>
		<td align="left"><?php echo $dt1['id']; 												?></td>
		<td align="left"><?php echo $dt1['nama']; 												?></td>
		<td align="right"><?php echo $dt1['jml']; 			$jml+=$dt1['jml'];					?></td>
		<td align="right"><?php echo $dt1['total'];			$total+=$dt1['total1'];				?></td>
		<td align="right"><?php echo $dt1['jmlsc'];			$jmlsc+=$dt1['jmlsc'];				?></td>
		<td align="right"><?php echo $dt1['totsc'];			$totsc+=$dt1['totsc1'];				?></td>
		<td align="right"><?php echo $dt1['jmlnc']; 		$jmlnc+=$dt1['jmlnc'];				?></td>
		<td align="right"><?php echo $dt1['totnc']; 		$totnc+=$dt1['totnc1'];				?></td>
		<td align="right"><?php echo $dt1['jmladjstock']; 	$jmladjstock+=$dt1['jmladjstock'];				?></td>
		<td align="right"><?php echo $dt1['totadjstock']; 	$totadjstock+=$dt1['totadjstock1'];				?></td>
		<td align="right"><?php echo $dt1['jmlretur']; 		$jmlretur+=$dt1['jmlretur'];		?></td>
		<td align="right"><?php echo $dt1['totretur']; 		$totretur+=$dt1['totretur1'];		?></td>
		<td align="right"><?php echo $dt1['totalqty']; 		$totalqty+=$dt1['totalqty'];		?></td>
		<td align="right"><?php echo $dt1['totalnominal']; 	$totalnominal+=$dt1['totalnominal1'];?></td>
        
		<td align="right"><?php echo $dt1['jml_topup']; 	   $jmltopup+= $dt1['jml_topup'];		?></td>
		<td align="right"><?php echo $dt1['total_topup']; 		$tottopup+=$dt1['total_topup1'];		?></td>
		<td align="right"><?php echo $dt1['jml_event']; 	   $jmlevent+= $dt1['jml_event'];		?></td>
		<td align="right"><?php echo $dt1['total_event']; 		$totevent+=$dt1['total_event1'];		?></td>
		<td align="right"><?php echo $dt1['totalqty_all']; 		$grandtotalqty+=$dt1['totalqty_all'];		?></td>
		<td align="right"><?php echo $dt1['totalnominal_all']; 	$grandtotalnominal+=$dt1['totalnominal_all1'];?></td>
	</tr>
	<?php
			endforeach;
			setlocale(LC_MONETARY, "en_US");
	?>
	<tr>
		<td align="left" colspan="2"><b><?php echo "TOTAL";			?></b></td>
		<td align="right"><b><?php echo $jml;			?></b></td>
		<td align="right"><b><?php echo number_format($total);?></b></td>
		<td align="right"><b><?php echo $jmlsc;			?></b></td>
		<td align="right"><b><?php echo number_format($totsc);			?></b></td>
		<td align="right"><i><?php echo $jmlnc;			?></i></td>
		<td align="right"><i><?php echo number_format($totnc);			?></i></td>
		<td align="right"><i><?php echo $jmladjstock;			?></i></td>
		<td align="right"><i><?php echo number_format($totadjstock);			?></i></td>
		<td align="right"><b><?php echo $jmlretur;		?></b></td>
		<td align="right"><b><?php echo number_format($totretur);		?></b></td>
		<td align="right"><b><?php echo $totalqty;		?></b></td>
		<td align="right"><b><?php echo number_format($totalnominal);	?></b></td>
        
		<td align="right"><b><?php echo $jmltopup;		?></b></td>
		<td align="right"><b><?php echo number_format($tottopup);		?></b></td>
		<td align="right"><b><?php echo $jmlevent;		?></b></td>
		<td align="right"><b><?php echo number_format($totevent);		?></b></td>
		<td align="right"><b><?php echo $grandtotalqty;		?></b></td>
		<td align="right"><b><?php echo number_format($grandtotalnominal);	?></b></td>
	</tr>
	<?php
		else:	?>
	<tr>
      <td colspan="100%">Data is not available.</td>
    </tr>
	<?php
		endif;
	?>
	<tr>
		<td colspan="100%" style="border-bottom:double medium #000099"></td>
	</tr>
</table>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
</script>
