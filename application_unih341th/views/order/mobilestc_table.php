<table width='99%'>
<?php echo form_open('order/mstc', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='60%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='40%' align='right'>search by: <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b>
			<?php }?>
			
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<table class="stripe">
	<tr>
      <th width='10%'>No.</th>
      <th width='15%'>Join Date</th>
      <th width='65%'>Stockiest ID / Name</th>
      <th width='10%'>Status</th>
    </tr>
   
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
      <td><?php echo anchor('order/mstc/view/'.$row['id'], $counter);?></td>
      <td><?php echo anchor('order/mstc/view/'.$row['id'], $row['created']);?></td>
      <td><?php echo anchor('order/mstc/view/'.$row['id'], $row['no_stc']." - ".$row['nama']);?></td>
      <td><?php echo anchor('order/mstc/view/'.$row['id'], $row['status']);?></td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="4">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
