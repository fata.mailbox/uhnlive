<table width='99%'>
<?php echo form_open('order/pjm', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<tr>
		<td width='50%'><strong><?php echo $this->pagination->create_links(); ?></strong></td>
		<td width='50%' align='right'>search by: <?php if($this->session->userdata('group_id') < 100 && $this->session->userdata('group_id')!= 28) echo form_dropdown('whsid',$warehouse);?> <?php $data = array('name'=>'search','id'=>'search','size'=>20,'value'=>set_value('search'));
    				echo form_input($data);?> <?php echo form_submit('submit','go');?>   <?php if($this->session->userdata('keywords')){ ?>
				<br />Your search keywords : <b><?php echo $this->session->userdata('keywords');?></b>
			<?php }?>
			
    	</td>  
  </tr>  
<?php echo form_close();?>				
</table>

<?php echo form_open('order/pjm/approved/', array('id' => 'my_form2', 'name' => 'my_form2', 'autocomplete' => 'off'));?>
<table class="stripe">
	<tr>
		<td colspan='2' valign='top'>remark: </td>
		<td colspan='9'><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','value'=>set_value('remark'));
			echo form_textarea($data);?><br>
			<?php echo form_submit('submit','approved');?>
		</td>
	</tr>
	<tr>
	  <th width='3%'>&nbsp;</th>
      <th width='5%'>No.</th>
      <th width='6%'>Invoice No.</th>
      <th width='10%'>Date</th>
      <th width='15%'>Stockiest ID / Name</th>
      <th width='11%'><div align="right">Total Price</div></th>
      <th width='12%'><div align="right">Total PV</div></th>
      <th width='11%'><div align="right">Warehouse</div></th>
      <th width='11%'><div align="right">App Time</div></th>
      <th width='8%'>Created By</th>
      <th width='8%'>Processed By</th>
    </tr>
   
<?php
if (isset($results)):
	$counter = $from_rows; foreach($results as $key => $row): 
	$counter = $counter+1;
?>
    <tr>
	  <td>
			<?php if($row['status'] == 'pending'){ 
				$data = array(
				'name'        => 'p_id[]',
				'id'          => 'p_id[]',
				'value'       => $row['id'],
				'checked'     => false,
				'style'       => 'border:none'
				);
				echo form_checkbox($data); } else {?>&nbsp;<?php }?> </td>
      <td><?php echo anchor('order/pjm/view/'.$row['id'], $counter);?></td>
      <td><?php echo anchor('order/pjm/view/'.$row['id'], $row['id']);?></td>
      <td><?php echo anchor('order/pjm/view/'.$row['id'], $row['tgl']);?></td>
      <td><?php echo anchor('order/pjm/view/'.$row['id'], $row['no_stc']." - ".$row['nama']);?></td>
      <td align="right"><?php echo anchor('order/pjm/view/'.$row['id'], $row['ftotalharga']);?></td>
		<td align="right"><?php echo anchor('order/pjm/view/'.$row['id'], $row['ftotalpv']);?></td>
		<td align="right"><?php echo anchor('order/pjm/view/'.$row['id'], $row['warehouse_name']);?></td>
		<td align="right"><?php echo anchor('order/pjm/view/'.$row['id'], $row['appdate']);?></td>
      <td><?php echo anchor('order/pjm/view/'.$row['id'], $row['createdby']);?></td>
      <td><?php echo anchor('order/pjm/view/'.$row['id'], $row['approvedby']);?></td>
    </tr>
    <?php endforeach; 
 else:
 ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php echo form_close();?>				
