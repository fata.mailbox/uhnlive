<table width='100%'>
<?php echo form_open('order/pjm_adm_his', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d',now())));  echo form_input($data);?>   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
   echo "to: ".form_input($data);?>
   </td>
  </tr>
  
  <?php if($this->session->userdata('group_id') > 100){ ?>
  <tr>
  <td width='24%'>STC No. / Name</td>
		<td width='1%'>:</td>
        <td width='75%'><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('username')." / ".$this->session->userdata('name');?></b></td>
     </tr>   
  
  <?php }else{ ?>
        <tr>
			<td valign='top'>Staff ID.</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_hidden('member_id',set_value('member_id','')); $data = array('name'=>'no_stc','id'=>'no_stc','size'=>15,'readonly'=>'1','value'=>set_value('no_stc'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('search/searchadmin/index/0', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?></td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
         <?php }?>
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
	</tr>
<?php echo form_close();?>				
</table>

<table class="stripe">
	<tr>
      <td colspan="8"><b>Saldo Pinjaman Product uptodate</b></td>
  </tr>
	<tr>
      <td colspan="8" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <th width='5%'>No.</th>
      <th width='10%'>Product ID.</th>
      <th width='25%'>Product Name</th>
      <th width='10%'><div align="right">Price</div></th>
      <th width='10%'><div align="right">PV</div></th>
      <th width='10%'><div align="right">Quantity</div></th>
      <th width='15%'><div align="right">Jumlah Harga</div></th>
      <th width='15%'><div align="right">Jumlah PV</div></th>
   </tr>
   	<tr>
      <td colspan="8" style="border-bottom:solid thin #000099"></td>
  </tr>

   
<?php
if ($saldopinjaman):
	foreach($saldopinjaman as $row): 
?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['item_id'];?></td>
      <td><?php echo $row['name'];?></td>
      <td align="right"><?php echo $row['fharga'];?></td>
      <td align="right"><?php echo $row['fpv'];?></td>
      <td align="right"><?php echo $row['fqty'];?></td>
      <td align="right"><?php echo $row['ftotalharga'];?></td>
      <td align="right"><?php echo $row['ftotalpv'];?></td>
    </tr>
     
  
  <?php endforeach; ?>
  <tr>
      <td colspan="8" style="border-bottom:solid thin #000099"></td>
  </tr>
  <tr>
      <td colspan="6" align="right"><b>Grand Total: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="right"><b><?php echo $totalsaldo['ftotalharga'];?></b></td>
      <td align="right"><b><?php echo $totalsaldo['ftotalpv'];?></b></td>
    </tr>
    <tr>
      <td colspan="8" style="border-bottom:double medium #000099"></td>
    </tr>
 <?php else: ?>
    <tr>
      <td colspan="8">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<table class="stripe">
	<tr>
      <td colspan="8"><b>Rekap Pinjaman Stockiest</b></td>
  </tr>
	<tr>
      <td colspan="8" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <th width='8%'>No.</th>
      <th width='15%'>Date</th>
      <th width='10%'>No. Ref</th>
      <th width='18%'><div align="right">Pinjaman</div></th>
      <th width='18%'><div align="right">Pelunasan</div></th>
      <th width='18%'><div align="right">Saldo</div></th>
      <th width='13%'>User ID</th>
   </tr>
   	<tr>
      <td colspan="7" style="border-bottom:solid thin #000099"></td>
  </tr>

   
<?php
if ($pinjamanhistory): ?>
<tr>
      <td>1</td>
      <td colspan='4'>Saldo awal pinjaman</td>
      <td><strong><?php echo $pinjamanhistory[0]['fawal'];?></strong></td>
		<td>&nbsp;</td>      
     </tr>
     
<?php foreach($pinjamanhistory as $row): ?>
    <tr>
      <td><?php echo $row['i'];?><?php if($row['remark']) echo "*";?></td>
      <td><?php echo $row['tgl'];?></td>
      <td><?php echo $row['noref'];?></td>
      <td align="right"><?php echo $row['fpinjam'];?></td>
      <td align="right"><?php echo $row['flunas'];?></td>
      <td align="right"><?php echo $row['fakhir'];?></td>
      <td><?php echo $row['createdby'];?></td>
    </tr>
     
  
  <?php endforeach; ?>
  <tr>
      <td colspan="7" style="border-bottom:solid thin #000099"></td>
  </tr>
  <tr>
      <td colspan="3" align="right"><b>Total: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="right"><b><?php echo $totalhistory['ftotalpinjam'];?></b></td>
      <td align="right"><b><?php echo $totalhistory['ftotallunas'];?></b></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="7" style="border-bottom:double medium #000099"></td>
    </tr>
    <tr>
      <td colspan="7">* = Pelunasan melalui retur pinjaman</td>
    </tr>
 <?php else: ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<table class="stripe">
	<tr>
      <td colspan="9"><b>History Pinjaman Product</b></td>
  </tr>
	<tr>
      <td colspan="9" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <th width='5%'>No.</th>
      <th width='25%'>ID / Product Name</th>
      <th width='10%'><div align="right">Price</div></th>
      <th width='10%'>Tanggal</th>
      <th width='10%'><div align="right">Saldo Awal</div></th>
      <th width='10%'><div align="right">Saldo In</div></th>
      <th width='10%'><div align="right">Saldo Out</div></th>
      <th width='10%'><div align="right">Saldo Akhir</div></th>
     <th width='10%'>Userid</th>
   </tr>
   	<tr>
      <td colspan="9" style="border-bottom:solid thin #000099"></td>
  </tr>

   
<?php
if ($stock):
	foreach($stock as $row): 
?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['item_id']." / ".$row['name'];?></td>
      <td align="right"><?php echo $row['fharga'];?></td>
      <td><?php echo $row['ftgl'];?></td>
      <td align="right"><?php echo $row['fawal'];?></td>
      <td align="right"><?php echo $row['fin'];?></td>
      <td align="right"><?php echo $row['fout'];?></td>
      <td align="right"><?php echo $row['fakhir'];?></td>
            <td><?php echo $row['createdby'];?></td>
    </tr>
     
  
  <?php endforeach; ?>
    <tr>
      <td colspan="9" style="border-bottom:double medium #000099"></td>
    </tr>
 <?php else: ?>
    <tr>
      <td colspan="9">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>
