<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<?php // Created by Boby 20140120 ?>
<script language="JavaScript">
function pickup(){
	var ratio = 0;
	var jml = 0;
	
	if(document.form.pu.value==1){
		if(document.form.timur.value==0){
			ratio_ = 1;
		}else{
			ratio_ = 1.1;
		}
	}else{
		ratio_ = 1;
	}
	
<?php for($i=0;$i<10;$i++){ ?>
	if(document.form.pv<?php echo $i; ?>.value==0){
		ratio = 1;
	}else{
		ratio = ratio_;
	}
	// document.form.price<?php echo $i; ?>.value = formatCurrency(Math.floor(document.form.gprice<?php echo $i; ?>.value * ratio));
	   document.form.price<?php echo $i; ?>.value = formatCurrency(Math.round((document.form.gprice<?php echo $i; ?>.value * ratio)/1000)*1000);
	jumlah(document.form.qty<?php echo $i; ?>,document.form.price<?php echo $i; ?>,document.form.subtotal<?php echo $i; ?>);
<?php } ?>
	document.form.total.value=total_curr(10,'document.form.subtotal');
	document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');
	
}

function afterklik(){
	// alert("Test");
}
</script>
<?php // End created by Boby 20140120 ?>
	
<?php
	if ($this->session->flashdata('message')){echo "<div class='message'>".$this->session->flashdata('message')."</div><br>";}
	echo form_open('order/so/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>
	<table width='100%'>
		<tr>
			<td width='20%'>Date</td>
			<td width='1%'>:</td>
			<td width='79%'>
				<?php 
					$data = array('name'=>'date','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('date',date('Y-m-d',now())));
					echo form_input($data);
				?>
				<span class='error'><?php echo form_error('date');?></span>
			</td>
		</tr>
        <tr>
			<td valign='top'>Member ID</td>
			<td valign='top'>:</td>
			<td valign='top'>
				<?php 
					$data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
					echo form_input($data);
					$atts = array(
						'width'      => '450',
						'height'     => '600',
						'scrollbars' => 'yes',
						'status'     => 'yes',
						'resizable'  => 'yes',
						'screenx'    => '0',
						'screeny'    => '0'
					);
					echo anchor_popup('memsearch/ewalletso', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
				?>
				<span class='error'>*<?php echo form_error('member_id');?></span>
			</td>	
		</tr>
		<tr>
			<td valign='top'>Member Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
        <tr>
			<td valign='top'>Ewallet Rp</td>
			<td valign='top'>:</td>
			<td><input type="text" name="ewallet" id="ewallet" readonly="1" value="<?php echo set_value('ewallet');?>" size="15" /></td>
		</tr>
		
		<tr><td colspan="3"><hr/></td></tr>
		
        <tr>
			<td valign='top'>Remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','tabindex'=>'1','value'=>set_value('remark'));
				echo form_textarea($data);?>
			</td> 
		</tr>
	</table>
		
	<table width='100%'>	
		<tr>
			<td width='18%'>Item Code</td>
			<td width='23%'>Item Name</td>
			<td width='8%'>Qty</td>
			<td width='12%'>Price</td>
			<td width='9%'>PV</td>
			<td width='13%'>Sub Total Price</td>
			<td width='13%'>Sub Total PV</td>
			<td width='5%'>Del?</td>
		</tr>
		
		<?php for($i=0;$i<=9;$i++){ ?>
		<tr>
			<td valign='top'>
			<?php
				$data = array('name'=>'itemcode'.$i,'id'=>'itemcode'.$i,'size'=>'8','readonly'=>'1','value'=>set_value('itemcode'.$i)); 
				echo form_input($data);
				$atts = array(
					'width'      => '600',
					'height'     => '500',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'no',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				echo anchor_popup('itemsosearch/index/'.$i, '<input class="button" type="button" tabindex="2" name="Button" value="browse" onclick="afterklik()"/>', $atts);
				echo form_hidden('titipan_id'.$i,set_value('titipan_id'.$i,0));
				echo form_hidden('bv'.$i,set_value('bv'.$i,0));
				echo form_hidden('subtotalbv'.$i,set_value('subtotalbv'.$i,0));
				echo form_hidden('gprice'.$i,set_value('gprice'.$i,0));
			?>
				
			<td valign='top'>
				<input type="text" name="itemname<?php echo $i;?>" id="itemname<?php echo $i;?>" value="<?php echo set_value('itemname'.$i);?>" readonly="1" size="24" />
			</td>
			<td>
				<input class='textbold aright' type="text" name="qty<?php echo $i;?>" id="qty<?php echo $i;?>"
					value="<?php echo set_value('qty'.$i,0);?>" 
					maxlength="12" size="3" tabindex="3" autocomplete="off" 
					onkeyup="
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.bv<?php echo $i;?>,document.form.subtotalbv<?php echo $i;?>);
						document.form.total.value=total_curr(10,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');
						document.form.totalbv.value=totalbv_curr(10,'document.form.subtotalbv');
				">
			</td>
			<td>
				<input class="aright" type="text" name="price<?php echo $i;?>" id="price<?php echo $i;?>" size="8" 
					value="<?php echo set_value('price'.$i,0);?>"
					onkeyup="
						this.value=formatCurrency(this.value);
						jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
						document.form.total.value=total_curr(10,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');
					">
			</td>
			<td>
				<input class="aright" type="text" name="pv<?php echo $i;?>" id="pv<?php echo $i;?>"
					value="<?php echo set_value('pv'.$i,0);?>" size="5" 
					onkeyup="this.value=formatCurrency(this.value); 
						jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
						jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
						document.form.total.value=total_curr(10,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');
				">
			</td>
			<td>
				<input class="aright" type="text" name="subtotal<?php echo $i;?>" id="subtotal<?php echo $i;?>" value="<?php echo set_value('subtotal'.$i,0);?>" readonly="1" size="12">
			</td>
			<td>
				<input class="aright" type="text" name="subtotalpv<?php echo $i;?>" id="subtotalpv<?php echo $i;?>" value="<?php echo set_value('subtotalpv'.$i,0);?>" readonly="1" size="10">
			</td>
			<td>
				<img alt="delete" 
					onclick="
					cleartext10(
							document.form.itemcode<?php echo $i;?>
							,document.form.itemname<?php echo $i;?>
							,document.form.qty0,document.form.price<?php echo $i;?>
							,document.form.pv0,document.form.subtotal<?php echo $i;?>
							,document.form.subtotalpv<?php echo $i;?>
							,document.form.titipan_id<?php echo $i;?>
							,document.form.bv<?php echo $i;?>
							,document.form.subtotalbv<?php echo $i;?>
						); 
						document.form.total.value=total_curr(10,'document.form.subtotal');
						document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');
						document.form.totalbv.value=totalbv_curr(10,'document.form.subtotalbv');" 
					src="<?php echo  base_url();?>images/backend/delete.png" border="0"
				/>
			 </td>
		</tr>
		<?php } ?>
		
		<tr>
			<td colspan='5'></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total',0);?>" readonly="1" size="11"></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv',0);?>" readonly="1" size="9">
             <?php echo form_hidden('totalbv',set_value('totalbv',0));?></td>
		</tr>	
        <?php if(validation_errors() or form_error('totalbayar')){?>
		<tr>
        	<td colspan='6'>&nbsp;</td>
			<td colspan='2'><span class="error"><?php echo form_error('total');?> <?php echo form_error('totalbayar');?></span></td>
		</tr>
		<?php }?>
		
		<?php // Created by Boby 20140120 ?>
		<tr><td colspan="8"><hr/></td></tr>
		<tr>
			<td colspan="2"><b>Pick up / Delivery</b></td>
			<td valign="top" colspan='3' align='left'>&nbsp;</td>
			<td colspan="2"><b>Payment</b></td>
		</tr>
		<tr>
			<td valign="top">Option</td>
			<td valign="top">:
				<?php
					$options = array(
						'0'  => 'Pick Up',
						'1'    => 'Delivery',
					);
					$js = 'id="pu" onChange="pickup();"';
					echo form_dropdown('pu', $options, '0', $js);
				?>
			</td>
			<td valign="top" colspan='4' align='right'>Cash :</td>
			<td colspan='2' valign='top'></span><input type="text" class="textbold" size="15" name="tunai" id="tunai" autocomplete="off" value="<?php echo set_value('tunai',0);?>" onkeyup="this.value=formatCurrency(this.value); totalBayar(document.form.tunai,document.form.debit,document.form.credit,document.form.totalbayar)">
            </td>
		</tr>
		<tr>
			<td valign="top">City of delivery</td>
			<td valign="top">:<?php
				echo form_hidden('kota_id', set_value('kota_id',0));
				echo form_hidden('propinsi', set_value('propinsi',0));
				echo form_hidden('timur', set_value('timur',0));
				echo form_hidden('deli_ad', set_value('deli_ad',0));
				
				echo form_hidden('addr1', set_value('addr1',0));
				echo form_hidden('kota_id1', set_value('kota_id1',0));
				
				$data = array('name'=>'city','id'=>'city','readonly'=>'1','value'=>set_value('city',$row['kota']));
				echo form_input($data);
				$atts = array(
					'width'      => '400',
					'height'     => '400',
					'scrollbars' => 'yes',
					'status'     => 'yes',
					'resizable'  => 'yes',
					'screenx'    => '0',
					'screeny'    => '0'
				);
				echo anchor_popup('citysearch/allso/', "<img src='/images/backend/search.gif' border='0'>", $atts);
				?>
				<span class='error'>*<?php echo form_error('city'); ?></span>
			</td>
			<td valign="top" colspan='4' align='right'>Debit Card :</td>
			<td colspan='2' valign='top'></span><input type="text" class="textbold" size="15" name="debit" id="debit" autocomplete="off" value="<?php echo set_value('debit',0);?>" onkeyup="this.value=formatCurrency(this.value); totalBayar(document.form.tunai,document.form.debit,document.form.credit,document.form.totalbayar)">
            </td>
		</tr>
		<tr>
			<td valign='top'>Delivery Address</td>
			<td valign='top'>:
			<input type="text" name="addr" id="addr" value="<?php echo set_value('addr');?>" size="30" /></td>
			<td valign="top" colspan='4' align='right'>Credit Card :</td>
			<td colspan='2' valign='top'></span><input type="text" class="textbold" size="15" name="credit" id="credit" autocomplete="off" value="<?php echo set_value('credit',0);?>" onkeyup="this.value=formatCurrency(this.value); totalBayar(document.form.tunai,document.form.debit,document.form.credit,document.form.totalbayar)">
            </td>
		</tr>
        
        <tr>
			<td colspan='6' align="right">Total Payment Rp.</td>
			<td colspan="2"><input class='textbold' type="text" name="totalbayar" id="totalbayar" size="15" value="<?php echo set_value('totalbayar',0);?>" readonly="1" size="11"></td>
		</tr>	
		<tr>
			<td colspan='6'>&nbsp;</td>
			<td><?php echo form_submit('submit', 'Submit', 'tabindex="23"');?></td>
            <td>&nbsp;</td>
		</tr>		
	</table>
<?php echo form_close();?>

<?php $this->load->view('footer');?>
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>