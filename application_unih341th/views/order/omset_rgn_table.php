<style type="text/css">
.style1 {color: #0000FF}
.style2 {color: #FF0000}
</style>
<table width='100%'>
<?php echo form_open('order/omsetrgn', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Start Date</td>
		<td width='1%'>:</td>
		<td width='75%'>
			<?php 
				$data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  
				echo form_input($data);
				
				/* Created by Boby 2011-03-01 */
				$data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));  
				echo " - ".form_input($data);
				/* End created by Boby 2011-03-01 */
				
			?>
		</td>
	</tr>
	<tr>
		<td>Category</td>
		<td>:</td>
        <td><?php 
				$data = array(
					'0'=>'Regional',
					'1'=>'Leader',
					);
				echo form_dropdown('leader',$data);
		?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
	</tr>
	<tr><td colspan="3"><i>*Include Tax</i></td></tr>
<?php echo form_close();?>				
</table>

<table border="1" bordercolor="#999999" class="stripe">
	<tr>
		<td colspan="8" style="border-bottom:solid thin #000099"></td>
	</tr>
	<?php
		if ($field_){			
	?>
	<tr>
		<th align="left"><div align="left">Category Name</div></th>
		<th align="right"><div align="right"><?php echo $field_['bln1'];?></div></th>
		<th align="right"><div align="right"><?php echo $field_['bln2'];?></div></th>
		<th align="right"><div align="right"><?php echo $field_['bln3'];?></div></th>
		<th align="right"><div align="right"><?php echo $field_['bln4'];?></div></th>
		<th align="right"><div align="right"><?php echo $field_['bln5'];?></div></th>
		<th align="right"><div align="right"><?php echo $field_['bln6'];?></div></th>
		<th align="right"><div align="right">Total Amount</div></th>
	</tr>
	<tr>
		<td colspan="11" style="border-bottom:solid thin #000099"></td>
	</tr>
	<?php
		if ($rekap):
			$b1 = 0;$b2 = 0;$b3 = 0;$b4 = 0;$b5 = 0;$b6 = 0;$bt = 0;$btt = 0;
			$_b1 = 0;$_b2 = 0;$_b3 = 0;$_b4 = 0;$_b5 = 0;$_b6 = 0;$_bt = 0;$_btt = 0;
			foreach($rekap as $dt1):
				$bt = 0; 
				$_b1 = $dt1[$field_['b1']];	$b1+=$_b1;	$_b2 = $dt1[$field_['b2']];	$b2+=$_b2;
				$_b3 = $dt1[$field_['b3']];	$b3+=$_b3;	$_b4 = $dt1[$field_['b4']];	$b4+=$_b4;
				$_b5 = $dt1[$field_['b5']];	$b5+=$_b5;	$_b6 = $dt1[$field_['b6']];	$b6+=$_b6;
	?>
	
	<tr>
		<td align="left"><?php echo $dt1['kota']; ?></td>
		<td align="right"><span class="style1"><?php echo number_format($_b1); ?></span></td>
		<td align="right"><span class="style<?php echo($_b1>$_b2)?"2":"1"; ?>"><?php echo number_format($_b2); ?></span></td>
		<td align="right"><span class="style<?php echo($_b2>$_b3)?"2":"1"; ?>"><?php echo number_format($_b3); ?></span></td>
		<td align="right"><span class="style<?php echo($_b3>$_b4)?"2":"1"; ?>"><?php echo number_format($_b4); ?></span></td>
		<td align="right"><span class="style<?php echo($_b4>$_b5)?"2":"1"; ?>"><?php echo number_format($_b5); ?></span></td>
		<td align="right"><span class="style<?php echo($_b5>$_b6)?"2":"1"; ?>"><?php echo number_format($_b6); ?></span></td>
		<td align="right"><b><?php echo number_format($dt1['total']); $btt += $dt1['total']; ?></b></td>
	</tr>
	<?php
			endforeach;
			setlocale(LC_MONETARY, "en_US");
	?>
	<tr>
		<td align="left"><b><?php echo "TOTAL";	?></b></td>
		<td align="right"><b><span class="style1"><?php echo number_format($b1);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b1>$b2)?"2":"1";?>"><?php echo number_format($b2);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b2>$b3)?"2":"1";?>"><?php echo number_format($b3);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b3>$b4)?"2":"1";?>"><?php echo number_format($b4);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b4>$b5)?"2":"1";?>"><?php echo number_format($b5);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b5>$b6)?"2":"1";?>"><?php echo number_format($b6);?></b></span></td>
		<td align="right"><b><?php echo number_format($btt);?></b></td>
	</tr>
	<?php
		else:	?>
	<tr>
      <td colspan="11">Data is not available.</td>
    </tr>
	<?php
		endif;
	}
	?>
	<tr>
		<td colspan="11" style="border-bottom:double medium #000099"></td>
	</tr>
</table>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
	
	/* Created by Boby 2011-03-01 */
	Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
	/* End created by Boby 2011-03-01 */
</script>
