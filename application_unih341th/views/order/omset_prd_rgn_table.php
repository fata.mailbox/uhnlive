<style type="text/css">
.style1 {color: #0000FF}
.style2 {color: #FF0000}
</style>
<table width='100%'>
<?php echo form_open('order/omsetrgnprd', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Start Date</td>
		<td width='1%'>:</td>
		<td width='75%'>
			<?php 
				$data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  
				echo form_input($data);
			?>
		</td>
	</tr>
	<tr>
		<td>Category</td>
		<td>:</td>
        <td><?php 
				$data = array(
					'0'=>'Regional',
					'1'=>'Leader',
					);
				echo form_dropdown('leader',$data);
		?></td>
	</tr>
	<tr>
		<td valign='top'>Leader</td>
		<td valign='top'>:</td>
		<td valign='top'>
			<?php 
				echo form_hidden('member_id',set_value('member_id','')); 
				$data = array('name'=>'no_stc','id'=>'no_stc','size'=>15,'readonly'=>'1','value'=>set_value('no_stc'));
				echo form_input($data);
				$atts = array(
				  'width'      => '400',
				  'height'     => '400',
				  'scrollbars' => 'yes',
				  'status'     => 'yes',
				  'resizable'  => 'yes',
				  'screenx'    => '0',
				  'screeny'    => '0'
				);
				echo anchor_popup('stcsearch/all/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
			?>
		</td>
	</tr>
	<tr>
		<td valign='top'>Region Area</td>
		<td valign='top'>:</td>
		<td valign='top'>
			<?php
			echo form_hidden('kota_id');
			echo form_hidden('propinsi');
			$data = array('name'=>'city','id'=>'city','readonly'=>'1','value'=>set_value('city'));
			echo form_input($data);
			$atts = array(
				'width'      => '400',
				'height'     => '400',
				'scrollbars' => 'yes',
				'status'     => 'yes',
				'resizable'  => 'yes',
				'screenx'    => '0',
				'screeny'    => '0'
				);
			echo anchor_popup('citysearch/all/', "<img src='/images/backend/search.gif' border='0'>", $atts); 
			?>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
	</tr>
	<tr><td colspan="3"><i>*Include Tax</i></td></tr>
<?php echo form_close();?>				
</table>

<table border="1" bordercolor="#999999" class="stripe">
	<tr>
		<td colspan="15" style="border-bottom:solid thin #000099"></td>
	</tr>
	<?php
		if ($field_){			
	?>
	<tr>
		<th align="left"><div align="left">Category Name</div></th>
		<th align="right" colspan="2"><div align="right"><?php echo $field_['bln1'];?></div></th>
		<th align="right" colspan="2"><div align="right"><?php echo $field_['bln2'];?></div></th>
		<th align="right" colspan="2"><div align="right"><?php echo $field_['bln3'];?></div></th>
		<th align="right" colspan="2"><div align="right"><?php echo $field_['bln4'];?></div></th>
		<th align="right" colspan="2"><div align="right"><?php echo $field_['bln5'];?></div></th>
		<th align="right" colspan="2"><div align="right"><?php echo $field_['bln6'];?></div></th>
		<th align="right" colspan="2"><div align="right">Total Amount</div></th>
	</tr>
	<tr>
		<td colspan="15" style="border-bottom:solid thin #000099"></td>
	</tr>
	<?php
		if ($rekap):
			$nama = ""; $flag = 0;	$nm = 'bln'; $nm1 = 'jml';
			
			$b1=0;		$b2=0;		$b3=0;		$b4=0;		$b5=0;		$b6=0;		$btt=0;	// Field Grand Total Nominal
			$b11=0;		$b21=0;		$b31=0;		$b41=0;		$b51=0;		$b61=0;		$btt1=0;// Field Grand Total Jumlah
			
			$_b1=0;		$_b2=0;		$_b3=0;		$_b4=0;		$_b5=0;		$_b6=0;		$_bt=0;		$_btt=0;	// Field Nominal Per Bulan
			$_b11=0;	$_b21=0;	$_b31=0;	$_b41=0;	$_b51=0;	$_b61=0;	$_bt1=0;	$_btt1=0;	// Field Jumlah Per Bulan
			
			$cat1=0;	$cat2=0;	$cat3=0;	$cat4=0;	$cat5=0;	$cat6=0;	$catjml=0;	$catnum=0;	// Field Nominal Sub Category
			$cat11=0;	$cat21=0;	$cat31=0;	$cat41=0;	$cat51=0;	$cat61=0;	$catjml1=0;	$catnum1=0;	// Field Jumlah Sub Category
					
			foreach($rekap as $dt1):
				// $bt = 0;	$bt1 = 0;
				
				// Nominal
				$_b1=$dt1[$nm.$field_['b1']];	$_b2=$dt1[$nm.$field_['b2']];	$_b3=$dt1[$nm.$field_['b3']];	// Nominal per bulan
				$_b4=$dt1[$nm.$field_['b4']];	$_b5=$dt1[$nm.$field_['b5']];	$_b6=$dt1[$nm.$field_['b6']];	//    -----||-----
				$b1+=$_b1;		$b2+=$_b2;		$b3+=$_b3;		$b4+=$_b4;		$b5+=$_b5;		$b6+=$_b6;		// Grand Total Nominal
				
				// Jumlah
				$_b11=$dt1[$nm1.$field_['b1']];	$_b21=$dt1[$nm1.$field_['b2']];	$_b31=$dt1[$nm1.$field_['b3']];	// Jumlah per bulan
				$_b41=$dt1[$nm1.$field_['b4']];	$_b51=$dt1[$nm1.$field_['b5']];	$_b61=$dt1[$nm1.$field_['b6']]; //    -----||-----
				$b11+=$_b11;	$b21+=$_b21;	$b31+=$_b31;	$b41+=$_b41;	$b51+=$_b51;	$b61+=$_b61; 	// Grand Total Jumlah
				
				
				
				if($nama!=$dt1['nama']){
					$nama=$dt1['nama'];
					$namaCat = $dt1['kota'];
					if($flag==1){
	?>
						<tr><td colspan="15" style="border-bottom:solid thin #000099"></td></tr>
						<tr>
							<td align="left"><b><i>Total</td>
							<td align="right"><b><i><span class="style1"><?php echo number_format($cat11); ?></span></td>
							<td align="right"><b><i><span class="style1"><?php echo number_format($cat1); ?></span></td>
							<td align="right"><b><i><span class="style<?php echo($cat11>$cat21)?"2":"1"; ?>"><?php echo number_format($cat21); ?></span></td>
							<td align="right"><b><i><span class="style<?php echo($cat1>$cat2)?"2":"1"; ?>"><?php echo number_format($cat2); ?></span></td>
							<td align="right"><b><i><span class="style<?php echo($cat21>$cat31)?"2":"1"; ?>"><?php echo number_format($cat31); ?></span></td>
							<td align="right"><b><i><span class="style<?php echo($cat2>$cat3)?"2":"1"; ?>"><?php echo number_format($cat3); ?></span></td>
							<td align="right"><b><i><span class="style<?php echo($cat31>$cat41)?"2":"1"; ?>"><?php echo number_format($cat41); ?></span></td>
							<td align="right"><b><i><span class="style<?php echo($cat3>$cat4)?"2":"1"; ?>"><?php echo number_format($cat4); ?></span></td>
							<td align="right"><b><i><span class="style<?php echo($cat41>$cat51)?"2":"1"; ?>"><?php echo number_format($cat51); ?></span></td>
							<td align="right"><b><i><span class="style<?php echo($cat4>$cat5)?"2":"1"; ?>"><?php echo number_format($cat5); ?></span></td>
							<td align="right"><b><i><span class="style<?php echo($cat51>$cat61)?"2":"1"; ?>"><?php echo number_format($cat61); ?></span></td>
							<td align="right"><b><i><span class="style<?php echo($cat5>$cat6)?"2":"1"; ?>"><?php echo number_format($cat6); ?></span></td>
							<td align="right"><b><i><?php echo number_format($catnum1);		?></b></td>
							<td align="right"><b><i><?php echo number_format($catnum);	?></b></td>
						</tr>
						<tr><td colspan="15" style="border-bottom:solid thin #000099"></td></tr>
						<tr><td colspan="15">&nbsp;</td></tr>
	<?php 			} ?>
					<tr><td colspan="15"><b><?php echo $nama; ?></b></td></tr>
					<tr><td colspan="15" style="border-bottom:solid thin #000099"></td></tr>
	<?php 		
					$cat1=0;	$cat2=0;	$cat3=0;	$cat4=0;	$cat5=0;	$cat6=0;	$catjml=0;	$catnum=0;	// Field Nominal Sub Category
					$cat11=0;	$cat21=0;	$cat31=0;	$cat41=0;	$cat51=0;	$cat61=0;	$catjml1=0;	$catnum1=0;	// Field Jumlah Sub Category
				}$flag = 1;
				// Sub Category
				$cat1+=$_b1;	$cat2+=$_b2;	$cat3+=$_b3;	$cat4+=$_b4;	$cat5+=$_b5;	$cat6+=$_b6;	$catnum1+=$dt1['total'];	// Field Nominal Sub Category
				$cat11+=$_b11;	$cat21+=$_b21;	$cat31+=$_b31;	$cat41+=$_b41;	$cat51+=$_b51;	$cat61+=$_b61;	$catnum+=$dt1['totalJml'];	// Field Jumlah Sub Category
	?>
	<tr>
		<td align="left"><?php echo $dt1['kota']; ?></td>
		<td align="right"><span class="style1"><?php echo number_format($_b11); ?></span></td>
		<td align="right"><span class="style1"><?php echo number_format($_b1); ?></span></td>
		<td align="right"><span class="style<?php echo($_b11>$_b21)?"2":"1"; ?>"><?php echo number_format($_b21); ?></span></td>
		<td align="right"><span class="style<?php echo($_b1>$_b2)?"2":"1"; ?>"><?php echo number_format($_b2); ?></span></td>
		<td align="right"><span class="style<?php echo($_b21>$_b31)?"2":"1"; ?>"><?php echo number_format($_b31); ?></span></td>
		<td align="right"><span class="style<?php echo($_b2>$_b3)?"2":"1"; ?>"><?php echo number_format($_b3); ?></span></td>
		<td align="right"><span class="style<?php echo($_b31>$_b41)?"2":"1"; ?>"><?php echo number_format($_b41); ?></span></td>
		<td align="right"><span class="style<?php echo($_b3>$_b4)?"2":"1"; ?>"><?php echo number_format($_b4); ?></span></td>
		<td align="right"><span class="style<?php echo($_b41>$_b51)?"2":"1"; ?>"><?php echo number_format($_b51); ?></span></td>
		<td align="right"><span class="style<?php echo($_b4>$_b5)?"2":"1"; ?>"><?php echo number_format($_b5); ?></span></td>
		<td align="right"><span class="style<?php echo($_b51>$_b61)?"2":"1"; ?>"><?php echo number_format($_b61); ?></span></td>
		<td align="right"><span class="style<?php echo($_b5>$_b6)?"2":"1"; ?>"><?php echo number_format($_b6); ?></span></td>
		<td align="right"><b><?php echo number_format($dt1['total']); $btt1 += $dt1['total']; ?></b></td>
		<td align="right"><b><?php echo number_format($dt1['totalJml']); $btt += $dt1['totalJml']; ?></b></td>
	</tr>
	<?php
			endforeach;
			setlocale(LC_MONETARY, "en_US");
	?>
	<tr><td colspan="15" style="border-bottom:solid thin #000099"></td></tr>
	<tr>
		<td align="left"><b><i>Total</td>
		<td align="right"><b><i><span class="style1"><?php echo number_format($cat11); ?></span></td>
		<td align="right"><b><i><span class="style1"><?php echo number_format($cat1); ?></span></td>
		<td align="right"><b><i><span class="style<?php echo($cat11>$cat21)?"2":"1"; ?>"><?php echo number_format($cat21); ?></span></td>
		<td align="right"><b><i><span class="style<?php echo($cat1>$cat2)?"2":"1"; ?>"><?php echo number_format($cat2); ?></span></td>
		<td align="right"><b><i><span class="style<?php echo($cat21>$cat31)?"2":"1"; ?>"><?php echo number_format($cat31); ?></span></td>
		<td align="right"><b><i><span class="style<?php echo($cat2>$cat3)?"2":"1"; ?>"><?php echo number_format($cat3); ?></span></td>
		<td align="right"><b><i><span class="style<?php echo($cat31>$cat41)?"2":"1"; ?>"><?php echo number_format($cat41); ?></span></td>
		<td align="right"><b><i><span class="style<?php echo($cat3>$cat4)?"2":"1"; ?>"><?php echo number_format($cat4); ?></span></td>
		<td align="right"><b><i><span class="style<?php echo($cat41>$cat51)?"2":"1"; ?>"><?php echo number_format($cat51); ?></span></td>
		<td align="right"><b><i><span class="style<?php echo($cat4>$cat5)?"2":"1"; ?>"><?php echo number_format($cat5); ?></span></td>
		<td align="right"><b><i><span class="style<?php echo($cat51>$cat61)?"2":"1"; ?>"><?php echo number_format($cat61); ?></span></td>
		<td align="right"><b><i><span class="style<?php echo($cat5>$cat6)?"2":"1"; ?>"><?php echo number_format($cat6); ?></span></td>
		<td align="right"><b><i><?php echo number_format($catnum1);		?></b></td>
		<td align="right"><b><i><?php echo number_format($catnum);	?></b></td>
	</tr>
	<tr><td colspan="15" style="border-bottom:solid thin #000099"></td></tr>
	<tr><td colspan="15">&nbsp;</td></tr>
	<tr>
		<td align="left"><b><?php echo "GRAND TOTAL";	?></b></td>
		<td align="right"><b><span class="style1"><?php echo number_format($b11);?></b></span></td>
		<td align="right"><b><span class="style1"><?php echo number_format($b1);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b11>$b21)?"2":"1";?>"><?php echo number_format($b21);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b1>$b2)?"2":"1";?>"><?php echo number_format($b2);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b21>$b31)?"2":"1";?>"><?php echo number_format($b31);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b2>$b3)?"2":"1";?>"><?php echo number_format($b3);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b31>$b41)?"2":"1";?>"><?php echo number_format($b41);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b3>$b4)?"2":"1";?>"><?php echo number_format($b4);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b41>$b51)?"2":"1";?>"><?php echo number_format($b51);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b4>$b5)?"2":"1";?>"><?php echo number_format($b5);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b51>$b61)?"2":"1";?>"><?php echo number_format($b61);?></b></span></td>
		<td align="right"><b><span class="style<?php echo($b5>$b6)?"2":"1";?>"><?php echo number_format($b6);?></b></span></td>
		<td align="right"><b><?php echo number_format($btt1);?></b></td>
		<td align="right"><b><?php echo number_format($btt);?></b></td>
	</tr>
	<?php
		else:	?>
	<tr>
      <td colspan="15">Data is not available.</td>
    </tr>
	<?php
		endif;
	}
	?>
	<tr>
		<td colspan="15" style="border-bottom:double medium #000099"></td>
	</tr>
</table>

<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
</script>
