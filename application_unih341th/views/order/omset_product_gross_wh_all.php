<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php 
if ($this->session->flashdata('message')){

	echo "<div class='message'>".$this->session->flashdata('message')."</div>";}
?>
<table width='100%'>
<?php echo form_open('order/omset_product_gross_wh', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'>
			<?php 
				$data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  
				echo form_input($data);
			?>   
			<?php 
				$data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
				echo "to: ".form_input($data);
			?>
   </td>
  </tr>
        <tr>
			<td valign='top'>Warehouse</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('whsid',$warehouse);?></td>
		</tr>
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td>
			<?php echo form_submit('submit','preview');?>
			<?php echo form_submit('submit','export');?>
        </td>
  </tr>
<?php echo form_close();?>				
</table>


<table border="1" bordercolor="#999999" class="stripe">
	<tr>
		<td colspan="100%" style="border-bottom:solid thin #000099"></td>
	</tr>
	<tr>
		<th width="20%" rowspan="2" align="right"><div align="left">SKU</div></th>
		<th width="30%" rowspan="2" align="right"><div align="left">Product</div></th>
		<th width="10%" colspan="2" align="right">Kantor Pusat</th>
		<th width="10%" colspan="2" align="right">PPG</th>
		<th width="10%" colspan="2" align="right">HUB Manado-Gorontalo</th>
		<th width="10%" colspan="2" align="right">HUB Jambi</th>
		<th width="10%" colspan="2" align="right">HUB Palu</th>
		<th width="10%" colspan="2" align="right">HUB Makasar</th>
		<th width="10%" colspan="2" align="right">HUB Banjarmasin</th>
		<th width="10%" colspan="2" align="right">HUB Balikpapan</th>
		<th width="10%" colspan="2" align="right">HUB Kendari</th>
		<th width="10%" colspan="2" align="right">HUB Pontianak</th>
		<th width="10%" colspan="2" align="right">HUB Medan</th>
		<th width="10%" colspan="2" align="right">HUB Palangkaraya</th>
	<?php 
	/*
		$query = $this->db->get('warehouse');
		foreach ($query->result() as $key) {
			
	?>
		<th colspan="2" align="right" width="18%"><div align="center"><?php echo $key->name;?>	</div></th>

	<?php }*/ ?>
	</tr>
	<tr>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
		<th align="right" width="4%"><div align="right">Qty</div></th>
		<th align="right" width="12%"><div align="right">Total Price</div></th>
	</tr>

	<tr>
		<td colspan="100%" style="border-bottom:solid thin #000099"></td>
	</tr>
	<?php
	$wh = $this->db->count_all('warehouse'); 
		$item = 0;
		if ($rekap):
			$jml=0;			$total=0;
			$jmlsc=0;		$totsc=0;
			$jmlnc=0;		$totnc=0;
			$jmladjstock=0;		$totadjstock=0;
			$jmlretur=0;	$totretur=0;
			$totalqty1=0;	$totalnominal1=0;
			$totalqty2=0;	$totalnominal2=0;
			$totalqty3=0;	$totalnominal3=0;
			$totalqty4=0;	$totalnominal4=0;
			$totalqty5=0;	$totalnominal5=0;
			$totalqty6=0;	$totalnominal6=0;
			$totalqty7=0;	$totalnominal7=0;
			$totalqty8=0;	$totalnominal8=0;
			$totalqty9=0;	$totalnominal9=0;
			$totalqty10=0;	$totalnominal10=0;
			$totalqty11=0;	$totalnominal11=0;
			$totalqty12=0;	$totalnominal12=0;
			
			$currGroup = '';
			foreach($rekap as $dt1):
			if($currGroup == '') {
				$currGroup =  $dt1['group_name'];
				echo '
				<tr>
					<td colspan="26" style="border-bottom:solid thin #000099"><b>'.$currGroup.'</b></td>
				</tr>
				';
			}else{
				if($currGroup !=  $dt1['group_name']){
					$currGroup =  $dt1['group_name'];
					echo '
					<tr>
						<td colspan="26" style="border-bottom:solid thin #000099;border-top:solid thin #000099"><b>'.$currGroup.'</b></td>
					</tr>
					';
				}
			}
			// if($item!=$dt1['id']){$item = $dt1['id'];}else{$dt1['jmlnc']=0;$dt1['totnc']=0;}
			$totalall = $dt1['totalqty1'] + $dt1['totalqty2'] + $dt1['totalqty3'] + $dt1['totalqty4'] + $dt1['totalqty5'] + $dt1['totalqty6'] + $dt1['totalqty7'] + $dt1['totalqty8'] + $dt1['totalqty9'] + $dt1['totalqty10'] + $dt1['totalqty11'] + $dt1['totalqty12'];
			if ($totalall != 0) {
				# code...
			
	?>
	<tr>
		<td align="left"><?php echo $dt1['id']; 												?></td>
		<td align="left"><?php echo $dt1['nama']; 												?></td>

		<td align="right"><?php echo $dt1['totalqty1']; 		$totalqty1+=$dt1['totalqty1'];		?></td>
		<td align="right"><?php echo $dt1['totalnominal1']; 	$totalnominal1+=$dt1['totalnominal1'];?></td>
		<td align="right"><?php echo $dt1['totalqty2']; 		$totalqty2+=$dt1['totalqty2'];		?></td>
		<td align="right"><?php echo $dt1['totalnominal2']; 	$totalnominal2+=$dt1['totalnominal2'];?></td>
		<td align="right"><?php echo $dt1['totalqty3']; 		$totalqty3+=$dt1['totalqty3'];		?></td>
		<td align="right"><?php echo $dt1['totalnominal3']; 	$totalnominal3+=$dt1['totalnominal3'];?></td>
		<td align="right"><?php echo $dt1['totalqty4']; 		$totalqty4+=$dt1['totalqty4'];		?></td>
		<td align="right"><?php echo $dt1['totalnominal4']; 	$totalnominal4+=$dt1['totalnominal4'];?></td>
		<td align="right"><?php echo $dt1['totalqty5']; 		$totalqty5+=$dt1['totalqty5'];		?></td>
		<td align="right"><?php echo $dt1['totalnominal5']; 	$totalnominal5+=$dt1['totalnominal5'];?></td>
		<td align="right"><?php echo $dt1['totalqty6']; 		$totalqty6+=$dt1['totalqty6'];		?></td>
		<td align="right"><?php echo $dt1['totalnominal6']; 	$totalnominal6+=$dt1['totalnominal6'];?></td>
		<td align="right"><?php echo $dt1['totalqty7']; 		$totalqty7+=$dt1['totalqty7'];		?></td>
		<td align="right"><?php echo $dt1['totalnominal7']; 	$totalnominal7+=$dt1['totalnominal7'];?></td>
		<td align="right"><?php echo $dt1['totalqty8']; 		$totalqty8+=$dt1['totalqty8'];		?></td>
		<td align="right"><?php echo $dt1['totalnominal8']; 	$totalnominal8+=$dt1['totalnominal8'];?></td>
		<td align="right"><?php echo $dt1['totalqty9']; 		$totalqty9+=$dt1['totalqty9'];		?></td>
		<td align="right"><?php echo $dt1['totalnominal9']; 	$totalnominal9+=$dt1['totalnominal9'];?></td>
		<td align="right"><?php echo $dt1['totalqty10']; 		$totalqty10+=$dt1['totalqty10'];		?></td>
		<td align="right"><?php echo $dt1['totalnominal10']; 	$totalnominal10+=$dt1['totalnominal10'];?></td>
		<td align="right"><?php echo $dt1['totalqty11']; 		$totalqty11+=$dt1['totalqty11'];		?></td>
		<td align="right"><?php echo $dt1['totalnominal11']; 	$totalnominal11+=$dt1['totalnominal11'];?></td>
		<td align="right"><?php echo $dt1['totalqty12']; 		$totalqty12+=$dt1['totalqty12'];		?></td>
		<td align="right"><?php echo $dt1['totalnominal12']; 	$totalnominal12+=$dt1['totalnominal12'];?></td>
	</tr>
	<?php
			}
			endforeach;
			setlocale(LC_MONETARY, "en_US");
	?>
	<tr>
		<td align="left" colspan="2"><b><?php echo "TOTAL";			?></b></td>
		<td align="right"><b><?php echo $totalqty1;		?></b></td>
		<td align="right"><b><?php echo number_format($totalnominal1);	?></b></td>
		<td align="right"><b><?php echo $totalqty2;		?></b></td>
		<td align="right"><b><?php echo number_format($totalnominal2);	?></b></td>
		<td align="right"><b><?php echo $totalqty3;		?></b></td>
		<td align="right"><b><?php echo number_format($totalnominal3);	?></b></td>
		<td align="right"><b><?php echo $totalqty4;		?></b></td>
		<td align="right"><b><?php echo number_format($totalnominal4);	?></b></td>
		<td align="right"><b><?php echo $totalqty5;		?></b></td>
		<td align="right"><b><?php echo number_format($totalnominal5);	?></b></td>
		<td align="right"><b><?php echo $totalqty6;		?></b></td>
		<td align="right"><b><?php echo number_format($totalnominal6);	?></b></td>
		<td align="right"><b><?php echo $totalqty7;		?></b></td>
		<td align="right"><b><?php echo number_format($totalnominal7);	?></b></td>
		<td align="right"><b><?php echo $totalqty8;		?></b></td>
		<td align="right"><b><?php echo number_format($totalnominal8);	?></b></td>
		<td align="right"><b><?php echo $totalqty9;		?></b></td>
		<td align="right"><b><?php echo number_format($totalnominal9);	?></b></td>
		<td align="right"><b><?php echo $totalqty10;		?></b></td>
		<td align="right"><b><?php echo number_format($totalnominal10);	?></b></td>
		<td align="right"><b><?php echo $totalqty11;		?></b></td>
		<td align="right"><b><?php echo number_format($totalnominal11);	?></b></td>
		<td align="right"><b><?php echo $totalqty12;		?></b></td>
		<td align="right"><b><?php echo number_format($totalnominal12);	?></b></td>
	</tr>
	<?php
		else:	?>
	<tr>
      <td colspan="100%">Data is not available.</td>
    </tr>
	<?php
		endif;
	?>
	<?php
	/*
		$item = 0;
		if ($rekap):
			$jml=0;			$total=0;
			$jmlsc=0;		$totsc=0;
			$jmlnc=0;		$totnc=0;
			$jmladjstock=0;		$totadjstock=0;
			$jmlretur=0;	$totretur=0;
			$totalqty=0;	$totalnominal=0;
			
			$currGroup = '';
			foreach($rekap as $dt1):
			if($currGroup == '') {
				$currGroup =  $dt1['group_name'];
				echo '
				<tr>
					<td colspan="12" style="border-bottom:solid thin #000099"><b>'.$currGroup.'</b></td>
				</tr>
				';
			}else{
				if($currGroup !=  $dt1['group_name']){
					$currGroup =  $dt1['group_name'];
					echo '
					<tr>
						<td colspan="12" style="border-bottom:solid thin #000099;border-top:solid thin #000099"><b>'.$currGroup.'</b></td>
					</tr>
					';
				}
			}
			// if($item!=$dt1['id']){$item = $dt1['id'];}else{$dt1['jmlnc']=0;$dt1['totnc']=0;}
	?>
	<tr>
		<td align="left"><?php echo $dt1['id']; 												?></td>
		<td align="left"><?php echo $dt1['nama']; 												?></td>
		<?php 
		$item_id = $dt1['id'];
		foreach ($query->result() as $key) {
			$whid = $key->id;
			$totalall = $this->MRekap->omset_ro_product_gross_new_wh_all($this->input->post('fromdate'),$this->input->post('todate'),$whid,$item_id);
			foreach ($totalall as $dt1) {
				# code...
			
		?>
		<td align="right"><?php echo $dt1['totalqty']; 			?></td>
		<td align="right"><?php echo $dt1['totalnominal']; 	?></td>
		<?php }
		} ?>
	</tr>
	<?php
			endforeach;
			setlocale(LC_MONETARY, "en_US");

	?>
	<tr>
		<td align="left" colspan="2"><b><?php echo "TOTAL";			?></b></td>
		<td align="right"><b><?php echo $totalqty;		?></b></td>
		<td align="right"><b><?php echo number_format($totalnominal);	?></b></td>
	</tr>
	<?php
		else:	?>
	<tr>
      <td colspan="100%">Data is not available.</td>
    </tr>
	<?php
		endif; */
	?>
	<tr>
		<td colspan="100%" style="border-bottom:double medium #000099"></td>
	</tr>
</table>

<?php $this->load->view('footer'); ?>
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
</script>
