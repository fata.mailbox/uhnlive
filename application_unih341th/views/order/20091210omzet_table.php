
<table width='100%'>
<?php echo form_open('order/omzet', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  echo form_input($data);?>   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
   echo "to: ".form_input($data);?>
   </td>
  </tr>
 
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
	</tr>
<?php echo form_close();?>				
</table>


<table class="stripe">
	<tr>
      <td colspan="4" style="border-bottom:solid thin #000099"></td>
  </tr>
  <tr>
  	<th align="right" width="25%"><div align="right">RO STC Rp.</div></th>
	<th align="right" width="25%"><div align="right">RO M-STC Rp.</di	v></th>
    <th align="right" width="25%"><div align="right">SO Rp.</div></th>
    <th align="right" width="25%"><div align="right">Total Penjualan</div></th>
  </tr>
  <tr>
      <td colspan="4" style="border-bottom:solid thin #000099"></td>
  </tr>
  <tr>
  	<td align="right"><?php echo $totalro['ftotalharga'];?></b></td>

<td align="right"><?php echo $totalro_mstc['ftotalharga'];?></b></td>
<td align="right"><?php echo $totalso['ftotalharga'];?></b></td>
<td align="right"><?php echo $total;?></b></td>
  </tr>
  	<tr>
      <td colspan="4" style="border-bottom:double medium #000099"></td>
  </tr>

</table>
<br />
<b>RO STC</b>
<table class="stripe">
	<tr>
      <td colspan="6" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <th width='5%'>No.</th>
      <th width='10%'>Invoice No.</th>
      <th width='15%'>Date</th>
      <th width='35%'>ID / Name</th>
      <th width='15%'><div align="right">Total Price</div></th>
      <th width='15%'><div align="right">Total PV</div></th>      
   </tr>
   <tr>
      <td colspan="6" style="border-bottom:solid thin #000099"></td>
  </tr>
<?php
if ($ro):
	foreach($ro as $row): 
?>
    <tr>
      <td><?php echo anchor('order/roapp/view/'.$row['id'],$row['i']);?></td>
      <td><?php echo anchor('order/roapp/view/'.$row['id'],$row['id']);?></td>
      <td><?php echo anchor('order/roapp/view/'.$row['id'],$row['tgl']);?></td>
      <td><?php echo anchor('order/roapp/view/'.$row['id'],$row['no_stc']." / ".$row['namamember']);?></td>
      <td align="right"><?php echo anchor('order/roapp/view/'.$row['id'],$row['ftotalharga']);?></td>
      <td align="right"><?php echo anchor('order/roapp/view/'.$row['id'],$row['ftotalpv']);?></td>
    </tr>
  <?php endforeach; ?>
  
  <tr>
      <td colspan="4" align="right"><b>Grand Total: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="right"><b><?php echo $totalro['ftotalharga'];?></b></td>
      <td align="right"><b><?php echo $totalro['ftotalpv'];?></b></td>
    </tr>
    <tr>
      <td colspan="6" style="border-bottom:double medium #000099"></td>
    </tr>
    
 <?php else: ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<br />
<b>RO M-STC</b>
<table class="stripe">
	<tr>
      <td colspan="6" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <th width='5%'>No.</th>
      <th width='10%'>Invoice No.</th>
      <th width='15%'>Date</th>
      <th width='35%'>ID / Name</th>
      <th width='15%'><div align="right">Total Price</div></th>
      <th width='15%'><div align="right">Total PV</div></th>      
   </tr>
   <tr>
      <td colspan="6" style="border-bottom:solid thin #000099"></td>
  </tr>
<?php
if ($ro_mstc):
	foreach($ro_mstc as $row): 
?>
    <tr>
      <td><?php echo anchor('order/roapp/view/'.$row['id'],$row['i']);?></td>
      <td><?php echo anchor('order/roapp/view/'.$row['id'],$row['id']);?></td>
      <td><?php echo anchor('order/roapp/view/'.$row['id'],$row['tgl']);?></td>
      <td><?php echo anchor('order/roapp/view/'.$row['id'],$row['no_stc']." / ".$row['namamember']);?></td>
      <td align="right"><?php echo anchor('order/roapp/view/'.$row['id'],$row['ftotalharga']);?></td>
      <td align="right"><?php echo anchor('order/roapp/view/'.$row['id'],$row['ftotalpv']);?></td>
    </tr>
  <?php endforeach; ?>
  
  <tr>
      <td colspan="4" align="right"><b>Grand Total: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="right"><b><?php echo $totalro_mstc['ftotalharga'];?></b></td>
      <td align="right"><b><?php echo $totalro_mstc['ftotalpv'];?></b></td>
    </tr>
    <tr>
      <td colspan="6" style="border-bottom:double medium #000099"></td>
    </tr>
    
 <?php else: ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<br />
<b>SO</b>
<table class="stripe">
	<tr>
      <td colspan="6" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <th width='5%'>No.</th>
      <th width='10%'>Invoice No.</th>
      <th width='15%'>Date</th>
      <th width='35%'>ID / Name</th>
      <th width='15%'><div align="right">Total Price</div></th>
      <th width='15%'><div align="right">Total PV</div></th>      
   </tr>
   <tr>
      <td colspan="6" style="border-bottom:solid thin #000099"></td>
  </tr>
<?php
if ($so):
	foreach($so as $row): 
?>
    <tr>
      <td><?php echo anchor('order/so/view/'.$row['id'], $row['i']);?></td>
      <td><?php echo anchor('order/so/view/'.$row['id'], $row['id']);?></td>
      <td><?php echo anchor('order/so/view/'.$row['id'], $row['tgl']);?></td>
      <td><?php echo anchor('order/so/view/'.$row['id'], $row['member_id']." / ".$row['namamember']);?></td>
      <td align="right"><?php echo anchor('order/so/view/'.$row['id'], $row['ftotalharga']);?></td>
      <td align="right"><?php echo anchor('order/so/view/'.$row['id'], $row['ftotalpv']);?></td>
    </tr>
  <?php endforeach; ?>
  
  <tr>
      <td colspan="4" align="right"><b>Grand Total: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="right"><b><?php echo $totalso['ftotalharga'];?></b></td>
      <td align="right"><b><?php echo $totalso['ftotalpv'];?></b></td>
    </tr>
    <tr>
      <td colspan="6" style="border-bottom:double medium #000099"></td>
    </tr>
    
 <?php else: ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>


<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>
