<?php $this->load->view('header');?>
<div class="ViewHold">
	<?php /* update by Boby 20120607 */ ?>
	<div id="companyDetails" align="right">
		<?php
			echo "<p><i>Lampiran B<br>FPSM 7-03-01/02-R01 </i></p>";
			echo "<img src='/images/backend/logo.jpg' border='0'><br>";
		?>
	</div>
	<?php /* end update by Boby 20120607 */?>

	<p>
		<strong>
			<?php echo "No. Invoice: ";?> <?php echo $row['id'];?><br />
			<?php
				echo $row['tgl'];
			?>
		</strong>
		
	</p>
	<br />
			<h2>Invoice</h2><p>
			PT. UNIVERSAL HEALTH NETWORK 
			<i>
			<?php
			if($row['flag_']==1){ 
				echo "(NPWP : 02.313.286.3-004.000)<br>Jl. PULO GADUNG RAYA No. 5 RT/RW 003/003, KIP, CAKUNG JAKARTA TIMUR. DKI JAKARTA RAYA. 13920.";
			}else{
				echo "(NPWP : 02.313.286.3-004.001)<br>MANGGA DUA SQUARE RUKAN BLOK F NO.20.<BR>Jl. GUNUNG SAHARI RAYA No. 1, ANCOL, PADEMANGAN, JAKARTA UTARA. DKI JAKARTA RAYA.";
			}
			?>
			</i>
	<hr />

	<h3><?php echo $row['member_id']," - ".$row['nama'];?></h3>

	<p>
		<?php
			echo "NPWP : ".$npwp['npwp']."<br />";
			if(strlen($npwp['nama'])>0){
				echo $npwp['alamat']."<br />";
			}else{
				echo $row['alamat']."<br />";
			}
			if($row['kota']<>'Unkown'){echo $row['kota']." - ".$row['propinsi']." ".$row['kodepos'];}
		?>
	</p>
	
	<table class="stripe">
	<tr>
      <th width='10%'>Item Code</th>
      <th width='25%'>Item Name</th>
      <th width='10%'><div align="right">Qty</div></th>
	  <th width='10%'><div align="right">PV</div></th>
      <th width='10%'><div align="right">Price*</div></th>
      <th width='15%'><div align="right">Sub Total PV</div></th>
      <th width='20%'><div align="right">Sub Total*</div></th>
    </tr>
    
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><?php echo $item['item_id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td align="right"><?php echo $item['fqty'];?></td>
			<td align="right"><?php echo $item['fpv'];?></td>
			<td align="right"><?php echo $item['fharga'];?></td>
			<td align="right"><?php echo $item['fsubtotalpv'];?></td>
			<td align="right"><?php echo $item['fsubtotal'];?></td>
		</tr>
		<?php endforeach;?>
		<tr>
			<td colspan='5' align='right'><b>Total Rp.</b>&nbsp;</td>
			<td align="right"><b><?php echo $row['ftotalpv'];?></b></td>
			<td align="right"><b><?php echo $row['ftotalharga'];?></b></td>
		</tr>
	</table>
	<i>*)include PPN 10%</i>
	<?php 
	$this->load->view('footer');
?>
