<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>

<div class="ViewHold">
	<?php /* Update by Boby 20120607 */ ?>
	<div id="companyDetails" align="right">
		<?php 
			echo "<p><i>Lampiran C<br>FPSM 7-03-01/03-R01</i></p>";
			echo "<img src='/images/backend/logo.jpg' border='0'><br>";
		?>
	</div>
	<?php /* End Update by Boby 20120607 */ ?>

	<p>
		<strong>
			<?php echo "No. Invoice: ";?> <?php echo $row['id'];?><br />
			<?php
				echo $row['date']; // localized month
				echo "<br /><br />"; // day and year numbers
			?>
		</strong>
		
	</p>
	<br />
			<h2>Invoice Request Order</h2>
	<hr />

	<h3><?php echo $row['no_stc']," - ".$row['nama'];?></h3>

	<p>
		<?php
			echo $row['alamat']."<br />";
			echo $row['kota']." - ".$row['propinsi']." ".$row['kodepos'];
		?>
	</p>
	<p>
		<?php
			echo "No Stc / Nama: <b>".$row['no_stc2']." / ".$row['namastc']."</b><br />";
			echo "Status: ".$row['status']."<br />";
			echo "Delivery Date: ".$row['tglapproved']."<br />";
			echo "Remark Delivered: ".$row['remark'];
		?>
	</p>	
	
	<table class="stripe">
	<tr>
      <th width='10%'>Item Code</th>
      <th width='25%'>Item Name</th>
      <th width='10%'><div align="right">Qty</div></th>
      <th width='10%'><div align="right">PV</div></th>
	  <th width='10%'><div align="right">Price</div></th>      
      <th width='15%'><div align="right">Sub Total PV</div></th>
	  <th width='20%'><div align="right">Sub Total</div></th>      
    </tr>
    
    <?php $counter =0; foreach ($items as $item): $counter = $counter+1;?>
		<tr>
			<td><?php echo $item['item_id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td align="right"><?php echo $item['fqty'];?></td>
			<td align="right"><?php echo $item['fpv'];?></td>
			<td align="right"><?php echo $item['fharga'];?></td>
			<td align="right"><?php echo $item['fsubtotalpv'];?></td>
			<td align="right"><?php echo $item['fsubtotal'];?></td>
		</tr>
		<?php endforeach;
				if($row['diskon']>0){ ?>
		<tr>
			<td colspan='5' align='right'><b>Total Rp.</b>&nbsp;</td>
			<td align="right"><b><?php echo $row['ftotalpv'];?></b></td>
			<td align="right"><b><?php echo $row['ftotalharga2'];?></b></td>			
		</tr>
		<?php
			// updated by Boby 20120227
			$disc = $row['totalharga2']*0.03;
			$discEcb = $row['totalharga2']*0.25;
			if($row['totalharga2'] > ($row['totalharga']+$discEcb)){
		?>
        <tr>
			<td colspan='5' align='right'>Diskon&nbsp;3<?php // echo $row['ftotalharga2'];?>% </td>
			<td colspan="2" align="right">(<?php echo number_format($disc);?>)</td>
		</tr>
		<tr>
			<td colspan='5' align='right'>Diskon ECB&nbsp;25<?php // echo $row['diskon'];?>% </td>
			<td colspan="2" align="right">(<?php echo number_format($discEcb);?>)</td>
		</tr>
		<?php
			}else{ $disc = $row['frpdiskon'];
		?>
		<tr>
			<td colspan='5' align='right'>Diskon&nbsp;<?php echo $row['diskon'];?>% </td>
			<td colspan="2" align="right">(<?php echo ($disc);?>)</td>
		</tr>
		<?php
			}
			// updated by Boby 20120227
		?>
        <?php } ?>
        <tr>
			<td colspan='5' align='right'><b>Total Bayar Rp.</b>&nbsp;</td>
			<td colspan="2" align="right"><b><?php echo $row['ftotalharga'];?></b></td>
		</tr>

	</table>
<?php
	// order -> requestorder_approved_view

	/* Created by Boby 20130822*/
	if($bns){
?>	<b>Get Support Item For Free</b>
	<table class="stripe">
		<tr>
		  <th width='10%'>Item Code</th>
		  <th width='25%'>Item Name</th>
		  <th width='10%'><div align="right">Qty</div></th>
		  <th width='55%'><div align="right">&nbsp;</div></th>
		</tr>
		<?php foreach ($bns as $free): ?>
		<tr>
			<td align="left"><?php echo $free['item_id'];?></td>
			<td align="left"><?php echo $free['prd'];?></td>
			<td align="right"><?php echo $free['qty'];?></td>
			<td align="right">&nbsp;</td>
		</tr>
		<?php endforeach; ?>
	</table>
<?php
	}
	/* End created by Boby 20130822*/
	/* Created by Andrew 20120530*/
	if($flag==0){ // jika admin
	?>
	<table width=100%>
	<tr>
	  <td colspan='1' align=center width='5%'> </td>
	  <td colspan='2' align=center width='25%'><b>Sales Counter</b></td>
	  <td colspan='1' align=center width='5%'> </td>
      <td colspan='2' align=center width='30%'><b>Warehouse</b></td>
	  <td colspan='1' align=center width='5%'> </td>
	  <td colspan='2' align=center width='25%'><b>Receiver</b></td> 	
	  <td colspan='1' align=center width='5%'> </td>
	  
	</tr>
	<tr>
	<td colspan='10'>
	<br><br><br><br><br><br>
	</td>
	</tr>
	
	<tr>
		<td></td>
		<td align='left'>(</td><td align='right'>)</td>
		<td></td>
		<td align='left'>(</td><td align='right'>)</td>
		<td></td>
		<td align='left'>(</td><td align='right'>)</td> 
		<td></td>
	</tr>
	</table>
	<?php 
		}
		/* End created by Andrew 20120530*/
	$this->load->view('footer');
?>
