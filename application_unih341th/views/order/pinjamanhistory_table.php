<link type="text/css" rel="stylesheet" href="<?php echo  site_url() ?>css/soho_tab.css" />

<table width='100%'>
<?php echo form_open('order/hpjm', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d',now())));  echo form_input($data);?>   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
   echo "to: ".form_input($data);?>
   </td>
  </tr>
  
  <?php if($this->session->userdata('group_id') > 100){ ?>
  <tr>
  <td width='24%'>STC No. / Name</td>
		<td width='1%'>:</td>
        <td width='75%'><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('username')." / ".$this->session->userdata('name');?></b></td>
     </tr>   
  
  <?php }else{ ?>
        <tr>
			<td valign='top'>Stockiest No.</td>
			<td valign='top'>:</td>
			<td valign='top'><?php echo form_hidden('member_id',set_value('member_id','')); $data = array('name'=>'no_stc','id'=>'no_stc','size'=>15,'readonly'=>'1','value'=>set_value('no_stc'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('stcsearch/all/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?></td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
         <?php }?>
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
	</tr>
<?php echo form_close();?>				
</table>

<!-- 
Start Modified By Budi 20190129 
Tambahan Untuk Stockiest Consignmnet Payment
-->
<br>
<div class="tabset">
  <!-- Tab 1 -->
  <input type="radio" name="tabset" id="tab1" aria-controls="marzen" checked>
  <label for="tab1">Blm Lunas</label>
  <!-- Tab 2 -->
  <input type="radio" name="tabset" id="tab2" aria-controls="rauchbier">
  <label for="tab2">Lunas/Closed</label>

  
  <div class="tab-panels">
    <section id="marzen" class="tab-panel">
	  <!--
      <p><strong>Overall Impression:</strong> An elegant, malty German amber lager with a clean, rich, toasty and bready malt flavor, restrained bitterness, and a dry finish that encourages another drink. The overall malt impression is soft, elegant, and complex, with a rich aftertaste that is never cloying or heavy.</p>
      <p><strong>History:</strong> As the name suggests, brewed as a stronger “March beer” in March and lagered in cold caves over the summer. Modern versions trace back to the lager developed by Spaten in 1841, contemporaneous to the development of Vienna lager. However, the Märzen name is much older than 1841; the early ones were dark brown, and in Austria the name implied a strength band (14 °P) rather than a style. The German amber lager version (in the Viennese style of the time) was first served at Oktoberfest in 1872, a tradition that lasted until 1990 when the golden Festbier was adopted as the standard festival beer.</p>
	  -->
	  
<table class="stripe">
	<tr>
		<th width='3%'></th>
		<th width='3%'>No.</th>
		<th width='8%'>Invoice No.</th>
		<th width='11%'>Date</th>
		<th width='20%'>Stockiest / Item Name</th>	  
		
		<th width='11%'><div align="right">Total&nbsp;Price</div></th>
		<th width='11%'><div align="right">Total&nbsp;PV</div></th>
		<th width='4%'><div align="right">Qty&nbsp;Item</div></th>
		<th width='5%'><div align="right">Qty&nbsp;Pelunasan</div></th>
		<th width='5%'><div align="right">Cancel</div></th>

		<th width='11%'><div align="right">Pelunasan(Price)</div></th>
		<th width='11%'><div align="right">Pelunasan(PV)</div></th>
		<!--
		<th width='11%'><div align="right">Total Price</div></th>
		<th width='11%'><div align="right">Total PV</div></th>
		-->
		<th width='11%'><div align="right">Sisa(Price)</div></th>
		<th width='11%'><div align="right">Sisa(PV)</div></th>
		<th><div align="right" >Status</div></th>

	  
    </tr>
<?php
if ($sc_payment){
	
	$counter=0;
	$gttl_hrg_lunas=0;
	$gttl_hrg_lunaspv=0;
	$gttl_hrg_retur=0;
	$gttl_hrg_returpv=0;
	$gttl_hrg_sisa=0;
	$gttl_hrg_sisapv=0;
	
	foreach($sc_payment as $row){
		
		
		$counter=$counter+1;
		
		$color_class = ($counter%2 == 0)? '#F3FFF1': '#DAF7A6;';
		
		
		$stat_lunas="Belum Lunas";
		if ($row['qty_total']<1){
			$stat_lunas="Lunas";

		}else{
			$strPay='';
			$ttl_hrg=0;
			$ttl_hrgpv=0;
			$ttl_hrg_lunas=0;
			$ttl_hrg_lunaspv=0;
			$ttl_hrg_retur=0;
			$ttl_hrg_returpv=0;
			$ttl_hrg_sisa=0;
			$ttl_hrg_sisapv=0;
			
			$pay_Id=$this->MRekap->getPayment($row['id']);
			if ($pay_Id){
				
				foreach ($pay_Id as $pay_Id_item){
					
					$ttl_hrg=$ttl_hrg+str_replace(",","",$pay_Id_item['jmlharga']);
					$ttl_hrgpv=$ttl_hrgpv+str_replace(",","",$pay_Id_item['jmlpv']);
					$ttl_hrg_lunas=$ttl_hrg_lunas+str_replace(",","",$pay_Id_item['total_pay_lunas']);
					$ttl_hrg_lunaspv=$ttl_hrg_lunaspv+str_replace(",","",$pay_Id_item['total_pay_lunaspv']);
					$ttl_hrg_retur=$ttl_hrg_retur+str_replace(",","",$pay_Id_item['total_retur']);
					$ttl_hrg_returpv=$ttl_hrg_returpv+str_replace(",","",$pay_Id_item['total_returpv']);
					$ttl_hrg_sisa=$ttl_hrg_sisa+str_replace(",","",$pay_Id_item['total_sisa']);
					$ttl_hrg_sisapv=$ttl_hrg_sisapv+str_replace(",","",$pay_Id_item['total_sisapv']);
					
					$strPay=$strPay.'
					<tr class="xChild'.$row['id'].'" style="background:none;display:none;">
						<td style="background:none;"></td>
						<td style="background:none;"></td>
						<td style="background:none;" align="right">'.$pay_Id_item['id'].'</td>
						<td style="background:none;" colspan="2" >'.$pay_Id_item['nama_item'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['harga'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['pv'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['qty'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['qty_lunas'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['qty_retur'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['total_pay_lunas'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['total_pay_lunaspv'].'</td>
						<!--
						<td align="right" style="background:none;">'.$pay_Id_item['total_retur'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['total_returpv'].'</td>
						-->
						<td align="right" style="background:none;">'.$pay_Id_item['total_sisa'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['total_sisapv'].'</td>
						<td align="right" align="right" style="background:none;"></td>
					</tr>';
				}
			}
			
			
			$free_item=$this->MRekap->get_FreeItem($row['id']);
			if ($free_item){
				foreach ($free_item as $si_free_item){
					$strPay=$strPay.'
					<tr class="xChild'.$row['id'].'" style="background:none;display:none;">
						<td style="background:none;"></td>
						<td style="background:none;"></td>
						<td style="background:none;" align="right">'.$si_free_item['free_item_id'].'</td>
						<td style="background:none;" colspan="2" >'.$si_free_item['Free_Item_Name'].'</td>
						<td style="background:none;" colspan="4">Free Item</td>
						<!--
						<td align="right" style="background:none;"  ></td>
						<td align="right" style="background:none;">'.$si_free_item['qty'].'</td>
						<td align="right" style="background:none;">'.$si_free_item['qty_lunas'].'</td>
						<td align="right" style="background:none;">'.$si_free_item['qty_retur'].'</td>
						-->
						<td align="right" style="background:none;"></td>
						<td align="right" style="background:none;"></td>
						<!--
						<td align="right" style="background:none;"></td>
						<td align="right" style="background:none;"></td>
						-->
						<td align="right" style="background:none;"></td>
						<td align="right" style="background:none;"></td>
						<td align="right" align="right" style="background:none;"></td>
						<td align="right" align="right" style="background:none;"></td>
					</tr>';
				}
			}
			
			
			
			echo '
			<tr id="trHead'.$counter.'" style="background:'.$color_class.';">
				<td style="">
					<div id="infoToggler_h'.$row['id'].'" onClick=showChild("'.$row['id'].'"); style="cursor:pointer;">
						
						<img src="'.site_url().'images/backend/tree_folder.gif">
					</div>
					
				</td>
				<td style="">
					'.$counter.'
				</td>
				<td style="" align="left">SC : '.$row['id'].'</td>
				<td style="">'.$row['tgl'].'</td>
				<td style="">'.$row['nama'].'</td>
				<td align="right" style="">'.$row['totalharga'].'</td>
				<td align="right" style="">'.$row['totalpv'].'</td>
				<td align="right" style="">'.$row['qty'].'</td>
				<td align="right" style="display:none;"></td>
				<td align="right" style="">'.$row['qty_lunas'].'</td>
				<td align="right" style="">'.$row['qty_retur'].'</td>
				<td align="right" style="">'.number_format($ttl_hrg_lunas).'</td>
				<td align="right" style="">'.number_format($ttl_hrg_lunaspv).'</td>
				<!--
				<td align="right" style="">'.number_format($ttl_hrg_retur).'</td>
				<td align="right" style="">'.number_format($ttl_hrg_returpv).'</td>
				-->
				<td align="right" style="">'.number_format($ttl_hrg_sisa).'</td>
				<td align="right" style="">'.number_format($ttl_hrg_sisapv).'</td>
				<td align="right" style="">'.$stat_lunas.'</td>
			</tr>
			';
			echo $strPay;
			
			$gttl_hrg_lunas=$gttl_hrg_lunas+$ttl_hrg_lunas;
			$gttl_hrg_lunaspv=$gttl_hrg_lunaspv+$ttl_hrg_lunaspv;
			$gttl_hrg_retur=$gttl_hrg_retur+$ttl_hrg_retur;
			$gttl_hrg_returpv=$gttl_hrg_returpv+$ttl_hrg_returpv;
			$gttl_hrg_sisa=$gttl_hrg_sisa+$ttl_hrg_sisa;
			$gttl_hrg_sisapv=$gttl_hrg_sisapv+$ttl_hrg_sisapv;
					
		}
		
	}
	echo '
	<tr class="footer" style="border-top:solid thin #000099;;">
		<td colspan="100%" >&nbsp;</td>
	</tr>
	<tr class="footer" >
		<td  align="right" style="background:none;" colspan="10" ><b>Grand Total</b></td>
		<!--
		<td align="right" style="background:none;"></td>
		<td align="right" style="background:none;"></td>
		<td align="right" style="background:none;"></td>
		<td align="right" style="background:none;"></td>
		<td align="right" style="background:none;"></td>
		-->
		<td align="right" style="background:none;"><b>'.number_format($gttl_hrg_lunas).'</b></td>
		<td align="right" style="background:none;"><b>'.number_format($gttl_hrg_lunaspv).'</b></td>
		<!--
		<td align="right" style="background:none;"></td>
		<td align="right" style="background:none;"></td>
		-->
		<td align="right" style="background:none;">'.number_format($gttl_hrg_sisa).'</td>
		<td align="right" style="background:none;">'.number_format($gttl_hrg_sisapv).'</td>
		<td align="right" align="right" style="background:none;"></td>
	</tr>
	<tr style="border-bottom:double medium #000099;">
		<td colspan="100%" >&nbsp;</td>
	</tr>';
	
}else{
	echo '
	<tr>
      <td colspan="100%">Data is not available.</td>
    </tr>
	';
}
?>
</table>

	</section>
    <section id="rauchbier" class="tab-panel">
	<!--
      <h2>6B. Rauchbier</h2>
      <p><strong>Overall Impression:</strong>  An elegant, malty German amber lager with a balanced, complementary beechwood smoke character. Toasty-rich malt in aroma and flavor, restrained bitterness, low to high smoke flavor, clean fermentation profile, and an attenuated finish are characteristic.</p>
      <p><strong>History:</strong> A historical specialty of the city of Bamberg, in the Franconian region of Bavaria in Germany. Beechwood-smoked malt is used to make a Märzen-style amber lager. The smoke character of the malt varies by maltster; some breweries produce their own smoked malt (rauchmalz).</p>
    -->
	<h4>Data Pelunasan Consignment</h4>
	<h2><?php echo $this->input->post('name') ?></h2>
    
	<!-- Table Lunas -->
<table class="stripe">
	<tr>
		<th width='3%'></th>
		<th width='3%'>No.</th>
		<th width='8%'>Invoice No.</th>
		<th width='11%'>Date</th>
		<th width='20%'>Stockiest / Item Name</th>	  
		
		<th width='11%'><div align="right">Total&nbsp;Price</div></th>
		<th width='11%'><div align="right">Total&nbsp;PV</div></th>
		<th width='4%'><div align="right">Qty&nbsp;Item</div></th>
		<th width='5%'><div align="right">Qty&nbsp;Pelunasan</div></th>
		<th width='5%'><div align="right">Cancel</div></th>

		<th width='11%'><div align="right">Pelunasan(Price)</div></th>
		<th width='11%'><div align="right">Pelunasan(PV)</div></th>
		<!--
		<th width='11%'><div align="right">Total Price</div></th>
		<th width='11%'><div align="right">Total PV</div></th>
		-->
		<th width='11%'><div align="right">Sisa(Price)</div></th>
		<th width='11%'><div align="right">Sisa(PV)</div></th>
		<th><div align="right" >Status</div></th>

	  
    </tr>
<?php
$counter=0;
if ($sc_payment_lunas){
	$counter=0;
	$gttl_hrg_lunas=0;
	$gttl_hrg_lunaspv=0;
	$gttl_hrg_retur=0;
	$gttl_hrg_returpv=0;
	$gttl_hrg_sisa=0;
	$gttl_hrg_sisapv=0;
	
	foreach($sc_payment_lunas as $row){
		
		$counter=$counter+1;
		$color_class = ($counter%2 == 0)? '#F3FFF1': '#DAF7A6;';
		
		$stat_lunas="Belum Lunas";
		if ($row['qty_total']<1){
			$stat_lunas="Lunas";
			$strPay='';
			$ttl_hrg=0;
			$ttl_hrgpv=0;
			$ttl_hrg_lunas=0;
			$ttl_hrg_lunaspv=0;
			$ttl_hrg_retur=0;
			$ttl_hrg_returpv=0;
			$ttl_hrg_sisa=0;
			$ttl_hrg_sisapv=0;
			
			$pay_Id=$this->MRekap->getPayment($row['id']);
			if ($pay_Id){
				
				foreach ($pay_Id as $pay_Id_item){
					
					$ttl_hrg=$ttl_hrg+str_replace(",","",$pay_Id_item['jmlharga']);
					$ttl_hrgpv=$ttl_hrgpv+str_replace(",","",$pay_Id_item['jmlpv']);
					$ttl_hrg_lunas=$ttl_hrg_lunas+str_replace(",","",$pay_Id_item['total_pay_lunas']);
					$ttl_hrg_lunaspv=$ttl_hrg_lunaspv+str_replace(",","",$pay_Id_item['total_pay_lunaspv']);
					$ttl_hrg_retur=$ttl_hrg_retur+str_replace(",","",$pay_Id_item['total_retur']);
					$ttl_hrg_returpv=$ttl_hrg_returpv+str_replace(",","",$pay_Id_item['total_returpv']);
					$ttl_hrg_sisa=$ttl_hrg_sisa+str_replace(",","",$pay_Id_item['total_sisa']);
					$ttl_hrg_sisapv=$ttl_hrg_sisapv+str_replace(",","",$pay_Id_item['total_sisapv']);
					
					$item_freeQty=0;
					if ($pay_FreeItem){
						foreach ($pay_FreeItem as $pay_FreeItem_item){
							if ($pay_FreeItem_item['pinjaman_d_id']==$pay_Id_item['pinjaman_d_id']){
								$item_freeQty=$pay_FreeItem_item['qty_lunas'];
							}
						}
					}
					
					$strPay=$strPay.'
					<tr class="xChild'.$row['id'].'" style="background:none;display:none;">
						<td style="background:none;"></td>
						<td style="background:none;"></td>
						<td style="background:none;" align="right">'.$pay_Id_item['id'].'</td>
						<td style="background:none;" colspan="2" >'.$pay_Id_item['nama_item'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['harga'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['pv'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['qty'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['qty_lunas'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['qty_retur'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['total_pay_lunas'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['total_pay_lunaspv'].'</td>
						<!--
						<td align="right" style="background:none;">'.$pay_Id_item['total_retur'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['total_returpv'].'</td>
						-->
						<td align="right" style="background:none;">'.$pay_Id_item['total_sisa'].'</td>
						<td align="right" style="background:none;">'.$pay_Id_item['total_sisapv'].'</td>
						<td align="right" align="right" style="background:none;"></td>
					</tr>';
				}
			}
			
			$free_item=$this->MRekap->get_FreeItem($row['id']);
			if ($free_item){
				foreach ($free_item as $si_free_item){
					$strPay=$strPay.'
					<tr class="xChild'.$row['id'].'" style="background:none;display:none;">
						<td style="background:none;"></td>
						<td style="background:none;"></td>
						<td style="background:none;" align="right">'.$si_free_item['free_item_id'].'</td>
						<td style="background:none;" colspan="2" >'.$si_free_item['Free_Item_Name'].'</td>
						<td style="background:none;" colspan="4">Free Item</td>
						<!--
						<td align="right" style="background:none;"  ></td>
						<td align="right" style="background:none;">'.$si_free_item['qty'].'</td>
						<td align="right" style="background:none;">'.$si_free_item['qty_lunas'].'</td>
						<td align="right" style="background:none;">'.$si_free_item['qty_retur'].'</td>
						-->
						<td align="right" style="background:none;"></td>
						<td align="right" style="background:none;"></td>
						<!--
						<td align="right" style="background:none;"></td>
						<td align="right" style="background:none;"></td>
						-->
						<td align="right" style="background:none;"></td>
						<td align="right" style="background:none;"></td>
						<td align="right" align="right" style="background:none;"></td>
						<td align="right" align="right" style="background:none;"></td>
					</tr>';
				}
			}
			
			echo '
			<tr id="trHead'.$counter.'" style="background:'.$color_class.'">
				<td>
					
					<div id="infoToggler_h'.$row['id'].'" onClick=showChild("'.$row['id'].'"); style="cursor: pointer;">
						<img src="'.site_url().'images/backend/tree_folder.gif">
					</div>
					
				</td>
				<td style="color:#000;">
					'.$counter.'
				</td>
				<td align="left">SC : '.$row['id'].'</td>
				<td >'.$row['tgl'].'</td>
				<td >'.$row['nama'].'</td>
				<td align="right" >'.$row['totalharga'].'</td>
				<td align="right" >'.$row['totalpv'].'</td>
				<td align="right" >'.$row['qty'].'</td>
				<td align="right" style="color:#000;display:none;"></td>
				<td align="right" >'.$row['qty_lunas'].'</td>
				<td align="right" >'.$row['qty_retur'].'</td>
				<td align="right" >'.number_format($ttl_hrg_lunas).'</td>
				<td align="right" >'.number_format($ttl_hrg_lunaspv).'</td>
				<!--
				<td align="right" >'.number_format($ttl_hrg_retur).'</td>
				<td align="right" >'.number_format($ttl_hrg_returpv).'</td>
				-->
				<td align="right" >'.number_format($ttl_hrg_sisa).'</td>
				<td align="right" >'.number_format($ttl_hrg_sisapv).'</td>
				<td align="right" >'.$stat_lunas.'</td>
			</tr>
			';
			echo $strPay;
			$gttl_hrg_lunas=$gttl_hrg_lunas+$ttl_hrg_lunas;
			$gttl_hrg_lunaspv=$gttl_hrg_lunaspv+$ttl_hrg_lunaspv;
			$gttl_hrg_retur=$gttl_hrg_retur+$ttl_hrg_retur;
			$gttl_hrg_returpv=$gttl_hrg_returpv+$ttl_hrg_returpv;
			$gttl_hrg_sisa=$gttl_hrg_sisa+$ttl_hrg_sisa;
			$gttl_hrg_sisapv=$gttl_hrg_sisapv+$ttl_hrg_sisapv;
			
		}

		
		
	}
	echo '
	<tr class="footer" style="border-top:solid thin #000099;;">
		<td colspan="100%" >&nbsp;</td>
	</tr>
	<tr class="footer" >
		<td  align="right" style="background:none;" colspan="10" ><b>Grand Total</b></td>
		<!--
		<td align="right" style="background:none;"></td>
		<td align="right" style="background:none;"></td>
		<td align="right" style="background:none;"></td>
		<td align="right" style="background:none;"></td>
		<td align="right" style="background:none;"></td>
		-->
		<td align="right" style="background:none;"><b>'.number_format($gttl_hrg_lunas).'</b></td>
		<td align="right" style="background:none;"><b>'.number_format($gttl_hrg_lunaspv).'</b></td>
		<!--
		<td align="right" style="background:none;"></td>
		<td align="right" style="background:none;"></td>
		-->
		<td align="right" style="background:none;">'.number_format($gttl_hrg_sisa).'</td>
		<td align="right" style="background:none;">'.number_format($gttl_hrg_sisapv).'</td>
		<td align="right" align="right" style="background:none;"></td>
	</tr>
	<tr style="border-bottom:double medium #000099;">
		<td colspan="100%" >&nbsp;</td>
	</tr>';
}else{
	echo '
	<tr>
      <td colspan="100%">Data is not available.</td>
    </tr>
	';
}
?>
</table>

	</section>

  </div>
  
</div>



<!-- 
End Modified By Budi 20190129 
Tambahan Untuk Stockiest Consignmnet Payment
-->

<table class="stripe">
	<tr>
      <td colspan="8"><b>Saldo Pinjaman Product uptodate</b></td>
  </tr>
	<tr>
      <td colspan="8" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <th width='5%'>No.</th>
      <th width='10%'>Product ID.</th>
      <th width='25%'>Product Name</th>
      <th width='10%'><div align="right">Price</div></th>
      <th width='10%'><div align="right">PV</div></th>
      <th width='10%'><div align="right">Quantity</div></th>
      <th width='15%'><div align="right">Jumlah Harga</div></th>
      <th width='15%'><div align="right">Jumlah PV</div></th>
   </tr>
   	<tr>
      <td colspan="8" style="border-bottom:solid thin #000099"></td>
  </tr>

   
<?php
if ($saldopinjaman):
	foreach($saldopinjaman as $row): 
?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['item_id'];?></td>
      <td><?php echo $row['name'];?></td>
      <td align="right"><?php echo $row['fharga'];?></td>
      <td align="right"><?php echo $row['fpv'];?></td>
      <td align="right"><?php echo $row['fqty'];?></td>
      <td align="right"><?php echo $row['ftotalharga'];?></td>
      <td align="right"><?php echo $row['ftotalpv'];?></td>
    </tr>
     
  
  <?php endforeach; ?>
  <tr>
      <td colspan="8" style="border-bottom:solid thin #000099"></td>
  </tr>
  <tr>
      <td colspan="6" align="right"><b>Grand Total: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="right"><b><?php echo $totalsaldo['ftotalharga'];?></b></td>
      <td align="right"><b><?php echo $totalsaldo['ftotalpv'];?></b></td>
    </tr>
    <tr>
      <td colspan="8" style="border-bottom:double medium #000099"></td>
    </tr>
 <?php else: ?>
    <tr>
      <td colspan="8">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<table class="stripe">
	<tr>
      <td colspan="8"><b>Rekap Pinjaman Stockiest</b></td>
  </tr>
	<tr>
      <td colspan="8" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <th width='8%'>No.</th>
      <th width='15%'>Date</th>
      <th width='10%'>No. Ref</th>
      <th width='18%'><div align="right">Pinjaman</div></th>
      <th width='18%'><div align="right">Pelunasan</div></th>
      <th width='18%'><div align="right">Saldo</div></th>
      <th width='13%'>User ID</th>
   </tr>
   	<tr>
      <td colspan="7" style="border-bottom:solid thin #000099"></td>
  </tr>

   
<?php
if ($pinjamanhistory): ?>
<tr>
      <td>1</td>
      <td colspan='4'>Saldo awal pinjaman</td>
      <td><strong><?php echo $pinjamanhistory[0]['fawal'];?></strong></td>
		<td>&nbsp;</td>      
     </tr>
     
<?php foreach($pinjamanhistory as $row): ?>
    <tr>
      <td><?php echo $row['i'];?><?php if($row['remark']) echo "*";?></td>
      <td><?php echo $row['tgl'];?></td>
      <td><?php echo $row['noref'];?></td>
      <td align="right"><?php echo $row['fpinjam'];?></td>
      <td align="right"><?php echo $row['flunas'];?></td>
      <td align="right"><?php echo $row['fakhir'];?></td>
      <td><?php echo $row['createdby'];?></td>
    </tr>
     
  
  <?php endforeach; ?>
  <tr>
      <td colspan="7" style="border-bottom:solid thin #000099"></td>
  </tr>
  <tr>
      <td colspan="3" align="right"><b>Total: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align="right"><b><?php echo $totalhistory['ftotalpinjam'];?></b></td>
      <td align="right"><b><?php echo $totalhistory['ftotallunas'];?></b></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="7" style="border-bottom:double medium #000099"></td>
    </tr>
    <tr>
      <td colspan="7">* = Pelunasan melalui retur pinjaman</td>
    </tr>
 <?php else: ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<table class="stripe">
	<tr>
      <td colspan="9"><b>History Pinjaman Product</b></td>
  </tr>
	<tr>
      <td colspan="9" style="border-bottom:solid thin #000099"></td>
  </tr>
	<tr>
	  <th width='5%'>No.</th>
      <th width='25%'>ID / Product Name</th>
      <th width='10%'><div align="right">Price</div></th>
      <th width='10%'>Tanggal</th>
      <th width='10%'><div align="right">Saldo Awal</div></th>
      <th width='10%'><div align="right">Saldo In</div></th>
      <th width='10%'><div align="right">Saldo Out</div></th>
      <th width='10%'><div align="right">Saldo Akhir</div></th>
     <th width='10%'>Userid</th>
   </tr>
   	<tr>
      <td colspan="9" style="border-bottom:solid thin #000099"></td>
  </tr>

   
<?php
if ($stock):
	foreach($stock as $row): 
?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['item_id']." / ".$row['name'];?></td>
      <td align="right"><?php echo $row['fharga'];?></td>
      <td><?php echo $row['ftgl'];?></td>
      <td align="right"><?php echo $row['fawal'];?></td>
      <td align="right"><?php echo $row['fin'];?></td>
      <td align="right"><?php echo $row['fout'];?></td>
      <td align="right"><?php echo $row['fakhir'];?></td>
            <td><?php echo $row['createdby'];?></td>
    </tr>
     
  
  <?php endforeach; ?>
    <tr>
      <td colspan="9" style="border-bottom:double medium #000099"></td>
    </tr>
 <?php else: ?>
    <tr>
      <td colspan="9">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>


<script type="text/javascript" src="<?php echo site_url() ?>js/jquery.js"></script>
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });




function showChild(id){
	if ($(".xChild"+id).is(":visible")){
		 $(".xChild"+id).hide();
		 $("#infoToggler_h"+id).find('img').attr("src","<?php echo site_url() ?>images/backend/tree_folder.gif");
	}else{
		$(".xChild"+id).show();
		$("#infoToggler_h"+id).find('img').attr("src","<?php echo site_url() ?>images/backend/tree_textfolder.gif");
	}
	
	/*
	$("#infoToggler_h"+id).find('img').toggle(
	   function () {
		  $(".xChild"+id).show();
		  $("#infoToggler_h"+id).find('img').attr("src","<?php echo site_url() ?>images/backend/tree_textfolder.gif");
	   },
	   function () {
		  $(".xChild"+id).hide();
		  $("#infoToggler_h"+id).find('img').attr("src","<?php echo site_url() ?>images/backend/tree_folder.gif");
	   }
	);
	*/
}


</script>
