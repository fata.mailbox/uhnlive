<?php
	$this->load->view('header');
	$disc = (100- $disk ) / 100;
	$disc_ = ($disk ) / 100;
?>
<h2><?php echo $page_title;?></h2>
	
	 <?php echo form_open('order/ro/create', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
		
		<table width='100%'>
		<tr>
			<td width='20%'>date</td>
			<td width='1%'>:</td>
			<td width='79%'><?php echo date('Y-m-d');?></td> 
		</tr>
		<tr>
			<td>name</td>
			<td>:</td>
			<td></b><?php echo $this->session->userdata('userid')." / ".$this->session->userdata('name');?></td> 
		</tr>
		<tr>
			<td>saldo ewallet STC Rp.</td>
			<td>:</td>
			<td><b><?php echo $row->fewallet;?></b></td> 
		</tr>
        <?php 
			echo form_hidden('member_id','0'); 
			echo form_hidden('diskon',$disk);
		?>
        <tr>
			<td valign='top'>remark</td>
			<td valign='top'>:</td>
			<td><?php $data = array('name'=>'remark','id'=>'remark','rows'=>2, 'cols'=>'30','tabindex'=>'1','value'=>set_value('remark'));
    					echo form_textarea($data);?></td> 
		</tr>
		
		</table>
		<table width='100%'>	
		<tr>
			<td width='18%'>Item Code</td>
			<td width='23%'>Item Name</td>
			<td width='8%'>Qty</td>
			<td width='12%'>Price</td>
			<td width='9%'>PV</td>
			<td width='13%'>Sub Total Price</td>
			<td width='13%'>Sub Total PV</td>
			<td width='5%'>Del?</td>
		</tr>
		
		<?php
		for($i=0;$i<10;$i++){
		?>
		<tr>
			<td valign='top'>
				<?php 
					$data = array('name'=>'itemcode'.$i,'id'=>'itemcode'.$i,'size'=>'8','readonly'=>'1','value'=>set_value('itemcode'.$i)); echo form_input($data); 
					$atts = array('width' => '600', 'height' => '500', 'scrollbars' => 'yes', 'status' => 'yes', 'resizable' => 'no', 'screenx' => '0', 'screeny' => '0');
					echo anchor_popup('itemrosearch/index/'.$i, '<input class="button" type="button" tabindex="2" name="Button" value="browse" />', $atts);
					echo form_hidden('titipan_id'.$i,set_value('titipan_id'.$i,0));
					echo form_hidden('bv'.$i,set_value('bv'.$i,0));
					echo form_hidden('subtotalbv'.$i,set_value('subtotalbv'.$i,0));
					echo form_hidden('diskon'.$i,set_value('diskon'.$i,0));
				?>
			<td valign='top'>
				<input type="text" name="itemname<?php echo $i;?>" id="itemname<?php echo $i;?>" value="<?php echo set_value('itemname'.$i);?>" readonly="1" size="24" />
			</td>
			<td>
				<input class='textbold aright' type="text" name="qty<?php echo $i;?>" id="qty<?php echo $i;?>" value="<?php echo set_value('qty'.$i,0);?>" maxlength="12" size="3" tabindex="3" autocomplete="off" 
					onkeyup="	this.value=formatCurrency(this.value);
					jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
					document.form.total.value=total_curr(10,'document.form.subtotal');
					jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
					document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');
					jumlah(document.form.qty<?php echo $i;?>,document.form.bv<?php echo $i;?>,document.form.subtotalbv<?php echo $i;?>);
					document.form.totalbv.value=totalbv_curr(10,'document.form.subtotalbv');
					if(document.form.subtotalpv<?php echo $i;?>.value == 0){
						document.form.diskon<?php echo $i;?>.value = 0;
					}else{
						document.form.diskon<?php echo $i;?>.value = ReplaceDoted(document.form.subtotal<?php echo $i;?>.value)*(document.form.diskon.value / 100);
					}
					document.form.disc.value = total_disc(10, 'document.form.diskon');
					totalbayardiskon(document.form.total,document.form.disc,document.form.payment);
				">
			</td>
			<td>
				<input class="aright" type="text" name="price<?php echo $i;?>" id="price<?php echo $i;?>" readonly="1" value="<?php echo set_value('price'.$i,0);?>" size="8"
					onkeyup="this.value=formatCurrency(this.value); 
					jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
					document.form.total.value=total_curr(10,'document.form.subtotal');
					jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
					document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');
					jumlah(document.form.qty<?php echo $i;?>,document.form.bv<?php echo $i;?>,document.form.subtotalbv<?php echo $i;?>);
					document.form.totalbv.value=totalbv_curr(10,'document.form.subtotalbv');
					
					document.form.diskon<?php echo $i;?>.value= ReplaceDoted(document.form.subtotal<?php echo $i;?>.value)*(document.form.diskon.value / 100);
					document.form.disc.value = total_disc(10, 'document.form.diskon');
					totalbayardiskon(document.form.total,document.form.disc,document.form.payment);
				">
			</td>
			<td>
				<input class="aright" type="text" name="pv<?php echo $i;?>" id="pv<?php echo $i;?>" readonly="1" value="<?php echo set_value('pv'.$i,0);?>" size="5"
					onkeyup="this.value=formatCurrency(this.value); 
					jumlah(document.form.qty<?php echo $i;?>,document.form.price<?php echo $i;?>,document.form.subtotal<?php echo $i;?>);
					document.form.total.value=total_curr(10,'document.form.subtotal');
					jumlah(document.form.qty<?php echo $i;?>,document.form.pv<?php echo $i;?>,document.form.subtotalpv<?php echo $i;?>);
					document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');
					jumlah(document.form.qty<?php echo $i;?>,document.form.bv<?php echo $i;?>,document.form.subtotalbv<?php echo $i;?>);
					document.form.totalbv.value=totalbv_curr(10,'document.form.subtotalbv');
					
					document.form.diskon<?php echo $i;?>.value= ReplaceDoted(document.form.subtotal<?php echo $i;?>.value)*(document.form.diskon.value / 100);
					document.form.disc.value = total_disc(10, 'document.form.diskon');
					totalbayardiskon(document.form.total,document.form.disc,document.form.payment);
				">
			</td>
			<td>
				<input class="aright" type="text" name="subtotal<?php echo $i;?>" id="subtotal<?php echo $i;?>" value="<?php echo set_value('subtotal'.$i,0);?>" readonly="1" size="12">
			</td>
			<td>
				<input class="aright" type="text" name="subtotalpv<?php echo $i;?>" id="subtotalpv<?php echo $i;?>" value="<?php echo set_value('subtotalpv'.$i,0);?>" readonly="1" size="10">
			</td>
			<td>
				<img alt="delete" onclick="cleartext10(
					document.form.itemcode<?php echo $i;?>
					, document.form.itemname<?php echo $i;?>
					, document.form.qty<?php echo $i;?>
					, document.form.price<?php echo $i;?>
					, document.form.pv<?php echo $i;?>
					, document.form.subtotal<?php echo $i;?>
					, document.form.subtotalpv<?php echo $i;?>
					, document.form.titipan_id<?php echo $i;?>
					, document.form.bv<?php echo $i;?>
					, document.form.subtotalbv<?php echo $i;?>
				); 
				document.form.total.value=total_curr(10,'document.form.subtotal');
				document.form.totalpv.value=totalpv_curr(10,'document.form.subtotalpv');
				document.form.totalbv.value=totalbv_curr(10,'document.form.subtotalbv');
				
				document.form.diskon<?php echo $i;?>.value= ReplaceDoted(document.form.subtotal<?php echo $i;?>.value)*(document.form.diskon.value / 100);
				document.form.disc.value = total_disc(10, 'document.form.diskon');
				totalbayardiskon(document.form.total,document.form.disc,document.form.payment);
				"
				src="<?php echo  base_url();?>images/backend/delete.png" border="0"/>
			</td>
		</tr>
        <?php } ?>
		
		<tr>
			<td colspan='5'></td>
			<td><input class='textbold aright' type="text" name="totalpv" id="totalpv" value="<?php echo set_value('totalpv',0);?>" readonly="1" size="9"><?php echo form_hidden('totalbv',set_value('totalbv',0));?></td>
			<td><input class='textbold aright' type="text" name="total" id="total" value="<?php echo set_value('total',0);?>" readonly="1" size="11"></td>
		</tr>
		<tr>
			<td colspan='5'></td>
			<td>Discount <?php // echo $disk."%"; ?></td>
			<td><input class='textbold aright' type="text" name="disc" id="disc" value="<?php echo set_value('disc',0);?>" readonly="1" size="11"></td>
		</tr>
		<tr>
			<td colspan='5'></td>
			<td>Total Payment </td>
			<td><input class='textbold aright' type="text" name="payment" id="payment" value="<?php echo set_value('payment',0);?>" readonly="1" size="11"></td>
		</tr>
        <?php if(validation_errors()){?>
		<tr>
        	<td colspan='6'>&nbsp;</td>
			<td colspan='2'><span class="error"><?php echo form_error('total');?></span></td>
		</tr>
		<?php }?>
				
		<tr>
			<td colspan='7' align='right' valign='top'>PIN : <input type="password" name="pin" id="pin" value="" maxlength="50" size="12" tabindex="22"/> 
			<span class="error">* <?php echo form_submit('submit', 'Submit', 'tabindex="23"');?><?php echo form_error('pin'); ?></span></td>
			<td>&nbsp;</td>
		</tr>
		
		</table>
		
<?php echo form_close();?>

<?php $this->load->view('footer');?>
