<div id="box_content">
        	<div id="menu_left">
            	<div id="menu_left_box">
                	<div id="menu_left_det">
                    	<div id="menu_left_judul">
                        	<div class="box_subtitle">OPPORTUNITY</div>
                        </div>
                        <div class="menu">
                            <div class="menu_uli"><a href="<?=site_url();?>opportunity">Why Join UHN</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>opportunity/started">Getting Started</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>event_training">Event &amp; Training</a></div>
                            <div class="menu_uli" style="height:90px; background-color:#FFF;">
                            	<div class="menu_uli_sub">Gallery &amp; News</div>
                                <div class="menu_uli_sub2">
                                	<a href="<?=site_url();?>news">News</a>
                                </div>
                                <div class="menu_uli_sub2">
                                	<a href="<?=site_url();?>gallery">Gallery</a>
                                </div>
                            </div>
                            <div class="menu_uli"><a href="<?=site_url();?>faq">FAQ</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>recognition">Recognition</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>marketing_plan">Marketing Plan</a></div>
                        </div>
                    </div>
                </div>
            </div>
                    
            <div id="menu_right">
            	<div id="menu_right_banner">
                	<img src="<?=base_url();?>images/banner_opportunity.jpg"  />
                </div>
                <div id="menu_right_content">
                	<div id="menu_right_title">NEWS</div>
                	<hr />
                </div>
                <div id="menu_right_mv">
                	<ul class="box_testi">
                    	<?php if($results){
						foreach($results as $key => $row){ ?>
                    	<li>
                        	<div class="imgTesti">
                            	<?php if($row['file'])$img = "userfiles/news/".$row['file']; else $img = "images/img_news.jpg";?>
                            	<a href="<?=site_url();?>news/detail/<?=$row['id'];?>"><img src="<?=base_url().$img;?>" class="imgOpacity"/></a>
                            </div>
                            <div class="infoTesti">
                            	<div class="iconEmail"><!-- <a href="email_form.php" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=400,menubar=yes,resizable=yes'); return false;"></a></div>
                            	<div class="iconPrint"><a href="print_page.php" title="Print" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=600,height=700,directories=no,location=no'); return false;"></a> --></div>
                                <h3><a href="<?=site_url();?>news/detail/<?=$row['id'];?>"><?php echo $row['title'];?></a></h3>
                                <p><?=$row['shortdesc'];?></p>
                                <div class="readmore"><a href="<?=site_url();?>news/detail/<?=$row['id'];?>">Detail &raquo;</a></div>
                            </div>
                          <div class="clearBoth"></div>
                        </li>
                        <?php }
						}else{?>
                    	<li>
                        	<div class="imgTesti">
                            	<a href="#"><img src="<?=base_url();?>images/img_news.jpg" class="imgOpacity"/></a>
                            </div>
                            <div class="infoTesti">
                            	<div class="iconEmail"><!-- <a href="email_form.php" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=400,menubar=yes,resizable=yes'); return false;"></a></div>
                            	<div class="iconPrint"><a href="print_page.php" title="Print" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=600,height=700,directories=no,location=no'); return false;"></a>--></div>
                                <h3><a href="#">NEWS UNAVAILABLE</a></h3>
                                <p>Data tidak tersedia</p>
                                <div class="readmore"><a href="#">Detail &raquo;</a></div>
                            </div>
                          <div class="clearBoth"></div>
                        </li>
                        <?php }?>
                    </ul>
                </div>
                <div class="pagiNation" align="center"><?php echo $this->pagination->create_links(); ?></div>
                      <div class="clearBoth"></div>
            </div>
            
        </div>