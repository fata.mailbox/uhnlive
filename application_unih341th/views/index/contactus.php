<div id="box_content">
        	<div id="menu_left">
            	<div id="menu_left_box">
                	<div id="menu_left_det">
                    	<div id="menu_left_judul">
                        	<div class="box_subtitle">ABOUT UNIHEALTH</div>
                        </div>
                         <div class="menu">
                            <div class="menu_uli"><a href="<?=site_url();?>aboutus"><!--di ganti-->UNIHEALTH</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>aboutus/soho_group">SOHO Group</a></div>
                            <div class="menu_uli" style="background-color:#FFF;"><a href="<?=site_url();?>aboutus/contactus">Contact Us</a></div>
                        </div> 
                    </div>
                </div>
            </div>
                    
            <div id="menu_right">
            	<div id="menu_right_banner">
                	<img src="<?=base_url();?>images/banner_about.jpg"  />
                </div>
                <div id="menu_right_content">
                	<div id="menu_right_title">Contact Us</div>
                	<hr />
                </div>
                <div id="menu_right_stop">
                	<div style="margin-bottom:10px;">
                      <table cellpadding="0" cellspacing="0" width="273" style="float:left; margin-right:10px;">
                        <tr>
                              <td colspan="3"><h3>Head Office:</h3> </td>
                        </tr>
                          <tr>
                              <td colspan="3">
                              	  PT. Universal Health Network<br />
                                  Jl. Pulo Gadung No.5<br />
                                  Kawasan Industri Pulo Gadung<br />
                                  Jakarta Timur - Indonesia
                              </td>
                          </tr>
                          <tr>
                              <td width="78">Telp</td>
                              <td width="8">:</td>
                              <td width="185">021-4605550</td>
                        </tr>
                          <tr>
                              <td>Fax</td>
                              <td>:</td>
                              <td>021-4603808</td>
                          </tr>
                    </table>
                      
                      <table cellpadding="0" cellspacing="0" width="275" style="float:left; margin-right:10px;">
                    	<tr>
                              <td colspan="3"><h3>Stock Point:</h3> </td>
                        </tr>
                          <tr>
                              <td colspan="3">
                              	Ruko Mangga Dua Square Blok F-20<br />
                                Jl. Gunung Sahari Raya No.1<br />
                                Jakarta Utara - Indonesia
                              </td>
                          </tr>
                          <tr>
                              <td width="116">Telp</td>
                              <td width="4">:</td>
                              <td width="153">021-62310402</td>
                        </tr>
                          <tr>
                              <td>Fax</td>
                              <td>:</td>
                              <td>021-62310403</td>
                          </tr>
                          <tr>
                              <td width="116">Customer Service</td>
                              <td width="4">:</td>
                              <td width="153">0857 1 7979 222</td>
                        </tr>
                          <tr>
                              <td>Email</td>
                              <td>:</td>
                              <td>info@uni-health.com</td>
                          </tr>
                      </table>
					  <div style="clear:both;"></div>
                    </div>
                    <hr /><!-- RSM di ganti semua, kecuali pak Riky-->
                    <table cellpadding="0" cellspacing="0" width="580" style="float:left; margin-bottom:20px;">
                    	<tr>
                              <td colspan="3"><h3>Regional Area:</h3> </td>
                          </tr>
                          <tr>
                              <td colspan="3">Regional Jawa & Bali </td>
                          </tr>
                          <tr>
                              <td width="78">Regional Sales Manager</td>
                              <td width="8">:</td>
                              <td width="181">Rahmat Hidayat</td>
                          </tr>
                          <tr>
                              <td>Email</td>
                              <td>:</td>
                              <td>rahmat.hidayat@sohogroup.com</td>
                          </tr>
						  <tr>
                              <td>Mobile</td>
                              <td>:</td>
                              <td>0812 6 8505 347</td>
                          </tr>
                          <tr>
                            <td height="21" colspan="3"><hr /></td>
                          </tr>
                          <tr>
                              <td colspan="3">Regional Sumatera</td>
                          </tr>
                          <tr>
                              <td width="78">Regional Sales Manager</td>
                              <td width="8">:</td>
                              <td width="181">Riky Triady</td>
                          </tr>
                          <tr>
                              <td>Email</td>
                              <td>:</td>
                              <td>riky.triady@sohogroup.com</td>
                          </tr>
						  <tr>
                              <td>Mobile</td>
                              <td>:</td>
                              <td>0813 8 0070 457</td>
                          </tr>
                          <tr>
                              <td height="25" colspan="3"><hr /></td>
                      </tr>
                          <tr>
                              <td colspan="3">Regional Kalimantan & Indonesia Timur</td>
                          </tr>
                          <tr>
                              <td width="78">Regional Sales Manager</td>
                              <td width="8">:</td>
                              <td width="181">Saleh Wiraswastika Mulyadi</td>
                          </tr>
                          <tr>
                              <td>Email</td>
                              <td>:</td>
                              <td>saleh.mulyadi@sohogroup.com </td>
                          </tr>
						  <tr>
                              <td>Mobile</td>
                              <td>:</td>
                              <td>0812 9 750 169</td>
                          </tr>
                          <tr>
                              <td height="23" colspan="3"><hr /></td>
                      </tr>
                          <tr>
                              <td colspan="3">Regional Jabodetabek & Jawa Barat</td>
                          </tr>
                          <tr>
                              <td width="78">Regional Sales Manager</td>
                              <td width="8">:</td>
                              <td width="181">Ardi Srijaya</td>
                          </tr>
                          <tr>
                              <td>Email</td>
                              <td>:</td>
                              <td>ardi.srijaya@sohogroup.com</td>
                          </tr>
						  <tr>
                              <td>Mobile</td>
                              <td>:</td>
                              <td>0819 7 6303 98</td>
                          </tr>
                      </table>
                      
              	</div>
            </div>
            
        </div>