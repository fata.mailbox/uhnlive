<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->


<div class="submenuContent">
          <div><img src="<?php echo base_url();?>images/frontend/head-title-category.gif" width="166" height="26" alt="" /></div>
          <ul class="submenuUL">
          <li><a href="<?php echo site_url();?>product/cat/">All Category</a></li>
		  <?php foreach($menu as $row):?>
          	<li><a href="<?php echo site_url();?>product/cat/<?php echo $row["id"];?>"><?php echo $row["category_name"];?></a></li>
			<?php endforeach;?>
          </ul>
  </div>
        <div class="detailContent">
	        <?php $this->load->view($subcontent);?>  
			<?php echo $this->pagination->create_links(); ?>			
        </div>
  		<?php $this->load->view('index/right_content');?>
