<div id="menu_right_content">
                	<div id="menu_right_title"><?=strtoupper($results['0']['category']);?></div>
                </div>
                <div id="menu_right_mv">
                	<ul class="box_prod_full">
                    	<?php if($results){
						foreach($results as $key => $row){ ?>
                        <li>
                        	<div class="imgProd">
                            	<a href="<?=site_url();?>product/detail/<?=$row['id'];?>"><img src="<?=base_url();?>userfiles/product/t_<?=$row['image'];?>" class="imgOpacity"/></a></div>
                            <div class="infoProd">
                            	<h3><a href="<?=site_url();?>product/detail/<?=$row['id'];?>"><?=$row['name'];?></a></h3>
                                <p><?=$row['headline'];?></p>
                                <div class="readmore"><a href="<?=site_url();?>product/detail/<?=$row['id'];?>">Detail &raquo;</a></div>
                            </div>
                          <div class="clearBoth"></div>
                        </li>
                        <?php }
                        }else{?>
                    	<li>
                        	<div class="imgProd">
                            	<a href="#"><img src="<?=base_url();?>userfiles/product/t_img_unavailable.jpg" class="imgOpacity"/></a>
                            </div>
                            <div class="infoProd">
                            	<h3><a href="#">UNAVAILABLE</a></h3>
                                <p>Data tidak tersedia</p>
                                <div class="readmore"><a href="#">Detail &raquo;</a></div>
                            </div>
                          <div class="clearBoth"></div>
                        </li>
                        <?php }?>
                    </ul>
                </div>
                <div class="pagiNation" align="center"><?php echo $this->pagination->create_links(); ?></div>