<div id="box_content">
        	<div id="menu_left">
            	<div id="menu_left_box">
                	<div id="menu_left_det">
                    	<div id="menu_left_judul">
                        	<div class="box_subtitle">ABOUT UNIHEALTH</div>
                        </div>
                        <div class="menu">
                            <div class="menu_uli" style="background-color:#FFF;"><a href="<?=site_url();?>aboutus"><!--Uni Health di ganti-->UNIHEALTH</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>aboutus/soho_group">SOHO Group</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>aboutus/contactus">Contact Us</a></div>
                        </div>
                    </div>
                </div>
            </div>
                    
            <div id="menu_right">
            	<div id="menu_right_banner">
                	<img src="<?=base_url();?>images/banner_about.jpg"  />
                </div>
                <div id="menu_right_content">
                	<div id="menu_right_title">UNIHEALTH</div>
                	<hr />
                </div>
                <div id="menu_right_stop">
					
                	<p>Selamat dan terima kasih telah bergabung dalam bisnis <b>UNIHEALTH - Member of SOHO Group.</b></p>
                    <p>Kami adalah perusahaan yang mengedepankan kualitas atas produk-produk yang berhubungan dengan kesehatan dan kecantikan serta mengajak segenap masyarakat dari berbagai golongan dan latar belakang untuk meraih kesuksesan dan kesejahteraan bersama.</p>
                    <p><b>UNIHEALTH</b> yang didirikan tahun 2009 ini merupakan <b>"Member of SOHO Group".</b> Seperti diketahui bahwa SOHO Group adalah perusahaan farmasi No. 5 terbesar di Indonesia dan telah berdiri lebih dari 60 tahun. Produk-produk yang dihasilkan sangat populer dan diterima dengan baik oleh masyarakat Indonesia, bahkan kini telah merambah ke luar negeri. </p>
                    <p>Dengan latar belakang di dukung oleh manajemen yang berpengalaman dalam bidang produk-produk kesehatan dan sistem pengelolaan perusahaan yang profesional serta tersertifikasi ISO 9000:2001, </p>
                    <p><b>UNIHEALTH</b> mempunyai komitmen penuh dan mempunyai tujuan  agar semakin banyak masyarakat Indonesia mampu meraih dan menikmati suatu taraf kehidupan yang lebih baik secara kesehatan, kecantikan maupun taraf kehidupan ekonomi, serta mewujudkan impian menjadi kenyataan. Kami berhasrat membawa setiap member dan konsumen untuk mencapai kehidupan yang lebih sehat secara jasmani dengan cara	 mengkonsumsi produk-produk kesehatan yang kami sediakan. Kami menawarkan produk-produk yang pasti memberi manfaat nyata bagi setiap penggunanya serta dengan harga yang relatif terjangkau bagi masyarakat luas.  </p>
                    <p>Kini saatnya Anda mengambil suatu keputusan untuk bergabung dan mewujudkan impian Anda menjadi kenyataan bersama <b>UNIHEALTH </b>.</p>
                    <hr />
                    
                    <h3>Vision</h3>  
                    <p>To become the Fastest Growing Company in Direct Selling Industry.</p> 
                    
                    <h3>Mission</h3>
                    <p>To help You to Realize Your DREAMS . </p>
                    <hr />
                    
                    <h3>Core Value</h3>
                    <ul style="margin:0 0 15px 20px; padding:0; list-style:disc outside;">
                        <li>Partnership</li>
                        <li>Trust & Integrity</li>
                        <li>Respect</li>
                        <li>Strive for Success</li>
                        <!--<li>Penghargaan atas prestasi. </li>
                        <li>Proaktif. </li>
                        <li>Kepemimpinan dan teladan yang baik. </li>-->
                    </ul>
					<hr />
					<h3>Operating Principle</h3>
                    <ul style="margin:0 0 15px 20px; padding:0; list-style:disc outside;">
                        <li>Result & Process Oriented</li>
                        <li>Quantity & Quality</li>
                        <li>Cost Efficiency & Simplicity</li>
                        <li>Long term Growth & Sustainability</li>
                        <!--<li>Penghargaan atas prestasi. </li>
                        <li>Proaktif. </li>
                        <li>Kepemimpinan dan teladan yang baik. </li>-->
                    </ul>
                </div>
            </div>
            
            
        </div>