<!--content-->
            <div class="content" style="width:572px;">
            	<!--about-->
                <div class="about" style="width:560px; padding:5px; border-color:#dfe8ec; background:none;">
                	<div class="aboutCont">
                    	<h2 style="color:#15384f;">Photo Gallery</h2><hr />
                        <!--highslide-gallery-->
                        
                        <div class="highslide-gallery" style="width:100%; margin:auto;">
                           <?php if($results){ 
								foreach($results as $row){?>   
                            <div class="highslide-bg">
                                <a href="<?=base_url();?>userfiles/gallery_photo/<?=$row['file'];?>" class="highslide post" onclick="return hs.expand(this)">
                                    <img src="<?=base_url();?>userfiles/gallery_photo/t_<?=$row['file'];?>" width="148" height="120" alt="Highslide JS" title="Click to enlarge" />
                                </a>
                                <div class="highslide-caption">
                                    <?=$row['description'];?>
                                </div>
                            </div>
                            <?php }
						 }else{ ?>
                            	<p>Data tidak tersedia.</p>                            
                            <?php }?>
                        
                        
                            
                          <div class="clearBoth"></div>
                        </div><!--end highslide-gallery-->
                    </div>
                  <div class="clearBoth"></div>
                </div><!--end about-->
                              
                <div class="pagiNation"><?php echo $this->pagination->create_links(); ?></div>
                        
              <div class="clearBoth"></div>
            </div><!--end content-->