<div id="menu_right">
            	<div id="menu_right_banner">
                	<img src="<?=base_url();?>images/banner_about.jpg"  />
                </div>
                <div id="menu_right_content">
                	<div id="menu_right_title">UNIHEALTH</div>
                	<hr />
                </div>
                <div id="menu_right_stop">
                	<p>Selamat dan terima kasih telah bergabung dalam bisnis <b>PT. Universal Health Network!</b></p>
                    <p>Kami adalah perusahaan yang mengedepankan kualitas atas produk-produk yang berhubungan dengan kesehatan dan kecantikan serta mengajak segenap masyarakat dari berbagai golongan dan latar belakang untuk meraih kesuksesan dan kesejahteraan bersama.</p>
                    <p><b>PT. UNIVERSAL  HEALTH  NETWORK</b> yang didirikan pada tanggal 6 April 2009 ini merupakan <b>"Member of SOHO Group".</b> Seperti diketahui bahwa SOHO Group adalah salah satu perusahaan farmasi terbesar di Indonesia dan telah berdiri lebih 50 tahun . Produk-produk yang dihasilkan sangat populer dan diterima dengan baik oleh masyarakat Indonesia, bahkan kini telah merambah ke luar negeri. </p>
                    <p>Dengan latar belakang di dukung oleh manajemen yang berpengalaman dalam bidang produk-produk kesehatan dan sistem pengelolaan perusahaan yang profesional serta tersertifikasi ISO 9000:2001, </p>
                    <p><b>PT UNIVERSAL HEALTH NETWORK</b> mempunyai komitmen penuh dan mempunyai tujuan  agar semakin banyak masyarakat Indonesia mampu meraih dan menikmati suatu taraf kehidupan yang lebih baik, baik secara kesehatan, kecantikan maupun secara taraf kehidupan ekonomi keluarganya  Kami berhasrat membawa setiap member dan konsumen untuk mencapai kehidupan yang lebih sehat secara jasmani dengan CARA mengkonsumsi produk-produk kesehatan yang kami sediakan. Kami menawarkan produk-produk yang pasti memberi manfaat nyata bagi setiap penggunanya serta dengan harga yang relatif terjangkau bagi masyarakat luas.  </p>
                    <p>Kini, saatnya kami dengan tangan terbuka penuh hangat mengajak seluruh masyarakat luas untuk bergabung dan mencapai kesuksesan bersama dengan menjadi anggota keluarga besar  PT. UNIVERSAL HEALTH NETWORK untuk kehidupan yang lebih baik.Dengan menyatukan visi dan misi melalui sikap dalam nilai-nilai budaya yang tersebut di atas, menjadi modal dan kepercayaan diri bahwa kesuksesan dan kehidupan lebih baik pasti akan bisa diraih.</p>
                    <hr />
                    
                    <h3>Visi</h3>  
                    <p>Menjadi Perusahaan Direct Selling yang professional dan dapat menjadi sebuh solusi kesuksesan para anggotanya untuk meraih sukses secara terhormat, baik secara local maupun internasional.</p> 
                    
                    <h3>Misi</h3>
                    <p>Mewujudkan kesehatan dan kesejahteraan bagi seluru lapisan masyarakat dengan menyediakan produk-produk kesehatan dan kecantikan berkualias serta sebuah solusi bisnis mandiri sesuai dengan kebutuhan dan perkembangan masyarakat. </p>
                    <hr />
                    
                    <h3>Nilai & Budaya</h3>
                    <ul style="margin:0 0 15px 20px; padding:0; list-style:disc outside;">
                        <li>Kekeluargaan dan kerjasama. </li>
                        <li>Komitmen dan konsisten. </li>
                        <li>Mengedepankan "win-win solution". </li>
                        <li>Keterbukaan dan kejujuran. </li>
                        <li>Penghargaan atas prestasi. </li>
                        <li>Proaktif. </li>
                        <li>Kepemimpinan dan teladan yang baik. </li>
                    </ul>
                </div>
            </div>
            