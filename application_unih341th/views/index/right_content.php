<!-- rc -->
<div class="rightContent">
          <?php $this->load->view('index/right_customer');?>
          
          <div class="testimonial">
            <div class="testiHeadTitle"><img src="<?php echo base_url();?>images/frontend/head-title-testi.gif" width="196" height="26" alt="" /></div>
            <ul class="newsList">
            <?php if($testi): 
				foreach($testi as $testi): ?>
              <li><img src="<?php echo base_url();?>userfiles/thumbnail/<?php echo $testi['thumbnail'];?>" alt="" width="60" height="60" /><?php echo $testi['shortdesc'];?><br /><a href="<?php echo site_url();?>testi/index/<?php echo $testi['id'];?>/<?php echo $this->uri->segment(4);?>">Read more...</a></li>
              <?php endforeach;?>
              
              <?php else: ?>
              	Data is not available.
              <?php endif;?>
              
            </ul>
            <div style="text-align:right; padding:6px 0 8px 0;"><a href="<?php echo site_url();?>testi/" style="font-weight:900;">Archives</a>&nbsp;&raquo;&nbsp;&nbsp;</div>
          </div>
          
          <?php $this->load->view('index/member_of_soho');?>
</div>