<div id="box_content">
        	<div id="menu_left">
            	<div id="menu_left_box">
                	<div id="menu_left_det">
                    	<div id="menu_left_judul">
                        	<div class="box_subtitle">ABOUT UNIHEALTH</div>
                        </div>
                        <div class="menu">
                            <div class="menu_uli"><a href="<?=site_url();?>aboutus">UNIHEALTH</a></div>
                            <div class="menu_uli" style="background-color:#FFF;"><a href="<?=site_url();?>aboutus/soho_group">SOHO Group</a></div>
                            <div class="menu_uli"><a href="<?=site_url();?>aboutus/contactus">Contact Us</a></div>
                        </div>
                    </div>
                </div>
            </div>
                    
            <div id="menu_right">
            	<div id="menu_right_banner">
                	<img src="<?=base_url();?>images/banner_about.jpg"  />
                </div>
                <div id="menu_right_content">
                	<div id="menu_right_title">SOHO Group</div>
                	<hr />
                </div>
				
                <div id="menu_right_stop">
					<p>SOHO Group is one of Indonesia's leading pharmaceutical and healthcare corporations in manufacturing, distributing, and providing quality health products and services.</p>
                    <p>Our corporation consists of 3 synergy business under SOHO Group: SOHO Group PHARMA (Prescription market : Branded Generics, Natural & TCM Product, Low Price Medicine, Medical Device, and Alliance Business), SOHO Group COSTUMER HEALTH (OTC Product, Consumer Health Product, Hezzel Farm Product and Unihealth MLM Product), SOHO Group DISTRIBUTION (Distribution arm of SOHO Group & Other Principal, Raw Material Trading Business, and Retail Business "Apotek Harmony”<!-- ditambah-->).</p>
                    <p>We focus on manufacturing - including under-licensed - herbal products (immunomodulator, antioxidant, anti diarrhea, anti laxantia) and synthetic products (antibiotic, cephalosporin, injection preparation, antiemetic, analgesic, musculoskeletal, vitamin, obgyn, gastro, neuro, and rheumatology products)</p>
                    <p>We distribute pharmaceutical and healthcare products of our companies, and other pharmaceutical and consumer goods manufacturers.</p>
                    <p>Our distribution channel is supported by our branches covering 90% of major cities in Indonesia.</p>
                    <p>Our global presence in emerging markets: Malaysia, Myanmar, Nigeria, Lebanon, Mongolia, Vietnam, Suriname, <!-- Brunie diganti menjadi Brunei-->Brunei, Mauritius.</p>
                    <p>We employ around 6,000 people.</p>
                    <p>In 2005 we started adopting Balanced Scorecard framework to translate strategy into operational objectives that drive both behavior and performance.</p>
                    <p>We have been investing heavily in Product Development, Human Capital and Information.</p>
                    <p>Capital in order to improve and upgrade these aspects across the organization since 2005.</p>
                    <p>We are committed to VISION 2015: New Horizon Strategy to create our future together.</p>
                    <p align="right"><a href="http://www.sohogroup.com/"><b>www.sohogroup.com </b>&nbsp; &nbsp; </p>

                </div>
            </div>
            
        </div>