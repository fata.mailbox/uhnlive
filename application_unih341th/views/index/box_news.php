
                <!--news_box-->
                <div class="news_box">
                	<h2><a href="<?=site_url();?>news">NEWS</a></h2>
                	
                    <div id="latestNewsBoxContent">	    
                        <div id="ctl00_ContentPlaceHolderNewsScoller_ctl00_radHeadlines" style="height:90px; width: 260px; visibility: visible;">
                            <div class="rrRelativeWrapper">
                                <div style="overflow: hidden; width: 260px; height:90px; position: relative;" class="rrClipRegion">
                                    <ul class="rrItemsList">
                                    <?php if($news): 
										foreach($news as $row): ?>
                                            <li style="width:260px; height:90px;">
                                                <div>
                                                    <p><a href="<?=site_url();?>news/detail/<?=$row['id'];?>"><?=strtoupper($row['title']);?></a></p>
                                                    <span><?=$row['ftgl'];?> /</span>
                                                    <span style="color:#89570f;">by <?=$row['createdby'];?></span> <br />
                                                    <p><?=$row['shortdesc'];?></p>
                                                </div>
                                            </li>
                                            
                                        	<?php endforeach;
                                         else:?>
                                            <li style="width:260px; height:90px;">
                                                <div>
                                                    <p>Data belum tersedia.</p>
                                                </div>
                                            </li>
                            	       <?php endif;?>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="newsMore">
                    	<a style="color:#00941f;" href="<?=site_url();?>news"><b>More...</b></a> &raquo;
                    </div>
                  <div class="clearBoth"></div>
                </div><!--end news_box-->