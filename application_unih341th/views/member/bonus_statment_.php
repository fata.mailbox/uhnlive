<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="100%">
<?php echo form_open('member/bonus/statement/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
<?php if($this->session->userdata('group_id') <= 100){ ?>
				 <tr>
			<td width='19%' valign='top'>Member ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?>				
					 <span class='error'>*<?php echo form_error('member_id');?></span></td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
        <?php }else{ ?>
        <tr>
			<td width='19%' valign='top'>Member ID / Nama </td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('userid')." / ".$this->session->userdata('name');?></b></td>
		</tr>
        <?php }?>
        
        <tr>
			<td valign='top'>Periode</td>
			<td valign='top'>:</td>
			<td><?php echo form_dropdown('periode',$dropdown);?></td>
		</tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
		<td><?php echo form_submit('submit','preview');?><?php echo form_submit('submit','export');?></td>
	</tr>
                    <?php echo form_close();?>
	</table>
    <br /><br />
    <table>
	<?php 
	if($totalaccumulation){
	// pop up attributes
	$attspop = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
	// eof pop up attributes
	?>
    <?php
		$memberJoinDate = $this->MBonus->getMemberJoinDate($memid);
		$memJoinDate = '';
		foreach($memberJoinDate as $rowJoin) :
			$memJoinDate = $rowJoin['joindate'];
		endforeach;
		//echo $memJoinDate;
	?>
  <!--
    <tr>
    	<td valign="top">Akumulasi Bonus (12 Bulan Berjalan)</td>
        <td>:</td>
        <td><b><?php  echo anchor_popup('search/search_bonus/index/'.date('Y-m-d',strtotime('-1 Year',strtotime($periodecurr))).'/'.$periodecurr.'/'.$memid, 'Rp.'.$totalaccumulation['fnominal'], $attspop); ?></b></td>
    </tr>
    <tr>
    	<td valign="top">Total Bonus (Sejak Bergabung)</td>
        <td>:</td>
        <td><b><?php  echo anchor_popup('search/search_bonus/index/'.$memJoinDate.'/'.$periodecurr.'/'.$memid, 'Rp.'.$totalbonusfromjoin['fnominal'], $attspop); ?></b></td>
        <!--<td><b><?php  echo 'Rp.'.$totalbonusfromjoin['fnominal']; ?></b></td>-->
    <!--
    </tr>
    -->
    <?php 
	}
	?>
    </table>
    <table class="stripe">
	<tr>
      <th width='7%'>No.</th>
      <th width='45%'>Description</th>
      <th width='15%'><div align="right">BV</div></th>
      <th width='10%'><div align="right">Persen %</div></th>
      <th width='23%'><div align="right">Nominal Rp</div></th>
    </tr>
   
<?php
if ($results): 
	foreach($results as $key => $row): ?>
    <?php
	if($row['id']!=0){
	?>
    <tr>
      <td><?php echo anchor('member/bonus/detail/'.$row['id'], $row['i']);?></td>
      <td><?php echo anchor('member/bonus/detail/'.$row['id'], $row['titlebonus']);?></td>
      <td align="right"><?php echo anchor('member/bonus/detail/'.$row['id'], $row['fbv']);?></td>
      <td align="right"><?php echo anchor('member/bonus/detail/'.$row['id'], $row['persen']);?></td>
     <td align="right"><?php echo anchor('member/bonus/detail/'.$row['id'], $row['fnominal']);?></td>
    </tr>
    <?php } else { ?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['titlebonus'];?></td>
      <td align="right"><?php echo $row['fbv'];?></td>
      <td align="right"><?php echo $row['persen'];?></td>
     <td align="right"><?php echo $row['fnominal'];?></td>
    </tr>
    <?php } ?>
    <?php endforeach; ?>
	<tr>
	      <td colspan="4"><b>Total Bonus Rp. </b></td>
     	<td align="right"><b><?php echo $total['fnominal'];?></b></td>
    </tr>
    <tr>
	      <td colspan="5"><br />Keterangan: <br />
          - PPH 21 dikenakan pada semua bonus per bulan, sesuai dengan ketentuan perpajakan yang berlaku.<br />
          - Bonus diatas Rp 50.000.000,- ke atas mengikuti ketentuan perpajakan yang berlaku (progresif).</td>
    </tr>
<?php else: ?>
    <tr>
      <td colspan="5">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>			
                
                
<?php $this->load->view('footer');?>
