<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<hr />

<?php echo form_open('member/activity/view', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off')); ?>
	<table width="55%">
		<tr>
			<td width="25%">Member ID</td>
			<td width="1%">:</td>
			<td width="74%"><?php 
				$data = array('name'=>'member_id','id'=>'member_id','maxlength'=>'20','size'=>'11','value'=>set_value('member_id'));
				echo form_input($data);
				$atts = array(
				  'width'      => '450',
				  'height'     => '500',
				  'scrollbars' => 'yes',
				  'status'     => 'yes',
				  'resizable'  => 'yes',
				  'screenx'    => '0',
				  'screeny'    => '0'
				);
				echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			</td>
		</tr>
		<tr>
			<td>Nama Member</td>
			<td>:</td>
			<td><?php 
				$data = array('name'=>'name','id'=>'name','maxlength'=>'20','readonly'=>'1','value'=>set_value('name'));
				echo form_input($data); 
				?>
			</td>
		</tr>                    
		<tr>
			<td>Periode</td>
			<td>:</td>
			<td><?php echo form_dropdown('periode',$periode,set_value('periode')); ?></td>                        
		</tr>
		<!-- 
		<tr>
			<td>Level ?</td>
			<td>:</td>
			<td><?php $data = array(''=>'All','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10');
			echo form_dropdown('level',$data); ?></td>
		</tr>
		-->
					<?php
					/* Updated by Boby 20150519 */
					// $sl['jenjang_id'] = 6;
					if($sl['jenjang_id']>5){
					?>
					<tr>
                        	<td>Jenjang</td>
                            <td>:</td>
                            <td><?php echo $sl['jenjang']; ?></td>
                    </tr>
					<tr>
                        	<td>TGPV Samping</td>
                            <td>:</td>
                            <td><?php echo number_format($sl['lq_tgpv']); ?></td>                        
                    </tr>
					<tr>
                        	<td>Car Qualified</td>
                            <td>:</td>
                            <td><?php echo $sl['note']; ?></td>                        
                    </tr>
					<?php
					}
					/* Updated by Boby 20150519 */
					?>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<?php 
					echo " ".form_submit('submit','Preview'); 
					if($result){ echo '&nbsp;'.anchor($hplink,'download');}
				?>
			</td>
		</tr>
	</table>				
<?php	echo form_close();?>
	<link rel="StyleSheet" href="<?php echo base_url();?>/dtree/dtree.css" type="text/css" />
	<script type="text/javascript" src="<?php echo base_url();?>/dtree/dtree.js"></script>
	
<div class="dtree">
	<p class="cName"><a href="javascript: d.openAll();">Buka</a> | <a href="javascript: d.closeAll();">Tutup</a> <!--| <a href="javascript: d.openTo(2, true);">Open --></p>

	<script type="text/javascript">
		<!--		
		
		d = new dTree('d');
		d.config.useIcons=false;
		d.config.useCookies=false;
		d.config.inOrder=true;
		
		<?php if($result){ 
		$level_ = 0; 
		$node = 0; 
		/* Created by Takwa 20141111 */

			$ps = 0;	// Personal sales

			$pvg = 0;	// PV Group

			$am = 0;	// active member

			$nm = 0;	// new member

			$nr = 0;	// new member blj

			$qrecruit = 0; // new member blj 1 jt

			$sf = 0;	// sales force, titik yg blj

			$im = 0; 	// inactive member

			/* End created by Takwa 20141111 */
		foreach($result as $key => $row){ 
		if($key == 0){ 
		/* Created by Takwa 20141111 */

			// active member

			if($row['act']=='active'){

				$am=$am+1;

			}else{

				$im+=1;

			}

			

			// new member

			if($row['newMember']==2){

				$nr+=1;

				if($row['fps']>=1000000) $qrecruit+=1;

			}else if($row['newMember']==1){

				$nm+=1;

			}

			

			// Sales Force

			if($row['fps']>0) $sf+=1;

			/* End created by Takwa 20141111 */
		?>
		<?php
	$bonus2persen = 0;
	$bonus4persen = 0;
	$bonusrevenue = 0;
	$bonusleadership = 0;
	$bonuscashaward = 0;
	$bonuscashawardL = 0;
	$bonuscashawardSL = 0;
	$bonuscashawardreqL = 0;
	$bonuscashawardreqSL = 0;
	$bonuscarreward = 0;
	$bonuscarccb = 0;
	$bonuscar = 0;
	$bonusstar = 0;
	$stcConf = 0;
	$leaderConf = 0;
	if($row['qualified']=='qualified green') {
		?>
		d.add('0','-1','<img src="<?=base_url();?>images/backend/<?=$row['jenjang_id'];?>i.png" height="17"><span class="qualified"><?php
				$ps=$row['fps'];
				$pvg=$row['TGPV']; 
				/* Updated by Boby 20131024 */
				/* Updated by Boby 20131120 */
				if($row['note']==1){
					$a="*";
					$level = $level_+1; 
					if($node==0){
						$node++;
						$parent = $row['parentid']; 
					}else{
						$parent = $parent; 
					}
				}else{
					$a="";
					$level = $row['level']; 
					$parent = $row['parentid']; 
					$node = 0; 
				}
				/* End updated by Boby 20131120 */
				/* End updated by Boby 20131024 */		
//echo $periodeSummary;
if($periodeSummary == date('Y-m-t',strtotime(date("Y-m-d"))))
{
	$bonus2persen = 0;
	$bonus4persen = 0;
	$bonusrevenue = 0;
	$bonusleadership = 0;
	$bonuscashaward = 0;
	$bonuscashawardL = 0;
	$bonuscashawardSL = 0;
	$bonuscashawardreqL = 0;
	$bonuscashawardreqSL = 0;
	$bonuscarreward = 0;
	$bonuscarccb = 0;
	$bonuscar = 0;
	$bonusstar = 0;
	$stcConf = 0;
	$leaderConf = 0;
}else{
	$result2persen=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,31);
    foreach($result2persen as $key2persen => $row2persen){ 
		$bonus2persen = $row2persen['nominal'];
	}
	$result4persen=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,32);
    foreach($result4persen as $key4persen => $row4persen){ 
		$bonus4persen = $row4persen['nominal'];
	}
	$resultrevenue=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,36);
    foreach($resultrevenue as $keyrevenue => $rowrevenue){ 
		$bonusrevenue = $rowrevenue['nominal'];
	}
	$resultleadership=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,133);
    foreach($resultleadership as $keyleadership => $rowleadership){ 
		$bonusleadership = $rowleadership['nominal'];
	}
	$resultcashawardL=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,33);
    foreach($resultcashawardL as $keycashawardL => $rowcashawardL){ 
		$bonuscashawardL = $rowcashawardL['nominal'];
	}
	$resultcashawardSL=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,34);
    foreach($resultcashawardSL as $keycashawardSL => $rowcashawardSL){ 
		$bonuscashawardSL = $rowcashawardSL['nominal'];
	}
	$resultcashawardreqL=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,37);
    foreach($resultcashawardreqL as $keycashawardreqL => $rowcashawardreqL){ 
		$bonuscashawardreqL = $rowcashawardreqL['nominal'];
	}
	$resultcashawardreqSL=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,38);
    foreach($resultcashawardreqSL as $keycashawardreqSL => $rowcashawardreqSL){ 
		$bonuscashawardreqSL = $rowcashawardreqSL['nominal'];
	}
	$bonuscashaward = $bonuscashawardL+$bonuscashawardSL+$bonuscashawardreqL+$bonuscashawardreqSL;
	
	$resultcarreward=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,132);
    foreach($resultcarreward as $keycarreward => $rowcarreward){ 
		$bonuscarreward = $rowcarreward['nominal'];
	}
	$resultcarccb=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,131);
    foreach($resultcarccb as $keycarccb => $rowcarccb){ 
		$bonuscarccb = $rowcarccb['nominal'];
	}
	
	$resultstar=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,35);
    foreach($resultstar as $keystar => $rowstar){ 
		$bonusstar = $rowstar['nominal'];
	}

	// stc conf track
	if(date('n',strtotime($periodeSummary)) < 3){
		$res_dateYear = date('Y',strtotime($periodeSummary)) - 1;
		$res_startdateQS = $res_dateYear.'-03-01';
		
		$res_dateYear_ly = date('Y',strtotime($periodeSummary_ly)) - 1;
		$res_startdateQS_ly = $res_dateYear_ly.'-03-01';
	}else{
		$res_startdateQS = date('Y',strtotime($periodeSummary)).'-03-01';
		$res_startdateQS_ly = date('Y',strtotime($periodeSummary_ly)).'-03-01';
	}
	$res_enddateQS = $periodeSummary;
	$res_enddateQS_ly = $periodeSummary_ly;
	
	//echo $startdateQS .'aaa<br>';
	//echo $enddateQS .'aaa<br>';
	
	$resultStcConf=$this->MTracking->getTracking($row['member_id'],$res_startdateQS,$res_enddateQS);
    foreach($resultStcConf as $keyStcConf => $rowStcConf){ 
		$stcConf = $rowStcConf['Q1']+$rowStcConf['Q2']+$rowStcConf['Q3']+$rowStcConf['Q4']+$rowStcConf['Q5']+$rowStcConf['Q6']+$rowStcConf['Q7']+$rowStcConf['Q8']+$rowStcConf['Q9']+$rowStcConf['Q10']+$rowStcConf['Q11']+$rowStcConf['Q12'];
	}

	//leader Conference
	$leader_startdateQ = date('Y',strtotime($periodeSummary)).'-01-01';
	$leader_enddateQ = $periodeSummary;
	//echo $startdateQ."<br>";
	//echo $enddateQ."<br>";
	$resultLeader=$this->MTracking_mem->getTracking($row['member_id'],$leader_startdateQ,$leader_enddateQ);
    foreach($resultLeader as $keyLeader => $rowLeader){ 
		$leaderConf = $rowLeader['Q1']+$rowLeader['Q2']+$rowLeader['Q3']+$rowLeader['Q4']+$rowLeader['Q5']+$rowLeader['Q6']+$rowLeader['Q7']+$rowLeader['Q8']+$rowLeader['Q9']+$rowLeader['Q10']+$rowLeader['Q11']+$rowLeader['Q12'];
	}

	
}

?><table class="stripe" border="1" style="font-family:Jill Sans MT;"><tr class="qualified green"><td>Id member</td><td>Level jaringan</td><td>Nama</td><td>hp</td><td>Personal PV</td><td>Tgl bergabung</td><td>Peringkat</td><td>TGPV</td><td>Status</td><td>Rekrutmen</td><td>Jumlah Leader</td><td>Jumlah Break Away</td></tr><tr><td><?=$row['member_id'];?></td><td><?=$level;?></td><td><?php echo $a.$row['name']; ?></td><td><?=$row['hp'];?></td><td><?=$row['fps'];?></td><td><?=$row['joindate'];?></td><td><?=$row['jenjang'];?></td><td><?=$row['TGPV'];?></td><td><?=$row['act'];?></td><td><?=$row['rekrut'];?></td><td><?=$row['qty_all_leader'];?></td><td><?=$row['qty_leg_leader'];?></td></tr><tr class="qualified green"><td>BPP</td><td>Bonus Aktivasi</td><td>Bonus Kesuksesan 2%</td><td>Bonus Kesuksesan 4%</td><td>Bonus UNIHEALTH tahunan 1%</td><td>Bonus Leadership</td><td>Bonus Cash Award</td><td>Bonus Car Ownership</td><td>Bonus Car Cash</td><td>Bonus STAR 2%</td><td>Stc Conf</td><td>Leader Conf</td></tr><tr><td><?php echo number_format($row['bpp']); ?></td><td><?php echo number_format($row['aktifasi']); ?></td><td><?php echo number_format($bonus2persen); ?></td><td><?php echo number_format($bonus4persen); ?></td><td><?php echo number_format($bonusrevenue); ?></td><td><?php echo number_format($bonusleadership); ?></td><td><?php echo number_format($bonuscashaward); ?></td><td><?php echo number_format($bonuscarreward); ?></td><td><?php echo number_format($bonuscarccb); ?></td><td><?php echo number_format($bonusstar); ?></td><td><?php echo $stcConf; ?></td><td><?php echo $leaderConf; ?></td></tr></table><?php
		$level_ = $level;
		$parent = $row['auto_id'];
		?>');
		<?php
		/*
BPP 9%
Bonus Aktivasi
Bonus Kesuksesan Sponsor 2%
Bonus Kesuksesan Uplinne 4%
Bonus Executive 1%
Bonus SL 1.5 %
Bonus GL 1.75 %
Bonus Exceutive SL 1.87%
Bonus Star 2%
Bonus leadership
Car bonus
Car cash bonus
		*/
		}else{
		?>
			d.add('0','-1','<img src="<?=base_url();?>images/backend/<?=$row['jenjang_id'];?>i.png" height="17"><span class="<?=$row['qualified'];?>"><?php 
				$ps=$row['fps'];
				$pvg=$row['TGPV']; 
				/* Updated by Boby 20131024 */
				/* Updated by Boby 20131120 */
				if($row['note']==1){
					$a="*";
					$level = $level_+1; 
					if($node==0){
						$node++;
						$parent = $row['parentid']; 
					}else{
						$parent = $parent; 
					}
				}else{
					$a="";
					$level = $row['level']; 
					$parent = $row['parentid']; 
					$node = 0; 
				}
				/* End updated by Boby 20131120 */
				/* End updated by Boby 20131024 */
				echo $row['sponsorId']." | ".$level." | ".$row['member_id']." | ".$a.$row['name']."(".$row['hp'].") | ps:".$row['fps']." | ".$row['joindate']." | "
				.$row['jenjang']." | tgpv:".$row['TGPV']." | bpp:".number_format($row['bpp'])." | bns aktifasi:".number_format($row['aktifasi'])." | "
				.$row['act']." | rekrut:".$row['rekrut']." | qal:".$row['qty_all_leader']." | qll:".$row['qty_leg_leader'];
				$level_ = $level;
				$parent = $row['auto_id']; 
				?>');
			<?php 
			}
			?>
		<?php 
		}else{
			/* Created by Takwa 20141111 */

			// active member

			if($row['act']=='active'){

				$am=$am+1;

			}else{

				$im+=1;

			}

			

			// new member

			if($row['newMember']==2){

				$nr+=1;

				if($row['fps']>=1000000) $qrecruit+=1;

			}else if($row['newMember']==1){

				$nm+=1;

			}

			

			// Sales Force

			if($row['fps']>0) $sf+=1;

			/* End created by Takwa 20141111 */
				/* Updated by Boby 20131024 */
				/* Updated by Boby 20131120 */
				if($row['note']==1){
					$a="*";
					$level = $level_+1; 
					if($node==0){
						$node++;
						$parent = $row['parentid']; 
					}else{
						$parent = $parent; 
					}
				}else{
					$a="";
					$level = $row['level']; 
					$parent = $row['parentid']; 
					$node = 0; 
				}
				/* End updated by Boby 20131120 */
				/* Updated by Boby 20131024 */
		?>
			<?php
			if($row['qualified']=='qualified green') {
					$bonus2persen = 0;
					$bonus4persen = 0;
					$bonusrevenue = 0;
					$bonusleadership = 0;
					$bonuscashaward = 0;
					$bonuscashawardL = 0;
					$bonuscashawardSL = 0;
					$bonuscashawardreqL = 0;
					$bonuscashawardreqSL = 0;
					$bonuscarreward = 0;
					$bonuscarccb = 0;
					$bonuscar = 0;
					$bonusstar = 0;
					$stcConf = 0;
					$leaderConf = 0;
				if($periodeSummary == date('Y-m-t',strtotime(date("Y-m-d"))))
				{
					$bonus2persen = 0;
					$bonus4persen = 0;
					$bonusrevenue = 0;
					$bonusleadership = 0;
					$bonuscashaward = 0;
					$bonuscashawardL = 0;
					$bonuscashawardSL = 0;
					$bonuscashawardreqL = 0;
					$bonuscashawardreqSL = 0;
					$bonuscarreward = 0;
					$bonuscarccb = 0;
					$bonuscar = 0;
					$bonusstar = 0;
					$stcConf = 0;
					$leaderConf = 0;
				}else{
					$result2persen=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,31);
					foreach($result2persen as $key2persen => $row2persen){ 
						$bonus2persen = $row2persen['nominal'];
					}
					$result4persen=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,32);
					foreach($result4persen as $key4persen => $row4persen){ 
						$bonus4persen = $row4persen['nominal'];
					}
					$resultrevenue=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,36);
					foreach($resultrevenue as $keyrevenue => $rowrevenue){ 
						$bonusrevenue = $rowrevenue['nominal'];
					}
					$resultleadership=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,133);
					foreach($resultleadership as $keyleadership => $rowleadership){ 
						$bonusleadership = $rowleadership['nominal'];
					}
					$resultcashawardL=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,33);
					foreach($resultcashawardL as $keycashawardL => $rowcashawardL){ 
						$bonuscashawardL = $rowcashawardL['nominal'];
					}
					$resultcashawardSL=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,34);
					foreach($resultcashawardSL as $keycashawardSL => $rowcashawardSL){ 
						$bonuscashawardSL = $rowcashawardSL['nominal'];
					}
					$resultcashawardreqL=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,37);
					foreach($resultcashawardreqL as $keycashawardreqL => $rowcashawardreqL){ 
						$bonuscashawardreqL = $rowcashawardreqL['nominal'];
					}
					$resultcashawardreqSL=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,38);
					foreach($resultcashawardreqSL as $keycashawardreqSL => $rowcashawardreqSL){ 
						$bonuscashawardreqSL = $rowcashawardreqSL['nominal'];
					}
					$bonuscashaward = $bonuscashawardL+$bonuscashawardSL+$bonuscashawardreqL+$bonuscashawardreqSL;
					
					$resultcarreward=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,132);
					foreach($resultcarreward as $keycarreward => $rowcarreward){ 
						$bonuscarreward = $rowcarreward['nominal'];
					}
					$resultcarccb=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,131);
					foreach($resultcarccb as $keycarccb => $rowcarccb){ 
						$bonuscarccb = $rowcarccb['nominal'];
					}
					
					$resultstar=$this->Mactivityreport->getBonusKu($row['member_id'],$periodeSummary,35);
					foreach($resultstar as $keystar => $rowstar){ 
						$bonusstar = $rowstar['nominal'];
					}
					
					// stc conf track
					if(date('n',strtotime($periodeSummary)) < 3){
						$res_dateYear = date('Y',strtotime($periodeSummary)) - 1;
						$res_startdateQS = $res_dateYear.'-03-01';
						
						$res_dateYear_ly = date('Y',strtotime($periodeSummary_ly)) - 1;
						$res_startdateQS_ly = $res_dateYear_ly.'-03-01';
					}else{
						$res_startdateQS = date('Y',strtotime($periodeSummary)).'-03-01';
						$res_startdateQS_ly = date('Y',strtotime($periodeSummary_ly)).'-03-01';
					}
					$res_enddateQS = $periodeSummary;
					$res_enddateQS_ly = $periodeSummary_ly;
					
					//echo $startdateQS .'aaa<br>';
					//echo $enddateQS .'aaa<br>';
					
					$resultStcConf=$this->MTracking->getTracking($row['member_id'],$res_startdateQS,$res_enddateQS);
					foreach($resultStcConf as $keyStcConf => $rowStcConf){ 
						$stcConf = $rowStcConf['Q1']+$rowStcConf['Q2']+$rowStcConf['Q3']+$rowStcConf['Q4']+$rowStcConf['Q5']+$rowStcConf['Q6']+$rowStcConf['Q7']+$rowStcConf['Q8']+$rowStcConf['Q9']+$rowStcConf['Q10']+$rowStcConf['Q11']+$rowStcConf['Q12'];
					}
				
					//leader Conference
					$leader_startdateQ = date('Y',strtotime($periodeSummary)).'-01-01';
					$leader_enddateQ = $periodeSummary;
					//echo $startdateQ."<br>";
					//echo $enddateQ."<br>";
					$resultLeader=$this->MTracking_mem->getTracking($row['member_id'],$leader_startdateQ,$leader_enddateQ);
					foreach($resultLeader as $keyLeader => $rowLeader){ 
						$leaderConf = $rowLeader['Q1']+$rowLeader['Q2']+$rowLeader['Q3']+$rowLeader['Q4']+$rowLeader['Q5']+$rowLeader['Q6']+$rowLeader['Q7']+$rowLeader['Q8']+$rowLeader['Q9']+$rowLeader['Q10']+$rowLeader['Q11']+$rowLeader['Q12'];
					}
								
				}
			?>
			d.add(<?php echo $row['auto_id'];?>,<?php echo $parent;?>,'<img src="<?=base_url();?>images/backend/<?=$row['jenjang_id'];?>i.png" height="17"><span class="qualified"><table class="stripe" border="1" style="font-family:Jill Sans MT;"><tr class="qualified green"><td>Id member</td><td>Level jaringan</td><td>Nama</td><td>hp</td><td>Personal PV</td><td>Tgl bergabung</td><td>Peringkat</td><td>TGPV</td><td>Status</td><td>Rekrutmen</td><td>Jumlah Leader</td><td>Jumlah Break Away</td></tr><tr><td><?=$row['member_id'];?></td><td><?=$level;?></td><td><?php echo $a.$row['name']; ?></td><td><?=$row['hp'];?></td><td><?=$row['fps'];?></td><td><?=$row['joindate'];?></td><td><?=$row['jenjang'];?></td><td><?=$row['TGPV'];?></td><td><?=$row['act'];?></td><td><?=$row['rekrut'];?></td><td><?=$row['qty_all_leader'];?></td><td><?=$row['qty_leg_leader'];?></td></tr><tr class="qualified green"><td>BPP</td><td>Bonus Aktivasi</td><td>Bonus Kesuksesan 2%</td><td>Bonus Kesuksesan 4%</td><td>Bonus UNIHEALTH tahunan 1%</td><td>Bonus Leadership</td><td>Bonus Cash Award</td><td>Bonus Car Ownership</td><td>Bonus Car Cash</td><td>Bonus STAR 2%</td><td>Stc Conf</td><td>Leader Conf</td></tr><tr><td><?php echo number_format($row['bpp']); ?></td><td><?php echo number_format($row['aktifasi']); ?></td><td><?php echo number_format($bonus2persen); ?></td><td><?php echo number_format($bonus4persen); ?></td><td><?php echo number_format($bonusrevenue); ?></td><td><?php echo number_format($bonusleadership); ?></td><td><?php echo number_format($bonuscashaward); ?></td><td><?php echo number_format($bonuscarreward); ?></td><td><?php echo number_format($bonuscarccb); ?></td><td><?php echo number_format($bonusstar); ?></td><td><?php echo $stcConf; ?></td><td><?php echo $leaderConf; ?></td></tr></table><?php
			$level_ = $level;
			$parent = $row['auto_id'];
			?>');
			<?php
			/*
			BPP 9%
			Bonus Aktivasi
			Bonus Kesuksesan Sponsor 2%
			Bonus Kesuksesan Uplinne 4%
			Bonus Executive 1%
			Bonus SL 1.5 %
			Bonus GL 1.75 %
			Bonus Exceutive SL 1.87%
			Bonus Star 2%
			Bonus leadership
			Car bonus
			Car cash bonus
			*/
			}else{
			?>
			d.add(<?php echo $row['auto_id'];?>,<?php echo $parent;?>,'<img src="<?=base_url();?>images/backend/<?=$row['jenjang_id'];?>i.png" height="17"><span class="<?=$row['qualified'];?>"><?php 
				
				echo $row['sponsorId']." | ".$level." | ".$row['member_id']." | ".$a.$row['name']."(".$row['hp'].") | ps:".$row['fps']." | ".$row['joindate']." | "
				.$row['jenjang']." | tgpv:".$row['TGPV']." | bpp:".number_format($row['bpp'])." | bns aktifasi:".number_format($row['aktifasi'])." | "
				.$row['act']." | rekrut:".$row['rekrut']." | qal:".$row['qty_all_leader']." | qll:".$row['qty_leg_leader'];
				$level_ = $level;
				$parent = $row['auto_id']; 
				?>');
			<?php 
			}
			?>
		<?php }?>
		
		<?php }}else{ ?>d.add(<?php echo '0';?>,'-1','Maaf, member diatas upline atau crossline anda.');<?php }?>
		document.write(d);

		//-->
	</script>
    <p class="cName"><a href="javascript: d.openAll();">Buka</a> | <a href="javascript: d.closeAll();">Tutup</a><br />
	<!-- Note: Level | Member ID | Nama Member | Posisi Sekarang | PV Pribadi | TGPV | APGS</p> -->
</div>
<h2>Summary of Activity Report</h2>
<?php
$dM = date('M Y',strtotime($periodeSummary));
$dM_ly =  date('M Y',strtotime($periodeSummary." - 1 Year"));
?>
<table class="stripe">

	<tr>

      <th width='50'>No.</th>

      <th width='200'>Description</th>

      <th width='70'><div align="right"><?php echo $dM; ?></div></th>
      <th width='50'><div align="right">vs</div></th>
      <th width='70'><div align="right"><?php echo $dM_ly; ?></div></th>
	  <td>&nbsp;</td>

    </tr>
<?php 
//echo $periodeSummary;
$penghasilan = 0;
$peringkat = '-';
$peringkatid = 0;
$qstc = 0;
$qleaders = 0;
$actm = 0;
$inactm = 0;
$reactm = 0;
$sfm = 0;
$spm = 0;
$nmm = 0;
$nrm = 0;
$nrqm = 0;

$penghasilan_ly = 0;
$peringkat_ly = '-';
$peringkatid_ly = 0;
$ps_ly = 0;
$pvg_ly = 0;
$qstc_ly = 0;
$qleaders_ly = 0;
$actm_ly = 0;
$inactm_ly = 0;
$reactm_ly = 0;
$sfm_ly = 0;
$spm_ly = 0;
$nmm_ly = 0;
$nrm_ly = 0;
$nrqm_ly = 0;

$vs = 'equal.png';

/*
if($this->input->post('periode')){
	echo $memberid
	$resultRowM=$this->MTree->get_member($memberid);
	foreach($resultRowM as $keyRowM => $rowM){ 
		if(isset($rowM['lft']))
		$lft = $rowM['lft'];
		if(isset($rowM['rgt']))
		$rgt = $rowM['rgt'];
		if(isset($rowM['level']))
		$level = $rowM['level'];
	}
}else{
	$lft = 0;$rgt = 0;$level = 0;
}
*/

$periodeSummary_ly = date('Y-m-t',strtotime($periodeSummary." - 1 Year"));

if($periodeSummary == date('Y-m-t',strtotime(date("Y-m-d"))))
{
	$penghasilan = 0;
}else{
	$result1=$this->Mactivityreport->getpenghasilan($memberid,$periodeSummary);
    foreach($result1 as $key1 => $row1){ 
		$penghasilan = $row1['bonus'];
	}
	
}

if($this->input->post('periode')){
	$result1_ly=$this->Mactivityreport->getpenghasilan($memberid,$periodeSummary_ly);
    foreach($result1_ly as $key1_ly => $row1_ly){ 
		$penghasilan_ly = $row1_ly['bonus'];
	}
	
}
if($penghasilan > $penghasilan_ly) $vs = 'arrow-up.png'; 
else if ($penghasilan == $penghasilan_ly) $vs = 'equal.png';
else $vs = 'arrow-down.png';
?>
<!--
	<tr>
      <td>1</td>
      <td>Penghasilan</td>
      <td><div align="right"><?php //echo number_format($penghasilan);?></div></td>
      <td><div align="right"><img src="<?=base_url();?>images/<?=$vs?>" style="width:20px; height:20px" /></div></td>
      <td><div align="right"><?php //echo number_format($penghasilan_ly);?></div></td>
    </tr>
-->
<?php 
// reset face
$vs = 'equal.png';
if($periodeSummary == date('Y-m-t',strtotime(date("Y-m-d"))))
{
	$result2=$this->Mactivityreport->getperingkatMember($memberid);
    foreach($result2 as $key2 => $row2){ 
		$peringkat = $row2['jenjang_name'];
		$peringkatid = $row2['jenjang_id'];
	}
}else{
	$result2=$this->Mactivityreport->getperingkatPgs($memberid,$periodeSummary);
    foreach($result2 as $key2 => $row2){ 
		$peringkat = $row2['jenjang_name'];
		$peringkatid = $row2['jenjang_id'];
	}
}
if($this->input->post('periode')){
	$result2_ly=$this->Mactivityreport->getperingkatPgs($memberid,$periodeSummary_ly);
    foreach($result2_ly as $key2_ly => $row2_ly){ 
		$peringkat_ly = $row2_ly['jenjang_name'];
		$peringkatid_ly = $row2_ly['jenjang_id'];
	}
}

if($peringkatid > $peringkatid_ly) $vs = 'arrow-up.png'; 
else if ($peringkatid == $peringkatid_ly) $vs = 'equal.png';
else $vs = 'arrow-down.png';

?>
	<tr>
      <td>1</td>
      <td>Peringkat</td>
      <td><div align="right"><?php echo $peringkat;?></div></td>
      <td><div align="right"><img src="<?=base_url();?>images/<?=$vs?>" style="width:20px; height:20px" /></div></td>
	  <td><div align="right"><?php echo $peringkat_ly;?></div></td>
    </tr>
<?php 
// reset face
$vs = 'equal.png';
if($this->input->post('periode')){
	$result6_ly=$this->Mactivityreport->getpsPgs($memberid,$periodeSummary_ly);
    foreach($result6_ly as $key6_ly => $row6_ly){ 
		$ps_ly = $row6_ly['ps'];
	}
}

if($ps > $ps_ly) $vs = 'arrow-up.png'; 
else if ($ps == $ps_ly) $vs = 'equal.png';
else $vs = 'arrow-down.png';

?>
	<tr>
      <td>2</td>
      <td>Personal Order</td>
      <td><div align="right"><?php echo number_format($ps);?></div></td>
      <td><div align="right"><img src="<?=base_url();?>images/<?=$vs?>" style="width:20px; height:20px" /></div></td>
      <td><div align="right"><?php echo number_format($ps_ly);?></div></td>
    </tr>
<?php
// reset face
$vs = 'equal.png';

	if(date('n',strtotime($periodeSummary)) < 3){
		$dateYear = date('Y',strtotime($periodeSummary)) - 1;
		$startdateQS = $dateYear.'-03-01';
		
		$dateYear_ly = date('Y',strtotime($periodeSummary_ly)) - 1;
		$startdateQS_ly = $dateYear_ly.'-03-01';
	}else{
		$startdateQS = date('Y',strtotime($periodeSummary)).'-03-01';
		$startdateQS_ly = date('Y',strtotime($periodeSummary_ly)).'-03-01';
	}
	$enddateQS = $periodeSummary;
	$enddateQS_ly = $periodeSummary_ly;
	
	//echo $startdateQS .'aaa<br>';
	//echo $enddateQS .'aaa<br>';
	
	$result3=$this->MTracking->getTracking($memberid,$startdateQS,$enddateQS);
    foreach($result3 as $key3 => $row3){ 
		$qstc = $row3['Q1']+$row3['Q2']+$row3['Q3']+$row3['Q4']+$row3['Q5']+$row3['Q6']+$row3['Q7']+$row3['Q8']+$row3['Q9']+$row3['Q10']+$row3['Q11']+$row3['Q12'];
	}

	$result3_ly=$this->MTracking->getTracking($memberid,$startdateQS_ly,$enddateQS_ly);
    foreach($result3_ly as $key3_ly => $row3_ly){ 
		$qstc_ly = $row3_ly['Q1']+$row3_ly['Q2']+$row3_ly['Q3']+$row3_ly['Q4']+$row3_ly['Q5']+$row3_ly['Q6']+$row3_ly['Q7']+$row3_ly['Q8']+$row3_ly['Q9']+$row3_ly['Q10']+$row3_ly['Q11']+$row3_ly['Q12'];
	}
	
if($qstc > $qstc_ly) $vs = 'arrow-up.png'; 
else if ($qstc == $qstc_ly) $vs = 'equal.png';
else $vs = 'arrow-down.png';
?>
	<tr>
      <td>3</td>
      <td>Qualifikasi Stockiest Conference</td>
      <td><div align="right"><?php echo $qstc;?></div></td>
      <td><div align="right"><img src="<?=base_url();?>images/<?=$vs?>" style="width:20px; height:20px" /></div></td>
      <td><div align="right"><?php echo $qstc_ly;?></div></td>
    </tr>
<?php
// reset face
$vs = 'equal.png';

	$startdateQ = date('Y',strtotime($periodeSummary)).'-01-01';
	$enddateQ = $periodeSummary;
	//echo $startdateQ."<br>";
	//echo $enddateQ."<br>";
	$result4=$this->MTracking_mem->getTracking($memberid,$startdateQ,$enddateQ);
    foreach($result4 as $key4 => $row4){ 
		$qleaders = $row4['Q1']+$row4['Q2']+$row4['Q3']+$row4['Q4']+$row4['Q5']+$row4['Q6']+$row4['Q7']+$row4['Q8']+$row4['Q9']+$row4['Q10']+$row4['Q11']+$row4['Q12'];
	}

	$startdateQ_ly = date('Y',strtotime($periodeSummary_ly)).'-01-01';
	$enddateQ_ly = $periodeSummary_ly;
	$result4_ly=$this->MTracking_mem->getTracking($memberid,$startdateQ_ly,$enddateQ_ly);
    foreach($result4_ly as $key4_ly => $row4_ly){ 
		$qleaders_ly = $row4_ly['Q1']+$row4_ly['Q2']+$row4_ly['Q3']+$row4_ly['Q4']+$row4_ly['Q5']+$row4_ly['Q6']+$row4_ly['Q7']+$row4_ly['Q8']+$row4_ly['Q9']+$row4_ly['Q10']+$row4_ly['Q11']+$row4_ly['Q12'];
	}
	
if($qleaders > $qleaders_ly) $vs = 'arrow-up.png'; 
else if ($qleaders == $qleaders_ly) $vs = 'equal.png';
else $vs = 'arrow-down.png';
?>
	<tr>
      <td>4</td>
      <td>Qualifikasi Leaders Conference</td>
      <td><div align="right"><?php echo $qleaders;?></div></td>
      <td><div align="right"><img src="<?=base_url();?>images/<?=$vs?>" style="width:20px; height:20px" /></div></td>
      <td><div align="right"><?php echo $qleaders_ly;?></div></td>
    </tr>
<?php 
// reset face
$vs = 'equal.png';
if($this->input->post('periode')){
	$result7_ly=$this->Mactivityreport->getpsPgs($memberid,$periodeSummary_ly);
    foreach($result7_ly as $key7_ly => $row7_ly){ 
		$pvg_ly = $row7_ly['tgpv'];
	}
}

if($pvg > $pvg_ly) $vs = 'arrow-up.png'; 
else if ($pvg == $pvg_ly) $vs = 'equal.png';
else $vs = 'arrow-down.png';

?>
	<tr>
      <td>5</td>
      <td>Personal Group PV</td>
      <td><div align="right"><?php echo number_format($pvg);?></div></td>
      <td><div align="right"><img src="<?=base_url();?>images/<?=$vs?>" style="width:20px; height:20px" /></div></td>
      <td><div align="right"><?php echo number_format($pvg_ly);?></div></td>
    </tr>
<?php
// reset face
$vs = 'equal.png';

	$showdateM = date('Y-m',strtotime($periodeSummary));
	$showdateM_ly =  date('Y-m',strtotime($periodeSummary." - 1 Year"));
	if($this->input->post('periode')){
		$result8=$this->Mactivityreport->get_sf_count($showdateM,$lft,$rgt,$level);
		foreach($result8 as $key8 => $row8){ 
			$sfm = $row8['jml'];
		}
		
		$result8_ly=$this->Mactivityreport->get_sf_count($showdateM_ly,$lft,$rgt,$level);
		foreach($result8_ly as $key8_ly => $row8_ly){ 
			$sfm_ly = $row8_ly['jml'];
		}
	}
if($sfm > $sfm_ly) $vs = 'arrow-up.png'; 
else if ($sfm == $sfm_ly) $vs = 'equal.png';
else $vs = 'arrow-down.png';

?>
	<tr>
	<tr>
      <td>6</td>
      <td>Sales Force in Personal Group</td>
      <td><div align="right"><?php echo number_format($sfm); //echo number_format($sf);?></div></td>
      <td><div align="right"><img src="<?=base_url();?>images/<?=$vs?>" style="width:20px; height:20px" /></div></td>
      <td><div align="right"><?php echo number_format($sfm_ly);?></div></td>
    </tr>
<?php
// reset face
$vs = 'equal.png';

	$showdateM = date('Y-m',strtotime($periodeSummary));
	$showdateM_ly =  date('Y-m',strtotime($periodeSummary." - 1 Year"));
	$rowM=$this->MTree->get_member($memberid);
	if($this->input->post('periode')){
		$result9=$this->Mactivityreport->get_sp_count($showdateM,$lft,$rgt,$level);
		foreach($result9 as $key9 => $row9){ 
			$spm = $row9['jml'];
		}
		
		$result9_ly=$this->Mactivityreport->get_sp_count($showdateM_ly,$lft,$rgt,$level);
		foreach($result9_ly as $key9_ly => $row9_ly){ 
			$spm_ly = $row9_ly['jml'];
		}
	}
if($spm > $spm_ly) $vs = 'arrow-up.png'; 
else if ($spm == $spm_ly) $vs = 'equal.png';
else $vs = 'arrow-down.png';

?>
	<tr>
      <td>7</td>
      <td>Sponsoring in Personal Group</td>
      <td><div align="right"><?php echo number_format($spm);?></div></td>
      <td><div align="right"><img src="<?=base_url();?>images/<?=$vs?>" style="width:20px; height:20px" /></div></td>
      <td><div align="right"><?php echo number_format($spm_ly);?></div></td>
    </tr>
<?php
// reset face
$vs = 'equal.png';

	$showdateM = date('Y-m',strtotime($periodeSummary));
	$showdateM_ly =  date('Y-m',strtotime($periodeSummary." - 1 Year"));
	$rowM=$this->MTree->get_member($memberid);
	if($this->input->post('periode')){
		$result10=$this->Mactivityreport->get_nm_count($showdateM,$lft,$rgt,$level);
		foreach($result10 as $key10 => $row10){ 
			$nmm = $row10['jml'];
		}
		
		$result10_ly=$this->Mactivityreport->get_nm_count($showdateM_ly,$lft,$rgt,$level);
		foreach($result10_ly as $key10_ly => $row10_ly){ 
			$nmm_ly = $row10_ly['jml'];
		}
	}
if($nmm > $nmm_ly) $vs = 'arrow-up.png'; 
else if ($nmm == $nmm_ly) $vs = 'equal.png';
else $vs = 'arrow-down.png';

?>

	<tr>
      <td>8</td>
      <td>New Member</td>
      <td><div align="right"><?php echo number_format($nmm); // echo number_format($nm);?></div></td>
      <td><div align="right"><img src="<?=base_url();?>images/<?=$vs?>" style="width:20px; height:20px" /></div></td>
      <td><div align="right"><?php echo number_format($nmm_ly); // echo number_format($nm);?></div></td>
    </tr>
<?php
// reset face
$vs = 'equal.png';

	$showdateM = date('Y-m',strtotime($periodeSummary));
	$showdateM_ly =  date('Y-m',strtotime($periodeSummary." - 1 Year"));
	$rowM=$this->MTree->get_member($memberid);
	if($this->input->post('periode')){
		$result11=$this->Mactivityreport->get_nr_count($showdateM,$lft,$rgt,$level);
		foreach($result11 as $key11 => $row11){ 
			$nrm = $row11['jml'];
		}
		
		$result11_ly=$this->Mactivityreport->get_nr_count($showdateM_ly,$lft,$rgt,$level);
		foreach($result11_ly as $key11_ly => $row11_ly){ 
			$nrm_ly = $row11_ly['jml'];
		}
	}
if($nrm > $nrm_ly) $vs = 'arrow-up.png'; 
else if ($nrm == $nrm_ly) $vs = 'equal.png';
else $vs = 'arrow-down.png';

?>
	<tr>
      <td>9</td>
      <td>New Recruits</td>
      <td><div align="right"><?php echo number_format($nrm);// echo number_format($nr);?></div></td>
      <td><div align="right"><img src="<?=base_url();?>images/<?=$vs?>" style="width:20px; height:20px" /></div></td>
      <td><div align="right"><?php echo number_format($nrm_ly);// echo number_format($nr);?></div></td>
    </tr>
<?php
// reset face
$vs = 'equal.png';

	$showdateM = date('Y-m',strtotime($periodeSummary));
	$showdateM_ly =  date('Y-m',strtotime($periodeSummary." - 1 Year"));
	$rowM=$this->MTree->get_member($memberid);
	if($this->input->post('periode')){
		$result12=$this->Mactivityreport->get_nrq_count($showdateM,$lft,$rgt,$level);
		foreach($result12 as $key12 => $row12){ 
			$nrqm = $row12['jml'];
		}
		
		$result12_ly=$this->Mactivityreport->get_nrq_count($showdateM_ly,$lft,$rgt,$level);
		foreach($result12_ly as $key12_ly => $row12_ly){ 
			$nrqm_ly = $row12_ly['jml'];
		}
	}
if($nrqm > $nrqm_ly) $vs = 'arrow-up.png'; 
else if ($nrqm == $nrqm_ly) $vs = 'equal.png';
else $vs = 'arrow-down.png';

?>
	<tr>
      <td>10</td>
      <td>New Qualified Recruits</td>
      <td><div align="right"><?php echo number_format($nrqm); //  echo number_format($qrecruit);?></div></td>
      <td><div align="right"><img src="<?=base_url();?>images/<?=$vs?>" style="width:20px; height:20px" /></div></td>
      <td><div align="right"><?php echo number_format($nrqm_ly); //  echo number_format($qrecruit);?></div></td>
    </tr>
<?php
// reset face
$vs = 'equal.png';

	$showdateM = date('Y-m',strtotime($periodeSummary));
	$showdateM_ly =  date('Y-m',strtotime($periodeSummary." - 1 Year"));
	$rowM=$this->MTree->get_member($memberid);
	if($this->input->post('periode')){
		$result5=$this->Mactivityreport->get_active_count($showdateM,$lft,$rgt,$level);
		foreach($result5 as $key5 => $row5){ 
			$actm = $row5['jml'];
		}
		
		$result5_ly=$this->Mactivityreport->get_active_count($showdateM_ly,$lft,$rgt,$level);
		foreach($result5_ly as $key5_ly => $row5_ly){ 
			$actm_ly = $row5_ly['jml'];
		}
	}
if($actm > $actm_ly) $vs = 'arrow-up.png'; 
else if ($actm == $actm_ly) $vs = 'equal.png';
else $vs = 'arrow-down.png';

?>
	<tr>
      <td>11</td>
      <td>Active Member</td>
      <td><div align="right"><?php echo number_format($actm);?></div></td>
      <td><div align="right"><img src="<?=base_url();?>images/<?=$vs?>" style="width:20px; height:20px" /></div></td>
      <td><div align="right"><?php echo number_format($actm_ly);?></div></td>
    </tr>
<?php
// reset face
$vs = 'equal.png';

	$showdateM = date('Y-m',strtotime($periodeSummary));
	$showdateM_ly =  date('Y-m',strtotime($periodeSummary." - 1 Year"));
	$rowM=$this->MTree->get_member($memberid);
	if($this->input->post('periode')){
		$result6=$this->Mactivityreport->get_inactive_count($showdateM,$lft,$rgt,$level);
		foreach($result6 as $key6 => $row6){ 
			$inactm = $row6['jml'];
		}
		
		$result6_ly=$this->Mactivityreport->get_inactive_count($showdateM_ly,$lft,$rgt,$level);
		foreach($result6_ly as $key6_ly => $row6_ly){ 
			$inactm_ly = $row5_ly['jml'];
		}
	}
if($inactm > $inactm_ly) $vs = 'arrow-up.png'; 
else if ($inactm == $inactm_ly) $vs = 'equal.png';
else $vs = 'arrow-down.png';
?>
	<tr>
      <td>12</td>
      <td>Inactive Member</td>
      <td><div align="right"><?php echo number_format($inactm);?></div></td>
      <td><div align="right"><img src="<?=base_url();?>images/<?=$vs?>" style="width:20px; height:20px" /></div></td>
      <td><div align="right"><?php echo number_format($inactm_ly);?></div></td>
    </tr>
<?php
// reset face
$vs = 'equal.png';

	$showdateM = date('Y-m',strtotime($periodeSummary));
	$showdateM_ly =  date('Y-m',strtotime($periodeSummary." - 1 Year"));
	$rowM=$this->MTree->get_member($memberid);
	if($this->input->post('periode')){
		$result7=$this->Mactivityreport->get_reactive_count($showdateM,$lft,$rgt,$level);
		foreach($result7 as $key7 => $row7){ 
			$reactm = $row7['jml'];
		}
		
		$result7_ly=$this->Mactivityreport->get_reactive_count($showdateM_ly,$lft,$rgt,$level);
		foreach($result7_ly as $key7_ly => $row7_ly){ 
			$reactm_ly = $row7_ly['jml'];
		}
	}
if($reactm > $reactm_ly) $vs = 'arrow-up.png'; 
else if ($reactm == $reactm_ly) $vs = 'equal.png';
else $vs = 'arrow-down.png';
?>
	<tr>
      <td>13</td>
      <td>Reactivate Member</td>
      <td><div align="right"><?php echo number_format($reactm);?></div></td>
      <td><div align="right"><img src="<?=base_url();?>images/<?=$vs?>" style="width:20px; height:20px" /></div></td>
      <td><div align="right"><?php echo number_format($reactm_ly);?></div></td>
    </tr>

	<!--
	<tr>

      <td>9</td>

      <td>Catalog per Sales Force</td>

      <td><div align="right"><?php echo "-";?></div></td>

	  <td></td>

    </tr>
	-->
</table>
<hr />
<p class="cName"> 
<b>Keterangan</b>
<table>
<tr><td>1</td><td>Personal Group</td><td>:</td><td>Member yang disponsori baik langsung maupun tidak langsung, namun tidak termasuk jaringan member leader kualifikasi 60.000.000 TGPV</td></tr>	
<tr><td>2</td><td>Peringkat</td><td>:</td><td>Jenjang Karir Member</td></tr>	
<tr><td>3</td><td>Personal Order</td><td>:</td><td>Pembelanjaan pribadi</td></tr>	
<tr><td>4</td><td>Qualifikasi Stockiest Conference</td><td>:</td><td>Jumlah Kualifikasi Stockiest untuk keberangkatan stockiest conference</td></tr>	
<tr><td>5</td><td>Qualifikasi Leaders Conference</td><td>:</td><td>Jumlah Kualifikasi Member untuk keberangkatan Leaders conference</td></tr>	
<tr><td>6</td><td>Personal Group PV</td><td>:</td><td>Pembelanjaan Personal Group</td></tr>
<tr><td>7</td><td>Sales Force in Personal Group</td><td>:</td><td>Member (lama dan baru) yang berbelanja dibulan itu dalam personal group (diluar yang break)</td></tr>
<tr><td>8</td><td>Sponsoring in Personal Group</td><td>:</td><td>Member (lama dan baru) yang berbelanja dibulan itu dalam personal group (diluar yang break)</td></tr>
<tr><td>9</td><td>New Member</td><td>:</td><td>Member baru dalam personal group</td></tr>
<tr><td>10</td><td>New Recruits</td><td>:</td><td>Member baru yang belanja (PV product) dalam personal group</td></tr><br />
<tr><td>11</td><td>New Qualified Recruits</td><td>:</td><td>Member baru yang belanja (pv product) >= 1 Juta PV dalam personal group</td></tr>
<tr><td>12</td><td>Member Active</td><td>:</td><td>Member yang berbelanja selama 3 bulan terakhir dalam personal group</td></tr>
<tr><td>13</td><td>Inactive Member</td><td>:</td><td>Member yang tidak berbelanja lebih dari 3 bulan dalam personal group</td></tr>
<tr><td>14</td><td>Reactive Member</td><td>:</td><td>Member Inactive yang kembali berbelanja selama 3 bulan terakhir dalam personal group</td></tr>
</table>
</p>
<?php $this->load->view('footer');?>