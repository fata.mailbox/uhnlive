<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php 
	/* Update by Boby 20140819 */
	if ($this->session->flashdata('message')){
		echo "<div class='message'>".$this->session->flashdata('message')."</div>";
	}
	/* End update by Boby 20140819 */
	
	/* Updated by Boby 20150102 */
	if($blocked==0){
		echo anchor('member/memprofile/edit/'.$row['id'],'(Edit Profile)');
	//if($this->session->userdata('group_id')==1){
		echo anchor('member/bank/edit/'.$row['id'],'  (Edit Bank Account)');
		echo anchor('member/pph21/edit/'.$row['id'],'  (Edit KK Information)');
		echo anchor('member/regodp/edit/'.$row['id'],'  (Reg ODP)'); // update by Boby 20110328
	}
	/* End updated by Boby 20150102 */
	//}	
	echo "<br /><br />";
?>
		
<table width="100%">
	<tr>
		<td colspan="6"><hr /></td>
	</tr>
	<tr>
		<td colspan="6"><b>DATA PENDAFTARAN</b></td>
	</tr>
	<tr>
		<td colspan="6"><hr /></td>
	</tr>
    <tr>
		<td colspan="6">
        	<table width="100%" style="font-size:medium">
	<tr>
		<td width='9%'>Tanggal Daftar</td>
		<td width='1%'>:</td>
		<td width='17%'><?php echo $row['ftglaplikasi'];?></td>
		<td width='17%'>Stockiest ID / Nama</td>
		<td width='1%'>:</td>
		<td width='19%'><?php if($row['stockiest_id'] == 'SH0000') echo "KANTOR UHN"; else echo $row['no_stc']." / ".$row['namastc'];?></td>
		<td width='17%'>Kelengkapan Data</td>
		<td width='1%'>:</td>
		<td width='18%'><?php if($row['lengkap'] == '1') echo "LENGKAP"; else echo "TIDAK LENGKAP";?></td>
	</tr>
    <!-- 20160913 ASP Start -->
    <tr>
    	<td colspan="8"></td>
    	<td><?php if($row['form'] == '1') echo "- FORMULIR (ADA)"; else echo "- <font color='red'>FORMULIR (TIDAK ADA)</font>";?></td>
    </tr>
    <tr>
    	<td colspan="8"></td>
    	<td><?php if($row['rek'] == '1') echo "- FC REKENING (ADA)"; else echo "- <font color='red'>FC REKENING (TIDAK ADA)</font>";?></td>
    </tr>
    <tr>
    	<td colspan="8"></td>
    	<td><?php if($row['ktp'] == '1') echo "- FC KTP (ADA)"; else echo "- <font color='red'>FC KTP (TIDAK ADA)</font>";?></td>
    </tr>
            </table>	
        </td>
	</tr>
    <!-- 20160913 ASP END  -->
	<tr>
		<td colspan="6"><hr /></td>
	</tr>
	<tr>
		<td colspan="6"><b>DATA PRIBADI ANDA</b></td>
	</tr>
	<tr>
		<td colspan="6"><hr /></td>
	</tr>
	<tr>
		<td>Member ID / Nama</td>
		<td>:</td>
		<td><?php echo $row['id']." / ".$row['nama'];?></td>
		<td>Tempat Lahir</td>
		<td>:</td>
		<td><?php echo $row['tempatlahir'];?></td>
	</tr>
	<tr>
		<td>Jenis Kelamin</td>
		<td>:</td>
		<td><?php echo $row['jk'];?></td>
		<td>Tanggal Lahir</td>
		<td>:</td>
		<td><?php echo $row['ftgllahir'];?></td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td>:</td>
		<td><?php echo $row['alamat'];?></td>
		<td>No. Telp</td>
		<td>:</td>
		<td><?php echo $row['telp'];?></td>
	</tr>
	<tr>
		<td>Kelurahan</td>
		<td>:</td>
		<td><?php echo $row['kelurahan'];?></td>
		<td>No. Fax</td>
		<td>:</td>
		<td><?php echo $row['fax'];?></td>
	</tr>
	<tr>
		<td>Kecamatan</td>
		<td>:</td>
		<td><?php echo $row['kecamatan'];?></td>
		<td>No. HP</td>
		<td>:</td>
		<td><?php echo $row['hp'];?></td>
	</tr>
	<tr>
		<td>Kota</td>
		<td>:</td>
		<td><?php echo $row['kota'];?></td>
		<td>Email</td>
		<td>:</td>
		<td><?php echo $row['email'];?></td>
	</tr>
	<tr>
		<td>Kode Pos</td>
		<td>:</td>
		<td><?php echo $row['kodepos'];?></td>
		<td>Ahliwaris</td>
		<td>:</td>
		<td><?php echo $row['ahliwaris'];?></td>
	</tr>
	<tr>
		<td>Propinsi</td>
		<td>:</td>
		<td><?php echo $row['propinsi'];?></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>No. KTP / SIM</td>
		<td>:</td>
		<td><?php echo $row['noktp'];?></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td colspan="6"><hr /></td>
	</tr>
	<?php /*modified by Boby 2010-01-25*/ ?>
	<tr>
		<td colspan="6"><b>DATA NPWP</b></td>
	</tr>
	<tr>
		<td colspan="6"><hr /></td>
	</tr>
	<tr>
		<td>No. NPWP </td>
		<td>:</td>
		<td><?php $str = (strlen($row1['npwp'])>0)? $row1['npwp'] : '-'; echo $str;?></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>Status menikah</td>
		<td>:</td>
		<td><?php if(strlen($row1['menikah'])>0){if($row1['menikah']==1){echo 'Menikah';}else{echo 'Single';}}else{echo '-';}?></td>
		<td>Bekerja</td>
		<td>:</td>
		<td><?php if(strlen($row1['office']>0)){if($row1['office']==1){echo 'Ya';}else{echo 'Tidak';}}else{echo '-';}?></td>
	</tr>
	<tr>
		<td>Jumlah anak</td>
		<td>:</td>
		<td><?php $str = (strlen($row1['anak'])>0)? $row1['anak'] : '-'; echo $str;?></td>
		<td>Perusahaan tempat bekerja</td>
		<td>:</td>
		<td><?php $str = (strlen($row1['office'])>0)? $row1['office'] : '-'; echo $str;?></td>
	</tr>
	<?php /*end modified by Boby 2010-01-25*/ ?>
	<tr>
		<td colspan="6"><hr /></td>
	</tr>
	<tr>
		<td colspan="6"><b>DATA BANK</b></td>
	</tr>
	<tr>
		<td colspan="6"><hr /></td>
	</tr>
	<tr>
		<td>Nama Bank</td>
		<td>:</td>
		<td><?php echo $row['namabank'];?></td>
		<td>Nama Nasabah</td>
		<td>:</td>
		<td><?php echo $row['namanasabah'];?></td>
	</tr>
	<tr>
		<td>Cabang</td>
		<td>:</td>
		<td><?php echo $row['area'];?></td>
		<td>No. Rekening</td>
		<td>:</td>
		<td><?php echo $row['no'];?></td>
	</tr>
	<tr>
		<td colspan="6"><hr /></td>
	</tr>
	<tr>
		<td colspan="6"><b>DATA SPONSOR</b></td>
	</tr>
	<tr>
		<td colspan="6"><hr /></td>
	</tr>
	<tr>
		<td>Member ID</td>
		<td>:</td>
		<td><?php echo $row['enroller_id'];?></td>
		<td>Nama Sponsor</td>
		<td>:</td>
		<td><?php echo $row['namaenroller'];?></td>
	</tr>
	<tr>
		<td colspan="6"><hr /></td>
	</tr>
	<tr>
		<td colspan="6"><b>DATA PENEMPATAN</b></td>
	</tr>
	<tr>
		<td colspan="6"><hr /></td>
	</tr>
	<tr>
		<td>Member ID</td>
		<td>:</td>
		<td><?php echo $row['sponsor_id'];?></td>
		<td>Nama</td>
		<td>:</td>
		<td><?php echo $row['namasponsor'];?></td>
	</tr>
</table>
<?php $this->load->view('footer');?>