<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="100%">
<?php echo form_open('member/memactive/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
        <tr>
			<td valign='top' width="19%">Keterangan</td>
			<td valign='top' width="1%">:</td>
			<td width="80%"><input type="radio" name="flag" value="tupo" <?php echo set_radio('flag', 'tupo', TRUE); ?> /> Member Belanja
				<input type="radio" name="flag" value="aktip" <?php echo set_radio('flag', 'aktip'); ?> /> Member Aktip (1 tahun ada belanja) 
				<input type="radio" name="flag" value="target" <?php echo set_radio('flag', 'target'); ?> /> Pencapaian Target (>= 40jt)<br />
</td>
		</tr>
        <tr><td colspan="2">&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td></tr>
         <?php echo form_close();?>
         
    <tr><td colspan="3"><hr /></td></tr>
    </table>
	
    <?php if($this->input->post('flag') == 'aktip'){ ?>
    <table class="stripe">
	<tr>
      <th width='5%'>No.</th>
      <th width='10%'>ID Member</th>
      <th width='20%'>Nama Member</th>
      <th width='10%'>Peringkat</th>
      <th width='20%'><div align="right">PV</div></th>
    </tr>
   
<?php
if ($results):
	foreach($results as $key => $row):?>
    <tr>
		<td><?php echo $row['i'];?></td>
		<td><?php echo $row['member_id'];?></td>
		<td><?php echo $row['nama'];?></td>
		<td><?php echo $row['jenjang'];?></td>
		<td align="right"><?php echo $row['fps'];?></td>
    </tr>
    <?php endforeach; ?>
    <!-- 
	<tr>
		<td colspan="4"><b>Total</b></td>
		<td align="right"><b><?php echo $total->fps;?></b></td>
    </tr> -->
<?php else: ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<?php }elseif($this->input->post('flag') == 'target'){ ?>
    <table class="stripe">
	<tr>
      <th width='5%'>No.</th>
      <th width='10%'>ID Member</th>
      <th width='20%'>Nama Member</th>
      <th width='10%'>Peringkat</th>
      <th width='20%'><div align="right">TGPV</div></th>
    </tr>
   
<?php
if ($results):
	foreach($results as $key => $row):?>
    <tr>
		<td><?php echo $row['i'];?></td>
		<td><?php echo $row['member_id'];?></td>
		<td><?php echo $row['nama'];?></td>
		<td><?php echo $row['jenjang'];?></td>
		<td align="right"><?php echo $row['fpgs'];?></td>
    </tr>
    <?php endforeach; ?>
    <!-- 
	<tr>
		<td colspan="4"><b>Total</b></td>
		<td align="right"><b><?php echo $total->fps;?></b></td>
    </tr>
    -->
<?php else: ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>			
<?php }else{?>
    <table class="stripe">
	<tr>
      <th width='5%'>No.</th>
      <th width='10%'>ID Member</th>
      <th width='20%'>Nama Member</th>
      <th width='10%'>Peringkat</th>
      <th width='20%'><div align="right">PV</div></th>
    </tr>
   
<?php
if ($results):
	foreach($results as $key => $row):?>
    <tr>
		<td><?php echo $row['i'];?></td>
		<td><?php echo $row['member_id'];?></td>
		<td><?php echo $row['nama'];?></td>
		<td><?php echo $row['jenjang'];?></td>
		<td align="right"><?php echo $row['fps'];?></td>
    </tr>
    <?php endforeach; ?>
    <!-- 
	<tr>
		<td colspan="4"><b>Total</b></td>
		<td align="right"><b><?php echo $total->fps;?></b></td>
    </tr> -->
<?php else: ?>
    <tr>
      <td colspan="6">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>			
<?php }?>
                
                
<?php $this->load->view('footer');?>
