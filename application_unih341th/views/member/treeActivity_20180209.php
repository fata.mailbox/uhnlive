<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<hr />

<?php echo form_open('member/activity/view', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off')); ?>
	<table width="55%">
		<tr>
			<td width="25%">Member ID</td>
			<td width="1%">:</td>
			<td width="74%"><?php 
				$data = array('name'=>'member_id','id'=>'member_id','maxlength'=>'20','size'=>'11','value'=>set_value('member_id'));
				echo form_input($data);
				$atts = array(
				  'width'      => '450',
				  'height'     => '500',
				  'scrollbars' => 'yes',
				  'status'     => 'yes',
				  'resizable'  => 'yes',
				  'screenx'    => '0',
				  'screeny'    => '0'
				);
				echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			</td>
		</tr>
		<tr>
			<td>Nama Member</td>
			<td>:</td>
			<td><?php 
				$data = array('name'=>'name','id'=>'name','maxlength'=>'20','readonly'=>'1','value'=>set_value('name'));
				echo form_input($data); 
				?>
			</td>
		</tr>                    
		<tr>
			<td>Periode</td>
			<td>:</td>
			<td><?php echo form_dropdown('periode',$periode,set_value('periode')); ?></td>                        
		</tr>
		<!-- 
		<tr>
			<td>Level ?</td>
			<td>:</td>
			<td><?php $data = array(''=>'All','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10');
			echo form_dropdown('level',$data); ?></td>
		</tr>
		-->
					<?php
					/* Updated by Boby 20150519 */
					// $sl['jenjang_id'] = 6;
					if($sl['jenjang_id']>5){
					?>
					<tr>
                        	<td>Jenjang</td>
                            <td>:</td>
                            <td><?php echo $sl['jenjang']; ?></td>
                    </tr>
					<tr>
                        	<td>TGPV Samping</td>
                            <td>:</td>
                            <td><?php echo number_format($sl['lq_tgpv']); ?></td>                        
                    </tr>
					<tr>
                        	<td>Car Qualified</td>
                            <td>:</td>
                            <td><?php echo $sl['note']; ?></td>                        
                    </tr>
					<?php
					}
					/* Updated by Boby 20150519 */
					?>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<?php 
					echo " ".form_submit('submit','Preview'); 
					if($result){ echo '&nbsp;'.anchor($hplink,'download');}
				?>
			</td>
		</tr>
	</table>				
<?php	echo form_close();?>
	<link rel="StyleSheet" href="<?php echo base_url();?>/dtree/dtree.css" type="text/css" />
	<script type="text/javascript" src="<?php echo base_url();?>/dtree/dtree.js"></script>
	
<div class="dtree">
	<p class="cName"><a href="javascript: d.openAll();">Buka</a> | <a href="javascript: d.closeAll();">Tutup</a> <!--| <a href="javascript: d.openTo(2, true);">Open --></p>

	<script type="text/javascript">
		<!--		
		
		d = new dTree('d');
		d.config.useIcons=false;
		d.config.useCookies=false;
		d.config.inOrder=true;
		
		<?php if($result){ 
		$level_ = 0; 
		$node = 0; 
		/* Created by Takwa 20141111 */

			$ps = 0;	// Personal sales

			$pvg = 0;	// PV Group

			$am = 0;	// active member

			$nm = 0;	// new member

			$nr = 0;	// new member blj

			$qrecruit = 0; // new member blj 1 jt

			$sf = 0;	// sales force, titik yg blj

			$im = 0; 	// inactive member

			/* End created by Takwa 20141111 */
		foreach($result as $key => $row){ 
		if($key == 0){ 
		/* Created by Takwa 20141111 */

			// active member

			if($row['act']=='active'){

				$am=$am+1;

			}else{

				$im+=1;

			}

			

			// new member

			if($row['newMember']==2){

				$nr+=1;

				if($row['fps']>=1000000) $qrecruit+=1;

			}else if($row['newMember']==1){

				$nm+=1;

			}

			

			// Sales Force

			if($row['fps']>0) $sf+=1;

			/* End created by Takwa 20141111 */
		?>
		<?php
		if($row['qualified']=='qualified green') {
		?>
		d.add('0','-1','<img src="<?=base_url();?>images/backend/<?=$row['jenjang_id'];?>i.png" height="17"><span class="qualified"><?php
				$ps=$row['fps'];
				$pvg=$row['TGPV']; 
				/* Updated by Boby 20131024 */
				/* Updated by Boby 20131120 */
				if($row['note']==1){
					$a="*";
					$level = $level_+1; 
					if($node==0){
						$node++;
						$parent = $row['parentid']; 
					}else{
						$parent = $parent; 
					}
				}else{
					$a="";
					$level = $row['level']; 
					$parent = $row['parentid']; 
					$node = 0; 
				}
				/* End updated by Boby 20131120 */
				/* End updated by Boby 20131024 */		
																		 
?><table class="stripe" border="1" style="font-family:Jill Sans MT;"><tr class="qualified green"><td>Id member</td><td>Level jaringan</td><td>Nama</td><td>hp</td><td>Personal PV</td><td>Tgl bergabung</td><td>Peringkat</td><td>TGPV</td><td>Status</td><td>Rekrutmen</td><td>Jumlah Leader</td><td>Jumlah Break Away</td></tr><tr><td><?=$row['member_id'];?></td><td><?=$level;?></td><td><?php echo $a.$row['name']; ?></td><td><?=$row['hp'];?></td><td><?=$row['fps'];?></td><td><?=$row['joindate'];?></td><td><?=$row['jenjang'];?></td><td><?=$row['TGPV'];?></td><td><?=$row['act'];?></td><td><?=$row['rekrut'];?></td><td><?=$row['qty_all_leader'];?></td><td><?=$row['qty_leg_leader'];?></td></tr><tr class="qualified green"><td>BPP</td><td>Bonus Aktivasi</td><td colspan="100%"></td></tr><tr><td><?php echo number_format($row['bpp']); ?></td><td><?php echo number_format($row['aktifasi']); ?></td><td colspan="100%"></td></tr></table><?php
		$level_ = $level;
		$parent = $row['auto_id'];
		?>');
		<?php
		/*
BPP 9%
Bonus Aktivasi
Bonus Kesuksesan Sponsor 2%
Bonus Kesuksesan Uplinne 4%
Bonus Executive 1%
Bonus SL 1.5 %
Bonus GL 1.75 %
Bonus Exceutive SL 1.87%
Bonus Star 2%
Bonus leadership
Car bonus
Car cash bonus
		*/
		}else{
		?>
			d.add('0','-1','<img src="<?=base_url();?>images/backend/<?=$row['jenjang_id'];?>i.png" height="17"><span class="<?=$row['qualified'];?>"><?php 
				$ps=$row['fps'];
				$pvg=$row['TGPV']; 
				/* Updated by Boby 20131024 */
				/* Updated by Boby 20131120 */
				if($row['note']==1){
					$a="*";
					$level = $level_+1; 
					if($node==0){
						$node++;
						$parent = $row['parentid']; 
					}else{
						$parent = $parent; 
					}
				}else{
					$a="";
					$level = $row['level']; 
					$parent = $row['parentid']; 
					$node = 0; 
				}
				/* End updated by Boby 20131120 */
				/* End updated by Boby 20131024 */
				echo $row['sponsorId']." | ".$level." | ".$row['member_id']." | ".$a.$row['name']."(".$row['hp'].") | ps:".$row['fps']." | ".$row['joindate']." | "
				.$row['jenjang']." | tgpv:".$row['TGPV']." | bpp:".number_format($row['bpp'])." | bns aktifasi:".number_format($row['aktifasi'])." | "
				.$row['act']." | rekrut:".$row['rekrut']." | qal:".$row['qty_all_leader']." | qll:".$row['qty_leg_leader'];
				$level_ = $level;
				$parent = $row['auto_id']; 
				?>');
			<?php 
			}
			?>
		<?php 
		}else{
			/* Created by Takwa 20141111 */

			// active member

			if($row['act']=='active'){

				$am=$am+1;

			}else{

				$im+=1;

			}

			

			// new member

			if($row['newMember']==2){

				$nr+=1;

				if($row['fps']>=1000000) $qrecruit+=1;

			}else if($row['newMember']==1){

				$nm+=1;

			}

			

			// Sales Force

			if($row['fps']>0) $sf+=1;

			/* End created by Takwa 20141111 */
				/* Updated by Boby 20131024 */
				/* Updated by Boby 20131120 */
				if($row['note']==1){
					$a="*";
					$level = $level_+1; 
					if($node==0){
						$node++;
						$parent = $row['parentid']; 
					}else{
						$parent = $parent; 
					}
				}else{
					$a="";
					$level = $row['level']; 
					$parent = $row['parentid']; 
					$node = 0; 
				}
				/* End updated by Boby 20131120 */
				/* Updated by Boby 20131024 */
		?>
			<?php
			if($row['qualified']=='qualified green') {
			?>
			d.add(<?php echo $row['auto_id'];?>,<?php echo $parent;?>,'<img src="<?=base_url();?>images/backend/<?=$row['jenjang_id'];?>i.png" height="17"><span class="qualified"><table class="stripe" border="1" style="font-family:Jill Sans MT;"><tr class="qualified green"><td>Id member</td><td>Level jaringan</td><td>Nama</td><td>hp</td><td>Personal PV</td><td>Tgl bergabung</td><td>Peringkat</td><td>TGPV</td><td>Status</td><td>Rekrutmen</td><td>Jumlah Leader</td><td>Jumlah Break Away</td></tr><tr><td><?=$row['member_id'];?></td><td><?=$level;?></td><td><?php echo $a.$row['name']; ?></td><td><?=$row['hp'];?></td><td><?=$row['fps'];?></td><td><?=$row['joindate'];?></td><td><?=$row['jenjang'];?></td><td><?=$row['TGPV'];?></td><td><?=$row['act'];?></td><td><?=$row['rekrut'];?></td><td><?=$row['qty_all_leader'];?></td><td><?=$row['qty_leg_leader'];?></td></tr><tr class="qualified green"><td>BPP</td><td>Bonus Aktivasi</td><td colspan="100%"></td></tr><tr><td><?php echo number_format($row['bpp']); ?></td><td><?php echo number_format($row['aktifasi']); ?></td><td colspan="100%"></td></tr></table><?php
			$level_ = $level;
			$parent = $row['auto_id'];
			?>');
			<?php
			/*
			BPP 9%
			Bonus Aktivasi
			Bonus Kesuksesan Sponsor 2%
			Bonus Kesuksesan Uplinne 4%
			Bonus Executive 1%
			Bonus SL 1.5 %
			Bonus GL 1.75 %
			Bonus Exceutive SL 1.87%
			Bonus Star 2%
			Bonus leadership
			Car bonus
			Car cash bonus
			*/
			}else{
			?>
			d.add(<?php echo $row['auto_id'];?>,<?php echo $parent;?>,'<img src="<?=base_url();?>images/backend/<?=$row['jenjang_id'];?>i.png" height="17"><span class="<?=$row['qualified'];?>"><?php 
				
				echo $row['sponsorId']." | ".$level." | ".$row['member_id']." | ".$a.$row['name']."(".$row['hp'].") | ps:".$row['fps']." | ".$row['joindate']." | "
				.$row['jenjang']." | tgpv:".$row['TGPV']." | bpp:".number_format($row['bpp'])." | bns aktifasi:".number_format($row['aktifasi'])." | "
				.$row['act']." | rekrut:".$row['rekrut']." | qal:".$row['qty_all_leader']." | qll:".$row['qty_leg_leader'];
				$level_ = $level;
				$parent = $row['auto_id']; 
				?>');
			<?php 
			}
			?>
		<?php }?>
		
		<?php }}else{ ?>d.add(<?php echo '0';?>,'-1','Maaf, member diatas upline atau crossline anda.');<?php }?>
		document.write(d);

		//-->
	</script>
    <p class="cName"><a href="javascript: d.openAll();">Buka</a> | <a href="javascript: d.closeAll();">Tutup</a><br />
	<!-- Note: Level | Member ID | Nama Member | Posisi Sekarang | PV Pribadi | TGPV | APGS</p> -->
</div>
<h2>Summary of Activity Report</h2>

<table class="stripe">

	<tr>

      <th width='50'>No.</th>

      <th width='200'>Description</th>

      <th width='50'><div align="right">Total</div></th>

	  <td>&nbsp;</td>

    </tr>

	<tr>

      <td>1</td>

      <td>Personal Order</td>

      <td><div align="right"><?php echo number_format($ps);?></div></td>

	  <td></td>

    </tr>

	<tr>

      <td>2</td>

      <td>Group PV</td>

      <td><div align="right"><?php echo number_format($pvg);?></div></td>

	  <td></td>

    </tr>

	<tr>

      <td>3</td>

      <td>Member Active</td>

      <td><div align="right"><?php echo number_format($am);?></div></td>

	  <td></td>

    </tr>

	<tr>

      <td>4</td>

      <td>Qualified Recruitment</td>

      <td><div align="right"><?php echo number_format($qrecruit);?></div></td>

	  <td></td>

    </tr>

	<tr>

      <td>5</td>

      <td>New Starters</td>

      <td><div align="right"><?php echo number_format($nm);?></div></td>

	  <td></td>

    </tr>

	<tr>

      <td>6</td>

      <td>New Recruits</td>

      <td><div align="right"><?php echo number_format($nr);?></div></td>

	  <td></td>

    </tr>

	<tr>

      <td>7</td>

      <td>SF in Personal Group</td>

      <td><div align="right"><?php echo number_format($sf);?></div></td>

	  <td></td>

    </tr>

	<tr>

      <td>8</td>

      <td>Inactive Member</td>

      <td><div align="right"><?php echo number_format($im);?></div></td>

	  <td></td>

    </tr>
	<!--
	<tr>

      <td>9</td>

      <td>Catalog per Sales Force</td>

      <td><div align="right"><?php echo "-";?></div></td>

	  <td></td>

    </tr>
	-->
</table>
<hr />
<p class="cName"> 
<b>Keterangan</b>
<table>
<tr><td>1</td><td>Personal Order</td><td>:</td><td>Pembelanjaan pribadi</td></tr>	
<tr><td>2</td><td>Group PV</td><td>:</td><td>Pembelanjaan Personal Group</td></tr>
<tr><td>3</td><td>Member Active</td><td>:</td><td>Member yang berbelanja dalam personal group</td></tr>
<tr><td>4</td><td>Qualified Recruitment</td><td>:</td><td>Member baru yang belanja (pv product) >= 1 Juta PV dalam personal group</td></tr>
<tr><td>5</td><td>New Starters</td><td>:</td><td>Member baru yang belum belanja dalam personal group</td></tr>
<tr><td>6</td><td>New Recruits</td><td>:</td><td>Member baru yang belanja (PV product) dalam personal group</td></tr>
<tr><td>7</td><td>SF in Personal Group</td><td>:</td><td>Member (lama dan baru) yang berbelanja dibulan itu dalam personal group (diluar yang break)</td></tr>
<tr><td>8</td><td>Inactive Member</td><td>:</td><td>Member yang tidak berbelanja lebih dari satu bulan</td></tr>
</table>
</p>
<?php $this->load->view('footer');?>