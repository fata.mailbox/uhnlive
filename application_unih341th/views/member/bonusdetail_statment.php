<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="100%">
        <tr>
			<td width='24%' valign='top'>Member ID / Nama </td>
			<td width='1%' valign='top'>:</td>
			<td width='75%'><b><?php echo $header['member_id']." / ".$header['nama'];?></b></td>
		</tr>
        
        <tr>
			<td valign='top'>Periode</td>
			<td valign='top'>:</td>
			<td><b><?php echo $header['tgl'];?></b></td>
		</tr>
        <!-- 
        <tr>
			<td valign='top'>Jenjang</td>
			<td valign='top'>:</td>
			     	<td><b><?php echo $header['jenjang'];?></b></td>
		</tr>
        -->
        <tr>
			<td valign='top'>Jenis Bonus / Total Rp. </td>
			<td valign='top'>:</td>
			<td><b><?php echo $header['titlebonus']." / ".$header['fnominal'];?></b></td>
		</tr>
        <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><div id="view"><input type="button" onclick="history.go(-1);" value="<<= Back"/></div><a href="<?php echo $this->baseurl.'/bonus/detailexport/'.$header['member_id']?>">Export</a></td>
		</tr>
	</table>
	
    <table class="stripe">
	<tr>
      <th width='7%'>No.</th>
      <th width='45%'>Member ID / Nama</th>
      <th width='15%'><div align="right">BV</div></th>
      <th width='10%'><div align="right">Persen %</div></th>
      <th width='23%'><div align="right">Nominal Rp</div></th>
    </tr>
   
<?php
if ($results): 
	foreach($results as $key => $row): ?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['downline_id']." / ".$row['nama'];?></td>
      <td align="right"><?php echo $row['fpv'];?></td>
      <td align="right"><?php echo $row['persen']."%";?></td>
     <td align="right"><?php echo $row['fnominal'];?></td>
    </tr>
    <?php endforeach; ?>
<?php endif; ?>    
</table>			
                
                
<?php $this->load->view('footer');?>
