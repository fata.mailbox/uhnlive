
<table width='100%'>
<?php echo form_open('member/topten', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'>
			<?php 
				$data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  
				echo form_input($data);
			?>   
			<?php 
				$data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
				echo "to: ".form_input($data);
			?>
   </td>
  </tr>
 
<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td><?php echo form_submit('submit','preview');?></td>
  </tr>
<?php echo form_close();?>				
</table>


<b>Top Ten Recruitor</b>
<table border="1" bordercolor="#999999" class="stripe">
	<tr>
		<td colspan="11" style="border-bottom:solid thin #000099"></td>
	</tr>
	<tr>
		<th align="right" width="3%"><div align="right">No</div></th>
		<th align="left" width="10%"><div align="left">Member Id </div></th>
		<th align="left" width="20%"><div align="left">Member Name</div></th>
		<th align="left" width="57%"><div align="left">Total Recruit/s </div></th>
	</tr>
	<tr>
		<td colspan="11" style="border-bottom:solid thin #000099"></td>
	</tr>
	<?
		if ($topten):
			$flag=0;
			foreach($topten as $dt1): 
				$flag+=1;
	?>
	<tr>
		<td align="right"><?php echo $flag; 				?></td>
		<td align="left"><?php echo $dt1['enroller_id'];	?></td>
		<td align="left"><?php echo $dt1['nama'];			?></td>
		<td align="left"><?php echo $dt1['jml']; 			?></td>
	</tr>
	<?php
			endforeach;setlocale(LC_MONETARY, "en_US");
		else:
	?>
	<tr>
      <td colspan="11">Data is not available.</td>
    </tr>
	<?php
		endif;
	?>
	<tr>
		<td colspan="11" style="border-bottom:double medium #000099"></td>
	</tr>
</table>

<p>&nbsp;</p>
<b>Top Ten Buyer/s</b>
<table border="1" bordercolor="#999999" class="stripe">
  <tr>
    <td colspan="11" style="border-bottom:solid thin #000099"></td>
  </tr>
  <tr>
    <th align="right" width="3%"><div align="right">No</div></th>
    <th align="left" width="10%"><div align="left">Member Id </div></th>
    <th align="left" width="20%"><div align="left">Member Name</div></th>
    <th align="right" width="20%"><div align="right">Total Shop </div></th>
	<th align="left" width="47%"><div align="left">Total PV </div></th>
  </tr>
  <tr>
    <td colspan="11" style="border-bottom:solid thin #000099"></td>
  </tr>
  <?
		if ($topbuy):
			$flag=0;
			foreach($topbuy as $dt1): 
				$flag+=1;
	?>
  <tr>
    <td align="right"><?php echo $flag; 				?></td>
    <td align="left"><?php echo $dt1['member_id'];		?></td>
    <td align="left"><?php echo $dt1['nama'];			?></td>
    <td align="right"><?php echo $dt1['jml']; 			?></td>
	<td align="left"><?php echo $dt1['pv']; 			?></td>
  </tr>
  <?php
			endforeach;setlocale(LC_MONETARY, "en_US");
		else:
	?>
  <tr>
    <td colspan="11">Data is not available.</td>
  </tr>
  <?php
		endif;
	?>
  <tr>
    <td colspan="11" style="border-bottom:double medium #000099"></td>
  </tr>
</table>
<p>
  <script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
  </script>
</p>
