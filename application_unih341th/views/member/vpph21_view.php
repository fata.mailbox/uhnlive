<br>
<?php echo form_open('member/pph21/edit/'.$datapph['member_id'], array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
<table width='100%'>
	<tr>
		<td width='24%' valign='top'>Member ID</td>
		<td width='1%' valign='top'>:</td>
		<td width='75%'><?php 
			//echo $datapph['member_id'];
			$data = array('name'=>'member_id',/*'type'=>'hidden',*/'value'=>$datapph['member_id']);echo form_input($data);
		?><span class='error'>*<?php echo form_error('member_id'); ?></span></td>
	</tr>
	<tr>
		<td valign='top'>Name</td>
		<td valign='top'>:</td>
		<td><?php 
			//echo $datapph['nama'];
			$data = array('name'=>'nama',/*'type'=>'hidden',*/'value'=>$datapph['nama']);echo form_input($data);
		?><span class='error'>*<?php echo form_error('nama'); ?></span></td>
	</tr>
	<tr>
		<td valign='top'>NPWP Number (number only)</td>
		<td valign='top'>:</td>
		<td><?php 
			$data = array('name'=>'npwp','maxlength'=>'15','value'=>set_value('npwp',$datapph['npwp']));echo form_input($data);
		?><span class='error'>*<?php echo form_error('npwp'); ?></span></td>
	</tr>
	<tr>
		<td valign='top'>NPWP Date (yyyy-mm-dd)</td>
		<td valign='top'>:</td>
		<td><?php 	
				$data = array('name'=>'npwpdate','maxlength'=>'10','value'=>set_value('npwpdate',$datapph['npwpdate']));echo form_input($data);
		?><span class='error'>*<?php echo form_error('npwpdate'); ?></span></td>
	</tr>
	<tr>
		<td width='24%' valign='top'>Address on NPWP</td>
		<td width='1%' valign='top'>:</td>
		<td width='75%'>
		<?php
			//echo "Alamat = ".$datapph['alamat']." <br>Alamat member = ".$datapph['alamatmember']."<br>";
			if(strlen($datapph['alamat'])>=1){$alamat = $datapph['alamat'];}else{$alamat = $datapph['alamatmember'] ;}
			$data = array('name'=>'address','id'=>'address','rows'=>'3','cols'=>'30','class'=>'txtarea','value'=>set_value('alamat',$alamat)); 
			echo form_textarea($data);
		?><span class='error'>*<?php echo form_error('address'); ?></span></td>
	</tr>
	<tr>
		<td valign='top'>Marital Status</td>
		<td valign='top'>:</td>
		<td><?php
			//$data=array(''=>'--Choose--','0'=>'Single','1'=>'Married'); 
			$data=array('0'=>'Single','1'=>'Married'); 
			echo form_dropdown('menikah',$data,set_value('menikah',$datapph['menikah']));
		?><span class='error'>*<?php echo form_error('menikah'); ?></span></td>
	</tr>
	<tr>
		<td valign='top'>Number of Children</td>
		<td valign='top'>:</td>
		<td><?php
			//$data=array(''=>'--Choose--','0'=>'0','1'=>'1 - One','2'=>'2 - Two','3'=>'3 - Tree'); 
			$data=array('0'=>'0','1'=>'1 - One','2'=>'2 - Two','3'=>'3 - Tree'); 
			echo form_dropdown('anak',$data,set_value('anak',$datapph['anak']));
		?><span class='error'>*<?php echo form_error('anak'); ?></span></td>
	</tr>
	<tr>
		<td valign='top'>Work on</td>
		<td valign='top'>:</td>
		<td><?php 
			if(strlen($datapph['office'])>1){$office = $datapph['office'];}else{$office = '-' ;}
			$data = array('name'=>'kerja','value'=>$office);echo form_input($data);
		?></td>
	</tr>
	<tr>
		<td valign='top'></td>
		<td valign='top'></td>
		<td><?php echo form_submit('submit','Process');?></td>
	</tr>
	
</table>
<?php echo form_close();?>