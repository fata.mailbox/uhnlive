<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<?php
	if ($this->session->flashdata('message')){
		echo "<div class='message'>".$this->session->flashdata('message')."</div>";
	}
?>
<table width="100%">
<?php echo form_open('member/level/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
<?php if($this->session->userdata('group_id')>100): ?>
        <tr>
			<td valign='top'>Member ID / Nama </td>
			<td valign='top'>:</td>
			<td><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('userid')." / ".$this->session->userdata('name');?></b></td>
		</tr>
		<tr>
			<td width='19%' valign='top'>Posisi Sekarang ~ Posisi Tertinggi</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php echo $row['jenjang'].' ~ '.$row['jenjang2'];?></td>
		</tr>
        <tr>
			<td width='19%' valign='top'>Akumulasi TGPV</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php echo $row['fapgs'];?></td>
		</tr>
        <?php else: ?>
        <tr>
                        	<td width="25%">Member ID</td>
                            <td width="1%">:</td>
                            <td width="74%">
<?php $data = array('name'=>'member_id','id'=>'member_id','maxlength'=>'20','size'=>'11','value'=>set_value('member_id'));
							echo form_input($data);?>
                            <?php $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?></td>                        
  </tr>
                    <tr>
                        	<td>Nama Member</td>
                            <td>:</td>
                            <td><?php $data = array('name'=>'name','id'=>'name','maxlength'=>'20','readonly'=>'1','value'=>set_value('name'));
							echo form_input($data); ?></td>                        
                    </tr>              
                <tr>
			<td width='19%' valign='top'>Periode</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php echo form_dropdown('periode',$dropdown);?></td>
		</tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
		<td><?php echo form_submit('submit','preview');?></td>
	</tr>
                    <?php echo form_close();?>

        
<?php endif;?>
	</table>
	<hr />
    <table class="stripe">
	<tr>
	  <td colspan="2">Jenjang Tertinggi</td>
	  <td colspan="3">: <strong><?=$hightjenjang->jenjang;?></strong></td>
	  <td colspan="2" style="text-align:right;"> 
    <?php 
    //ASP Start 20200221
    if($this->session->userdata('group_id')==1){
    // EOF ASP 20200221
    ?>
		<?= $this->session->userdata('jenjangMemberId') && !empty($this->session->userdata('jenjangMemberId')) && $this->session->userdata('rowIdResults') && !empty($this->session->userdata('rowIdResults')) ? anchor('member/level/edit/'.$this->session->userdata('jenjangMemberId').'/'.$this->session->userdata('rowIdResults'), '<strong>Tambah Jenjang</strong>') : '';?>
	  <?php
    //ASP Start 20200221
    }
    // EOF ASP 20200221
    ?>
    </td>
	</tr>
	<tr>
      <th width='5%'>No.</th>
      <th width='15%'>Periode</th> 
      <th width='30%'>Member ID / Nama</th>
     <th width='15%'>Jenjang Lama</th>
      <th width='15%'>Jenjang Baru</th>
      <th width='15%'>Bulan Akhir</th>
      <th width='5%'></th>
	</tr>
   
<?php
if ($results): 
	foreach($results as $key => $row): ?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['ftgl'];?></td>
      <td><?php echo $row['member_id']." / ".$row['nama'];?></td>
      <td><?php echo $row['jenjanglama'];?></td>
      <td><?php echo $row['jenjangbaru'];?></td>
      <td><?php echo $row['ftglakhir'];?></td>
      <td><?php //echo ($key === 0) && ($this->session->userdata('group_id') <= 2)? anchor('member/level/edit/'.$row['member_id'].'/'.$row['id'], 'Edit') : ''; ?></td>
    </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>			
<br />
<table class="stripe">
<tr>
      <td colspan="4"><strong>Qualifikasi Leader</strong></td>
  </tr>
	<tr>
      <th width='5%'>No.</th>
      <th width='15%'>Periode</th> 
      <th width='35%'>Member ID / Nama</th>
     <th width='15%'>Jumlah</th>
	</tr>
   
<?php
if($hightjenjang->jenjangbaru > 5){
	$xrow = 1;
	if ($rs_sl){
		foreach($rs_sl as $key => $rowsl): ?>
        <tr>
          <td><?php echo $xrow;?></td>
          <td><?php echo $rowsl['ftgl'];?></td>
          <td><?php echo $rowsl['member_id']." / ".$row['nama'];?></td>
          <td><?php echo $rowsl['qty'];?></td>
        </tr>
        <?php 
		$xrow++;
		endforeach;
	}
	if ($rs){
		foreach($rs as $key => $row): ?>
        <tr>
          <td><?php echo $xrow;?></td>
          <td><?php echo $row['ftgl'];?></td>
          <td><?php echo $row['member_id']." / ".$row['nama'];?></td>
          <td><?php echo $row['qty'];?></td>
        </tr>
        <?php 
		$xrow++;
		endforeach;
	}
}else{
if ($rs): 
	foreach($rs as $key => $row): ?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['ftgl'];?></td>
      <td><?php echo $row['member_id']." / ".$row['nama'];?></td>
      <td><?php echo $row['qty'];?></td>
    </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
      <td colspan="4">Data is not available.</td>
    </tr>
<?php endif; 
}
?>    
</table>
<br />
<?php
if ($results2): ?>
<table class="stripe">
<tr>
      <td colspan="4"><strong>Top Qualifikasi Leader</strong></td>
    </tr>
	<tr>
      <th width='5%'>No.</th>
      <th width='15%'>Member ID</th> 
      <th width='35%'>Nama</th>
     <th width='15%'>Jumlah</th>
	</tr>
   
<?php
	foreach($results2 as $key => $row): ?>
    <tr>
      <td><?php echo $row['i'];?></td>
      <td><?php echo $row['member_id'];?></td>
      <td><?php echo $row['nama'];?></td>
      <td><?php echo $row['lqty'];?></td>
    </tr>
    <?php endforeach; ?>
</table>
<div id="dialog" title="Basic dialog">
  <p>This is the default dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>
</div>
<?php endif; ?>

<script>
	$(document).ready(function(){
		$('#editJenjang').click(function(){
			$('#dialog').fadeIn("slow");
		});
	});
</script>

<?php $this->load->view('footer');?>
