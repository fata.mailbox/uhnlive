<?php $this->load->view('header');?>

<h2><?php echo $page_title;?></h2>
<?php
	echo form_open('member/level/insert', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));
?>
<table width="100%">
	<tr>
		<td width="19%" valign='top'>Member ID</td>
		<td valign='top'>:&nbsp;&nbsp;
		<?= $memberID?>
		<input type="hidden" name="memberId" id="memberId" value="<?=$memberID?>" />
		</td>
	</tr>
	<tr>
		<td valign='top'>Nama Member</td>
		<td valign='top'>:&nbsp;&nbsp;&nbsp;<?= $results['nama'];?>
		<input type="hidden" name="sponsorId" id="sponsorId" value="<?=$results['sponsorId']?>" />
		</td>
	</tr>
	<tr>
		<td valign='top'>Jenjang Baru</td>
		<td valign='top'>:&nbsp;&nbsp;
			<select name="jBaru" id="jBaru">
				<option value="-"> -- Option -- </option>
			<?php
				foreach($jenjangList as $key => $value){
					echo '<option value="'.$value['id'].'"'. ($results['idJenjangBaru'] == $value['id'] ? 'selected' : '') .'>'.$value['decription'].'</option>';
				}
			?>
			</select>
			<input type="hidden" name="idJenjangBaru" id="idJenjangBaru" value="<?=$results['idJenjangBaru']?>" />
		</td>
	</tr>
	<tr>
		<!--<td valign='top'>Jenjang Lama</td>-->
		<td valign='top'>Jenjang Saat Ini</td>
		<td valign='top'>:&nbsp;&nbsp;
			<?= $results['desc_jenjangbaru']; //$results['desc_jenjanglama']; ?>
			<input type="hidden" name="idJenjangLama" id="idJenjangLama" value="<?=$results['idJenjangLama']?>" />
		</td>
	</tr>
	<tr>
		<td valign='top'>Period</td>
		<td valign='top'>:&nbsp;&nbsp;&nbsp;<?php echo form_dropdown('periode',$dropdown, '', 'id="period"');?></td>
	</tr>
	<tr>
		<!--<td colspan="2"><button name="btnEditJenjang" id="btnEditJenjang" value="submit">Submit</button></td>-->
		<td colspan="2"><?php echo form_submit('submit', 'Submit', 'tabindex="23"');?></td>
	</tr>
</table>
<?php echo form_close();?>

<script>
	function con(message) {
	  var answer = confirm(message);
	  if (answer) {
		return true;
	  }else{
		return false;
	  }
	}
	var idJenjangBaru = $('#idJenjangBaru').val();
	$(document).ready(function(){
		$('#btnEditJenjang').click(function(){
			var labelJejaring = $('#jBaru option:selected').text();
			var period = $('#period option:selected').text();
			con('apakah anda yakin untuk mendowngrade jenjang menjadi '+ labelJejaring +' dengan period '+ period +' ?');
		});
	});
</script>

<?php $this->load->view('footer');?>
