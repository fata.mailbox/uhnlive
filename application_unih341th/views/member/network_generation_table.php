<table class="stripe" cellpadding="0" cellspacing="0" width="100%">
	<div style="height:30px; line-height:30px; text-align:center; font-weight:bold; border-bottom:1px solid #ddd; background:#eff5f9;">
    	Generation <?=$t;?>
    </div>  
    <thead>
	<tr>
        <td width="2%" rowspan="2">No.</td>
      <td colspan="4">Member</td>
        <td colspan="2" align="center">Sponsor</td>
        <td width="7%" rowspan="2" align="right">PS</td>
        <td width="17%" rowspan="2" align="right">TGPV</td>
        <td colspan="2" align="center">Qty Leader</td>
      <td width="3%" rowspan="2">Tupo</td>
	</tr>
    <tr>
    <td width="8%">ID</td>
      <td width="17%">Name</td>
      <td width="6%">Join Date</td>
      <td width="5%">Peringkat</td>
      <td width="8%">ID</td>
      <td width="17%">Name</td>
      <td width="5%" align="center">All</td>
      <td width="5%" align="center">Leg</td>
    </tr>
    </thead>
    <?php foreach($results as $row) { ?>
    <tr class="<?=$row['qualified'];?>">
        <td data-title="No." align="center"><?php echo $row['i'];?></td>
        <td data-title="Member : ID"><?php echo $row['member_id'];?></td>
        <td data-title="Member : Name"><?php echo $row['nama'];?></td>
        <td data-title="Join Date"><?=$row['ftglaplikasi'];?></td>
        <td data-title="Package"><?=$row['jenjang'];?></td>
        <td data-title="Sponsor : ID"><?php echo $row['sponsorid'];?></td>
      	<td data-title="Sponsor : Name"><?php echo $row['namasponsor'];?></td>
      	<td data-title="Personal Sales" align="right"><?php echo $row['fps'];?></td>
      	<td data-title="eMaintain" align="right"><?php echo $row['fpgs'];?></td>
      	<td data-title="Tupo" align="center"><?=$row['fqtyleader'];?></td>
      	<td data-title="Tupo" align="center"><?=$row['fqtyleg'];?></td>
        <td data-title="Tupo" align="center"><img src="<?=base_url();?>images/backend/<?php echo $row['tupo'];?>.png" /></td>
    </tr>
    <?php }?>
</table>
