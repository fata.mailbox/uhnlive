<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +62 21 96 213 688 
    	Yahoo Messenger	: taqwatea
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="100%">
<?php echo form_open('member/tgpvo/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
        <tr>
			<td valign='top'>Period</td>
			<td valign='top'>:</td>
			<td><input type="radio" name="periode" value="1" <?php echo set_radio('periode', '1', TRUE); ?> /> Curr Month 
<input type="radio" name="periode" value="2" <?php echo set_radio('periode', '2'); ?> /> Last Month</td>
		</tr>
<?php if($this->session->userdata('group_id') <= 100){ ?>
	<tr>
			<td width='19%' valign='top'>Member ID</td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><?php $data = array('name'=>'member_id','id'=>'member_id','size'=>15,'readonly'=>'1','value'=>set_value('member_id'));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?>				
					 <span class='error'>*<?php echo form_error('member_id');?></span></td>
					
		</tr>
		<tr>
			<td valign='top'>Name</td>
			<td valign='top'>:</td>
			<td><input type="text" name="name" id="name" readonly="1" value="<?php echo set_value('name');?>" size="30" /></td>
		</tr>
        <?php }else{ ?>
        <tr>
			<td width='19%' valign='top'>Member ID / Name </td>
			<td width='1%' valign='top'>:</td>
			<td width='80%'><b><?php echo form_hidden('member_id',$this->session->userdata('userid')); echo $this->session->userdata('userid')." / ".$this->session->userdata('name');?></b></td>
		</tr>
        <?php }?>
        <tr>
			<td>Highest PIN</td>
			<td>:</td>
			<td><b><?php echo $row['jenjang'];?></b></td>
		</tr>
       <tr>
			<td>PV Personal Sales</td>
			<td>:</td>
			<td><b><?php if($this->input->post('periode') == '1')echo $row['fps']; else echo $row['fpsbak']?></b></td>
		</tr>
		<tr>
			<td>Total of Group PV</td>
			<td>:</td>
			<td><b><?php if($this->input->post('periode') == '1')echo $row['fpgs']; else echo $row['fpgsbak']?></b></td>
		</tr>
		
		<?php /*Update by Boby (2009-11-17)*/?>
		<tr>
			<td>Accumulation of Group PV</td>
			<td>:</td>
			<td><b><?php echo $agps['apgs'];?></b></td>
		</tr>
		<?php /*end Update by Boby (2009-11-17)*/?>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
		<td><?php echo form_submit('submit','preview');?></td>
	</tr>
                    <?php echo form_close();?>
	</table>
	
    <table class="stripe">
	<tr>
      <th width='8%'>No.</th>
      <th width='16%'>Downline ID</th>
      <th width='40%'>Downline Name</th>
      <th width='12%'>Highest PIN</th>
	  <th width='12%'><div align="right">Personal Sales</div></th>
      <th width='12%'><div align="right">Total of Group PV</div></th>
    </tr>
   
<?php
if ($results): 
	foreach($results as $key => $row): ?>
    <tr>
		<td><?php echo $row['i'];?></td>
		<td><?php echo $row['id'];?></td>
		<td><?php echo $row['nama'];?></td>
		<td><?php echo $row['jenjang'];?></td>
		<td align="right"><b><?php if($this->input->post('periode') == '1')echo $row['fps']; else echo $row['fpsbak']?></b></td>
		<td align="right"><b><?php if($this->input->post('periode') == '1')echo $row['fpgs']; else echo $row['fpgsbak']?></b></td>
    </tr>
    <?php endforeach; ?>
    <tr>
    <td colspan="5"><strong>Total PV : </strong></td>
     <td align="right"><b><?php if($this->input->post('periode') == '1')echo $total['fpgs']; else echo $total['fpgsbak']?></b></td>
     </tr>
<?php else: ?>
    <tr>
      <td colspan="5">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>			
                
                
<?php $this->load->view('footer');?>
