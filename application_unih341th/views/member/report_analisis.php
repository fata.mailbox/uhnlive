<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>


<table width="100%">
<?php echo form_open('member/report/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
	<tr>
		<td valign='top' width="19%">Periode</td>
		<td valign='top' width="1%">:</td>
		<td width="80%">
			<?php 
				$thn = date('Y', now());
				$bln = date('m', now());
				
				$bulan = array(
					'01'  => 'January',		'02'	=> 'February',	'03'  => 'March',		'04'	=> 'April',
					'05'  => 'May',			'06'	=> 'Juny',		'07'  => 'July',		'08'	=> 'August',
					'09'  => 'September',	'10'	=> 'October',	'11'  => 'November',	'12'	=> 'December'
				);
				
				for($i=$thn;$i>=2009;$i--){
					$tahun[$i] = $i;
				}
				//echo form_dropdown('periode',$dropdown);
				echo form_dropdown('bulan', $bulan);
				echo form_dropdown('tahun', $tahun);
				
			?>
		</td>
	</tr>
	<tr>
		<td valign='top' width="19%">Category</td>
		<td valign='top' width="1%">:</td>
		<td>
			<?php
				$flag = array(
					'1' => 'New Member',
					'2' => 'Member Birth',
					'3' => 'All'
				);
				echo form_dropdown('flag',$flag);
				echo form_submit('submit','preview');
			?>
		</td>
	</tr>
	<?php //updated by Boby 20110329 ?>
	<tr>
		<td valign='top' width="19%">Download CSV</td>
		<td valign='top' width="1%">:</td>
		<td>
			<?php
				echo form_submit('submit','All')." ";
				echo form_submit('submit','JaBoDeTaBek');
			?>
		</td>
	</tr>
	<?php //end updated by Boby 20110329 
	//update by Andrew 20120529
	?>
    <tr>
    <td colspan="100%">
    <hr />
    </td>
    </tr>
	<tr>
		<td colspan = 3>&nbsp;</td>
	</tr>
	
	<tr>
		<td valign='top' width="19%">Stockiest CSV</td>
		<td valign='top' width="1%">:</td>
		<td>
			<?php
				echo form_submit('submit','Stockiest')." ";
				echo form_submit('submit','M-Stockiest')." ";
			?>
		</td>
	</tr>
	<?php //end update by Andrew 20120529?>

	<?php // Update by Boby 20131112?>
	<tr>
		<td valign='top' width="19%">Email Stockiest CSV</td>
		<td valign='top' width="1%">:</td>
		<td>
			<?php
				echo form_submit('submit','E-Stockiest')." ";
				echo form_submit('submit','E-M_Stockiest')." ";
			?>
		</td>
	</tr>
	<?php // End update by Boby 20131112?>
	
	<?php // Update by Boby 20140806 ?>
	<tr>
		<td valign='top' width="19%">Member Aktif CSV</td>
		<td valign='top' width="1%">:</td>
		<td>
			<?php
				echo form_submit('submit','E-Member')." ";
				echo form_submit('submit','HP-Member')." ";
			?>
		</td>
	</tr>
	<tr>
		<td valign='top' width="19%">Invalid Account CSV</td>
		<td valign='top' width="1%">:</td>
		<td>
			<?php
				// echo form_submit('submit','E-AccMember')." ";
				echo form_submit('submit','HP-AccMember')." ";
			?>
		</td>
	</tr>
    <tr>
    	<td colspan="100%"><hr /></td>
    </tr>
    <!--
  	<tr>
    	<td width='24%'>Periode</td>
		<td width='1%'>:</td>
		<td width='75%'>
			<?php 
				$data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  
				echo form_input($data);
			?>   
			<?php 
				$data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
				echo "to: ".form_input($data);
			?>
   </td>
   -->
	<tr>
		<td valign='top' width="19%">Category</td>
		<td valign='top' width="1%">:</td>
		<td>
			<?php
				$reqcatdata = array(
					'allmemberemail' => 'All Member E-Mail',
					'newmemberemail' => 'New Member E-Mail',
					'newmemberhp' => 'New Member HP',
				);
				echo form_dropdown('reqcat',$reqcatdata);
			?>
		</td>
	</tr>
	<?php //updated by Boby 20110329 ?>
	<tr>
		<td valign='top' width="19%">Download CSV</td>
		<td valign='top' width="1%">:</td>
		<td>
			<?php
				echo form_submit('submit','export')." ";
			?>
		</td>
	</tr>
  </tr>
	<?php // End update by Boby 20140806 ?>
	<?php echo form_close();?>
	<tr><td colspan="3"><hr /></td></tr>             
</table>

	
    <table class="stripe">
	<tr>
      <th><div align="right">No.</div></th>
      <th><div align="left">Member ID</div></th>
      <th><div align="left">Nama Member</th>
      <th>Tanggal Join</th>
      <th>Tanggal Lahir</th>
	  <th>Hp</th>
      <th><div align="right">Umur</div></th>
    </tr>
   
<?php
if ($results):
	$i = 0;
	foreach($results as $key => $row):
		$i += 1;
?>
	<?php	//if($i == 1){ echo '<tr> <td colspan="7">Bonus MLM<td> </tr>';	} ?>
    <tr>
		<td align="right"><?php echo $i;?></td>
		<td><?php echo $row['id'];?></td>
		<td><?php echo $row['nama'];?></td>
		<td align="center"><?php echo $row['created'];?></td>
		<td align="center"><?php echo $row['tgllahir'];?></td>
		<td align="right"><?php echo $row['hp'];?></td>
		<td align="right"><?php echo $row['umur'];?></td>
    </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
      <td colspan="7">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>
<!--			
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
</script>
-->                
<?php $this->load->view('footer');?>
