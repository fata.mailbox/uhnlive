<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<?php echo form_open('member/memprofile/edit/'.$row['id'], array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>
<table width="100%">
            	<tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA PENDAFTARAN</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
		<td colspan="6">
        	<table width="100%" style="font-size:medium">
	<tr>
		<td width='9%'>Tanggal Daftar</td>
		<td width='1%'>:</td>
		<td width='15%'><?php echo $row['ftglaplikasi'];?></td>
		<td width='15%'>Stockiest ID / Nama</td>
		<td width='1%'>:</td>
		<td width='15%'><?php if($row['stockiest_id'] == 'SH0000') echo "KANTOR UHN"; else echo $row['no_stc']." / ".$row['namastc'];?></td>
		<td width='13%'>Kelengkapan Data</td>
		<td width='1%'>:</td>
		<td width='25%'>
		
		<?php /*if($row['lengkap'] == '1')$flag = true; else $flag = false;
		
		$data = array(
    'name'        => 'lengkap',
    'id'          => 'lengkap',
    'value'       => 1,
    'checked'     => set_value('lengkap',$flag),
    'style'       => 'border:none'
    );					
					echo form_checkbox($data);*/?>
                    
		<!--Kelengkapan Data -->
        
        <?php if($row['form'] == '1')$flag1 = true; else $flag1 = false;
		
		$data1 = array(
    'name'        => 'data_form',
    'id'          => 'data_form',
    'value'       => 1,
    'checked'     => set_value('data_form',$flag1),
    'style'       => 'border:none'
    );					
					echo form_checkbox($data1);?>
                    
		Formulir
        <?php if($row['rek'] == '1')$flag2 = true; else $flag2 = false;
		
		$data2 = array(
    'name'        => 'data_rek',
    'id'          => 'data_rek',
    'value'       => 1,
    'checked'     => set_value('data_rek',$flag2),
    'style'       => 'border:none'
    );					
					echo form_checkbox($data2);?>
                    
		FC Rekening
        <?php if($row['ktp'] == '1')$flag3 = true; else $flag3 = false;
		
		$data3 = array(
    'name'        => 'data_ktp',
    'id'          => 'data_ktp',
    'value'       => 1,
    'checked'     => set_value('data_ktp',$flag3),
    'style'       => 'border:none'
    );					
					echo form_checkbox($data3);?>
                    
		FC KTP
        </td>
	</tr>
            </table>	
        </td>
	</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA PRIBADI ANDA</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Member ID / Nama</td>
                    <td>:</td>
					<td><?php echo $row['id']." / ".$row['nama'];?></td>
				  <td valign="top">Tempat Lahir</td>
                    <td valign="top">:</td>
                    <td><?php $data = array('name'=>'tempatlahir','value'=>set_value('tempatlahir',$row['tempatlahir']));
    						echo form_input($data);?> <span class='error'>*<?php echo form_error('tempatlahir'); ?></span></td>
				</tr>
                <tr>
					<td>Jenis Kelamin</td>
                    <td>:</td>
					<td><?php $options = array('Laki-laki' => 'Laki-laki', 'Perempuan' => 'Perempuan');
    echo form_dropdown('jk',$options);?></td>
					<td>Tanggal Lahir</td>
                    <td>:</td>
                    <td><?php 
						if($this->session->userdata('group_id')>100){
							$data = array('name'=>'date','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('date',$row['tgllahir']));
						}else{
							$data = array('name'=>'date','id'=>'date1','size'=>12,'maxlength'=>'10', 'value'=>set_value('date',$row['tgllahir']));
						}
						echo form_input($data);
					?> (yyyy-mm-dd)</td>
				</tr>
				<tr>
				  <td valign="top">Alamat</td>
                    <td valign="top">:</td>
					<td><?php 
						//if(strlen($row['noktp'])>1){
					//		$data = array('name'=>'alamat','readonly'=>'true','id'=>'alamat','rows'=>3, 'cols'=>'28','value'=>set_value('alamat',$row['alamat']));
    					//}else{
							$data = array('name'=>'alamat','id'=>'alamat','rows'=>3, 'cols'=>'28','value'=>set_value('alamat',$row['alamat']));
					//	}
						echo form_textarea($data);?><span class='error'>*<?php echo form_error('alamat'); ?></span></td>
					<td>No. Telp</td>
                    <td>:</td>
                    <td><?php $data = array('name'=>'telp','value'=>set_value('telp',$row['telp']));	echo form_input($data);?></td>
				</tr>
                <tr>
				  <td valign="top">Kota</td>
                    <td valign="top">:</td>
					<td><?php 
					echo form_hidden('id',$row['id']); echo form_hidden('account_id',$row['account_id']);
					echo form_hidden('nama',$row['nama']); 
					 echo form_hidden('kota_id', set_value('kota_id',$row['kota_id']));
					 $data = array('name'=>'city','id'=>'city','readonly'=>'1','value'=>set_value('city',$row['kota']));
    echo form_input($data);?> <?php $atts = array(
              'width'      => '400',
              'height'     => '400',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );

					echo anchor_popup('citysearch/all/', "<img src='/images/backend/search.gif' border='0'>", $atts); ?> <span class='error'>*<?php echo form_error('city'); ?></span></td>
					<td>No. Fax</td>
                    <td>:</td>
                    <td><?php $data = array('name'=>'fax','value'=>set_value('fax',$row['fax']));
    						echo form_input($data);?></td>
				</tr>
                <tr>
				  <td valign="top">Kelurahan</td>
                    <td valign="top">:</td>
					<td><?php $data = array('name'=>'kelurahan','value'=>set_value('kelurahan',$row['kelurahan']));
    						echo form_input($data);?><span class='error'>*<?php echo form_error('kelurahan'); ?></span></td>
					<td>No. HP</td>
                    <td>:</td>
                    <td><?php $data = array('name'=>'hp','value'=>set_value('hp',$row['hp']));	echo form_input($data);?></td>
				</tr>
                <tr>
				  <td valign="top">Kecamatan</td>
                    <td valign="top">:</td>
					<td><?php $data = array('name'=>'kecamatan','value'=>set_value('kecamatan',$row['kecamatan']));
    						echo form_input($data);?><span class='error'>*<?php echo form_error('kecamatan'); ?></span></td>
				  <td valign="top">Email</td>
                    <td valign="top">:</td>
                    <td valign="top"><?php $data = array('name'=>'email','value'=>set_value('email',$row['email']));
    						echo form_input($data);?><span class='error'><?php echo form_error('email'); ?></span></td>
				</tr>
                <tr>
				  <td valign="top">Kode Pos</td>
                    <td valign="top">:</td>
					<td><?php $data = array('name'=>'kodepos','value'=>set_value('kodepos',$row['kodepos']));
    						echo form_input($data);?><span class='error'>*<?php echo form_error('kodepos'); ?></span></td>
					<td>Ahliwaris</td>
                    <td>:</td>
                    <td><?php 
						if(strlen($row['ahliwaris'])>1){
							$data = array('name'=>'ahliwaris','value'=>set_value('ahliwaris',$row['ahliwaris']));
						}else{
							$data = array('name'=>'ahliwaris','value'=>set_value('ahliwaris',$row['ahliwaris']));
						}
						echo form_input($data);
						?>
					</td>
				</tr>
                <tr>
					<td>Propinsi</td>
                    <td>:</td>
					<td><?php $data = array('name'=>'propinsi','id'=>'propinsi','size'=>30,'readonly'=>'1','value'=>set_value('propinsi',$row['propinsi']));
    echo form_input($data);?></td>
                    <td></td><td></td><td></td>
                    <!--
					<td>Ahliwaris</td>
                    <td>:</td>
                    <td><?php
						/*
						if(strlen($row['ahliwaris'])>1){
							$data = array('name'=>'ahliwaris','value'=>set_value('ahliwaris',$row['ahliwaris']));
						}else{
							$data = array('name'=>'ahliwaris','value'=>set_value('ahliwaris',$row['ahliwaris']));
						}
						echo form_input($data);
						*/
						?>
					</td>
                    -->
				</tr>
                <tr>
				  <td valign="top">No. KTP / SIM</td>
                    <td valign="top">:</td>
					<td><?php 
						//if(strlen($row['noktp'])>1){
						//	$data = array('name'=>'noktp','readonly'=>'true','value'=>set_value('noktp',$row['noktp']));
						//}else{
							$data = array('name'=>'noktp','value'=>set_value('noktp',$row['noktp']));
						//}
						echo form_input($data);
						?><span class='error'><?php echo form_error('noktp');?></span>
					</td>
                    <td></td><td></td><td></td>
                    <!--
					<td>Ahliwaris</td>
                    <td>:</td>
                    <td><?php 
						/*
						if(strlen($row['ahliwaris'])>1){
							$data = array('name'=>'ahliwaris','value'=>set_value('ahliwaris',$row['ahliwaris']));
						}else{
							$data = array('name'=>'ahliwaris','value'=>set_value('ahliwaris',$row['ahliwaris']));
						}
						echo form_input($data);
						*/
						?>
					</td>
                    -->
				</tr>
                <!--
                <tr>
					<td></td>
                    <td></td>
                    <td></td>
                    <td>No. NPWP</td>
                    <td>:</td>
                    <td><?php 
						/*
						// modified by Boby 20100715
						$data = array('name'=>'npwp','value'=>set_value('npwp',$row['npwp']));	
						echo form_input($data);
						*/
					?><span class='error'>*<?php //echo form_error('npwp'); ?></span></td>
				</tr>
                -->
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<?php //if(strlen($row['no'])>1){}else{?>
                <tr>
					<td colspan="6"><b>DATA BANK</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                
                <tr>
					<td colspan="6"><em>Data bank hanya bisa di rubah oleh bagian administrasi perusahaan</em></td>
                </tr>
                <tr>
					<td>Nama Bank</td>
                    <td>:</td>
					<td>
						<?php 
							/*-------Modified by Boby (2010-01-08)-------*/
							//if(strlen($row['no'])>1){
							//	$data = array('name'=>'bank_id', 'readonly'=>'true','value'=>set_value('bank_id',$row['bank_id']));	echo form_input($data);
								//echo form_dropdown('bank_id',$bank,set_value('bank_id',$row['bank_id']));
							//}else{
								echo form_dropdown('bank_id',$bank,set_value('bank_id',$row['bank_id']), 'readonly');
							//}
							/*-------End Modified by Boby (2010-01-08)-------*/
							/*-------Modified by Boby (2009-11-19)-------*/
							$data = array('name'=>'currbank_id','type'=>'hidden','value'=>set_value('currbank_id',$row['bank_id']));echo form_input($data);
							/*-----end Modified by Boby (2009-11-19)-----*/
						?>
					</td>
					<td valign="top">Nama Nasabah</td>
                    <td valign="top">:</td>
                    <td>
				<?php 
					/*-------Modified by Boby (2010-01-08)-------*/
					//if(strlen($row['no'])>1){
					//	$data = array('name'=>'namanasabah', 'readonly'=>'true','value'=>set_value('namanasabah',$row['namanasabah']));	echo form_input($data);
					//}else{
						$data = array('name'=>'namanasabah','value'=>set_value('namanasabah',$row['namanasabah']));	echo form_input($data);
					//}
					/*-------End Modified by Boby (2010-01-08)-------*/
					/*-------Modified by Boby (2009-11-19)-------*/
					$data = array('name'=>'currnamanasabah','type'=>'hidden','value'=>set_value('currnamanasabah',$row['namanasabah']));	echo form_input($data);
					/*-----end Modified by Boby (2009-11-19)-----*/
				?> 
				<span class='error'><?php echo form_error('namanasabah'); ?>
			  </td>
				</tr>
                <tr>
					<td valign="top">Cabang <?php //echo $row['flag'];?></td>
                    <td valign="top">:</td>
					<td>
					<?php 
						/*-------Modified by Boby (2010-01-08)-------*/
						//if(strlen($row['no'])>1){
						//	$data = array('name'=>'area','readonly'=>'true','value'=>set_value('area',$row['area'])); echo form_input($data);
						//}else{
							$data = array('name'=>'area','value'=>set_value('area',$row['area'])); echo form_input($data);
						//}
						/*-------End Modified by Boby (2010-01-08)-------*/
						/*-------Modified by Boby (2009-11-19)-------*/
						$data = array('name'=>'flag','type'=>'hidden','value'=>set_value('flag',$row['flag'])); echo form_input($data);
						/*-----end Modified by Boby (2009-11-19)-----*/
					?> 
					<span class='error'><?php echo form_error('area'); ?></td>
					<td valign="top">No. Rekening</td>
                    <td valign="top">:</td>
                    <td>
				<?php 
					//if(strlen($row['no'])>1){
					//	$data = array('name'=>'norek','readonly'=>'true','value'=>set_value('norek',$row['no']));	echo form_input($data);
					//}else{
						$data = array('name'=>'norek','value'=>set_value('norek',$row['no']));	echo form_input($data);
					//}
					/*-------Modified by Boby (2009-11-19)-------*/
					$data = array('name'=>'currnorek','type'=>'hidden','value'=>set_value('currnorek',$row['no']));	echo form_input($data);
					/*-----end Modified by Boby (2009-11-19)-----*/
				?> 
				<span class='error'><?php echo form_error('no');?>
			  </td>
				</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<?php //} ?>
                <tr>
					<td colspan="6"><b>DATA SPONSOR</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Member ID</td>
                    <td>:</td>
					<td><?php echo $row['enroller_id'];?></td>
					<td>Nama Sponsor</td>
                    <td>:</td>
                    <td><?php echo $row['namaenroller'];?></td>
				</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA PENEMPATAN</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Member ID</td>
                    <td>:</td>
					<td><?php echo $row['sponsor_id'];?></td>
					<td>Nama</td>
                    <td>:</td>
                    <td><?php echo $row['namasponsor'];?></td>
				</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				 <tr>
					<td><?php echo form_submit('submit','Process');?></td>
                  <td></td>
					<td></td>
					<td></td>
                    <td></td>
                    <td></td>
				</tr>
                
</table>
			<?php echo form_close();?>
            
<?php $this->load->view('footer');?>
            
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
</script>