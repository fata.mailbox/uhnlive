<!--
	Copyright (c) 2009-<?php echo date("Y");?> 
	developed by  	: www.smartindo-technology.com
    	contact person	: Takwa
        Handphone	: +62 817 906 1982
    	Telphone 	: +6221 5435 5600 
    	Yahoo Messenger	: qtakwa@yahoo.com
-->
<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="99%">
<?php echo form_open('member/tgpv/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
		<?php if($this->session->userdata('group_id') > 100){?>
        <tr>
			<td>Nama</td>
			<td>:</td>
			<td></b><?php echo form_hidden('member_id',set_value('member_id',$this->session->userdata('userid'))); echo $this->session->userdata('username')." / ".$this->session->userdata('name');?></td> 
		</tr>
        <?php }else{?>
        <tr>
                        	<td width="25%">Member ID</td>
                            <td width="1%">:</td>
                            <td width="74%">
<?php $data = array('name'=>'member_id','id'=>'member_id','maxlength'=>'20','size'=>'11','value'=>set_value('member_id'));
							echo form_input($data);?>
                            <?php $atts = array(
              'width'      => '450',
              'height'     => '500',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
					echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); 
					?></td>                        
                    </tr>
                    <tr>
                        	<td>Nama Member</td>
                            <td>:</td>
                            <td><?php $data = array('name'=>'name','id'=>'name','maxlength'=>'20','readonly'=>'1','value'=>set_value('name'));
							echo form_input($data); ?></td>                        
                    </tr>              
        <?php }?>
        <tr>
			<td valign='top'>Level</td>
			<td valign='top'>:</td>
			<td><?php $data = array('0'=>'Level 1 - 10','10'=>'Level 11 - 20','20'=>'Level 21 - 30','30'=>'Level 31 - 40','40'=>'Level 41 - 50'); echo form_dropdown('level',$data,set_value('level'));?></td>
		</tr>
        <tr>
			<td valign='top' width="19%">Periode</td>
			<td valign='top' width="1%">:</td>
			<td width="80%"><?php $data = array('name'=>'fromdate','id'=>'date1','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('fromdate',date('Y-m-d')));  echo form_input($data);?>   
   <?php $data = array('name'=>'todate','id'=>'date2','size'=>12,'readonly'=>'1','maxlength'=>'10', 'value'=>set_value('todate',date('Y-m-d')));
   echo " s.d ".form_input($data);?> <?=form_submit('submit','Preview');?></td>
		</tr>
                
         <?=form_close();?>
	</table>
	
    <table class="stripe">
    <tr>
    	<th width="3%"><div align="right">Level</div></th>
<?php if ($downline): 
	foreach($downline as $key => $row): ?>
      <th><div align="right"><?php echo $row['member_id']."<br>".$row['name']."<br>".$row['ftglaplikasi']."<br>"; if(isset($row['level0']))echo $row['level0']['ftotal']; else echo "0";?></div></th>
      
      <?php endforeach; 
	  else:?>      
      <th width="97%">0</th>
      <?php endif; ?>
    </tr>      
    
   <tr height='25'>
      <td align="right"><?=$page+1;?></td>
<?php if ($downline): 
	foreach($downline as $key => $row): ?>
    <td align="right"><?php if($row['level1'])echo $row['level1']['ftotal']; else echo "0";?></td>
    <?php endforeach; 
	endif;?>
  <tr>
   <tr height='25'>
      <td align="right"><?=$page+2;?></td>
<?php if ($downline): 
	foreach($downline as $key => $row): ?>
    <td align="right"><?php if($row['level2'])echo $row['level2']['ftotal']; else echo "0";?></td>
    <?php endforeach; 
	endif;?>
  <tr>      
  <tr height='25'>
      <td align="right"><?=$page+3;?></td>
<?php if ($downline): 
	foreach($downline as $key => $row): ?>
    <td align="right"><?php if($row['level3'])echo $row['level3']['ftotal']; else echo "0";?></td>
    <?php endforeach; 
	endif;?>
  <tr>      
  <tr height='25'>
      <td align="right"><?=$page+4;?></td>
<?php if ($downline): 
	foreach($downline as $key => $row): ?>
    <td align="right"><?php if($row['level4'])echo $row['level4']['ftotal']; else echo "0";?></td>
    <?php endforeach; 
	endif;?>
  <tr>      
   <tr height='25'>
      <td align="right"><?=$page+5;?></td>
<?php if ($downline): 
	foreach($downline as $key => $row): ?>
    <td align="right"><?php if($row['level5'])echo $row['level5']['ftotal']; else echo "0";?></td>
    <?php endforeach; 
	endif;?>
  <tr>      
  <tr height='25'>
      <td align="right"><?=$page+6;?></td>
<?php if ($downline): 
	foreach($downline as $key => $row): ?>
    <td align="right"><?php if($row['level6'])echo $row['level6']['ftotal']; else echo "0";?></td>
    <?php endforeach; 
	endif;?>
  <tr>      
  <tr height='25'>
      <td align="right"><?=$page+7;?></td>
<?php if ($downline): 
	foreach($downline as $key => $row): ?>
    <td align="right"><?php if($row['level7'])echo $row['level7']['ftotal']; else echo "0";?></td>
    <?php endforeach; 
	endif;?>
  <tr>      
  <tr height='25'>
      <td align="right"><?=$page+8;?></td>
<?php if ($downline): 
	foreach($downline as $key => $row): ?>
    <td align="right"><?php if($row['level8'])echo $row['level8']['ftotal']; else echo "0";?></td>
    <?php endforeach; 
	endif;?>
  <tr>      
  <tr height='25'>
      <td align="right"><?=$page+9;?></td>
<?php if ($downline): 
	foreach($downline as $key => $row): ?>
    <td align="right"><?php if($row['level9'])echo $row['level9']['ftotal']; else echo "0";?></td>
    <?php endforeach; 
	endif;?>
  <tr>      
  <tr height='25'>
      <td align="right"><?=$page+10;?></td>
<?php if ($downline): 
	foreach($downline as $key => $row): ?>
    <td align="right"><?php if($row['level10'])echo $row['level10']['ftotal']; else echo "0";?></td>
    <?php endforeach; 
	endif;?>
  <tr>      
  <tr height='25'>
      <td align="right"><strong>Jumlah</strong></td>
<?php if ($downline): 
	foreach($downline as $key => $row): ?>
    <td align="right"><strong><?php if($row['fjml'])echo $row['fjml']; else echo "0";?></strong></td>
    <?php endforeach; 
	endif;?>
  <tr>      
  <tr height="10">
  <td>&nbsp;</td>
  <?php if ($downline): 
	foreach($downline as $key => $row): ?>
		  <td>&nbsp;</td>
              <?php endforeach; 
	endif;?>
  </tr>
    <tr height='25'>
      <td align="right"><strong>TOTAL</strong></td>
<?php if ($downline): 
	foreach($downline as $key => $row): ?>
    <td align="right"><strong><?php if($row['total'])echo $row['total']['ftotal']; else echo "0";?></strong></td>
    <?php endforeach; 
	endif;?>
  <tr>      

</table>			                
                
<?php $this->load->view('footer');?>
<script type="text/javascript">
    function catcalc(cal){ var date = cal.date;}
    Calendar.setup({
        inputField     :    "date1",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    Calendar.setup({
        inputField     :    "date2",   // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,
        timeFormat     :    "24",
        onUpdate       :    catcalc
    });
    </script>
