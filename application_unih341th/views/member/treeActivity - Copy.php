<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>
<hr />

<?php echo form_open('member/activity/view', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off')); ?>
	<table width="55%">
		<tr>
			<td width="25%">Member ID</td>
			<td width="1%">:</td>
			<td width="74%"><?php 
				$data = array('name'=>'member_id','id'=>'member_id','maxlength'=>'20','size'=>'11','value'=>set_value('member_id'));
				echo form_input($data);
				$atts = array(
				  'width'      => '450',
				  'height'     => '500',
				  'scrollbars' => 'yes',
				  'status'     => 'yes',
				  'resizable'  => 'yes',
				  'screenx'    => '0',
				  'screeny'    => '0'
				);
				echo anchor_popup('memsearch/', '<input class="button" type="button" name="Button" value="browse" />', $atts); ?>
			</td>
		</tr>
		<tr>
			<td>Nama Member</td>
			<td>:</td>
			<td><?php 
				$data = array('name'=>'name','id'=>'name','maxlength'=>'20','readonly'=>'1','value'=>set_value('name'));
				echo form_input($data); 
				?>
			</td>
		</tr>                    
		<tr>
			<td>Periode</td>
			<td>:</td>
			<td><?php echo form_dropdown('periode',$periode,set_value('periode')); ?></td>                        
		</tr>
		<!-- 
		<tr>
			<td>Level ?</td>
			<td>:</td>
			<td><?php $data = array(''=>'All','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10');
			echo form_dropdown('level',$data); ?></td>
		</tr>
		-->
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<?php 
					echo " ".form_submit('submit','Preview'); 
					if($result){ echo '&nbsp;'.anchor($hplink,'download');}
				?>
			</td>
		</tr>
	</table>				
<?php	echo form_close();?>
	<link rel="StyleSheet" href="<?php echo base_url();?>/dtree/dtree.css" type="text/css" />
	<script type="text/javascript" src="<?php echo base_url();?>/dtree/dtree.js"></script>
	
<div class="dtree">
	<p class="cName"><a href="javascript: d.openAll();">Buka</a> | <a href="javascript: d.closeAll();">Tutup</a> <!--| <a href="javascript: d.openTo(2, true);">Open --></p>

	<script type="text/javascript">
		<!--		
		
		d = new dTree('d');
		d.config.useIcons=false;
		d.config.useCookies=false;
		d.config.inOrder=true;
		
		<?php if($result){ 
		foreach($result as $key => $row){ 
		if($key == 0){ ?>
			d.add('0','-1','<img src="<?=base_url();?>images/backend/<?=$row['jenjang_id'];?>i.png" height="17"> <?php 
				if($row['note']==1){$a="*";}else{$a="";} /* Updated by Boby 20131024 */
				echo $row['sponsorId']." | ".$row['level']." | ".$row['member_id']." | ".$a.$row['name']."(".$row['hp'].") | ps:".$row['fps']." | ".$row['joindate']." | "
				.$row['jenjang']." | tgpv:".$row['TGPV']." | bpp:".number_format($row['bpp'])." | bns aktifasi:".number_format($row['aktifasi'])." | "
				.$row['act']." | rekrut:".$row['rekrut']." | bulan:".$row['bln']
				;?>');
		<?php }else{?>
			d.add(<?php echo $row['auto_id'];?>,<?php echo $row['parentid'];?>,'<img src="<?=base_url();?>images/backend/<?=$row['jenjang_id'];?>i.png" height="17"> <?php 
				if($row['note']==1){$a="*";}else{$a="";} /* Updated by Boby 20131024 */
				echo $row['sponsorId']." | ".$row['level']." | ".$row['member_id']." | ".$a.$row['name']."(".$row['hp'].") | ps:".$row['fps']." | ".$row['joindate']." | "
				.$row['jenjang']." | tgpv:".$row['TGPV']." | bpp:".number_format($row['bpp'])." | bns aktifasi:".number_format($row['aktifasi'])." | "
				.$row['act']." | rekrut:".$row['rekrut']." | bulan:".$row['bln']
				;?>');
		<?php }?>
		
		<?php }}else{ ?>d.add(<?php echo '0';?>,'-1','Maaf, member diatas upline atau crossline anda.');<?php }?>
		document.write(d);

		//-->
	</script>
    <p class="cName"><a href="javascript: d.openAll();">Buka</a> | <a href="javascript: d.closeAll();">Tutup</a><br />
	<!-- Note: Level | Member ID | Nama Member | Posisi Sekarang | PV Pribadi | TGPV | APGS</p> -->
</div>

<?php $this->load->view('footer');?>