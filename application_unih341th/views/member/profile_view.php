		
			<table width="100%">
            	<tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA PENDAFTARAN</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
		<td colspan="6">
        	<table width="100%" style="font-size:medium">
	<tr>
		<td width='9%'>Tanggal Daftar</td>
		<td width='1%'>:</td>
		<td width='17%'><?php echo $row['ftglaplikasi'];?></td>
		<td width='17%'>Stockiest ID / Nama</td>
		<td width='1%'>:</td>
		<td width='19%'><?php if($row['stockiest_id'] == 'SH0000') echo "KANTOR UHN"; else echo $row['no_stc']." / ".$row['namastc'];?></td>
		<td width='17%'>Kelengkapan Data</td>
		<td width='1%'>:</td>
		<td width='18%'><?php if($row['lengkap'] == '1') echo "LENGKAP"; else echo "TIDAK LENGKAP";?></td>
	</tr>
            </table>	
        </td>
	</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA PRIBADI ANDA</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Member ID / Nama</td>
                    <td>:</td>
					<td><?php echo $row['id']." / ".$row['nama'];?></td>
					<td>Tempat Lahir</td>
                    <td>:</td>
                    <td><?php echo $row['tempatlahir'];?></td>
				</tr>
                <tr>
					<td>Jenis Kelamin</td>
                    <td>:</td>
					<td><?php echo $row['jk'];?></td>
					<td>Tanggal Lahir</td>
                    <td>:</td>
                    <td><?php echo $row['ftgllahir'];?></td>
				</tr>
				<tr>
					<td>Alamat</td>
                    <td>:</td>
					<td><?php echo $row['alamat'];?></td>
					<td>No. Telp</td>
                    <td>:</td>
                    <td><?php echo $row['telp'];?></td>
				</tr>
                <tr>
					<td>Kota</td>
                    <td>:</td>
					<td><?php echo $row['kota'];?></td>
					<td>No. Fax</td>
                    <td>:</td>
                    <td><?php echo $row['fax'];?></td>
				</tr>
                <tr>
					<td>Kode Pos</td>
                    <td>:</td>
					<td><?php echo $row['kodepos'];?></td>
					<td>No. HP</td>
                    <td>:</td>
                    <td><?php echo $row['hp'];?></td>
				</tr>
                <tr>
					<td>Propinsi</td>
                    <td>:</td>
					<td><?php echo $row['propinsi'];?></td>
					<td>Email</td>
                    <td>:</td>
                    <td><?php echo $row['email'];?></td>
				</tr>
                <tr>
					<td>No. KTP / SIM</td>
                    <td>:</td>
					<td><?php echo $row['noktp'];?></td>
					<td>Ahliwaris</td>
                    <td>:</td>
                    <td><?php echo $row['ahliwaris'];?></td>
				</tr>
				<!--
                <tr>
					<td></td>
                    <td></td>
                    <td></td>
                    <td>No. NPWP</td>
                    <td>:</td>
					<td><?php echo $row['npwp'];?></td>
				</tr>
				-->
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA BANK</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Nama Bank</td>
                    <td>:</td>
					<td><?php echo $row['namabank'];?></td>
					<td>Nama Nasabah</td>
                    <td>:</td>
                    <td><?php echo $row['namanasabah'];?></td>
				</tr>
                <tr>
					<td>Cabang</td>
                    <td>:</td>
					<td><?php echo $row['area'];?></td>
					<td>No. Rekening</td>
                    <td>:</td>
                    <td><?php echo $row['no'];?></td>
				</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td colspan="6"><b>DATA NPWP</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Nama</td>
                    <td>:</td>
					<td><?php echo $npwp['nama'];?></td>
					<td>Alamat NPWP</td>
                    <td>:</td>
                    <td><?php echo $npwp['alamat'];?></td>
				</tr>
                <tr>
					<td>No NPWP</td>
                    <td>:</td>
					<td><?php echo $npwp['npwp'];?></td>
					<td>Tgl NPWP</td>
                    <td>:</td>
                    <td><?php echo $npwp['npwp_date'];?></td>
				</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA SPONSOR</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Member ID</td>
                    <td>:</td>
					<td><?php echo $row['enroller_id'];?></td>
					<td>Nama Sponsor</td>
                    <td>:</td>
                    <td><?php echo $row['namaenroller'];?></td>
				</tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
                <tr>
					<td colspan="6"><b>DATA PENEMPATAN</b></td>
                </tr>
                <tr>
					<td colspan="6"><hr /></td>
                </tr>
				<tr>
					<td>Member ID</td>
                    <td>:</td>
					<td><?php echo $row['sponsor_id'];?></td>
					<td>Nama</td>
                    <td>:</td>
                    <td><?php echo $row['namasponsor'];?></td>
				</tr>
	</table>
