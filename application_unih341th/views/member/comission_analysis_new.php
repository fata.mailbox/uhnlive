<?php $this->load->view('header');?>
<h2><?php echo $page_title;?></h2>

<table width="100%">
<?php echo form_open('member/analysis/', array('id' => 'form', 'name' => 'form', 'autocomplete' => 'off'));?>	
        <tr>
			<td valign='top' width="19%">Periode</td>
			<td valign='top' width="1%">:</td>
			<td width="80%"><?php echo form_dropdown('periode1',$dropdown);?> s/d <?php echo form_dropdown('periode2',$dropdown);?> <?php echo form_submit('submit','preview');?> <?php echo form_submit('submit','export');?></td>
		</tr>
         <?php echo form_close();?>
    <tr><td colspan="3"><hr /></td></tr>
    <tr>
        <td>Total Omzet (Rp)</td>
        <td>:</td>
		<td><b><?php echo $results[0]['ftotalharga'];?></b></td>
	</tr>
	<tr>
        <td>Total Omzet (Rp) Nett</td>
        <td>:</td>
		<td><b><?php echo number_format($results[0]['totalharga']/1.1);?></b></td>
	</tr>
    <tr>
        <td>Total Omzet (BV)</td>
        <td>:</td>
		<td><b><?php echo $results[0]['ftotalbv'];?></b></td>
	</tr>
    <tr>
        <td>Total Payout Bonus Rp. </td>
        <td>:</td>
		<td><b><?php echo $total['fnominal'];?></b></td>
	</tr>
    <tr>
        <td>Ratio Payout Terhadap Rp.</td>
        <td>:</td>
		<td><b><?php if(isset($total['nominal'])<1) $ratiorp =0; else $ratiorp = round(($total['nominal']/($results[0]['totalharga']/1.1))*100,2); echo $ratiorp."%";?></b></td>
	</tr>
    <tr>
        <td>Ratio Payout Terhadap BV</td>
        <td>:</td>
		<td><b><?php if(isset($total['nominal'])<1) $ratiobv =0; else $ratiobv = round(($total['nominal']/($results[0]['totalbv']))*100,2); echo $ratiobv."%";?></b></td>
	</tr>
	</table>
	
<table class="stripe">
	<tr>
      <th width='5%'>No.</th>
      <th width='35%'>Description</th>
      <th width='15%'><div align="right">BV</div></th>
      <th width='20%'><div align="right">Nominal Rp</div></th>
      <th width='8%'><div align="right">% Rp</div></th>
      <th width='8%'><div align="right">% Bv</div></th>
    </tr>
   
<?php
if ($results):
	$i = 0;
	foreach($results as $key => $row):
		$i += 1;
?>
    <tr>
		<td><?php echo $i;?></td>
		<td><?php echo $row['titlebonus'];?></td>
		<td align="right"><?php echo $row['fbv'];?></td>
		<td align="right"><?php echo $row['fnominal'];?></td>
		<td align="right"><?php $rp = round(($row['nominal'] / ($row['totalharga'] / 1.1))*100,2); echo $rp."%";?></td>
		<td align="right"><?php if($row['totalbv']>0)$bv = round(($row['nominal'] / ($row['totalbv']))*100,2); else $bv=0; echo $bv."%";?></td>
	</tr>
    <?php endforeach; ?>
	<tr>
		<td colspan="3"><b>Total Bonus Rp. </b></td>
		<td align="right"><b><?php echo $total['fnominal'];?></b></td>
		<td align="right"><b><?php $ratiorp = round(($total['nominal']/($results[0]['totalharga']/1.1))*100,2); echo $ratiorp."%";?></b></td>
		<td align="right"><b><?php $ratiobv = round(($total['nominal']/($results[0]['totalbv']))*100,2); echo $ratiobv."%";?></b></td>
	</tr>
	<?php
	if ($pph21):
	?>
	<tr>
      <td colspan=100%>&nbsp;</td>
    </tr>
	<tr>
      <th width='5%'>No.</th>
      <th width='35%'>Description</th>
      <th width='15%'><div align="right">BV</div></th>
      <th width='20%'><div align="right">Nominal Rp</div></th>
      <th width='8%'>&nbsp;</th>
      <th width='8%'>&nbsp;</th>
    </tr>
    <?php
		foreach($pph21 as $keys => $pph):
	?>
	<tr>
		<td> - </td>
		<td><?php echo $pph['titlebonus'];?></td>
		<td align="right"><?php echo $pph['fbv'];?></td>
		<td align="right"><?php echo $pph['fnominal'];?></td>
		<td align="right"><?php // $rp = round(($pph['nominal'] / $results[0]['totalharga'])*100,2); echo $rp."%";?> &nbsp;</td>
	</tr>
	<?php
		endforeach;
	endif;
	?>
<?php else: ?>
    <tr>
      <td colspan="100%">Data is not available.</td>
    </tr>
<?php endif; ?>    
</table>

<?php $this->load->view('footer');?>
