var i=0;
$(document).on('ready', function() {
      $(".vertical-center-4").slick({
        dots: true,
        vertical: true,
        centerMode: true,
        slidesToShow: 4,
        slidesToScroll: 2
      });
      $(".vertical-center-3").slick({
        dots: true,
        vertical: true,
        centerMode: true,
        slidesToShow: 3,
        slidesToScroll: 3
      });
      $(".vertical-center-2").slick({
        dots: true,
        vertical: true,
        centerMode: true,
        slidesToShow: 2,
        slidesToScroll: 2
      });
      $(".vertical-center").slick({
        dots: true,
        vertical: true,
        centerMode: true,
      });
      $(".vertical").slick({
        dots: true,
        vertical: true,
        slidesToShow: 3,
        slidesToScroll: 3
      });
      $(".regular").slick({
        dots: true,
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 5
      });
      $(".center").slick({
        dots: true,
        infinite: true,
        centerMode: true,
        slidesToShow: 5,
        slidesToScroll: 3
      });
      $(".variable").slick({
        dots: true,
        infinite: true,
        variableWidth: true
      });
      $(".lazy").slick({
        lazyLoad: 'ondemand', // ondemand progressive anticipated
        infinite: true
      });
    });
 

function increment(){
  i+=1;                         /* function for automatic increament of feild's "Name" attribute*/                 
}

function removeElement(parentDiv, childDiv){
     if (childDiv == parentDiv) {
          alert("The parent div cannot be removed.");
     }
     else if (document.getElementById(childDiv)) {     
          var child = document.getElementById(childDiv);
          var parent = document.getElementById(parentDiv);
          parent.removeChild(child);
     }
     else {
          alert("Child div has already been removed or does not exist.");
          return false;
     }
}

function pageFunction()
{
     increment();
     var r=document.createElement('span');
     var table = document.createElement("table")
     table.setAttribute("class","stripe");

     var row = document.createElement("tr");
     var col1 = document.createElement("td");
     var a = document.createElement("input");
     a.setAttribute("type","text");
     a.setAttribute("name", "page[]");
     

     var col2 = document.createElement("td");
     var b = document.createElement("input");
     b.setAttribute("type","file");
     b.setAttribute("name", "img[]");

     var col3= document.createElement("td");
     var c = document.createElement("BUTTON");      
     var text = document.createTextNode("Delete");     
     c.appendChild(text); 

     

     col3.appendChild(c);
     col2.appendChild(b);
     col1.appendChild(a);

     row.appendChild(col1);
     row.appendChild(col2);
     row.appendChild(col3);
     table.appendChild(row);
     r.appendChild(table);

     c.setAttribute("type", "button");
     c.setAttribute("onclick", "removeElement('myForm','id_"+ i +"')");
     
     r.setAttribute("id", "id_"+i);
     document.getElementById("myForm").appendChild(r);
}

function resetElements(){
  document.getElementById('myForm').innerHTML = '';
}

function removeEditElement(parentDiv, childDiv){
     if (childDiv == parentDiv) {
          alert("The parent div cannot be removed.");
     }
     else if (document.getElementById(childDiv)) {     
          var child = document.getElementById(childDiv);
          var parent = document.getElementById(parentDiv);
          parent.removeChild(child);
     }
     else {
          alert("Child div has already been removed or does not exist.");
          return false;
     }
}