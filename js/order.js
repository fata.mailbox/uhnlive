
function ReplaceDoted(nStr)
{
	rtn = nStr.replace(/\$|\./g,'');
	rtn = rtn.replace(/\$|\,00/g,'');

	return rtn;
}
function formatCurrency(num)
{
	num = num.toString().replace(/\$|\,/g,'');
	num = ReplaceDoted(num);
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+'.'+
	num.substring(num.length-(4*i+3));
	if (num) return (((sign)?'':'-') + num);
}

function jumlah(a,b,result)
{
	var temp;
	b.value = formatCurrency(b.value);
	b = ReplaceDoted(b.value);
	a.value = formatCurrency(a.value);
	a = ReplaceDoted(a.value);
	temp = parseInt(a) * parseInt(b);
	result.value = formatCurrency(temp);
}

function diskon(a,b,result)
{
	var temp;
	a.value = formatCurrency(a.value);
	a = ReplaceDoted(a.value);
	b.value = formatCurrency(b.value);
	b = ReplaceDoted(b.value);
	temp = parseInt(a) * parseInt(b)/100;
	
	result.value = formatCurrency(temp);
}
function totalbayardiskon(a,b,result)
{
	var temp;
	b.value = formatCurrency(b.value);
	b = ReplaceDoted(b.value);
	a.value = formatCurrency(a.value);
	a = ReplaceDoted(a.value);
	temp = parseInt(a) - parseInt(b);
	result.value = formatCurrency(temp);
}

function bayar_curr (a,ttlrow,subtotal)
{
	var rtn = 0;
	var temp  = 0;
	a.value = formatCurrency(a.value);
	a = ReplaceDoted(a.value);
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subtotal+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	
	temp = parseInt(rtn) - parseInt(a);
	return formatCurrency(temp);
}
function total_curr (ttlrow,subtotal)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subtotal+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}
function totalpv_curr (ttlrow,subtotalpv)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subtotalpv+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}

function totalbv_curr (ttlrow,subtotalbv)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subtotalbv+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}

function totaldiskon_curr (ttlrow,subrpdiskon)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subrpdiskon+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}

function vtotal_curr (ttlrow,subtotal)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subtotal+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}
function vtotalpv_curr (ttlrow,subtotalpv)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subtotalpv+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}

function vtotalbv_curr (ttlrow,subtotalbv)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subtotalbv+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}

function totalAfterVoucher(a,b){
	var temp = 0;
	temp = parseInt(ReplaceDoted(a)) - parseInt(ReplaceDoted(b));
	return formatCurrency(temp);
}

function containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i] === obj) {
            return true;
        }
    }

    return false;
}