$(document).ready(function(){
	$("a[rel='screenshot']").colorbox();
	$("a[rel='signup'],area[rel='signup']").colorbox({iframe:true, innerWidth:572, innerHeight:330, rel:'nofollow'});
	$("a[rel='video'],area[rel='video']").colorbox({iframe:true, innerWidth:805, innerHeight:556, rel:'nofollow'});
	$("a[rel='video2']").colorbox({iframe:true, innerWidth:805, innerHeight:556});
});

function closeColorbox() {
	$.fn.colorbox.close();
}