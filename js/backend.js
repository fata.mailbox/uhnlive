function checkMail(email) {
	var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (filter.test(email)) {return true;}
	else {return false;}
}

function IsNumeric(fieldInfo) {
	var allowChars = "0123456789.";
	var IsNumeric=true;
	var Char;
	
	for (i = 0; i < fieldInfo.length && IsNumeric == true; i++) { 
		Char = fieldInfo.charAt(i); 
		if (allowChars.indexOf(Char) == -1) {
			IsNumeric = false;
		}
	}
	return IsNumeric;   
}

function addEvent( obj, type, fn ) {
	if (obj.addEventListener) {
		obj.addEventListener( type, fn, false );
		EventCache.add(obj, type, fn);
	}
	else if (obj.attachEvent) {
		obj["e"+type+fn] = fn;
		obj[type+fn] = function() { obj["e"+type+fn]( window.event ); }
		obj.attachEvent( "on"+type, obj[type+fn] );
		EventCache.add(obj, type, fn);
	}
	else {
		obj["on"+type] = obj["e"+type+fn];
	}
}

var EventCache = function(){
	var listEvents = [];
	return {
		listEvents : listEvents,
		add : function(node, sEventName, fHandler){
			listEvents.push(arguments);
		},
		flush : function(){
			var i, item;
			for(i = listEvents.length - 1; i >= 0; i = i - 1){
				item = listEvents[i];
				if(item[0].removeEventListener){
					item[0].removeEventListener(item[1], item[2], item[3]);
				};
				if(item[1].substring(0, 2) != "on"){
					item[1] = "on" + item[1];
				};
				if(item[0].detachEvent){
					item[0].detachEvent(item[1], item[2]);
				};
				item[0][item[1]] = null;
			};
		}
	};
}();
addEvent(window,'unload',EventCache.flush);

var stripe = function() {
  var tables = document.getElementsByTagName("table");  

  for(var x=0;x!=tables.length;x++){
    var table = tables[x];
	// if it isn't a table, or if it lacks a stripe class, skip it
	if (! table || document.getElementsByClassName("stripe")=="") { return; } 
	
    var tbodies = table.getElementsByTagName("tbody");
    
    for (var h = 0; h < tbodies.length; h++) {
		
	  var even = true;
      var trs = tbodies[h].getElementsByTagName("tr");
      
      for (var i = 0; i < trs.length; i++) {
        trs[i].onmouseover=function(){
          this.className += " ruled"; return false
        }
        trs[i].onmouseout=function(){
          this.className = this.className.replace("ruled", ""); return false
        }
        
        if(even)
          trs[i].className += " even";
        
        even = !even;
      }
    }
  }
}


function highlightInputs() {
	var inputs = document.getElementsByTagName('input');
	var i;
	for (i=0; i<inputs.length; i++) {
		inputs[i].onfocus = function() {
			this.style.background = '#F6F6F6';
		}
		inputs[i].onblur = function() {
			this.style.background = '#FFF';	
		}
	}
}

function showPrint() {
	if ($("invprintli")) {
		$("invprintli").style.display = "block";
	}
	
	// not the best place for this, but convenient enough for now
	if ($("private_note_form")) {
		$("private_note_form").style.display = "none"
	}
}

function requiredFields() {
	requiredFields = document.getElementsByClassName('requiredfield');
	validForm = 1;
	for (i=0; i<requiredFields.length; i++) {
		if (requiredFields[i].value == '') {
			validForm++;
			new Insertion.After (requiredFields[i].getAttribute('id'), ' <strong class="error">' + lang_field_required + '</strong>');
		}
	}
	if (validForm == 1) {
		return true;	
	} else {
		return false;
	}
}


addEvent (window, "load", highlightInputs);
addEvent (window, "load", stripe);
addEvent (window, "load", showPrint);

function ReplaceDoted(nStr)
{
	rtn = nStr.replace(/\$|\./g,'');
	rtn = rtn.replace(/\$|\,00/g,'');

	return rtn;
}
function formatCurrency(num)
{
	num = num.toString().replace(/\$|\,/g,'');
	num = ReplaceDoted(num);
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+'.'+
	num.substring(num.length-(4*i+3));
	if (num) return (((sign)?'':'-') + num);
}
function jumlah(a,b,result)
{
	var temp;
	b.value = formatCurrency(b.value);
	b = ReplaceDoted(b.value);
	a.value = formatCurrency(a.value);
	a = ReplaceDoted(a.value);
	temp = parseInt(a) * parseInt(b);
	result.value = formatCurrency(temp);
}
function jumlahvoucher(a,result)
{
	var temp;
	a.value = formatCurrency(a.value);
	a = ReplaceDoted(a.value);
	temp = parseInt(a);
	result.value = formatCurrency(temp);
}

function bayar_curr (a,ttlrow,subtotal)
{
	var rtn = 0;
	var temp  = 0;
	a.value = formatCurrency(a.value);
	a = ReplaceDoted(a.value);
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subtotal+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	
	temp = parseInt(rtn) - parseInt(a);
	return formatCurrency(temp);
}
function total_curr (ttlrow,subtotal)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subtotal+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}
function totalpv_curr (ttlrow,subtotalpv)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subtotalpv+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}

function totalbv_curr (ttlrow,subtotalbv)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subtotalbv+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}

function vtotal_curr (ttlrow,subtotal)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subtotal+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}
function vtotalpv_curr (ttlrow,subtotalpv)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subtotalpv+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}

function vtotalbv_curr (ttlrow,subtotalbv)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subtotalbv+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}

function containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i] === obj) {
            return true;
        }
    }

    return false;
}

function cleartext(text1,text2,text3) 
{
     text1.value = "";
     text2.value = "";
     text3.value = "0";
}

function cleartext2(text1,text2,text3,text4,text5) 
{
     text1.value = "";
     text2.value = "";
     text3.value = "0";
     text4.value = "0";
     text5.value = "0";
}

function subtotalMutasi(a,b,result)
{
	a.value = formatCurrency(a.value);
	a = ReplaceDoted(a.value);
	b.value = formatCurrency(b.value);
	b = ReplaceDoted(b.value);
	temp = parseInt(a) * parseInt(b);
	result.value = formatCurrency(temp);
}

function totalMutasiSearch()
{
	var a,b,c,d,e,f,g,h,i,j;
	a = ReplaceDoted(window.opener.document.form.tprice0.value);
	b = ReplaceDoted(window.opener.document.form.tprice1.value);
	c = ReplaceDoted(window.opener.document.form.tprice2.value);
	d = ReplaceDoted(window.opener.document.form.tprice3.value);
	e = ReplaceDoted(window.opener.document.form.tprice4.value);
	f = ReplaceDoted(window.opener.document.form.tprice5.value);
	g = ReplaceDoted(window.opener.document.form.tprice6.value);
	h = ReplaceDoted(window.opener.document.form.tprice7.value);
	i = ReplaceDoted(window.opener.document.form.tprice8.value);
	j = ReplaceDoted(window.opener.document.form.tprice9.value);

	temp = parseInt(a) + parseInt(b) + parseInt(c) + parseInt(d) + parseInt(e) + parseInt(f) + parseInt(g) + parseInt(h) + parseInt(i) + parseInt(j);
	window.opener.document.form.totalprice.value = formatCurrency(temp);
}

function totalMutasi()
{
	var a,b,c,d,e,f,g,h,i,j;
	a = ReplaceDoted(document.form.tprice0.value);
	b = ReplaceDoted(document.form.tprice1.value);
	c = ReplaceDoted(document.form.tprice2.value);
	d = ReplaceDoted(document.form.tprice3.value);
	e = ReplaceDoted(document.form.tprice4.value);
	f = ReplaceDoted(document.form.tprice5.value);
	g = ReplaceDoted(document.form.tprice6.value);
	h = ReplaceDoted(document.form.tprice7.value);
	i = ReplaceDoted(document.form.tprice8.value);
	j = ReplaceDoted(document.form.tprice9.value);

	temp = parseInt(a) + parseInt(b) + parseInt(c) + parseInt(d) + parseInt(e) + parseInt(f) + parseInt(g) + parseInt(h) + parseInt(i) + parseInt(j);
	document.form.totalprice.value = formatCurrency(temp);
}

function totalMutasiActualHilrus()
{
	var temp=0;
	var temp2=0;
	//alert(document.form.counter.value);
	for (i=1; i <= document.form.counter.value; i++){
		temp = temp + parseInt(ReplaceDoted(form["tprice"+i].value));
		temp2 = temp2 + parseInt(ReplaceDoted(form["rtprice"+i].value));
	}
	document.form.totalprice_act.value = formatCurrency(temp);
	document.form.totalprice_hilrus.value = formatCurrency(temp2);
}


function totalDeposit(a,b,c,d,result)
{
	var temp;
	a.value = formatCurrency(a.value);
	a = ReplaceDoted(a.value);
	b.value = formatCurrency(b.value);
	b = ReplaceDoted(b.value);
	c.value = formatCurrency(c.value);
	c = ReplaceDoted(c.value);
	d.value = formatCurrency(d.value);
	d = ReplaceDoted(d.value);
	temp = parseInt(a) + parseInt(b) +  parseInt(c) +  parseInt(d);
	result.value = formatCurrency(temp);
}
function totalBayar(a,b,c,result)
{
	var temp;
	a.value = formatCurrency(a.value);
	a = ReplaceDoted(a.value);
	b.value = formatCurrency(b.value);
	b = ReplaceDoted(b.value);
	c.value = formatCurrency(c.value);
	c = ReplaceDoted(c.value);
	temp = parseInt(a) + parseInt(b) +  parseInt(c);
	result.value = formatCurrency(temp);
}

function diskon(a,b,result)
{
	var temp;
	a.value = formatCurrency(a.value);
	a = ReplaceDoted(a.value);
	b.value = formatCurrency(b.value);
	b = ReplaceDoted(b.value);
	temp = parseInt(a) * parseInt(b)/100;
	
	result.value = formatCurrency(temp);
}
function totalbayardiskon(a,b,result)
{
	var temp;
	b.value = formatCurrency(b.value);
	b = ReplaceDoted(b.value);
	a.value = formatCurrency(a.value);
	a = ReplaceDoted(a.value);
	temp = parseInt(a) - parseInt(b);
	result.value = formatCurrency(temp);
}
function totalbayardiskon1(a,b,c,result)
{
	var temp;
	c.value = formatCurrency(c.value);
	c = ReplaceDoted(c.value);
	b.value = formatCurrency(b.value);
	b = ReplaceDoted(b.value);
	a.value = formatCurrency(a.value);
	a = ReplaceDoted(a.value);
	temp = parseInt(a) - parseInt(b) - parseInt(c);
	result.value = formatCurrency(temp);
}
function cleartext7(text1,text2,text3,text4,text5,text6,text7) 
{
     text1.value = "";
     text2.value = "";
     text3.value = "0";
     text4.value = "0";
     text5.value = "0";
     text6.value = "0";
     text7.value = "0";
}
function cleartext7a(text1,text2,text3,text4,text5,text6,text7) 
{
     text1.value = "";
     text2.value = "0";
     text3.value = "0";
     text4.value = "0";
     text5.value = "0";
     text6.value = "0";
     text7.value = "0";
}
function cleartext8(text1,text2,text3,text4,text5,text6,text7,text8) 
{
     text1.value = "";
     text2.value = "";
     text3.value = "0";
     text4.value = "0";
     text5.value = "0";
     text6.value = "0";
     text7.value = "0";
	 text8.value = "0";
}
function cleartext9(text1,text2,text3,text4,text5,text6,text7,text8,text9) 
{
     text1.value = "";
     text2.value = "";
     text3.value = "0";
     text4.value = "0";
     text5.value = "0";
     text6.value = "0";
     text7.value = "0";
	 text8.value = "0";
	 text9.value = "0";
}
function cleartext10(text1,text2,text3,text4,text5,text6,text7,text8,text9,text10) 
{
     text1.value = "";
     text2.value = "";
     text3.value = "0";
     text4.value = "0";
     text5.value = "0";
     text6.value = "0";
     text7.value = "0";
	 text8.value = "0";
	 text9.value = "0";
	 text10.value = "0";
}
function cleartext11(text1,text2,text3,text4,text5,text6,text7,text8,text9,text10,text11) 
{
     text1.value = "";
     text2.value = "";
     text3.value = "0";
     text4.value = "0";
     text5.value = "0";
     text6.value = "0";
     text7.value = "0";
	 text8.value = "0";
	 text9.value = "0";
	 text10.value = "0";
	 text11.value = "0";
}

function cleartext5(text1,text2,text3,text4,text5) 
{
     text1.value = "";
     text2.value = "";
     text3.value = "0";
     text4.value = "0";
     text5.value = "0";
}
function cleartext4(text1,text2,text3,text4) 
{
     text1.value = "";
     text2.value = "";
     text3.value = "0";
     text4.value = "0";
}
function total_disc (ttlrow,diskon)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(diskon+i);
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}
function recount_disc (ttlrow, pv, price, persen, rpdiskon)
{
	var pv_ = 0;
	var price_ = 0;
	var rpdiskon = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		pv_ = eval(subtotalpv+i);
		price_ = eval(price+i);
		
		pv_ = parseInt (ReplaceDoted(pv_.value));
		price_ = parseInt (ReplaceDoted(price_.value));
			
		if(pv_ > 0){
			rpdiskon += (price_ * persen.value)/100;
		}else{
			rpdiskon += 0;
		}
	}
	return formatCurrency(rpdiskon);
}

function totaldiskon_curr (ttlrow,subrpdiskon)
{
	var rtn = 0;
	
	for (i=0; i < ttlrow; i++)
	{
		var str = eval(subrpdiskon+i);
		if(!str.value) str.value=0;
		
		rtn += parseInt (ReplaceDoted(str.value));
	}
	return formatCurrency(rtn);
}
function totalAfterVoucher(a,b){
	var temp = 0;
	temp = parseInt(ReplaceDoted(a)) - parseInt(ReplaceDoted(b));
	return formatCurrency(temp);
}

function validOri(){
	var fak = 0;
	var tot = document.form.counter.value;
	var ori = 0;
	var act = 0;
	var hil = 0;
	var fori = 'qtyOri';
	var fqty = 'qty';
	var frqty = 'rqty';
	//alert('oi');
	//alert(tot);
	for (i=1; i <= tot; i++)
	{ 
		
		if( parseInt (ReplaceDoted(form["qtyOri"+i].value)) != parseInt (ReplaceDoted(form["qty"+i].value)) + parseInt (ReplaceDoted(form["rqty"+i].value)) )
		{
			alert ("Produk no. "+i+" : jumlah actual + jumlah hilang tidak sama dengan jumlah intransit");
			return false;
		}else{
			return true;	
		}
	}
	//return false;
}