<?php
/*
header("Content-Type: text/csv");
header("Content-Disposition: attachment; filename=file.csv");
// Disable caching
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
header("Pragma: no-cache"); // HTTP 1.0
header("Expires: 0"); // Proxies
*/

function outputCSV($data) {
    $output = fopen("efakturfile/faktur_pajak_keluaran_non_npwp_".date("YmdHis").".csv", "w");
    //$output = fopen("/home/deploy/source/sohomlm/efakturfile/faktur_pajak_keluaran_non_npwp_".date("YmdHis").".csv", "w");
    //$output = fopen("file.csv", "w");
    foreach ($data as $row) {
        fputcsv($output, $row, ';'); // here you can change delimiter/enclosure
    }
    fclose($output);
}

//----------------------------- database ---------------------------//
/*
$nama_svr = 'localhost';
$nama_db = 'soho_20200331_rbfinclose'; 
$nama_usr = 'admin';
$pwd_usr = 'P@ssw0rd';
*/
$nama_svr = 'localhost';
$nama_db = 'soho_20200531_rbfinclose';
$nama_usr = 'admin';
$pwd_usr = 'P@ssw0rd';

	$con = mysqli_connect($nama_svr, $nama_usr, $pwd_usr);
	if(!$con){
	  trigger_error("Problem connecting to server");
	}	
	$db =  mysqli_select_db( $con,$nama_db);
	if(!$db){
	   trigger_error("Problem selecting database");
	}	
//--------------------------- EOF database ------------------------//

/*
outputCSV(array(
    array("name 1", "age 1", "city 1"),
    array("name 2", "age 2", "city 2"),
    array("name 3", "age 3", "city 3")
));
*/
$yearTf = '2020';
$monthTf = '5';

$queryItem 	= "
SELECT 'A' AS 'Kode Pajak'
	,'2' AS 'Kode Transaksi'
	,'1'AS 'Kode Status'
	,CASE WHEN tf.`type` = 'Retur' THEN '2'
		ELSE '1' END AS 'Kode Dokumen'
	,'0'AS 'Flag VAT'
	-- ,CASE WHEN npwp.npwp IS NULL THEN '00.000.000.0-000.000'
    --           ELSE CONCAT(
    --                            LEFT(npwp.npwp,2),'.'
    --                            ,RIGHT(LEFT(npwp.npwp,5),3),'.'
    --                            ,RIGHT(LEFT(npwp.npwp,8),3),'.'
    --                            ,RIGHT(LEFT(npwp.npwp,9),1),'-'
    --                            ,LEFT(RIGHT(npwp.npwp,6),3),'.'
    --                            ,LEFT(RIGHT(npwp.npwp,3),3)
    --            )
                -- npwp.npwp
    --            END AS 'npwp'-- 'NPWP / Nomor Paspor'
	,CASE WHEN npwp.npwp IS NULL THEN '000000000000000'
                ELSE npwp.npwp
                END AS 'npwp'-- 'NPWP / Nomor Paspor'
	,m.nama AS 'nama_lawan_transaksi'-- 'Nama Lawan Transaksi'
	-- ,tf.faktur_pajak AS 'no_faktur_pajak'-- 'Nomor Faktur / Dokumen'
	,replace(replace(tf.faktur_pajak,'.',''),'-','') AS 'no_faktur_pajak'-- 'Nomor Faktur / Dokumen'
	,CASE WHEN tf.`type` = 'Retur' THEN '1'
		ELSE '0' END AS 'Jenis Dokumen'
	
	,IFNULL(tr.facts_id,'') AS 'Nomor Faktur Pengganti / Retur'
	
	,CASE WHEN tf.`type` = 'Retur' THEN '1'
		ELSE ' ' END AS 'Jenis Dokumen Dokumen Pengganti / Retur'
	
	-- ,CONCAT(DAY(tf.tgl),'/',MONTH(tf.tgl),'/',YEAR(tf.tgl)) AS 'tanggal_faktur'-- 'Tanggal Faktur / Dokumen'
	,CONCAT(LPAD(DAY(tf.tgl),2,'0'),'/',LPAD(MONTH(tf.tgl),2,'0'),'/',YEAR(tf.tgl)) AS 'tanggal_faktur'-- 'Tanggal Faktur / Dokumen'
	,' ' AS 'Tanggal SSP'
	-- ,CONCAT(LEFT('00', 2-(LENGTH(MONTH(tf.tgl)))), MONTH(tf.tgl),LEFT('00', 2-(LENGTH(MONTH(tf.tgl)))), MONTH(tf.tgl))AS 'masa_pajak'-- 'Masa Pajak'
	,MONTH(tf.tgl) AS 'masa_pajak'-- 'Masa Pajak'
	,YEAR(tf.tgl) AS 'tahun_pajak'-- 'Tahun Pajak'	
	,'0' AS 'Pembetulan'
	
	,CASE WHEN tf.`type` = 'SO' THEN ROUND(tf.total*(1/1.1))
                                WHEN tf.`type` = 'RO' THEN ROUND(tf.total*(1/1.1))
                                WHEN tf.`type` = 'SCP' THEN ROUND(tf.total*(1/1.1))
                                WHEN tf.`type` = 'Retur' THEN ROUND(tf.total*(1/1.1))*(-1)
                                WHEN tf.`type` = 'NC' THEN ROUND(tf.total)
                                ELSE '0'
                                END AS DPP
                                ,
                                CASE WHEN tf.`type` = 'SO' THEN ROUND(tf.total*(0.1/1.1))
                                WHEN tf.`type` = 'RO' THEN ROUND(tf.total*(0.1/1.1))
                                WHEN tf.`type` = 'SCP' THEN ROUND(tf.total*(0.1/1.1))
                                WHEN tf.`type` = 'Retur' THEN ROUND(tf.total*(0.1/1.1))*(-1)
                                WHEN tf.`type` = 'NC' THEN ROUND(tf.total*0.1)
                                ELSE '0'
                                END AS PPN	
								
	-- ,CASE WHEN tf.`type` = 'SO' THEN (tf.total*(1/1.1))
    --                            WHEN tf.`type` = 'RO' THEN (tf.total*(1/1.1))
    --                            WHEN tf.`type` = 'SCP' THEN (tf.total*(1/1.1))
    --                           WHEN tf.`type` = 'Retur' THEN (tf.total*(1/1.1))*(-1)
    --                            WHEN tf.`type` = 'NC' THEN (tf.total)
    --                            ELSE '0'
    --                            END AS DPP
    --                            ,
    --                            CASE WHEN tf.`type` = 'SO' THEN (tf.total*(0.1/1.1))
    --                            WHEN tf.`type` = 'RO' THEN (tf.total*(0.1/1.1))
    --                            WHEN tf.`type` = 'SCP' THEN (tf.total*(0.1/1.1))
    --                            WHEN tf.`type` = 'Retur' THEN (tf.total*(0.1/1.1))*(-1)
    --                            WHEN tf.`type` = 'NC' THEN (tf.total*0.1)
    --                            ELSE '0'
    --                            END AS PPN	
	,'0'AS ppnbm -- PPnBM
	-- ,npwp.alamat AS 'alamat_lengkap'
	,IFNULL(npwp.alamat,'-') AS 'alamat_lengkap'
	,tf.inv_id
	,tf.type
FROM tax_facts tf
LEFT JOIN tax_return tr ON tf.inv_id=tr.rtr_id AND tf.type='Retur'
LEFT JOIN (
	SELECT MAX(id)AS id,member_id
	FROM npwp
	-- where npwp.npwp not like '%.%'
	GROUP BY member_id
	ORDER BY id DESC
	)AS npwp_d ON tf.member_id=npwp_d.member_id
LEFT JOIN npwp ON npwp.id=npwp_d.id
LEFT JOIN member m ON tf.member_id=m.id
WHERE 
YEAR(tf.tgl) = '".$yearTf."'
and MONTH(tf.tgl) = ".$monthTf."
	-- and tf.id>=1780
	-- and tf.id>=22772
	-- and (tf.urut >= 9109637 AND tf.urut <= 9109737)
and tf.type <> 'Retur'
and npwp.npwp is null
-- and m.nama like 'tio setyo%'
ORDER BY tf.tgl, RIGHT(tf.faktur_pajak,8)
-- limit 0,1
";
$arrayData =  array(
				array("FK", "KD_JENIS_TRANSAKSI", "FG_PENGGANTI", "NOMOR_FAKTUR", "MASA_PAJAK", "TAHUN_PAJAK", "TANGGAL_FAKTUR", "NPWP", "NAMA", "ALAMAT_LENGKAP", "JUMLAH_DPP", "JUMLAH_PPN", "JUMLAH_PPNBM", "ID_KETERANGAN_TAMBAHAN", "FP_UANG_MUKA", "UANG_MUKA_DPP", "UANG_MUKA_PPN", "UANG_MUKA_PPNBM", "REFERENSI"),
				array("LT", "NPWP", "NAMA", "JALAN", "BLOK", "NOMOR", "RT", "RW","KECAMATAN", "KELURAHAN", "KABUPATEN", "PROPINSI","KODE_POS", "NOMOR_TELEPON"),
				array("OF", "KODE_OBJEK", "NAMA", "HARGA_SATUAN", "JUMLAH_BARANG", "HARGA_TOTAL", "DISKON", "DPP","PPN", "TARIF_PPNBM", "PPNBM")
					);
$queryData	= mysqli_query($con,$queryItem) or die(mysqli_error($con));
$xx = 3;
echo 'start retrive data /n/r';
while ($row = mysqli_fetch_array($queryData, MYSQLI_ASSOC))
{
	//$result[] = $row;
	/*
	array("FK", "KD_JENIS_TRANSAKSI", "FG_PENGGANTI", "NOMOR_FAKTUR", "MASA_PAJAK", "TAHUN_PAJAK", "TANGGAL_FAKTUR", "NPWP", "NAMA", "ALAMAT_LENGKAP", "JUMLAH_DPP", "JUMLAH_PPN", "JUMLAH_PPNBM", "ID_KETERANGAN_TAMBAHAN", "FP_UANG_MUKA", "UANG_MUKA_DPP", "UANG_MUKA_PPN", "UANG_MUKA_PPNBM", "REFERENSI"),
    array("LT", "NPWP", "NAMA", "JALAN", "BLOK", "NOMOR", "RT", "RW","KECAMATAN", "KELURAHAN", "KABUPATEN", "PROPINSI","KODE_POS", "NOMOR_TELEPON"),
    array("OF", "KODE_OBJEK", "NAMA", "HARGA_SATUAN", "JUMLAH_BARANG", "HARGA_TOTAL", "DISKON", "DPP","PPN", "TARIF_PPNBM", "PPNBM")
	*/
	
	//$arrayData[$xx] = array("FK", "01", "0", substr($row['no_faktur_pajak'],3,13), $row['masa_pajak'], $row['tahun_pajak'], $row['tanggal_faktur'], $row['npwp'], $row['nama_lawan_transaksi'], $row['alamat_lengkap'], round($row['DPP'],0,PHP_ROUND_HALF_DOWN), round($row['PPN'],0,PHP_ROUND_HALF_DOWN), "0", "0", "0", "0", "0", "0", $row['inv_id']);
	$arrayData[$xx] = array("FK", "01", "0", substr($row['no_faktur_pajak'],3,13), $row['masa_pajak'], $row['tahun_pajak'], $row['tanggal_faktur'], $row['npwp'], $row['nama_lawan_transaksi'], $row['alamat_lengkap'], $row['DPP'], $row['PPN'], "0", "0", "0", "0", "0", "0", $row['inv_id']);
	//$arrayData[$xx] = array("FK", "01", "0", substr($row['no_faktur_pajak'],3,13), $row['masa_pajak'], $row['tahun_pajak'], $row['tanggal_faktur'], $row['npwp'], $row['nama_lawan_transaksi'], $row['alamat_lengkap'], 0, 0, "0", "0", "0", "0", "0", "0", $row['inv_id']);
	$currHeaderPosition = $xx;
	//$xx++;
    //$arrayData[$xx] = array("LT", $row['npwp'], $row['nama_lawan_transaksi'], "-", "-", "-", "0", "0","-", "-", "-", "-","-", "-");
	$xx++;
		if($row['type']=="RO"){
			$qry1 = "	SELECT rod.item_id, i.name as prd_, rod.jmlharga, rod.qty, rod.harga -- 0 AS qty, 0 AS harga
						, i.satuan, CONCAT(i.name,' (', rod.qty, ' ', REPLACE(i.satuan, '`', ''),')') as prd
						, 0 as disc, round(rod.jmlharga/1.1) as tprc, round(rod.jmlharga/1.1) - 0 as dpp,  round((round(rod.jmlharga/1.1) - 0)*0.1) as ppn
						FROM ro_d rod
						LEFT JOIN item i ON rod.item_id = i.id
						WHERE ro_id = '".$row["inv_id"]."' ";
		}else if($row['type']=="SO"){
			$qry1 = "	SELECT rod.item_id, i.name as prd_, rod.jmlharga, rod.qty, rod.harga -- 0 AS qty, 0 AS harga
						, i.satuan, CONCAT(i.name,' (', rod.qty, ' ', REPLACE(i.satuan, '`', ''),')') as prd
						, 0 as disc, round(rod.jmlharga/1.1) as tprc, round(rod.jmlharga/1.1) - 0 as dpp,  round((round(rod.jmlharga/1.1) - 0)*0.1) as ppn
						FROM so_d rod
						LEFT JOIN item i ON rod.item_id = i.id
						WHERE so_id = '".$row["inv_id"]."' ";
		}else if($row['type']=="Retur"){
			$qry1 = "	SELECT rod.item_id, i.name as prd_, rod.jmlharga, rod.qty, rod.harga
						, i.satuan, CONCAT(i.name,' (', rod.qty, ' ', REPLACE(i.satuan, '`', ''),')') as prd
						, 0 as disc, round(rod.jmlharga/1.1) as tprc, round(rod.jmlharga/1.1) - 0 as dpp,  round((round(rod.jmlharga/1.1) - 0)*0.1) as ppn
						FROM retur_titipan_d rod
						LEFT JOIN item i ON rod.item_id = i.id
						WHERE retur_titipan_id = '".$row["inv_id"]."' ";
		}else if($row['type']=="NC"){
			$qry1 = "	SELECT rod.item_id, i.name as prd_, rod.hpp*rod.qty as jmlharga, rod.qty, rod.hpp as harga-- 0 AS qty, 0 AS harga
						, i.satuan, CONCAT(i.name,' (', rod.qty, ' ', REPLACE(i.satuan, '`', ''),')') as prd
						, 0 as disc, round(rod.hpp*rod.qty/1.1) as tprc, round(rod.hpp*rod.qty/1.1) - 0 as dpp,  round((round(rod.hpp*rod.qty/1.1) - 0)*0.1) as ppn
						FROM ncm_d rod
						LEFT JOIN item i ON rod.item_id = i.id
						WHERE ncm_id = '".$row["inv_id"]."' ";
			/* updated by Boby 20130417 */
			$tprc = $fetch['total'];
			$ppn = $dpp * 0.1;
			/* end updated by Boby 20130417 */
		}else{
			$qry1 = "	SELECT rod.item_id, i.name as prd_, rod.jmlharga, rod.qty, rod.harga -- , 0 AS qty, 0 AS harga
						, i.satuan, CONCAT(i.name,' (', rod.qty, ' ', REPLACE(i.satuan, '`', ''),')') as prd
						, 0 as disc, round(rod.jmlharga/1.1) as tprc, round(rod.jmlharga/1.1) - 0 as dpp,  round((round(rod.jmlharga/1.1) - 0)*0.1) as ppn
						FROM pinjaman_titipan_d rod
						LEFT JOIN item i ON rod.item_id = i.id
						WHERE pinjaman_titipan_id = '".$row["inv_id"]."' ";
		}
		
	$rs1 = mysqli_query($con,$qry1) or die(mysqli_error($con));
	$dpp = 0;
	$ppn = 0;
	$totaldpp = 0;
	$totalppn = 0;
	while($fetch1 = mysqli_fetch_array($rs1, MYSQLI_ASSOC)){
		unset($dpp); unset($ppn); unset($tprc); unset($disc);
		
		$tprc = $fetch1['jmlharga']/1.1; // 123456;
		$disc = 0;
		$dpp = round($tprc - $disc,0,PHP_ROUND_HALF_DOWN);
		$ppn = round($dpp * 0.1,0,PHP_ROUND_HALF_DOWN);
		$totaldpp+= $dpp;
		$totalppn+= $ppn;
		/*
		$tprc = $fetch1['tprc']; // 123456;
		$disc = $fetch1['disc'];
		$dpp = $fetch1['dpp'];
		$ppn = $fetch1['ppn'];
		*/
		//    array("OF", "KODE_OBJEK", "NAMA", "HARGA_SATUAN", "JUMLAH_BARANG", "HARGA_TOTAL", "DISKON", "DPP","PPN", "TARIF_PPNBM", "PPNBM")

		$arrayData[$xx] = array("OF", $fetch1['item_id'], $fetch1['prd_'],  $fetch1['harga'], $fetch1['qty'], $fetch1['jmlharga'], $disc, $dpp,$ppn, "0", "0");
		$xx++;
	}
	$arrayData[$currHeaderPosition][10] = $totaldpp;//dpp;
	$arrayData[$currHeaderPosition][11] = $totalppn;//ppn;
	//echo $totaldpp.'<br>';
	//echo $totalppn.'<br>';
	//var_dump($arrayData[$currHeaderPosition]);
	//echo '<br>';
}
echo 'write CSV /n/r';
outputCSV($arrayData);
echo 'done /n/r';
//----------------------------- database ---------------------------//
 mysqli_close($con);
//--------------------------- EOF database ------------------------//
?>