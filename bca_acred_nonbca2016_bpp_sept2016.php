 <?php
 //----------------------------- database ---------------------------//
$nama_svr = 'localhost';//'sgh-production.cy9qiwmgjpp1.ap-southeast-1.rds.amazonaws.com';
$nama_db = 'soho';
$nama_usr = 'admin';
$pwd_usr = 'P@ssw0rd';//'H!21h3j00q1';//

	$con = mysqli_connect($nama_svr, $nama_usr, $pwd_usr);
	if(!$con){
	  trigger_error("Problem connecting to server");
	}	
	$db =  mysqli_select_db( $con,$nama_db);
	if(!$db){
	   trigger_error("Problem selecting database");
	}	
//--------------------------- EOF database ------------------------//

 

$petik = ""; //"\'";
            $query = "SELECT ifnull(btb.bi_code,'') as bicode, dt.*
                FROM(
                        SELECT namaNasabah, bank_id, MAX(cabang)AS cabang, concat('".$petik."',`no`) as noRekening, SUM(ewallet)AS payoutTransfer
                                -- , MAX(biayaTransfer)AS biayaTransfer
                                -- , SUM(ewallet) -  MAX(biayaTransfer) AS tkHomePayed
                                , CASE 
									WHEN MAX(urut) = 0 THEN 0
									WHEN MAX(urut) = 1 THEN MAX(biayaTransfer)
								  END AS biayaTransfer
                                , CASE
									WHEN MAX(urut) = 0 THEN SUM(ewallet)
									WHEN MAX(urut) = 1 THEN	SUM(ewallet) -  MAX(biayaTransfer) 
								  END AS tkHomePayed
                                -- , urut2, flag
                                , CASE 
                                        WHEN MAX(urut) = 0 THEN 'BCA'
                                        WHEN MAX(urut) = 1 THEN 'Non BCA'
                                        ELSE 'NoValid'
                                END AS urut
								, 'Bonus Member' as Berita
                        FROM
                        (
                                SELECT dtbr.memberId AS memberId, dtbr.account_id
                                        , CASE WHEN dtbr.nama <> dtbr.namaNasabah THEN 'Cek' ELSE '' END AS note
                                        , dtbr.ewallet - IFNULL(bns.bns,0) AS b4, IFNULL(bns.bns,0)AS bns
                                        , dtbr.ewallet - 5000 AS ewallet, dtbr.nama AS nama, dtbr.namaNasabah
                                        , dtbr.bank_id AS bank_id, dtbr.cabang AS cabang
                                        , dtbr.urut AS urut2, flag
                                        , CASE 
                                                WHEN dtbr.urut THEN 5000
                                                WHEN flag = 2 THEN 2500
                                                WHEN flag = 1 THEN 0
                                                ELSE 'n/a'
                                        END AS biayaTransfer
                                        , IFNULL(REPLACE(REPLACE(REPLACE(dtbr.no,'-',''),'.',''),' ',''),'-') AS NO
                                        -- , dtbr.no as no
                                        , dtbr.urut
                                FROM(
                                        SELECT dt.*, CASE 
                                                        WHEN 
                                                                -- `no` LIKE '%000000%' 
                                                                `no` REGEXP '^[0-0]+$' -- LIKE '%000000%' 
                                                                OR namaNasabah LIKE '%000000%' 
                                                                OR `no` IS NULL 
                                                                OR (bank_id = 'BCA' AND LENGTH(REPLACE(REPLACE(REPLACE(`no`,'.',''),' ',''),'-','')) <> 10 )
                                                                OR bank_id = '-' 
																OR ( LENGTH(REPLACE(REPLACE(REPLACE(`no`,'.',''),' ',''),'-','')) < 8 )
                                                        THEN '2'
                                                        -- WHEN bank_id = 'BCA' AND `no` NOT LIKE '%000000%' THEN '0'
														WHEN bank_id = 'BCA' AND `no` NOT REGEXP '^[0-0]+$' THEN '0' 
                                                        ELSE '1' END AS urut
                                        FROM(
                                                SELECT 
                                                        m.id AS memberId, m.ewallet, m.nama, m.account_id
                                                        , IFNULL(acc.bank_id,'-')AS bank_id, IFNULL(acc.area,'-') AS cabang
                                                        , IFNULL(acc.no,'_')AS NO, UCASE(IFNULL(acc.`name`,'-')) AS namaNasabah
                                                        , acc.flag
                                                FROM member m
                                                LEFT JOIN account acc ON m.account_id = acc.id
                                                WHERE m.ewallet >= 55000 AND m.id <> '00000001'
                                                -- and acc.bank_id = 'BCA'
                                                GROUP BY m.id
                                                ORDER BY acc.bank_id DESC, m.ewallet DESC
                                        )AS dt
                                        GROUP BY dt.memberId
                                        ORDER BY dt.bank_id DESC, dt.ewallet DESC
                                )AS dtbr
                                LEFT JOIN(
                                        SELECT member_id, SUM(nominal) AS bns
                                        FROM bonus
                                        WHERE periode = LAST_DAY(CURDATE() - INTERVAL 1 MONTH)
                                        GROUP BY member_id
                                )AS bns ON dtbr.memberId = bns.member_id
                                WHERE ewallet-5000 > 50000
                                ORDER BY urut, bank_id, ewallet DESC
                        )AS dt
                        -- WHERE bank_id = 'BCA'
                        GROUP BY namaNasabah, bank_id, `no`
                        ORDER BY urut, namaNasabah
                )AS dt
				left join bank_to_bicode btb ON dt.bank_id = btb.id
                WHERE urut <> 'NoValid'
				and urut = 'Non BCA'
				-- agustus 2016, september 2016
				and noRekening NOT IN (
				'zulimaQQAGUNGSET'
				)
				having bicode <> ''
				-- limit 0,64

                ";
$query = "";
$query = "
SELECT IFNULL(btb.bi_code,'') AS bicode, dt.*
                FROM(
SELECT 
 -- m.sponsor_id
dt.member_id
, dt.nama
, dt.jenjang_id AS jenjang_old
, dt.jenjang_new
, dt.ps AS pv
, dt.bv
, dt.pgs AS pgs
, ROUND((dt.bv*j.pb)/100) AS bpp
, CASE 
	WHEN urut = 0 THEN 0
	WHEN urut = 1 THEN biayaTransfer
END AS biayaTransfer
, CASE
	WHEN urut = 0 THEN ROUND((dt.bv*j.pb)/100)
	WHEN urut = 1 THEN ROUND((dt.bv*j.pb)/100) -  biayaTransfer 
END AS tkHomePayed
-- , dt.*
, dt.bank_id
, dt.namaNasabah
, dt.no_rek AS noRekening
, dt.cabang
, CASE 
   WHEN dt.urut = 0 THEN 'BCA'
   WHEN dt.urut = 1 THEN 'Non BCA'
   ELSE 'NoValid'
   END AS urut
, 'Bonus Member' AS Berita
FROM 
(
	SELECT dt.*
		, CASE 
		     WHEN dt.urut THEN 5000
		     WHEN dt.flag = 2 THEN 2500
		     WHEN dt.flag = 1 THEN 0
		     ELSE 'n/a'
		  END AS biayaTransfer	
	FROM (
		SELECT 
		CASE
			WHEN m.jenjang_id < 4 AND pb.ps+pb.pgs >= 40000000 AND pb.ps >= 1000000 THEN 4 	
			WHEN m.jenjang_id < 3 AND pb.ps+pb.pgs >= 20000000 AND pb.ps >= 1000000 THEN 3	
			WHEN m.jenjang_id < 2 AND pb.ps+pb.pgs >= 1000000 THEN 2
		END AS jenjang_new	
		, pb.ps
		, pb.pgs
		, pb.bv
		,m.id AS member_id, m.nama , m.jenjang_id
		, dt.bank_id
		, dt.namaNasabah--  AS nama_rek
		, dt.cabang
		, IFNULL(REPLACE(REPLACE(REPLACE(dt.no,'-',''),'.',''),' ',''),'-') AS no_rek
		, CASE 
		      WHEN 
		       -- `no` LIKE '%000000%' 
		      dt.`no` REGEXP '^[0-0]+$' -- LIKE '%000000%' 
		      OR dt.namaNasabah LIKE '%000000%' 
		      OR dt.`no` IS NULL 
		      OR (dt.bank_id = 'BCA' AND LENGTH(REPLACE(REPLACE(REPLACE(dt.`no`,'.',''),' ',''),'-','')) <> 10 )
		      OR dt.bank_id = '-' 
		      OR ( LENGTH(REPLACE(REPLACE(REPLACE(dt.`no`,'.',''),' ',''),'-','')) < 8 )
		      THEN '2'
		      -- WHEN bank_id = 'BCA' AND `no` NOT LIKE '%000000%' THEN '0'
		      WHEN dt.bank_id = 'BCA' AND dt.`no` NOT REGEXP '^[0-0]+$' THEN '0' 
		      ELSE '1' END AS urut
		, dt.flag
		FROM member m
		LEFT JOIN pgs_bulanan pb ON m.id = pb.member_id AND pb.tgl = '2016-09-30'
		-- left join account a on m.account_id = a.id
		LEFT JOIN (
			SELECT 
			m.id AS memberId, m.ewallet, m.nama, m.account_id
			, IFNULL(acc.bank_id,'-')AS bank_id, IFNULL(acc.area,'-') AS cabang
			, IFNULL(acc.no,'_')AS NO, UCASE(IFNULL(acc.`name`,'-')) AS namaNasabah
			, acc.flag
			FROM member m
			LEFT JOIN account acc ON m.account_id = acc.id
			-- WHERE m.ewallet >= 55000 AND m.id <> '00000001'
			-- and acc.bank_id = 'BCA'
			GROUP BY m.id
			ORDER BY acc.bank_id DESC, m.ewallet DESC
		) AS dt ON m.id = dt.memberId
		WHERE m.tglaplikasi BETWEEN '2016-09-01' AND '2016-09-30'
		AND pb.ps >= 1000000
	) AS dt
) AS dt
LEFT JOIN jenjang j ON dt.jenjang_new = j.id
-- left join member m on dt.member_id = m.id
ORDER BY urut
)AS dt
LEFT JOIN bank_to_bicode btb ON dt.bank_id = btb.id
WHERE urut <> 'NoValid'
and urut = 'Non BCA'
having bicode <> ''
";				
$queryData	= mysqli_query($con,$query) or die(mysqli_error($con));
$xx = 0;
$totalAmount = 0;
echo 'start retrive detail data'."<br>"."\r\n";
while ($row = mysqli_fetch_array($queryData, MYSQLI_ASSOC))
{
	/*
	Field	Name	Type	Length	Value	Mandatory
	
	Detail 					
	1	RECORD-TYPE 				Number	1	1 =Detail 	TRUE
	2	Credited Account 			Number	34	Credit Account 	TRUE
	3	Filler 						String	18	-	TRUE
	4	Credited Amount 			Number	17.2	Account to be credited	TRUE
	5	Credited Amount  Name 		String	70	Receiving Account Name (mandatory for LLG / RTGS)	TRUE 
	6	Transfer Type 				String	6	BCA = B / LLG= N / RTGS = Y	TRUE 
	7	Filler 						String	1	-	
	8	BI CODE 					String 	7		
	9	Filler						String 	4		
	10	Bank Name 					String 	18	bank name 	
	11	RECEIVER-BANK-BRANCH 		String	18		FALSE 
	12	TRANSACTION-REMARK-1		String	18	Remarks	FALSE
	13	TRANSACTION-REMARK-2		String	18	Remarks	FALSE
	14	Filler 						String 	18		
	15	Receiver Cust Type 			String 	1	Jenis Nasabah Penerima (1 = perorangan , 2 = perusahaan, 3 = pemerintah )	TRUE 
	16	Receiver Cust Residence 	String 	1	Status Penduduk Nasabah Penerima	TRUE ( R = penduduk , N = bukan penduduk) - (Non BCA only)	
	17	Bank Code 					Number	3	014/888	014 =BCA , 888 = Non BCA 
	*/
	
	$detail['RECORD-TYPE'][$xx] 			= "1"; 
	$detail['credited_account'][$xx] 		= str_pad($row['noRekening'],34," ",STR_PAD_RIGHT);
	$detail['filler1'][$xx] 				= str_pad("",18," ",STR_PAD_RIGHT); 
	$detail['credited_amount'][$xx] 		= str_pad($row['tkHomePayed'],17,"0",STR_PAD_LEFT).".00";  $totalAmount+=$row['tkHomePayed']; 
	$detail['credited_amount_name'][$xx] 	= str_pad($row['namaNasabah'],70," ",STR_PAD_RIGHT);
	if($row['urut'] == 'BCA') 
		$detail['transfer_type'][$xx] 			= str_pad("B",6," ",STR_PAD_RIGHT);  //BCA = B / LLG= N / RTGS = Y
	else
		$detail['transfer_type'][$xx] 			= str_pad("N",6," ",STR_PAD_RIGHT);  //BCA = B / LLG= N / RTGS = Y
	$detail['filler2'][$xx] 				= str_pad("",1," ",STR_PAD_RIGHT); 
	$detail['BI_CODE'][$xx] 				= str_pad($row['bicode'],7," ",STR_PAD_RIGHT); //BCA = 1111111 
	$detail['filler3'][$xx] 				= str_pad("",4," ",STR_PAD_RIGHT); 
	$detail['bank_name'][$xx] 				= str_pad($row['bank_id'],18," ",STR_PAD_RIGHT); 
	$detail['RECEIVER-BANK-BRANCH'][$xx] 	= str_pad(substr($row['cabang'],0,18),18," ",STR_PAD_RIGHT); 
	$detail['TRANSACTION-REMARK-1'][$xx] 	= str_pad("BONUS MEMBER",18," ",STR_PAD_RIGHT); 
	$detail['TRANSACTION-REMARK-2'][$xx] 	= str_pad("SEPTEMBER 2016",18," ",STR_PAD_RIGHT); 
	$detail['filler4'][$xx] 				= str_pad("",18," ",STR_PAD_RIGHT); 
	$detail['receiver_cust_type'][$xx] 		= "1";  // (1 = perorangan , 2 = perusahaan, 3 = pemerintah )
	$detail['receiver_cust_residence'][$xx] = "R"; //( R = penduduk , N = bukan penduduk) - (Non BCA only)
	if($row['urut'] == 'BCA')
		$detail['bank_code'][$xx] 				= "014"; //014 =BCA , 888 = Non BCA 
	else
		$detail['bank_code'][$xx] 				= "888"; //014 =BCA , 888 = Non BCA 

	$xx++;
}

 /*
 Header 					
1	RECORD-TYPE 		Number	1	0 = Header 	TRUE
2	TRANSACTION-TYPE 	String	2	SP/MP	TRUE
3	EFFECTIVE-DATE 		Number	8	Format YYYYMMDD	TRUE
4	COMPANY-ACCOUNT 	Number	10	To be debited account	TRUE
5	Filler				String 	1	RightPad w/ Space 	-
6	COMPANY-CODE 		Number	8	Company code Auto Credit	TRUE
7	Filler				String 	15		
8	TOTAL-AMOUNT		Number	17.2	Include Decimal Amount	TRUE
9	TOTAL-RECORD 		Number	5	Total of detail records	TRUE
10	Transfer type 		String 	3	BCA/LLG/RTG	TRUE 
11	Filler 				String 	15	-	
12	Remark 1 			String	18	Remark 1 	FALSE 
13	Remark 2 			String	18	Remark 2 	FALSE 
14	Filler 				String 	132	- 	

 */
echo "start retrieve header Data"."<br>"."\r\n";
$header['RECORD-TYPE'] = "0"; 
$header['TRANSACTION-TYPE'] = "MP"; // SP = BCA, MP = NON BCA 
$header['EFFECTIVE-DATE'] = "20161010"; 
$header['COMPANY-ACCOUNT'] = "0033059008";  //BCA unihealth
$header['filler1'] = str_pad("",1," ",STR_PAD_LEFT); 
$header['COMPANY-CODE'] = str_pad("00030331",8," ",STR_PAD_LEFT); 
$header['filler2'] = str_pad("",15," ",STR_PAD_LEFT); 
$header['TOTAL-AMOUNT'] = str_pad($totalAmount,17,"0",STR_PAD_LEFT).".00"; 
$header['TOTAL-RECORD'] = str_pad($xx,5,"0",STR_PAD_LEFT); 
$header['transfer_type'] = "LLG";  //BCA/LLG/RTG
$header['filler3'] = str_pad("",15," ",STR_PAD_LEFT);
$header['remark1'] = str_pad("BONUS MEMBER",18," ",STR_PAD_RIGHT); 
$header['remark2'] = str_pad("SEPTEMBER 2016",18," ",STR_PAD_RIGHT); 
$header['filler4'] = str_pad("",132," ",STR_PAD_LEFT); 

$headerText = "";

$headerText.= $header['RECORD-TYPE'];
$headerText.= $header['TRANSACTION-TYPE'];
$headerText.= $header['EFFECTIVE-DATE'];
$headerText.= $header['COMPANY-ACCOUNT'];
$headerText.= $header['filler1'];
$headerText.= $header['COMPANY-CODE'];
$headerText.= $header['filler2'];
$headerText.= $header['TOTAL-AMOUNT'];
$headerText.= $header['TOTAL-RECORD'];
$headerText.= $header['transfer_type'];
$headerText.= $header['filler3'];
$headerText.= $header['remark1'];
$headerText.= $header['remark2'];
$headerText.= $header['filler4'];
			  
echo "creating file..."."<br>"."\r\n";
$namaFile = "acred_nonbca_bpp_sept2016_".date("YmdHis").".txt";
$myfile = fopen("bcatransferfile/".$namaFile, "wb") or die("Unable to open file!");
//$myfile = fopen("/home/deploy/source/sohomlm/bcatransferfile/".$namaFile, "wb") or die("Unable to open file!");
fwrite($myfile, $headerText."\r\n");
for($yy = 0; $yy < $xx; $yy++){
	$detailText = "";
	$detailText.= $detail['RECORD-TYPE'][$yy];
	$detailText.= $detail['credited_account'][$yy];
	$detailText.= $detail['filler1'][$yy];
	$detailText.= $detail['credited_amount'][$yy];
	$detailText.= $detail['credited_amount_name'][$yy];
	$detailText.= $detail['transfer_type'][$yy];
	$detailText.= $detail['filler2'][$yy];
	$detailText.= $detail['BI_CODE'][$yy];
	$detailText.= $detail['filler3'][$yy];
	$detailText.= $detail['bank_name'][$yy];
	$detailText.= $detail['RECEIVER-BANK-BRANCH'][$yy];
	$detailText.= $detail['TRANSACTION-REMARK-1'][$yy];
	$detailText.= $detail['TRANSACTION-REMARK-2'][$yy];
	$detailText.= $detail['filler4'][$yy];
	$detailText.= $detail['receiver_cust_type'][$yy];
	$detailText.= $detail['receiver_cust_residence'][$yy];
	$detailText.= $detail['bank_code'][$yy];
	fwrite($myfile, $detailText."\r\n");
}
fclose($myfile);

echo "||||||||||.... 100% DONE"."<br>"."\r\n";
echo "<a href='bcatransferfile/".$namaFile."'>".$namaFile."</a>";
?> 

