 <?php
 //----------------------------- database ---------------------------//
$nama_svr = 'localhost';//'sgh-production.cy9qiwmgjpp1.ap-southeast-1.rds.amazonaws.com';
$nama_db = 'soho_20200630_rbfinclose'; //'soho';
$nama_usr = 'admin';
$pwd_usr = 'P@ssw0rd';//'H!21h3j00q1';//

	$con = mysqli_connect($nama_svr, $nama_usr, $pwd_usr);
	if(!$con){
	  trigger_error("Problem connecting to server");
	}	
	$db =  mysqli_select_db( $con,$nama_db);
	if(!$db){
	   trigger_error("Problem selecting database");
	}	
//--------------------------- EOF database ------------------------//

 

$petik = ""; //"\'";
            $query = "SELECT ifnull(btb.bi_code,'') as bicode, dt.*
                FROM(
                        SELECT namaNasabah, bank_id, MAX(cabang)AS cabang, concat('".$petik."',`no`) as noRekening, SUM(ewallet)AS payoutTransfer
                                -- , MAX(biayaTransfer)AS biayaTransfer
                                -- , SUM(ewallet) -  MAX(biayaTransfer) AS tkHomePayed
                                , CASE 
									WHEN MAX(urut) = 0 THEN 0
									WHEN MAX(urut) = 1 THEN MAX(biayaTransfer)
								  END AS biayaTransfer
                                , CASE
									WHEN MAX(urut) = 0 THEN SUM(ewallet)
									WHEN MAX(urut) = 1 THEN	SUM(ewallet) -  MAX(biayaTransfer) 
									-- meike bulan jan 2017
									-- WHEN MAX(urut) = 1 AND NO = '1540007832649' THEN SUM(ewallet) -  MAX(biayaTransfer) - 15991750
									-- meike bulan feb 2017
									-- WHEN MAX(urut) = 1 AND NO <> '1540007832649' THEN SUM(ewallet) -  MAX(biayaTransfer)
								  END AS tkHomePayed
                                -- , urut2, flag
                                , CASE 
                                        WHEN MAX(urut) = 0 THEN 'BCA'
                                        WHEN MAX(urut) = 1 THEN 'Non BCA'
                                        ELSE 'NoValid'
                                END AS urut
								, 'Bonus Member' as Berita
                        FROM
                        (
                                SELECT dtbr.memberId AS memberId, dtbr.account_id
                                        , CASE WHEN dtbr.nama <> dtbr.namaNasabah THEN 'Cek' ELSE '' END AS note
                                        , dtbr.ewallet - IFNULL(bns.bns,0) AS b4, IFNULL(bns.bns,0)AS bns
                                        , dtbr.ewallet - 5000 AS ewallet, dtbr.nama AS nama, dtbr.namaNasabah
                                        , dtbr.bank_id AS bank_id, dtbr.cabang AS cabang
                                        , dtbr.urut AS urut2, flag
                                        , CASE 
                                                WHEN dtbr.urut THEN 5000
                                                WHEN flag = 2 THEN 2500
                                                WHEN flag = 1 THEN 0
                                                ELSE 'n/a'
                                        END AS biayaTransfer
                                        , IFNULL(REPLACE(REPLACE(REPLACE(dtbr.no,'-',''),'.',''),' ',''),'-') AS NO
                                        -- , dtbr.no as no
                                        , dtbr.urut
                                FROM(
                                        SELECT dt.*, CASE 
                                                        WHEN 
                                                                -- `no` LIKE '%000000%' 
                                                                `no` REGEXP '^[0-0]+$' -- LIKE '%000000%' 
                                                                OR namaNasabah LIKE '%000000%' 
                                                                OR `no` IS NULL 
                                                                OR (bank_id = 'BCA' AND LENGTH(REPLACE(REPLACE(REPLACE(`no`,'.',''),' ',''),'-','')) <> 10 )
                                                                OR bank_id = '-' 
																OR ( LENGTH(REPLACE(REPLACE(REPLACE(`no`,'.',''),' ',''),'-','')) < 8 )
                                                        THEN '2'
                                                        -- WHEN bank_id = 'BCA' AND `no` NOT LIKE '%000000%' THEN '0'
														WHEN bank_id = 'BCA' AND `no` NOT REGEXP '^[0-0]+$' THEN '0' 
                                                        ELSE '1' END AS urut
                                        FROM(
                                                SELECT 
                                                        m.id AS memberId, m.ewallet, m.nama, m.account_id
                                                        , IFNULL(acc.bank_id,'-')AS bank_id, IFNULL(acc.area,'-') AS cabang
                                                        , IFNULL(acc.no,'_')AS NO, UCASE(IFNULL(acc.`name`,'-')) AS namaNasabah
                                                        , acc.flag
                                                FROM member m
                                                LEFT JOIN account acc ON m.account_id = acc.id
                                                WHERE m.ewallet >= 55000 AND m.id <> '00000001'
                                                -- and acc.bank_id = 'BCA'
                                                GROUP BY m.id
                                                ORDER BY acc.bank_id DESC, m.ewallet DESC
                                        )AS dt
                                        GROUP BY dt.memberId
                                        ORDER BY dt.bank_id DESC, dt.ewallet DESC
                                )AS dtbr
                                LEFT JOIN(
                                        SELECT member_id, SUM(nominal) AS bns
                                        FROM bonus
                                        WHERE periode = LAST_DAY(CURDATE() - INTERVAL 1 MONTH)
                                        GROUP BY member_id
                                )AS bns ON dtbr.memberId = bns.member_id
                                WHERE ewallet-5000 > 50000
                                ORDER BY urut, bank_id, ewallet DESC
                        )AS dt
                        -- WHERE bank_id = 'BCA'
                        GROUP BY namaNasabah, bank_id, `no`
                        ORDER BY urut, namaNasabah
                )AS dt
				left join bank_to_bicode btb ON dt.bank_id = btb.id
                WHERE urut <> 'NoValid'
				and urut = 'Non BCA'
				-- agustus 2016, september 2016
				/*
				and noRekening NOT IN (
				'zulimaQQAGUNGSET'
				,'0447521345IDR'
				,'0380431760IDR'
				,'0389735964IDR'
				,'477335218IDR'
				)
				*/
				-- november 2016
				/*
				and noRekening NOT IN (
				'0447521345IDR'
				,'0488934423IDR'
				,'0470875125IDR'
				,'0080219427IDR'
				,'4918010p1443537'
				)
				*/
				-- Januari 2017
				/*
				and noRekening NOT IN (
				'0418496005IDR'					   
				,'0447521345IDR'
				,'0470875125IDR'
				,'0326010461>1950'
				)
				*/
				-- Februari 2017
				/*
				and noRekening NOT IN (
				'1540007832649'
				,'0470875125IDR'
				)
				*/
				-- Maret 2017
				/*
				and noRekening NOT IN (
				'1540007832649'
				,'519001007230539Palu'
				,'Tanahabang'
				)
				*/
				/*
				-- April 2017
				and noRekening NOT IN (
				'1540007832649'
				,'519001007230539Palu'
				)
				*/
				/*
				-- mei 2017
				and noRekening NOT IN (
				'1540007832649'
				,'519001007230539Palu'
				,'banakyangdilampirkan'
				,'HNurAliSullah'
				)
				*/
				/*
				-- juni 2017
				and noRekening NOT IN (
				'1540007832649'
				-- ,'519001007230539Palu'
				,'banakyangdilampirkan'
				,'2554384435idr'
				-- ,'HNurAliSullah'
				)
				*/
				/*
				-- july 2017
				and noRekening NOT IN (
				'1540007832649'
				,'tidakdiisiysb'
				,'databankbukanybs'
				,'tidkaadalampirandant'
				)
				*/
				/*
				-- Agustus 2017
				and noRekening NOT IN (
				'1540007832649'
				,'tidakdiisiysb'
				,'databankbukanybs'
				-- ,'tidkaadalampirandant'
				)
				*/
				/*
				-- September 2017
				and noRekening NOT IN (
				'0484963621IDR'
				)
				*/
				/*
				-- Oktober 2017
				and noRekening NOT IN (
				'0484963621IDR'
				,'0387395021IDR'
				,'0393104772IDR'
				)
				*/
				/*
				-- November 2017
				and noRekening NOT IN (
					'000000[0000'
				)
				*/
				/*
				-- December 2017
				and noRekening NOT IN (
					'IMAMBONJOLPALU'
					,'0438105213IDR'
					,'0645667618IDR'
					,'0611880741IDR'
				)
				*/
				/*
				-- Januari 2018
				and noRekening NOT IN (
					'0464485963IDR'
					,'0700087878489?'
				)
				*/
				/*
				-- Februari 2018
				and noRekening NOT IN (
					'036301014090=500'
					,'0674915331IDR'
					,'0291433064IDR'
					,'0372451642IDR'
					,'0435524511IDR'
					,'4040777999IDR'
					,'0700087878489?'
				)
				*/
				/*
				-- Maret 2018
				and noRekening NOT IN (
					'0691100086IDR'
					,'0503673918IDR'
					,'0291433064IDR'
					,'0428626545IDR'
					,'RISPARUSSIANYSITORU'
				)
				*/
				/*
				-- April 2018
				and noRekening NOT IN (
				'516101022897531`'
				)
				*/
				/*
				-- Mei 2018
				and noRekening NOT IN (
				'0381683333IDR'
				,'0201276506IDR'
				,'0698383055IDR'
				,'0357523990IDR'
				,'0809874210IDR'
				)
				*/
				/*
				-- Juni 2018
				and noRekening NOT IN (
				'214101015677503:'
				,'0587837392IDR'
				,'1540009722772:'
				,'491401015416534:'
				,'0326010461>1950'
				)
				*/
				/*
				-- Juli 2018
				and noRekening NOT IN (
				'0119863011IDR'
				,'0104126391IDR'
				,'0379635928IDR'
				)
				*/
				/*
				-- Agustus 2018
				and noRekening NOT IN (
				'1540020001958' -- ROVENNY
				,'0119863011IDR'
				,'0104126391IDR'
				,'0379635928IDR'
				)
				*/
				/*
				-- September 2018
				and noRekening NOT IN (
				'1540020001958' -- ROVENNY
				,'563201023584534' -- Dela Marsella info pak riky
				,'0119863011IDR'
				,'0463887289IDR'
				,'0104126391IDR'
				,'106=0010613134'
				)
				*/
				/*
				-- October 2018
				and noRekening NOT IN (
				'563201023584534' -- Dela Marsella info pak riky
				,'0119863011IDR'
				,'0763869392IDR'
				,'0377038649IDR'
				,'0104126391IDR'
				,'106=0010613134'
				)
				*/
				/*
				-- november 2018
				and noRekening NOT IN (
				'563201023584534' -- Dela Marsella info pak riky
				,'0119863011IDR'
				,'0763869392IDR'
				,'0104126391IDR'
				,'106=0010613134'
				,'500601OO1147533'
				,'O371813026'
				)
				*/
				/*
				-- Desember 2018
				and noRekening NOT IN (
				'0119863011IDR'
				,'0763869392IDR'
				,'0784331961IDR'
				,'0725859980IDR'
				,'0104126391IDR'
				,'500601OO1147533'
				,'O371813026'
				)
				*/
				/*
				-- Januari 2019
				and noRekening NOT IN (
				'0482048385IDR'
				,'0119863011IDR'
				,'0763869392IDR'
				,'0389865983IDR'
				,'0784331961IDR'
				,'0725859980IDR'
				,'0104126391IDR'
				,'500601OO1147533'
				,'O371813026'
				)
				*/
				/*
				-- Februari 2019
				and noRekening NOT IN (
				'0482048385IDR'
				,'0119863011IDR'
				,'0763869392IDR'
				,'0389865983IDR'
				,'0380087103IDR'
				,'0790836362IDR'
				,'0784331961IDR'
				,'0725859980IDR'
				,'0104126391IDR'
				,'1717170959idr'
				,'500601OO1147533'
				,'O371813026'
				)
				*/
				/*
				-- Maret 2019
				and noRekening NOT IN (
				'1717170959idr'
				,'0104126391IDR'
				,'0725859980IDR'
				,'0784331961IDR'
				,'0792549004IDR'
				,'0389865983IDR'
				,'0763869392IDR'
				,'0119863011IDR'
				,'0482048385IDR'
				,'500601OO1147533'
				,'O533308021'
				)
				*/
				/*
				-- April 2019
				and noRekening NOT IN (
				'0482048385IDR'
				,'0119863011IDR'
				,'0763869392IDR'
				,'0389865983IDR'
				,'0792549004IDR'
				,'0784331961IDR'
				,'0707118422IDR'
				,'0725859980IDR'
				,'0104126391IDR'
				,'500601OO1147533'
				,'1717170959idr'
				,'O533308021'
				)
				*/
				/*
				-- Mei 2019
				and noRekening NOT IN (
				'0816261310IDR'
				,'0482048385IDR'
				,'0119863011IDR'
				,'0763869392IDR'
				,'0389865983IDR'
				,'0792549004IDR'
				,'496470049IDR'
				,'0725859980IDR'
				,'0104126391IDR'
				,'0495479054IDR'
				,'500601OO1147533'
				,'1717170959idr'
				,'O533308021'
				)
				*/
				/*
				-- September 2019
				and noRekening NOT IN (
				'347201033113=537'
				)
				*/
				/*
				-- Oktober 2019
				and noRekening NOT IN (
				'0624688166IDR'
				,'0790836362IDR'
				,'0494648050IDR'
				,'353_343_582'
				)
				*/
				/*
				-- November 2019
				and noRekening NOT IN (
				'0453469589IDR'
				,'0494648050IDR'
				,'0839468851IDR'
				,'353_343_582'
				,'480_455_461'
				)
				*/
				/*
				-- Desember 2019
				and noRekening NOT IN (
				'0358954143IDR'
				,'0612501311IDR'
				,'0535908139IDR'
				-- ,'51240¹001188501'
				,'1520014580647'
				,'1520012438087'
				)
				*/
				/*
				-- Januari 2020
				and noRekening NOT IN (
				'0773408500idr'
				,'0473524594IDR'
				,'0812271480idr'
				,'0535908139IDR'
				,'1520014580647'
				,'1520012438087'

				)
				*/
				/*
				-- Februari 2020
				and noRekening NOT IN (
				'0863854927idr'
				,'38110100323050t'
				)
				*/
				/*
				-- Maret 2020
				and noRekening NOT IN (
				'0707118422IDR'
				)
				*/
				/*
				-- April 2020
				and noRekening NOT IN (
				'0801337808IDR'
				)
				*/
				/*
				-- Mei 2020
				and noRekening NOT IN (
				'0773234542idr'
				)
				*/
				-- Juni 2020
				and noRekening NOT IN (
				'0496470041IDR'
				)
				having bicode <> ''
				-- limit 0,64
                ";
$queryData	= mysqli_query($con,$query) or die(mysqli_error($con));
$xx = 0;
$totalAmount = 0;
echo 'start retrive detail data'."<br>"."\r\n";
while ($row = mysqli_fetch_array($queryData, MYSQLI_ASSOC))
{
	/*
	Field	Name	Type	Length	Value	Mandatory
	
	Detail 					
	1	RECORD-TYPE 				Number	1	1 =Detail 	TRUE
	2	Credited Account 			Number	34	Credit Account 	TRUE
	3	Filler 						String	18	-	TRUE
	4	Credited Amount 			Number	17.2	Account to be credited	TRUE
	5	Credited Amount  Name 		String	70	Receiving Account Name (mandatory for LLG / RTGS)	TRUE 
	6	Transfer Type 				String	6	BCA = B / LLG= N / RTGS = Y	TRUE 
	7	Filler 						String	1	-	
	8	BI CODE 					String 	7		
	9	Filler						String 	4		
	10	Bank Name 					String 	18	bank name 	
	11	RECEIVER-BANK-BRANCH 		String	18		FALSE 
	12	TRANSACTION-REMARK-1		String	18	Remarks	FALSE
	13	TRANSACTION-REMARK-2		String	18	Remarks	FALSE
	14	Filler 						String 	18		
	15	Receiver Cust Type 			String 	1	Jenis Nasabah Penerima (1 = perorangan , 2 = perusahaan, 3 = pemerintah )	TRUE 
	16	Receiver Cust Residence 	String 	1	Status Penduduk Nasabah Penerima	TRUE ( R = penduduk , N = bukan penduduk) - (Non BCA only)	
	17	Bank Code 					Number	3	014/888	014 =BCA , 888 = Non BCA 
	*/
	
	$detail['RECORD-TYPE'][$xx] 			= "1"; 
	$detail['credited_account'][$xx] 		= str_pad($row['noRekening'],34," ",STR_PAD_RIGHT);
	$detail['filler1'][$xx] 				= str_pad("",18," ",STR_PAD_RIGHT); 
	$detail['credited_amount'][$xx] 		= str_pad($row['tkHomePayed'],17,"0",STR_PAD_LEFT).".00";  $totalAmount+=$row['tkHomePayed']; 
	$detail['credited_amount_name'][$xx] 	= str_pad($row['namaNasabah'],70," ",STR_PAD_RIGHT);
	if($row['urut'] == 'BCA') 
		$detail['transfer_type'][$xx] 			= str_pad("B",6," ",STR_PAD_RIGHT);  //BCA = B / LLG= N / RTGS = Y
	else
		$detail['transfer_type'][$xx] 			= str_pad("N",6," ",STR_PAD_RIGHT);  //BCA = B / LLG= N / RTGS = Y
	$detail['filler2'][$xx] 				= str_pad("",1," ",STR_PAD_RIGHT); 
	$detail['BI_CODE'][$xx] 				= str_pad($row['bicode'],7," ",STR_PAD_RIGHT); //BCA = 1111111 
	$detail['filler3'][$xx] 				= str_pad("",4," ",STR_PAD_RIGHT); 
	$detail['bank_name'][$xx] 				= str_pad($row['bank_id'],18," ",STR_PAD_RIGHT); 
	$detail['RECEIVER-BANK-BRANCH'][$xx] 	= str_pad(substr($row['cabang'],0,18),18," ",STR_PAD_RIGHT); 
	$detail['TRANSACTION-REMARK-1'][$xx] 	= str_pad("BONUS MEMBER",18," ",STR_PAD_RIGHT); 
	$detail['TRANSACTION-REMARK-2'][$xx] 	= str_pad("JUNI 2020",18," ",STR_PAD_RIGHT); 
	$detail['filler4'][$xx] 				= str_pad("",18," ",STR_PAD_RIGHT); 
	$detail['receiver_cust_type'][$xx] 		= "1";  // (1 = perorangan , 2 = perusahaan, 3 = pemerintah )
	$detail['receiver_cust_residence'][$xx] = "R"; //( R = penduduk , N = bukan penduduk) - (Non BCA only)
	if($row['urut'] == 'BCA')
		$detail['bank_code'][$xx] 				= "014"; //014 =BCA , 888 = Non BCA 
	else
		$detail['bank_code'][$xx] 				= "888"; //014 =BCA , 888 = Non BCA 

	$xx++;
}

 /*
 Header 					
1	RECORD-TYPE 		Number	1	0 = Header 	TRUE
2	TRANSACTION-TYPE 	String	2	SP/MP	TRUE
3	EFFECTIVE-DATE 		Number	8	Format YYYYMMDD	TRUE
4	COMPANY-ACCOUNT 	Number	10	To be debited account	TRUE
5	Filler				String 	1	RightPad w/ Space 	-
6	COMPANY-CODE 		Number	8	Company code Auto Credit	TRUE
7	Filler				String 	15		
8	TOTAL-AMOUNT		Number	17.2	Include Decimal Amount	TRUE
9	TOTAL-RECORD 		Number	5	Total of detail records	TRUE
10	Transfer type 		String 	3	BCA/LLG/RTG	TRUE 
11	Filler 				String 	15	-	
12	Remark 1 			String	18	Remark 1 	FALSE 
13	Remark 2 			String	18	Remark 2 	FALSE 
14	Filler 				String 	132	- 	

 */
echo "start retrieve header Data"."<br>"."\r\n";
$header['RECORD-TYPE'] = "0"; 
$header['TRANSACTION-TYPE'] = "MP"; // SP = BCA, MP = NON BCA 
$header['EFFECTIVE-DATE'] = "20200709"; 
$header['COMPANY-ACCOUNT'] = "0033059008";  //BCA unihealth
$header['filler1'] = str_pad("",1," ",STR_PAD_LEFT); 
$header['COMPANY-CODE'] = str_pad("00030331",8," ",STR_PAD_LEFT); 
$header['filler2'] = str_pad("",15," ",STR_PAD_LEFT); 
$header['TOTAL-AMOUNT'] = str_pad($totalAmount,17,"0",STR_PAD_LEFT).".00"; 
$header['TOTAL-RECORD'] = str_pad($xx,5,"0",STR_PAD_LEFT); 
$header['transfer_type'] = "LLG";  //BCA/LLG/RTG
$header['filler3'] = str_pad("",15," ",STR_PAD_LEFT);
$header['remark1'] = str_pad("BONUS MEMBER",18," ",STR_PAD_RIGHT); 
$header['remark2'] = str_pad("JUNI 2020",18," ",STR_PAD_RIGHT); 
$header['filler4'] = str_pad("",132," ",STR_PAD_LEFT); 

$headerText = "";

$headerText.= $header['RECORD-TYPE'];
$headerText.= $header['TRANSACTION-TYPE'];
$headerText.= $header['EFFECTIVE-DATE'];
$headerText.= $header['COMPANY-ACCOUNT'];
$headerText.= $header['filler1'];
$headerText.= $header['COMPANY-CODE'];
$headerText.= $header['filler2'];
$headerText.= $header['TOTAL-AMOUNT'];
$headerText.= $header['TOTAL-RECORD'];
$headerText.= $header['transfer_type'];
$headerText.= $header['filler3'];
$headerText.= $header['remark1'];
$headerText.= $header['remark2'];
$headerText.= $header['filler4'];
			  
echo "creating file..."."<br>"."\r\n";
$namaFile = "acred_nonbca_".date("YmdHis").".txt";
$myfile = fopen("bcatransferfile/".$namaFile, "wb") or die("Unable to open file!");
//$myfile = fopen("/home/deploy/source/sohomlm/bcatransferfile/".$namaFile, "wb") or die("Unable to open file!");
fwrite($myfile, $headerText."\r\n");
for($yy = 0; $yy < $xx; $yy++){
	$detailText = "";
	$detailText.= $detail['RECORD-TYPE'][$yy];
	$detailText.= $detail['credited_account'][$yy];
	$detailText.= $detail['filler1'][$yy];
	$detailText.= $detail['credited_amount'][$yy];
	$detailText.= $detail['credited_amount_name'][$yy];
	$detailText.= $detail['transfer_type'][$yy];
	$detailText.= $detail['filler2'][$yy];
	$detailText.= $detail['BI_CODE'][$yy];
	$detailText.= $detail['filler3'][$yy];
	$detailText.= $detail['bank_name'][$yy];
	$detailText.= $detail['RECEIVER-BANK-BRANCH'][$yy];
	$detailText.= $detail['TRANSACTION-REMARK-1'][$yy];
	$detailText.= $detail['TRANSACTION-REMARK-2'][$yy];
	$detailText.= $detail['filler4'][$yy];
	$detailText.= $detail['receiver_cust_type'][$yy];
	$detailText.= $detail['receiver_cust_residence'][$yy];
	$detailText.= $detail['bank_code'][$yy];
	fwrite($myfile, $detailText."\r\n");
}
fclose($myfile);

echo "||||||||||.... 100% DONE"."<br>"."\r\n";
echo "<a href='bcatransferfile/".$namaFile."'>".$namaFile."</a>";
?> 

